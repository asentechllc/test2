<?php

class Mcmr_Date extends Zend_Date
{
    protected static $_defaultTimezone = null;
    
    public function __construct($date = null, $part = null, $locale = null) 
    {
        parent::__construct($date, $part, $locale);
        
        $this->setTimezone(self::$_defaultTimezone);
    }
    
    public static function setDefaultTimezone($timezone)
    {
        self::$_defaultTimezone = $timezone;
    }
    
    public static function getDefaultTimezone()
    {
        return self::$_defaultTimezone;
    }
    
    /**
     * Returns the actual date as new date object
     *
     * @param  string|Zend_Locale        $locale  OPTIONAL Locale for parsing input
     * @return Zend_Date
     */
    public static function now($locale = null)
    {
        return new Mcmr_Date(time(), self::TIMESTAMP, $locale);
    }

    public function toString( $format = null, $type = null, $locale = null ) 
    {
        if ( 'php' == $type ) {
            $formatParts = preg_split( '/(\\\\.)/', $format, 0, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY );
            $outputParts = array();
            foreach ($formatParts as $f ) {
                if ( '\\' == substr($f, 0, 1) ) {
                    $outputParts[] = substr( $f, 1);
                } else {
                    $outputParts[] = Zend_Date::toString($f, 'php');
                }
            }
            return implode($outputParts);
        } else {
            return Zend_Date::toString( $format, $type, $locale );
        }
    }
    
}
