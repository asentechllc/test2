<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Payment.php 1226 2010-08-16 10:44:09Z leigh $
 */

/**
 * Class for Payment services
 *
 * @category Mcmr
 * @package Mcmr_Payment
 */
class Mcmr_Payment
{
    static private $_loader = null;
    
    /**
     * Factory for Mcmr_Payment_Adapter classes
     *
     * $adapter corresponds to the adapter class base. For example 'SagepayServer'
     * corresponds to Mcmr_Payment_Adapter_SagepayServer
     *
     * @param string $adapter
     * @param array|Zend_Config $config
     * @return Mcmr_Payment_AdapterAbstract
     * @throws Mcmr_Payment_Exception
     */
    public static function factory($adapter, $config = array())
    {
        if (null === self::$_loader) {
            $loader = new Zend_Loader_PluginLoader();
            $loader->addPrefixPath('Mcmr_Payment_Adapter', 'Mcmr/Payment/Adapter');
        }

        $paymentClass = $loader->load($adapter);
        
        return new $paymentClass($config);
    }
    
}