<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Settings
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 */

/**
 * A class to get/set settings using the DB
 *
 * @category Mcmr
 * @package Mcmr_Settings
 */
class Mcmr_Settings
{

	private static $_cache = array();

	/**
	 * Get setting or group of settings
	 **/
	public static function get($group,$key=null,$refresh=false){
	
		# check cache first
		if(!$refresh && isset(self::$_cache[$group])){
		
			if(array_key_exists($key,self::$_cache[$group]))
				return self::$_cache[$group][$key];
				
		}
	
		# fetch setting from DB
		$dbRows = self::_readFromDb($group, $key);
		if (!isset(self::$_cache[$group])) {
			self::$_cache[$group] = array();
		}
		if(!$key){
			foreach($dbRows as $row){
				self::$_cache[$group][$row['setting']] = $row['value'];
			}
		} else {
			$row = count($dbRows)?$dbRows[0]:null;
			self::$_cache[$group][$key] = $row ? $row['value'] : null;
		}

		return $key ? self::$_cache[$group][$key] : self::$_cache[$group];
	}

	protected static function _readFromDb($group, $key=null)
	{
        $cacheid = self::_getCacheId($group, $key);
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        if (false === ($result = $cache->load($cacheid))) {
			if ($db = self::_getDb()) {
				$params = array($group,$key);
				// Added to fix the MySQL Error - Invalid parameters bound
				if(null === $key){
					$params = array($group);
				}
				if($key){
					$stmt = $db->query('SELECT * FROM settings WHERE `group` = ? AND `setting` = ? LIMIT 1', $params);
				} else {
					$stmt = $db->query('SELECT * FROM settings WHERE `group` = ?', array($group));
				} 
				$result = $stmt->fetchAll();
			} 
			$cache->save($result, $cacheid, array('setting'));
		}
		return $result;
	}

    private static function _cleanCache()
    {
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array('setting'));
    }

	protected static function _getCacheId($group, $key=null) {
        $params = 'group:'.$group;
        if (null!==$key) {
        	$params .=',key:'.$key;
        }
        return 'Sertting_'.md5($params);
	}
	
	/**
	 * Set a setting or group of settings
	 **/
	public static function set($group,$key,$value=null){
		if($db = self::_getDb()){
			if(is_array($key)){

				# replace mode
				if($value){
					$stmt = $db->query("DELETE FROM settings WHERE `group` = ?",array($group));
				}

				foreach($key as $k=>$v){
					self::_replace($db,$group,$k,$v);
				}
			}
			else{
				self::_replace($db,$group,$key,$value);
			}
			self::_cleanCache();
			return true;
		}
		return false;
	}
	
	private static function _replace($db,$group,$key,$value){
		$stmt = $db->prepare('REPLACE INTO settings (`group`,`setting`,`value`) VALUES(?,?,?)');
		$stmt->execute(array($group,$key,$value));
		self::_cleanCache();
	}
	
	private static function _getDb(){
		# fetch setting from DB
		$front = Zend_Controller_Front::getInstance();
		$bootstrap = $front->getParam("bootstrap");
		if ($bootstrap->hasPluginResource("db")) {
			$dbResource = $bootstrap->getPluginResource("db");
			return $dbResource->getDbAdapter();
		}
		return false;
	}

}