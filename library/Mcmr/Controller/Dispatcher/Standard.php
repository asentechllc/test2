<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mcmr Dispatcher. Adds site to the standard module/controller/action
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Dispatcher
 */
class Mcmr_Controller_Dispatcher_Standard extends Zend_Controller_Dispatcher_Standard 
{
    /**
     * Default site
     * @var string
     */
    protected $_defaultSite = 'default';
    
    /**
     * Get the default site name
     *
     * @return string
     */
    public function getDefaultSite() 
    {
        return $this->_defaultSite;
    }

    /**
     * Set the default site name
     *
     * @param string $defaultSite
     * @return Mcmr_Controller_Dispatcher_Standard 
     */
    public function setDefaultSite($defaultSite) 
    {
        $this->_defaultSite = (string) $defaultSite;
        
        return $this;
    }
}
