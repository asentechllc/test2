<?php

class Mcmr_Controller_Router_Route_Redirect extends Zend_Controller_Router_Route
{
    public function match($path, $partial = false)
    {
        if ($route = parent::match($path, $partial)) {
            $helper = new Zend_Controller_Action_Helper_Redirector();
            $helper->setCode(301);
            
            if(isset($route[0]) && count($route) == 1){
	            $helper->gotoUrl($route[0]);
            }
            else{
	            $helper->gotoRoute($route);
	        }
        }
    }
}