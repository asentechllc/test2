<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A development route that always formats the URL using the default structure.
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Mcmr_Controller_Router_Development extends Zend_Controller_Router_Rewrite
{
    public function  addRoute($name, Zend_Controller_Router_Route_Interface $route)
    {
        $route = Zend_Controller_Router_Route_Module::getInstance(new Zend_Config(array()));

        return parent::addRoute($name, $route);
    }
}
