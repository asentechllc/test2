<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ViewRenderer.php 1310 2010-09-03 16:04:33Z leigh $
 */

/**
 * ViewRender action helper used to change the view template script.
 * Will automatically fallback if the script doesn't exist.
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Action
 */
class Mcmr_Controller_Action_Helper_ViewRenderer extends Zend_Controller_Action_Helper_ViewRenderer
{
    protected $_oldActions = array();

    protected $_allowFallback = true;

    protected $_viewSitePathSpec = null;
    
    protected function _getSitePath()
    {
        if (null === $this->_actionController) {
            return './views';
        }

        $inflector = new Zend_Filter_Inflector();
        $inflector->setStaticRuleReference('moduleDir', $this->_moduleDir) // moduleDir must be specified before the less specific 'module'
             ->addRules(array(
                 ':site'     => array('Word_CamelCaseToDash', 'StringToLower'),
                 ':module'     => array('Word_CamelCaseToDash', 'StringToLower'),
                 ':controller' => array('Word_CamelCaseToDash', new Zend_Filter_Word_UnderscoreToSeparator('/'), 'StringToLower', new Zend_Filter_PregReplace('/\./', '-')),
                 ':action'     => array('Word_CamelCaseToDash', new Zend_Filter_PregReplace('#[^a-z0-9' . preg_quote('/', '#') . ']+#i', '-'), 'StringToLower'),
             ))
             ->setStaticRuleReference('suffix', $this->_viewSuffix)
             ->setTargetReference($this->getViewSitePathSpec());

        $dispatcher = $this->getFrontController()->getDispatcher();
        $request = $this->getRequest();

        $parts = array(
            'site'       => 'default',
            'module'     => (($moduleName = $request->getModuleName()) != '') ? $dispatcher->formatModuleName($moduleName) : $moduleName,
            'controller' => $request->getControllerName(),
            'action'     => $dispatcher->formatActionName($request->getActionName())
            );

        if (method_exists($request, 'getSiteName')) {
            $parts['site'] = $request->getSiteName();
        }
        $path = $inflector->filter($parts);
        return $path;
    }
    
    public function setViewSitePathSpec($path)
    {
        $this->_viewSitePathSpec = (string) $path;
        return $this;
    }
    
    public function &getViewSitePathSpec()
    {
        if (null === $this->_viewSitePathSpec) {
            return $this->_viewBasePathSpec;
        } else {
            return $this->_viewSitePathSpec;
        }
    }
    
    public function initView($path = null, $prefix = null, array $options = array()) 
    {
        parent::initView($path, $prefix, $options);
        
        if (null === $prefix) {
            $prefix = $this->_generateDefaultPrefix();
        }
        
        $this->view->addBasePath($this->_getSitePath(), $prefix);
    }
    
    /**
     * Set whether the View Renderer should fallback to previous action names if the name is overridden
     *
     * @param boolean $fallback
     */
    public function setAllowFallback($fallback)
    {
        $this->_allowFallback = $fallback;

        return $this;
    }

    /**
     * Render a view based on path specifications
     *
     * Renders a view based on the view script path specifications.
     *
     * @param  string  $action
     * @param  string  $name
     * @param  boolean $noController
     * @return void
     */
    public function render($action = null, $name = null, $noController = null)
    {
        // If the render fails recall this function with a previously set action name
        try {
            parent::render($action, $name, $noController);
        } catch (Zend_View_Exception $e) {
            Mcmr_Debug::dump('ViewRenderer: '.$e->getMessage(), 6, 'file');
            if (!empty ($this->_oldActions) && $this->_allowFallback) {
                $action = array_pop($this->_oldActions);
                $this->setScriptAction($action);
                $this->render();
            } else {
                throw $e;
            }
        }
    }

    /**
     * Use this helper as a method; proxies to setRender()
     *
     * @param  string  $action
     * @param  string  $name
     * @param  boolean $noController
     * @return void
     */
    public function direct($action = null, $name = null, $noController = null)
    {
        $action = strtolower($action);
        
        // Record the old action in case the new action fails
        $oldAction = $this->getScriptAction();
        if (null === $oldAction) {
            $request = $this->getRequest();
            $oldAction = $request->getActionName();
        }

        array_unshift($this->_oldActions, $oldAction);
        parent::direct($action, $name, $noController);
    }

    /**
     * Overwrite to check if device-specific view script exists. if not return the default one.
     */
    protected function _translateSpec(array $vars = array())
    {
        $defaultScript = parent::_translateSpec($vars);
        if(!$this->view->getDeviceViews())
        {
            return $defaultScript;
        }
        $view = $this->view;
        $request = $this->getRequest();
        $inflector = $this->getInflector();
        $originalSuffix = $inflector->getRules('suffix');
        $deviceSuffix = $request->getDeviceClass().'.'.$originalSuffix;
        $inflector->setStaticRule('suffix', $deviceSuffix);
        $deviceScript = parent::_translateSpec($vars);
        $inflector->setStaticRule('suffix', $originalSuffix);
        if ($view->scriptExists($deviceScript)) {
            return $deviceScript;
        }
        return $defaultScript;
    }

}
