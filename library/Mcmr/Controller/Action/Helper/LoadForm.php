<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: LoadForm.php 1460 2010-09-29 17:06:34Z leigh $
 */

/**
 * An Action helper used to load form objects. Uses the plugin interface
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Action
 */
class Mcmr_Controller_Action_Helper_LoadForm extends Zend_Controller_Action_Helper_Abstract
{

    /**
     * @var Zend_Loader_PluginLoader
     */
    private $_pluginLoader;

    const PLUGIN_CONFIG = 'plugins.ini';

    /**
     * Constructor: initialize plugin loader
     *
     * @return void
     */
    public function __construct()
    {
        $this->_pluginLoader = new Zend_Loader_PluginLoader();
        $this->_addNamespaces();
    }

    /**
     * Strategy pattern: call helper as broker method
     *
     * @param  string $name
     * @param  array|Zend_Config $options
     * @return Zend_Form
     */
    public function direct($name, $options=null)
    {
        return $this->loadForm($name, $options);
    }
    
    /**
     * Load a form with the provided options
     *
     * @param  string $name
     * @param  array|Zend_Config $options
     * @return Zend_Form
     */
    public function loadForm($name, $options=null)
    {
        $name = ucfirst((string) $name);
        $formClass = $this->_pluginLoader->load($name);
        
        return new $formClass($options);
    }


    private function _addNamespaces()
    {
        // Set the default plugin prefix path to the module path
        $module = $this->getRequest()->getModuleName();
        $front = $this->getFrontController();
        $default = $front->getDispatcher()
                        ->getDefaultModule();
        if (empty($module)) {
            $module = $default;
        }
        $moduleDirectory = $front->getControllerDirectory($module);
        $formsDirectory = dirname($moduleDirectory) . '/forms';

        $prefix = (('default' == $module) ? '' : ucfirst($module) . '_')
                . 'Form_';
        $this->_pluginLoader->addPrefixPath($prefix, $formsDirectory);

        // Load the configured prefix paths
        $options = null;
        try {
            // Get the config. If we can't load the config file ignore it.
            $options = Mcmr_Config_Ini::getInstance(self::PLUGIN_CONFIG, APPLICATION_ENV);
            if (isset($options->form)) {
                $options = $options->form->toArray();
            } else {
                $options = null;
            }
        } catch (Exception $e) {
        }

        if (null !== $options) {
            foreach ($options['namespace'] as $namespace) {
                $this->_pluginLoader->addPrefixPath($namespace['prefix'], $namespace['path']);
            }
        }
    }
}
