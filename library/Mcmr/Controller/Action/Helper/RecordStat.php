<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: LoadForm.php 1240 2010-08-17 17:22:40Z leigh $
 */

/**
 * An Action helper used to record model action statistics
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Action
 */class Mcmr_Controller_Action_Helper_RecordStat extends Zend_Controller_Action_Helper_Abstract
{
    /**
     * Strategy pattern: call helper as broker method
     *
     * @param  string $name
     * @param  array|Zend_Config $options
     * @return Zend_Form
     */
    public function direct($modelname, $action, $id)
    {
        return $this->recordStat($modelname, $action, $id);
    }

    /**
     * Increments a counter against the model name, action and id. Can be used to record
     * things like read statistics etc.
     *
     * @param string $modelname
     * @param string $action
     * @param int $id
     */
    public function recordStat($modelname, $action, $id)
    {
        $mapper = Default_Model_Stat::getMapper();
        $stat = $mapper->findOneByField(
            array(
                'modeltype'=>'News_Model_Article',
                'modelid'=>$id,
                'date'=>  strtotime('today'),
                'action'=>'read'
            )
        );
        $stat->increment();
        $mapper->save($stat);
    }
}
