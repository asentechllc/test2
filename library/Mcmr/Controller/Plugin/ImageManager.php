<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ImageManager.php 1368 2010-09-10 18:45:18Z leigh $
 */

/**
 * Controller plugin to look after the Image Manager authentication
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Plugin
 */
class Mcmr_Controller_Plugin_ImageManager extends Zend_Controller_Plugin_Abstract
{
    const SESSION_NAME='mcImageManagerAuthorised';
    
    /**
     * Determine whether the user has access to the image manager and store that
     * result in the session to be accessed by the image manager system
     *
     * @param Zend_Controller_Request_Abstract $request 
     */
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        $user = Zend_Registry::get('authenticated-user');
        if (null !== $user) {
            $acl = Mcmr_Acl::getInstance();

            // Set the access privilege
            $_SESSION[self::SESSION_NAME] = $acl->isAllowed($user->getRole(), null, 'image-manager');
        }
    }
}
