<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 */

/**
 * Plugin to fix URLs if entered incorrectly
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Plugin
 */
class Mcmr_Controller_Plugin_UrlFixer extends Zend_Controller_Plugin_Abstract
{
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        // Correct consecutive slashes in the URL.
        $uri = $request->getRequestUri();
        $correctedUri = preg_replace('/\/{2,}/', '/', $uri);
        
        # INT112-374 - redirect /index.php URLs to correct URL
        $correctedUri = preg_replace('/^\/index\.php/i', '', $correctedUri);

        if ($uri != $correctedUri) {
        	/*$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('Redirector');
        	$redirector->setCode(301);
        	$redirector->gotoUrl($correctedUri);*/
        	
        	# INT112-374 redirector would put the index.php back in resulting in infinite loop
        	# so we're going old-school with this one.
			header("HTTP/1.1 301 Moved Permanently");
			header("Location: ".$correctedUri);
			exit;
        }
    }
}
