<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Acl.php 1901 2010-12-06 13:25:08Z leigh $
 */

/**
 * Plugin to authenticate the user for this module controller and action.
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Plugin
 */
class Mcmr_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
    private $_user = null;
    
    /**
     * Fetch the authenticated user and put them in the registry
     *
     * @param Zend_Controller_Request_Abstract $request 
     */
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        // Get the authenticated user and user role
        $user = new User_Model_User();
        $user->setRole('guest');
        $this->_user = $user;
        
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $username = $auth->getIdentity();
            $mapper = User_Model_User::getMapper();
            $user = $mapper->findByUsername($username);
            $mapper->notify('authenticated', $user);
        }
        
        if (!$user instanceof User_Model_User) {
            $log = new Zend_Log(new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/acl.log'));
            $log->log(print_r($user, true), Zend_Log::INFO);
            
            $user = new User_Model_User();
        }
        
        $this->_user = $user;
        Zend_Registry::set('authenticated-user', $user);
    }
    
    /**
     * Check the user's Access. Performed on every dispatch loop.
     *
     * @param Zend_Controller_Request_Abstract $request 
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request) 
    {
        $acl = Mcmr_Acl::getInstance();
        $role = $this->_user->getRole();

        // Set up system ACL variables including navigation view helpers
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultAcl($acl);
        Zend_View_Helper_Navigation_HelperAbstract::setDefaultRole($role);

        // Check the user's access
        $resource = Mcmr_Acl::getResourceName($request->getModuleName(), $request->getControllerName());
        try {
            if (! $acl->isAllowed($role, $resource, $request->getActionName())) {
                $config = Zend_Registry::get('default-config');
                
                $this->getResponse()->setHttpResponseCode(401);
                if ('guest' == $role) {
                    // User is not logged in, redirect to login page.
                    $request->setModuleName('user')
                            ->setControllerName('index')
                            ->setActionName('login')
                            ->setParam('format', 'html');
                } elseif (isset($config->unauthorised)) {
                    // Redirect the user to the unauthorised page
                    $request->setModuleName($config->unauthorised->module)
                            ->setControllerName($config->unauthorised->controller)
                            ->setActionName($config->unauthorised->action)
                            ->setParam('format', 'html');
                    if (isset($config->unauthorised->params)) {
                        foreach ($config->unauthorised->params as $key=>$value) {
                            $request->setParam($key, $value);
                        }
                    }
                } else {
                    // No configuration set, just throw an access denied error
                    $request->setModuleName('')->setControllerName('')->setActionName('');
                    $reason = $acl->getReason();
                    $messages = $acl->getMessages();
                    throw new Mcmr_Exception_PermissionDenied($reason.($messages ? '<ul><li>'.implode('</li><li>',$messages).'</li></ul>' : ''));
                }
            }
        } catch (Exception $e) {
            // If there have been any problems clear the dispatcher and rethrow
            $request->setModuleName('')->setControllerName('')->setActionName('');
            throw $e;
        }
    }
}
