<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Acl.php 1901 2010-12-06 13:25:08Z leigh $
 */

/**
 * Plugin to cache the entire page
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Plugin
 */
class Mcmr_Controller_Plugin_PageCache extends Zend_Controller_Plugin_Abstract
{
    public static $doNotCache = false;
    
    private $_cacheKey = null;
    
    /**
     * Check if the page is cached and use that if possible
     *
     * @param Zend_Controller_Request_Abstract $request 
     */
    public function dispatchLoopStartup(Zend_Controller_Request_Abstract $request)
    {
        // Do not cache authenticated users
        $user = Zend_Registry::get('authenticated-user');
        if (null !== $user) {
            if (null !== $user->getId()) {
                self::$doNotCache = true;
                return;
            }
        }
        
        // Do not cache POST data
        if (!$request->isGet()) {
            self::$doNotCache = true;
            return;
        }
        
        $cacheManager = Zend_Registry::get('cachemanager');
        $cache = $cacheManager->getCache('fullpage');
        
        if (null === $cache) {
            return;
        }

        $site = $request->getSiteName();
        $path = $request->getRequestUri();
        $device = $request->getDeviceClass();
        $this->_cacheKey = md5($site.$device.$path);
        
        $response = $cache->load($this->_cacheKey);
        

        // Let through 1 in 100 people.

        /**
         * in theory this doesn't seem like such a good idea as during higher server loads
         * more requests would get past the cache decreasing its efficiency
         *
         * this was probably added as a client request for more popular articles to be
         * refreshed from the cache more often... unfortunately counter-intuative
         * let it remain for posterity.
         **/
        
        /*$passThrough = rand(0, 100);
        if ((false !== $response) && (1 !== $passThrough)) {
            $response->sendResponse();
            SimpleStats::output();
            exit;
        }*/

        if ((false !== $response)) {
            $response->sendResponse();
            SimpleStats::output();
            exit;
        }

    }
    
    public function dispatchLoopShutdown()
    {
        // Do not cache if this is a redirect, POST, or there is no cache key
        if (self::$doNotCache
            || $this->getResponse()->isRedirect()
            || (null === $this->_cacheKey)
        ) {
            return;
        }
        
        $cacheManager = Zend_Registry::get('cachemanager');
        $cache = $cacheManager->getCache('fullpage');
        if (null !== $cache) {
            $cache->save($this->getResponse(), $this->_cacheKey);
        }
    }
}
