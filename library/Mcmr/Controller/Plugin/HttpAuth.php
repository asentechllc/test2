<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mcmr
 * @package    Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: HttpAuth.php 825 2010-06-16 18:04:48Z leigh $
 */

/**
 * A controller plugin that will authenticate a user via HTTP Authentication
 *
 * @category   Mcmr
 * @package    Mcmr_Controller
 * @subpackage Plugin
 */
class Mcmr_Controller_Plugin_HttpAuth extends Zend_Controller_Plugin_Abstract
{
    /**
     * Authenticate a user from HTTP Authentication values
     *
     * @param Zend_Controller_Request_Abstract $request 
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        // If we have HTTP authentication credentials process them
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
            $values = array(
                'username' => $_SERVER['PHP_AUTH_USER'],
                'password' => $_SERVER['PHP_AUTH_PW'],
            );

            $mapper = User_Model_User::getMapper();

            $result = $mapper->authenticate($values);
            if (!$result->isValid()) {
                // Authentication failed. Throw an error
                throw new Mcmr_Exception_PermissionDenied();
            }
        }
    }
}
