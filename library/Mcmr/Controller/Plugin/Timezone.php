<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Acl.php 1901 2010-12-06 13:25:08Z leigh $
 */

/**
 * Plugin to set the user's timezone
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Plugin
 */
class Mcmr_Controller_Plugin_Timezone extends Zend_Controller_Plugin_Abstract
{
    /**
     * Set the user timezone based on the authenticated user details
     *
     * @param Zend_Controller_Request_Abstract $request 
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request) 
    {
        $user = Zend_Registry::get('authenticated-user');
        if ((null !== $user) && (null !== $user->getId())) {
            $timezone = $user->getTimezone();
            if (!empty($timezone)) {
                Mcmr_Date::setDefaultTimezone($timezone);
            }
        }
    }
}
