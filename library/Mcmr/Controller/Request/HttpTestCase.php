<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Extends the standard http request object to add site names and keys
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Request
 */
class Mcmr_Controller_Request_HttpTestCase extends Zend_Controller_Request_HttpTestCase
{
    /**
     *
     * @var string
     */
    protected $_site = null;
    
    /**
     *
     * @var string
     */
    protected $_siteKey = 'site';
    
    /**
     * Retrieve the site name
     *
     * @return type 
     */
    public function getSiteName() 
    {
        if (null === $this->_site) {
            $this->_site = $this->getParam($this->getSiteKey());
            
            if (null === $this->_site) {
                $this->_site = 'default';
            }
        }
        
        return $this->_site;
    }

    /**
     * Set the site name
     *
     * @param type $site 
     */
    public function setSiteName($site) 
    {
        $this->_site = (string)$site;
    }

    /**
     * Set the site key
     *
     * @return type 
     */
    public function getSiteKey() 
    {
        return $this->_siteKey;
    }

    /**
     * Retrieve the site key
     *
     * @param type $siteKey 
     */
    public function setSiteKey($siteKey) 
    {
        $this->_siteKey = (string)$siteKey;
    }
    
}
