<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Extends the standard http request object to add site names and keys
 *
 * @category Mcmr
 * @package Mcmr_Controller
 * @subpackage Request
 */
class Mcmr_Controller_Request_Http extends Zend_Controller_Request_Http
{
    /**
     *
     * @var string
     */
    protected $_site = null;
    
    /**
     *
     * @var string
     */
    protected $_siteKey = 'site';


    protected $_deviceClass = null;
    
    /**
     * Retrieve the site name
     *
     * @return type 
     */
    public function getSiteName() 
    {
        if (null === $this->_site) {
            $this->_site = $this->getParam($this->getSiteKey());
            
            if (null === $this->_site) {
                $this->_site = 'default';
            }
        }
        
        return $this->_site;
    }

    /**
     * Set the site name
     *
     * @param type $site 
     */
    public function setSiteName($site) 
    {
        $this->_site = (string)$site;
    }

    /**
     * Set the site key
     *
     * @return type 
     */
    public function getSiteKey() 
    {
        return $this->_siteKey;
    }

    /**
     * Retrieve the site key
     *
     * @param type $siteKey 
     */
    public function setSiteKey($siteKey) 
    {
        $this->_siteKey = (string)$siteKey;
    }

    public function getDeviceClass() {
        if (null==$this->_deviceClass) {
            $this->_checkDeviceClass();
        }
        return $this->_deviceClass;
    }


    /**
     * Is originator of this request a mobile device?
     */
    public function getMobileRequest() 
    {
        $deviceClass = $this->getDeviceClass();
        return $deviceClass!='desktop';

    }

    /**
     * Is originator of this request a mobile device?
     */
    public function isMobileRequest()
    {
        return $this->getMobileRequest();
    }

    
    protected function _detectDeviceClass()
    {
        # include third-party detection library Mobile_Detect
        require_once APPLICATION_PATH.'/../library/ThirdParty/MobileDetect/Mobile_Detect.php';
        
        $detect = new Mobile_Detect();
        if ($detect->isTablet()) {
            return 'tablet';
        } elseif ($detect->isMobile()) {
            return 'mobile';
        } else {
            return 'desktop';
        }
    }

    protected function _checkDeviceClass()
    {
        $remember = Zend_Controller_Front::getInstance()->getParam('rememberDeviceClass');
        if ($remember) {
            $cookie = $this->getCookie('DeviceClass');
            if (null!==$cookie && 'mobile'!=$cookie && 'tablet'!=$cookie ) {
                $cookie = 'desktop';
            }
            $this->_deviceClass = $cookie;

        }
        if (!$this->_deviceClass) {
            $this->_deviceClass = $this->_detectDeviceClass();
        }
        if ($remember) {
            setcookie( 'DeviceClass', $this->_deviceClass );
        }
    }

    
}
