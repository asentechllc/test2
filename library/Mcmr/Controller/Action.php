<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2224 2011-02-24 11:54:37Z leigh $
 */

/**
 * IndexController for the News module. Looks after all article models.
 *
 * @category News
 * @package News_IndexController
 */
abstract class Mcmr_Controller_Action extends Zend_Controller_Action
{

    protected $_query;
    protected $_conditions;
    protected $_orders;
    protected $_page;
    protected $_mapper;
    protected $_models;
    protected $_model;
    protected $_form;
    protected $_values;
    protected $_type;

	public function isAdmin(){
		return Mcmr_StdLib::isAdmin();
	}

    public function init()
    {
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->setAutoJsonSerialization(false)
            ->addActionContext('index', 'json')
            ->addActionContext('index', 'html')
            ->addActionContext('create', 'json')
            ->addActionContext('create', 'html')
            ->addActionContext('read', 'json')
            ->addActionContext('read', 'html')
            ->addActionContext('update', 'json')
            ->addActionContext('update', 'html')
            ->addActionContext('delete', 'json')
            ->addActionContext('delete', 'html')
            ->initContext();
        $modelClassName = $this->getModelClassName();

        if(class_exists($modelClassName)) {
	        $this->_mapper = $modelClassName::getMapper();
        }

		# website only
		if(!$this->isAdmin()){

			$this->view->adSection = array('modelTags'=>array());

			if($title = $this->_getPageMeta('title'))
				$this->view->headTitle($title);

			if($desc = $this->_getPageMeta('desc'))
				$this->view->headMeta()->appendName('description', $desc);

			# Decide which ad slots to show on the page:
			if(is_object($config = Zend_Registry::get('default-config'))){
				if(is_object($config->adSection)){
					$urlParts = explode("/", str_replace("//", "/", $_SERVER['REQUEST_URI']));

					if($urlParts[1] == '')
						$urlParts[1] = '_home';

					# section based tags
					if(isset($config->adSection->_useSectionTags) && $config->adSection->_useSectionTags && preg_match('/^news-(section|article)-/',Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName())){
						$sectionSlug = explode('/',$this->_request->getPathInfo());
						$sectionSlug = $sectionSlug[1];
						$this->view->adSection['section'] = str_replace(' ','',ucwords(str_replace('-',' ',$sectionSlug)));
					}

					else if(isset($config->adSection->$urlParts[1]))
						$this->view->adSection['section'] = $config->adSection->$urlParts[1];
					else if(isset($config->adSection->_default))
						$this->view->adSection['section'] = $config->adSection->_default;

					if(isset($config->adSection->_flags)){
						$this->view->adSection['flags'] = array();
						foreach($config->adSection->_flags->toArray() as $k=>$v){
							$this->view->adSection['flags'][$k] = array_key_exists('default',$v) ? $v['default'] : false;
							if(isset($v['condition']) && in_array($this->view->adSection['section'],$v['condition'])){
								$this->view->adSection['flags'][$k] = isset($v['value']) ? $v['value'] : true;
							}
						}
					}

				}
			}
		}
    }

    public function batchAction(){
    	$ids = explode(',',$this->_request->getParam('ids'));
    	$from = urldecode($this->_request->getParam('from'));
    	$do = $this->_request->getParam('do');

    	# clean up IDs
    	foreach($ids as $k=>$v)
    		$ids[$k] = intval($v);

    	$query = Mcmr_Model_Query::factory(static::getModelName().'-index');
    	$query->addConditions(array(
    		'id' => array(
    			'condition' => 'in',
    			'value' => $ids
    		)
    	));

        $models = $this->_mapper->findAllByQuery($query);


		$this->view->getHelper('DisplayMessages')->addMessage('The selected items have been '.($do == 'remove' ? 'remov' : $do).'ed', 'info');

        foreach($models as $model){

	        switch($do){
	        	case 'unpublish':
	        	case 'publish':
	        		$model->$do();
	        		$this->_mapper->save($model);
	        		break;
	        	# should not have the option to batch remove -- hence commented out
	        	/*case 'remove':
	        		$this->_mapper->delete($model);*/
	        }
        }

		$this->_redirect($from);

    }

    /**
     * Delete a model on POST
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            if (null !== ($this->_model = $this->_mapper->find($id))) {
                $this->view->model = $this->_model;

                if ($this->_request->isPost()) {
                    try {
                        $this->_mapper->delete($this->_model);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Article Deleted', 'info');
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');;
                    }
                }
            }
        }

		$this->_redirect('/'.$this->_request->module);
    }


    public function indexAction()
    {
    	$this->_query = Mcmr_Model_Query::factory(static::getModelName().'-index');
        $this->_indexProcessRequest();
        $this->_indexBeforeFind();
        $this->_models = $this->_mapper->findAllByQuery($this->_query);
        $this->_indexAfterFind();
        $this->view->query = $this->_query;
        $this->view->models = $this->_models;
    }

    protected function _indexProcessRequest()
    {
        $this->_conditions = array();
        $this->_processIndexConditions($this->_conditions);
        $this->_query->addConditions($this->_conditions);
        $this->_orders = array();
        $this->_processIndexOrders($this->_orders);
        $this->_query->addOrders($this->_orders);
        if (null!==($page=$this->_request->getParam('page'))) {
            $this->_query->offsetPage($this->_getParam('page'));
        }
        if (null!==($count=$this->_request->getParam('count'))) {
            $this->_query->setCount($count);
        }
    }

    protected function _processIndexConditions(&$conditions)
    {
        $this->_setTitleCondition($conditions);
        $this->_setPublishedCondition($conditions);
        $this->_setTypeCondition($conditions);
    }

    protected function _setTitleCondition(&$conditions)
    {
        if ($ltitle=$this->_request->getParam('ltitle')) {
            $conditions['title'] = array('condition'=>'LIKE', 'value'=>"%{$ltitle}%");
        } elseif ($title=$this->_request->getParam('title')) {
            if (false === strpos($title, '%')) {
                $conditions['title'] = $title;
            } else {
                $conditions['title'] = array('condition'=>'LIKE', 'value'=>$title);
            }
        }
    }

    protected function _setPublishedCondition(&$conditions)
    {
        $published = $this->_request->getParam('published');
        if (null!==$published&&''!==$published) {
            $conditions['published'] = ($published=='1');
        }
    }

    protected function _setTypeCondition(&$conditions)
    {
        if (null!==($this->_type=$this->_request->getParam('type'))) {
            if (!is_array($this->_type)) {
                $this->_type = explode(',', $this->_type);
            }
            if (1==count($this->_type)) {
                $conditions['type'] = array(
                    'condition'=>'url_equals',
                    'value' => array('url'=>$this->_type[0], 'model'=>static::getModelTypeClassName())
                );
            } else {
                $conditions['type'] = array(
                    'condition'=>'url_in',
                    'value' => array('url'=>$this->_type, 'model'=>static::getModelTypeClassName())
                );
            }
        }
    }


    public function _setRequestParams(&$conditions, $params)
    {
        foreach ($params as $name) {
            $value=$this->_request->getParam($name);
            if (null!==$value && ''!==$value) {
                $conditions[$name] = $value;
            }
        }
    }

    protected function _processIndexOrders(&$orders)
    {
        if (null!==($order=$this->_request->getParam('order'))) {
            $parts = explode(',', $order);
            if (count($parts)>1) {
                $orders[$parts[0]]=$parts[1];
            } else {
                $orders[$parts[0]]='asc';
            }
        }
    }

    protected function _indexBeforeFind()
    {
    }

    protected function _indexAfterFind()
    {
    }


    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $seoId = $this->_request->getParam('seoId', null);
        $url = $this->_request->getParam('url', null);

        $parentUrl = $this->_request->getParam('parentUrl', null);
        $sectionUrl = $this->_request->getParam('sectionUrl', null);
        
        if (null !== $id || null !== $url || null !== $seoId) {
            $this->_readBeforeFind();

            // Get the article from the IDs or the URL provided
            if($id){
            	$this->_model = $this->_mapper->find($id);
            }
            elseif($seoId){
            	$id = substr($seoId,1) - 99;
            	$this->_model = $this->_mapper->find($id);

				# redirect to correct section URL if incorrect URL used
				if($parentUrl && method_exists($this->_model,'getSectionUrl')){
					$requestSectionUrl = $sectionUrl ? $parentUrl.'/'.$sectionUrl : $parentUrl;
					$modelSectionUrl = $this->_model->getSectionUrl();
					$connected = $this->_model->getConnectedModel();
					
					# no section? do we have a connected model?
					if(!$modelSectionUrl && $connected){
						$this->_redirect(
							$this->view->defaultReadUrl($connected),
							array('code'=>301)
						);
					}
					
					if($requestSectionUrl != $modelSectionUrl){
						$this->_redirect(
							str_replace($requestSectionUrl,$modelSectionUrl,$_SERVER['REQUEST_URI']),
							array('code'=>301)
						);
    	        	}
    	        }
    	        
            	// make 301 redirect if URL invalid
            	if($this->_model->url != $url){
           			$this->_redirect(
           				str_replace($url,$this->_model->url,$_SERVER['REQUEST_URI']),
           				array('code'=>301)
           			);
            	}
            }
            else{
	            $this->_model = $this->_mapper->findOneByField(array('url'=>$url));
	        }

            if (null !== $this->_model) {

				# record a POSTed view stat
				if($this->_request->getParam('_stats') && $this->_request->isPost()){
					Mcmr_Stats::read($this->_model);
					exit('OK');
				}

				# INT107-615 canonical header
				$cfg = Zend_Registry::get('default-config');
				if(isset($cfg->seo->cannonicalTag->routes)){
					$matchedRoute = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
					$defaultType = isset($cfg->seo->cannonicalTag->type) ? $cfg->seo->cannonicalTag->type : 'meta';
					$routes = $cfg->seo->cannonicalTag->routes->toArray();
					foreach($routes as $route){

						$type = $defaultType;
						if(is_array($route)){
	
							if(isset($route['type']))
								$type = $route['type'];
								
							if(isset($route['route']))
								$route = $route['route'];
							else continue;
						}
						
						if(preg_match('/'.$route.'/',$matchedRoute)){
							if($type == 'header')
								header('Link: <'.$this->view->serverUrl().$this->view->defaultReadUrl($this->_model).'>;rel="canonical"');
							else
								$this->view->headLink(array('rel' => 'canonical','href' => $this->view->serverUrl().$this->view->defaultReadUrl($this->_model)));
						}
					}
			    }

                $this->_readAfterFind();
                $this->view->model = $this->_model;

                if(isset($this->view->adSection) && method_exists($this->_model,'getCategorytitles'))
	                $this->view->adSection['modelTags'] = explode(',',strtolower($this->_model->getCategorytitles()));

            } else {
                throw new Mcmr_Exception_PageNotFound('Not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    protected function _readBeforeFind()
    {
    }

    protected function _readAfterFind()
    {
    }


    public function updateAction()
    {
        $id = $this->_request->getParam('id', null);
        if (!$id) {
            throw new Mcmr_Exception_PageNotFound('Model Not Found');
        }
        $this->_updateBeforeFind();
        $this->_model = $this->_mapper->find($id);
        $this->view->model = $this->_model;

        if (!$this->_model) {
            throw new Mcmr_Exception_PageNotFound('Model Not Found');
        }
        $this->_updateAfterFind();
        $this->_form = $this->_loadForm();
        $this->view->form = $this->_form;

        $this->_values = $this->_model->getOptions(true);
        if ($this->_request->isPost()) {
            // Merge the existing news data with the form values. This allows for partial updates.
            $post = $this->_request->getPost();
            $formName = $this->_form->getName();
            $post[$formName] = array_merge($this->_values, $post[$formName]);

            if ($this->_form->isValid($post)) {
                $values = $this->_form->getValues(true);
                $this->_model->setOptions($values);
                $this->_updateBeforeSave();

                try {
                    $this->_mapper->save($this->_model);
                    $this->view->error = false;

                    $modelMsgKey = 'model'.array_pop(explode('_',get_class($this->_model))).'Saved';

                    $this->view->getHelper('DisplayMessages')->addMessage(array($modelMsgKey,'modelSaved'), 'info');
                    $this->view->getHelper('Redirect')->notify('update', $this->_model);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('incorrectFormValues', 'warn');
            }
        } else {
            $this->_updateNotPost();
            $this->_form->populate($this->_values);
        }
        $this->_updateBeforeView();

    }


    protected function _updateBeforeFind()
    {
    }

    protected function _updateAfterFind()
    {
    }

    protected function _updateNotPost()
    {
    }

    protected function _updateBeforeSave()
    {
    }

    protected function _updateBeforeView()
    {
    }

    protected function _loadForm()
    {
        $name = ucfirst(static::getModelName());
        $options = $this->_getFormOptions();
        $form = $this->_helper->loadForm($name, $options);
        return $form;
    }

    protected function _getFormOptions()
    {
        $options = array();
        if (method_exists(static::getModelClassName(), 'getType')) {
            if ($this->_model) {
                $type = $this->_model->getType();
                if ($type && is_object($type) && method_exists($type, 'getUrl')) {
                    $options['type']=$type->getUrl();
                }
            } else {
                if (null!==($typeName = $this->_request->getParam('type'))) {
                    $options['type']=$typeName;
                }
            }
        }
        return $options;
    }

    /**
     * This probably needs a but more customiseability, at least with regards to messaging
     */
    public function orderAction()
    {
        $this->_type = $this->_request->getParam('type', self::getModuleConfig()->order->default);
        $form = $this->_helper->loadForm( static::getOrderFormName(), array('type'=>$this->_type ));
        $this->view->form = $form;
        if ($this->_request->isPost()) {
            $this->_orderBeforeOrder();
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                // Reset the ordering before resaving the order
                $this->_mapper->resetOrdering($this->_type);
                if ('chronological' == $values['orderby']) {
                    $this->view->getHelper('DisplayMessages')->addMessage('Ordering Reset to Chronological', 'info');
                } else {
                    foreach ($values['models'] as $order=>$id) {
                        $model = $this->_mapper->find($id);
                        if (null !== $model) {
                            $model->setOrder(1000-$order, $this->_type); // Done in reverse to accomodate null values
                            $this->_mapper->save($model);
                        }
                    }
                    $this->view->getHelper('DisplayMessages')->addMessage('Ordering set to specific', 'info');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Form values were invalid', 'warn');
            }
            $this->_orderAfterOrder();
        }
        $this->_orderBeforeView();
    }

    protected function _orderBeforeOrder()
    {
    }

    protected function _orderAfterOrder()
    {
    }

    protected function _orderBeforeView()
    {
    }

    /**
     * This assumes that controller will implement CRUD operations for a single model. We need to know the model;
     * Default implementation assumes model name is the same as controller name, except when controller name is index.
     * Then model name is the same as module name.
     * Only needs to be overriden in modules where model name handled by index controller is different to module name,
     * like in news, where index controller handles article model.
     */
    public static function getModelName()
    {
        $calledClassParts = explode('_', get_called_class());
        $lastPart = array_pop($calledClassParts);
        $name = strtolower(substr($lastPart, 0, strlen($lastPart)-10));
        if ('index'==$name) {
            return strtolower($calledClassParts[0]);
        }
        return $name;
    }

    public static function getModuleName()
    {
        $calledClassParts = explode('_', get_called_class());
        $name = strtolower($calledClassParts[0]);
        return $name;
    }

    public static function getModelClassName($shortName=null)
    {
        if (null===$shortName) {
            $shortName = static::getModelName();
        }
        $calledClassParts = explode('_', get_called_class());
        return $calledClassParts[0]."_Model_".ucfirst($shortName);
    }

    public static function getModelTypeClassName()
    {
        $calledClassParts = explode('_', get_called_class());
        return $calledClassParts[0]."_Model_Type";
    }

    public static function getControllerClassName($controller, $module)
    {
        return ucfirst($module).'_'.ucfirst($controller).'Controller';
    }

    public static function getModuleConfig()
    {
        $name = static::getModuleName();
        $config = Zend_Registry::get($name.'-config');
        return $config;
    }

    protected static function getOrderFormName()
    {
        return 'Order';
    }

    protected function _getPageMeta($key,$url=null){

    	if(empty($url))
	    	$url = $this->_request->getPathInfo();

    	$urlBits = explode('/',substr($url,1));

		# use base URL for complex read URLs
    	if($readIndex = strpos($url,'/read/'))
    		$url = substr($url,0,$readIndex);

    	$url = explode('-',preg_replace('/[^0-9a-z\-]+/i','',$url));

    	if(count($url) == 1 && !$url[0])
    		$url[0] = 'home';

    	for($i = 1; $i < count($url); $i++)
    		$url[$i] = strtoupper($url[$i][0]).substr($url[$i],1);

    	$settingKey = implode('',$url).ucfirst($key);
    	$value = Mcmr_Settings::get('meta-data',$settingKey);

    	# if nothing, try parent URL
    	if(empty($value) && count($urlBits) > 1)
    		return $this->_getPageMeta($key,'/'.implode('/',array_slice($urlBits,0,-1)));

		return $value;
    }

}