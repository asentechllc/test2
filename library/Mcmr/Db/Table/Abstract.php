<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mcmr
 * @package    Mcmr_Db
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Profiler.php 840 2010-06-17 13:50:49Z leigh $
 */

/**
 * Extends the Zend Db table class to add read/write splitting on the database
 *
 * @category   Mcmr
 * @package    Mcmr_Db
 * @subpackage Table
 */
abstract class Mcmr_Db_Table_Abstract extends Zend_Db_Table_Abstract
{
    /**
     * The default adapter to use for all write queries
     *
     * @var Zend_Db_Adapter_Interface 
     */
    protected static $_defaultWriteDb = null;
    
    /**
     * The default adapter to use for all read queries
     *
     * @var Zend_Db_Adapter_Interface 
     */
    protected static $_defaultReadDb = null;
    
    /**
     *
     * @var Zend_Db_Adapter_Interface 
     */
    protected $_writeDb = null;
    
    /**
     *
     * @var Zend_Db_Adapter_Interface 
     */
    protected $_readDb = null;
    
    /**
     * Flag to switch the read adapter to the write adapter on a database write
     *
     * @var boolean
     */
    protected $_switchOnWrite = true;
    
    /**
     * Flag to determine whether the adapter has been switched
     *
     * @var boolean
     */
    private $_adapterSwitched = false;
    
    /**
     *
     * @param mixed $db Either an Adapter object, or a string naming a Registry key
     * @param boolean $setDefault Set this adapter as the general default adapter.
     */
    public static function setDefaultWriteAdapter($db, $setDefault = true)
    {
        self::$_defaultWriteDb = self::_setupAdapter($db);
        
        // Set the default adapter to be the write adapter.
        if ($setDefault && (null !== $db)) {
            self::setDefaultAdapter($db);
        }
    }
    
    /**
     *
     * @param mixed $db Either an Adapter object, or a string naming a Registry key
     * @param boolean $setDefault Set this adapter as the general default adapter.
     */
    public static function setDefaultReadAdapter($db, $setDefault = false)
    {
        self::$_defaultReadDb = self::_setupAdapter($db);
        
        // Set the default adapter to be the write adapter.
        if ($setDefault && (null !== $db)) {
            self::setDefaultAdapter($db);
        }
    }
    
    /**
     *
     * @return Zend_Db_Adapter_Abstract or null 
     */
    public static function getDefaultWriteAdapter()
    {
        return self::$_defaultWriteDb;
    }
    
    /**
     *
     * @return Zend_Db_Adapter_Abstract or null 
     */
    public static function getDefaultReadAdapter()
    {
        return self::$_defaultReadDb;
    }
    
    /**
     * Return the adapter to be used for all DB write commands
     *
     * @param boolean $switch Switch the adapter to the write adapter for all queries
     * @return Zend_Db_Adapter_Abstract 
     */
    public function getWriteAdapter($switch = false)
    {
        if ($this->getSwitchOnWrite()) {
            $this->_adapterSwitched = true;
        }
        
        if (null !== $this->_writeDb) {
            return $this->_writeDb;
        } else {
            return $this->_db;
        }
    }
    
    /**
     * Return the adapter to be used for all DB read commands
     *
     * @return Zend_Db_Adapter_Abstract 
     */
    public function getReadAdapter()
    {
        if (null !== $this->_readDb && !$this->_adapterSwitched) {
            return $this->_readDb;
        } elseif (null !== $this->_writeDb) {
            return $this->_writeDb;
        } else {
            return $this->_db;
        }
    }
    
    /**
     * Returns the switch on write flag that determins whether the read DB adapter should
     * be switched to the write adapter after a DB write has been performed
     *
     * @return boolean
     */
    public function getSwitchOnWrite() 
    {
        return $this->_switchOnWrite;
    }

    /**
     * Set the switch on write flag.
     *
     * @param type $switchOnWrite
     * @return Mcmr_Db_Table 
     */
    public function setSwitchOnWrite($switchOnWrite) 
    {
        $this->_switchOnWrite = $switchOnWrite;
        
        return $this;
    }

    /**
     * Inserts a new row.
     *
     * @param  array  $data  Column-value pairs.
     * @return mixed         The primary key of the row inserted.
     */
    public function insert(array $data)
    {
        // Get the write adapter.
        $db = $this->_db;
        $this->_db = $this->getWriteAdapter(true);
        
        $return = parent::insert($data);
        
        // Restore the original adapter.
        $this->_db = $db;
        
        return $return;
    }
    
	/**
	 * SQLSTATE[23000]: Integrity contraint violation
	 * Duplicate key, this occurs when more than one save occurs at the same time
	 * No transactions or table locking used so race condition occurs
	 * Process losing race condition causes an exception
	 * Simulate INSERT IGNORE by supressing this exception and allowing others through
	 **/
    public function insertIgnore(array $data)
	{
		try{
			self::insert($data);
		}
		catch(Exception $e){

			# Pass it on if error code not 23000
			if($e->getCode() != 23000){
				throw new Exception($e->getMessage(),$e->getCode());
			}
			
		}
	}
	
    /**
     * Updates existing rows.
     *
     * @param  array        $data  Column-value pairs.
     * @param  array|string $where An SQL WHERE clause, or an array of SQL WHERE clauses.
     * @return int          The number of rows updated.
     */
    public function update(array $data, $where)
    {
        // Get the write adapter.
        $db = $this->_db;
        $this->_db = $this->getWriteAdapter(true);
        
        $return = parent::update($data, $where);
        
        // Restore the original adapter.
        $this->_db = $db;
        
        return $return;
    }
    
    /**
     * Deletes existing rows.
     *
     * @param  array|string $where SQL WHERE clause(s).
     * @return int          The number of rows deleted.
     */
    public function delete($where)
    {
        // Get the write adapter.
        $db = $this->_db;
        $this->_db = $this->getWriteAdapter(true);
        
        $return = parent::delete($where);
        
        // Restore the original adapter.
        $this->_db = $db;
        
        return $return;
    }
    
    /**
     * Returns an instance of a Zend_Db_Table_Select object.
     *
     * @param bool $withFromPart Whether or not to include the from part of the select based on the table
     * @return Zend_Db_Table_Select
     */
    public function select($withFromPart = self::SELECT_WITHOUT_FROM_PART)
    {
        // Get the write adapter.
        $db = $this->_db;
        $this->_db = $this->getReadAdapter();
        
        $return = parent::select($withFromPart);
        
        // Restore the original adapter.
        $this->_db = $db;
        
        return $return;
    }
    
    /**
     * Support method for fetching rows.
     *
     * @param  Zend_Db_Table_Select $select  query options.
     * @return array An array containing the row results in FETCH_ASSOC mode.
     */
    protected function _fetch(Zend_Db_Table_Select $select)
    {
        // Get the write adapter.
        $db = $this->_db;
        $this->_db = $this->getReadAdapter();
        
        $return = parent::_fetch($select);
        
        // Restore the original adapter.
        $this->_db = $db;
        
        return $return;
    }
    
    /**
     * Initialize database adapters.
     *
     * @return void
     * @throws Zend_Db_Table_Exception
     */
    protected function _setupDatabaseAdapter()
    {
        parent::_setupDatabaseAdapter();
        
        if (! $this->_writeDb) {
            $this->_writeDb = self::getDefaultWriteAdapter();
        }
        
        if (! $this->_readDb) {
            $this->_readDb = self::getDefaultReadAdapter();
        }
    }     
}
