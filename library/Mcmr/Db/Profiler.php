<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mcmr
 * @package    Mcmr_Db
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Profiler.php 840 2010-06-17 13:50:49Z leigh $
 */

/**
 * A lightweight profiler that just stores most recent query. Used for error handling
 * and debugging in a production environment where the full profiler is overkill and
 * too resource intensive
 *
 * @category   Mcmr
 * @package    Mcmr_Db
 * @subpackage Profiler
 */
class Mcmr_Db_Profiler extends Zend_Db_Profiler
{
    protected $_queryText = null;
    protected $_queryType = null;

    public function queryStart($queryText, $queryType=null)
    {
        $this->_queryText = $queryText;
        $this->_queryType = $queryType;

        return null;
    }

    public function queryEnd($queryId)
    {
        return null;
    }

    public function getQueryProfile($queryId)
    {
        return null;
    }

    public function getLastQueryProfile()
    {
        $queryId = parent::queryStart($this->_queryText, $this->_queryType);

        return parent::getLastQueryProfile();
    }
}
