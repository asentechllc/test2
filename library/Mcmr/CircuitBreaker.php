<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CircuitBreaker.php 2277 2011-03-25 15:57:35Z leigh $
 */

/**
 * A Circuit Breaker pattern implementation
 * 
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 */
class Mcmr_CircuitBreaker
{
    protected static $_defaultStore = null;
    protected static $_defaultLogger = null;

    private $_appid = null;
    private $_state = null;
    private $_failures = 0;
    private $_threshold = 10;
    private $_timeout = 10;
    private $_trippedtime = 0;
    
    private $_store = null;
    private $_logger = null;

    public function __construct($config = array())
    {
        if ($config instanceof Zend_Config) {
            $config = $config->toArray();
        }

        if (isset($config['appid'])) {
            $this->setAppid($config['appid']);
        }
        if (isset($config['threshold'])) {
            $this->setThreshold($config['threshold']);
        }
    }

    public static function setDefaultStore(Mcmr_CircuitBreaker_Store_StoreAbstract $store)
    {
        self::$_defaultStore = $store;
    }

    public static function setDefaultLogger(Zend_Log $logger)
    {
        self::$_defaultLogger = $logger;
    }

    /**
     * Set the CB storage object. Used to make the CB state persistent
     *
     * @param Mcmr_CircuitBreaker_Store_StoreAbstract $store
     * @return Mcmr_CircuitBreaker
     */
    public function setStore(Mcmr_CircuitBreaker_Store_StoreAbstract $store)
    {
        $this->_store = $store;

        // Load the circuit breaker
        $this->_store->load();
        $this->setState($this->_store->getState());
        $this->_failures = $this->_store->getFailures();
        $this->_timeout = $this->_store->getTimeout();
        $this->_trippedtime = $this->_store->getTrippedtime();
        
        return $this;
    }

    /**
     * Get the storage object. Used to make the CB state persistent
     *
     * @return Mcmr_CircuitBreaker_Store_StoreAbstract
     */
    public function getStore()
    {
        if (null === $this->_store) {
            $this->setStore(self::$_defaultStore);
        }

        return $this->_store;
    }

    /**
     * Set the log object. Used tp log state changes and errors
     *
     * @param Zend_Log $logger
     * @return Mcmr_CircuitBreaker
     */
    public function setLogger(Zend_Log $logger)
    {
        $this->_logger = $logger;

        return $this;
    }

    /**
     * Get the log object
     *
     * @return Zend_Log
     */
    public function getLogger()
    {
        if (null === $this->_logger) {
            $this->_logger = self::$_defaultLogger;
        }

        return $this->_logger;
    }

    /**
     * Set the threshold limit. Number of failures allowed before opening the circuit
     *
     * @param int $threshold
     * @return Mcmr_CircuitBreaker
     */
    public function setThreshold($threshold)
    {
        $this->_threshold = (int)$threshold;

        return $this;
    }

    /**
     * Get the threshold limit. Number of failures allowed before opening the circuit
     *
     * @return int
     */
    public function getThreshold()
    {
        if (null === $this->_threshold) {
            $this->_threshold = 10;
        }

        return $this->_threshold;
    }

    /**
     * Set the ID used to identify this circuitbreaker
     *
     * @param string $appid
     * @return Mcmr_CircuitBreaker
     */
    public function setAppid($appid)
    {
        $this->_appid = $appid;

        return $this;
    }

    /**
     * Get the ID used to identify this circuit breaker
     *
     * @return string
     */
    public function getAppid()
    {
        return $this->_appid;
    }

    /**
     * Set the state of this circuit
     *
     * @param string $state
     */
    public function setState($state)
    {
        // Set the time of state change. If state is null then this CB is being loaded and this should not be set here.
        if (null !== $this->_state) {
            $this->_trippedtime = time();
            $this->_state = Mcmr_CircuitBreaker_State_StateAbstract::getInstance($this, $state);
            $this->save();
            
            // Log the state change
            $logger = $this->getLogger();
            if (null !== $logger) {
                $logger->log($state);
            }
        } else {
            $this->_state = Mcmr_CircuitBreaker_State_StateAbstract::getInstance($this, $state);
        }
    }

    /**
     * Check if the error threshold has been reached
     *
     * @return bool
     */
    public function thresholdReached()
    {
        return ($this->_failures >= $this->getThreshold());
    }

    /**
     * Increment the number of failures
     */
    public function incrementFailures()
    {
        $this->_failures++;
        
        $this->save();
    }

    /**
     * Reset the number of failures
     */
    public function resetFailures()
    {
        if ($this->_failures > 0) {
            $this->_failures = 0;
            $this->save();
        }
    }

    public function getTimeout()
    {
        return $this->_timeout;
    }

    /**
     * Get the time in unix time the circuit was tripped
     *
     * @return int
     */
    public function getTrippedtime()
    {
        return $this->_trippedtime;
    }
    
    /**
     * Save the circuit breaker to the store.
     * 
     * @return null
     */
    public function save()
    {
        $store = $this->getStore();
        if (null !== $store) {
            $store->setState($this->_state->getName());
            $store->setFailures($this->_failures);
            $store->setTimeout($this->_timeout);
            $store->setTrippedtime($this->_trippedtime);

            $store->save($this->_appid);
        } else {
            throw new Mcmr_CircuitBreaker_Exception("Cannot save circuit breaker state. Store is not set");
        }
    }

    /**
     * Execute the protected function.
     * 
     * @param $function The function to be executed
     * @return $function return value
     */
    public function execute()
    {
        // Alert the state that we are about to call the code
        $this->_state->onCalling();
        
        try {
            // Get the function and the arguments.
            $args = func_get_args();
            $function = array_shift($args);
            $returnval = call_user_func_array($function, $args);
        } catch (Exception $e) {
            // Alert the state that we caught an exception then rethrow
            $this->_state->onException();
            
            throw $e;
        }
        
        // Alert the state that the code was called successfully
        $this->_state->onCalled();
        
        return $returnval;
    }
}
