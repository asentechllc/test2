<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: GenericMapper.php 2407 2011-05-10 08:52:37Z michal $
 */

/**
 * Description of Mcmr_Model_GenericMapper
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage GenericMapper
 */
abstract class Mcmr_Model_GenericMapper extends Mcmr_Model_MapperAbstract
{

    public function __construct()
    {
        parent::__construct();
        $this->registerObserver('Stats');
        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
        $this->registerObserver('Follow');
    }

    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {

        // auto-generate url from title
        if (method_exists($model,'getUrl') && method_exists($model,'getTitle')){
        	$currentUrl = $model->getUrl();
        	$newUrl = Mcmr_StdLib::urlize($model->getTitle());
        	if($currentUrl != $newUrl)
        		$model->setUrl($newUrl);
        }

        $options = $model->getOptions();
        if ($model instanceof Mcmr_Model_Ordered) {
            unset($options['order']);
        }
        $data = array();
        foreach ($options as $field => $value) {
            $field = $this->_sanitiseField($field);
            $value = $this->_sanitiseValue($field, $value);
            $data[$field] = $value;
        }
        $data = $this->_sanitiseValues($data);

        if (Mcmr_Model_ModelAbstract::STATE_INITIALISED === $model->state()
                || Mcmr_Model_ModelAbstract::STATE_VIRGIN === $model->state()) {
            $this->notify('preInsert', $model);

            // Perform an insert
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);

            if (null !== $this->_dbAttrTableClass) {
                $this->_saveAttributes($model);
            }
            if (null !== $this->_dbOrdrTableClass) {
                $this->_saveOrder($model);
            }
            $this->_postSave($model);

            $model->state(Mcmr_Model_ModelAbstract::STATE_CLEAN);
            // Notify Observers
            $this->notify('insert', $model);
        } else {
            $id = $model->getId();
            if (!is_array($id)) {
                $id = array('id' => $id);
            }
            $this->notify('preUpdate', $this->findOneByField($id));
            // Perform an update
            $key = $this->_keyCondition($this->getDbTable(), $model->getId());
            $this->getDbTable()->update($data, $key);
            if (null !== $this->_dbAttrTableClass) {
                $this->_deleteAttributes($key);
                $this->_saveAttributes($model);
            }
            if (null !== $this->_dbOrdrTableClass) {
                $this->_saveOrder($model);
            }
            $this->_postSave($model);

            $model->state(Mcmr_Model_ModelAbstract::STATE_CLEAN);

            // Notify observers
            $this->notify('update', $model);

        }
        return $this;
    }

    /**
     * Delete the model information from the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        $this->getDbTable()->delete($this->_keyCondition($this->getDbTable(), $model->getId()));

        if (null !== $this->_dbAttrTableClass) {
            $this->_deleteAttributes($this->_keyCondition($this->getDbTable(), $model->getId()));
        }

        // Notify observers
        $this->notify('delete', $model);

        return $this;
    }

    /**
     * Find a model based on the condition
     *
     * @param array $conditions
     * @return Mcmr_Model_ModelAbstract
     */
    public function findOneByField($conditions = null, $order = null)
    {
        $cacheid = $this->_cacheIdPrefix . 'findOneByField' . $this->_getCacheId($conditions, $order);
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($model = $cache->load($cacheid))) {
            $this->_resetMapper();
            $table = $this->getDbTable();
            $select = $table->select();
            $select->from($table, $this->_selectFields($select));
            if (null !== $conditions) {
                foreach ($conditions as $field => $value) {
                    $this->_processSingleCondition($select, $field, $value);
                }
            }
            $select = $this->_postOneConditions($select);

            if (!empty($order)) {
                foreach ($order as $field => $direction) {
                    $select = $this->_addOrderCondition($select, $field, $direction);
                }
            }
            $select = $this->_postOneOrder($select);
            $row = $table->fetchRow($select);
            $model = $this->_convertRowToModel($row);
            if (is_object($row)) {
                $cache->save($model, $cacheid, array(strtolower($this->_modelClass) . '_id_' . $model->getId()));
            } else {
                $cache->save($model, $cacheid);
            }

            $this->notify('cacheMiss', $model);
        }

        // Notify observers
        if (null !== $model) {
            $this->notify('load', $model);
        }

        return $model;
    }

    /**
     * Get a paginator object based on the condition
     *
     * @param array|null $conditions
     * @param array|null $order
     * @param array|int $page
     * @return Zend_Paginator
     */
    public function findAllByField($conditions = null, $order=null, $page = null)
    {
        $intialCountOffset = false;
        if (is_array($page) && isset($page['count']) && isset($page['page'])) {
            $countPerPage = $page['count'];
            $page = $page['page'];
        } else {
            $page = 1;
            $countPerPage = PHP_INT_MAX;
        }

        $cachecondition = $conditions;
        $cachecondition['page'] = $page;
        //count per page should be in the cache condition too,
        //otherwise different view overwriting the default count would get wrong results
        $cachecondition['countPerPage'] = $countPerPage;
        $cacheid = $this->_cacheIdPrefix . 'findAllByField' . $this->_getCacheId($cachecondition, $order);
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        if (false == ($paginator = $cache->load($cacheid))) {
            $this->_resetMapper();
            $table = $this->getDbTable();
            $select = $table->select();
            $select->from($table, $this->_selectFields($select));
            if (!empty($conditions)) {
                foreach ($conditions as $field => $value) {
                    $this->_processSingleCondition($select, $field, $value);
                }
            };
            $select = $this->_postAllConditions($select);

            if (!empty($order)) {
                foreach ($order as $field => $direction) {
                    $select = $this->_addOrderCondition($select, $field, $direction);
                }
            }
            $select = $this->_postAllOrder($select);
            $paginator = new Mcmr_Paginator(new Zend_Paginator_Adapter_DbSelect($select), $intialCountOffset);
            $paginator->setItemCountPerPage($countPerPage);
            $paginator->setCurrentPageNumber($page);
            $paginator->setFilter(new Mcmr_Filter_Callback(array($this, 'filter')));
            //$paginator->getCurrentItems(); // Force the items to be retrieved from the database now for the cache
            $indexTag = strtolower($this->_modelClass).'_index';
            $cache->save($paginator, $cacheid, array($indexTag), $this->_maxCacheLifetime($cache, $conditions));

            $this->notify('cacheMiss', $paginator);
        }

        // Notify observers of a paginator load
        $this->notify('load', $paginator);

        return $paginator;
    }

    protected function _processSingleCondition($select, $field, $value)
    {
        $fieldName = $this->_sanitiseField($field);
        if (false !== $fieldName && null !== $value) {
            if (0 === strncmp('attr_', $field, 5)) {
                $fieldName = str_replace('attr_', '', $field);
                $key = $this->getDbTable()->info(Zend_Db_Table::PRIMARY);
                if (is_array($value)) {
                    $condition = $value['condition'];
                    $value = $value['value'];
                } else {
                    $condition = '=';
                }
                $this->_addAttributeCondition(
                    $select, $this->getDbTable()->info(Zend_Db_Table::NAME),
                    $fieldName, $key[1], $value, $condition
                );
            } else {
                $select = $this->_addCondition($select, $fieldName, $value);
            }
        }
    }

    public function findOneByQuery(Mcmr_Model_Query $query)
    {
        $conditions = $query->getProcessedConditions();
        $orders = $query->getProcessedOrders();
        $result = $this->findOneByField($conditions, $orders);
        return $result;
    }

    public function findAllByQuery(Mcmr_Model_Query $query)
    {
        $conditions = $query->getProcessedConditions();
        $orders = $query->getProcessedOrders();
        $page = $query->getProcessedPage();
        $result = $this->findAllByField($conditions, $orders, $page);
        return $result;
    }

    public function countByQuery(Mcmr_Model_Query $query)
    {
        $conditions = $query->getProcessedConditions();
        $result = $this->countByField($conditions);
        return $result;
    }


    /**
     * Return a count of models based on the conditions
     *
     * @param array $condition
     * @return int
     */
    public function countByField($condition)
    {
        $cachecondition = $condition;
        $cacheid = $this->_cacheIdPrefix . 'countByField' . $this->_getCacheId($cachecondition);
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        if (false == ($res = $cache->load($cacheid))) {
            $this->_resetMapper();
            $table = $this->getDbTable();
            $select = $table->select();
            $select->from($table, array('total' => 'count(*)'));
            if (!empty($condition)) {
                foreach ($condition as $field => $value) {
                    $this->_processSingleCondition($select, $field, $value);
                }
            }
            $select = $this->_postAllOrder($select);
            $stmt = $select->query();
            $res = $stmt->fetch();
            $cache->save($res, $cacheid, array(strtolower($this->_modelClass) . '_index'));
            $this->notify('cacheMiss', $res);
        }

        // Notify observers of a paginator load
        $this->notify('load', $res);

        return $res['total'];
    }

    /**
     * Return a count of models based on the conditions.
     * An alias for count()
     *
     * @param array $condition
     * @return int
     */
    public function count(array $condition = null)
    {
        return $this->countByField($condition);
    }

    /**
     * Takes Zend_Db_Table rows and maps them to the model.
     *
     * @param array $rows
     * @return array
     */
    public function filter($rows)
    {
        $models = array();
        foreach ($rows as $row) {
            // Use the mapper find. This is more taxing on first load. But afterwards every model will be cached.
            // This prevents a full DB hit if a single model is updated.
            $model = $this->find($row[$this->_columnPrefix . 'id']);

            $models[] = $model;
        }

        return $models;
    }

    /**
     * Converts a row returned by database query to an instance of model class.
     * @param Zend_Db_Table_Row_Abstract $row
     * @param instance of the model class
     */
    protected function _convertRowToModel($row)
    {
        if (is_object($row) && $row instanceof Zend_Db_Table_Row) {
            $row = $row->toArray();
        }

        if (is_array($row)) {
            $model = new $this->_modelClass();
            $model->setOptions($this->_stripColumnPrefix($row, $this->_columnPrefix));

            if (null !== $this->_dbAttrTableClass) {
                $this->_loadAttributes($model);
            }

            $model->state(Mcmr_Model_ModelAbstract::STATE_CLEAN);
        } else {
            $model = null;
        }

        return $model;
    }

//_convertRowToModel

    /**
     * Convert a readable fieldname into the database fieldname
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        return $this->_columnPrefix . $field;
    }

    /**
     * Convert field value before inserting into databse query.
     *
     * @param string $field field name passed to be able to tell which filed you are dealing with.
     * @param mixed $value field value as stored in  the object. Process if necessary and return.
     * @return Zend_Db_Expr|string|int field value in the format suitable for a database query.
     */
    protected function _sanitiseValue($field, $value)
    {
        return $value;
    }

    protected function _sanitiseValues($data)
    {
        return $data;
    }

    /**
     * Create a list of fields to be read form the databse in SELECT query.
     * By default read all fields using sql wildcard.
     * Subclasses can append to this if some calculated fields are required or replace with a list of specific fields.
     *
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        return array(Zend_Db_Table_Select::SQL_WILDCARD);
    }

    public function getUniqueUrl($model)
    {
        $url = Mcmr_StdLib::urlize($model->getTitle());
        if ($this->urlExists($url)) {
            $urlTail = 2;
            $newUrl = Mcmr_StdLib::urlize($url . ' ' . $urlTail);
            while ($this->urlExists($newUrl)) {
                $urlTail++;
                $newUrl = $url . '-' . $urlTail;
            }
            return $newUrl;
        }

        return $url;
    }

    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    public function urlExists($url)
    {
        return (null !== $this->findOneByField(array('url' => $url)));
    }


    /**
     * Add extra conditions to the select query when fetching all
     *
     * @param Zend_Db_Select $select
     * @return Zend_Db_Select
     */
    protected function _postAllConditions(Zend_Db_Select $select)
    {
        return $select;
    }

    /**
     * Add extra order conditions to the select query when fetching all
     *
     * @param Zend_Db_Select $select
     * @return Zend_Db_Select
     */
    protected function _postAllOrder(Zend_Db_Select $select)
    {
        return $select;
    }

    /**
     * Add extra conditions to the select query when fetching one
     *
     * @param Zend_Db_Select $select
     * @return Zend_Db_Select
     */
    protected function _postOneConditions(Zend_Db_Select $select)
    {
        return $select;
    }

    /**
     * Add extra order conditions to the select query when fetching one
     *
     * @param Zend_Db_Select $select
     * @return Zend_Db_Select
     */
    protected function _postOneOrder(Zend_Db_Select $select)
    {
        return $select;
    }

    protected function _postSave(Mcmr_Model_ModelAbstract $model)
    {
    }

    protected function _maxCacheLifetime(Zend_Cache_Core $cache, $conditions)
    {
        return false;
    }

}