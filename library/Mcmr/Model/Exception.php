<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Exception.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Exception class for Mcmr_Model
 *
 * @category Mcmr
 * @package Mcmr_Model
 */
class Mcmr_Model_Exception extends Zend_Exception
{
}
