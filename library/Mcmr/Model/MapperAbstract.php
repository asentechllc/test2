<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: MapperAbstract.php 2392 2011-05-04 11:00:40Z leigh $
 */

/**
 * Abstract class for Mapper classes
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage MapperAbstract
 */
abstract class Mcmr_Model_MapperAbstract
{
    protected $_dbTable = null;
    protected $_dbAttrTable = null;
    protected $_dbTableClass = null;
    protected $_dbTableName = null;
    protected $_dbAttrTableClass = null;
    protected $_dbAttrTableName = null;
    protected $_dbOrdrTableClass = null;
    protected $_dbOrdrTableName = null;
    protected $_columnPrefix = null;
    protected $_cacheIdPrefix = null;
    protected $_modelClass = null;
    protected $_fields = array(Zend_Db_Table_Select::SQL_WILDCARD);
    protected $_joinedAttributes = array();
    protected $_joinedOrders = array();
    
    private $_observers = array();
    private $_observerLoader = null;
    private $_observersEnabled = true;
    private static $_transactionNesting = 0;
    
    const OBSERVER_CONFIG = 'plugins.ini';

    public function  __construct()
    {
        // Register default observer namespace
        $this->addObserverNamespace(array('prefix' => 'Mcmr_Model_Observer_', 'path'=>'Mcmr/Model/Observer'));

        // Register configured observers
        $options = null;
        try {
            // Get the config. If we can't load the config file ignore it.
            $options = Mcmr_Config_Ini::getInstance(self::OBSERVER_CONFIG, APPLICATION_ENV);
            $options = $options->observer;
        } catch (Exception $e) {
        }

        if (null !== $options) {
            if (null !== $options->namespace) {
                $namespaceArray=$options->namespace->toArray();
                if (isset($namespaceArray['prefix'])) {
                    $this->addObserverNamespace($namespaceArray);
                } else {
                    foreach($namespaceArray as $namespace) {
                        $this->addObserverNamespace($namespace);
                    }
                }
            }

            $class = get_class($this);
            if (null !== $options->$class) {
                $this->setObservers($options->$class->toArray());
            }
        }

        // Register observer for application stats
        if (defined('APPLICATION_STATS') && APPLICATION_STATS) {
            $this->registerObserver('Cachestats');
        }
        
        $this->init();
    }

    /**
     * Serialise magic function. Should stop anything being serialised
     *
     * @return array
     */
    public function __sleep()
    {
        // Nothing should be serialised in mappers
        return array();
    }

    /**
     * Serailise magic function. If this object was serialised clear everything
     * and start again.
     */
    public function __wakeup()
    {
        // Reset this object if it was serialised
        $this->_observerLoader = null;
        $this->init();
    }

    public function init()
    {
    }

    /**
     * Create an instance of the dbTable class
     *
     * @param Zend_Db_Table_Abstract|string $dbTable
     * @throws Mcmr_Model_Exception
     * @return Mcmr_Model_MapperAbstract
     */
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable) && class_exists($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (! $dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Mcmr_Model_Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        
        return $this;
    }

    /**
     * Gets an instance of the dbTable
     * @return Zend_Db_Table_Abstract
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable($this->_dbTableClass);
        }
        
        return $this->_dbTable;
    }
    
    protected function _getDbTableName() 
    {
        if (null === $this->_dbTableName) { 
            $this->_dbTableName = $this->getDbTable()->info(Zend_Db_Table::NAME);
        }
        return $this->_dbTableName;
    }
    
    protected function _getDbAttrTableName() 
    {
        if (null === $this->_dbAttrTableName) {
            $dbAttrTable = $this->_dbAttrTableClass;
            $dbAttrTable = new $dbAttrTable();
            $this->_dbAttrTableName = $dbAttrTable->info(Zend_Db_Table::NAME);
        }
        return $this->_dbAttrTableName;
    }

    protected function _getDbOrdrTableName() 
    {
        if (null === $this->_dbOrdrTableName) {
            $dbOrdrTable = $this->_dbOrdrTableClass;
            $dbOrdrTable = new $dbOrdrTable();
            $this->_dbOrdrTableName = $dbOrdrTable->info(Zend_Db_Table::NAME);
        }
        return $this->_dbOrdrTableName;
    }

    protected function _prefixField($field) {
        return $this->_getDbTableName().'.'.$field;
    }

    /**
     * Find a single model by the id
     *
     * @param int $id
     * @return Mcmr_Model_ModelAbstract
     */
    public function find($id)
    {
        return $this->findOneByField(array('id' => $id));
    }

    /**
     * Generate a cache key based on the condition array
     *
     * @param array $condition
     * @param array $order
     * @return string
     */
    protected function _getCacheId($condition, $order=null)
    {
        $cacheid = '';
        if (is_array($condition)) {
            foreach ($condition as $key=>$value) {
                if (is_array($value)) {
                    if (isset($value['condition'])) {
                        $condition = $value['condition'];
                        $value = $value['value'];
                    }

                    if (is_array($value)) {
                        $value = serialize($value);
                    }
                } else {
                    $condition = '=';
                }
                $cacheid .= "{$key}-{$condition}-{$value}_";
            }
        }

        if (is_array($order)) {
            foreach ($order as $key=>$value) {
                if (!is_string($value)) {
                    $value = serialize($value);
                }
                $cacheid .= "orderby-{$key}-{$value}_";
            }
        }

        return md5($cacheid);
    }

    /**
     * Adds a prefix to each array index name.
     *
     * @param array $data
     * @param string $prefix
     * @return array
     */
    protected function _addColumnPrefix(array $data, $prefix)
    {
        // Prefix each table field with 'user_'
        foreach ($data as $key=>$value) {
            $newkey = $prefix . $key;
            $data[$newkey] = $value;
            unset($data[$key]);
        }

        return $data;
    }



    protected function _addOrderCondition($select, $field, $direction)
    {
    
    	$joinFn = 'joinLeft';
    	if(is_array($direction)){
    		if(isset($direction['join'])){
    			switch(strtolower($direction['join'])){
    				case 'inner':
    					$joinFn = 'join';
    					break;
    			}
    		}
    		$direction = isset($direction['dir']) ? $direction['dir'] : '';
    	}
    
        if ('asc' !== strtolower($direction) && 'desc' !== strtolower($direction)) {
            throw new Mcmr_Model_Exception('Invalid Order By direction '.$direction);
        }

        if (empty($field)) {
            return $select;
        }
        
        if (0 === strncmp('ordr_', $field, 5)) {
            $table = $this->getDbTable();
            if (in_array($field, $table->info('cols'))) {
                $select->order($field . ' ' . $direction);
            } else {
                $fieldName = str_replace('ordr_', '', $field);
                $this->_addOrderJoin($select, $fieldName, $joinFn );
                $select->order($fieldName . ".ordr_value " . $direction);
            }
        } elseif (0 === strncmp('attr_', $field, 5)) {
            $fieldName = str_replace('attr_', '', $field);
            $this->_addAttributeJoin($select, $fieldName );
            $select->order($fieldName . ".attr_value " . $direction);
        } else {
            $field = $this->_sanitiseField($field);
            $select->order($field.' '.$direction);
        }
        return $select;
    }

    protected function _addOrderStatCondition($select, $stat, $direction)
    {
        $start = strtotime('-2 weeks');
        $end = strtotime('yesterday');

        $select->setIntegrityCheck(false);
        $tableName = 'default_stats_'.$stat;
        $select->join(array($tableName=>'default_stats'), "{$tableName}.stat_modelid = {$this->_columnPrefix}");
        $select->where("{$tableName}.stat_modeltype = ?", $this->_modelClass);
        $select->where("{$tableName}.stat_action = ?", $stat);
        $select->where("{$tableName}.stat_date BETWEEN DATE(FROM_UNIXTIME({$start})) AND DATE(FROM_UNIXTIME({$end}))");
        $select->order("{$tableName}_stat_sum $direction");
        $select->group($this->_columnPrefix.'id');

        $this->_fields[] = "UNIX_TIMESTAMP({$tableName}.stat_date) AS {$tableName}_stat_date";
        $this->_fields[] = "SUM({$tableName}.stat_count) as {$tableName}_stat_sum";

        return $select;
    }

    /**
     * Strips the prefix from the array index names
     * 
     * @param array $data
     * @param string $prefix
     * @return array
     */
    protected function _stripColumnPrefix(array $data, $prefix)
    {
        foreach ($data as $key=>$value) {
            // If this is not an attribute field or an order field strip the prefix
            if ((0 !== strncmp('attr_', $key, 5))
                && (0 !== strncmp('ordr_', $key, 5))
                ) {
                $newkey = str_replace('_', '', str_replace($prefix, '', $key));
                if ( $key != $newkey ) {
                    $data[$newkey] = $value;
                    unset($data[$key]);
                }
            }
        }

        return $data;
    }

    /**
     * Save the attributes for the model in the model's attribute table.
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    protected function _saveAttributes(Mcmr_Model_ModelAbstract $model)
    {
        if (null !== $this->_dbAttrTableClass) {
            $attributes = $model->getAttributes();
            $id = $model->getId();

            $modelTable = $this->getDbTable();
            $table = new $this->_dbAttrTableClass();
            $table->setAttrId($id);

            $tableData = array();
            foreach ($attributes as $name=>$value) {
                if (is_array($value) || is_object($value)) {
                    $value = serialize($value);
                }
                if (null === $value) {
                    $value = '';
                }

                $field = 'attr_'.$name;
                if (in_array($field, $modelTable->info('cols'))) {
                    $tableData[$field] = $value;
                } else {
                    $table->setAttrName($name);
                    $table->setAttrValue($value);
                    $table->setAttrType($model->getAttributeType($name));

                    $table->insert();
                }
            }
            
            if (!empty($tableData)) {
                $key = $this->_keyCondition($modelTable, $model->getId());
                $modelTable->update($tableData, $key);
            }
        }
    }

    /**
     * Delete the model's attributes
     * 
     * @param Mcmr_Model_ModelAbstract $model
     * @param array|string $where SQL WHERE clause(s).
     */
    protected function _deleteAttributes($where)
    {
        if (null !== $this->_dbAttrTableClass) {
            $table = new $this->_dbAttrTableClass();
            $table->delete($where);
        }
    }

    /**
     * Load the attributes from the attribute table.
     * 
     * @param Mcmr_Model_ModelAbstract $model
     */
    protected function _loadAttributes(Mcmr_Model_ModelAbstract $model)
    {
        if (null !== $this->_dbAttrTableClass) {
            $table = new $this->_dbTableClass();
            $attrtable = new $this->_dbAttrTableClass();

            $rows = $attrtable->fetchAll($this->_keyCondition($table, $model->getId()));
            foreach ($rows as $row) {
                // If we have a attribute type, get the model, or an array of the models if using a link table
                if (null !== $row['attr_type']) {
                    $temp = explode(':', $row['attr_type']);
                    $classname = $temp[0];
                    if (isset($temp[1])) {
                        $linktable = $temp[1];
                    } else {
                        $linktable = null;
                    }

                    if (null !== $linktable) {
                        $value = $this->_loadAttributeLink($row['attr_value'], $linktable, $classname);
                    } else {
                        $value = $this->_loadAttributeType($row['attr_value'], $classname);
                    }
                } else {
                    $value = $row['attr_value'];
                }

                $tValue = @unserialize($value);
                if ((false === $tValue) && ($tValue !== $value)) {
                    $tValue = $value;
                }

                $model->setAttribute($row['attr_name'], $tValue);
            }
        }
    }
    
    
    /**
     * Save the order lists for the model
     *
     * @param $model 
     */
    protected function _saveOrder($model)
    {
        if ($model instanceof Mcmr_Model_Ordered) {
            if (null !== $this->_dbOrdrTableClass) {
                $table = new $this->_dbTableClass();
                $orderTable = new $this->_dbOrdrTableClass();

                $order = $model->getOrder();
                $this->_deleteOrder($model);
                $id = $model->getId();

                $orderTable->setOrdrId($id);
                foreach ($order as $name=>$value) {
                    $field = 'ordr_'.$name;
                    if (in_array($field, $table->info('cols'))) {
                        $orderTableData[$field] = $value;
                    } else {
                        $orderTable->setOrdrName($name);
                        $orderTable->setOrdrValue($value);
                        $orderTable->insert();
                    }
                }
            } else {
                throw new Mcmr_Model_Exception("Model is ordered, but order table not defined");
            }
        }
    }

    /**
     * Load the order lists for this model
     *
     * @param Game_Model_Game $model
     */
    public function loadOrder($model)
    {
        if (null !== $this->_dbOrdrTableClass) {
            if (null === $model->getId()) {
                return array();
            }
            
            $cacheid = $this->_cacheIdPrefix.$this->_getCacheId(array('id'=>$model->getId()));
            $cache = Zend_Registry::get('cachemanager')->getCache('database');

            if (false === ($order = $cache->load($cacheid))) {

                //fetch rows from order table
                $table = new $this->_dbOrdrTableClass();
                $rows = $table->fetchAll($this->_keyCondition($table, $model->getId()));
                $order = array();
                foreach ($rows as $row) {
                    $order[$row['ordr_name']] = $row['ordr_value'];
                }

                //fetch colums from model table
                $modelTable = $this->getDbTable();
                $data = $modelTable->find($model->getId());
                $data = $data->toArray();
                if (array_key_exists(0, $data)) {
                    foreach ($data[0] as $field=>$value) {
                        if (0 === strncmp('ordr_', $field, 5)) {
                            $field = str_replace('ordr_', '', $field);
                            $order[$field] = $value;
                        }                
                    }
                }

                $idTag = strtolower($this->_modelClass).'_id_'.$model->getId();
                $indexTag = strtolower($this->_modelClass).'_index';
                $cache->save($order, $cacheid, array($idTag, $indexTag));
            }

            $model->setOrder($order);
        }
    }

    
    /**
     * Delete the order lists for the model
     *
     * @param Game_Model_Game $model 
     */
    protected function _deleteOrder($model)
    {
        if (null !== $this->_dbOrdrTableClass) {
            $table = new $this->_dbOrdrTableClass();
            $table->delete($this->_keyCondition($table, $model->getId()));
        }
    }

    /**
     * Using the table and ID value generate the array used to find the record in the table
     *
     * @param Zend_Db_Table $table
     * @param mixed $id
     * @return array
     */
    protected function _keyCondition(Zend_Db_Table_Abstract $table, $id)
    {
        if (!is_array($id)) {
            $keys = $table->info(Zend_Db_Table::PRIMARY);
            
            return array($keys[1].'=?'=>$id);
        } else {
            $condition = array();
            foreach ($id as $key=>$value) {
                $condition[$key.'=?'] = $value;
            }

            return $condition;
        }
    }

    /**
     * Begin a transaction
     *
     * @return Mcmr_Model_MapperAbstract
     */
    final protected function _begin()
    {
        // We only want to begin a transaction if no other model saves are being performed.
        if (0 === self::$_transactionNesting) {
            //Zend_Db_Table::getDefaultAdapter()->beginTransaction();
        }

        // Increase the nest count to indicate we are deeper into a model save
        self::$_transactionNesting++;

        return $this;
    }

    /**
     * Commit the transaction
     *
     * @return Mcmr_Model_MapperAbstract
     */
    final protected function _commit()
    {
        // We only want to commit a transaction if we are the top transaction level
        if (1 === self::$_transactionNesting) {
            try {
                //Zend_Db_Table::getDefaultAdapter()->commit();
            } catch (Exception $e) {
                throw $e;
            }
        }

        // Decrement the nesting level
        self::$_transactionNesting--;

        return $this;
    }

    /**
     * Rollback a transaction
     *
     * @return Mcmr_Model_MapperAbstract
     */
    final protected function _rollback()
    {
        // We only want to rollback a transaction if we are the top transaction level
        if (1 === self::$_transactionNesting) {
            //Zend_Db_Table::getDefaultAdapter()->rollback();
        }

        // Decrement the nesting level
        self::$_transactionNesting--;

        return $this;
    }

    /**
     * Loads a dynamic attribute that uses a linking table to other data in the system.
     * Will return an array of that data in the model class if a type is provided.
     *
     * @param int $id
     * @param string $linktable The link table name
     * @param string $type The field type, a model classname
     * @return array An array of IDs that link to this attribute
     */
    private function _loadAttributeLink($id, $linktable, $type = null)
    {
        return array();
    }

    /**
     * Takes the database value and a Model classname and returns a model object
     * or the raw value if no type is provided
     *
     * @param string $value
     * @param string $type
     * @return string|Mcmr_Model_ModelAbstract
     */
    private function _loadAttributeType($value, $type = null)
    {
        if ((null !== $type) && class_exists($type)) {
            $mapper = call_user_func(array($type, 'getMapper'));
            $value = $mapper->find($value);
        }

        return $value;
    }

    /**
     * Registers a namespace in which to search for observer classes
     *
     * @param string $namespace
     * @return Mcmr_Model_MapperAbstract
     */
    public function addObserverNamespace($namespace)
    {
        if (null === $this->_observerLoader) {
            $this->_observerLoader = new Zend_Loader_PluginLoader();
        }

        $this->_observerLoader->addPrefixPath($namespace['prefix'], $namespace['path']);
        
        return $this;
    }

    /**
     * Register an observer for this mapper
     *
     * @param string $name
     * @return Mcmr_Model_MapperAbstract
     */
    public function registerObserver($name)
    {
        // Lazy loading. We won't create the instance unless observed
        $this->_observers[$name] = null;

        return $this;
    }

    /**
     *
     * @param array $observers
     * @return Mcmr_Model_MapperAbstract
     */
    public function setObservers(array $observers)
    {
        $this->_observers = array();
        foreach ($observers as $observer) {
            $this->_observers[$observer] = null;
        }
        
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getObservers()
    {
        return $this->_observers;
    }

    /**
     *
     * @return Mcmr_Model_MapperAbstract
     */
    public function clearObservers()
    {
        $this->_observers = array();

        return $this;
    }

    /**
     * Remove an observer from this mapper
     *
     * @param string $name
     * @return Mcmr_Model_MapperAbstract
     */
    public function deleteObserver($name)
    {
        if (array_key_exists($name, $this->_observers)) {
            unset($this->_observers[$name]);
        } else {
            throw new Mcmr_Model_Exception("Cannot delete unknown mapper observer '{$name}'");
        }

        return $this;
    }

    /**
     * Enable or disable the observers
     *
     * @param bool $enable
     * @return Mcmr_Model_MapperAbstract
     */
    public function setObserversEnabled($enable)
    {
        $this->_observersEnabled = (bool)$enable;

        return $this;
    }

    /**
     * Return the observers enabled flag
     *
     * @return bool
     */
    public function getObserversEnabled()
    {
        return $this->_observersEnabled;
    }

    /**
     * Notify all observers of an event. Some observers may be added to a message
     * queue to be executed seperately by a processor. If the queue doesn't exist 
     * then the observer is executed immediately. Observers must be specifically made
     * queuable by implementing Mcmr_Model_Observer_QueueableInterface.
     *
     * @param string $event
     * @param Object $model
     * @return Mcmr_Model_MapperAbstract
     */
    public function notify($event, $model)
    {
        if (!$this->_observersEnabled) {
            return $this;
        }

        foreach ($this->_observers as $name => $instance) {
            if (null == $instance) {
                $instance = $this->_loadObserver($name);
                if ($instance instanceof Mcmr_Model_ObserverAbstract) {
                    $this->_observers[$name] = $instance;
                }
            }
            
            if ($instance->isEnabled() && method_exists($instance, $event)) {
                if ($instance instanceof Mcmr_Model_Observer_QueueableInterface) {
                    $queue = Mcmr_Queue::getInstance('observer');
                    if (is_object($queue) && $queue->isSupported('send')) {
                        $message = new stdClass();
                        $message->observer = $instance;
                        $message->event = $event;
                        $message->model = $model;
                        $message->modelClass = get_class($model);
                        $message->modelId = $model->getId();
                        
                        $queue->send(serialize($message));
                    } else {
                        $instance->$event($model);
                    }
                } else {
                    $instance->$event($model);
                }
            }
        }

        return $this;
    }

    protected function _defaultAddCondition($select, $field, $condition, $value) 
    {
    //    $select->where( $this->_prefixField($field)." $condition (?) ", $value);
        $select->where( $this->_prefixField($field)." $condition (?) ", $value);
    }

    protected function _addSitesCondition($select, $field, $value) 
    {
        if ($value && '*' !== $value ) {
            $select->where( 'FIND_IN_SET( ?, '.$field.' ) > 0', $value );
        }
        return $select;
    }
    

    protected function _addBetweenCondition($select, $field, $value) 
    {
        $start = $value[0];
        $end = $value[1];
        $select->where( $this->_prefixField($field)." BETWEEN {$start} AND {$end}");
    }
    
    protected function _addUrlEqualsCondition($select, $field, $value) 
    {
        $model = $value['model'];
        $url = $value['url'];
        $id = Mcmr_Model::idForUrl($model, $url);
        if (null===$id) {
            throw new Exception("Instance of $model with url '$url' not found");
        }
        $this->_defaultAddCondition($select, $field, '=', $id);
    }

    protected function _addUrlInCondition($select, $field, $value) 
    {
        $model = $value['model'];
        $urls = $value['url'];
        $ids = Mcmr_Model::idsForUrls($model, $urls);
        $this->_defaultAddCondition($select, $field, 'IN', $ids);
    }

    protected function _addAttributeJoin( $select, $fieldName )
    {
        if (null === $this->_joinedAttributes) {
            $this->_joinedAttributes = array();
        }
        if (in_array($fieldName, $this->_joinedAttributes)) {
                return;
        }
        $this->_joinedAttributes[] = $fieldName;
        $tableName = $this->_getDbTableName();
        $attrTableName = $this->_getDbAttrTableName();
        $keys = $this->getDbTable()->info(Zend_Db_Table::PRIMARY);
        $joinParts = array();
        if (!is_array($keys))	{
            $joinParts[] = "{$tableName}.{$keys} = {$fieldName}.{$keys}";
        } else {
            $joinParts = array();
            foreach ($keys as $key) {
                $joinParts[] = "{$tableName}.{$key} = {$fieldName}.{$key}";
            }
        }
        
        $joinParts[] = "{$fieldName}.attr_name = '{$fieldName}'";
        $joinCondition = implode( ' AND ', $joinParts );
		$select->setIntegrityCheck(false);
        $select->join(array($fieldName=>$attrTableName), $joinCondition, array( 'attr_value' ) );
        $select->where("{$fieldName}.attr_name = ?", $fieldName);
        return $select;
    }

    protected function _addOrderJoin( $select, $fieldName, $joinFn = 'joinLeft' )
    {
        if (null === $this->_joinedOrders) {
            $this->_joinedOrders = array();
        }
        if (in_array($fieldName, $this->_joinedOrders)) {
            return;
        }
        $this->_joinedOrders[] = $fieldName;
        $tableName = $this->_getDbTableName();
        $ordrTableName = $this->_getDbOrdrTableName();
        $keys = $this->getDbTable()->info(Zend_Db_Table::PRIMARY);
        $joinParts = array();
        if (!is_array($keys))	{
            $joinParts[] = "{$tableName}.{$keys} = {$fieldName}.{$keys}";
        } else {
            foreach ($keys as $key) {
                $joinParts[] = "{$tableName}.{$key} = {$fieldName}.{$key}";
            }
        }
        $joinParts[] = "{$fieldName}.ordr_name = '{$fieldName}'";
        $joinCondition = implode( ' AND ', $joinParts );
        $select->setIntegrityCheck(false);
        $select->$joinFn(array($fieldName=>$ordrTableName), $joinCondition, array( 'ordr_value' ) );
        return $select;
    }
    
    /**
     * Add a condition to the select query
     *
     * @param Zend_Db_Select $select
     * @param string $field
     * @param string $value
     * @return Zend_Db_Select
     */
    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        if (is_array($value)) {
            $condition = $value['condition'];
            $value = $value['value'];
        } else {
            $condition = '=';
        }
        if (!is_array($value))
        	$sanitisedValue = $this->_sanitiseValue($field, $value);
        if (is_array($value)) {
            $sanitisedValue = array();
            foreach ($value as $k => $v) {
                $sanitisedValue[$k] = $this->_sanitiseValue($field, $v);
            }
            switch (strtolower($condition)) {
            case 'between':
                $this->_addBetweenCondition($select, $field, $sanitisedValue);
                break;
            case 'url_equals':
                $this->_addUrlEqualsCondition($select, $field, $sanitisedValue);
                break;
            case 'url_in':
                $this->_addUrlInCondition($select, $field, $sanitisedValue);
                break;
            default:
                $this->_defaultAddCondition($select, $field, $condition, $sanitisedValue);
            }
        } elseif ('null' === strtolower($sanitisedValue)) {
            $select->where( $this->_prefixField($field)." $condition " . $sanitisedValue);
        } else {
            $this->_defaultAddCondition($select, $field, $condition, $sanitisedValue);
        }
        return $select;
    }

    protected function _addAttributeCondition($select, $tableName, $fieldName, $idField, $value, $condition = '=')
    {
        $modelTable = $this->getDbTable();
        if (in_array('attr_'.$fieldName, $modelTable->info('cols'))) {
            $select->where("attr_{$fieldName} {$condition} ?", $value);
        } else {
            $this->_addAttributeJoin($select, $fieldName);
            $select->where("{$fieldName}.attr_value {$condition} ?", $value);
        }

        return $select;
    }
    
    
    public function resetOrdering($type)
    {
        $table = $this->getDbTable();
        if (in_array('ordr_'.$type, $table->info('cols'))) {
            $table->update(array('ordr_'.$type=>0), '1');
        } else {
            $dbOrdrTable = $this->_dbOrdrTableClass;
            $dbOrdrTable = new $dbOrdrTable();
            $select = $dbOrdrTable->delete(array('ordr_name = ?'=>$type));
        }
        // Clean the cache
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        $cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, array(strtolower($this->_modelClass).'_index'));

        return $this;
    }

    protected function _resetMapper()
    {
        $this->_joinedOrders = array();
        $this->_joinedAttributes = array();
    }

    protected function _selectFields()
    {
        // This is not a good place for this. Mappers REALLY need to be refactored
        $this->_resetMapper();
        
        return $this->_fields;
    }

    /**
     * Create an instance of an observer. Searches the registered observer namespaces
     *
     * @param string $name
     * @return Mcmr_Model_ObserverAbstract
     */
    private function _loadObserver($name)
    {
        $classname = $this->_observerLoader->load($name);
        if (class_exists($classname)) {
            $observer = new $classname(get_class($this));
            if ($observer instanceof Mcmr_Model_ObserverAbstract) {
                // Observer found. Return it
                return $observer;
            }
        }

        // Unable to find the observer
        throw new Mcmr_Model_Exception("Mapper observer '$name' not found.");
    }

    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    abstract public function save(Mcmr_Model_ModelAbstract $model);

    /**
     * Delete the model information from the database
     * 
     * @param Mcmr_Model_ModelAbstract $model
     */
    abstract public function delete(Mcmr_Model_ModelAbstract $model);

    /**
     * Find a model based on the condition
     *
     * @param array $condition
     * @return Mcmr_Model_ModelAbstract
     */
    abstract public function findOneByField($condition = null);

    /**
     * Get a paginator object based on the condition
     *
     * @param array|null $condition
     * @param array|null $order
     * @param int $page
     * @return Zend_Paginator
     */
    abstract public function findAllByField($condition = null, $order=null, $page = 1);
    
}