<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ObserverAbstract.php 2116 2011-01-26 15:52:30Z michal $
 */

/**
 * Abstract class for All Observers
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage Observer
 */
abstract class Mcmr_Model_ObserverAbstract
{
    protected $_mapperClass = null;

    static protected $_disabled=array();
    
    public function __construct($mapperClass)
    {
        $this->_mapperClass = $mapperClass;
        
        $this->init();
    }
    
    public function init()
    {
        
    }
    
    public function  __call($name,  $arguments)
    {
        // Catch all function calls. Allows observers to create any event they want
    }
    
    public function setEnabled( $enabled ) 
    {
        $key = get_class($this);
        if ( $enabled ) {
            unset( self::$_disabled[ $key ] );
        } else {
            self::$_disabled[ $key ] = true;
        }
        return $this;
    }
    
    public function getEnabled() 
    {
        if (isset(self::$_disabled[get_class($this)])) {
            return false;
        } else {
            return true;
        }
    }
    
    public function isEnabled() 
    {
        return $this->getEnabled();
    }
    
    public function getConfig()
    {
        $model = strtolower(array_shift(explode('_', $this->_mapperClass)));
        $observer = strtolower(array_pop(explode('_', get_class($this))));
        
        $config = Zend_Registry::get($model . '-config');
        if (isset($config->$observer)) {
            return $config->$observer;
        } else {
            return new Zend_Config(array());
        }
    }
    
}
