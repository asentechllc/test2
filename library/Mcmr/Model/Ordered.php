<?php

abstract class Mcmr_Model_Ordered extends Mcmr_Model_ModelAbstract
{
    protected $_order = null;
    
    /**
     * Get the model order number.
     *
     * @return int
     */
    final public function getOrder($name = null)
    {
        if (null === $this->_order) {
            // Lazy load the order. Don't fetch from storage unless requested
            $this->getMapper()->loadOrder($this);
        }

        if (null !== $name) {
            if (isset($this->_order[$name])) {
                return $this->_order[$name];
            } else {
                return null;
            }
        }

        return $this->_order;
    }

    /**
     * Set the order of the model.
     *
     * @param int $order
     */
    final public function setOrder($order, $name = null)
    {
        if (null !== $name) {
            $current = $this->getOrder();
            $current[$name] = (int)$order;

            $this->_order = $current;
        } elseif (is_array($order)) {
            $this->_order = $order;
        }

        return $this;
    }    
}
