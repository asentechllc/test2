<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: AttrInterface.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Interface class for all Attribute model classes
 * 
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage AttrInterface
 */
interface Mcmr_Model_AttrInterface
{
    public function insert();
    public function setAttrName($name);
    public function setAttrValue($value);
    public function setAttrId($id);
    public function setAttrType($type);
}
