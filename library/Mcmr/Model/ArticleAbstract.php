<?php

/**
 * Class for share article-like features
 * - Sections
 **/

abstract class Mcmr_Model_ArticleAbstract extends Mcmr_Model_Ordered{

    protected $_sectionid = 0;
    protected $_topsectionid = null;
    protected $_subsectionid = null;

    private $_section = null;
    private $_parentSection = null;
    private $_topSection = null;

    /**
     *
     * @return News_Model_Section
     */
    public function getSection()
    {
    
    	if(!$this->getSectionid())
    		return null;
    
        if (null === $this->_section) {
            $mapper = News_Model_Section::getMapper();
            $this->_section = $mapper->find($this->getSectionid());
        }

        return $this->_section;
    }
    public function getParentsection()
    {
        if (null === $this->_parentSection) {
        	$section = $this->getSection();
        	if($section)
        		$this->_parentSection = $section->getParent();
        	
        	# don't want to check twice	
        	if(!$this->_parentSection)
        		$this->_parentSection = false;
        }

        return $this->_parentSection;
    }
    public function getSectionUrl(){
    	$section = $this->getSection();
    	if($section instanceof News_Model_Section){
    		$parent = $this->getParentSection();
    		return ($parent instanceof News_Model_Section ? $parent->getUrl().'/' : '').$section->getUrl();
    	}
    	return null;
    }

    /**
     * Get the article Section ID
     *
     * @return int
     */
    public function getSectionid()
    {
        return $this->_sectionid;
    }
    public function getTopsectionid()
    {
    	$parent = $this->getParentsection();
        return $parent ? $parent->getId() : $this->getSectionid();
    }
    public function getSubsectionid()
    {
    	$parent = $this->getParentsection();
        return $parent ? $this->getSectionid() : 0;
    }
    public function getTopsection(){
    	if(null === $this->_topSection && ($topSectionId = $this->getTopSectionid())){
    		$this->_topSection = News_Model_Section::getMapper()->find($topSectionId);
    	}
    	return $this->_topSection;
    }
    public function getSections(){
    	$sections = array();
    	$sections[] = $this->getSection();
    	
    	# no sections -- return empty array
    	if(!$sections[0])
    		return array();
    	
    	if($sections[0]->getParentid()){
    		array_unshift($sections,$this->getTopsection());
    	}
    	return $sections;
    }

    /**
     * Set the article Section ID
     *
     * @param int $sectionid
     * @return News_Model_Article
     */
    public function setSectionid($sectionid)
    {
        // If the sectionid changes reset the section object
        if ($sectionid !== $this->_sectionid) {
            $this->_section = null;
        }

        $this->_sectionid = $sectionid;

        return $this;
    }


    /**
     * Get author information
     *
     * @return array
     */
    public function getAuthorInfo(){

    	$info = array(
    		'type' => method_exists($this,'getAuthortype') ? $this->getAuthortype() : $this->getAttribute('author'),
    		'name' => 'Anonymous',
    		'image' => null,
    		'model' => null,
    		'url' => null,
    	);

    	switch($info['type']){
    		case 'regular':
    			if($info['model'] = User_Model_User::getMapper()->find($this->getUseridauthor())){
	    			$info['name'] = $info['model']->getFullname();
	    			$info['image'] = trim($info['model']->getAttribute('image'));
	    		}
    			break;

    		case 'guest':
    			$info['name'] = $this->getAuthorname();
    			$info['image'] = method_exists($this,'getAuthorimage') ? $this->getAuthorimage() : '';    			
    			break;

    		case 'partner':
    			$info['name'] = $this->getPartnername();
    			$info['image'] = $this->getPartnerimage();
    			break;

    		case 'sponsored':
    			$info['name'] = $this->getAttribute('sponsorTitle');
    			$info['image'] = $this->getAttribute('sponsorImage');
    			$info['url'] = $this->getAttribute('sponsorUrl');
    			break;
    	}
    	
    	return $info;
    }

}