<?php

class Mcmr_Model_Query
{
    protected $_conditions = null;
    protected $_page = null;
    protected $_orders = null;

    protected static $_request;
    protected static $_deviceQueries;

    public static function setDeviceQueries($val)
    {
        self::$_deviceQueries = $val;
    }

    public static function getDeviceQueries()
    {
        return self::$_deviceQueries;
    }


    protected static function getRequest()
    {
        if (null===self::$_request) {
            self::$_request = Zend_Controller_Front::getInstance()->getRequest();
        }
        return self::$_request;
    }

    public static function factory($query, $module=null)
    {
        $request = static::getRequest();
        if (null===$module) {
            $module = $request->getModuleName();
        }
        $config = static::getConfig($query, $module);
        $query = new Mcmr_Model_Query();
        $query->setConfig($config);
        return $query;
    }

    public static function exists($query, $module=null)
    {
        $request = static::getRequest();
        if (null===$module) {
            $module = $request->getModuleName();
        }
        try {
            $config = static::getConfig($query, $module);
            return null!==$config;
        } catch (Exception $e) {
            return false;
        }

    }

    public static function getSingleConfig($query, $module, $device=true)
    {
        $queryConfig = self::getSingleConfigFromModule($query, $module, $device);
        if (null===$queryConfig) {
            $queryConfig = self::getSingleConfigFromModule($query, 'default', $device);
        }
        if (null===$queryConfig) {
            throw new Exception("Cannot find query config with name $query");
        }
        return $queryConfig;
    }

    public static function getSingleConfigFromModule($query, $module, $device=true)
    {
        $moduleConfig = Zend_Registry::get($module.'-config');
        if (!isset($moduleConfig->queries)) {
            return null;
        }
        $deviceQuery = $query.'-'.self::$_request->getDeviceClass();
        if ($device && self::getDeviceQueries() && isset($moduleConfig->queries->$deviceQuery)) {
            return $moduleConfig->queries->$deviceQuery;
        }
        if (isset($moduleConfig->queries->$query)) {
            return $moduleConfig->queries->$query;
        }
        return null;
    }

    public static function getConfig($query, $module, $device=true)
    {
        $queryConfig = self::getSingleConfig($query, $module, $device);
        if (isset($queryConfig->_extends)) {
            $newConfig = new Zend_Config(array(), true);
            $baseConfigName = $queryConfig->_extends;
            $device &= ($query != $baseConfigName);
            $baseConfig = static::getConfig($baseConfigName, $module, $device);
            $newConfig->merge($baseConfig);
            $newConfig->merge($queryConfig);
            $newConfig->setReadOnly();
            $queryConfig = $newConfig;
        }
        return $queryConfig;
    }

    public function __construct()
    {
        $this->_conditions = new Zend_Config(array());
        $this->_page = new Zend_Config(array());
        $this->_orders = new Zend_Config(array());

    }

    public function setConfig($config)
    {
        if (!($config instanceof Zend_Config)) {
            $config = new Zend_Config($config);
        }
        //the other way round, store zend config objects, because they are nicely overwriten
        if (isset($config->conditions)) {
            $this->setConditions($config->conditions);

        }
        if (isset($config->orders)) {
            $this->setOrders($config->orders);

        }
        if (isset($config->page)) {
            $this->setPage($config->page);

        }
        return $this;
    }

    public function addConfig($config)
    {
        if (!($config instanceof Zend_Config)) {
            $config = new Zend_Config($config);
        }
        if (isset($config->conditions)) {
            $this->addConditions($config->conditions);

        }
        if (isset($config->orders)) {
            $this->addOrders($config->orders);

        }
        if (isset($config->page)) {
            $this->addPage($config->page);

        }
        return $this;
    }

    public function getProcessedConditions()
    {
        $conditions = $this->_conditions;
        if ($conditions instanceof Zend_Config ) {
            $conditionsArray = $conditions->toArray();
            $conditions = static::_processValue($conditionsArray);
        }
        $processedConditions = array();
        foreach ($conditions as $key => $value ) {
            $current = $this->processFieldConditions($key, $value);
            $processedConditions = array_merge($processedConditions, $current);
        }
        if (count($processedConditions)) {
            return $processedConditions;
        }
        return null;
    }

    public function processFieldConditions($field, $conditions)
    {
        return array($field=>$conditions);
    }

    public function getConditions()
    {
        return $this->_conditions;
    }

    /**
     * Set the search conditions for model instances returned by the query.
     * @param  an array with page and count or a string 'all'
     */
    public function setConditions($conditions)
    {
        if (!($conditions instanceof Zend_Config)) {
            $conditions = new Zend_Config($conditions);
        } else {
            $conditions = clone $conditions;
            $conditions->setReadOnly();
        }
        $this->_conditions = $conditions;
        return $this;
    }

    /**
     * Add/override conditions in the query
     */
    public function addConditions($conditions)
    {
        if (!($conditions instanceof Zend_Config)) {
            $conditions = new Zend_Config($conditions);
        }
        $newConditions = new Zend_Config(array(), true);
        $newConditions->merge($this->_conditions);
        $newConditions->merge($conditions);
        $newConditions->setReadOnly();
        $this->_conditions = $newConditions;
        return $this;
    }

    public function getProcessedPage()
    {
        if (null!=$this->_page) {
            return  $this->_page->toArray();
        }
        return null;
    }

    public function getPage()
    {
        return $this->_page;
    }

    /**
     * Set the page of results to be returned by this query.
     * @param  an array with page and count or a string 'all'
     */
    public function setPage($page)
    {
        if (is_string($page)) {
            $page = new Zend_Config(array($page=>true));
        } elseif (!($page instanceof Zend_Config)) {
            $page = new Zend_Config($page);
        } else {
            $page = clone $page;
            $page->setReadOnly();
        }
        $this->_page = $page;
        return $this;
    }

    public function addPage($page)
    {
        if (!($page instanceof Zend_Config)) {
            $page = new Zend_Config($page);
        }
        $newPage = new Zend_Config(array(), true);
        $newPage->merge($this->_page);
        $newPage->merge($page);
        $this->_page = $newPage;
        return $this;
    }

    /**
     * Move page to a specified number without changing the count and only if the count is set.
     */
    public function offsetPage($page)
    {
        $page = array('page'=>$page);
        $this->addPage($page);
        return $this;
    }

    /**
     * Set Count independently of page
     */
    public function setCount($count)
    {
        if ($this->_page instanceof Zend_Config) {
            $this->_page = new Zend_Config(array_merge($this->_page->toArray(), array('count' => intval($count))));
        }

        return $this;
    }


    public function getProcessedOrders()
    {
        if (null!=$this->_orders) {
            $orders = static::_processValue($this->_orders->toArray());
            return  $orders;
        }
        return null;
    }

    public function getOrders()
    {
        return $this->_orders;
    }

    /**
     * Set the ordering of model instances in the query.
     * @param  an array with page and count or a string 'all'
     */
    public function setOrders($orders)
    {
        if (!($orders instanceof Zend_Config)) {
            $orders = new Zend_Config($orders);
        } else {
            $orders = clone $orders;
            $orders->setReadOnly();
        }
        $this->_orders = $orders;
        return $this;
    }

    public function addOrders($orders)
    {
        if (!($orders instanceof Zend_Config)) {
            $orders = new Zend_Config($orders);
        }
        $newOrders = new Zend_Config(array(), true);
        $newOrders->merge($this->_orders);
        $newOrders->merge($orders);
        $newOrders->setReadOnly();
        $this->_orders = $newOrders;
        return $this;
    }

    public function prependOrders($orders)
    {
        if (!($orders instanceof Zend_Config)) {
            $orders = new Zend_Config($orders);
        }
        $newOrders = new Zend_Config(array(), true);
        $newOrders->merge($orders);
        $newOrders->merge($this->_orders);
        $newOrders->setReadOnly();
        $this->_orders = $newOrders;
        return $this;
    }

    protected static function _processKeyValuePairs($config) {
        $result = array();
        $keys = array_keys($config);
        foreach ($keys as $key ) {
            $value = $config[$key];
            if  (is_array($value)&&isset($value['key'])&&isset($value['value'])) {
                $result[$value['key']] = $value['value'];
            } else {
                $result[$key] = $value;
            }
        }
        return $result;

    }

    protected static function _processValue($config) {
        if (is_array($config)&&count($config)) {
            $preprocessed = static::_processKeyValuePairs($config);
            $keys = array_keys($preprocessed);
            $result = array();
            foreach($keys as $key) {
                $value = static::_processValue($preprocessed[$key]);
                if ('_unset' !== $value) {
                    $result[$key]=$value;
                }
            }
            return $result;
        } else {

        	# is the value a function
        	if(is_string($config) && substr($config,0,7) == 'return '){
        		$fn = @create_function('',$config);
        		if(!empty($fn)){
        			$config = $fn();
	        	}
        	}

            return $config;
        }
    }

    public function cloneQuery()
    {
        $result = clone $this;
        return $result;
    }

}