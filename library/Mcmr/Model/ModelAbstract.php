<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ModelAbstract.php 2402 2011-05-05 14:58:38Z leigh $
 */

/**
 * Abstract class for Mcmr_Model model classes
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage ModelAbstract
 */
abstract class Mcmr_Model_ModelAbstract
{
    protected $_attributes = array();
    protected $_attributeTypes = array();
    private $_state = 'virgin';
    
    static private $_mapperinstance = array();

    const STATE_VIRGIN  = 'virgin';
    const STATE_INITIALISED = 'initialised';
    const STATE_CLEAN   = 'clean';
    const STATE_DIRTY   = 'dirty';

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }


    /**
     * Set a model property.
     *
     * @param string $name
     * @param mixed $value
     * @throws Mcmr_Model_Exception
     */
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Mcmr_Model_Exception('Invalid property');
        }
        $this->$method($value);
    }

    /**
     * Return a model property.
     *
     * @param string $name
     * @throws Mcmr_Model_Exception
     */
    public function __get($name)
    {
        $method = 'get' . ucfirst($name);
        if (! method_exists($this, $method)) {
            throw new Mcmr_Model_Exception('Invalid property '.$name);
        }
        return $this->$method();
    }

    public function __wakeup()
    {
        self::$_mapperinstance = array();
    }

    /**
     * 
     */
    public function __toString()
    {
        $method = 'getTitle';
        if (method_exists($this, $method)) {
            return $this->getTitle();
        } else {
            return '';
        }
    }
    
    /**
     * Takes a classname and returns the mapper object.
     *
     * @param string $mapperclass
     * @throws Mcmr_Model_Exception
     * @return Mcmr_Model_MapperAbstract
     */
    static function getMapper($mapperclass=null)
    {
        if (null===$mapperclass) {
            $mapperclass = static::$_mapperclass;
        }
        if (class_exists($mapperclass)) {
            return new $mapperclass();
        } else {
            throw new Mcmr_Model_Exception("Unknown Mapper class {$mapperclass}");
        }
    }

    /**
     * Get the state of the model which is one of the following.
     * 'virgin' - Default model state. No data
     * 'initialised' - Model has been populated but never saved to the database
     * 'clean' - Model has been populated from the database
     * 'dirty' - Model data has been modified but not saved to the database. (Warning. This is currently unreliable)
     *
     * @param string $state
     * @return string
     */
    public function state($state = null)
    {
        if (null !== $state) {
            $this->_state = $state;
        }

        return $this->_state;
    }

    /**
     * Sets model properties based on the array key/value pairs
     *
     * @param array $options
     * @return Mcmr_Model_ModelAbstract
     */
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            if (0 === strncmp('attr_', $key, 5)) {
                $this->setAttribute(str_replace('attr_', '', $key), $value);
            } else {
                $method = 'set' . ucfirst(str_replace('_', '', $key));
                if (in_array($method, $methods)) {
                    $this->$method($value);
                }
            }
        }

        switch ($this->_state) {
            case self::STATE_CLEAN:
                $this->state(self::STATE_DIRTY);
                break;

            case self::STATE_VIRGIN:
                $this->state(self::STATE_INITIALISED);
                break;
        }

        return $this;
    }

    /**
     * Return an array of all the model properties
     *
     * @return array
     */
    public function getOptions($includeattributes = false)
    {
        $return = array();
        
        $vars = get_object_vars($this);
        foreach ($vars as $name => $value) {
            $name = str_replace('_', '', $name);
            try {
                if ('attributes' !== $name && 'attributeTypes' !== $name) {
                    $return[$name] = $this->__get($name);
                }
            } catch (Mcmr_Model_Exception $e) {
                // We don't really care if there is a property without a 'get' function. Ignore exceptions
            }
        }

        if ($includeattributes) {
            $attributes = $this->getAttributes();
            foreach ($attributes as $key => $value) {
                $return['attr_'.$key] = $value;
            }
        }

        return $return;
    }

    /**
     * Set the model dynamic attribute values
     *
     * @param array $attributes
     * @return Mcmr_Model_ModelAbstract
     */
    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $name => $value) {
            $this->setAttribute($name, $value);
        }
        
        return $this;
    }

    /**
     * Return an array of attribute values
     * 
     * @return array
     */
    public function getAttributes()
    {
        return $this->_attributes;
    }

    /**
     * Return a single attribute value
     * 
     * @param string $name
     * @throws Mcmr_Model_Exception
     * @return string
     */
    public function getAttribute($name)
    {
        if (isset($this->_attributes[$name])) {
            return $this->_attributes[$name];
        } else {
            return null;
            //throw new Mcmr_Model_Exception("Unknown attribute: {$name}");
        }
    }

    /**
     * Set an attribute value
     *
     * @param string $name
     * @param string $value
     * @param string $type The classname this value should be returned as.
     * @return Mcmr_Model_ModelAbstract
     */
    public function setAttribute($name, $value, $type = null)
    {
        if (is_int($value)) {
            // This is to standardise values to ensure they are all saved the same for sql comparisons etc.
            // Was having problems with Intent events.
            $value = "$value";
        }

        $this->_attributes[$name] = $value;
        if (null !== $type) {
            $this->_attributeTypes[$name] = $type;
        }

        return $this;
    }

    /**
     * Return a single attribute type
     *
     * @param string $name
     * @throws Mcmr_Model_Exception
     * @return string
     */
    public function getAttributeType($name)
    {
        if (isset($this->_attributeTypes[$name])) {
            return $this->_attributeTypes[$name];
        } elseif (!isset($this->_attributes[$name])) {
            return null;
            throw new Mcmr_Model_Exception("Unknown attribute: {$name}");
        } else {
            return null;
        }
    }

    /**
     * Set the attribute Types
     * 
     * @param array $types
     * @return Mcmr_Model_ModelAbstract
     */
    public function setAttributeTypes(array $types)
    {
        $this->_attributeTypes = $types;
        
        return $this;
    }
    
    /**
     * Get all the attribute Types
     * @return array
     */
    public function getAttributeTypes()
    {
        return $this->_attributeTypes;
    }

    /**
     * Convert this model into an array
     * 
     * @return ArrayIterator
     */
    public function toArray()
    {
        $vars = get_object_vars($this);
        $values = array();
        foreach ($vars as $key => $value) {
            if ('_attributes' !== $key && '_attributetypes' !== $key) {
                $values[ltrim($key, '_')] = $value;
            }
        }

        $values = array_merge($values, $this->_attributes);
        
        return new ArrayIterator($values);
    }

    public function getControllerName()
    {
        $parts = explode('_', get_class($this));
        return strtolower($parts[2]);
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return null;
    }

    public function setTypeUrl($url) {
        $modelClass = get_class($this);
        $parts = explode('_', $modelClass);
        $parts[count($parts)-1] = 'Type';
        $typeClass = implode('_', $parts);
        $id = Mcmr_Model::idForUrl($typeClass, $url);
        if (method_exists($this, 'setTypeid')) {
            $this->setTypeid($id);
        } elseif(method_exists($this, 'setTypeId')) {
            $this->setTypeId($id);
        } else {
            throw new Exception( "$modelClass does not support type");
        }
    }

    /**
     * By default publosh date is the same as date. model doesn't have to have either.
     */
    public function getPublishdate()
    {
        return $this->getDate();
    }

    public function getConnectedModel($modelClass=false) 
    {
    	# attempt to auto-detect based on attributes
    	if(!$modelClass){
    		foreach(array('feature','job','product') as $part){
    			if($this->getAttribute($part.'Id')){
    				$modelClass = ucfirst($part);
    				$modelClass = $modelClass.'_Model_'.$modelClass;
    				break;
    			}
    		}
    		
    		if(!$modelClass)
    			return null;
    	}
    	
        $parts = explode('_', $modelClass);
        $shortName = strtolower($parts[count($parts)-1]);
        $id = $this->getAttribute($shortName.'Id');
        $mapper = $modelClass::getMapper();
        $model = $mapper->find($id);
        return $model; 
    }



}
