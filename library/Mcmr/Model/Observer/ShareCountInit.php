<?php
class Mcmr_Model_Observer_ShareCountInit extends Mcmr_Model_ObserverAbstract
{

    public function insert($model)
    {
    	$this->_initCount($model);
    }

    public function update($model)
    {
    	if (!$model->getAttribute('shareCountUpdate')) {
	    	$this->_initCount($model);
    	}
    }

    protected function _initCount($model) 
    {
		$model->setAttribute('shareCountUpdate', 1);
		$mapper = $model->getMapper();
        $this->setEnabled(false);
		$model->getMapper()->save($model);
        $this->setEnabled(true);
    }

}
