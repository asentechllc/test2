<?php
class Mcmr_Model_Observer_NewsArticle extends Mcmr_Model_ObserverAbstract
{

    protected $_articleMapper=null;

	/**
	 * Check if article has connected model and update as required
	 **/
    public function update(Mcmr_Model_ModelAbstract $article)
    {
    	$attr = $article->getAttributes();
    	foreach($attr as $k=>$v){
    		if(substr($k,-2) == 'Id'){
    			$class = ucfirst(substr($k,0,-2));
    			$class = $class.'_Model_'.$class;
    			if(class_exists($class)){
    				$fn = create_function('','return '.$class.'::getMapper();');
	    			$this->_updateModel($fn()->find($v),$article);
    			}
    		}
    	}
    }

    protected function _updateModel($model, $article) 
    {
    	if(is_object($model)){
			if(method_exists($model,'setPublished'))
				$model->setPublished($article->getPublished());
			
			$mapper = $model->getMapper();
			$mapper->setObserversEnabled(false);
			$mapper->save($model);
			$mapper->setObserversEnabled(true);
		}
    }
}
