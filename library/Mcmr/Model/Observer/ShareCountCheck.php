<?php
class Mcmr_Model_Observer_ShareCountCheck extends Mcmr_Model_ObserverAbstract
{

    public function load($model)
    {
        //we are only interested on a sinlge model load and only on it's own page
        //exclude all model lists
        if (!is_object($model)||'Mcmr_Paginator'==get_class($model)) {
            return;
        }
        //check if either the id or the url pased in the request matches that of the model
        //this will idneity 'load of article X on the poage of thast article'
        //rather than in a sidepanel of another page for example
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $reqId = $request->getParam('id');
        $reqUrl = $request->getParam('url');
        $modelId = method_exists($model,'getId')?$model->getId():null;
        $modelUrl = method_exists($model, 'getUrl')?$model->getUrl():null;
        if ($reqId != $modelId && $reqUrl != $modelUrl) {
            return;
        }

        $publishDate = $this->findPublishDate($model);
        if (!$publishDate) {
            return;
        }
        $now = time();
        $modelAge = $now - $publishDate;
        $shareCountUpdate = $model->getAttribute('shareCountUpdate');
        $shareCountsAge = $now - $shareCountUpdate;
        if (!$model->getAttribute('shareCountFlag') && $this->modelNeedsUpdate($modelAge, $shareCountsAge)) {
            $model->setAttribute('shareCountFlag', 1);
            $mapper = $model->getMapper();
            $this->setEnabled(false);
            $model->getMapper()->save($model);
            $this->setEnabled(true);
        }
    }

    public function modelNeedsUpdate($modelAge, $shareCountsAge) 
    {
            //if model is less than day old, refresh every hour
        if (($modelAge < 86400 && $shareCountsAge > 3600 ) ||
            //if model is less than a week old, refresh every 6 hours
            ($modelAge < 86400*7 && $shareCountsAge > 3600 * 6 ) ||
            //if model is less than a month old, refresh every day
            ($modelAge < 86400*28 && $shareCountsAge > 86400 ) ||
            //finally if the count age is more than a month, refresh
            ($shareCountsAge > 86400 )) {
            return true;
        }
        return false;
    }

    public function findPublishDate($model)
    {
        $publishDate = null;
        if (method_exists($model, 'getPublishdate')) {
            $publishDate = $model->getPublishdate();
        } elseif (method_exists($model, 'getCreatedate')) {
            $publishDate = $model->getCreatedate();
        } elseif  (method_exists($model, 'getDate')) {
            $publishDate = $model->getDate();
        }
        return $publishDate;
    }

}
