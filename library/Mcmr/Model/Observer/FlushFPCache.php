<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Cache.php 2108 2011-01-25 11:56:45Z leigh $
 */

/**
 * Observer that flushes the Website full page cache depending on certain model updates
 * This should only be enabled via the admin, nothing will happen if triggered from the Website
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage Observer
 */
class Mcmr_Model_Observer_FlushFPCache extends Mcmr_Model_ObserverAbstract
{

	private $_oldModel;

    /**
     *
     * @param type $user 
     */
    public function preUpdate($model)
    {
        $this->_oldModel = $model;
    }

    /**
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function insert(Mcmr_Model_ModelAbstract $model)
    {

    	if($this->_skipFlush($model))
    		return;
    
    	# if model is published - flush
    	if($model->getPublished()){
	    	Mcmr_Cache::flushWeb();
	    }
    }

    /**
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function update(Mcmr_Model_ModelAbstract $model)
    {
    
    	if($this->_skipFlush($model))
    		return;
    
    	# if model is published OR was published - flush
    	if($model->getPublished() || $this->_oldModel->getPublished()){
	    	Mcmr_Cache::flushWeb();
	    }
    }

    /**
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
    	Mcmr_Cache::flushWeb();
    }

    /**
     * Determine is cache flush should be skipped
     * When certain models have associated News Articles the article will cause a flush
     * @param Mcmr_Model_ModelAbstract $model
     */
	private function _skipFlush(Mcmr_Model_ModelAbstract $model){

		# if not a post request -- skip flush
        $request = Zend_Controller_Front::getInstance()->getRequest();
		if(!$request->isPost())
			return true;

		# model is in stream (news article observer will flush)
		if(!($model instanceof News_Model_Article)){
			return $model->getAttribute('stream') ? true : false;
		}
		
		return false;
	}
	    

}
