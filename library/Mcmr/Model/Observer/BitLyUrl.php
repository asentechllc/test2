<?php
class Mcmr_Model_Observer_BitLyUrl extends Mcmr_Model_ObserverAbstract
{
	protected $_bitLyService = null;


    public function insert($model)
    {
        if (!is_object($model)||get_class($model)=='Mcmr_Paginator') {
            return;
        }
    	$this->_createBitLyUrl($model);
    }

    public function update($model)
    {
        if (!is_object($model)||get_class($model)=='Mcmr_Paginator') {
            return;
        }
    	if (!$model->getAttribute('bitLyUrl')) {
	    	$this->_createBitLyUrl($model);
    	}
    }

    public function load($model)
    {
        if (!is_object($model)||get_class($model)=='Mcmr_Paginator') {
            return;
        }
        if (!$model->getAttribute('bitLyUrl')) {
            $this->_createBitLyUrl($model);
        }
    }

    protected function _createBitLyUrl($model) 
    {
    	if($model->getTitle()){
			$url = $this->_getAbsoluteUrl($model);
			$bitLy = $this->_getBitLyService();
			$short = $bitLy->shorten($url); 
			$model->setAttribute('bitLyUrl', $short);
			$mapper = $model->getMapper();
			$this->setEnabled(false);
			$model->getMapper()->save($model);
			$this->setEnabled(true);
		}
    }

    protected function _getAbsoluteUrl($model) {
        $baseDomain = Zend_Registry::get('app-config')->media->baseDomain;
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $relativeUrl = $view->defaultReadUrl($model);
        if ('/'==substr($baseDomain,-1)&&'/'==substr($relativeUrl,0,1)) {
            $baseDomain = substr($baseDomain, 0, -1);
        }
        $url = $baseDomain.$relativeUrl;
        return $url;
    }

    protected function _getBitLyService() {
    	if (null==$this->_bitLyService) {
    		$config = Zend_Registry::get('default-config');
    		$accessToken = $config->shorturl->bitly->accessToken;
			$this->_bitLyService = new Mcmr_Service_ShortUrl_BitLy($accessToken);
    	}
    	return $this->_bitLyService;
    }
}
