<?php

class Mcmr_Model_Observer_QueueTest extends Mcmr_Model_ObserverAbstract 
    implements Mcmr_Model_Observer_QueueableInterface
{
    public function update($model)
    {
        Mcmr_Debug::dump("Observer executed", Zend_Log::ALERT, 'file');
        var_dump('observer executed');
    }
}
