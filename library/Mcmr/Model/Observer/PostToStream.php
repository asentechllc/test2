<?php
class Mcmr_Model_Observer_PostToStream extends Mcmr_Model_ObserverAbstract
{

    protected $_articleMapper=null;


    public function insert(Mcmr_Model_ModelAbstract $model)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        if (!$request->isPost() || !$model->getAttribute('stream')) {
            return;
        }
        $modelMapper = $model->getMapper();
        $article = $this->_createArticle($model);
        $this->_updateArticle($article, $model);
        $this->_getArticleMapper()->save($article);
        $this->_updateModel($model, $article);
    }

    public function update(Mcmr_Model_ModelAbstract $model)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        if (!$request->isPost() || $model->getAttribute('stream') == null) {
            return;
        }

        $modelMapper = $model->getMapper();
		if (null===($article=$this->_findArticle($model))) {
			$article = $this->_createArticle($model);
		}
        
        if($article){
			$this->_updateArticle($article, $model);
	        $this->_getArticleMapper()->save($article);
    	    $this->_updateModel($model, $article);
    	}
    }
    
    public function delete(Mcmr_Model_ModelAbstract $model){
    	$article = $this->_findArticle($model);
        if ($article) {
    	   $article->getMapper()->delete($article);
        }
    }

    protected function _getArticleMapper() 
    {
        if (null===$this->_articleMapper) {
            $this->_articleMapper = News_Model_Article::getMapper();
        }
        return $this->_articleMapper;
    }

    protected function _createArticle($model) 
    {
        $class = get_class($model);
        $parts = explode('_', $class);
        $modelName = strtolower($parts[2]);
        $article = new News_Model_Article();
        $article->setTypeUrl($this->_getArticleType($model));
        $article->setStream($model->getPublished());
        $article->setAttribute($modelName.'Id', $model->getId());
        return $article;
    }

    protected function _getArticleType($model) 
    {
        $class = get_class($model);
        $parts = explode('_', $class);
        $articleTypeUrl = strtolower($parts[2]);
        if (method_exists($model, 'getType')) {
            $type = $model->getType();
            if ($type && is_object($type) && method_exists($type, 'getUrl')) {
                $typeUrl = $type->getUrl();
                $articleTypeUrl.='-'.$typeUrl;
            }
        }
        return $articleTypeUrl;
    }

    protected function _findArticle($model) 
    {
        $class = get_class($model);
        $parts = explode('_', $class);
        $modelName = strtolower($parts[2]);
        $article = $this->_getArticleMapper()->findOneByField(array("attr_{$modelName}Id"=>$model->getId()));
        return $article;
    }

    protected function _updateArticle($article, $model)
    {
        $article->setTitle($model->getTitle());
        $article->setPublished($model->getPublished()&&$model->getAttribute('stream'));
        $article->setStream($model->getAttribute('stream'));
        $article->setPublishdate($model->getPublishdate());
        $article->setAttribute('keyword', $model->getAttribute('keyword'));
    }

    protected function _updateModel($model, $article) 
    {
        $model->setAttribute('articleId', $article->getId());
        // Disable the observers
        $this->setEnabled(false);
        $model->getMapper()->save($model);
        $this->setEnabled(true);
    }

}
