<?php

/**
 * Simple extension to allow Features etc. to have News Articles created regardless of
 * stream flag so that they can appear alongside other posts for sectioned content
 *
 * Only creates article if section_id is populated (to avoid routing issues)
 **/


class Mcmr_Model_Observer_PostToStreamSectioned extends Mcmr_Model_Observer_PostToStream
{

	private $_postedToStream = null;

    public function insert(Mcmr_Model_ModelAbstract $model)
    {
    	# don't create article if there is no section ID!
    	if(!$model->getSectionid())
    		return;
    		
    	$this->_postedToStream = $model->getAttribute('stream');

    	parent::insert($model);
    }

    public function update(Mcmr_Model_ModelAbstract $model)
    {
    	# don't create article if there is no section ID!
    	if(!$model->getSectionid())
    		return;
    		
    	$this->_postedToStream = $model->getAttribute('stream');
    	$model->setAttribute('stream',1);
    	
    	parent::update($model);
    }

    protected function _updateArticle($article, $model)
    {
    	$model->setAttribute('stream',$this->_postedToStream);
    	parent::_updateArticle($article,$model);
        $article->setPublished($model->getPublished());
    	$article->setImage($model->getImage());
        $article->setSectionid($model->getSectionid());
    }
    
}
