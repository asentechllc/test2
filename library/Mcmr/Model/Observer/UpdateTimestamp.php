<?php
class Mcmr_Model_Observer_UpdateTimestamp extends Mcmr_Model_ObserverAbstract
{

    protected $_articleMapper=null;

    public function update(Mcmr_Model_ModelAbstract $model)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        if (!$request->isPost()) {
            return;
        }
        $this->_updateModel($model);
    }
    
    protected function _getArticleMapper() 
    {
        if (null===$this->_articleMapper) {
            $this->_articleMapper = News_Model_Article::getMapper();
        }
        return $this->_articleMapper;
    }

    protected function _updateModel($model) 
    {
        $model->setAttribute('updateDate', time());
        // Disable the observers
        $this->setEnabled(false);
        $model->getMapper()->save($model);
        $this->setEnabled(true);
    }

}
