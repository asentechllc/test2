<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Changelog.php 1376 2010-09-13 13:06:27Z leigh $
 */

/**
 * An observer to makes sure you are still authenticated if you update your email or whatever the username field is.
 * When initiating the request, Acl plugin takes the Zend_auth identity, which is the username field of the user,
 * and then looks up this user in the db and checks his permission.
 * If you update the usenrame field in your account, this look up fails and you are logged out or show access denied.
 * We need to force authenticate user with the new identity if this happens.
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage Observer
 */
class Mcmr_Model_Observer_UsernameUpdate extends Mcmr_Model_ObserverAbstract
{
    static $originalUsername = null;

    /**
     * On model deletion delete all the stats for that model
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function preUpdate($user)
    {
        //we only save the username for later update if it's of the current authenticated user that is beign updated
        $username = $user->getUsernameFromField();
        $authenticatedUser = Zend_Registry::get('authenticated-user');
        if (null!==$authenticatedUser && 'userguest'!=$authenticatedUser->getRole() && $username==$authenticatedUser->getUsernameFromField()) {
            self::$originalUsername = $username;
        }

    }

    public function update($user)
    {
        $newUsername = $user->getUsernameFromField();
        if (null!==self::$originalUsername && self::$originalUsername!=$newUsername) {
            Zend_Auth::getInstance()->getStorage()->write($newUsername);
        }

    }
}
