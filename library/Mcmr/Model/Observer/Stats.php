<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Changelog.php 1376 2010-09-13 13:06:27Z leigh $
 */

/**
 * An observer to remove stats for models that are deleted
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage Observer
 */
class Mcmr_Model_Observer_Stats extends Mcmr_Model_ObserverAbstract
{
    /**
     * On model deletion delete all the stats for that model
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function delete($model)
    {
        $mapper = Default_Model_Stat::getMapper();
        $mapper->deleteAll($model);
    }
}
