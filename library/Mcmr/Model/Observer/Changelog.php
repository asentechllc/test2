<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Changelog.php 1491 2010-10-06 16:40:10Z leigh $
 */

/**
 * Record all system actions in the system changelog
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage Observer
 */
class Mcmr_Model_Observer_Changelog extends Mcmr_Model_ObserverAbstract
{
    private $_record = null;
    private $_config = null;

    public function __construct()
    {
        try {
            $this->_config = Zend_Registry::get('changelog-config');
        } catch (Exception $e) {
        }
    }

    /**
     * Record a model insert
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function insert(Mcmr_Model_ModelAbstract $model)
    {
        if ($this->_logModel($model)) {
            $this->_record = new Changelog_Model_Record();
            $this->_record->setAction('insert');
            $this->_saveRecord($model);
        }
    }

    /**
     * Record a model update
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function update(Mcmr_Model_ModelAbstract $model)
    {
        if ($this->_logModel($model)) {
            $this->_record = new Changelog_Model_Record();
            $this->_record->setAction('update');
            $this->_saveRecord($model);
        }
    }
    
    /**
     * Record a model delete
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        if ($this->_logModel($model)) {
            $this->_record = new Changelog_Model_Record();
            $this->_record->setAction('delete');
            $this->_saveRecord($model);
        }
    }

    /**
     * Save the record to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    private function _saveRecord(Mcmr_Model_ModelAbstract $model)
    {
        $user = Zend_Registry::get('authenticated-user');
        if (null !== $user) {
            $this->_record->setUserid($user->getId());
        }
        $this->_record->setType(get_class($model))
                ->setItemid($model->getId())
                ->setDate(time());

        $mapper = Changelog_Model_Record::getMapper();
        $mapper->save($this->_record);
    }
    
    /**
     * Check that the we should be logging this model change
     * 
     * @param $model
     * @return unknown_type
     */
    private function _logModel($model)
    {
        if (null !== $this->_config && isset($this->_config->logModels)) {
            return (in_array(get_class($model), $this->_config->logModels->toArray()) && $model->getId());
        } else {
            // If there is no config don't log anything
            return false;
        }
    }
}
