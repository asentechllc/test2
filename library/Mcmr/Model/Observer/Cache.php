<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Cache.php 2108 2011-01-25 11:56:45Z leigh $
 */

/**
 * A Mapper Observer that cleans the cache based on tags generated from the model object name
 * Any cache item taged by model class name prepended by '_index' and '_id_<id>' will be cleaned
 *
 * eg. User_Model_User update will clean tags 'user_model_user_index' and 'user_model_user_id_1'
 * with 1 being the model primary key
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage Observer
 */
class Mcmr_Model_Observer_Cache extends Mcmr_Model_ObserverAbstract
{
    private $_cache = null;
    private static $_tags = array();

    public function  __construct()
    {
        $this->_cache = Zend_Registry::get('cachemanager')->getCache('database');
    }

    /**
     * Return an array of tags used by this page
     *
     * @return array
     */
    public static function getTags()
    {
        return array_keys(self::$_tags);
    }

    public function load($object)
    {
        if (is_object($object)) {
            $tagprefix = strtolower(get_class($object));
            if ($object instanceof Mcmr_Model_ModelAbstract) {
                    self::$_tags[$tagprefix.'_id_'.$object->getId()] = true;
            } else {
                    self::$_tags[$tagprefix.'_index'] = true;
            }
        }
    }

    /**
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function insert(Mcmr_Model_ModelAbstract $model)
    {
        // Clean the object cache
        $tagprefix = strtolower(get_class($model));
        $tags = array(
                $tagprefix.'_index',
        );

        $this->_cleanCache($tags);
    }

    /**
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function update(Mcmr_Model_ModelAbstract $model)
    {
        // Clean the object cache
        $tagprefix = strtolower(get_class($model));
        $tags = array(
                $tagprefix.'_index',
                $tagprefix.'_id_'.$model->getId(),
        );

        $this->_cleanCache($tags);
    }

    /**
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        // Clean the object cache
        $tagprefix = strtolower(get_class($model));
        $tags = array(
            $tagprefix.'_index',
            $tagprefix.'_id_'.$model->getId(),
        );

        $this->_cleanCache($tags);
    }

    /**
     *
     * @param array $tags
     */
    private function _cleanCache($tags)
    {
        if (null != $this->_cache) {
            $this->_cache->clean(Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG, $tags);
        }
    }
    
    /**
     * Get the number of seconds the slave database server is behind the master.
     * This time is used to delay the tag expiry to ensure fresh data is read.
     * 
     * @return int
     */
    private function _getSlaveDelay()
    {
        $delay = null;
        
        $slave = Mcmr_Db_Table_Abstract::getDefaultReadAdapter();
        if (null !== $slave) {
            try {
                // @TODO This is Mysql specific at the moment. Need to move this 
                // into the adapter somehow
                $stmt = $slave->query("SHOW SLAVE STATUS");
                $status = $stmt->fetch(Zend_Db::FETCH_ASSOC);

                if (is_array($status) && array_key_exists('Seconds_Behind_Master', $status)) {
                    $delay = intval($status['Seconds_Behind_Master']);
                }
            } catch (Zend_Db_Statement_Exception $e) {
            }
        }
        
        return $delay;
    }
}
