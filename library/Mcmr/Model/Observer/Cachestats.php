<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Cachestats.php 1460 2010-09-29 17:06:34Z leigh $
 */

/**
 * Description of Stats
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage Observer
 */
class Mcmr_Model_Observer_Cachestats extends Mcmr_Model_ObserverAbstract
{
    static $objectLoads = 0;
    static $cacheMiss = 0;

    /**
     * Observer all model loads
     *
     * @param Mcmr_Model_ModelAbstract|Zend_Paginator $object
     */
    function load($object)
    {
        self::$objectLoads++;
    }

    /**
     * Observe cache misses
     *
     * @param Mcmr_Model_ModelAbstract|Zend_Paginator $object
     */
    function cacheMiss($object)
    {
        self::$cacheMiss++;
    }
}
