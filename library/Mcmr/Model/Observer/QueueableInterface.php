<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ObserverAbstract.php 2116 2011-01-26 15:52:30Z michal $
 */

/**
 * Interface that makes an observer a queueable object.
 *
 * @category Mcmr
 * @package Mcmr_Model
 * @subpackage Observer
 */
interface Mcmr_Model_Observer_QueueableInterface
{
}
