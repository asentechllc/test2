<?php
class Mcmr_Model_Observer_Follow extends Mcmr_Model_ObserverAbstract
{
    /**
     * Clear the updates for the user following this model
     *
     * @param Mcmr_Model_ModelAbstract $model 
     */
    public function read(Mcmr_Model_ModelAbstract $model)
    {
        $user = Zend_Registry::get('authenticated-user');
        if (null !== $user) {
            $mapper = Changelog_Model_Follow::getMapper();
            $mapper->resetUpdates($user->getId(), get_class($model), $model->getId());
        }
    }

    /**
     * Register an event for this model
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function update(Mcmr_Model_ModelAbstract $model)
    {
//        $this->_registerUpdate($model);
    }

    /**
     * Register an event for this model
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function insert(Mcmr_Model_ModelAbstract $model)
    {
        $this->_registerUpdate($model);
    }

    private function _registerUpdate($model)
    {
        $mapper = Changelog_Model_Follow::getMapper();
//        $mapper->incrementUpdates(get_class($model), $model->getId());

        // This is nasty. But don't have time to work out the 'proper' way to do this.
        // It works dammit! Don't look at me!! :'(
        if ($model instanceof Comment_Model_Comment) {
            $thread = $model->getThread();
            $mapper->incrementUpdates(get_class($thread), $thread->getId());
        }

        return $this;
    }
}
