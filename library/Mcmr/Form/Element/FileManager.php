<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @copyright Copyright (c) 2009 Soflomo V.O.F. (http://www.soflomo.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 */

/**
 * Extension for choosing files/directories using elFinder
 *
 * @category   Mcmr
 * @package    Mcmr_Form
 * @subpackage Element
 */
class Mcmr_Form_Element_FileManager extends Zend_Form_Element_Text
{
    /**
     * Use formTextarea view helper by default
     * @var string
     */
    public $helper = 'formFileManager';
}