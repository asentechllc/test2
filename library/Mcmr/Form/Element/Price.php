<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Date.php 1632 2010-10-25 12:05:59Z leigh $
 */

/**
 * A Date form element
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Element
 */
class Mcmr_Form_Element_Price extends Zend_Form_Element_Xhtml
{
    /**
     * The major currency amount (eg Pounds, Dollars)
     *
     * @var int
     */
    protected $_major = null;

    /**
     * The minor currency amount (eg Pence, Cents)
     *
     * @var int
     */
    protected $_minor = null;

    /**
     * Load default decorators
     *
     * @return Zend_Form_Element
     */
    public function loadDefaultDecorators()
    {
        if ($this->loadDefaultDecoratorsIsDisabled()) {
            return;
        }

        $decorators = $this->getDecorators();
        if (empty($decorators)) {
            $this->addDecorator('Price')
                ->addDecorator('Errors')
                ->addDecorator(
                    'Description', array(
                        'tag' => 'p',
                        'class' => 'description'
                    )
                )
                ->addDecorator(
                    'HtmlTag', array(
                        'tag' => 'dd',
                        'id'  => $this->getName() . '-element'
                    )
                )
                ->addDecorator('Label', array('tag' => 'dt'));
        }
    }

    /**
     * Set the major currency value of the price (eg Pounds/Pence)
     *
     * @param int $major
     * @return Mcmr_Form_Element_Price
     */
    public function setMajor($major)
    {
        $this->_major = (int)$major;

        return $this;
    }

    /**
     * Get the major currency value of the price (eg Pounds/Pence)
     *
     * @return int
     */
    public function getMajor()
    {
        if (null === $this->_major) {
            $this->_major = 0;
        }

        return $this->_major;
    }

    /**
     * Set the minor currency value of the price (eg Pence/Cents)
     *
     * @param int $minor
     * @return Mcmr_Form_Element_Price
     */
    public function setMinor($minor)
    {
        $this->_minor = (int)$minor;

        return $this;
    }

    /**
     * Get the moinor currency value of the price (eg Pence/Cents)
     *
     * @return int
     */
    public function getMinor()
    {
        if (null === $this->_minor) {
            $this->_minor = 0;
        }

        return $this->_minor;
    }

    /**
     * Set the value. Value is set in the minor currency value (eg set in Pence/Cents)
     *
     * @param int $value
     * @return Mcmr_Form_Element_Price
     */
    public function setValue($value)
    {
        if (is_array($value)) {
            $major = $value['major'];
            $minor = $value['minor'];
        } elseif (is_numeric($value)) {
            $value = (int)$value;

            $major = floor($value/100);
            $minor = $value % 100;
        }
        
        $this->setMajor($major);
        $this->setMinor($minor);

        return $this;
    }

    /**
     * Get the total value in the minor currency value. EG $14 is returned as 1400
     *
     * @return int
     */
    public function getValue()
    {
        return $this->getMajor() * 100 + $this->getMinor();
    }
}
