<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ImageManager.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * ImageManager form element
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Element
 */
class Mcmr_Form_Element_ImageManager extends Zend_Form_Element_Text
{
    /**
     * Use formTextarea view helper by default
     * @var string
     */
    public $helper = 'formImageManager';
}