<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Date.php 2165 2011-02-08 12:09:36Z michal $
 */

/**
 * A Date form element
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Element
 */
class Mcmr_Form_Element_Date extends Zend_Form_Element_Xhtml
{
    /**
     *
     * @var Zend_Date
     */
    protected $_date = null;
    
    protected $_mainDecorator = null;

    /**
     * Load default decorators
     *
     * @return Zend_Form_Element
     */
    public function loadDefaultDecorators()
    {
        if ($this->loadDefaultDecoratorsIsDisabled()) {
            return;
        }
        $decorators = $this->getDecorators();
        if (empty($decorators)) {
            $this->addDecorator($this->getMainDecorator())
                ->addDecorator('Errors')
                ->addDecorator(
                    'Description', array(
                        'tag' => 'p',
                        'class' => 'description'
                    )
                )
                ->addDecorator(
                    'HtmlTag', array(
                        'tag' => 'dd',
                        'id'  => $this->getName() . '-element'
                    )
                )
                ->addDecorator('Label', array('tag' => 'dt'));
        }
        
        return $this;
    }
    
    /**
     * Return the date time minutes
     *
     * @return int
     */
    public function getMinutes()
    {
        return $this->getDate()->toString(Zend_Date::MINUTE);
    }
    
    /**
     * Set the element's minute value
     *
     * @param type $minutes
     * @return Mcmr_Form_Element_Date 
     */
    public function setMinutes($minutes)
    {
        $this->getDate()->setMinute($minutes);
        
        return $this;
    }
    
    /**
     * Get the date time hour value
     *
     * @return int 
     */
    public function getHour()
    {
        return $this->getDate()->toString(Zend_Date::HOUR);
    }
    
    /**
     * Set the date element hour value
     *
     * @param int $hour
     * @return Mcmr_Form_Element_Date 
     */
    public function setHour($hour)
    {
        $this->getDate()->setHour($hour);
        
        return $this;
    }
    
    /**
     * Get the day of the month
     *
     * @return int
     */
    public function getDay()
    {
        return $this->getDate()->toString(Zend_Date::DAY);
    }

    /**
     * Set the day of the month value
     *
     * @param int $day
     * @return Mcmr_Form_Element_Date 
     */
    public function setDay($day)
    {
        $this->getDate()->setDay($day);
        
        return $this;
    }

    /**
     * Get the month value
     *
     * @return int
     */
    public function getMonth()
    {
        return $this->getDate()->toString(Zend_Date::MONTH);
    }

    /**
     * Set the month value
     *
     * @param int $month
     * @return Mcmr_Form_Element_Date 
     */
    public function setMonth($month)
    {
        $this->getDate()->setMonth($month);

        return $this;
    }

    /**
     * Get the year value
     *
     * @return int
     */
    public function getYear()
    {
        return $this->getDate()->toString(Zend_Date::YEAR);
    }

    /**
     * Set the year value
     *
     * @param int $year
     * @return Mcmr_Form_Element_Date 
     */
    public function setYear($year)
    {
        $this->getDate()->setYear($year);

        return $this;
    }
    
    public function getMainDecorator() 
    {
        if ( null === $this->_mainDecorator ) {
            $this->_mainDecorator = 'Date';
        }
        return $this->_mainDecorator;
    }
    
    public function setMainDecorator( $deco ) 
    {
        $this->_mainDecorator = $deco;
        return $this;
    }

    /**
     * Set the element's value using a unix timestamp
     *
     * @param mixed $value
     * @return Mcmr_Form_Element_Date 
     */
    public function setValue($value)
    {
        $this->_date = Mcmr_StdLib::timeToDate($value);
        
        return $this;
    }

    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = new Mcmr_Date();
        }
        
        return $this->_date;
    }
    
    /**
     * Get the element's value as a unit timestamp
     *
     * @return int
     */
    public function getValue()
    {
        return $this->getDate()->getTimestamp();
    }
}
