<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ImageManager.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * ImageManager form element
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Element
 */
class Mcmr_Form_Element_Hash extends Zend_Form_Element_Hash
{

	private $_sessionSuffix = null;
	private $_enableSessionSuffix = true;

    public function __construct($spec, $options = null)
    {
    	
    	# generate a unique session suffix for this form
    	if(!$this->_sessionSuffix){
    		$this->_sessionSuffix = uniqid('');
    	}

        parent::__construct($spec, $options);
    }

    public function initCsrfValidator()
    {
        parent::initCsrfValidator();
        $v = $this->getValidator('Identical');
        $v->setMessages(
            array(
                Zend_Validate_Identical::NOT_SAME
                    => 'Sorry, for security reasons we cannot complete this action. Please reload the page.',
                Zend_Validate_Identical::MISSING_TOKEN
                    => 'Sorry, for security reasons we cannot complete this action. Please reload the page.',
            )
        );

        return $this;
    }
    public function getSessionName()
    {
    	if(!$this->_enableSessionSuffix)
    		return parent::getSessionName();
    	
        return __CLASS__ . '_' . $this->getSalt() . '_' . $this->getName() . $this->_sessionSuffix;
    }
    
    public function setValue($value){
    
    	if($this->_enableSessionSuffix){
			# do we have a session suffix being passed in?
			# if so split out the token and suffix and recreate the validator
			if(strlen($value) > 32 && count($_value = explode(',',base64_decode($value))) == 2){
				$this->_hash = $_value[0];
				$this->_sessionSuffix = $_value[1];
				$this->_session = null;
			
				# re-init validator based on new session name
				$this->clearValidators();   		
				$this->initCsrfValidator();
			
				# set raw value of token for validator comparison
				$value = $_value[0];
			}
		
			# otherwise we need to add the suffix in
			else{
				$value = base64_encode($value.','.$this->_sessionSuffix);
			}
		}
    	
    	parent::setValue($value);
    }

}