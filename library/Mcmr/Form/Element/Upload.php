<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Date.php 2165 2011-02-08 12:09:36Z michal $
 */

/**
 * An Upload form element. Works the same as File except it can store the existing value
 * and won't force a re-upload when the value is required.
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Element
 */
class Mcmr_Form_Element_Upload extends Zend_Form_Element_File
{
    protected $_urlpath = null;
    protected $_locked = false;
    protected $_sessionKey = null;

    /**
     * @var string Default view helper
     */
    public $helper = 'formUpload';

    public function render(Zend_View_Interface $view = null)
    {
        if ($this->_isPartialRendering) {
            return '';
        }

        if (null !== $view) {
            $this->setView($view);
        }

        $content = '';
        foreach ($this->getDecorators() as $decorator) {
            $decorator->setElement($this);
            $content = $decorator->render($content);
        }
        return $content;
    }
    
    /**
     * Processes the file, returns null or the filename only
     * For the complete path, use getFileName
     *
     * @return null|string
     */
    public function getValue()
    {
        $value =  parent::getValue();
        $delete = (bool)$this->getView()->request()->getParam($this->getName().'_delete');

        if (!empty($value) && null !== $this->getUrlpath() && (false === strpos($value, $this->getUrlpath()))) {
            $value = $this->getUrlpath() . DIRECTORY_SEPARATOR . $value;
        }

        // If there is no value attempt to get it out of the session.
        $key = $this->getSessionKey();
        $session = new Zend_Session_Namespace($this->getName());
        if (empty($session->existingValue) || !is_array($session->existingValue)) {
            $session->existingValue = array();
        }

        if (isset($session->existingValue[$key])) {
            if (empty($value) && !$delete) {
                $value = $session->existingValue[$key];
                unset($session->existingValue[$key]);
            } elseif ((($value !== $session->existingValue[$key]) || $delete) && !$this->getLocked()) {
                // There is a value. If it is different from the old one, delete the old file
                $originalFile = $this->getDestination() . DIRECTORY_SEPARATOR .
                        pathinfo($session->existingValue[$key], PATHINFO_FILENAME) .'.'.
                        pathinfo($session->existingValue[$key], PATHINFO_EXTENSION);
                if (is_file($originalFile)) {
                    unlink($originalFile);
                }
            }
        }

        // Set the existing value to the new value
        $session->existingValue[$key] = $value;

        return $value;
    }

    /**
     * Validate upload
     *
     * @param  string $value   File, can be optional, give null to validate all files
     * @param  mixed  $context
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        $valid = parent::isValid($value, $context);
        
        // File upload doesn't like ajax submission. Hack around this to make the value valid if this was submitted
        // by ajax
        if (!$valid) {
            $errors = $this->getErrors();
            if ((1 === count($errors)
                    && Zend_Validate_File_Upload::INI_SIZE === $errors[0]) || (0 === count($errors))) {
                $front = Zend_Controller_Front::getInstance();
                $request = $front->getRequest();
                if ($request->isXmlHttpRequest()) {
                    $valid = true;
                }
            }
        }

        return $valid;
    }

    /**
     * Get the session key that is holding the existing value
     *
     * @return type 
     */
    public function getSessionKey()
    {
        if (null === $this->_sessionKey) {
            // Attempt to get the key from the request. Default to a random string
            $this->_sessionKey = $this->getView()->request()
                ->getParam($this->getName().'_value', Mcmr_StdLib::randomString());
        }

        return $this->_sessionKey;
    }

    /**
     * Set the existing value.
     *
     * @param string $value
     * @return Mcmr_Form_Element_Upload 
     */
    public function setValue($value)
    {
        // Store the value in the session.
        $key = $this->getSessionKey();
        $session = new Zend_Session_Namespace($this->getName());
        if (empty($session->existingValue)) {
            $session->existingValue = array();
        }
        $session->existingValue[$key] = $value;
        
        return $this;
    }

    /**
     * Set the url path for this upload
     *
     * @param string $path
     * @return Mcmr_Form_Element_Upload 
     */
    public function setUrlpath($path)
    {
        $this->_urlpath = $path;

        return $this;
    }

    /**
     * Get the URL path for this file.
     *
     * @return string 
     */
    public function getUrlpath()
    {
        return $this->_urlpath;
    }

    /**
     * Get the locked flag. Determin if the file is locked on the filesystem preventing
     * it from being deleted
     *
     * @return type 
     */
    public function getLocked()
    {
        return $this->_locked;
    }

    /**
     * If this element is locked the old file will not be deleted on edit.
     *
     * @param bool $locked
     * @return Mcmr_Form_Element_Upload
     */
    public function setLocked($locked)
    {
        $this->_locked = $locked;

        return $this;
    }


}
