<?php

class Mcmr_Form_Element_DateTypeSelect extends Zend_Form_Element_Select
{
    protected $_targetElement;

    public function init()
    {
        parent::init();
        $this->setMultiOptions(
            array(
                    'date' => 'Specific',
                    'month' => 'Month',
                    'quarter' => 'Quarter',
                    'year' => 'Year'
            )
        );
    }

    public function setTargetElement($val)
    {
        $this->_targetElement = $val;
        return $this;
    }

    public function getTargetElementQualifiedName()
    {
        $name = $this->getTargetElement();
        if (null !== ($belongsTo = $this->getBelongsTo())) {
            $name = $belongsTo . '-' . $name;
        }
        $name = str_replace('[', '-', $name);
        $name = str_replace(']', '', $name);
        return $name;
    }

    public function getTargetElement()
    {
        return $this->_targetElement;
    }

}
