<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Date.php 1886 2010-12-01 11:34:38Z michal $
 */

/**
 * Decorator class for building a date field
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Decorator
 */
class Mcmr_Form_Decorator_PeriodDate extends Zend_Form_Decorator_Abstract
{

    /**
     * Decorate content and/or element
     *
     * @param  string $content
     * @return string
     * @throws Zend_Form_Decorator_Exception when unimplemented
     */
    public function render($content)
    {
        $element = $this->getElement();
        if (!$element instanceof Mcmr_Form_Element_Date) {
            // only want to render Date elements
            return $content;
        }

        $view = $element->getView();
        if (!$view instanceof Zend_View_Interface) {
            // using view helpers, so do nothing if no view present
            return $content;
        }

        if ($element->getValue() > 0) {
            $day = $element->getDay();
            $month = $element->getMonth();
            $year = $element->getYear();
        } else {
            $day = '';
            $month = '';
            $year = '';
        }
        $name = $element->getFullyQualifiedName();
        $hourClass = $element->getAttrib('hourClass');
        $minuteClass = $element->getAttrib('minuteClass');
        $dayClass = $element->getAttrib('dayClass');
        $monthClass = $element->getAttrib('monthClass');
        $yearClass = $element->getAttrib('yearClass');

        $hourParams = array(
                'size' => 2,
                'maxlength' => 2,
                'class' => $hourClass,
        );
        $minuteParams = array(
                'size' => 2,
                'maxlength' => 2,
                'class' => $minuteClass,
        );


        $dayParams = array(
                'size' => 2,
                'maxlength' => 2,
                'class' => $dayClass,
        );
        $monthParams = array(
                'size' => 2,
                'maxlength' => 2,
                'class' => $monthClass,
        );
        $yearParams = array(
                'size' => 4,
                'maxlength' => 4,
                'class' => $yearClass,
        );

        $markup = "<div class='date_type_entry date_entry'>"
                . $view->formHidden($name . '[minutes]', '0', $minuteParams)
                . $view->formHidden($name . '[hour]', '7', $hourParams)
                . $view->formText($name . '[day]', $day, $dayParams)
                . '<span> / </span>' . $view->formText($name . '[month]', $month, $monthParams)
                . '<span> / </span>' . $view->formText($name . '[year]', $year, $yearParams);

        $hourParams['disabled'] = true;
        $minuteParams['disabled'] = true;
        $monthParams['disabled'] = true;
        unset($monthParams['size']);
        unset($monthParams['maxlength']);
        $yearParams['disabled'] = true;
        $dayParams['disabled'] = true;

        $markup .= "</div>";
        $monthOptions = array(
            '1' => 'January', '2' => 'February', '3' => 'March',
            '4' => 'April', '5' => 'May', '6' => 'June',
            '7' => 'July', '8' => 'August', '9' => 'September', '10' => 'October',
            '11' => 'November', '12' => 'December'
        );
        $markup .= "<div style='display: none' class='date_type_entry  month_entry'>"
                . $view->formHidden($name . '[minutes]', '0', $minuteParams)
                . $view->formHidden($name . '[hour]', '12', $hourParams)
                . $view->formSelect($name . '[month]', $month, $monthParams, $monthOptions)
                . '<span> / </span>' . $view->formText($name . '[year]', $year, $yearParams)
                . $view->formHidden($name . '[day]', $day, $dayParams)
                . "</div>";

        $markup .= "<div style='display: none'  class='date_type_entry year_entry'>"
                . $view->formHidden($name . '[minutes]', '0', $minuteParams)
                . $view->formHidden($name . '[hour]', '22', $hourParams)
                . $view->formHidden($name . '[month]', '12', $monthParams, $monthOptions)
                . $view->formText($name . '[year]', $year, $yearParams)
                . $view->formHidden($name . '[day]', $day, $dayParams)
                . "</div>";

        $quarterOptions = array('3' => 'Q1', '6' => 'Q2', '9' => 'Q3', '12' => 'Q4');
        $markup .= "<div style='display: none'  class='date_type_entry quarter_entry'>"
                . $view->formHidden($name . '[minutes]', '0', $minuteParams)
                . $view->formHidden($name . '[hour]', '17', $hourParams)
                . $view->formSelect($name . '[month]', $month, $monthParams, $quarterOptions)
                . '<span> / </span>' . $view->formText($name . '[year]', $year, $yearParams)
                . $view->formHidden($name . '[day]', $day, $dayParams)
                . "</div>";



        if ($element->getAttrib('includeTime')) {

            $markup .= $view->formHidden($name . '[hour]', 0) . $view->formHidden($name . '[minutes]', 0);
        }


        switch ($this->getPlacement()) {
            case self::PREPEND:
                return $markup . $this->getSeparator() . $content;
            case self::APPEND:
            default:
                return $content . $this->getSeparator() . $markup;
        }
    }

}
