<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Custom.php 1198 2010-08-05 11:52:39Z leigh $
 */

/**
 * Decorator class for building the form element using the HTML we find useful
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Decorator
 */
class Mcmr_Form_Decorator_Custom extends Zend_Form_Decorator_Abstract
{
    /**
     * Return the html for the element label
     *
     * @return string
     */
    protected function _buildLabel()
    {
        $element = $this->getElement();
        $label = $element->getLabel();
        if (null !== ($translator = $element->getTranslator())) {
            $label = $translator->translate($label);
        }
        if ($element->isRequired()) {
            $label .= '<span>*</span>';
        }
        
        return $element->getView()
                ->formLabel($element->getName(), $label, array('escape'=>false));
    }

    /**
     * Return the html for the input field
     *
     * @return string
     */
    protected function _buildInput()
    {
        $element = $this->getElement();
        $helper  = $element->helper;
        $attribs = $element->getAttribs()?$element->getAttribs():array();
        $options = is_array($element->options)?$element->options:array();
        return $element->getView()->$helper(
                $element->getName(),
                $element->getValue(),
                $attribs,
                $options
        );
    }

    /**
     * Return the html for the error messages
     *
     * @return string
     */
    protected function _buildErrors()
    {
        $element  = $this->getElement();
        $messages = $element->getMessages();
        if (empty($messages)) {
            return '';
        }
        return '<div class="errors">' .
                $element->getView()->formErrors($messages) . '</div>';
    }

    /**
     * Return the html for the description
     *
     * @return string
     */
    protected function _buildDescription()
    {
        $element = $this->getElement();
        $desc    = $element->getDescription();
        if (empty($desc)) {
            return '';
        }
        return '<div class="description">' . $desc . '</div>';
    }

    /**
     * Render the form element
     *
     * @param string $content
     * @return string
     */
    public function render($content)
    {
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }
        if ($element instanceof Zend_Form_Element_Hidden) {
            return $this->_buildInput();
        }

        $separator = $this->getSeparator();
        $placement = $this->getPlacement();
        $label     = $this->_buildLabel();
        $input     = $this->_buildInput();
        $errors    = $this->_buildErrors();
        $desc      = $this->_buildDescription();

        $output = '<div class="form element">'
        . $label
        . $input
        . $errors
        . $desc
        . '</div>';

        switch ($placement) {
            case (self::PREPEND):
                return $output . $separator . $content;
            
            case (self::APPEND):
            default:
                return $content . $separator . $output;
        }
    }
}