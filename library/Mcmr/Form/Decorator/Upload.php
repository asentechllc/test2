<?php

class Mcmr_Form_Decorator_Upload extends Zend_Form_Decorator_File
{
    /**
     * Render a form upload
     *
     * @param  string $content
     * @return string
     */
    public function render($content)
    {
        $content = parent::render($content);

        $element = $this->getElement();
        if (!$element instanceof Mcmr_Form_Element_Upload) {
            // only want to render Upload elements
            return $content;
        }

        $view = $element->getView();
        if (!$view instanceof Zend_View_Interface) {
            // using view helpers, so do nothing if no view present
            return $content;
        }

        // If there is a value, add a checkbox for deleting the file

        if ($element->getValue()) {
            $name = $element->getName();
            $content .= $view->formHidden($name.'_value', $element->getSessionKey());
            $content .= "<span class=\"delete\">".$view->formCheckbox($name.'_delete')." Delete File?</span>";
        }
        
        return $content;
    }
}
