<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Date.php 2402 2011-05-05 14:58:38Z leigh $
 */

/**
 * Decorator class for building a date field
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Decorator
 */
class Mcmr_Form_Decorator_Date extends Zend_Form_Decorator_Abstract
{
    /**
     * Decorate content and/or element
     *
     * @param  string $content
     * @return string
     * @throws Zend_Form_Decorator_Exception when unimplemented
     */
    public function render($content)
    {
        $element = $this->getElement();
        if (!$element instanceof Mcmr_Form_Element_Date) {
            // only want to render Date elements
            return $content;
        }

        $view = $element->getView();
        if (!$view instanceof Zend_View_Interface) {
            // using view helpers, so do nothing if no view present
            return $content;
        }

        if ($element->getValue() > 0) {
            $day   = sprintf("%02d", $element->getDay());
            $month = sprintf("%02d", $element->getMonth());
            $year  = $element->getYear();
        } else {
            $day = '';
            $month = '';
            $year = '';
        }
        $name  = $element->getFullyQualifiedName();
        $dayClass = $element->getAttrib('dayClass');
        $monthClass = $element->getAttrib('monthClass');
        $yearClass = $element->getAttrib('yearClass');
        $disable = $element->getAttrib('disable');
        
        $dayParams = array(
            'size'      => 2,
            'maxlength' => 2,
            'class' => $dayClass,
            'disable' => $disable,
        );
        $monthParams = array(
            'size'      => 2,
            'maxlength' => 2,
            'class' => $monthClass,
            'disable' => $disable,
        );
        $yearParams = array(
            'size'      => 4,
            'maxlength' => 4,
            'class' => $yearClass,
            'disable' => $disable,
        );
        
        $markup = $view->formText($name . '[day]', $day, $dayParams)
                . '<span> / </span>' . $view->formText($name . '[month]', $month, $monthParams)
                . '<span> / </span>' . $view->formText($name . '[year]', $year, $yearParams);

        if ($element->getAttrib('includeTime')) {
            $hourParams = array(
                'size' => 2,
                'maxlength' =>2,
                'class' => $element->getAttrib('hourClass'),
            );
            
            $minuteParams = array(
                'size' => 2,
                'maxlength' =>2,
                'class' => $element->getAttrib('minuteClass'),
            );

            if ($element->getValue() > 0) {
                $hour = sprintf("%02d", $element->getHour());
                $minute = sprintf("%02d", $element->getMinutes());
            } else {
                $hour = '';
                $minute = '';
            }

            $markup .= $view->formText($name. '[hour]', $hour, $hourParams)
                . ':' . $view->formText($name . '[minutes]', $minute, $minuteParams);
        }
                
        switch ($this->getPlacement()) {
            case self::PREPEND:
                return $markup . $this->getSeparator() . $content;
            case self::APPEND:
            default:
                return $content . $this->getSeparator() . $markup;
        }
    }
}
