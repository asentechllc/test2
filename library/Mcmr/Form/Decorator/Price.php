<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Date.php 1886 2010-12-01 11:34:38Z michal $
 */

/**
 * Decorator class for building a date field
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @subpackage Decorator
 */
class Mcmr_Form_Decorator_Price extends Zend_Form_Decorator_Abstract
{
    /**
     * Decorate content and/or element
     *
     * @param  string $content
     * @return string
     * @throws Zend_Form_Decorator_Exception when unimplemented
     */
    public function render($content)
    {
        $element = $this->getElement();
        if (!$element instanceof Mcmr_Form_Element_Price) {
            // only want to render Date elements
            return $content;
        }

        $view = $element->getView();
        if (!$view instanceof Zend_View_Interface) {
            // using view helpers, so do nothing if no view present
            return $content;
        }

        $name  = $element->getFullyQualifiedName();
        $majorClass = $element->getAttrib('majorClass');
        $minorClass = $element->getAttrib('minorClass');

        $majorParams = array(
            'size'      => 4,
            'maxlength' => 10,
            'class' => $majorClass,
        );
        $minorParams = array(
            'size'      => 2,
            'maxlength' => 2,
            'class' => $minorClass,
        );

        $symbol = $element->getAttrib('symbol');

        $markup = "<span> {$symbol} </span>" 
            . $view->formText($name . '[major]', $element->getMajor(), $majorParams)
            . '<span> . </span>' 
            . $view->formText($name . '[minor]', sprintf('%02d', $element->getMinor()), $minorParams);

        switch ($this->getPlacement()) {
            case self::PREPEND:
                return $markup . $this->getSeparator() . $content;
            case self::APPEND:
            default:
                return $content . $this->getSeparator() . $markup;
        }
    }
}
