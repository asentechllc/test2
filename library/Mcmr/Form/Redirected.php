<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Form.php 651 2010-05-14 15:56:29Z leigh $
 */

/**
 * Support for a form that is processed and displayed by different actions.
 * Form validation funcaionality assumes that the same action that processed and validated the form
 * will display it together with error messages.
 * Sometimes it is required to display form on page genereated by action A and then submit it to action B
 * If the validation fails B redirects back to A which should display the form (and the rest of the page)
 * together with the validation messages.
 * This form provides support for this functionality.
 *
 * @category Mcmr
 * @package Mcmr_Form
 */
class Mcmr_Form_Redirected extends Mcmr_Form
{

    public function init()
    {
        parent::init();
        
        $el = new Zend_Form_Element_Hidden('redirect');
        $el->addValidator('NotEmpty');
        $this->addElement($el);
    }

    /**
     * Configure action and redirect url. 
     * @param array $action form submit action. Array is converted to url using url helper.
     * @param string $redirect url to which user will be redirected after form submission.
     * Current request url will be used if omitted.
     */
    public function configure($action, $redirect=null)
    {
        //configure the hidden redirect value
        if ($redirect === null) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $redirect = $request->getRequestUri();
        }
        $this->setRedirect($redirect);
        if (is_array($action)) {
            $url = Zend_Controller_Action_HelperBroker::getStaticHelper('url');
            $action = $url->url($action, 'default', true);
        }
        
        $this->setAction($action);
    } //configure
    
    

    /**
     * Add a hidden redirect field with specified url as value.
     * @param string $url value for the field
     */
    public function setRedirect($url)
    {
        $el = $this->getElement('redirect');
        $el->setValue($url);
    } //setRedirect
    
}
