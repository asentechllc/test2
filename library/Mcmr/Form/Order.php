<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   News
 * @package    Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Order.php 2359 2011-04-15 11:15:41Z leigh $
 */

/**
 * A form for changing the display order of the news articles
 *
 * @category   News
 * @package    Form
 * @subpackage Order
 */
class Mcmr_Form_Order extends Mcmr_Form
{
    protected $_options = null;
    protected $_type = null;
    protected $_count = null;
    protected $_models = null;
    protected $_source = null;

    public function __construct($options = null)
    {
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }
            $this->_options = $options;
            $this->_setConfigFile();
        }
        parent::__construct($this->_options);
    }

    public function postInit()
    {
        $module = $this->_getModuleName();
        $this->setName($module.'formorder')->setElementsBelongTo($module.'-form-order');
        $this->setMethod('post');

        $this->_processModels();
        
        $subform = new Mcmr_Form();
        $subform->setElementsBelongTo('models');

        $displayGroupElements = array();

        
        for ($i=0; $i<$this->_count; $i++) {
            if ($i<count($this->_models)) {
                $value = $this->_models[$i]->getId(); 
            } else {
                $value = ''; 
            }
            $elementIndex = "".($i+1);
            $displayGroupElements[] = $elementIndex;
            $this->_addSingleElement($subform, $elementIndex, $value);
        }
        if (isset($displayGroupElements[0])) {
            $subform->addDisplayGroup($displayGroupElements, 'models', array('class'=>'sortable-items'));
        }

        $this->addSubForm($subform, 'models');

        // Set the value of the 'orderby' field
        $orderby = $this->getElement('orderby');
        if (null !== $orderby) {
            $typeValue = $this->_getOrderbyValue();
            $orderby->setValue($typeValue);
        }
    }

    protected function _addSingleElement($subform, $index, $value)
    {
        $elementConfig = array(
            'options' => array(
                'required' => true,
                'class' => 'sortable'
            ),
            'optionSource' => array_merge($this->_source, array(
                'allowNull'=>true,
                'allowNullDisplay'=>'Please Select...',
                'optGroup' => array(
                	'relationIdMethod' => 'getTypeid',
                	'relationModelMethod' => 'getType',
                ),
            ))
        );
        $elementConfig['options']['label'] = "Entry ".$index;
        $elementConfig['options']['value'] =  $value;
        $element = $this->_processElement($elementConfig);
        $subform->addElement('Select', $index, $element['options']);
    }

    public function setModels($models)
    {
        $this->_models = $models;
        return $this;
    }

    public function getModels()
    {
        return $this->_models;
    }

    protected function _processModels()
    {
        $orderName = 'ordr_'.$this->_type;
        $this->_source = $this->_models['source'];
        $this->_count = $this->_models['count'];
        $module = $this->_source['module'];
        $model = $this->_source['model'];
        $query = $this->_source['query'];
        $page  = array('page'=>1, 'count'=>$this->_models['count']);
        $config = array('page'=>$page);
        $queryObject = Mcmr_Model_Query::factory($query, $module);
        $currentOrder = array($orderName=>'desc');
        $queryObject->prependOrders($currentOrder);
        $models = Mcmr_Model::queryAll($queryObject, $model, $module, $config);
        $this->_models = Mcmr_Model::filterOrdered($models, $this->_type);
    }

    protected function _getOrderbyValue() 
    {
        if (is_array($this->_models)&&count($this->_models)) {
            $orderby = 'specific';
	    	if($this->getElement('orderby')){
	    		if(count($this->_models) == 1 && array_key_exists('feature',$this->getElement('orderby')->getMultiOptions())){
	    			$orderby = 'feature';
	    		}
    		}
        } else {
            $orderby = 'chronological';
        }
        return $orderby;
    }

    protected function _setConfigFile() 
    {
        $type = $this->_options['type'];
        $module = $this->_getModuleName();
        $basename = strtolower(get_class($this));
        $this->_options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . $module
            . DIRECTORY_SEPARATOR . $basename .'-'.strtolower($type).'.ini';
        $this->_type = $type;
    }

}
