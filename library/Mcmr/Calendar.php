<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Calendar
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Cache.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Basic calendar class with factory method to return a concrete implementation
 *
 * @category Mcmr
 * @package Mcmr_Calendar
 */
abstract class Mcmr_Calendar
{

    /**
     * Create an instance of calendar for a given format.
     */
    static function getInstance( $format, $options = array())
    {
        $class = "Mcmr_Calendar_Impl_".$format;
        return new $class(  $options );
    }
    
    /**
     * Convert the calendar to a string suitable for outputting to the browser.
     */
    abstract public function __toString();
    
    /**
     * Add an event to the calendar.
     */
    abstract public function addEvent( Mcmr_Calendar_Event $e );
} //Mcmr_Calendar

