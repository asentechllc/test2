<?php

class Mcmr_Model
{
	/**
	 */
	public static function idsForUrls($modelClass, $urls) 
	{
		if (!is_array($urls)) {
			$urls = explode(',', $urls);
		}
		$ids = array();
		foreach( $urls as $url ) {
			if(null!==($id=static::idForUrl($modelClass, $url))) {
				$ids[]=$id;
			}
		}
		return $ids;
	}

	public static function idForUrl($modelClass, $url) {
		$model = $modelClass::getMapper()->findOneByField(array('url'=>$url));
		if (null!==$model) {
			return $model->getId();
		}
		return null;
	}


	public static function queryOne($query, $model, $moduleName, $config = null)
	{
		$query = static::queryBuild($query, $model, $moduleName, $config);
    	$mapper = static::_getMapper($model, $moduleName);
    	$models = $mapper->findOneByQuery($query);
    	return $models;
	}


	public static function queryAll($query, $model, $moduleName, $config = null)
	{
		$query = static::queryBuild($query, $model, $moduleName, $config);
    	$mapper = static::_getMapper($model, $moduleName);
    	$models = $mapper->findAllByQuery($query);
    	return $models;
	}

	public static function queryCount($query, $model, $moduleName, $config = null)
	{
		$query = static::queryBuild($query, $model, $moduleName, $config);
    	$mapper = static::_getMapper($model, $moduleName);
    	$models = $mapper->countByQuery($query);
    	return $models;
	}

	public static function queryBuild($query, $model, $moduleName, $config = null)
	{
		if (!$query instanceof Mcmr_Model_Query) {
	    	$query = Mcmr_Model_Query::factory($query, $moduleName);
		}
        if (null!==$config) {
        	$query->addConfig($config);
        }
        return $query;
	}


	public static function queryExists($query, $model, $moduleName)
	{
	    $result = Mcmr_Model_Query::exists($query, $moduleName);
        return $result;
	}

	protected static function _getMapper($model, $moduleName) {
    	$modelClassName = ucfirst($moduleName).'_Model_'.ucfirst($model);
    	$mapper = $modelClassName::getMapper();
    	return $mapper;
	}

	public static function filterOrdered($models, $order) {
		$filtered = array();
		foreach ($models as $model) {
			$orderValue = $model->getOrder($order);
			if (null!==$orderValue && 0!=$orderValue) {
				$filtered[]=$model;
			}
		}
		return $filtered;
	}

	public static function getPathPart($model, $withType=true, $withSection=true)
	{
        $parts = explode('_', get_class($model));
        $moduleName = $parts[0];
        $modelName = $parts[2];
        $controllerName = $model->getControllerName();
        $pathPart = strtolower($moduleName).'-'.strtolower($modelName);

		# check if we have a section first
		# getSection will return null if there is no section assigned
        if ($withSection && method_exists($model, 'getSection')) {
            $section = $model->getSection();
	        if ($section && is_object($section)) {
	        
	        	# sections can only use one model - so we want news-article
	        	$pathPart = 'news-article';

	        	# determine URL nesting
	        	if($parent = $section->getParent()){
	        		$pathPart .= '-sub-'.$parent->getUrl();
	        	}
	        	else{
		            $pathPart.='-'.$section->getUrl();
		        }

	            return $pathPart;
	        }
	    }
	    
        if ($withType && method_exists($model, 'getType')) {
            $type = $model->getType();
	        if ($type && is_object($type) && method_exists($type, 'getUrl')) {
	            $typeUrl = $type->getUrl();
	            $pathPart.='-'.$typeUrl;
	        }
	    }
        return $pathPart;
	}

    public static function getCanonicalReadUrl($model)
    {
        $parts = explode('_', get_class($model));
        $moduleName = $parts[0];
        $controllerName = $model->getControllerName();
        $route = static::getPathPart($model).'-read';
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $url=$view->gossipUrl(array('module'=>$moduleName,'controller'=>$controllerName,'action'=>'read','model'=>$model), $route, true);
        return $url;
    }

}
