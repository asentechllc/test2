<?php
/**
 * Resource class to plug in custom layout class
 */
class Mcmr_Application_Resource_Layout extends Zend_Application_Resource_Layout
{
    public function getLayout()
    {
        if (null === $this->_layout) {
            $this->_layout = Mcmr_Layout::startMvc($this->getOptions());
        }
        return $this->_layout;
    }

}
