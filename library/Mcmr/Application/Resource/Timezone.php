<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Resource class for the system dates. Set the default timezone
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @subpackage Resource
 */
class Mcmr_Application_Resource_Timezone extends Zend_Application_Resource_ResourceAbstract
{
    public function init() 
    {
        $options = $this->getOptions();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $siteName = $request->getSiteName();
        
        if (isset($options[$siteName]) && isset($options[$siteName]['userTimezone'])) {
            Mcmr_Date::setDefaultTimezone($options[$siteName]['userTimezone']);
        } elseif (isset($options['userTimezone'])) {
            Mcmr_Date::setDefaultTimezone($options['userTimezone']);
        }
    }
}
