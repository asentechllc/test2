<?php

class Mcmr_Application_Resource_Query extends Zend_Application_Resource_ResourceAbstract
{
    
    public function init()
    {
        $options = $this->getOptions();
        foreach($options as $option => $value) {
        	$setter = 'set'.ucfirst($option);
        	Mcmr_Model_Query::$setter($value);
        }
    }
}
