<?php

class Mcmr_Application_Resource_Queue extends Zend_Application_Resource_ResourceAbstract
{
    
    public function init()
    {
        $options = $this->getOptions();
        
        foreach ($options as $name=>$options) {
            Mcmr_Queue::setQueue($name, $options);
        }
    }
}
