<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Circuit Breaker resource class.
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @subpackage Resource
 */
class Mcmr_Application_Resource_CircuitBreaker extends Zend_Application_Resource_ResourceAbstract
{
    const DEFAULT_REGISTRY_KEY = 'Mcmr_CircuitBreaker';

    public function init()
    {
        $options = array_change_key_case($this->getOptions(), CASE_LOWER);
        
        if (empty($options['store'])) {

        }
        if (empty($options['logger'])) {

        }

        
    }
}