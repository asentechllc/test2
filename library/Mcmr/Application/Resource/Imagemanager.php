<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Resource class for the image manager
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @subpackage Resource
 */
class Mcmr_Application_Resource_Imagemanager extends Zend_Application_Resource_ResourceAbstract
{
    const PATH = 'public/assets/media/';
    const ROOT_PATH = 'public/assets/media/';
    
    /**
     * Set up all the image manager configuration.
     */
    public function init()
    {
        $this->getBootstrap()->bootstrap('session');

        $options = array_change_key_case($this->getOptions(), CASE_LOWER);
        
        Zend_Session::start();
        if (!isset($options['path']) || empty($options['path'])) {
            $_SESSION['imagemanager.filesystem.path'] = SITE_PATH . DIRECTORY_SEPARATOR . self::PATH;
        } else {
            $_SESSION['imagemanager.filesystem.path'] = $options['path'];
        }

        if (!isset($options['rootpath']) || empty($options['rootpath'])) {
            $_SESSION['imagemanager.filesystem.rootpath'] = SITE_PATH  . DIRECTORY_SEPARATOR . self::ROOT_PATH;
        } else {
            $_SESSION['imagemanager.filesystem.rootpath'] = $options['rootpath'];
        }

        if ( isset( $options[ 'basepath' ] ) ) {
            $_SESSION['imagemanager.preview.urlprefix'] = $options[ 'basepath' ];
            $_SESSION['imagemanager.relative_urls'] ='false';
            $_SESSION['imagemanager.remove_script_host'] ='false';
        }
    }
}
