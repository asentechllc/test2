<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Resource class for the read/write split database adapters
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @subpackage Resource
 */
class Mcmr_Application_Resource_Splitdb extends Zend_Application_Resource_ResourceAbstract
{
    /**
     * Write adapter settings
     *
     * @var Zend_Config|array 
     */
    protected $_writedb = null;
    
    /**
     * Read adapter settings
     *
     * @var Zend_Config|array 
     */
    protected $_readdb = null;
    
    /**
     * Adapter for database write queries
     *
     * @var Zend_Db_Adapter_Interface 
     */
    protected $_dbWriteAdapter = null;
    
    /**
     * Adapter for database read queries
     *
     * @var Zend_Db_Adapter_Interface 
     */
    protected $_dbReadAdapter = null;
    
    /**
     * Get the write DB settings
     *
     * @return array
     */
    public function getWritedb() 
    {
        return $this->_writedb;
    }

    /**
     * Set the write DB adapter settings
     *
     * @param Zend_Config|array $writeDb
     * @return Mcmr_Application_Resource_Splitdb 
     */
    public function setWritedb($writeDb) 
    {
        if (is_object($writeDb) && $writeDb instanceof Zend_Config) {
            $writeDb = $writeDb->toArray();
        }
        $this->_writedb = $writeDb;
        
        return $this;
    }

    /**
     * Get the read DB adapter settings
     *
     * @return array 
     */
    public function getReaddb() 
    {
        return $this->_readdb;
    }

    /**
     * Set the read DB adapter settings
     *
     * @param Zend_Config|array $readDb
     * @return Mcmr_Application_Resource_Splitdb 
     */
    public function setReaddb($readDb) 
    {
        if (is_object($readDb) && $readDb instanceof Zend_Config) {
            $readDb = $readDb->toArray();
        }
        $this->_readdb = $readDb;
        
        return $this;
    }
    
    /**
     * Get the write adapter name
     *
     * @return null|string 
     */
    public function getWriteAdapter()
    {
        if (null !== $this->_writedb && isset($this->_writedb['adapter'])) {
            return $this->_writedb['adapter'];
        } else {
            return null;
        }
    }
    
    /**
     * Get the read adapter name
     *
     * @return null|string 
     */
    public function getReadAdapter()
    {
        if (null !== $this->_readdb && isset($this->_readdb['adapter'])) {
            return $this->_readdb['adapter'];
        } else {
            return null;
        }
    }
    
    /**
     * Get the write adapter
     *
     * @return null|Zend_Db_Adapter_Interface
     */
    public function getDbWriteAdapter()
    {
        $writeDb = $this->getWritedb();
        if ((null === $this->_dbWriteAdapter)
            && (null !== ($adapter = $this->getWriteAdapter())
            && isset($writeDb['params'])
                )
        ) {
            $this->_dbWriteAdapter = Zend_Db::factory($adapter, $writeDb['params']);
        }
        return $this->_dbWriteAdapter;
        
    }
    
    /**
     * Get the read adapter
     *
     * @return null|Zend_Db_Adapter_Interface
     */
    public function getDbReadAdapter()
    {
        $readDb = $this->getReaddb();
        if ((null === $this->_dbReadAdapter)
            && (null !== ($adapter = $this->getReadAdapter())
            && isset($readDb['params'])
                )
        ) {
            $this->_dbReadAdapter = Zend_Db::factory($adapter, $readDb['params']);
        }
        return $this->_dbReadAdapter;
    }
    
    /**
     * Set up the default read and write database adapters
     */
    public function init()
    {
        Mcmr_Db_Table_Abstract::setDefaultReadAdapter($this->getDbReadAdapter());
        Mcmr_Db_Table_Abstract::setDefaultWriteAdapter($this->getDbWriteAdapter());
    }
}
