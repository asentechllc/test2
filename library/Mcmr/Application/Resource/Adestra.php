<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of Adestra
 *
 * @category Mcmr
 */
class Mcmr_Application_Resource_Adestra extends Zend_Application_Resource_ResourceAbstract
{
    
    public function init()
    {
        $options = $this->getOptions();
        
        Mcmr_Service_Adestra::setDefaultOptions($options);
    }
}
