<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Base class for all module bootstraps. Sets the module configuration and ACL class paths
 *
 * @category Mcmr
 * @package Mcmr_Application
 * @subpackage Module
 */
class Mcmr_Application_Module_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * Get the module name
     *
     * @return string
     */
    public function getBootstrapModule()
    {
        $classname = get_class($this);
        $modulename = substr($classname, 0, strpos($classname, '_'));
        
        return $modulename;
    }

    /**
     * Initialise the module's configuration and store in the registry
     */
    protected function _initConfig()
    {
        try {
            // Setup the config settings for this module
            $modulename = strtolower($this->getBootstrapModule());
            $configfile = 'modules' . DIRECTORY_SEPARATOR . $modulename . '.ini';
            $options = Mcmr_Config_Ini::getInstance($configfile, APPLICATION_ENV);
            Zend_Registry::set($modulename . '-config', $options);
        } catch (Exception $e) {
            //if module config is not found, ignore the exception
            if (false!==strpos($e->getMessage(), "No such file or directory")) {
                Zend_Registry::set($modulename . '-config', null);
            } else {
                throw $e;
            }
        }
    }

    /**
     * Initialise the module's ACL class path
     */
    public function _initAcl()
    {
        $autoloader = $this->getResourceLoader();
        $autoloader->addResourceType('acl', 'acls/', 'Acl');
    }
}
