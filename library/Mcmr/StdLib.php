<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_StdLib
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: StdLib.php 2406 2011-05-10 08:48:26Z michal $
 */

/**
 * A collection of random useful functions
 * 
 * @category Mcmr
 * @package Mcmr_StdLib
 */
class Mcmr_StdLib
{
    /**
     * Urls cannot be longer than this.
     */
    const MAX_URL_LENGTH = 128;
    
    /**
     * Convert a real ID to seo standard
     * 
     * @param int $id
     * @return string
     */
    static function seoId($id){return '0'.(99+$id);}

	/**
	 * A basic method to determine if a controller is being accessed via gossip admin
	 * Feel free to make this more accurate...
	 * @return boolean if SITE_PATH contains 'admin'
	 **/
	static function isAdmin(){
		return strpos(SITE_PATH,'admin') ? true : false;
	}

    /**
     * Determine if an HTML string is empty to the human eye
     * 
     * @param string $html
     * @return boolean
     */
    static function emptyHtml($html){
    	return trim(strip_tags(preg_replace('/&[0-9a-z]+;/i','',$html))) ? false : true;
    }

    /**
     * Convert an SEO ID to real ID
     * 
     * @param string $seoId
     * @return string
     */
    static function realId($seoId){return intval(substr($seoId,1)-99);}

    /**
     * Return a random generated string
     * 
     * @param int $min
     * @param int $max
     * @return string
     */
    static function randomString($min=4, $max=8)
    {
        $string = "";
        for ($i = 0; $i < mt_rand($min, $max); $i ++) {
            $randnum = mt_rand(0, 61);
            if ($randnum < 10) {
                $string .= chr($randnum + 48);
            } elseif ($randnum < 36) {
                $string .= chr($randnum + 55);
            } else {
                $string .= chr($randnum + 61);
            }
        }

        return $string;
    }

    /**
     * Return a unique string token
     * return @string
     */
    static function uniqueToken() 
    {
        // http://stackoverflow.com/questions/9198903/secure-signin-with-unique-token
        return sha1(microtime(true) . mt_rand(10000, 90000));
    }

    /**
     * Takes bytes and converts it into a human readable string
     *
     * @param int $size
     * @return string
     */
    static function readableMemory($size)
    {
        $unit = array('b', 'KB', 'MB', 'GB', 'TB', 'PB');
        
        return @round($size/pow(1024, ($i=floor(log($size, 1024)))), 2) . ' ' . $unit[$i];
    }

    /**
     * Take an array and produce a CSV string
     *
     * @author simeonl@dbc.co.nz (http://uk.php.net/manual/en/function.fputcsv.php)
     * @staticvar resource $fp
     * @param array $set
     * @param string $delimiter
     * @param string $enclosure
     * @param string $eol
     * @return string
     */
    static function arrayToCsv($set, $delimiter = ',', $enclosure = '"', $eol = "\n")
    {
        $csv = '';
        foreach ($set as $row) {
            if (($line = self::sputcsv($row, $delimiter, $enclosure, $eol)) === false) {
                return false;
            } else {
                $csv .= $line;
            }
        }
        return $csv;
    }
    
    /**
     * Converts a one dimentional array into a CSV string and returned
     *
     * @staticvar boolean $fp
     * @param type $row
     * @param type $delimiter
     * @param type $enclosure
     * @param type $eol
     * @return string 
     */
    function sputcsv($row, $delimiter = ',', $enclosure = '"', $eol = "\n")
    {
        static $fp = false;
        if ($fp === false) {
            // see http://php.net/manual/en/wrappers.php.php - yes there are 2 '.php's on the end.
            $fp = fopen('php://temp', 'r+');
            // NB: anything you read/write to/from 'php://temp' is specific to this filehandle
        } else {
            rewind($fp);
        }
       
        if (fputcsv($fp, $row, $delimiter, $enclosure) === false) {
            return false;
        }
       
        rewind($fp);
        $csv = fgets($fp);
       
        if ($eol != PHP_EOL) {
            $csv = substr($csv, 0, (0 - strlen(PHP_EOL))) . $eol;
        }
       
        return $csv;
    }
    
    /**
     * Converts any string to one usable as a url key.
     * Replaces all non-ascii characters with closest ascii equivalents, converts to lower case and replaces 
     * all non-alphanumeric clusters of characters (including whitespace) with a single separator character each.
     */
    public static function urlize($text, $separator = '-')
    {
        $quotedSeparator = preg_quote($separator);
        //replace any non-ascii characters like accentuated letters  with a closest ascii equivalent
        $tempText = @iconv("UTF-8", "ISO-8859-1//IGNORE//TRANSLIT", $text);
        
        // Work around weird iconv bug where it is returning an empty string on some systems
        if (!empty($tempText)) {
            $text = $tempText;
        }
        
        //lowercase everything
        $text = strtolower($text);
        //replace everything non-alphanumeric with the seperator
        $text = preg_replace('/[^a-z0-9]/', '-', $text);
        //now replace multiple instances of seprator with a single one
        $text = preg_replace('/'.$quotedSeparator.'+/', $separator, $text);
        //trim it to max length
        $text = substr( $text, 0, self::MAX_URL_LENGTH );
        //remove extra seperators from the ends
        $text = trim($text, $separator);
        return $text;
    }
    
    /**
     * Replace urls in plain text with links to that urls.
     * http://stackoverflow.com/questions/206059/php-validation-regex-for-url
     */
    public static function linkUrls($text) 
    {
        $pattern = "#((http|https|ftp)://(\S*?\.\S*?))(\s|\;|\)|\]|\[|\{|\}|,|\"|'|:|\<|$|\.\s)#ie";
        $replace = "'<a href=\"$1\" >$3</a>$4'";
        $modifiedText = preg_replace($pattern, $replace, $text);
        
        return $modifiedText;
    }

    
    /**
     * Returns a beginning part of text up to a given length but made of complete words only.
     * Html tags will be stripped from the input text.
     * @param string $text the text to process.
     * @param int $count maximum length of the substring to return.
     * @return starting part of the string cut off at a word break.
     */
    public static function firstWords($text, $count)
    {
        $separator = "###SEPARATOR###";
        $parts = explode($separator, wordwrap(strip_tags($text), $count, $separator));
        
        return trim($parts[0]);
    }

    /**
     * Parse a URL query string and return the variable value
     *
     * @param string $url
     * @param string $var
     * @return mixed
     */
    public static function queryStringValue($url, $var)
    {
        $query = parse_url($url, PHP_URL_QUERY);
        parse_str($query);

        return $$var;
    }
    
    /**
     * Get file's MIME type
     * 
     * @param string $file_path
     */
    public static function getFileType($filePath)
    {
        $type = '';
        if (function_exists('finfo_file')) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $type = finfo_file($finfo, $filePath);
        } elseif (function_exists('mime_content_type')) {
            $type = mime_content_type($filePath);
        } else {
            $mimeTypes = array(
                'txt' => 'text/plain',
                'htm' => 'text/html',
                'html' => 'text/html',
                'php' => 'text/html',
                'css' => 'text/css',
                'js' => 'application/javascript',
                'json' => 'application/json',
                'xml' => 'application/xml',
                'swf' => 'application/x-shockwave-flash',
                'flv' => 'video/x-flv',

                // images
                'png' => 'image/png',
                'jpe' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'jpg' => 'image/jpeg',
                'gif' => 'image/gif',
                'bmp' => 'image/bmp',
                'ico' => 'image/vnd.microsoft.icon',
                'tiff' => 'image/tiff',
                'tif' => 'image/tiff',
                'svg' => 'image/svg+xml',
                'svgz' => 'image/svg+xml',

                // archives
                'zip' => 'application/zip',
                'rar' => 'application/x-rar-compressed',
                'exe' => 'application/x-msdownload',
                'msi' => 'application/x-msdownload',
                'cab' => 'application/vnd.ms-cab-compressed',

                // audio/video
                'mp3' => 'audio/mpeg',
                'qt' => 'video/quicktime',
                'mov' => 'video/quicktime',

                // adobe
                'pdf' => 'application/pdf',
                'psd' => 'image/vnd.adobe.photoshop',
                'ai' => 'application/postscript',
                'eps' => 'application/postscript',
                'ps' => 'application/postscript',

                // ms office
                'doc' => 'application/msword',
                'rtf' => 'application/rtf',
                'xls' => 'application/vnd.ms-excel',
                'ppt' => 'application/vnd.ms-powerpoint',

                // open office
                'odt' => 'application/vnd.oasis.opendocument.text',
                'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
            );

            $pathinfo = pathinfo($filePath);
            if (isset($pathinfo['extension'])) {
                $extension = strtolower($pathinfo['extension']);
                if (isset($mimeTypes[$extension])) {
                    $type = $mimeTypes[$extension];
                } else {
                    $type = 'application/octet-stream';
                }
            }
        }
        
        return $type;
    }//end of getFileType()
    
    
    /**
     * Return a unix timestamp rounded down to the closest precision
     *
     * @param string $time
     * @param string $precision second, minute, quarter, hour, day
     */
    public function time($time = 'now', $precision = 'hour')
    {
        $offset = 0;    
        $timestamp = is_numeric($time)?((int)$time):strtotime($time);
        switch ($precision) {
            case 'second':
                $round = 1;
                break;

            case 'minute':
                $round = 60;
                break;

            case 'quarter':
                $round = 900;
                break;

            case 'hour':
                $round = 3600;
                break;

            case 'day':
                $round = 86400;
                $offset = (int)date("Z", $timestamp);
                break;

            default:
                throw new Zend_Exception("Unknown time precision '{$precision}'");
                break;
        }

        return $timestamp - ($timestamp % $round) - $offset;
    }
    
    /**
     * Open a file and convert contents into utf8. Returns a resource handle
     * http://www.php.net/manual/en/function.fopen.php
     * 
     * @author (splogamurugan at gmail dot com)
     * @param string $fileName
     * @return resource 
     */
    function fopenReadUtf8($fileName) 
    {
        $filter = new Mcmr_Filter_Utf8();
        
        $content = $filter->filter(file_get_contents($fileName));
        $handle=fopen("php://memory", "rw");
        fwrite($handle, $content);
        fseek($handle, 0);
        
        return $handle;
    }     
    
    /**
     * Take a value and convert it into a Zend_Date object.
     * 
     * If 'systemTimezone' is set them the Zend_Date object is created using the
     * system timezone before being set to the Zend_Date default timezone. If this
     * isn't used in a consistent manner then times could be mixed up between
     * the system timezone and the user's timezone.
     * 
     * This is ignored if created using a timestamp. Timestamps are always GMT
     *
     * @param mixed $time
     * @param bool $systemTimezone
     * @return Zend_Date 
     */
    public static function timeToDate($time, $systemTimezone=true)
    {
        $date = new Mcmr_Date();
        
        /*
         * Initialise the date. If we don't do this, then the date isn't always created properly
         * due to the number of days in the month.
         * 
         * For example. If today is 31st of December attemping to change the date to the 1st of Feb
         * will first set the month, but because Feb doesn't have 31 days it causes an error.
         */
        $date->setYear(1970);
        $date->setMonth(1);
        $date->setDay(1);
        $date->setHour(1);
        $date->setMinute(0);
        $date->setSecond(0);
        
        if (null === $time) {
            return null;
        } elseif (is_object($time) && $time instanceof Zend_Date) {
            $date = $time;
        } elseif (is_numeric($time)) {
            $date->setTimestamp($time);
        } elseif (is_string($time)) {
            $date->setTimestamp(strtotime($time));
        } elseif (is_array($time)) {
            if (empty($time['year'])) {
                $time['year'] = 1970;
            }
            if (empty($time['month'])) {
                $time['month'] = 1;
            }
            if (empty($time['day'])) {
                $time['day'] = 1;
            }
            if (empty($time['hour'])) {
                $time['hour'] = 1;
            }
            if (empty($time['minutes'])){
                $time['minutes'] = 0;
            }
            
            // Change the system timezone to get the timestamp from mktime.
            // We do it this way because Zend_Date does not handle the day of DST change very well.
            // Times on that day get saved incorrectly.
            $timezone = date_default_timezone_get();
            date_default_timezone_set(Mcmr_Date::getDefaultTimezone());
            $timestamp = mktime($time['hour'], $time['minutes'], 0, $time['month'], $time['day'], $time['year']);
            date_default_timezone_set($timezone);
            
            $date->setTimestamp($timestamp);
            
        } elseif (!is_object ($time) || !$time instanceof Zend_Date) {
            throw new Exception('Invalid date value provided');
        }
        
        return $date;
    }
    
    public static function dateToString( $format, $timestamp=null ) 
    {
        if (null==$timestamp) {
            $timestamp = time();
        }
        $date = new Mcmr_Date($timestamp);
        return $date->toString( $format, 'php' );
    }

    public static function timestampRelativeLink($file) 
    {
        //crude check if the file is a local url and that it doesn't already contain query string
        if ('http'!=substr($file, 0, 4) && '//'!=substr($file, 0, 2) && false===strpos($file, '?')) { 
            $path = SITE_PATH.'/public'.$file;
            if (file_exists($path)) {
                $timestamp = filemtime($path);
                $file = $file.'?ts='.$timestamp;
            }
        }
        return $file;

    }
}
