<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @author     Sean P. O. MacCath-Moran
 * @email      zendcode@emanaton.com
 * @website    http://www.emanaton.com
 * @copyright  This work is licenced under a Attribution Non-commercial Share Alike Creative Commons licence
 * @license    http://creativecommons.org/licenses/by-nc-sa/3.0/us/
 * @version    $Id: IdenticalField.php 2236 2011-03-07 11:20:20Z michal $
 */

/**
 * Validation class for validating two form fields have identical values
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @subpackage IdenticalField
 */
class Mcmr_Validate_IdenticalField extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'notMatch';
    const MISSING_FIELD_NAME = 'missingFieldName';
    const INVALID_FIELD_NAME = 'invalidFieldName';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
            self::MISSING_FIELD_NAME  =>
            'DEVELOPMENT ERROR: Field name to match against was not provided.',
            self::INVALID_FIELD_NAME  =>
            'DEVELOPMENT ERROR: The field "%fieldName%" was not provided to match against.',
            self::NOT_MATCH =>
            'Does not match %fieldTitle%.'
    );

    /**
     * @var array
     */
    protected $_messageVariables = array(
            'fieldName' => '_fieldName',
            'fieldTitle' => '_fieldTitle'
    );

    /**
     * Name of the field as it appear in the $context array.
     *
     * @var string
     */
    protected $_fieldName;

    /**
     * Title of the field to display in an error message.
     *
     * If evaluates to false then will be set to $this->_fieldName.
     *
     * @var string
     */
    protected $_fieldTitle;

    protected $_fields;

    /**
     * Sets validator options
     *
     * @param  string $fieldName
     * @param  string $fieldTitle
     * @return void
     */
    public function __construct($options)
    {
        if (isset($options['fields'])) {
            $this->setFields($options['fields']);
        } else {
            $this->setFields(array($options));
        }
    }

    public function getFields()
    {
        return $this->_fields;
    }

    public function setFields($fields)
    {
        $this->_fields = $fields;

        return $this;
    }

    /**
     * Returns the field name.
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->_fieldName;
    }

    /**
     * Sets the field name.
     *
     * @param  string $fieldName
     * @return Zend_Validate_IdenticalField Provides a fluent interface
     */
    public function setFieldName($fieldName)
    {
        $this->_fieldName = $fieldName;
        return $this;
    }

    /**
     * Returns the field title.
     *
     * @return integer
     */
    public function getFieldTitle()
    {
        return $this->_fieldTitle;
    }

    /**
     * Sets the field title.
     *
     * @param  string:null $fieldTitle
     * @return Zend_Validate_IdenticalField Provides a fluent interface
     */
    public function setFieldTitle($fieldTitle = null)
    {
        $this->_fieldTitle = $fieldTitle ? $fieldTitle : $this->_fieldName;
        return $this;
    }

    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if a field name has been set, the field name is available in the
     * context, and the value of that field name matches the provided value.
     *
     * @param  string $value
     *
     * @return boolean
     */
    public function isValid($value, $context = null)
    {
        $this->_setValue($value);
        $fields = $this->getFields();

        foreach ($fields as $field) {
            //what is this doing here and can we revmoe that?
            $this->setFieldName($field['fieldName']);
            $this->setFieldTitle($field['fieldTitle']);
            $field = $this->getFieldName();

            if (empty($field)) {
                $this->_error(self::MISSING_FIELD_NAME);

                return false;
            } elseif (!isset($context[$field])) {
                $this->_error(self::INVALID_FIELD_NAME);

                return false;
            } elseif (is_array($context)) {
                if ($value == $context[$field]) {
                    return true;
                }
            } elseif (is_string($context) && ($value == $context)) {
                return true;
            }
            $this->_error(self::NOT_MATCH);
        }
        return false;
    }
}
