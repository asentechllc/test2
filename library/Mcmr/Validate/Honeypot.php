<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @version    $Id$
 */

/**
 * Validation class for turning elements into spam honeypots
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @subpackage Honeypot
 */
class Mcmr_Validate_Honeypot extends Zend_Validate_Abstract
{
    const SPAM = 'spam';
    
    protected $_messageTemplates = array(
        self::SPAM  => "Spambot",
    );
    
    /**
     * Validate the element is empty
     *
     * @param string $value
     * @return bool 
     */
    public function isValid($value)
    {
        $value = (string)$value;
        $this->_setValue($value);
        
        if (!empty($value)) {
            $this->_error(self::SPAM);
            
            return false;
        } else {
            return true;
        }
    }
}
