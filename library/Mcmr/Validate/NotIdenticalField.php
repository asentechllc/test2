<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @version    $Id: IdenticalField.php 1250 2010-08-19 11:51:07Z leigh $
 */

/**
 * Validation class for validating two form fields do not have identical values
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @subpackage NotIdenticalField
 */
class Mcmr_Validate_NotIdenticalField extends Mcmr_Validate_IdenticalField
{
    const MATCH = 'match';
    const MISSING_FIELD_NAME = 'missingFieldName';
    const INVALID_FIELD_NAME = 'invalidFieldName';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
            self::MATCH =>
            'Not a unique value'
    );

    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if a field name has been set, the field name is available in the
     * context, and the value of that field name matches the provided value.
     *
     * @param  string $value
     *
     * @return boolean
     */
    public function isValid($value, $context = null)
    {
        if (parent::isValid($value, $context)) {
            $this->_error(self::MATCH);

            return false;
        } else {
            return true;
        }
    }
}
