<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @version    $Id: IdenticalField.php 1250 2010-08-19 11:51:07Z leigh $
 */

/**
 * Validation class for email addresses. Allows us to deny specific domains.
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @subpackage EmailAddress
 */
class Mcmr_Validate_EmailAddress extends Zend_Validate_EmailAddress
{
    
    protected $_denyHostnames = array();
    
    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if $value is a valid email address
     * according to RFC2822
     *
     * @link   http://www.ietf.org/rfc/rfc2822.txt RFC2822
     * @link   http://www.columbia.edu/kermit/ascii.html US-ASCII characters
     * @param  string $value
     * @return boolean
     */
    public function isValid($value) 
    {
        $valid = parent::isValid($value);
        
        if ($valid) {
            $valueDomain = array_pop(explode('@', $value));
            
            foreach ($this->_denyHostnames as $domain) {
                $domain = strtolower($domain);
                
                if ($domain == $valueDomain) {
                    $this->_error(self::INVALID_HOSTNAME);
                    $valid = false;
                }
            }
        }
        
        return $valid;
    }
    
    public function setOptions(array $options = array()) 
    {
        parent::setOptions($options);
        
        if (array_key_exists('denyHostnames', $options)) {
            $this->setDenyHostnames($options['denyHostnames']);
        }
    }
    
    /**
     *
     * @param array $hostnames
     * @return Mcmr_Validate_EmailAddress 
     */
    public function setDenyHostnames($hostnames)
    {
        if (is_object($hostnames) && $hostnames instanceof Zend_Config) {
            $hostnames = $hostnames->toArray();
        }
        
        if (!is_array($hostnames)) {
            throw new Zend_Validate_Exception("Hostname list is not an array");
        }
        
        $this->_denyHostnames = $hostnames;
        
        return $this;
    }
}
