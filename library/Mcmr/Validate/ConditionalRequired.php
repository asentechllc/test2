<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @author     Sean P. O. MacCath-Moran
 * @email      zendcode@emanaton.com
 * @website    http://www.emanaton.com
 * @copyright  This work is licenced under a Attribution Non-commercial Share Alike Creative Commons licence
 * @license    http://creativecommons.org/licenses/by-nc-sa/3.0/us/
 * @version    $Id: IdenticalField.php 2236 2011-03-07 11:20:20Z michal $
 */

/**
 * Validation class for validating two form fields have identical values
 *
 * @category Mcmr
 * @package Mcmr_Validate
 * @subpackage ConditionalRequired
 */
class Mcmr_Validate_ConditionalRequired extends Zend_Validate_Abstract
{
    const REQUIRED = 'required';

    /**
     * @var array
     */
    protected $_messageTemplates = array(
            self::REQUIRED =>
            'Because of the current %fieldTitle% value this field is required.'
    );

    /**
     * Name of the field as it appear in the $context array.
     *
     * @var string
     */
    protected $_fieldName;

    /**
     * Title of the field to display in an error message.
     *
     * If evaluates to false then will be set to $this->_fieldName.
     *
     * @var string
     */
    protected $_fieldTitle;

    protected $_fieldValue;

    public function __construct($options)
    {
        $this->setFieldName($options['fieldName']);
        $this->setFieldValue($options['fieldValue']);
        if (isset($options['fieldTitle'])) {
            $this->setFieldTitle($options['fieldTitle']);
        }
    }


    /**
     * Returns the field name.
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->_fieldName;
    }

    /**
     * Sets the field name.
     *
     * @param  string $fieldName
     * @return Zend_Validate_IdenticalField Provides a fluent interface
     */
    public function setFieldName($fieldName)
    {
        $this->_fieldName = $fieldName;
        return $this;
    }

    /**
     * Returns the field title.
     *
     * @return integer
     */
    public function getFieldTitle()
    {
        return $this->_fieldTitle;
    }

    /**
     * Sets the field title.
     *
     * @param  string:null $fieldTitle
     * @return Zend_Validate_IdenticalField Provides a fluent interface
     */
    public function setFieldTitle($fieldTitle = null)
    {
        $this->_fieldTitle = $fieldTitle ? $fieldTitle : $this->_fieldName;
        return $this;
    }


    public function getFieldValue()
    {
        return $this->_fieldValue;
    }


    public function setFieldValue($fieldValue)
    {
        $this->_fieldValue = $fieldValue;
        return $this;
    }

    /**
     * Defined by Zend_Validate_Interface
     *
     * Returns true if and only if a field name has been set, the field name is available in the
     * context, and the value of that field name matches the provided value.
     *
     * @param  string $value
     *
     * @return boolean
     */
    public function isValid($value, $context = null)
    {
        $this->_setValue($value);
        if (isset($context[$this->_fieldName]) && $context[$this->_fieldName]==$this->_fieldValue && !$value) {
            $this->_error(self::REQUIRED);
            return false;
        }
        return true;
    }
}
