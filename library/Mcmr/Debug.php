<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Debug
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Debug.php 2024 2011-01-10 16:46:58Z leigh $
 */

/**
 * A class for debugging system information. Ensures that debug information is never output in a
 * production environment.
 *
 * @category Mcmr
 * @package Mcmr_Debug
 */
class Mcmr_Debug
{
    private static $_logger = array();

    /**
     * Dump the variable to the logger. Default priority of Zend_Log::INFO
     * 
     * @param $var
     * @param $priority
     * @param $logger The logger type to use ('firebug'|'file')
     * @param $force Force the debug even if this is production
     * @return unknown_type
     */
    static public function dump($var, $priority = 6, $logger='file', $force = false)
    {
        // Only log if this is a dev/test environment or we are logging to file
        if ((defined('APPLICATION_ENV') && 'production' != APPLICATION_ENV) || $force) {
            $bt = debug_backtrace(false);

            $log = array('trace' => $bt[0]['file'].'('.$bt[0]['line'].')',
                'var' => $var
            );
            self::log($log, $priority, $logger, $force);

        }
    }
    
    static function dumpTrace($priority = 6, $logger='file', $force = false) 
    {
        $trace = debug_backtrace(); 
        $traceString = "Trace:";
        array_shift($trace);
        foreach ( $trace as $entry ) {
            $traceString.="\n";
            if (isset($entry['class'])) {
                $traceString.=$entry['class'].$entry['type'];
            }
            if (isset($entry['function'])) {
                $traceString.=$entry['function'];
            }
            $traceString.=" ";
            if (isset($entry['file'])) {
                $traceString.="{$entry['file']}({$entry['line']})";
            }
            
        }
        self::log( $traceString, $priority, $logger, $force);
        
    }
    
    static function log($log, $priority, $logger, $force) {
        switch ($logger) {
            case 'firebug':
                if (!isset(self::$_logger[$logger])) {
                    self::$_logger[$logger] = new Zend_Log(new Zend_Log_Writer_Firebug());
                }
                break;

            case 'file':
                if (!isset(self::$_logger[$logger])) {
                    self::$_logger[$logger] = new Zend_Log(
                        new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/debug.log')
                    );
                }
                $log = print_r($log, true);
                break;

        }

        self::$_logger[$logger]->log($log, $priority);
    }
}
