<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Calendar
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Cache.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 *
 * Event data
 *
 * @category Mcmr
 * @package Mcmr_Calendar
 */
class Mcmr_Calendar_Event
{
    private $_description;
    private $_location;
    private $_summary;
    //private $_organizer;
    private $_url;
    private $_startDate;
    private $_endDate;

    public function setDescription($description)
    {
        $this->_description = $description;
        return $this;
    }

//setDescription

    public function getDescription()
    {
        return $this->_description;
    }

//getDescription

    public function setLocation($location)
    {
        $this->_location = $location;
        return $this;
    }

//setLocation

    public function getLocation()
    {
        return $this->_location;
    }

//getLocation

    public function setSummary($summary)
    {
        $this->_summary = $summary;
        return $this;
    }

//setSummary

    public function getSummary()
    {
        return $this->_summary;
    }

//getSummary

    /*
      public function setOrganizer( $organizer ) {
      $this->_organizer = $organizer;
      return $this;
      } //setOrganizer

      public function getOrganizer() {
      return $this->_organizer;
      } //getOrganizer
     */

    public function setUrl($url)
    {
        $this->_url = $url;
        return $this;
    }

//setUrl

    public function getUrl()
    {
        return $this->_url;
    }

//getUrl

    public function setStartDate($startDate)
    {
        $this->_startDate = $startDate;
        return $this;
    }

//setStartDate

    public function getStartDate()
    {
        return $this->_startDate;
    }

//getStartDate

    public function setEndDate($endDate)
    {
        $this->_endDate = $endDate;
        return $this;
    }

//setEndDate

    public function getEndDate()
    {
        return $this->_endDate;
    }

//getEndDate
}

//Mcmr_Calendar_Event

