<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of ConfigNotFound
 *
 * @category Mcmr
 */
class Mcmr_Cron_Exception_ConfigNotFound extends Zend_Exception
{
}
