<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cron
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for Cron modules.
 *
 * @category Mcmr
 * @package Mcmr_Cron
 * @subpackage ModuleAbstract
 */
abstract class Mcmr_Cron_ModuleAbstract implements Mcmr_Cron_ModuleInterface
{
    protected $_config = null;
    protected $_consoleOpts = null;
    protected $_request = null;
    protected $_args = null;

    public function init()
    {
    }
    
    /**
     * Set the config for this module
     *
     * @param Zend_Config $config
     * @return Mcmr_Cron_ModuleAbstract 
     */
    public function setConfig(Zend_Config $config)
    {
        $this->_config = $config;

        return $this;
    }
    
    public function getConfig($parameter = null)
    {
        if (null !== $parameter) {
            if (is_object($this->_config->$parameter)) {
                return $this->_config->$parameter;
            } else {
                throw new Mcmr_Cron_Exception_ConfigNotFound("Configuration parameter '{$parameter}' not found");
            }
        } else {
            return $this->_config;
        }
    }

    /**
     * Set the console options
     *
     * @param Zend_Console_Getopt $options 
     */
    public function setConsoleOpts(Zend_Console_Getopt $options)
    {
        $this->_consoleOpts = $options;
    }
    
    public function getRequest()
    {
        return $this->_request;
    }

    public function setRequest($request)
    {
        $this->_request = $request;
        
        return $this;
    }
    
    public function setArgs($args)
    {
        $this->_args = $args;
        
        return $this;
    }
    
}
