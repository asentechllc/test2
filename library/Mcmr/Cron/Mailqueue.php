<?php

class Mcmr_Cron_Mailqueue extends Mcmr_Cron_ModuleAbstract
{
    public function execute()
    {
        $this->_processRegistered();
        $this->_processPending();
        //$this->_processEvents();
    }

    private function _processPending()
    {
        $mapper = Mailqueue_Model_Mail::getMapper();
        $emails = $mapper->findAllByField(
            array('status'=>  Mailqueue_Model_Mail::STATUS_PENDING), 
            null,
            array('page'=>1, 'count'=>1000000)
        );
        echo $emails->getCurrentItemCount()." pending emails found.\n";

        foreach ($emails as $email) {
            try {
                $email->addRecipientsFromUsers();
                $email->register();

                $mapper->save($email);

                echo "{$email->getSubject()} registered\n";
            } catch (Mcmr_Service_Pure360_SearchException $e) {
                $log = new Zend_Log(new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/mailqueue.log'));
                $log->log(
                    "Unable to register {$email->getSubject()}. Caught error '{$e->getMessage()}'. Ignoring\n",
                    Zend_Log::ERR
                );
            } catch (Exception $e) {
                $log = new Zend_Log(new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/mailqueue.log'));
                $log->log(
                    "Unable to register {$email->getSubject()}. Caught error '{$e->getMessage()}'\n",
                    Zend_Log::ERR
                );
                $log->log(print_r($e->getErrors(), true), Zend_Log::ERR);
                
                $email->setStatus(Mailqueue_Model_Mail::STATUS_FAILURE);
                $email->setAttribute('error', $e->getMessage());
                $mapper->save($email);
            }
        }

        echo "Done\n";
    }

    /**
     * Process all registered emails and set the send time.
     */
    private function _processRegistered()
    {
        $mapper = Mailqueue_Model_Mail::getMapper();
        $emails = $mapper->findAllByField(
            array('status'=>  Mailqueue_Model_Mail::STATUS_REGISTERED),
            null,
            array('page'=>1, 'count'=>1000000)
        );
        echo $emails->getCurrentItemCount()." registered emails found.\n";

        foreach ($emails as $email) {
            try {
                $email->send();

                $mapper->save($email);

                echo "\"{$email->getSubject()}\" queued\n";
            } catch (Mcmr_Service_Pure360_SearchException $e) {
                $log = new Zend_Log(new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/mailqueue.log'));
                $log->log(
                    "Unable to register {$email->getSubject()}. Caught error '{$e->getMessage()}'. Ignoring\n",
                    Zend_Log::ERR
                );
            } catch (Exception $e) {
                $log = new Zend_Log(new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/mailqueue.log'));
                $log->log(
                    "Unable to process \"{$email->getSubject()}\". Caught error '{$e->getMessage()}' {$e->getFile()}({$e->getLine()})\n",
                    Zend_Log::ERR
                );

                $email->setStatus(Mailqueue_Model_Mail::STATUS_FAILURE);
                $email->setAttribute('error', $e->getMessage());
                $mapper->save($email);

                $fullMessage = Mcmr_Service_Pure360_PaintSession::convertResultToDebugString($e->getErrors());
                $email->setAttribute('error', $fullMessage);
                $mapper->save($email);
                $log->log($fullMessage, Zend_Log::ERR);
            }
        }

        echo "Done\n";
    }

    private function _processEvents()
    {
        $config = Zend_Registry::get('mailqueue-config');

        foreach ($config->profiles as $profile) {
            $service = new Mcmr_Service_Pure360($profile->options);
            $service->login();

            //@TODO
            //var_dump($service->retrieveEventNotifications("CLICK,OPEN,BOUNCE,OPTOUT,OPTIN,DELIVERY", 5, "", ""));
        }
    }
}
