<?php

/**
 * Cron script to flush full-page cache for whatever reason
 * Should run every 5 minutes
 * 
 *	- Check for future-dated posts
 * 
 **/

class Mcmr_Cron_FlushCache extends Mcmr_Cron_ModuleAbstract
{

    public function execute()
    {    
    
		if($this->_flushRequired()){

			echo "Flushing website full-page cache...";

			# need to find an admin user for the access key, any will do
			$user = User_Model_User::getMapper()->findOneByField(array('role'=>'admin'));
	
			if(empty($user))
				throw new Exception("FlushCache Error: Could not find admin user for access token");
				
			Zend_Registry::set('authenticated-user',$user);
			
			Mcmr_Cache::flushWeb();
			
			echo "done." . PHP_EOL;
				
		
		}
		else
			echo "No flush required at this time" . PHP_EOL;
		
    }
    
    /**
     * Do we need to flush the cache loo?
     **/
    private function _flushRequired(){
    
    	foreach(array(
    		'_countFutureDated' => 'future-dated posts',
    	) as $fn => $desc){
    	
    		echo "Checking for {$desc}...";
    		
    		$n = $this->$fn();

   			echo (is_numeric($n) ? "$n found." : ($n ? "found." : "none.")) . PHP_EOL;
    	
    		if($n){
    			return true;
    		}
    	}
    		
    }
    
    private function _countFutureDated(){
   		$db = News_Model_Article::getMapper()->getDbTable()->getAdapter();
   		$date = date('Y-m-d H:i:s',strtotime('-5 minutes'));
   		return $db->query("SELECT COUNT(*) FROM news_articles WHERE article_publishdate >= ? AND article_publishdate < NOW()",array($date))->fetchColumn();
    }
    
}
