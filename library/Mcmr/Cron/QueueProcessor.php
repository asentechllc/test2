<?php

class Mcmr_Cron_QueueProcessor extends Mcmr_Cron_ModuleAbstract
{

    protected function getSleep()
    {
        $sleep = 1;
        if ( $this->_config->queue && $this->_config->queue->sleep ) {
            $sleep = (int)$this->_config->queue->sleep;
        }
        return $sleep;
    }
    
    protected function getBatch()
    {
        $batch = 50;
        if ( $this->_config->queue && $this->_config->queue->batch ) {
            $batch = (int)$this->_config->queue->batch;
        }
        return $batch;
    }

    protected function getQueue() 
    {
        $name = 'observer';
        if ( $this->_config->queue && $this->_config->queue->name ) {
            $name = $this->_config->queue->name;
        }
        $queue = Mcmr_Queue::getInstance( $name );
        return $queue;
    }
    
    public function execute()
    {
        $queue = $this->getQueue();
        if (is_object($queue)) {
            while (true) {
                $this->_processQueue($queue);
                sleep($this->getSleep());
            }
        }
    }
    
    protected function _processQueue($queue)
    {
        $messages = $queue->receive($this->getBatch());
        foreach ($messages as $message) {
            $object = unserialize($message->body);
            $event = $object->event;
            if (method_exists($object->observer, $event)) {
                try {
                    $model = $object->model;
                    if (!is_object($object->model) && isset($object->modelClass) && isset($object->modelId)) {
                        $mapper = call_user_func(array($object->modelClass, 'getMapper'));
                        $model = $mapper->find($object->modelId);
                    }
                    $object->observer->$event($model);

                    $queue->deleteMessage($message);
                } catch (Exception $e) {
                    //@TODO handle error
                }
            }
        }
    }
}
