<?php

class Mcmr_Cron_StatQueueProcessor extends Mcmr_Cron_QueueProcessor
{
    
    protected function _processQueue($queue)
    {
        $batched = array();
        $messages = $queue->receive($this->getBatch());
        foreach ($messages as $message) {
            $body = unserialize($message->body);
            if ( !$body ) {
                continue;
            }
            
            $key = new stdClass();
            $key->modelClass = $body->modelClass;
            $key->modelId = $body->modelId;
            //remove the original date. assume all stats were redcorded today. will not cause much difference and will ensure batching works
            $key->date = strtotime('today');
            $key->action = $body->action;
            $key->site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
            
            $keyString = serialize($key);
            
            if (isset($batched[$keyString])) {
                $batched[$keyString] = $batched[$keyString] + 1;
            } else {
                $batched[$keyString] = 1;
            }
            $queue->deleteMessage($message);
        }
        
        foreach ($batched as $key => $count) {
            $key = unserialize($key);
            $this->_processStat($key, $count);
            
        }
    }
    
    protected function _processStat($key, $count) 
    {
        $mapper = Default_Model_Stat::getMapper();
        $stat = $mapper->findOneByField(
            array(
                'modeltype'=> $key->modelClass,
                'modelid'=> $key->modelId, 
                'date'=> $key->date, 
                'site'=> $key->site,
                'action'=> $key->action,
            )
        );
        
        $mapper->increment($stat, $count);
    }
}
