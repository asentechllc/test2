<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cron
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron module interface
 *
 * @category Mcmr
 * @package Mcmr_Cron
 * @subpackage ModuleInterface
 */
interface Mcmr_Cron_ModuleInterface
{
    public function setConfig(Zend_Config $config);
    public function setConsoleOpts(Zend_Console_Getopt $options);
    public function execute();
}
