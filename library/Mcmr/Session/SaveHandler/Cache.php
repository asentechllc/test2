<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Session
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Cache.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * A Session handler using a Zend_Cache object as the storage mechanism
 *
 * @category Mcmr
 * @package Mcmr_Session
 * @subpackage SaveHandler
 */
class Mcmr_Session_SaveHandler_Cache implements Zend_Session_SaveHandler_Interface
{
    private $_maxlifetime = 3600;
    public $cache = '';

    public function __construct(Zend_Cache_Core $cacheHandler)
    {
        $this->cache = $cacheHandler;
    }

    public function open($savePath, $name)
    {
        return true;
    }

    public function close()
    {
        return true;
    }

    public function read($id)
    {
        if (! ($data = $this->cache->load($id))) {
            return '';
        } else {
            return $data;
        }
    }

    public function write($id, $sessionData)
    {
        $this->cache->save($sessionData, $id, array(), $this->_maxlifetime);
        
        return true;
    }

    public function destroy($id)
    {
        $this->cache->remove($id);
        return true;
    }

    public function gc($notusedformemcache)
    {
        return true;
    }
}
