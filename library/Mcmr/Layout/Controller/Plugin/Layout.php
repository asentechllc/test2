<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Layout
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Change the layout file based on the site ID passed from the request.
 * 
 * @category Mcmr
 * @package Mcmr_Layout
 */
class Mcmr_Layout_Controller_Plugin_Layout extends Zend_Layout_Controller_Plugin_Layout
{
    protected $_layoutName=null;

    public function preDispatch(Zend_Controller_Request_Abstract $request) 
    {
        $this->getLayout()->setLayout($this->_getLayoutName());
    }

    protected function _getLayoutName() {
        if (null===$this->_layoutName) {
            $request = $this->getRequest();
            $layout = $this->getLayout();
            $this->_layoutName = $layout->getLayout();
            if (method_exists($request, 'getSiteName') && 'default' !== $request->getSiteName()) {
                $this->_layoutName = $request->getSiteName();
            }
            if ($layout->getDeviceLayouts() && 'desktop'!=($device=$request->getDeviceClass())) {
                $deviceLayoutName = $this->_layoutName.'.'.$device;
                if ($layout->layoutExists($deviceLayoutName)) {
                    $this->_layoutName  = $deviceLayoutName;
                }
            }
        }
        return $this->_layoutName;
    }
    
}
