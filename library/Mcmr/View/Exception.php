<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Exception.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Mcmr View Exception
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Exception
 */
class Mcmr_View_Exception extends Zend_View_Exception
{
}
