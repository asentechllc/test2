<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: EmailEncode.php 1234 2010-08-16 15:39:23Z jay $
 */

/**
 * This is a bit of a sledgehammer to remove all potentially hamful
 * characters from a a string that is meant to go into a sitemap.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_SitemapEncode extends Zend_View_Helper_Abstract
{
    /**
     * Removes all non-ascii characters from it and then does XML-compatible quoting of amps and angle brackets
     *
     * @param string $text
     * @return string
     */
    public function sitemapEncode($text)
    {
        $text = @iconv("UTF-8", "ASCII//IGNORE//TRANSLIT", $text);
        $text = htmlspecialchars($text);
        return $text;
    }
}
