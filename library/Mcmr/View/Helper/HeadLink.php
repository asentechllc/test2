<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package  View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * An extension of Zend built in HeadLink helper which appends file modification timestamp to the href to prevent caching of modified stylesheets.
 *
 * @category Mcmr
 * @package  View
 * @subpackage Helper
 */
class Mcmr_View_Helper_HeadLink extends Zend_View_Helper_HeadLink
{
    protected $_sitePath;

    public function __construct()
    {
        parent::__construct();
        $this->_sitePath = SITE_PATH.'/public';
    }

    public function createData(array $attributes)
    {
        if (isset($attributes['href'])) {
            $attributes['href'] = Mcmr_StdLib::timestampRelativeLink($attributes['href']);
        }
        if (isset($attributes['src'])) {
            $attributes['src'] = Mcmr_StdLib::timestampRelativeLink($attributes['src']);
        }
        return parent::createData($attributes);
    }

}
