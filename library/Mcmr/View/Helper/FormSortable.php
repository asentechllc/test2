<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @copyright Copyright (c) 2009 Soflomo V.O.F. (http://www.soflomo.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 * @author jurian
 */

/**
 * Sozfo View Helper FormTinyMce
 *
 * The rendering of the view element. Using the TinyMce view helper javascript
 * initiazion.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @subpackage Helper
 */
class Mcmr_View_Helper_FormSortable extends Zend_View_Helper_FormElement
{
    public function FormSortable ($name, $value = null, $attribs = null)
    {
    
    	if(!is_array($value))
    		$value = array($value);
    	else if(!$value)
    		$value[] = '';
    
    	$name .= '[]';

        $html = '<div class="sortable-list">';
        
        $html .= '<div class="list-labels">';
        
        $i = 1;
        foreach($value as $v){
	        $html .= '<label>Position '.$i.'</label>';
	        $i++;
        }
        $html .= '</div>'; // list-labels
        
        $html .= '<div class="list-elements">';
        
        foreach($value as $v){
			$html .= '<div class="element">';
			$html .= '<input class="category" type="text" name="'.$name.'" value="'.$this->view->escape($v).'" />';

			$html .= '<div class="controls">';
			$html .= '<a class="add" href="#">Add</a>';
			$html .= '<a class="remove" href="#">Remove</a>';
			$html .= '</div>'; // controls

			$html .= '</div>'; // element
		}
        
        $html .= '</div>'; // list-elements
        
        $html .= '</div>'; // sortable-list
        
        return $html;
    }
}