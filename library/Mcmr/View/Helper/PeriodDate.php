<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Image.php 1376 2010-09-13 13:06:27Z leigh $
 */

/**
 * A view helper for converting a unix timestamp into a human readable date eg '2 weeks ago'
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_PeriodDate extends Zend_View_Helper_Abstract
{

    /**
     * Return a human readable date
     *
     * @param int $timestamp
     * @param string $format
     * @param string $specificFormat
     * @return type 
     */
    public function periodDate($timestamp, $format=null, $specificFormat=null)
    {
        if (null === $timestamp) {
            return '';
        }
        
        switch ($format) {
            case 'year':
                $res = date('Y', $timestamp);
                break;
            
            case 'quarter':
                $quarter = floor((date('n', $timestamp) - 1) / 3) + 1;
                $res = 'Q' . $quarter . date(' Y', $timestamp);
                break;

            case 'month':
                $res = date('F Y', $timestamp);
                break;

            case 'specific':
            default:
                if (null === $specificFormat) {
                    $specificFormat = 'j F Y';
                }
                $res = date($specificFormat, $timestamp);
        }
        
        return $res;
    }

}
