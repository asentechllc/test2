<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package  View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Performs the same task as the Url view helper but takes a gossip model and converts it to
 * an array to be used in the URL and generates an 'SEO' friendly ID.
 *
 * @category Mcmr
 * @package  View
 * @subpackage Helper
 */
class Mcmr_View_Helper_GossipUrl extends Zend_View_Helper_Abstract
{
    /**
     * List of options to include as url parameters
     */
    protected $_optionsList = array('url', 'id', 'sectionUrl');
    
    const route_invalidator = '__dummy_route__';

    public function gossipUrl(array $urlOptions = array(), $name = null, $reset = false, $encode = true)
    {
        if (isset($urlOptions['model'])
                && is_object($urlOptions['model'])
                && $urlOptions['model'] instanceof Mcmr_Model_ModelAbstract) {

            if (null === $name || 'default' === $name) {
                // We must have a route if using a model, otherwise all model data can be exposed in the url.
                throw new Zend_View_Exception("Model has been provided but no route name provided.");
            }
            $model = $urlOptions['model'];
            $modelOptions = $this->_getModelOptions($model);
            
            if(!empty($modelOptions['sectionUrl']))
	            $modelOptions['sectionUrl'] = array_pop(explode('/',$modelOptions['sectionUrl']));
	            
            $urlOptions = array_merge($urlOptions, $modelOptions);
            // Create an ID to use for SEO. Google rules say cannot start with '200' or '199' 
            // and must be at least 3 digits
            $seoId = $model->getId();
            if (!is_array($seoId)) {
                $seoId = intval($seoId)+99;
                $urlOptions['seoId'] = '0'.$seoId;
            }

        }
        
        return str_replace(self::route_invalidator,'',$this->view->url($urlOptions, $name, $reset, $encode));
    }
    
    public function modelId($url) {
        $parts=explode('/', $url );
        $id=intval($parts[count($parts)-1])-99;
        return $id;
    }

    protected function _getModelOptions($model) {
        $options = array();
        $router = Zend_Controller_Front::getInstance()->getRouter();
        if (!$router instanceof Mcmr_Controller_Router_Development) {
            //original implementation used $módel->getOptions(true)
            //qureying all model options for url is a bit ridiculous, as some of them are calculated and trigger
            //database requests
            //also we don't use anything other than url and id anywhere so there is no point.
            foreach ($this->_optionsList as $optionName) {
                $methodName = 'get'.ucfirst($optionName);
                if (method_exists($model, $methodName)) {
                    $options[$optionName] = $model->{$methodName}();
                }
            }
        } else {
            $options['id']=$model->getId();
        }
        return $options;
    }
}
