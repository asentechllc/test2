<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Request.php 1723 2010-11-03 16:18:08Z michal $
 */

/**
 * A view helper to get a request parameter. Optional filters and validators
 * can be used to sanitise the value
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Request extends Zend_View_Helper_Abstract
{
    private $_request = null;
    
    public function __construct()
    {
        if (null === $this->_request) {
            $front = Zend_Controller_Front::getInstance();
            $this->_request = $front->getRequest();
        }
    }

    /**
     * Caputure all function calls and forward them to the request object for any
     * functions we haven't overwritten
     *
     * @param callback $name
     * @param mixed $arguments
     */
    public function __call($name, $arguments)
    {
        return call_user_func_array(array($this->_request, $name), $arguments);
    }
    
    public function request()
    {
        return $this;
    }

    /**
     * Get the valid of a request parameter. If filters are provided they will be
     * used to filter the value first. If a validator or validator chain is provided
     * it will be used to validate the value. If validation fails the default value
     * is returned
     *
     * @param string $key
     * @param string $default
     * @param array|Zend_Filter_Interface $filters
     * @param Zend_Validate_Abstract $validator
     * @return mixed
     */
    public function getParam($key, $default=null, $filters=null, Zend_Validate_Abstract $validator = null)
    {
        // Get the value and filter it
        $value = $this->_request->getParam($key, $default);
        $value = $this->_filter($value, $filters);

        // Validate the value. If not valid set to default
        if (null !== $validator) {
            if (!$validator->isValid($value)) {
                $value = $default;
            }
        }

        return $value;
    }

    public function isPost()
    {
        return $this->_request->isPost();
    }

    /**
     * Filter the value with the provided filters
     *
     * @param string $value
     * @param array|Zend_Filter_Interface $filters
     * @return mixed
     */
    protected function _filter($value, $filters)
    {
        if (null !== $filters) {
            if (!is_array($filters)) {
                $filters = array($filters);
            }
        } else {
            $filters = array();
        }

        foreach ($filters as $filter) {
            if (!$filter instanceof Zend_Filter_Interface) {
                throw new Zend_View_Exception('Filter not an instance of Zend_Filter_Interface');
            }

            $value = $filter->filter($value);
        }

        return $value;
    }
}
