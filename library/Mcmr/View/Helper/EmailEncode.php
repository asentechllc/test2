<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: EmailEncode.php 1234 2010-08-16 15:39:23Z jay $
 */

/**
 * Description of EmailEncode
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_EmailEncode extends Zend_View_Helper_Abstract
{

    /**
     * Takes an email address and converts it to an encoded string to make email
     * scrapping harder
     *
     * @param string $emailAddress
     * @param string $method
     * @return string
     */
    public function emailEncode($emailAddress, $method='hex')
    {
        switch ($method) {
            case 'hex':
                $encoded = $this->_hexEncode($emailAddress);
                break;

            case 'slashed':
                $encoded = $this->_slashedEncode($emailAddress);
                break;

            default:
                throw new Zend_View_Exception("Unknown encode method '{$method}'");
        }

        return $encoded;
    }

    /**
     * Encode the email address as a hex encoded string
     *
     * @param string $emailAddress
     * @return string
     */
    private function _hexEncode($emailAddress)
    {
        $encoded = '';
        for ($i = 0; $i < strlen($emailAddress); $i++) {
            $encoded .= '&#' . ord($emailAddress[$i]) . ';';
        }

        return $encoded;
    }

    /**
     * Encode the email address as a hex encoded string
     *
     * @param string $emailAddress
     * @param string $$filterLevel
     * @return string
     */
    private function _slashedEncode($emailAddress, $filterLevel = 'normal')
    {
        $emailAddress = strrev($emailAddress);
        $emailAddress = preg_replace('[@]', '//', $emailAddress);
        $emailAddress = preg_replace('[\.]', '/', $emailAddress);

        if ($filterLevel == 'low') {
            $emailAddress = strrev($emailAddress);
        }

        return $emailAddress;
    }

}
