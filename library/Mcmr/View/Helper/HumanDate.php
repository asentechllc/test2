<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Image.php 1376 2010-09-13 13:06:27Z leigh $
 */

/**
 * A view helper for converting a unix timestamp into a human readable date eg '2 weeks ago'
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_HumanDate extends Zend_View_Helper_Abstract
{

    public function humanDate($timestamp, $format= 'human' )
    {
        if (null === $timestamp) {
            return '';
        }
        $method = '_format_'.$format;
        
        return $this->$method( $timestamp );
    }
    
    protected function _format_human($timestamp)
    {
        $difference = time() - $timestamp;
        $periods = array("sec", "min", "hour", "day", "week",
            "month", "years", "decade");
        $lengths = array("60", "60", "24", "7", "4.35", "12", "10");

        if ($difference > 0) { 
            // this was in the past
            $ending = "ago";
        } else {
            // this was in the future
            $difference = -$difference;
            $ending = "to go";
        }

        for ($j = 0; $difference >= $lengths[$j]; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);
        if ($difference != 1) {
            $periods[$j].= "s";
        }

        $text = "$difference $periods[$j] $ending";

        return $text;
    }
    
    protected function _format_long_date($timestamp)
    {
        return date('j F Y', $timestamp);
    }
}
