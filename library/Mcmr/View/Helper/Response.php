<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A view helper to get the response object
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Response extends Zend_View_Helper_Abstract
{
    /**
     * Fetch the zend controller response object.
     *
     * @return Zend_Controller_Response_Abstract
     */
    public function response()
    {
        $front = Zend_Controller_Front::getInstance();
        return $front->getResponse();
    }
}
