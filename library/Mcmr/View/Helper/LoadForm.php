<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Loads a specific module's form. To allow module forms to be displayed on any page.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_LoadForm extends Zend_View_Helper_Abstract
{

    public function loadForm(array $form, $options=null)
    {
        if (!isset($form['module'])) {
            throw new Zend_View_Exception("form must supply a module name");
        }
        if (!isset($form['name'])) {
            throw new Zend_View_Exception("form must supply a form name");
        }

        $className = ucfirst(strtolower($form['module']))
            . '_Form_'
            . ucfirst($form['name']);

        if (!class_exists($className)) {
            throw new Zend_View_Exception("Form does not exist");
        }

        return new $className($options);
    }
}