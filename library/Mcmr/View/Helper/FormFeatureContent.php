<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @copyright Copyright (c) 2009 Soflomo V.O.F. (http://www.soflomo.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 * @author jurian
 */

/**
 * Sozfo View Helper FormTinyMce
 *
 * The rendering of the view element. Using the TinyMce view helper javascript
 * initiazion.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @subpackage Helper
 */
class Mcmr_View_Helper_FormFeatureContent extends Zend_View_Helper_FormHidden
{
    protected $_tinyMce;

    public function FormFeatureContent ($name, $value = null, $attribs = null)
    {

        $info = $this->_getInfo($name, $value, $attribs);
        extract($info); // name, value, attribs, options, listsep, disable

		$this->view->headScript()->appendFile('/assets/scripts/gossip3.feature.js', 'text/javascript'); 

		$xhtml = '<input type="hidden" id="feature_id" value="'.$this->view->model->id.'" />';
		
		$types = array(
			'block_copy' => array(
				'title' => 'Copy',
				'elements' => array(
					'copy' => array('type'=>'copy'),
				),
			),
			'block_copyimageright' => array(
				'title' => 'Copy + Image (right)',
				'elements' => array(
					'image' => array('type'=>'image','description'=>'480 pixels wide'),
					'copy' => array('type'=>'copy'),
				),
			),
			'block_copyimageleft' => array(
				'title' => 'Copy + Image (left)',
				'elements' => array(
					'image' => array('type'=>'image','description'=>'480 pixels wide'),
					'copy' => array('type'=>'copy'),
				),
			),
			'block_fullimage' => array(
				'title' => 'Full-width Image',
				'elements' => array(
					'image' => array('type'=>'image','description'=>'980 pixels wide'),
				),
			),
			'block_quote' => array(
				'title' => 'Quote',
				'elements' => array(
					'copy' => array('type'=>'copy'),
				),
			),
			'block_gallery' => array(
				'title' => 'Gallery',
				'elements' => array(
					'gallery' => array('type'=>'gallery','description'=>'980 pixels wide by 490 pixels tall'),
				),
			),
		);
		
		# block add-links
		$xhtml .= '<div class="feature_help">Use the <b>Add Block</b> button to add content blocks. You can arrange the blocks by dragging them to the desired position.</div>';
		$xhtml .= '<div class="feature_toolbar feature_toolbar_first">';
		$xhtml .= '<a href="#" class="button remove"><span><span>Remove Block</span></span></a>';
		$xhtml .= '<div class="feature_block_types"><a href="#" class="button"><span><span>Add Block</span></span></a><ul>';
		foreach($types as $id=>$type){
			$xhtml .= '<li><a class="'.$id.'_link" href="#">'.$type['title'].'</a></li>';
		}
		$xhtml .= '</ul></div>';
		//$xhtml .= '<a href="#" class="button moveup"><span><span>Move Up</span></span></a>';
		//$xhtml .= '<a href="#" class="button movedown"><span><span>Move Down</span></span></a>';
		$xhtml .= '<div class="clear"></div>';
		$xhtml .= '</div>';
		
		$xhtml .= '<div id="feature_canvas">';
		$xhtml .= '</div>'; // feature_canvas

		# hidden blocks
		
		$xhtml .= '<div class="feature_blocks" style="display:none">';
		
		foreach($types as $id=>$type){
		
			$blockName = $name.'[new]';

			# external wrapper
			$xhtml .= '<div class="'.$id.'">';
			
			$xhtml .= '<div class="block">';
			$xhtml .= '<h3>'.$type['title'].'</h3>';
			$xhtml .= '<input type="hidden" name="'.$this->view->escape($blockName.'[type]').'" value="'.$this->view->escape($id).'" />';
			
			# render elements
			foreach($type['elements'] as $el){
			
				$xhtml .= '<div class="formrow clearfix">';
				
				switch($el['type']){

					case 'copy':
						$xhtml .= '<label>Copy</label>';
						$xhtml .= '<textarea name="' . $this->view->escape($blockName.'[copy]') . '"'
								/*. ' id="' . $this->view->escape($id) . '"'
								. $disabled*/
								. $this->_htmlAttribs($attribs) . '>'
								. $this->view->escape('') . '</textarea>';
						break;

					case 'image':
						$xhtml .= '<label>Image</label>';
						$xhtml .= '<input class="file-chooser"'.(isset($info['attribs']['imagePath']) ? ' title="'.$this->view->escape($info['attribs']['imagePath']).'"' : '').' type="text" name="'.$this->view->escape($blockName.'[image]').'" value="'.$this->view->escape('').'" />';
						break;

					case 'gallery':
						$xhtml .= '<label>Gallery</label>';
						$xhtml .= '<input class="dir-chooser"'.(isset($info['attribs']['galleryPath']) ? ' title="'.$this->view->escape($info['attribs']['galleryPath']).'"' : '').' type="text" name="'.$this->view->escape($blockName.'[gallery]').'" value="'.$this->view->escape('').'" />';
						$xhtml .= '<input class="dir-chooser-data" type="hidden" value="'.$this->view->escape('').'" name="'.$this->view->escape($blockName.'[data]').'" />';
						break;

				}
				
				if(!empty($el['description'])){
					$xhtml .= '<div class="description">'.$el['description'].'</div>';
				}

				$xhtml .= '</div>';

			}
			
			$xhtml .= '</div>';
			
			# close external wrapper
			$xhtml .= '</div>';
		
		}
		
		$xhtml .= '</div>';

		if(is_array($value)){

			$xhtml .= '<script type="text/javascript">';
			$xhtml .= 'var feature_blocks = [';
			$blocks = '';
			foreach($value as $val){
				$block = '';
				if(is_array($val)){
					foreach($val as $k=>$v){
						# make content json-friendly
						$v = str_replace('"','\\"',$v);
						$v = preg_replace('/\r{0,1}\n/','\n',$v);
						$block .= ($block ? ',' : '') . $k.':"'.$v.'"';
					}
				}
				$blocks .= ($blocks ? ',' : '').'{'.$block.'}';
			}
			$xhtml .= $blocks;
			$xhtml .= '];';
			$xhtml .= '</script>';
		
		}
		
		$this->view->featureContent()->setOptions($attribs['editorOptions']);
        $this->view->featureContent()->render();

        return $xhtml;
    }
}