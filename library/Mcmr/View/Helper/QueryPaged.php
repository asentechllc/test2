<?php

class Mcmr_View_Helper_QueryPaged extends Zend_View_Helper_Abstract
{
    public function queryPaged($query, $model, $module=null, $config = null, $filteredIds = null)
    {
		$page = $this->view->request()->getParam('page', 1);
		
		if(is_string($query) && $page > 1){
			# do we have a config to use alternate JSON count?
			$cfg = $this->view->config($module)->toArray();

			$count = isset($cfg['queries'][$query]['page']['count']) ? $cfg['queries'][$query]['page']['count'] : -1;

			if(!empty($cfg['queries'][$query]['page']['countMore'])){
				
				$countJson = $cfg['queries'][$query]['page']['countMore'];

				$query = $this->view->queryBuild($query,$model,$module,$config);

				# fetch first page of results so we know what to exclude
				if(!is_array($filteredIds))
					$filteredIds = array();

				$filteredQuery = $query->cloneQuery()
					->addPage(array('page'=>1));
			
				if($filteredIds){
					$filteredQuery->addConditions(array(
						'id' => array(
							'condition' => 'not in',
							'value' => $filteredIds,
						),
					));
				}
				
				$filteredModels = $this->view->queryAll($filteredQuery,$model,$module);
				foreach($filteredModels as $_model){
					$filteredIds[] = $_model->getId();
				}

				if($filteredIds){
					$query->addPage(array(
						'page'=>($page-1),
						'count' => $countJson,
					));
				
					$query->addConditions(array(
						'id' => array(
							'condition' => 'not in',
							'value' => $filteredIds,
						),
					));
				}

			}
			
			# standard query without alternate page counts
			else{
				$query = $this->view->queryBuild($query,$model,$module,$config);
				$query->addPage(array(
					'page'=>$page,
					'count' => $count,
				));
			}
			
	    }
	    
		return $this->view->queryAll($query, $model, $module, $config);
	    
    }
}
