<?php

class Mcmr_View_Helper_QueryBuild extends Zend_View_Helper_Abstract
{
	/**
	 * $query to be:
	 	 string -> then factory a query based on named config
	 	 query object -> use that
	 	 array -> do not have array to make it explicit providing parameters is dicourages, will have to go through bulding a query
	 * $model: model name, is string then derive from current controller name, needs mapping to refletc that index controller uses articles model
	 * $module: module name, can derive from current module name
	 */
    public function queryBuild($query, $model, $module=null, $config = null)
    {
    	if (null===$module) {
    		$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
    	}
        $models = Mcmr_Model::queryBuild($query, $model, $module, $config);
        return $models;
    }
}
