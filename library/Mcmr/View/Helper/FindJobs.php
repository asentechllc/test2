<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A view helper that sets a redirect the user. Unlike the 'redirect' helper this helper
 * will redirect the user immediately.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_FindJobs extends Zend_View_Helper_Abstract
{

    /**
     * Redirect the user to the supplied location. 
     *
     * @param string $location
     * @param int $timeout 
     */
    public function findJobs($fn='latest',$limit=5)
    {
    	$_fn = '_find'.$fn;
    	if(method_exists($this,$_fn))
    		return $this->$_fn($limit);
    		
    	throw new Exception("Job scope not found: $fn");
    }
    
    private function _findLatest($limit){
    	return Job_Model_Job::getMapper()->findAllByField(array(
    		'published' => 1,
    		'date' => array(
    			'condition' => '<',
    			'value' => strtotime(date("Y-m-d H:00:00")),
    		),
    	),array(
    		'date' => 'ASC',
    		'title' => 'ASC',
    	),array(
    		'page' => 1,
    		'count' => $limit,
    	));
    }
}
