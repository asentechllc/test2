<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 */

/**
 * A View helper to add anaylitics to the website
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Analytics extends Zend_View_Helper_Abstract
{
    
    
    
    /**
     * Add google analytics to the website headScript.
     * This will create the analytics with the necessary code for the Cookie Consent
     * scripts. The headScript will 
     *
     * @param string $account 
     */
    public function analytics($account)
    {
        $this->view->headScript()
            ->appendScript(" var _gaq = _gaq || [];\n"
                    ." _gaq.push(['_setAccount', '{$account}']);\n"
                    ." _gaq.push(['_trackPageview']);"
                    ." (function() {\n"
                    ."   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;\n"
                    ."   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';\n"
                    ."   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);\n"
                    ." })();\n",
                'text/plain', 
                array('class'=>'cc-onconsent-inline-social')
            );

        return '';
    }
}