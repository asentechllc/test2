<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A view helper that sets a redirect the user. Unlike the 'redirect' helper this helper
 * will redirect the user immediately.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_FindArticles extends Zend_View_Helper_Abstract
{

    const TIME_ROUNDING = 3600;
	private $_type;


    /**
     * Redirect the user to the supplied location. 
     *
     * @param string $location
     * @param int $timeout 
     */
    public function findArticles($fn,$limit=5)
    {
		$this->_type = News_Model_Type::getMapper()->findOneByField(array('url'=>'news'));

    	$_fn = '_find'.$fn;
    	if(method_exists($this,$_fn))
    		return $this->$_fn($limit);
    		
    	throw new Exception("Article scope not found: $fn");
    }
    
    private function _findRecommended($limit){
    	return News_Model_Article::getMapper()->findAllByField(array(
    		//'type_id' => $this->_type->getId(),
    		'published' => 1,
    	),array(
    		'ordr_recommended' => array(
    			'dir' => 'DESC',
    			'join' => 'inner',
    		),
    		//'ordr_recommended' => 'DESC',
    	),array(
    		'page' => 1,
    		'count' => $limit,
    	));
    }

    private function _findLatest($limit){
    	return News_Model_Article::getMapper()->findAllByField(array(
    		'type_id' => $this->_type->getId(),
    		'published' => 1,
    	),array(
    		'publishdate' => 'DESC',
    	),array(
    		'page' => 1,
    		'count' => $limit,
    	));
    }
    
    private function _findMostRead($limit){return $this->_byStat('read',$limit);}
    private function _findMostCommented($limit){return $this->_byStat('comment',$limit);}
    private function _findMostEmailed($limit){return $this->_byStat('email',$limit);}
    
    private function _byStat($stat,$limit){
        //round time to one hour (or whatever time rounding specifies) to make this request cacheable
        $roundedTime = floor(time()/self::TIME_ROUNDING)*self::TIME_ROUNDING;
        $from = $roundedTime - Mcmr_Stats::popularity_window * 86400;
    	return News_Model_Article::getMapper()->findAllByField(array(
    		'published' => 1,
    		'publishdate' => array(
    			'condition' => '>',
    			'value' => $from
    		),
    		'num'.$stat => array(
    			'condition' => '>',
    			'value' => 0
    		),
    	),array(
    		'num'.$stat => 'DESC',
    	),array(
    		'page' => 1,
    		'count' => $limit,
    	));
    }
}
