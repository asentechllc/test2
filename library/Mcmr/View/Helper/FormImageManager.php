<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: FormImageManager.php 1956 2010-12-20 16:47:36Z www-data $
 */

/**
 * Helper to display an ImageManager
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_FormImageManager extends Zend_View_Helper_FormText
{
    public function formImageManager($name, $value = null, $attribs = null)
    {
        $xhtml = $this->formText($name, $value, $attribs);

        $info = $this->_getInfo($name, $value, $attribs);
        
        extract($info); // name, value, attribs, options, listsep, disable

        //$this->view->headScript()->appendFile('/imagemanager/js/mcimagemanager.js');

        $attribs['destination'] = isset($attribs['destination'])
            ? $attribs['destination']
            : SITE_PATH . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR
            . 'assets' . DIRECTORY_SEPARATOR . 'images';
        $attribs['rootpath'] = isset($attribs['rootpath'])?$attribs['rootpath']:$attribs['destination'];
        $attribs['buttonClass'] = isset($attribs['buttonClass'])?$attribs['buttonClass']:'';

        $domain = $this->view->serverUrl();
        $relativeUrls = isset($attribs['relativeUrls'])?$attribs['relativeUrls']:'false';

        $xhtml .= "<input type=\"button\" value=\"Browse...\" onclick=\"mcImageManager.browse("
            . "{fields: '{$id}', no_host : true, use_url_path : false, "
            . "relative_urls: {$relativeUrls}, remove_script_host : true, "
            . "path: '{$attribs['destination']}', rootpath: '{$attribs['rootpath']}', "
            . "insert_filter : function(data) {data.url = '{$attribs['baseUrl']}' + "
            . "data.url.replace('{$domain}', '');},});\" class=\"{$attribs['buttonClass']}\">";

        return $xhtml;
    }
}
