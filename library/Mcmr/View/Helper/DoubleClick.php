<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 */

/**
 * A View helper to display double click adverts on the website
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_DoubleClick extends Zend_View_Helper_Abstract
{
    protected $_slots = array();
    protected $_slotHtml = '';
    
    public function __toString()
    {
        return $this->_slotHtml;
    }
    
    /**
     * Display an advert on the website
     *
     * @param string $slotId
     * @param string $divId
     * @param int $width
     * @param int $height
     * @return Mcmr_View_Helper_Advert 
     */
    public function doubleClick($slotId=null, $divId=null, $width=null, $height=null)
    { 
        static $jsAppended = false;
        
        if (!$jsAppended) {
            $this->view->headScript()
                ->appendScript(
                    "var googletag = googletag || {};\n"
                        . "googletag.cmd = googletag.cmd || [];\n"
                        . "(function() {\n"
                        . "var gads = document.createElement('script');\n"
                        . "gads.async = true;\n"
                        . "gads.type = 'text/javascript';\n"
                        . "var useSSL = 'https:' == document.location.protocol;\n"
                        . "gads.src = (useSSL ? 'https:' : 'http:') +\n"
                        . "'//www.googletagservices.com/tag/js/gpt.js';\n"
                        . "var node = document.getElementsByTagName('script')[0];\n"
                        . "node.parentNode.insertBefore(gads, node);\n"
                        . "})();"
                    );
            $jsAppended = true;
        }
        
        if (null !== $divId
                && null !== $slotId
                && null !== $width
                && null !== $height
                ) {
            
            $this->defineSlot($slotId, $divId, $width, $height);
            $this->_slotHtml = $this->displaySlot($divId);
        }
        
        return $this;
    }
    
    /**
     * Build the head script content for all the defined slots.
     *
     * @param type $accountId
     * @return Mcmr_View_Helper_Advert 
     */
    public function initialise($accountId)
    {
        $headScript = "googletag.cmd.push(function() {\n";
        foreach ($this->_slots as $id=>$slot) {
            $headScript .=  "googletag.defineSlot('/{$accountId}/{$id}', [{$slot['width']}, {$slot['height']}], '{$slot['id']}').addService(googletag.pubads());\n";
        }
        $headScript .= "googletag.pubads().collapseEmptyDivs();\n"
            . "googletag.enableServices();\n"
            . "});";
        
        $this->view->headScript()->appendScript($headScript);
        
        return $this;
    }
    
    /**
     * Get the code necessary for displaying the advert
     *
     * @param type $divId
     * @return string 
     */
    public function displaySlot($divId)
    {
        return "<script type='text/javascript'>googletag.cmd.push(function() {googletag.display('{$divId}');});</script>";
    }
    
    /**
     * Define the advert slot
     *
     * @param string $slotId
     * @param string $divId
     * @param int $width
     * @param int $height
     * @return Mcmr_View_Helper_Advert 
     */
    public function defineSlot($slotId, $divId, $width, $height)
    {
        $this->_slots[$slotId] = array(
            'id' =>$divId,
            'width'=>intval($width),
            'height'=>intval($height),
        );
        
        return $this;
    }
}