<?php
class Mcmr_View_Helper_FormSelectWithEmpty extends Zend_View_Helper_FormSelect
{

    public function formSelectWithEmpty($name, $value = null, $attribs = null,
        $options = null, $listsep = "<br />\n")
    {
        $info = $this->_getInfo($name, $value, $attribs, $options, $listsep);
        extract($info); // name, id, value, attribs, options, listsep, disable

        // force $value to array so we can compare multiple values to multiple
        // options; also ensure it's a string for comparison purposes.
        $value = array_map('strval', (array) $value);

        // check if element may have multiple values
        $multiple = '';

        if (substr($name, -2) == '[]') {
            // multiple implied by the name
            $multiple = ' multiple="multiple"';
        }

        if (isset($attribs['multiple'])) {
            // Attribute set
            if ($attribs['multiple']) {
                // True attribute; set multiple attribute
                $multiple = ' multiple="multiple"';

                // Make sure name indicates multiple values are allowed
                if (!empty($multiple) && (substr($name, -2) != '[]')) {
                    $name .= '[]';
                }
            } else {
                // False attribute; ensure attribute not set
                $multiple = '';
            }
            unset($attribs['multiple']);
        }

        // now start building the XHTML.
        $disabled = '';
        if (true === $disable) {
            $disabled = ' disabled="disabled"';
        }

        // Build the surrounding select element first.
                
        $xhtml = '<select'
                . ' name="' . $this->view->escape($name) . '"'
                . ' id="' . $this->view->escape($id) . '"'
                . $multiple
                . $disabled
                . $this->_htmlAttribs($attribs)
                . ">\n    ";

        // build the list of options
        $list       = array();
        $translator = $this->getTranslator();
        $this->_selectedOptions = array();
        foreach ((array) $options as $optValue => $optLabel) {
            if (is_array($optLabel)) {
                $optDisable = '';
                if (is_array($disable) && in_array($optValue, $disable)) {
                    $optDisable = ' disabled="disabled"';
                }
                if (null !== $translator) {
                    $optValue = $translator->translate($optValue);
                }
                $list[] = '<optgroup'
                        . $optDisable
                        . ' label="' . $this->view->escape($optValue) .'">';
                foreach ($optLabel as $val => $lab) {
                    $list[] = $this->_build($val, $lab, $value, $disable);
                }
                $list[] = '</optgroup>';
            } else {
                $list[] = $this->_build($optValue, $optLabel, $value, $disable);
            }
        }
        foreach ( $value as $v ) {
            if (!in_array($v, $this->_selectedOptions)) {
                $list[] = $this->_build($v, $v, $value, $disable);
            }
        }

        // add the options to the xhtml and close the select
        $xhtml .= implode("\n    ", $list) . "\n</select>";

        if ( isset( $attribs[ 'class' ] ) ) {
            $attribs[ 'class' ] .= ' empty';
        } else {
            $attribs[ 'class' ] = 'empty';
        }
        if ( $multiple ) {
            $xhtml  .= '<span '.$this->_htmlAttribs($attribs).'><input type="checkbox"' 
                    . ' name="' . $this->view->escape(substr($name, 0, -2).'[__EMPTY__]') . '"'
                    . ' id="' . $this->view->escape($id.'-__EMPTY__') . '"'
                    . $disabled
                    . $this->_htmlAttribs($attribs)
                    . "><span> Empty</span></span>\n";
        }

        return $xhtml;
    }
    
    
    protected function _build($value, $label, $selected, $disable) 
    {
        if (in_array((string) $value, $selected)) {
            $this->_selectedOptions[] = $value;
        }
        return parent::_build($value, $label, $selected, $disable);
    }
}