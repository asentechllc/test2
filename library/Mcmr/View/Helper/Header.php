<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Header.php 1209 2010-08-11 14:54:43Z leigh $
 */

/**
 * A view helper for changing the page header
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Header
{
    /**
     * Set a header
     *
     * If $replace is true, replaces any headers already defined with that
     * $name.
     *
     * @param string $name
     * @param string $value
     * @param boolean $replace
     * @return Zend_Controller_Response_Abstract
     */
    public function header($name, $value, $replace = false)
    {
        $front = Zend_Controller_Front::getInstance();
        $front->getResponse()->setHeader($name, $value, $replace);
    }
}
