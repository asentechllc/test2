<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: AuthenticatedUser.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * A View helper to output a noindex metatag if the site is not in 'production'
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_NoIndex extends Zend_View_Helper_Abstract
{

    /**
     * Return a no index/no follow meta tag when the application environment is not
     * production. Used to prevent search engines from spidering development copies of
     * the site.
     * 
     * If $force is set to true then it will always return the no index meta tag
     *
     * @param boolean $force
     * @return string
     */
    public function noIndex($force = false)
    {
        if ('production' !== APPLICATION_ENV || $force) {
            $this->view->headMeta()->appendName('robots', "noindex,nofollow");
            return '';
        } else {
            return '';
        }
    }
}
