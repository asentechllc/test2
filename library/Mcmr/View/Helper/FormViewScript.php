<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: FormViewScript.php 1258 2010-08-20 11:14:59Z leigh $
 */

/**
 * A view helper containing functions for form view scripts
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_FormViewScript extends Zend_View_Helper_Abstract
{
    public function correctSubForm($form, $subform)
    {
        $formArrayName = $form->getElementsBelongTo();
        $subformArrayName = $subform->getElementsBelongTo();
        
        $pieces = explode('[', $subformArrayName, 2);
        if (isset($pieces[1])) {
            $subform->setElementsBelongTo($formArrayName.'['.$pieces[0].']['.$pieces[1]);
        } else {
            $subform->setElementsBelongTo($formArrayName.'['.$pieces[0].']');
        }
    }

    public function elementScript($element)
    {
        $path = isset($element->viewScriptPath)?$element->viewScriptPath:'form';
        $parts = explode('_', get_class($element));
        $name = $path.'/element/'.strtolower(array_pop($parts)).'.phtml';
        try {
            $content = $this->view->partial($name, array('element'=>$element));
        } catch(Exception $e) {
            return false;
        }

        return $content;
    }

    public function formViewScript()
    {
        return $this;
    }
}
