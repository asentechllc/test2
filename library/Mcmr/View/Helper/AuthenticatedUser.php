<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: AuthenticatedUser.php 2013 2011-01-06 14:41:42Z leigh $
 */

/**
 * A View helper to access the current authenticated user.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_AuthenticatedUser extends Zend_View_Helper_Abstract
{
    /**
     * Return the current authenticated user. Returns null is there is no authenticated user.
     *
     * @return User_Model_User|null
     */
    public function authenticatedUser()
    {
        $user = Zend_Registry::get('authenticated-user');
        if (!is_object($user) || !$user instanceof User_Model_User) {
            $user = new User_Model_User();
        }

        return $user;
    }
}
