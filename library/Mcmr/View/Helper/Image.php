<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Image.php 2220 2011-02-22 19:08:25Z leigh $
 */

/**
 * A View helper that takes an image URL and resizes/crops the image caching the result.
 * The cache file's URL is then returned to the view.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Image extends Zend_View_Helper_Abstract
{
    const CACHE_PATH='/public/cimages';
    const CACHE_URL='/cimages';

    private $_originalPath = null;
    private $_filePath = null;
    private $_fileUrl = null;
    private $_mime = null;
    private $_srcWidth = null;
    private $_srcHeight = null;
    private $_dstWidth = null;
    private $_dstHeight = null;
    private $_srcX = null;
    private $_srcY = null;
    private $_dstX = 0;
    private $_dstY = 0;

    // images with alternate dimensions to be served via JS
    private static $_jsImages = array();

    /**
     * Take image crop and resize options and returns an image URL for the resized image
     *
     * @param string $filename Original URL of the image
     * @param array  $crop
     * @param array  $resize
     * @param string $basePath
     * @param bool   $generateAltImage
     * @return string
     */
    public function image($filename, $crop = null, $resize = null, $basePath = SITE_PATH, $generateAltImage = true)
    {

		$filename = urldecode($filename);

    	if($filename == 'script')
    		return $this->_script();

        $cacheFilename = $this->_cacheFilename($filename, $crop, $resize);

        $this->_filePath = $basePath . self::CACHE_PATH . DIRECTORY_SEPARATOR . $cacheFilename;
        $this->_fileUrl = self::CACHE_URL . '/' . $cacheFilename;

        if (Zend_Uri::check($filename)) {
            $this->_originalPath = $filename;
        } else {
            $this->_originalPath = $basePath . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $filename;
        }
        if (!file_exists($this->_filePath)) {
            if (!function_exists('gd_info')) {
                throw new Zend_View_Exception('Cannot process image. GD Library not loaded');
            }

            $info = @getimagesize($this->_originalPath);
            if (false === $info) {

            	if(isset($resize['width']) && isset($resize['height']))
            		return $this->_placeholder($resize['width'],$resize['height']);

                return $this->_defaultImage();
            }

            if (isset($resize['width']) && !isset($resize['height'])) {
                $ratio = $resize['width'] / $info[0];
                $resize['height'] = $info[1] * $ratio;
            }

            if (!isset($resize['width']) && isset($resize['height'])) {
                $ratio = $resize['height'] / $info[1];
                $resize['width'] = $info[0] * $ratio;
            }

            $this->_mime = $info['mime'];

            $this->_imgWidth = $this->_dstWidth = $resize['width'];
            $this->_imgHeight = $this->_dstHeight = $resize['height'];

            $this->_srcX = isset($crop['startX']) ? $crop['startX'] : 0;
            $this->_srcY = isset($crop['startY']) ? $crop['startY'] : 0;

            $this->_srcWidth = isset($crop['width']) ? $crop['width'] : $info[0]; // Default original width
            $this->_srcHeight = isset($crop['height']) ? $crop['height'] : $info[1]; // Default original height

			$this->_srcRatio = $this->_srcWidth/$this->_srcHeight;
			$this->_dstRatio = $this->_dstWidth/$this->_dstHeight;
			$this->_resizeRatioThreshold = 0.5;
			$this->_padImage = ($this->_srcRatio > $this->_dstRatio ? $this->_srcRatio - $this->_dstRatio : $this->_dstRatio - $this->_srcRatio) > $this->_resizeRatioThreshold;

			$this->_dstX = 0;
			$this->_dstY = 0;
			$this->_srcX = 0;
			$this->_srcY = 0;

			# do we have specific image fitting instructions?
			if(isset($resize['fit'])){

				$resize['fit'] = strtolower(trim($resize['fit']));

				# we should crop the image to fit
				if($resize['fit'] == 'crop'){

					# does image need cropping?
					if($this->_dstRatio != $this->_srcRatio){
						# trim top + bottom
						if($this->_dstRatio > $this->_srcRatio){
							# need new target height
							$_ratio = $this->_dstWidth/$this->_srcWidth;
							$_imgHeight = $this->_imgHeight/$_ratio;
							$this->_srcY = round(($this->_srcHeight-$_imgHeight)/2);
							$this->_dstHeight = round($_ratio * $this->_srcHeight);
						}
						# trim sides
						else{
							# need new target width
							$_ratio = $this->_dstHeight/$this->_srcHeight;
							$_imgWidth = $this->_imgWidth/$_ratio;
							$this->_srcX = round(($this->_srcWidth-$_imgWidth)/2);
							$this->_dstWidth = round($_ratio * $this->_srcWidth);
						}

						$this->_padImage = false;
					}
				}
			}

			# image requires automatic padding
			if($this->_padImage){
				# pad sides
				if($this->_dstRatio > $this->_srcRatio){
					# need new target width
					$this->_dstWidth = round(($this->_dstHeight/$this->_srcHeight) * $this->_srcWidth);
					$this->_dstX = round(($this->_imgWidth-$this->_dstWidth)/2);
				}
				# pad top + bottom
				else{
					# need new target height
					$this->_dstHeight = round(($this->_dstWidth/$this->_srcWidth) * $this->_srcHeight);
					$this->_dstY = round(($this->_imgHeight-$this->_dstHeight)/2);
				}
			}

            $this->_processImage();
        }

		$fileUrl = $this->_fileUrl;

		# register alternate image for non-mobile devices
        $config = Mcmr_Config_Ini::getInstance('application.ini', APPLICATION_ENV);
        if($generateAltImage && isset($config->media->image_width_threshold) && (isset($resize['width']) && $resize['width'] > $config->media->image_width_threshold)){
        	$fullUrl = $fileUrl;

        	$urlSuffix = '?'.$resize['width'];

			$ratio = $resize['width']/$config->media->image_width_threshold;
			$resize['width'] = $config->media->image_width_threshold;
			if(isset($resize['height']))
				$resize['height'] /= $ratio;

        	$fileUrl = $this->image($filename,$crop,$resize,$basePath).$urlSuffix;
        	self::$_jsImages[$fileUrl] = $fullUrl;
        }

        return $fileUrl;
    }

    /**
     * Take all the image details and create the new image saving to the cache
     */
    private function _processImage()
    {
        if (!is_dir(SITE_PATH . self::CACHE_PATH . DIRECTORY_SEPARATOR)) {
            if (!@mkdir(SITE_PATH . self::CACHE_PATH . DIRECTORY_SEPARATOR, 0755)) {
                throw new Zend_View_Exception('Unable to create image cache directory: '.SITE_PATH . self::CACHE_PATH . DIRECTORY_SEPARATOR);
            }
        }

        // Open the original image
        $data = file_get_contents($this->_originalPath);
        if (false === $data) {
            throw new Zend_View_Exception('Unable to open orginal image file');
        }
        $srcImage = imagecreatefromstring($data);

        // Create the destination image and preserve the transparency
        $dstImage = @imagecreatetruecolor($this->_imgWidth, $this->_imgHeight);
        imagealphablending($dstImage, false);
        imagesavealpha($dstImage, true);

        $transparentIndex = imagecolortransparent($srcImage);
        $palletsize = imagecolorstotal($srcImage);
        $transparentColor = null;
        if(0 <= $transparentIndex && $transparentIndex < $palletsize) {
            $transparentColor = imagecolorsforindex($srcImage,$transparentIndex);
        }
        if(null !== $transparentColor) {
            $transparentNew = imagecolorallocatealpha(
                $dstImage,
                $transparentColor['red'],
                $transparentColor['green'],
                $transparentColor['blue'],
                $transparentColor['alpha']
            );
            $transparentIndex = imagecolortransparent($dstImage, $transparentNew );
        }
        imagefill($dstImage, 0,0, $transparentIndex);

        $resampleResult = @imagecopyresampled(
            $dstImage,
            $srcImage,
            $this->_dstX,
            $this->_dstY,
            $this->_srcX,
            $this->_srcY,
            $this->_dstWidth,
            $this->_dstHeight,
            $this->_srcWidth,
            $this->_srcHeight
        );
        if (false === $resampleResult) {
            throw new Zend_View_Exception('Unable to process image');
        }

        // Save the new image to the cache
        switch ($this->_mime) {
            case 'image/jpeg':
                $success = @imagejpeg($dstImage, $this->_filePath, 95);
                break;
            case 'image/gif':
                $success = @imagegif($dstImage, $this->_filePath);
                break;
            case 'image/png':
                $success = @imagepng($dstImage, $this->_filePath);
                break;
            default:
                //@TODO Handle unknown image types better.
                return;
                //throw new Zend_View_Exception('Unknown image type \'' . $this->_mime . '\'');
                break;
        }

        if (!$success) {
            throw new Zend_View_Exception('Unable to create new image');
        }
    }

    /**
     * Get the filename of the cached image
     *
     * @param string $filename
     * @param array $options
     * @return string
     */
    private function _cacheFilename($filename, $crop, $resize)
    {
        $cacheFilename = $filename;
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        if (strpos($extension, '?') !== false) {
            $extension = substr($extension, 0, strpos($extension, '?'));
        }
        $parts = array();
        if (is_array($crop)) {
            foreach ($crop as $key => $value) {
                $parts[] = 'resize' . $key . '-' . $value;
            }
        }
        if (is_array($resize)) {
            foreach ($resize as $key => $value) {
                $parts[] = 'crop' . $key . '-' . $value;
            }
        }
        $cacheFilename .= implode(':', $parts);

        return md5($cacheFilename) . '.' . $extension;
    }

    private function _defaultImage()
    {
        $config = Mcmr_Config_Ini::getInstance('application.ini', APPLICATION_ENV);
        if (isset($config->resources->imagemanager->default_image)) {
            return $config->resources->imagemanager->default_image;
        } else {
            return '/media/images/default.jpg';
        }
    }

    private function _placeholder($w,$h){
    	$cacheFilename = "{$w}x{$h}.jpg";
        $file = SITE_PATH . self::CACHE_PATH . DIRECTORY_SEPARATOR . $cacheFilename;
        $url = self::CACHE_URL . '/' . $cacheFilename;

		# generate placeholder if it doesn't exist
        if (!file_exists($file)) {
            if (!function_exists('gd_info')) {
                throw new Zend_View_Exception('Cannot process image. GD Library not loaded');
            }

            $img = imagecreatetruecolor($w,$h);

	        $config = Mcmr_Config_Ini::getInstance('application.ini', APPLICATION_ENV);

			$defaults = array(
				'background' => 'ccc',
				'fontSize' => 20,
				'fontMinSize' => 6,
				'fontColor' => '444',
				'fontAngle' => 0,
				'fontPad' => 5,
				'fontFile' => false,
				'text' => 'dimensions',
			);

			foreach($defaults as $k=>$v){
				if(isset($config->placeholder->$k))
					$$k = $config->placeholder->$k;
				else
					$$k = $v;
			}

            if($text == 'dimensions'){
            	$text = "{$w} x {$h}";
            }

            if($text && $fontFile){

            	$fontFile = SITE_PATH.$fontFile;
            	$fontColor = $this->_rgb($fontColor);
            	$fontColor = imagecolorallocate($img,$fontColor[0],$fontColor[1],$fontColor[2]);
            	$fontPad += 5;

				do{

					if(!empty($fontW)){
						$fontSize--;
					}

					# text is too small, skip it
					if($fontSize < $fontMinSize){
						$text = false;
						break;
					}

					$fontBox = imagettfbbox($fontSize,$fontAngle,$fontFile,$text);
					$fontW = abs($fontBox[4] - $fontBox[0]);
					$fontH = abs($fontBox[1] - $fontBox[5]);
					$fontX = floor(($w-$fontW)/2) - $fontBox[1];
					$fontY = floor(($h-$fontH)/2) - $fontBox[5];

				}while(($fontW + ($fontPad*2)) > $w);
			}
			else{
				$text = false;
			}

           	$background = $this->_rgb($background);
           	$background = imagecolorallocate($img,$background[0],$background[1],$background[2]);
            imagefill($img,0,0,$background);

            if($text){
	            imagettftext($img,$fontSize,$fontAngle,$fontX,$fontY,$fontColor,$fontFile,$text);
	        }

            imagejpeg($img,$file,90);
        }

        return $url;
    }

    private function _rgb($colorCode,$default='ccc',$recurse=true){

		switch(strlen($colorCode)){
			case 3:
				return array(
					hexdec($colorCode[0].$colorCode[0]),
					hexdec($colorCode[1].$colorCode[1]),
					hexdec($colorCode[2].$colorCode[2]),
				);
				break;
			case 6:
				return array(
					hexdec($colorCode[0].$colorCode[1]),
					hexdec($colorCode[2].$colorCode[3]),
					hexdec($colorCode[4].$colorCode[5]),
				);
				break;
			default:
				return $recurse ? $this->_rgb($default,'',false) : array(128,128,128);
		}
    }

    private function _clearImageCache()
    {

    }

    private function _script(){

    	$images = array();
    	foreach(self::$_jsImages as $small => $large)
    		$images[] = "['$small','$large']";

    	$script = '<script type="text/javascript">';
    	$script .= 'intentmedia.website.jsImages(['.implode(',',$images).']);';
    	$script .= '</script>';
    	return $script;
    }

}
