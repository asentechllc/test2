<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @copyright Copyright (c) 2009 Soflomo V.O.F. (http://www.soflomo.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 * @author jurian
 */

/**
 * Sozfo View Helper FormTinyMce
 *
 * The rendering of the view element. Using the TinyMce view helper javascript
 * initiazion.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @subpackage Helper
 */
class Mcmr_View_Helper_FormFileManager extends Zend_View_Helper_FormElement
{

	const	default_preview_width = 200;
	
    public function FormFileManager ($name, $value = null, $attribs = null)
    {
    	$opts = new StdClass;
    	
    	foreach(array('path','width','height','minWidth','minHeight','maxWidth','maxHeight','ignoreSizeWarning','showSizeWarning') as $opt){
	    	if(isset($attribs[$opt]))
    			$opts->$opt = $attribs[$opt];
    	}
    	
    	if(isset($attribs['preview']) && !$attribs['preview'])
    		$attribs['class'] = (empty($attribs['class']) ? '' : $attribs['class'].' ') . 'no-preview';
    	
    	$html = '<input id="'.$attribs['id'].'" class="file-chooser'.(!empty($attribs['class']) ? ' '.$attribs['class'] : '').'"'.(!empty($attribs['placeholder']) ? ' placeholder="'.$this->view->escape($attribs['placeholder']).'"' : '').' type="text" name="'.$this->view->escape($name).'" value="'.$this->view->escape($value).'" autocomplete="off" />';

		$width = false;
		$height = false;

		$desc = array();
		if(!empty($attribs['width'])){
			$desc[] = $attribs['width'].'px wide';
			$width = $attribs['width'];
		}
		else if(!empty($attribs['minWidth']) && !empty($attribs['maxWidth'])){
			$desc[] = "Between {$attribs['minWidth']}px and {$attribs['maxWidth']}px wide";
		}
		else if(!empty($attribs['minWidth'])){
			$desc[] = "At least {$attribs['minWidth']}px wide";
		}
		else if(!empty($attribs['maxWidth'])){
			$desc[] = "Up to {$attribs['maxWidth']}px wide";
		}

		if(!empty($attribs['height'])){
			$desc[] = $attribs['height'].'px tall';
			$height = $attribs['height'];
		}
		else if(!empty($attribs['minHeight']) && !empty($attribs['maxHeight'])){
			$desc[] = "Between {$attribs['minHeight']}px and {$attribs['maxHeight']}px tall";
		}
		else if(!empty($attribs['minHeight'])){
			$desc[] = "At least {$attribs['minHeight']}px tall";
		}
		else if(!empty($attribs['maxHeight'])){
			$desc[] = "Up to {$attribs['maxHeight']}px tall";
		}

		if($desc){
			$html .= '<div class="file-chooser-dimensions">';

			if($width)
				$html .= '<div class="width">&larr; '.$width.'px &rarr;</div>';
			
			if($height && $width)
				$html .= '<div class="height">&uarr;<br />'.$height.'px<br />&darr;</div>';
			
			$w = $width < self::default_preview_width ? $width : self::default_preview_width;
			$html .= '<div class="file-chooser-preview" style="width:'.$w.'px;';

			if($width && $height){
				$h = $height/($width/$w);
				$html .= 'height:'.$h.'px';
			}
			
			$html .= '"></div>';
			
			$html .= '</div>';
			$html .= '<div class="description">'.implode(' / ',$desc).'</div>';
		}
    	
    	$html .= '<script type="text/javascript">
    gossip3.elFinder.bind($("#'.$attribs['id'].'").parent(),'.json_encode($opts).');
</script>';

    	return $html;
    }
}