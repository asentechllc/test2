<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package  View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * An extension of Zend built in HeadStyle helper which appends file modification timestamp to the href to prevent caching of modified stylesheets.
 *
 * @category Mcmr
 * @package  View
 * @subpackage Helper
 */
class Mcmr_View_Helper_HeadStyle extends Zend_View_Helper_HeadStyle
{
    public function append($value) {
        $this->_processValue($value);
        return parent::append($value);
    }

    public function prepend($value) {
        $this->_processValue($value);
        return parent::prepend($value);
    }

    public function set($value) {
        $this->_processValue($value);
        return parent::set($value);
    }


    protected function _processValue($value) {
        if (isset($value->attributes)&&isset($value->attributes['src'])) {
            $value->attributes['src'] = Mcmr_StdLib::timestampRelativeLink($value->attributes['src']);
        }
    }



}
