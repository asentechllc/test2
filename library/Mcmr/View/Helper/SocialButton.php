<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: AuthenticatedUser.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * A View helper to output social buttons
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_SocialButton extends Zend_View_Helper_Abstract
{
    
    private $_url = null;
    
    private $_displayHtml = '';
    protected $_request = null;
    
    public function __toString()
    {
        return $this->toString();
    }
    
    public function toString()
    {
        return $this->_displayHtml;
    }
    
    /**
     *
     * @return Mcmr_View_Helper_SocialButton 
     */
    public function socialButton($url)
    {
        $this->_displayHtml = '';
        $this->_url = $url;
        
        return $this;
    }
    
    /**
     * Display a facebook like link.
     *
     * @param array $attr
     * @return Mcmr_View_Helper_SocialButton 
     */
    public function facebook($attr = array())
    {
        static $jsAppended = false;
        
        $class = !empty($attr['class'])?$attr['class']:'';
        $width = !empty($attr['width'])?$attr['width']:'90';
        $verb = !empty($attr['verb'])?$attr['verb']:'like';
        $layout = !empty($attr['layout'])?$attr['layout']:'button_count';
        $faces = (!empty($attr['faces'])&&$attr['faces'])?'true':'false';
        $send = (!empty($attr['send'])&&$attr['send'])?'true':'false';
        
        $frameId = 'fb-frame';
        if (isset($attr['id']) && !empty($attr['id'])) {
            $frameId .= '-' . $attr['id'];
        }
        
        $fbHtml = "<div class=\"socialbutton fb-like-container\"><div class=\"fb-like\" data-href=\"{$this->_url}\" data-send=\"{$send}\" "
            . "data-layout=\"{$layout}\" data-width=\"{$width}\" data-show-faces=\"{$faces}\" "
            . "data-action=\"{$verb}\"></div><div class=\"count\"></div></div>";
        
        if (!$jsAppended) {
            $this->view->inlineScript()
                ->appendScript(
                    "(function(d, s, id) {"
                      . "var js, fjs = d.getElementsByTagName(s)[0];"
                      . "if (d.getElementById(id)) return;"
                      . "js = d.createElement(s); js.id = id;"
                      . "js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1&appId=120921804655574\";"
                      . "fjs.parentNode.insertBefore(js, fjs);"
                      . "}(document, 'script', 'facebook-jssdk'));", 
                    $this->_getContentType(), 
                    array('class'=>'cc-onconsent-inline-social')
                );
            $jsAppended = true;
        }
        
        $this->_displayHtml .= $fbHtml;
        
        return $this;
    }
    
    /**
     * Display a retweet button
     *
     * @param array $attr
     * @return Mcmr_View_Helper_SocialButton 
     */
    public function twitter($attr = array())
    {
        static $jsAppended = false;
        
        $class = !empty($attr['class'])?$attr['class']:'';
        $size = !empty($attr['size'])?$attr['size']:'';
        $via = !empty($attr['via'])?$attr['via']:'';;
        $related = !empty($attr['related'])?$attr['related']:'';;
        $hashtag = !empty($attr['hashtag'])?$attr['hashtag']:'';;
        $text = !empty($attr['text'])?$attr['text']:'';;
        
        $twitterHtml = "<a href=\"https://twitter.com/share\" class=\"twitter-share-button\" "
            . "data-url=\"{$this->_url}\" data-via=\"{$via}\" data-size=\"{$size}\" data-related=\"{$related}\" "
            . "data-text=\"{$text}\" data-hashtags=\"{$hashtag}\"></a>";
        
        if (!$jsAppended) {
            $this->view->inlineScript()
                ->appendScript(
                    "!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];"
                        . "if(!d.getElementById(id)){js=d.createElement(s);js.id=id;"
                        . "js.src=\"//platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}"
                        . "(document,\"script\",\"twitter-wjs\");", 
                    $this->_getContentType(), 
                    array('class'=>'cc-onconsent-inline-social')
                );
            
            $jsAppended = true;
        }
        
        $this->_displayHtml .= $twitterHtml;
        
        return $this;
    }
    
    /**
     * Display a linked in button
     *
     * @param array $attr
     * @return Mcmr_View_Helper_SocialButton 
     */
    public function linkedIn($attr = array())
    {
        static $jsAppended = false;
        
        $class = !empty($attr['class'])?$attr['class']:'';
        $counter = !empty($attr['counter'])?$attr['counter']:'';
        
        $linkedInHtml = "<script type=\"in/share\" data-url=\"{$this->_url}\" data-counter=\"{$counter}\"></script>";

        if (!$jsAppended) {
            $this->view->inlineScript()
                ->appendFile(
                    'http://platform.linkedin.com/in.js', 
                    $this->_getContentType(), 
                    array('class'=>'cc-onconsent-inline-social')
                );
            
            $jsAppended = true;
        }

       $this->_displayHtml .= $linkedInHtml;
        
        return $this;
    }
    
    /**
     * Display a google plus button.
     *
     * @param array $attr
     * @return Mcmr_View_Helper_SocialButton 
     */
    public function googlePlus($attr=array())
    {
        static $jsAppended = false;
        
        $annotation = !empty($attr['annotation'])?$attr['annotation']:'bubble';
        $size = !empty($attr['size'])?$attr['size']:'medium';
        $width = !empty($attr['width'])?$attr['width']:'450';
        
        $plusHtml = "<g:plusone annotation=\"{$annotation}\" width=\"{$width}\" "
            . "size=\"{$size}\" href=\"{$this->_url}\"></g:plusone>";

        if (!$jsAppended) {
            $this->view->inlineScript()
                ->appendScript(
                        "(function() {"
                         ."var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;"
                         ."po.src = 'https://apis.google.com/js/plusone.js';"
                         ."var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);"
                         ."})();", 
                    $this->_getContentType(), 
                    array('class'=>'cc-onconsent-inline-social')
                );
            $jsAppended = true;
        }

        $this->_displayHtml .= $plusHtml;

        return $this;
    }

    protected function _getContentType() 
    {
        if (null===$this->_request) {
            $this->_request = Zend_Controller_Front::getInstance()->getRequest();
        }
        if ($this->_request->isMobileRequest()) {
            return "text/javascript";
        } else {
            return "text/plain";
        }
    }
    
}
