<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IsAllowed.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * A View Helper to check ACL permissions
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_IsAllowed extends Zend_View_Helper_Abstract
{
    /**
     * Check if the user has permission to access the provided resource
     *
     * @param string $module
     * @param string $controller
     * @param string $action
     * @param array $params
     * @return boolean
     */
    public function isAllowed($module, $controller, $action, $params=array())
    {
        Mcmr_Acl::setAssertParams($params);
        $acl = Mcmr_Acl::getInstance();
        
        $user = Zend_Registry::get('authenticated-user');
        if (null !== $user) {
            $role = $user->getRole();
        } else {
            $role = 'guest';
        }

        $resource = Mcmr_Acl::getResourceName($module, $controller);
        return $acl->isAllowed($role, $resource, $action);
    }
}