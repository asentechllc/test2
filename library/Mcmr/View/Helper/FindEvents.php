<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A view helper that sets a redirect the user. Unlike the 'redirect' helper this helper
 * will redirect the user immediately.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_FindEvents extends Zend_View_Helper_Abstract
{

    /**
     * Redirect the user to the supplied location. 
     *
     * @param string $location
     * @param int $timeout 
     */
    public function findEvents($fn='upcoming',$limit=5)
    {
		$this->_type = News_Model_Type::getMapper()->findOneByField(array('url'=>'event'));

    	$_fn = '_find'.$fn;
    	if(method_exists($this,$_fn))
    		return $this->$_fn($limit);
    		
    	throw new Exception("Event scope not found: $fn");
    }
    
    private function _findUpcoming($limit){
    	return News_Model_Article::getMapper()->findAllByField(array(
    		'published' => 1,
    		'type_id' => $this->_type->getId(),
    		'attr_eventStart' => array(
    			'condition' => '>',
    			'value' => strtotime(date("Y-m-d 00:00:00")),
    		),
    	),array(
    		'attr_eventStart' => 'ASC',
    	),array(
    		'page' => 1,
    		'count' => $limit,
    	));
    }
}
