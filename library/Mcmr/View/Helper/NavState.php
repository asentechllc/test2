<?php

class Mcmr_View_Helper_NavState extends Zend_View_Helper_Abstract
{

    public function navState($item)
    {
    	$cfg = $this->view->config('default');
    	if(isset($cfg->navigationStates->$item)){
    		$url = $this->view->request()->getPathInfo();
    		foreach($cfg->navigationStates->$item->href->toArray() as $href){
    			if(substr($url,0,strlen($href)) == $href)
    				return true;
    		}
	    }
    	return false;
    }
}
