<?php
class Mcmr_View_Helper_FormText extends Zend_View_Helper_FormText
{
    /**
     * Generates a 'text' element.
     */
    public function formText($name, $value = null, $attribs = null)
    {
        if (is_array($value)) {
            $value = implode(',',$value);
        }
        return parent::formText($name, $value, $attribs);
    }
}
