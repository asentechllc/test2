<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A view helper that sets a redirect the user. Unlike the 'redirect' helper this helper
 * will redirect the user immediately.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_RedirectNow extends Zend_View_Helper_Abstract
{

    /**
     * Redirect the user to the supplied location. 
     *
     * @param string $location
     * @param int $timeout 
     */
    public function redirectNow($location, $timeout=0)
    {
        $this->view->headMeta()->appendHttpEquiv('refresh', $timeout.';url='.$location);
        $this->view->headScript()->appendScript(
            "function __mcmrRedirect(){document.location = '{$location}';} "
            . "setTimeout('__mcmrRedirect()', {$timeout}000);"
        );
        if (0 === $timeout) {

            header('Location: '.$location);
            exit;
        }
    }
}
