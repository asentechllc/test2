<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Redirect.php 1578 2010-10-18 15:44:12Z jian $
 */

/**
 * A view helper that sets a redirect on certain events
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Redirect extends Zend_View_Helper_Abstract
{
    /**
     * Redirect to the options when the event is registered
     * If the event is null the redirect is performed immediately
     *
     * @param string $event
     * @param array|string $options Where to redirect
     * @param boolean $clobber Overwrite the existing redirect
     */
    public function redirect($event, $options, $clobber = true)
    {
        if (null !== $event) {
            // Save the redirect information to the session
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $module = $request->getModuleName();
            $controller = $request->getControllerName();
            $namespace = new Zend_Session_Namespace("Mcmr_Redirect");

            if (!isset($namespace->$module)) {
                $namespace->$module = new stdClass();
            }
            
            if (!isset($namespace->$module->$controller)) {
                $namespace->$module->$controller = new stdClass();
            }

            if (!isset($namespace->$module->$controller->$event) || $clobber) {
                $namespace->$module->$controller->$event = $options;
            }
        } else {
            $this->_doRedirect($options);
        }
    }

    /**
     * Notify the redirect of an event. Will then perform the registered redirect
     *
     * @param string $event
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function notify($event, Mcmr_Model_ModelAbstract $model=null)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        
        $namespace = new Zend_Session_Namespace("Mcmr_Redirect");

        if (!isset($namespace->$module)) {
            $namespace->$module = new stdClass();
        }

        if (!isset($namespace->$module->$controller)) {
            $namespace->$module->$controller = new stdClass();
        }

        if (isset($namespace->$module->$controller->$event)) {
            $options = $namespace->$module->$controller->$event;

            // About to redirect. Remove the redirect info
            unset($namespace->$module->$controller->$event);

            $this->_doRedirect($options, $model);
        }
    }

    /**
     * Perform the redirect
     *
     * @param array $options
     */
    protected function _doRedirect($options, $model=null)
    {
        // A Redirect has been registered. Build the parameters from the model if necessary
        $params = array();
        if (is_array($options)) {
            if (isset($options['params']) && is_array($options['params'])) {
                foreach ($options['params'] as $key => $value) {
                    if (is_array($value)) {
                        $value = $model->$value['key'];
                    } elseif ('model' == $value) {
                        $value = $model->$key;
                    }

                    $params[$key] = $value;
                }
            }
        }
        
        // Redirect the user
        $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        if (is_array($options)) {
            $redirector->gotoSimple($options['action'], $options['controller'], $options['module'], $params);
        } else {
            $redirector->gotoUrl($options);
        }
        
    }
}
