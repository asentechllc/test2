<?php

class Mcmr_View_Helper_QueryOrdered extends Zend_View_Helper_Abstract
{
	/**
	 * Improved querying for ordered queries
	 **/
    public function queryOrdered($query,$model,$module=null,$config=null){

		$query = Mcmr_Model::queryBuild($query, $model, $module, $config);
		
		$modelClass = ucfirst($module)."_Model_".ucfirst($model);
		$mapper = $modelClass::getMapper();
		$orderTable = $mapper->getDbTable()->info('name').'_ordr';

		$db = $mapper->getDbTable()->getAdapter();
		
		$modelIds = array();
		$ordering = null;
		$leftJoin = true;
		
		$page = $query->getPage()->toArray();
		$count = isset($page['count']) ? $page['count'] : false;
		
		foreach($query->getOrders() as $order){
			$order = $order->toArray();
			if(isset($order['key']) && substr($order['key'],0,5) == 'ordr_'){

				if(isset($order['join']) && strtolower(trim($order['join'])) != 'left')
					$leftJoin = false;

				$ordering = $order['key'];
				$order = substr($order['key'],5);
				$rows = $db->query("SELECT * FROM `$orderTable` WHERE `ordr_name`=?",array($order))->fetchAll();
				foreach($rows as $row){
					if(isset($row[$model."_id"]))
						$modelIds[] = $row[$model."_id"];
				}
				break;
			}
		}
		
		# we have model IDs -- include in query for faster querying
		if($modelIds && count($modelIds) == $count){
			$query->addConditions(array(
				'id' => array(
					'condition' => 'in',
					'value' => $modelIds,
				)
			));
		}
		
		else{
		
			# if we're using a non-left join and there are no model IDs, we return an empty paginator
			if(!$leftJoin){
				return new Mcmr_Paginator(new Zend_Paginator_Adapter_Null);
			}
		
			# remove order to avoid un-neccessary joins if there are no available models
			if($ordering && !$modelIds){
				$orders = new Zend_Config(array(), true);
				$orders->merge($query->getOrders());
				foreach($orders as $k=>$order){
					$order = $order->toArray();
					if(isset($order['key']) && $order['key'] == $ordering){
						unset($orders->$k);
						break;
					}
				}
				$orders->setReadOnly();
				$query->setOrders($orders);
			}
		}
				
		return $this->view->queryAll($query,$model,$module);
	}
}