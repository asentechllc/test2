<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Referrer.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * A view helper to get the http referrer
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Referrer extends Zend_View_Helper_Abstract
{
    /**
     * Return the HTTP_REFERER
     *
     * @return string
     */
    public function referrer()
    {
        return Zend_Controller_Front::getInstance()->getRequest()->getServer('HTTP_REFERER');
    }
}
