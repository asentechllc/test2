<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Register a hit on the specified model and action. Can be used to record view, or download
 * statistics on gossip models.
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Mcmr_View_Helper_QueueStat extends Zend_View_Helper_Abstract
{

    /**
     * Take a gossip model and record a statistic against the supplied model and
     * statistic action name.
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @param string $action 
     */
    public function queueStat(Mcmr_Model_ModelAbstract $model, $action)
    {
        $queue = Mcmr_Queue::getInstance('stats');
        if (is_object($queue) && $queue->isSupported('send')) {
            $message = new stdClass();
            $message->modelClass = get_class($model);
            $message->modelId = $model->getId();
            $message->date = strtotime('today');
            $message->action = $action;
            $message->site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
            $queue->send(serialize($message));
        }
    }
}
