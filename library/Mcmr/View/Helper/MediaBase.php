<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Referrer.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * A view helper to get the configured URL for all public media.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_MediaBase extends Zend_View_Helper_Abstract
{
    /**
     * Return the configured URL for all public media
     *
     * @return string
     */
    public function mediaBase( $url = null )
    {
        $config = Zend_Registry::get('app-config');
        $base = $config->media->baseDomain;
        if ( null !== $url ) {
                $base .= $url;
        }
        
        return $base;
    }
}
