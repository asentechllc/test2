<?php

class Mcmr_View_Helper_QueryExists extends Zend_View_Helper_Abstract
{
	/**
	 * $query to be:
	 	 string -> then factory a query based on named config
	 	 query object -> use that
	 	 array -> do not have array to make it explicit providing parameters is dicourages, will have to go through bulding a query
	 * $model: model name, is string then derive from current controller name, needs mapping to refletc that index controller uses articles model
	 * $module: module name, can derive from current module name
	 */
    public function queryExists($query, $model, $module=null)
    {
    	if (null===$module) {
    		$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName();
    	}
        $result = Mcmr_Model::queryExists($query, $model, $module);
        return $result;
    }
}
