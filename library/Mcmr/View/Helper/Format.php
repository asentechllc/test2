<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: AuthenticatedUser.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Generic string format helper. Can be used to format a string in different ways
 *  - currency For converting a numberic value into a currency string
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Format extends Zend_View_Helper_Abstract
{
    public function format()
    {
        return $this;
    }

    /**
     * Format the number as a currency value. If the supplied value is not numeric it
     * is returned unchanged
     *
     * @param float $value
     * @param string $currency
     * @return string
     */
    public function currency($value, $currency='&pound;')
    {
        if (is_numeric($value)) {
            return $currency . number_format($value, 2);
        } else {
            return $value;
        }
    }
    
    /**
     * Format a number with grouped thousands.
     *
     * @param type $value
     * @return string
     */
    public function number($value)
    {
        return number_format($value);
    }
}
