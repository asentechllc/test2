<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IsAllowed.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * A View Helper to get module configuration
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_Config extends Zend_View_Helper_Abstract
{
    /**
     * Check if the user has permission to access the provided resource
     *
     * @param string $module
     * @param string $controller
     * @param string $action
     * @param array $params
     * @return boolean
     */
    public function config($module=null)
    {
        if (null === $module) {
            $module = $this->view->request()->getParam('module');
        }
        
        # load in an admin module
        if(substr($module,0,6) == 'admin-'){
        	try{
	        	return new Zend_Config_Ini(ADMIN_SITE_PATH.'/configs/modules/'.substr($module,6).'.ini',APPLICATION_ENV);
        	}catch(Exception $e){
        		return null;
        	}        	
        }

        $config = Zend_Registry::get($module.'-config');

        return $config;
    }
}