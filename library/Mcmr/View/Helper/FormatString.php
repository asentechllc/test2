<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: AuthenticatedUser.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Various functions for changing the format of a string.
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_FormatString extends Zend_View_Helper_Abstract
{

    public function formatString()
    {
        return $this;
    }

    /**
     * Insert extra content between specified paragraph
     *
     * @param string $content
     * @param string $insert
     * @param int $paragraphNum
     * @return string 
     */
    public function paragraphSplit($content)
    {
        preg_match_all("~<p>.*?</p>~s", $content, $match);
        
        return $match[0];
    }
    
    /**
     * 
     *
     * @param string $text
     * @param int $count
     * @param int $maxparagraphs
     * @param bool $link
     * @return string
     */
    public function summarise($text, $count = 100, $maxparagraphs = 1, $link = false)
    {
        preg_replace('/<script.*?<\/script.*?>/', '', $text);

        $words = preg_split(
            '/(<(?:\\s|".*?"|[^>])+>|\\s+)/', 
            $text, 
            $count + 1, 
            PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY
        );

        $ellipsis = '';
        if (count($words) > $count * 2) {
            array_pop($words);
            $ellipsis = ' ...';
            if ($link) {
                $ellipsis .= '<a href="' . $link . '" class="readmore">read more &raquo;</a>';
            }
        }
        $output = '';

        $paragraphs = 0;

        $stack = array();
        foreach ($words as $word) {
            if (preg_match('/<.*\/\\s*>$/', $word)) {
                // If the tag self-closes, do nothing.
                $output.= $word;
            } elseif (preg_match('/<[\\s\/]+/', $word)) {
                // If the tag ends, pop one off the stack (cheatingly assuming well-formed!)
                array_pop($stack);
                preg_match('/<\s*\/\s*(\\w+)/', $word, $tagn);
                switch ($tagn[1]) {
                    case 'br':
                    case 'p':
                    case 'div':
                    case 'ol':
                    case 'ul':
                        $paragraphs++;
                        if ($paragraphs >= $maxparagraphs) {
                            $output.= $word;
                            $ellipsis = '';
                            break 2;
                        }
                }
                $output.= $word;
            } elseif ($word[0] == '<') {
                // If the tag begins, push it on the stack
                $stack[] = $word;
                $output .= $word;
            } else {
                $output.= $word;
            }
        }
        $output.= $ellipsis;

        if (count($stack) > 0) {
            preg_match('/<(\\w+)/', $stack[0], $tagn);
            $stack = array_reverse($stack);
            foreach ($stack as $tag) {
                preg_match('/<(\\w+)/', $tag, $tagn);
                if (array_key_exists(1, $tagn)) {
                    $output.= '</' . $tagn[1] . '>';
                }
            }
        }
        return $output;
    }

}