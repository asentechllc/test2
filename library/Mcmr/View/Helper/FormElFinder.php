<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @copyright Copyright (c) 2009 Soflomo V.O.F. (http://www.soflomo.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 * @author jurian
 */

/**
 * Sozfo View Helper FormElFinder
 *
 * The rendering of the view element. Using the ElFinder view helper javascript
 * initiazion.
 *
 * @category  Mcmr
 * @package   Mcmr_Form
 * @subpackage Helper
 */
class Mcmr_View_Helper_FormElFinder extends Zend_View_Helper_FormTextarea
{

	public $root;

    private static	$_rendered = false,
    				$_index = 0;

    public function FormElFinder ($name, $value = null, $attribs = null)
    {

		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
	    $options = $bootstrap->getOptions();

		$id = 'elfinder_'.self::$_index++;
		$inputId = $id.'_input';
		$labelId = $id.'_label';
		$selectId = $id.'_select';
		$clearId = $id.'_clear';
		
		$value = str_replace('"','&quot;',$value);
		$name = str_replace('"','&quot;',$name);

		$urlSuffix = '';
		$query = array();
		if(isset($attribs['rootPath'])){
			$query['rootSuffix'] = $attribs['rootPath'];
		}
		
		$this->root = $options['media']['dir']['root'];
		
		if(isset($attribs['rootPath']))
			$this->root .= $attribs['rootPath'][0] == '/' ? substr($attribs['rootPath'],1) : $attribs['rootPath'];

		$none = '<em>(none)</em>';
		$labelValue = $none;
		$dirHash = '';
		
		if($value){
			$value = explode(',',$value);
			if(count($value) == 2){
				$labelValue = $value[0];
				$dirHash = $value[1];
			}
			else{
				$value = '';
			}
		}
		
		$xhtml = <<<EOQ
<div class="elFinderChooser">
	<div class="label" id="{$labelId}"><strong>Assigned folder:</strong> <span>{$labelValue}</span></div>
	<div class="assignment">
		<a id="{$selectId}" class="assign" href="#">Use current folder</a>
		<a id="{$clearId}" class="unassign" href="#">Unassign folder</a>
	</div>
	<input type="hidden" id="{$inputId}" name="{$name}" value="{$value}" />
	<div id="{$id}" class="elfinder"></div>
EOQ;

		# check if we've already rendered an element
		if(!self::$_rendered){
			self::$_rendered = true;
			
			$xhtml .= <<<EOQ

	<script type="text/javascript" src="/assets/scripts/jquery-1.7.2.js"></script>
	<script type="text/javascript" src="/assets/scripts/jquery.ui-1.8.4.js"></script>
	<script type="text/javascript" src="/assets/scripts/elfinder/js/elfinder.min.js"></script>
	
	<link rel="stylesheet" type="text/css" media="screen" href="/assets/css/jqueryui.smoothness-1.8.18.css">
	<link rel="stylesheet" type="text/css" media="screen" href="/assets/scripts/elfinder/css/elfinder.min.css">
	<link rel="stylesheet" type="text/css" media="screen" href="/assets/scripts/elfinder/css/theme.css">
EOQ;
		}

		if($query)		
			$urlSuffix = '?'.http_build_query($query);
			
		$xhtml .= <<<EOQ

	<script type="text/javascript" charset="utf-8">
	
		$().ready(function() {
			var elf = $('#{$id}').elfinder({
				resizable : false,
				handlers : {
					init : function(event,elf){
						elf.lastDir('{$dirHash}');
					}
				},
				uiOptions: {
					toolbar: [
						['back', 'forward'],
						// ['reload'],
						// ['home', 'up'],
						['mkdir', /*'mkfile',*/ 'upload'],
						['open', 'download', 'getfile'],
						['info'],
						['quicklook'],
						['copy', 'cut', 'paste'],
						['rm'],
						['duplicate', 'rename', 'edit', 'resize'],
						['extract', 'archive'],
						// ['search'],
						['view'],
						['help']
					]
				},
				url : '/files/index/elfinder{$urlSuffix}'
			}).elfinder('instance');
			$("#{$selectId}").click(function(){
				var hash = elf.cwd().hash;
				var path = elf.path(hash);
				$("#{$inputId}").val(path+','+hash);
				$("#{$labelId} span").html(path);
				$("#{$clearId}").show();
				return false;
			});
			$("#{$clearId}").click(function(){
				$("#{$inputId}").val('');
				$("#{$labelId} span").html('{$none}');
				$("#{$clearId}").hide();
				return false;
			});
			if(!$("#{$inputId}").val()){
				$("#{$clearId}").hide();
			}
		});
	
	</script>			
</div>
EOQ;
                
        return $xhtml;
    }

}