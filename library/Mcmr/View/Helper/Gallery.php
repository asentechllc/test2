<?php

class Mcmr_View_Helper_Gallery extends Zend_View_Helper_Abstract
{

    public function gallery($html,$opt=false)
    {
    
    	if(!is_array($opt))
    		$opt = array();
    	
    	if(!empty($opt['path'])){
    		$html = '<input class="gallery" value="'.$html.'" />';
    	}
    
    	if(preg_match_all('/<input .*?class="gallery".*?>/i',$html,$m)){
    		foreach($m[0] as $tag){
    			if(preg_match('/ value="(.*?)"/i',$tag,$tagM)){

					$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
					$options = $bootstrap->getOptions();

					$url = '/files/'.$tagM[1].'/';
					$dir = $options['media']['basePath'].'/public/files/'.$tagM[1].'/';

    				$images = array();
					$dh = @opendir($dir);
					while($image = @readdir($dh)){
						if(!is_dir($dir.$image)){
							$caption = preg_replace('/(.*?)(\..*?$)/','$1',$image);
							
							if(isset($opt['captions']) && isset($opt['captions'][$image])){
								$caption = $opt['captions'][$image];
							}
							
							$images[$caption] = $url.$image;
						}
					}
    				
		    		$html = str_replace($tag,$this->view->partial('partial/elements/gallery.phtml',array('images'=>$images,'size'=>(empty($opt['size']) ? null : $opt['size']))),$html);
		    	}
    		}
    	}
    	return $html;
    }
}
