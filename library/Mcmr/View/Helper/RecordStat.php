<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Register a hit on the specified model and action. Can be used to record view, or download
 * statistics on gossip models.
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Mcmr_View_Helper_RecordStat extends Zend_View_Helper_Abstract
{

    /**
     * Take a gossip model and record a statistic against the supplied model and
     * statistic action name.
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @param string $action 
     */
    public function recordStat(Mcmr_Model_ModelAbstract $model, $action)
    {
        $mapper = Default_Model_Stat::getMapper();
        $stat = $mapper->findOneByField(
            array(
                'modeltype'=> get_class($model),
                'modelid'=>$model->getId(), 'date'=>  strtotime('today'), 'action'=>$action
            )
        );
        $stat->increment();
        $mapper->save($stat);
    }
}
