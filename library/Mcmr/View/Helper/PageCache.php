<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package  View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A view helper to interact with the full page cache
 *
 * @category Mcmr
 * @package  View
 * @subpackage Helper
 */
class Mcmr_View_Helper_PageCache extends Zend_View_Helper_Abstract
{

    public function pageCache()
    {
        return $this;
    }
    
    /**
     * Disable the full page cache on this page. 
     */
    public function disable()
    {
        Mcmr_Controller_Plugin_PageCache::$doNotCache = true;
        
        return $this;
    }
}
