<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: DisplayMessages.php 2340 2011-04-08 12:10:48Z leigh $
 */

/**
 * A View Helper to display saved system messages for the module
 *
 * @category Mcmr
 * @package Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_DisplayMessages extends Zend_View_Helper_Abstract
{
    private $_messages = array();
    private $_level = null;
    private $_module = null;
    private $_namespace = array();

    const INFO='info';
    const WARN='warn';
    const ERROR='error';

    /**
     * Display the saved system messages
     *
     * @param string $level
     * @param string $module
     * @return Mcmr_View_Helper_DisplayMessages 
     */
    public function displayMessages($level = null, $module = null)
    {
        // Get the messages out of the registry
        if (null === $module) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $this->_module = $request->getModuleName();
        } else {
            $this->_module = $module;
        }
        
        $this->_level = $level;
        
        return $this;
    }

    public function  __toString()
    {
        return $this->toString();
    }

    /**
     *
     * @return string
     */
    public function toString()
    {
        $messages = $this->getMessages();
        $displayHtml = $this->view->partial('partial/messages.phtml', array('messages'=>$messages));
        
        // Clear the messages after displaying
        $this->clear();
        
        return $displayHtml;
    }
    
    /**
     * Clear the stored system messages
     *
     * @return string
     */
    public function clear($module=false)
    {
        $namespace = $this->_getNamespace($module);
        
        // Clear the messages once displayed
        if (null !== $this->_level) {
            unset($namespace->messages[$this->_level]);
        } else {
            unset($namespace->messages);
        }
        
        if($module != 'default')
        	$this->clear('default');

        return '';
    }

    /**
     * Add a message to the session to be displayed later.
     *
     * @param string $message
     * @param string $level
     * @param string $module 
     */
    public function addMessage($message, $level=self::INFO, $module = null)
    {
        if (!isset($this->_messages[$level])) {
            $this->_messages[$level] = array();
        }

        if (null === $module) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $module = $request->getModuleName();
        }
        
        // If the module is empty, there will be no message configuration.
        if (!empty($module)) {
            $siteName = $this->view->request()->getSiteName();

            // Attempt to read the message out of the module configuration
            $config = Zend_Registry::get($module.'-config');

			// Check if we have an array of messages to match
			$messages = !is_array($message) ? array($message) : $message;

			foreach($messages as $message){
				if (isset($config->$siteName->messages) && isset($config->$siteName->messages->$level)
						&& isset($config->$siteName->messages->$level->$message)) {
					$message = $config->$siteName->messages->$level->$message;
					break;
				} elseif (isset($config->messages) && isset($config->messages->$level)
						&& isset($config->messages->$level->$message)) {
					$message = $config->messages->$level->$message;
					break;
				}
			}

            $this->_messages[$level][] = $message;

            // Save the messages to the session. Allows display between redirects

            $namespace = new Zend_Session_Namespace("messages-$module");
            $namespace->messages = $this->_messages;
        }
    }
    
    /**
     * Get an array of all the display messages
     *
     * @return array
     */
    public function getMessages($module=false)
    {
        $namespace = $this->_getNamespace($module);
        
        // Get the messages out of the session according to the message level.
        if (null !== $this->_level) {
            $messages = array($this->_level=>$namespace->messages[$this->_level]);
        } else {
            $messages = $namespace->messages;
        }
        
        if (!is_array($messages)) {
            $messages = array();
        }
        
        if($module != 'default')
        	$messages = array_merge($messages,$this->getMessages('default'));
        
        return $messages;
    }
    
    protected function _getNamespace($module=false)
    {
    	
    	if($module === false)
    		$module = $this->_module;
    
        if (!isset($this->_namespace[$module])) {
            $this->_namespace[$module] = new Zend_Session_Namespace("messages-{$module}");
        }
        
        return $this->_namespace[$module];
    }
}
