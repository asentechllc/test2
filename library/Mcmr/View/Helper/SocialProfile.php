<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package  View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A view helper used to fetch the current time to a specified precision
 *
 * @category Mcmr
 * @package  View
 * @subpackage Helper
 */
class Mcmr_View_Helper_SocialProfile extends Zend_View_Helper_Abstract
{
    /**
     * Get the current time according to the supplied precision. A precision of 'hour' will return 
     * the current time to the closed hour. Other supported precisions are 'second', 'minute', 'quarter',
     * 'hour', 'day'.
     * 
     * @param type $time
     * @param type $precision
     * @return type 
     */
    public function socialProfile($provider=null, $user=null)
    {
        if (null===$provider) {
            $front = Zend_Controller_Front::getInstance();
            $request = $front->getRequest();
            $provider = $request->getParam('provider');

        }
        if (null===$user) {
            $user = Zend_Registry::get('authenticated-user');
        }
        $hybridauth = Mcmr_Service_Hybrid_Auth::getInstance();
        $adapter = $hybridauth->authenticate( $provider );
        $profile = $adapter->getUserProfile();
        return $profile;
    }
}
