<?php
/**
 * Zend Framework
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://framework.zend.com/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@zend.com so we can send you a copy immediately.
 *
 * @category  Mcmr
 * @package   Mcmr_View
 * @copyright Copyright (c) 2009 Soflomo V.O.F. (http://www.soflomo.com)
 * @license   http://framework.zend.com/license/new-bsd     New BSD License
 * @author jurian
 */

/**
 * Sozfo View Helper TinyMce
 *
 * Used to render javascript for the TinyMce textarea conversion.
 *
 * @category  Mcmr
 * @package   Mcmr_View
 * @subpackage Helper
 */
class Mcmr_View_Helper_FeatureContent extends Mcmr_View_Helper_TinyMce
{
    protected $_enabled = false;
    protected $_defaultScript = '/assets/scripts/tiny_mce/tiny_mce.js';
    protected $_styleFile = '/assets/css/gossip3.feature.css';

    protected $_supported = array(
        'mode'      => array('textareas', 'specific_textareas', 'exact', 'none'),
        'theme'     => array('simple', 'advanced'),
        'format'    => array('html', 'xhtml'),
        'languages' => array('en'),
        'plugins'   => array('style', 'layer', 'table', 'save',
                             'advhr', /*'advimage',*/ 'advlink', 'emotions',
                             'iespell', 'insertdatetime', 'preview', 'media',
                             'searchreplace', 'print', 'contextmenu', 'paste',
                             'directionality', 'fullscreen', 'noneditable', 'visualchars',
                             'nonbreaking', 'xhtmlxtras', /*'imagemanager', 'filemanager',*/'template'));

    protected $_config = array('mode'  =>'textareas',
                               'theme' => 'simple',
                               'element_format' => 'html',
                               'file_browser_callback' => 'gossip3.tinyMCE.fileBrowser');
    protected $_scriptPath;
    protected $_scriptFile;
    protected $_useCompressor = false;

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (!method_exists($this, $method)) {
            throw new Mcmr_View_Exception('Invalid tinyMce property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (!method_exists($this, $method)) {
            throw new Mcmr_View_Exception('Invalid tinyMce property');
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            } else {
                $this->_config[$key] = $value;
            }
        }
        return $this;
    }

    public function FeatureContent ()
    {
        return $this;
    }

    public function setScriptPath ($path)
    {
        $this->_scriptPath = rtrim($path, '/');
        return $this;
    }

    public function setScriptFile ($file)
    {
        $this->_scriptFile = (string) $file;
    }

    public function setCompressor ($switch)
    {
        $this->_useCompressor = (bool) $switch;
        return $this;
    }

    public function render()
    {
        if (false === $this->_enabled) {
            $this->_renderScript();
            $this->_renderEditor();
        }
        $this->_enabled = true;
    }

    protected function _renderScript ()
    {
        if (null === $this->_scriptFile) {
            $script = $this->_defaultScript;
        } else {
            $script = $this->_scriptPath . '/' . $this->_scriptFile;
        }

        $this->view->headScript()->appendFile($script);
        $this->view->headLink()->appendStylesheet($this->_styleFile);
        return $this;
    }

    protected function _renderEditor ()
    {
    
    	if(isset($this->_config['mode']))
    		unset($this->_config['mode']);

    	if(isset($this->_config['editor_selector']))
    		unset($this->_config['editor_selector']);
    		
        $script = 'function featureContentTinyMCE(class_selector){' . PHP_EOL;
        $script .= 'tinyMCE.init({' . PHP_EOL;
        $script .= 'mode:"textareas",' . PHP_EOL;
        $script .= 'editor_selector:class_selector,' . PHP_EOL;

        $params = array();
        foreach ($this->_config as $name => $value) {
            if (is_array($value)) {
                $value = implode(',', $value);
            }
            if (!is_bool($value)) {
                $value = '"' . $value . '"';
            }
            $params[] = $name . ': ' . $value;
        }
        $script .= implode(',' . PHP_EOL, $params) . PHP_EOL;

        $script .= ",content_css : '/assets/css/tinymce.css'";
        $script .= '});}';


        $this->view->headScript()->appendScript($script);
        return $this;
    }
}


