<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Form.php 2330 2011-04-05 16:17:58Z leigh $
 */

/**
 * A Form class that will load its elements from a configuration file. To be used for all forms that
 * can have dynamic attributes.
 *
 * @category Mcmr
 * @package Mcmr_Form
 */
class Mcmr_Form extends Zend_Form
{
    /**
     * The field types of all the form elements
     * 
     * @var array
     */
    protected $_elementTypes = null;

    protected $_viewScriptPath = 'form';

    protected $_model = null;
    
    private $_optGroupData = array();

    /**
     * Constructor
     * 
     * @param array|Zend_Config|null $options
     * @return Mcmr_Form
     */
    public function __construct($options = null)
    {
        $this->setAttrib('charset', 'UTF-8'); // Default character encoding

        $this->addPrefixPath('Mcmr_Form_Element', 'Mcmr/Form/Element/', 'element')
                ->addPrefixPath('Mcmr_Form_Decorator', 'Mcmr/Form/Decorator/', 'decorator')
                ->addElementPrefixPath('Mcmr_Validate', 'Mcmr/Validate/', 'validate')
                ->addElementPrefixPath('Mcmr_Filter', 'Mcmr/Filter/', 'filter')
                ->addElementPrefixPath('Mcmr_Filter_File', 'Mcmr/Filter/File', 'filter');

        // We want the form init function called first so that we can
        // append the config file elements to the form afterwards.
        parent::__construct();

        if (!is_array($options) && $options instanceof Zend_Config) {
            $options = $options->toArray();
        } elseif (!is_array($options)) {
            $options = array();
        }

        // Try to load form options from a configuration file
        if (isset($options['configFile'])) {
            $fileoptions = $this->_loadOptionsFromFile($options['configFile']);
        } else {
            $fileoptions = $this->_loadOptionsFromFile();
        }
        $options = array_merge($options, $fileoptions);

        // Now append the options elements and load the rest of the options
        if (isset($options['elements'])) {
            $elements = $this->_processElements($options['elements']);
            $this->addElements($elements);
            unset($options['elements']);
        }

        if (isset($options['subforms'])) {
            $this->_processSubforms($options['subforms']);
            unset($options['subforms']);
        }

        $this->setOptions($options);
        
        $this->postInit();
        
        // Set the form to use view script templates if requested
        if (isset($options['useViewScripts']) && (true == $options['useViewScripts'])) {
            $this->useViewScripts();
        }
    }

    /**
     * Load the default decorators
     *
     * @return void
     */
    public function loadDefaultDecorators()
    {
        return parent::loadDefaultDecorators();
    }

    /**
     * Executed after the object initialisation. Used to insert elements after the configuration file
     * elements
     *
     * @return void
     */
    public function postInit()
    {
    }

    public function useViewScripts()
    {
        foreach ($this->getElements() as $element) {
            $element->viewScriptPath = $this->getViewScriptPath();
            
            $element->setDecorators(
                array(
                    array(
                        'ViewScript',
                        array('viewScript' => $this->getViewScriptPath().'/element.phtml',)
                    )
                )
            );
        }

        foreach ($this->getDisplayGroups() as $group) {
            $group->setDecorators(
                array(
                    array(
                        'ViewScript',
                        array('viewScript' => $this->getViewScriptPath().'/displaygroup.phtml',)
                    )
                )
            );
        }

        // Set elements for sub forms
        foreach ($this->getSubForms() as $form) {
            if ($form instanceof Mcmr_Form) {
                $form->setViewScriptPath($this->getViewScriptPath());
                $form->useViewScripts();
            } else {
                foreach ($form->getElements() as $element) {
                    $element->setDecorators(
                        array(
                            array(
                                'ViewScript',
                                array('viewScript' => $this->getViewScriptPath().'/element.phtml',)
                            )
                        )
                    );
                }
            }
            $form->setDecorators(
                array(array('ViewScript', array('viewScript' => $this->getViewScriptPath().'/subform.phtml',)))
            );
        }
        $this->setDecorators(
            array(array('ViewScript', array('viewScript' => $this->getViewScriptPath().'/form.phtml',)))
        );
    }

    /**
     * Return an array of the element types
     * 
     * @return array
     */
    public function getTypes()
    {
        if (null == $this->_elementTypes) {
            $this->_elementTypes = array();
            foreach ($this->_elements as $element) {
                $type = $element->getAttrib('attrType');
                if (null == $type) {
                    $type = 'string';
                }
                
                $this->_elementTypes[str_replace('attr_', '', $element->getName())] = $type;
            }
        }
        
        return $this->_elementTypes;
    }

    /**
     * Populate form
     *
     * Proxies to {@link setDefaults()}
     *
     * @param  array|Mcmr_Model_ModelAbstract $values
     * @return Zend_Form
     */
    public function  populate(array $values) 
    {
        if ($values instanceof Mcmr_Model_ModelAbstract) {
            $this->_model = $values;
            $values = $this->_model->getOptions(true);
        }
        
        return parent::populate($values);
    }

    /**
     * Get the model set to this form
     *
     * @return Mcmr_Model_ModelAbstract
     */
    public function getModel()
    {
        return $this->_model;
    }

    /**
     * Set the model to use in this form
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return Mcmr_Form
     */
    public function setModel($model)
    {
        $this->_model = $model;

        return $this;
    }


    /**
     * Process each element. Check its privileges and any database source multi options
     *
     * @param array $elements
     * @return array
     */
    protected function _processElements($elements)
    {
        $returnElements = array();

        foreach ($elements as $name=>$element) {
            $element = $this->_processElement($element);
            $returnElements[$name] = $element;
        }
        
        return $returnElements;
    }

    /**
     * Takes a single element description and checks its privileges and database options
     *
     * @param array $element
     * @return array
     */
    protected function _processElement($element)
    {
        if (isset($element['_config'])) {
            $config = $element['_config'];
            $element = $this->_processElementConfig($config, $element);
        }

        // Check the element privileges. Only continue is allowed
        if (isset($element['privilege']) && !$this->_elementHasPermission($element['privilege'])) {
            return;
        }


        // Get the element multiOptions from the database
        if (isset($element['optionSource'])) {
            $element['options']['multiOptions'] = $this->_getMultiOptions($element['optionSource']);
        }

        return $element;
    }

    protected function _processElementConfig($configName, $element) 
    {
        $config = $this->_findElementConfig($configName);
        $newConfig = new Zend_Config(array(), true);
        $newConfig->merge($config);
        $newConfig->merge(new Zend_Config($element));
        return $newConfig->toArray();
    }

    protected function _findElementConfig($name)
    {
        $currentModule = $this->_getModuleName();
        $currentModuleConfig = Zend_Registry::get($currentModule.'-config');
        $newsConfig = Zend_Registry::get('news-config');
        $config = null;
        if (isset($currentModuleConfig) && isset($currentModuleConfig->formElementConfigs->$name)) {
            $config = clone $currentModuleConfig->formElementConfigs->$name;
        }
        if (null===$config) {
            $defualtModuleConfig = Zend_Registry::get('default-config');
            if (isset($defualtModuleConfig->formElementConfigs) && isset($defualtModuleConfig->formElementConfigs->$name)) {
                $config = clone $defualtModuleConfig->formElementConfigs->$name;
            }
        }
        if (null===$config) {
            throw new Exception("Cannot find form element config with name $name");
        }
        if (isset($config->_extends)) {
            $baseConfigName = $config->_extends;
            $baseConfig = $this->_findElementConfig($baseConfigName);
            $newConfig = new Zend_Config(array(),true);
            $newConfig->merge($baseConfig);
            $newConfig->merge($config);
            $config = $newConfig;
        }
        $config->setReadOnly();
        return $config;
    }

    protected function _getModuleName() 
    {
        $parts = explode( '_', get_class($this));
        if (count($parts)>2 ) {
            return strtolower($parts[0]);
        }
        return null;
    }

    protected function _processSubforms(array $subforms)
    {
        foreach ($subforms as $name => $options) {
            $form = new Mcmr_Form($options);
            $form->setElementsBelongTo($name);

            $this->addSubForm($form, $name);
        }

        return $this;
    }

    /**
     * Get the element select options from the database.
     *
     * @param array $source
     * @return array
     */
    protected function _getMultiOptions($source)
    {
        if (isset($source['query'])) {
            $options = $this->_getMultiOptionsQuery($source);
        } elseif (isset($source['mapper'])) {
            $options = $this->_getMultiOptionsMapper($source);
        } else {
            throw new Mcmr_Model_Exception("Unknown option source");
        }
        
        if(!empty($source['regorder']) && is_array($source['regorder'])){
        	$_options = array();
        	foreach($source['regorder'] as $exp){
				foreach($options as $k=>$v){
					if(preg_match($exp,$v)){
						$_options[$k] = $v;
						unset($options[$k]);
					}
				}
			}
			$options = $_options + $options;
			unset($_options);
	    }
        
        return $options;
    }

    protected function _getMultiOptionsQuery($source)
    {
        $module = $source['module'];
        $model = $source['model'];
        $query = $source['query'];
        $models = Mcmr_Model::queryAll($query, $model, $module);
        $options = $this->_convertModelsToOptions($models, $source);
        return $options;
    }


    protected function _getMultiOptionsMapper($source)
    {
        $classname = $source['mapper'];
        $mapper = call_user_func($classname .'::getMapper');
        $conditions = isset($source['conditions'])?$source['conditions']:null;
        $order = isset($source['order'])?$source['order']:null;
        $page = isset($source['page'])?$source['page']:null;
        $models = $mapper->findAllByField($conditions, $order, $page);
        $options = $this->_convertModelsToOptions($models, $source);
        return $options;
    }

    protected function _convertModelsToOptions($models, $source) 
    {

		# init option group
		if(isset($source['optGroup'])){
			$optGroupKey = uniqid();
			if(!isset($this->_optGroupData[$optGroupKey]))
				$this->_optGroupData[$optGroupKey] = array();
				
			if(!isset($source['optGroup']['relationIdMethod']))
				throw new Exception('No relationIdMethod supplied for optGroup');
			if(!isset($source['optGroup']['relationModelMethod']))
				throw new Exception('No relationModelMethod supplied for optGroup');

			if(!isset($source['optGroup']['relationLabelAttribute']))
				$source['optGroup']['relationLabelAttribute'] = 'title';
		}

        $options = array();
        if (isset($source['allowNull']) && $source['allowNull']) {
            if (isset($source['allowNullDisplay'])) {
                $options[] = $source['allowNullDisplay'];
            } else {
                $options[] = '';
            }
        }
        foreach ($models as $model) {
        
        	# check for applicable option group if defined
        	if(isset($source['optGroup'])){
        		
        		if(method_exists($model,$source['optGroup']['relationIdMethod'])){
					$relId = $model->$source['optGroup']['relationIdMethod']();
					if(isset($this->_optGroupData[$optGroupKey][$relId])){
						$relModel = $this->_optGroupData[$optGroupKey][$relId];
					}
					else{
						$relModel = $this->_optGroupData[$optGroupKey][$relId] = $model->$source['optGroup']['relationModelMethod']();
					}

					# model exists -- add to optgroup        			
					if($relModel){
						$labelFn = 'get'.$source['optGroup']['relationLabelAttribute'];
						if(!isset($options[$relModel->$labelFn()])){
							$options[$relModel->$labelFn()] = array();
						}
					
						if(is_array($options[$relModel->$labelFn()])){
							if (isset($source['displayField'])) {
								$options[$relModel->$labelFn()][$model->$source['valueField']] = $this->_convertModelToDisplayField($model,$source['displayField']);
							} else {
								$options[$relModel->$labelFn()][$model->$source['valueField']] = $model;
							}
							continue;
						}
					
					}
				}
        	}

            if (isset($source['displayField'])) {
                $options[$model->$source['valueField']] = $this->_convertModelToDisplayField($model,$source['displayField']);
            } else {
                $options[$model->$source['valueField']] = $model;
            }
        }

        return $options;

    }

    protected function _convertModelToDisplayField($model, $displayField)
    {
    	$fields = explode(',',$displayField);
    	$display = '';
    	foreach($fields as $field){
   			$fn = 'get'.$field;
    		if(property_exists($model,$field))
    			$display .= $model->$field;
    		else if(method_exists($model,$fn))
    			$display .= $model->$fn();
    		else
	    		$display .= $field;
    	}
        return $display;
    }

    /**
     * Check the element privileges
     *
     * @param array $element
     * @return boolean
     */
    protected function _elementHasPermission($privilege)
    {
        $acl = Mcmr_Acl::getInstance();
        $user = Zend_Registry::get('authenticated-user');
        $role = (null !== $user)?$user->getRole():'guest'; // Role is guest if user not logged in

        return $acl->isAllowed($role, $privilege['resource'], $privilege['privilege']);
    }
    
    /**
     * Create an instance of a form or restore one from a session if there is one saved there.
     * To be used in case the form is displayed by a different action to the one that validated it. <br/>
     * If the form is retrieved form the session it's immediately unset there to prevent it from showing up on
     * further views of the page or other pages. By default unsuccessful validation will save the form in
     * automatically and successulf validation will automatically unset it.<br/>
     * The use case for that is following: <br/>
     * The comment form can displayed by a helper from Comment module on any page on the site.
     * This form is always submitted to an action in the Comment module controller, where it's validated and processed.
     * That action then redirect to the page where the form was originally shown.
     * Should the validation fail, error messages are stored in the form instance. We want to display them on the page
     * where the form was shown. Because of the Form API limitations the only way to do that is to pass the actual
     * instance to the helper that shows the form on that page. The easiest way to do it is via session. <br/>
     * 
     * @param form class name
     * @return Mcmr_Form
     */
    public static function getInstance($class, $options = null) 
    {
        $session = new Zend_Session_Namespace('Mcmr_Form');
        //there were some issues with form instanc serialization so 
        //we are making sure the class is loaded before an unserialization attempt is made
        $form = new $class($options);
        $key = self::createSessionKey($class, $options);
        if (isset($session->$key)) {
            $form = unserialize($session->$key);
            unset($session->$key);
        }
        return $form;
    } //getInstance
    
    /**
     * Explicitly save this instance of the form in a session. 
     * @see getInstance
     */
    public static function saveInstance(Mcmr_Form $form, $options = null) 
    {
        $key = self::createSessionKey($form, $options);
        $session = new Zend_Session_Namespace('Mcmr_Form');
        $session->$key = serialize($form);
    } //saveInstance
    
    private static function createSessionKey($form, $options = null) 
    {
        if (is_string($form)) {
          $class = $form;
        } else {
          $class = get_class($form);
        }
        $key = $class;
        if ($options) {
          $key .= '__'.preg_replace('/\W+/', '_', serialize($options));
        }
        return $key;
    }
    
    /**
     * @see Zend_Form::isValid
     */
    public function isValid($options) 
    {
        $res = parent::isValid($options);
        return $res;
    } //isValid

    /**
     * @see Zend_Form::isValidPartial
     */
    public function isValidPartial(array $options=null) 
    {
        $res = parent::isValidPartial($options);
        if (!$res) {
            self::saveInstance($this);
        }
        return $res;
    } //isValidPartial

    public function getViewScriptPath()
    {
        if (null === $this->_viewScriptPath) {
            $this->_viewScriptPath = 'form';
        }

        return $this->_viewScriptPath;
    }

    public function setViewScriptPath($viewScriptPath)
    {
        $this->_viewScriptPath = (string)$viewScriptPath;
    }

    /**
     * Load configuration from config file. If it fails fallback to the default config file
     *
     * @param string $configfile
     * @return array
     */
    protected function _loadOptionsFromFile($configfile = null)
    {
        if (null !== $configfile) {
            if (!file_exists(SITE_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . $configfile)) {
                $configfile = 'forms' . DIRECTORY_SEPARATOR . strtolower(get_class($this)) . '.ini';
            }
        } else {
            $configfile = 'forms' . DIRECTORY_SEPARATOR . strtolower(get_class($this)) . '.ini';
        }

        //if the config file is not found that's alright. we don't want to force
        //writing config files foe every form we use
        //in that cae empty option list is returned
        //if the file is found but not possible to parse the excpetion will be re-thrown
        if (file_exists(SITE_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . $configfile)) {
            $fileoptions = Mcmr_Config_Ini::getInstance($configfile);
            $fileoptions = $fileoptions->toArray();
        } else {
            $fileoptions = array();
        }
        
        return $fileoptions;
    }


}
;