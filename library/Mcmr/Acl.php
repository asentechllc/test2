<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Acl.php 1654 2010-10-27 10:48:37Z leigh $
 */

/**
 * Class to load system ACLs from a configuration file.
 *
 * @category Mcmr
 * @package Mcmr_Acl
 */
class Mcmr_Acl extends Zend_Acl
{
    const CONFIGURATION_FILE = 'acl.ini';
    static private $_instance = null;
    static private $_assertParam = null;
    static private $_roleOptions = array();
    
    private $_messages = array();
    private $_reason = 'User Access Denied';
    
    /**
     * Set the reason string of why access was denied
     */
    public function setReason($reason){
    	$this->_reason = $reason;
    }

    /**
     * Add a message regarding an ACL operation
     */
    public function addMessage($message){
    	$this->_messages[] = $message;
    }
    
    /**
     * Returns any messages that have been added
     *
     * @return array
     */
    public function getMessages(){
    	return $this->_messages;
    }

    /**
     * Returns reason for denial of access
     *
     * @return string
     */
    public function getReason(){
    	return $this->_reason;
    }

    /**
     * Return an instance of Mcmr_Acl. Singleton pattern
     *
     * @return Mcmr_Acl
     */
    static public function getInstance() 
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }

    /**
     * Sets parameters that can be used by ACL assertions to check the user has permission to access the resource
     *
     * @param mixed $param
     */
    static public function setAssertParams($param)
    {
        self::$_assertParam = $param;
    }

    /**
     * Returns the parameters that can be used by ACL assertions
     *
     * @return mixed
     */
    static public function getAssertParams()
    {
        return self::$_assertParam;
    }

    /**
     * Return the ACL resource name, created from the module and controller name
     *
     * @param type $module
     * @param type $controller
     * @return type 
     */
    static public function getResourceName($module, $controller)
    {
        if (null == $module) {
            return $controller;
        }

        if (null == $controller) {
            return $module;
        }

        return "{$module}-{$controller}";
    }

    static public function getRoleOptions()
    {
        return self::$_roleOptions;
    }

    /**
     * Private constructor. Singleton pattern
     */
    private function __construct()
    {
        $config = Mcmr_Config_Ini::getInstance(self::CONFIGURATION_FILE, APPLICATION_ENV);
        
        $this->_addRoles($config->roles);
        $this->_addResources($config->resources);

        if (isset($config->roleOptions)) {
            self::$_roleOptions = $config->roleOptions->toArray();
        } else {
            $aclroles = $this->getRoles();
            $roles = array();
            foreach ($aclroles as $role) {
                $roles[$role] = ucfirst($role);
            }

            self::$_roleOptions = $roles;
        }
    }

    /**
     * Add the system user roles to the ACLs
     * 
     * @param array $roles
     */
    private function _addRoles($roles)
    {
        foreach ($roles as $name => $parents) {
            if (! $this->hasRole($name)) {
                if (empty($parents)) {
                    $parents = array();
                } else {
                    $parents = explode(',', $parents);
                }
                
                $this->addRole(new Zend_Acl_Role($name), $parents);
            }
        }
    }

    /**
     * Add the ACL resources and role permissions
     *
     * @param array $resources
     */
    private function _addResources($resources)
    {
        $assertion = null;
        foreach ($resources as $controller=>$controllers) {
            if ('all' == $controller) {
                $controller = null;
            } else {
                if (! $this->has($controller)) {
                    $this->add(new Zend_Acl_Resource($controller));
                }
            }

            foreach ($controllers as $action=>$roles) {
                foreach ($roles as $role=>$permission) {
                    if (!is_string($permission)) {
                        if (null !== $permission->assertion) {
                            $assertion = new $permission->assertion();
                        }
                        $permission = $permission->permission;
                    }
                    if ('all' == $action) {
                        $action = null;
                    }
                    if ('allow' == $permission) {
                        $this->allow($role, $controller, $action, $assertion);
                    }
                    if ('deny' == $permission) {
                        $this->deny($role, $controller, $action, $assertion);
                    }

                    $assertion = null;
                }
            }
        }
    }

	public static function accessToken($user=null){

		if(empty($user)){
			$user = Zend_Registry::get('authenticated-user');
		}
		
		if($user && $user instanceof User_Model_User){
			return base64_encode($user->getId().','.sha1($user->getEmail().$user->getSalt()));
		}
		return '';
	}
	
	public static function accessTokenValid($token){
		$challenge = explode(',',base64_decode($token));
		if(count($challenge) == 2){
			$mapper = User_Model_User::getMapper();
			$user = $mapper->find(intval($challenge[0]));
			if($user){
				$token = sha1($user->getEmail().$user->getSalt());
				return $token == $challenge[1];
			}
		}
		return false;
	}

}
