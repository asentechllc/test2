<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: AdapterAbstract.php 1922 2010-12-14 10:42:43Z leigh $
 */

/**
 * Interface for payment adapters
 *
 * @category Mcmr
 * @package Mcmr_Payment
 */
abstract class Mcmr_Payment_AdapterAbstract
{
    /**
     * The gateway that should be used for this payment. Either 'live' or 'test'
     * @var string
     */
    protected $_gateway = null;

    /**
     * The gateway user ID. Used by the gateway to identify the vendor
     * @var string
     */
    protected $_vendorName = null;

    /**
     * A unique transaction ID.
     * @var string
     */
    protected $_transactionId = null;

    /**
     * The payment billing address
     * @var Mcmr_Payment_Data_Address
     */
    protected $_billingAddress = null;

    /**
     * The payment delivery address
     * @var Mcmr_Payment_Data_Address
     */
    protected $_deliveryAddress = null;

    /**
     * The credit card information
     * @var Mcmr_Payment_Data_CreditCard
     */
    protected $_creditCard = null;

    /**
     * The items being purchased
     * @var array
     */
    protected $_items = null;

    /**
     * The total amount to be charged. This should be an integer measuring the number
     * of cents or pennies in the amount.
     * @var int
     */
    protected $_amount = null;

    /**
     * A string indicating the payment currency eg. GBP, EUR, USD
     * @var string
     */
    protected $_currency = null;

    /**
     * A description of the transaction to be displayed by the Gateway
     * @var string
     */
    protected $_description = null;

    /**
     * The URL to send the customer on success
     * @var string
     */
    protected $_successUrl = null;

    /**
     * The URL to send the customer on failure
     * @var string
     */
    protected $_failureUrl = null;

    /**
     * A callback URL used by most
     * @var string
     */
    protected $_callbackUrl = null;

    /**
     * The current payment status
     * @var string
     */
    protected $_status = null;

    /**
     * An array of attributes for any values that are not covered by the standard
     * variables
     * @var array
     */
    protected $_attributes = array();

    /**
     * Flag to indicate whether to use 3DSecure
     * @var bool
     */
    protected $_3dSecure = true;

    /**
     * A flag to determine whether to charge tax or not
     * @var bool
     */
    protected $_taxExempt = false;
    
    const STATUS_UNPROCESSED = 'UNPROCESSED';
    const STATUS_OK = 'OK';
    const STATUS_NOTAUTHED = 'NOTAUTHED';
    const STATUS_ABORT = 'ABORT';
    const STATUS_REJECTED = 'REJECTED';
    const STATUS_AUTHENTICATED = 'AUTHENTICATED';
    const STATUS_REGISTERED = 'REGISTERED';
    const STATUS_ERROR = 'ERROR';

    const GATEWAY_TEST = 'test';
    const GATEWAY_LIVE = 'live';

    /**
     * Constructor
     * 
     * $options can be either a Zend_Config object or an array with all the
     * settings
     *
     * @param Zend_Config|array $options
     */
    public function __construct($options = null)
    {
        if ($options instanceof Zend_Config) {
            $this->setConfig($options);
        } elseif (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Set the payment options using an array
     *
     * @param array $options
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $normalized = ucfirst($key);
            $method = 'set' . $normalized;
            if (method_exists($this, $method)) {
                $this->$method($value);
            } else {
                $this->setAttrib($key, $value);
            }
        }
        
        return $this;
    }

    /**
     * Set the payment options using a Zend_Config object
     *
     * @param Zend_Config $config
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());

        return $this;
    }

    /**
     * Get an attribute value
     *
     * @param string $key
     * @return mixed
     */
    public function getAttrib($key)
    {
        $key = (string) $key;
        if (!isset($this->_attributes[$key])) {
            return null;
        }

        return $this->_attributes[$key];
    }

    /**
     * Set an attribute value
     *
     * @param string $key
     * @param mixed $value
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setAttrib($key, $value)
    {
        $key = (string)$key;
        $this->_attributes[$key] = $value;

        return $this;
    }

    /**
     * Get the gateway. If not set the test gateway is returned
     *
     * @return string
     */
    public function getGateway()
    {
        if (null === $this->_gateway) {
            $this->_gateway = self::GATEWAY_TEST;
        }

        return $this->_gateway;
    }

    /**
     * Set the gateway type ID. This should be set to either 'test' or 'live'
     *
     * @param string $gateway
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setGateway($gateway)
    {
        switch ($gateway) {
            case self::GATEWAY_LIVE:
            case self::GATEWAY_TEST:
                $this->_gateway = $gateway;

                return $this;
                break;
        }

        // Gateway must be 'live' or 'test'
        throw new Mcmr_Payment_Exception("Invalid Gateway '{$gateway}'");
    }

    /**
     * Get the vendor name. This should be the ID used by the gateway to identify the
     * vendor's account
     *
     * @return string
     */
    public function getVendorName()
    {
        return $this->_vendorName;
    }

    /**
     * Set the vendor name. This is the ID used by the gateway to identify the
     * vendor's account.
     *
     * @param string $vendorName
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setVendorName($vendorName)
    {
        $this->_vendorName = $vendorName;

        return $this;
    }

    /**
     * Get the payment transaction ID
     *
     * @return string
     */
    public function getTransactionId()
    {
        return $this->_transactionId;
    }

    /**
     * Set the payment transaction ID
     *
     * @param string $transactionId
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setTransactionId($transactionId)
    {
        $this->_transactionId = $transactionId;

        return $this;
    }

        /**
     * Get the payment billing address
     * 
     * @return Mcmr_Payment_Data_Address
     */
    public function getBillingAddress()
    {
        return $this->_billingAddress;
    }

    /**
     * Set the payment billing address
     *
     * @param Mcmr_Payment_Data_Address $billingAddress
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setBillingAddress(Mcmr_Payment_Data_Address $billingAddress)
    {
        $this->_billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get the payment delivery address
     * 
     * @return Mcmr_Payment_Data_Address
     */
    public function getDeliveryAddress()
    {
        return $this->_deliveryAddress;
    }

    /**
     * Set the payment delivery address
     *
     * @param Mcmr_Payment_Data_Address $deliveryAddress
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setDeliveryAddress(Mcmr_Payment_Data_Address $deliveryAddress)
    {
        $this->_deliveryAddress = $deliveryAddress;

        return $this;
    }

    /**
     * Get the credit card data
     *
     * @return Mcmr_Payment_Data_CreditCard
     */
    public function getCreditCard()
    {
        return $this->_creditCard;
    }

    /**
     * Set the credit card data
     *
     * @param Mcmr_Payment_Data_CreditCard $creditCard
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setCreditCard(Mcmr_Payment_Data_CreditCard $creditCard)
    {
        $this->_creditCard = $creditCard;

        return $this;
    }

    /**
     * Get the payment items
     *
     * @return array
     */
    public function getItems()
    {
        if (null === $this->_items) {
            $this->_items = array();
        }

        return $this->_items;
    }

    /**
     * Set the payment items
     *
     * @param array $items
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setItems($items)
    {
        if (is_array($items) || is_object($items) && $items instanceof Traversable) {
            $this->_items = $items;
        } else {
            throw new Zend_Exception("Invalid items");
        }

        return $this;
    }

    /**
     * Add an item to this payment
     *
     * @param Mcmr_Payment_Data_ItemInterface $item
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function addItem(Mcmr_Payment_Data_ItemInterface $item)
    {
        $items = $this->getItems();
        $items[] = $item;
        $this->setItems($items);

        return $this;
    }

    /**
     * Get the payment amount
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->_amount;
    }

    /**
     * Set the payment amount
     *
     * @param int $amount
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setAmount($amount)
    {
        $this->_amount = (int)$amount;

        return $this;
    }

    /**
     * Get the payment currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->_currency;
    }

    /**
     * Set the payment currency
     *
     * @param string $currency
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setCurrency($currency)
    {
        $this->_currency = $currency;

        return $this;
    }

    /**
     * Get the description of the transaction. Gateways can often display this message
     * on the payment screen
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the transaction description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }

    /**
     * Get the payment success URL
     *
     * @return string
     */
    public function getSuccessUrl()
    {
        return $this->_successUrl;
    }

    /**
     * Set the payment success URL
     *
     * @param string $successUrl
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setSuccessUrl($successUrl)
    {
        if (!Zend_Uri::check($successUrl)) {
            throw new Mcmr_Payment_Exception('Invalid Success Url');
        }

        $this->_successUrl = $successUrl;

        return $this;
    }

    /**
     * Get the payment failure URL
     *
     * @return string
     */
    public function getFailureUrl()
    {
        return $this->_failureUrl;
    }

    /**
     * Set the payment failure URL
     *
     * @param string $failureUrl
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setFailureUrl($failureUrl)
    {
        if (!Zend_Uri::check($failureUrl)) {
            throw new Mcmr_Payment_Exception('Invalid Failure Url');
        }

        $this->_failureUrl = $failureUrl;

        return $this;
    }

    /**
     * Get the payment callback URL
     *
     * @return string
     */
    public function getCallbackUrl()
    {
        return $this->_callbackUrl;
    }

    /**
     * Set the payment callback URL
     *
     * @param string $callbackUrl
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setCallbackUrl($callbackUrl)
    {
        if (!Zend_Uri::check($callbackUrl)) {
            throw new Mcmr_Payment_Exception('Invalid Callback Url');
        }

        $this->_callbackUrl = $callbackUrl;

        return $this;
    }

    /**
     * Get the 3DSecure flag
     *
     * @return bool
     */
    public function get3dSecure()
    {
        return $this->_3dSecure;
    }

    /**
     * Set the 3DSecure flag to true or false
     *
     * @param bool $secure
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function set3dSecure($secure)
    {
        $this->_3dSecure = (bool)$secure;

        return $this;
    }

    /**
     * Get the tax exemption flag
     *
     * @return bool
     */
    public function getTaxExempt()
    {
        return $this->_taxExempt;
    }

    /**
     * Set the tax exemption flag
     *
     * @param bool $taxExempt
     * @return Mcmr_Payment_AdapterAbstract 
     */
    public function setTaxExempt($taxExempt)
    {
        $this->_taxExempt = (bool)$taxExempt;
        
        return $this;
    }

        
    /**
     * Get the payment status
     *
     * @return string
     */
    public function getStatus()
    {
        if (null === $this->_status) {
            $this->_status = self::STATUS_UNPROCESSED;
        }

        return $this->_status;
    }

    /**
     * Set the payment status
     *
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->_status = $status;
    }

    /**
     * Process a payment. Returns a payment response object
     *
     * @return Mcmr_Payment_Response
     */
    abstract public function doPayment();

    /**
     * Process a gateway callback request. Returns a payment response object
     * 
     * @return Mcmr_Payment_Response
     */
    abstract public function doCallback($data=null, $paymentResponse=null);
}
