<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: SagepayServer.php 1922 2010-12-14 10:42:43Z leigh $
 */

/**
 * Class for Sagepay payment adapter
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @subpackage Adapter
 */
class Mcmr_Payment_Adapter_SagepayServer extends Mcmr_Payment_AdapterAbstract
{
    const GATEWAY_SIMULATOR = 'simulator';

    const PROTOCOL = '2.23';

    protected $_gateways = array(
        self::GATEWAY_LIVE=>'https://live.sagepay.com/gateway/service/vspserver-register.vsp',
        self::GATEWAY_TEST=>'https://test.sagepay.com/gateway/service/vspserver-register.vsp',
        self::GATEWAY_SIMULATOR=>'https://test.sagepay.com/simulator/VSPServerGateway.asp?Service=VendorRegisterTx',
        );

    /**
     * Get the gateway ID. If not set a default of 'simulator' is returned.
     *
     * @return string
     */
    public function getGateway()
    {
        if (null === $this->_gateway) {
            $this->_gateway = self::GATEWAY_SIMULATOR;
        }

        return $this->_gateway;
    }

    /**
     * Set the gateway ID.
     *
     * @param string $gateway
     * @return Mcmr_Payment_Adapter_SagepayServer
     */
    public function setGateway($gateway)
    {
        if (self::GATEWAY_SIMULATOR === $gateway) {
            $this->_gateway = $gateway;
        } else {
            parent::setGateway($gateway);
        }

        return $this;
    }

    /**
     *
     * @return Mcmr_Payment_Response
     */
    public function doPayment()
    {
        $this->_checkValues();
        $sagepayValues = $this->_registerPayment();

        if (('MALFORMED' === $sagepayValues['Status']) || ('ERROR' === $sagepayValues['Status'])) {
            throw new Mcmr_Payment_Exception($sagepayValues['StatusDetail']);
        } elseif (self::STATUS_OK === $sagepayValues['Status']) {
            // Translate the status to 'registered' At this point we don't want an OK status. 
            // OK should be reserved for successful payments
            $sagepayValues['Status'] = self::STATUS_REGISTERED;
        }
        
        // Build the response object;
        $response = new Mcmr_Payment_Response();
        $response->setProtocol($sagepayValues['VPSProtocol']);
        $response->setMessage($sagepayValues['StatusDetail']);
        $response->setStatus($sagepayValues['Status']);
        $response->setTxId(isset($sagepayValues['VPSTxId'])?$sagepayValues['VPSTxId']:'');
        $response->setVendorTxId($this->getTransactionId());
        $response->setAttrib('nextUrl', isset($sagepayValues['NextURL'])?$sagepayValues['NextURL']:null);
        $response->setAttrib('securityKey', isset($sagepayValues['SecurityKey'])?$sagepayValues['SecurityKey']:null);
        $response->setAttrib('vendorName', $this->getVendorName());

        return $response;
    }

    /**
     * This handles the Sagepay gateway callback.
     *
     * $data is the information that is posted to us by the gateway.
     * $paymentResponse is the response object that was returned by the
     * doPayment() function call. This information is used to verify the payment
     * details.
     *
     * A Payment response object is returned. Echoing out this object will return
     * the necessary information back to SagePay
     *
     * @param array $data
     * @param Mcmr_Payment_Response $paymentResponse
     * @return Mcmr_Payment_Response
     */
    public function doCallback($data=null, $paymentResponse=null)
    {
        $options = array();
        if ($paymentResponse instanceof Mcmr_Payment_Response) {
            if ($this->_verifySignature($data, $paymentResponse)) {
                if ('OK' !== $data['Status']) {
                    $options['status'] = $data['Status'];
                    $options['message'] = $data['StatusDetail'];
                    $options['RedirectURL'] = $this->getFailureUrl();
                } else {
                    $options['status'] = $data['Status'];
                    $options['message'] = '';
                    $options['RedirectURL'] = $this->getSuccessUrl();
                }
            } else {
                $options['status'] = 'ERROR';
                $options['message'] = 'Invalid security signature';
                $options['RedirectURL'] = $this->getFailureUrl();
            }
        } else {
            $options['status'] = 'INVALID';
            $options['message'] = 'Unable to find payment';
            $options['RedirectURL'] = $this->getFailureUrl();
        }
        
        $options['protocol'] = self::PROTOCOL;
        $options['VendorTxId'] = $data['VendorTxCode'];
        $options['txId'] = $data['VPSTxId'];

        return new Mcmr_Payment_Response_SagepayServer($options);
    }

    /**
     * Register the payment with SagePay
     */
    protected function _registerPayment()
    {
        // Build the SagePay transaction registration request
        $billingAddress = $this->getBillingAddress();
        if (null === ($deliveryAddress = $this->getDeliveryAddress())) {
            $deliveryAddress = $billingAddress;
        }
        $profile = (null !== $this->getAttrib('profile'))?$this->getAttrib('profile'):'LOW';

        $client = new Zend_Http_Client($this->_gateways[$this->getGateway()]);
        $client->setMethod(Zend_Http_Client::POST);

        $postData = array(
            'VPSProtocol'=>self::PROTOCOL,
            'TxType'=>'PAYMENT',
            'Vendor'=>$this->getVendorName(),
            'VendorTxCode'=>$this->getTransactionId(),
            'Amount'=>sprintf("%01.2f", ($this->getAmount()/100)),
            'Currency'=>$this->getCurrency(),
            'Description'=>$this->getDescription(),
            'NotificationURL'=>$this->getCallbackUrl(),
            'BillingSurname'=>$billingAddress->getSurname(),
            'BillingFirstnames'=>$billingAddress->getFirstname(),
            'BillingAddress1'=>$billingAddress->getAddress1(),
            'BillingAddress2'=>$billingAddress->getAddress2(),
            'BillingCity'=>$billingAddress->getCity(),
            'BillingPostCode'=>$billingAddress->getPostcode(),
            'BillingCountry'=>$billingAddress->getCountry(),
            'BillingState'=>$billingAddress->getState(),
            'BillingPhone'=>$billingAddress->getPhone(),
            'DeliverySurname'=>$deliveryAddress->getSurname(),
            'DeliveryFirstnames'=>$deliveryAddress->getFirstname(),
            'DeliveryAddress1'=>$deliveryAddress->getAddress1(),
            'DeliveryAddress2'=>$deliveryAddress->getAddress2(),
            'DeliveryCity'=>$deliveryAddress->getCity(),
            'DeliveryPostCode'=>$deliveryAddress->getPostcode(),
            'DeliveryCountry'=>$deliveryAddress->getCountry(),
            'DeliveryState'=>$deliveryAddress->getState(),
            'DeliveryPhone'=>$deliveryAddress->getPhone(),
            'CustomerEmail'=>$this->getAttrib('customerEmail'),
            'Basket'=>$this->_buildBasket(),
            'AllowGiftAid'=>$this->getAttrib('giftAid'),
            'ApplyAVSCV2'=>$this->getAttrib('applyAVSCV2'),
            'Apply3DSecure'=>$this->get3dSecure(),
            'Profile'=>$profile,
            'BillingAgreement'=>$this->getAttrib('billingAgreement'),
            'AccountType'=>$this->getAttrib('accountType'),
        );
        $client->setParameterPost($postData);
        $client->request();

        return $this->_parseResponse($client->getLastResponse()->getBody());
    }

    /**
     * Check the payment settings. If any are missing an exception is thrown
     *
     * @throws Mcmr_Payment_Exception
     */
    protected function _checkValues()
    {
        if (null === ($billingAddress = $this->getBillingAddress())) {
            throw new Mcmr_Payment_Exception_Address('Cannot perform payment. Billing Address not set');
        }

        if (null === ($amount = $this->getAmount())) {
            throw new Mcmr_Payment_Exception_Amount('Cannot perform payment. Amount not set');
        }

        if (null === ($currency = $this->getCurrency())) {
            throw new Mcmr_Payment_Exception_Currency('Cannot perform payment. Currency not set');
        }

        if (null === ($transactionId = $this->getTransactionId())) {
            throw new Mcmr_Payment_Exception_Transaction('Cannot perform payment. Transaction ID not set');
        }

        if (null === ($callbackUrl = $this->getCallbackUrl())) {
            throw new Mcmr_Payment_Exception_Callback('Cannot perform payment. Callback URL not set');
        }

        if (null === $this->getDescription()) {
            throw new Mcmr_Payment_Exception_Description('Cannot perform payment. Description not set');
        }
    }

    /**
     * Takes a SagePay response and returns an array of Key Values
     *
     * @param string $data
     * @return array
     */
    protected function _parseResponse($data)
    {
        $response = array();

        $lines = explode("\n", $data);
        foreach ($lines as $line) {
            if (!empty($line)) {
                list($key, $value) = explode('=', $line, 2);
                $response[$key] = rtrim($value);
            }
        }

        // Translate the SagePay status codes to our response status codes

        switch ($response['Status']) {
            case 'OK':
                $response['Status'] = Mcmr_Payment_Response::STATUS_OK;
                break;

            case 'INVALID':
                $response['Status'] = Mcmr_Payment_Response::STATUS_INVALID;
                break;
        }

        return $response;
    }

    /**
     * Take the callback data and verify the supplied signature with the data we sent
     *
     * @param array $data
     * @param Mcmr_Payment_Response $paymentResponse
     * @return bool
     */
    protected function _verifySignature($data, Mcmr_Payment_Response $paymentResponse)
    {
        if (!isset($data['CAVV'])) {
            $data['CAVV'] = '';
        }
        if (!isset($data['AddressStatus'])) {
            $data['AddressStatus'] = '';
        }
        if (!isset($data['PayerStatus'])) {
            $data['PayerStatus'] = '';
        }

        $verificationString =
            $paymentResponse->getTxId() .
            $paymentResponse->getVendorTxId() .
            $data['Status'] .
            $data['TxAuthNo'] .
            $paymentResponse->getAttrib('vendorName') .
            $data['AVSCV2'] .
            $paymentResponse->getAttrib('securityKey') .
            $data['AddressResult'] .
            $data['PostCodeResult'] .
            $data['CV2Result'] .
            $data['GiftAid'] .
            $data['3DSecureStatus'] .
            $data['CAVV'] .
            $data['AddressStatus'] .
            $data['PayerStatus'] .
            $data['CardType'] .
            $data['Last4Digits'] .
            '';

        return $data['VPSSignature'] === strtoupper(md5($verificationString));
    }

    protected function _buildBasket()
    {
        $items = $this->getItems();
        $incTax = !$this->getTaxExempt();

        $basket = array();
        foreach ($items as $item) {
            if ($item instanceof Mcmr_Payment_Data_ItemInterface) {
                if ($incTax) {
                    $tax = $item->getTax()/100;
                    $value = ($item->getAmount($incTax) - $item->getTax())/100;
                } else {
                    $tax = 0;
                    $value = $item->getAmount($incTax);
                }
                
                $total = ($item->getAmount($incTax) * $item->getQuantity())/100;
                $valueIncTax = $item->getAmount($incTax)/100;
                $description = str_replace(":", "", $item->getDescription());
                
                $basket[] = "{$description}:{$item->getQuantity()}:{$value}:{$tax}:{$valueIncTax}:{$total}";
            }
        }

        if (empty($basket)) {
            return '';
        } else {
            return count($items).':'.implode(':', $basket);
        }
    }
}
