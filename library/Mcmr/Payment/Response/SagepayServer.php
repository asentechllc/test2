<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: SagepayServer.php 1230 2010-08-16 14:05:47Z leigh $
 */

/**
 * Class for Sagepay server payment adapter responses
 *
 * @category Mcmr
 * @package Mcmr_Payment
 */
class Mcmr_Payment_Response_SagepayServer extends Mcmr_Payment_Response
{
    public function __toString()
    {
        return "Status={$this->getStatus()}\r\n" .
            "RedirectURL={$this->getAttrib('RedirectURL')}\r\n" .
            "StatusDetail={$this->getMessage()}\r\n";
    }
}
