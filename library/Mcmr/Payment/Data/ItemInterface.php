<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Interface for Payment item data
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @subpackage Data
 */
interface Mcmr_Payment_Data_ItemInterface
{
    /**
     * @return string Short description of the item
     */
    public function getDescription();

    /**
     * @return int The number of items being purchased
     */
    public function getQuantity();

    /**
     * @return int The value of the item including tax in pence/cents
     */
    public function getAmount();

    /**
     * @return int The amount of tax on the item in pence/cents
     */
    public function getTax();
    
}
