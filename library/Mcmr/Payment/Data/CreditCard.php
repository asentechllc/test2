<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CreditCard.php 1210 2010-08-11 17:11:19Z leigh $
 */

/**
 * Class for Payment credit card data
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @subpackage Data
 */
class Mcmr_Payment_Data_CreditCard
{
    
}