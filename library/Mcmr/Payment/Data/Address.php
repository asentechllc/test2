<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Address.php 1643 2010-10-26 13:45:11Z leigh $
 */

/**
 * Class for Payment address data
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @subpackage Data
 */
class Mcmr_Payment_Data_Address extends Mcmr_Model_ModelAbstract
{
    /**
     * Address destination first name
     * @var string
     */
    protected $_firstname = null;

    /**
     * Address destination surname
     * @var string
     */
    protected $_surname = null;

    /**
     * First line of the address
     * @var string
     */
    protected $_address1 = null;

    /**
     * Optional second line of the address
     * @var string
     */
    protected $_address2 = null;

    /**
     * Address postcode
     * @var string
     */
    protected $_postcode = null;

    /**
     * Address city
     * @var string
     */
    protected $_city = null;

    /**
     * Address state
     * @var string
     */
    protected $_state = null;

    /**
     * Address Country
     * @var string
     */
    protected $_country = null;

    /**
     * Address company
     * @var string
     */
    protected $_company = null;

    /**
     * Customer Phone number
     * @var string
     */
    protected $_phone = null;

    /**
     * Is billing address eligible for exeption from tax?
     */
    protected $_taxExempt = null;
    protected $_taxIncluded = null;

    /**
     * Constructor
     *
     * $options can be either an array or an instance of Zend_Config
     *
     * @param Zend_Config $options
     */
    public function __construct($options = null)
    {
        if (null !== $options) {
            if ($options instanceof Zend_Config) {
                $this->setConfig($options);
            } elseif (is_array($options)) {
                $this->setOptions($options);
            }
        }
    }

    /**
     * Set the values of this object using an instance of Zend_Config
     *
     * @param Zend_Config $config
     * @return Mcmr_Payment_Data_Address
     */
    public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());
        
        return $this;
    }

    /**
     * Get the Address first name
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->_firstname;
    }

    /**
     * Set the address first name
     * @param string $firstname
     * @return Mcmr_Payment_Data_Address
     */
    public function setFirstname($firstname)
    {
        $this->_firstname = $firstname;

        return $this;
    }

    /**
     * Get the address surname
     * @return string
     */
    public function getSurname()
    {
        return $this->_surname;
    }

    /**
     * Set the address surname
     * 
     * @param string $surname
     * @return Mcmr_Payment_Data_Address
     */
    public function setSurname($surname)
    {
        $this->_surname = $surname;

        return $this;
    }

    /**
     * Set the first line of the address
     *
     * @return string
     */
    public function getAddress1()
    {
        return $this->_address1;
    }

    /**
     * Set the first line of the address
     *
     * @param string $address1
     * @return Mcmr_Payment_Data_Address
     */
    public function setAddress1($address1)
    {
        $this->_address1 = $address1;

        return $this;
    }

    /**
     * Get the second line of the address
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->_address2;
    }

    /**
     * Set the second line of the address
     *
     * @param string $address2
     * @return Mcmr_Payment_Data_Address
     */
    public function setAddress2($address2)
    {
        $this->_address2 = $address2;

        return $this;
    }

    /**
     * Get the address postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->_postcode;
    }

    /**
     * Set the postcode
     *
     * @param string $postcode
     * @return Mcmr_Payment_Data_Address
     */
    public function setPostcode($postcode)
    {
        $this->_postcode = $postcode;

        return $this;
    }

    /**
     * Get the address city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->_city;
    }

    /**
     * Set the address city
     *
     * @param string $city
     * @return Mcmr_Payment_Data_Address
     */
    public function setCity($city)
    {
        $this->_city = $city;

        return $this;
    }

    /**
     * Get the address state
     *
     * @return string
     */
    public function getState()
    {
        if (!$this->_state) {
            $this->_state = '';
        }
        
        return $this->_state;
    }

    /**
     * Set the address state
     *
     * @param string$state
     * @return Mcmr_Payment_Data_Address
     */
    public function setState($state)
    {
        $this->_state = $state;

        return $this;
    }

    /**
     * Get the address state
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->_country;
    }

    /**
     * Set the address country
     *
     * @param string $country
     * @return Mcmr_Payment_Data_Address 
     */
    public function setCountry($country)
    {
        $this->_country = $country;

        return $this;
    }

    /**
     * Get the address company name
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * Set the address company name
     *
     * @param string $company
     * @return Mcmr_Payment_Data_Address
     */
    public function setCompany($company)
    {
        $this->_company = $company;

        return $this;
    }

    /**
     * Get the customer phone number
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->_phone;
    }

    /**
     * Set the customer phone number
     *
     * @param string $phone
     * @return Mcmr_Payment_Data_Address
     */
    public function setPhone($phone)
    {
        $this->_phone = $phone;

        return $this;
    }


    /**
     * Is billing address eligible for exeption from tax?
     * We store exemption rather than more sensible icnlusion to keep it in line with what is already done 
     * in Payment_Model_payment
     */
    public function getTaxExempt()
    {
        if (null===$this->_taxExempt) {
            $this->_taxExempt = false;
        }
        return $this->_taxExempt;
    }

    public function setTaxExempt($taxExempt) 
    {
        $this->_taxExempt = (bool)$taxExempt;
        $this->_taxIncluded = !$this->_taxExempt;
        return $this;
    }


    /**
     * For the purpose of completeness and ease of use of the form  allow getting/setting tax inclusion rather than exemption
     */
    public function getTaxIncluded()
    {
        return !$this->getTaxExempt();
    }


    public function setTaxIncluded($taxIncluded)
    {
        return $this->setTaxExempt(!$taxIncluded);
    }


}
