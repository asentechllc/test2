<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Payment
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Exception.php 1226 2010-08-16 10:44:09Z leigh $
 */

/**
 * Exception class for Mcmr_Payment
 *
 * @category Mcmr
 * @package Mcmr_Payment
 */
class Mcmr_Payment_Exception_Currency extends Mcmr_Payment_Exception
{
}

