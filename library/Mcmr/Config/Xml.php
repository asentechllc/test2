<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Config
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Xml.php 709 2010-05-27 11:00:15Z leigh $
 */

/**
 * An Xml configuration class.
 *
 * A singleton class that takes a configuration filename and returns an instance of Zend_Config_Xml.
 * Will attempt to cache the config object to reduce load time.
 *
 * @category Mcmr
 * @package Mcmr_Config
 * @subpackage Xml
 */
class Mcmr_Config_Xml extends Zend_Config_Xml
{

    /**
     * Load an xml config file.
     *
     * @param string $filename
     * @return Zend_Config
     */
    static function getInstance($filename, $section = null)
    {
        // Load the config from the cache if possible
        $cacheid = str_replace(array('-', '/', '.', '\\'), '_', $filename);
        $manager = Zend_Registry::get('cachemanager');
        $cache = $manager->getCache('config');
        
        if (null !== $cache) {
            try {
                $cache->setMasterFiles(array(SITE_PATH . '/configs/' . $filename));
            } catch (Zend_Cache_Exception $e) {
                // The file doesn't exist. But we don't want a cache exception thrown. Disable cache.
                // Allow config exeption to be thrown later
                $cache = null;
            }
        }
        if ((null == $cache) || (! $config = $cache->load($cacheid))) {
            // Not cached; create instance
            $config = new Zend_Config_Xml(SITE_PATH . '/configs/' . $filename, $section);

            if (null !== $cache) {
                $cache->save($config, $cacheid);
            }
        }

        return $config;
    }
}
