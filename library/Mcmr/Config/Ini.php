<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Config
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Ini.php 1202 2010-08-09 09:14:06Z leigh $
 */

/**
 * A Ini configuration class.
 * 
 * A singleton class that takes a configuration filename and returns an instance of Zend_Config_Ini.
 * Will attempt to cache the config object to reduce load time.
 *
 * @category Mcmr
 * @package Mcmr_Config
 * @subpackage Ini
 */
class Mcmr_Config_Ini extends Zend_Config_Ini
{

    /**
     * Load an ini config file.
     * 
     * @param string $filename
     * @param string $section The Ini file section to use. If not set will use APPLICATION_ENV
     * @return Zend_Config
     */
    static function getInstance($filename, $section = null)
    {
        if (null === $section) {
            $section = APPLICATION_ENV;
        }
        
        // Load the config from the cache if possible
        $cacheid = str_replace(array('-', '/', '.', '\\'), '_', $filename);
        $manager = Zend_Registry::get('cachemanager');
        $cache = $manager->getCache('config');
        
        if (null !== $cache) {
            try {
                $cache->setMasterFiles(array(SITE_PATH . '/configs/' . $filename));
            } catch (Zend_Cache_Exception $e) {
                // The file doesn't exist. But we don't want a cache exception thrown. Disable cache.
                // Allow config exeption to be thrown later
                $cache = null;
            }
        }
        if ((null == $cache) || (! $config = $cache->load($cacheid))) {
            // Not cached; create instance, make it modifiable by default
            $config = new Mcmr_Config_Ini(SITE_PATH . '/configs/' . $filename, $section);

            if (null !== $cache) {
                $cache->save($config, $cacheid);
            }
        }
        
        return $config;
    }

}
