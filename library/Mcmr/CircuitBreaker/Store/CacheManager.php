<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CacheManager.php 2277 2011-03-25 15:57:35Z leigh $
 */

/**
 * A circuit breaker store using Zend_Cache and the SqLite backend
 * 
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @subpackage Store
 */
class Mcmr_CircuitBreaker_Store_CacheManager extends Mcmr_CircuitBreaker_Store_StoreAbstract
{
    private $_cache;
    private $_name;

    public function init()
    {
        $name = $this->getName();
        $cacheManager = $bootstrap->bootstrap('CacheManager')
            ->getResource('CacheManager');
        if (null !== $cacheManager &&
            $cacheManager->hasCache($name)
        ) {
            $this->_cache = $cacheManager->getCache($name);
        } else {
            throw new Mcmr_CircuitBreaker_Exception("Circuitbreaker cache '{$name}' does not exist");
        }
    }

    public function setName($name)
    {
        $this->_name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->_name;
    }

    /**
     * Load the Circuit Breaker state information
     */
    public function load()
    {
        $this->_store = $this->_cache->load($this->_appid);
    }

    /**
     * Save the Circuit Breaker state information
     */
    public function save()
    {
        $this->_cache->save($this->_store, $this->_appid);
    }

}
