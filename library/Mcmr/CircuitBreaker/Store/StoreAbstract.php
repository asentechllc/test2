<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: StoreAbstract.php 2277 2011-03-25 15:57:35Z leigh $
 */

/**
 * Abstract class for Circuit breaker stores
 * 
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @subpackage Store
 */
abstract class Mcmr_CircuitBreaker_Store_StoreAbstract implements Mcmr_CircuitBreaker_Store_StoreInterface
{
    protected $_appid = null;
    protected $_store = null;

    public function __construct($options = array())
    {
        $this->_appid = $appid;
        $this->_store = new stdClass();

        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }

        $this->init();
    }

    public function init()
    {
    }

    public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());

        return $this;
    }

    /**
     * Set properties based on the array key/value pairs
     *
     * @param array $options
     * @return Mcmr_Model_ModelAbstract
     */
    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst(str_replace('_', '', $key));
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }

        return $this;
    }
    
    public function setAppid($appid)
    {
        $this->_appid = $appid;
        
        return $this;
    }

    public function getAppid()
    {
        return $this->_appid;
    }

    /**
     * Get the current state name
     *
     * @return string
     */
    public function getState()
    {
        if (isset($this->_store->state)) {
            return $this->_store->state;
        } else {
            return 'closed';
        }
    }

    /**
     * Get the number of failures
     * 
     * @return int
     */
    public function getFailures()
    {
        if (isset($this->_store->failures)) {
            return $this->_store->failures;
        } else {
            return 0;
        }
    }

    /**
     * Get the current timeout in seconds
     *
     * @return int
     */
    public function getTimeout()
    {
        if (isset($this->_store->timeout)) {
            return $this->_store->timeout;
        } else {
            return 30;
        }
    }

    /**
     * Get unix time when the circuit was tripped.
     * 
     * @return int
     */
    public function getTrippedtime()
    {
        if (isset($this->_store->tripped)) {
            return $this->_store->tripped;
        } else {
            return 0;
        }
    
    }

    /**
     * Set the state name in the store.
     * 
     * @param string $state
     */
    public function setState($state)
    {
        $this->_store->state = $state;
    }

    /**
     * Set the number of failures
     *
     * @param int $failures
     */
    public function setFailures($failures)
    {
        $this->_store->failures = $failures;
    }

    /**
     * Set the faulure timeout
     *
     * @param int $timeout
     */
    public function setTimeout($timeout)
    {
        $this->_store->timeout = $timeout;
    }

    /**
     * Set the unix time of when the circuit was tripped
     *
     * @param int $time
     */
    public function setTrippedtime($time)
    {
        $this->_store->tripped = $time;
    }
}