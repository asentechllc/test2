<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CacheSqlite.php 2277 2011-03-25 15:57:35Z leigh $
 */

/**
 * A circuit breaker store using Zend_Cache and the SqLite backend
 * 
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @subpackage Store
 */
class Mcmr_CircuitBreaker_Store_CacheSqlite extends Mcmr_CircuitBreaker_Store_StoreAbstract
{
    private $_cache;

    public function init()
    {
        $this->_cache = Zend_Cache::factory(
            'Core', 'Sqlite', array(), array(
                'cache_db_complete_path' => SITE_PATH.'/data/cbstore.sqlite',
                'automatic_serialization' => true
            )
        );
    }

    /**
     * Load the Circuit Breaker state information
     */
    public function load()
    {
        $this->_store = $this->_cache->load($this->getAppid());
    }

    /**
     * Save the Circuit Breaker state information
     */
    public function save($appid = null)
    {
        if (null === $appid) {
            $appid = $this->getAppid();
        }

        if (null === $appid) {
            throw new Mcmr_CircuitBreaker_Exception("Cannot save CB state. Appid is not set");
        }

        $this->_cache->save($this->_store, $appid);
    }

}
