<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: StoreInterface.php 2277 2011-03-25 15:57:35Z leigh $
 */

/**
 * Circuit Breaker storage interface
 * 
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @subpackage Store
 */
interface Mcmr_CircuitBreaker_Store_StoreInterface
{
    public function load();
    public function save($appid = null);
    
    public function getState();
    public function getFailures();
    public function getTimeout();
    public function getTrippedtime();
        
    public function setState($state);
    public function setFailures($failures);
    public function setTimeout($timeout);
    public function setTrippedtime($time);
}
