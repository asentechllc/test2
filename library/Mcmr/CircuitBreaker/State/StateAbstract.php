<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: StateAbstract.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Abstract class for all CurcuitBreaker states
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @subpackage State
 */
abstract class Mcmr_CircuitBreaker_State_StateAbstract
{
    protected $_circuitbreaker;
    protected $_statename;

    /**
     * State factory. Returns the state object according to the state name
     * 
     * @param $circuitbreaker
     * @param $state
     * @return Mcmr_CircuitBreaker_State_StateAbstract
     */
    public static function getInstance($circuitbreaker, $state)
    {
        $statename = ucfirst(strtolower($state));
        $classname = 'Mcmr_CircuitBreaker_State_' . $statename;
        
        if (class_exists($classname)) {
            $state = new $classname($circuitbreaker);
            $state->_statename = $statename;
            
            return $state;
        } else {
            throw new Zend_Exception("Invalid Circuit Breaker State '{$state}'");
        }
    }

    public function __construct(Mcmr_CircuitBreaker $circuitbreaker)
    {
        $this->_circuitbreaker = $circuitbreaker;
    }

    public function getName()
    {
        return $this->_statename;
    }
    
    public function onCalling()
    {
    }

    public function onCalled()
    {
        $this->_circuitbreaker->resetFailures();
    }

    public function onException()
    {
        $this->_circuitbreaker->incrementFailures();
    }
}
