<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Open.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Circuit breaker open state
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @subpackage State
 */
class Mcmr_CircuitBreaker_State_Open extends Mcmr_CircuitBreaker_State_StateAbstract
{
    /**
     * Called when about to execute protected code. Set state to 'half' if timeout passed
     * Throw exception otherwise.
     *
     * @throws Mcmr_CircuitBreaker_Exception
     */
    public function onCalling()
    {
        parent::onCalling();
        
        // If the timeout has passed set the circuit to half open. Else throw exception.
        if (time() > $this->_circuitbreaker->getTimeout() + $this->_circuitbreaker->getTrippedtime()) {
            $this->_circuitbreaker->setState('half');
        } else {
            throw new Mcmr_CircuitBreaker_Exception('Circuit is open');
        }
    }
}
