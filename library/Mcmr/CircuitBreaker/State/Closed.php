<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Closed.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Closed state for Circuitbreaker
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @subpackage State
 */
class Mcmr_CircuitBreaker_State_Closed extends Mcmr_CircuitBreaker_State_StateAbstract
{
    /**
     * Called when exception detected on executed code. Set's circuit to open if threshold reached
     */
    public function onException()
    {
        parent::onException();
        
        if ($this->_circuitbreaker->thresholdReached()) {
            $this->_circuitbreaker->setState('open');
        }
    }
}
