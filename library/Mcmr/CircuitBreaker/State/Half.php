<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Half.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Half-open state for circuit breaker
 *
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 * @subpackage State
 */
class Mcmr_CircuitBreaker_State_Half extends Mcmr_CircuitBreaker_State_StateAbstract
{

    /**
     * Called when exception detected on executed code. Set's circuit to open
     */
    public function onException()
    {
        parent::onException();
        
        $this->_circuitbreaker->setState('open');
    }

    /**
     * Called when code successfully executed. Set circuit to closed.
     */
    public function onCalled()
    {
        parent::onCalled();

        $this->_circuitbreaker->setState('closed');
    }
}
