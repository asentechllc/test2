<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 *
 * @category Mcmr
 */
class Mcmr_View extends Zend_View
{
	protected $_deviceViews;

    public function scriptExists($path)
    {
        $originalIncludePath = get_include_path();
        $viewPath = implode(PATH_SEPARATOR, $this->getScriptPaths());
        set_include_path($viewPath);
        $result = stream_resolve_include_path($path);
        set_include_path($originalIncludePath);
        return $result;
    }

    public function setDeviceViews($val) {
    	$this->_deviceViews = $val;
    	return $this;
    }

    public function getDeviceViews()
    {
    	return $this->_deviceViews;
    }

    public function __construct($config = array())
    {
    	parent::__construct($config);
    	if (isset($config['deviceViews'])) {
	    	$this->_deviceViews = $config['deviceViews'];
    	}
    }


}
