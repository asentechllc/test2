<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Paginator
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Paginator.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Paginator class
 *
 * @category Mcmr
 * @package Mcmr_Paginator
 */
class Mcmr_Paginator extends Zend_Paginator
{
    public $itemCount;

    /**
     * Extended Constructor to pass second argument for item count to return if "initialCount" value is set in config
     * @param Zend_Paginator_Adapter  $adapter  Paginator Adapter
     * @param mixed $itemCount                  Number of items to return (will return all if "false")
     */
    public function __construct($adapter, $itemCount = false) {
        if ($itemCount) {
            $this->itemCount = $itemCount;
        }

        parent::__construct($adapter);
    }

    /**
     * Returns the items of the current page as JSON.
     *
     * @return string
     */
    public function toJson()
    {
        $currentItems = $this->getCurrentItems();

        if ($currentItems instanceof Zend_Db_Table_Rowset_Abstract) {
            $items = $currentItems->toArray();
        } else {
            $items = array();
            foreach ($currentItems as $item) {
                if (is_object($item) && method_exists($item, 'toArray')) {
                    $items[] = $item->toArray();
                } else {
                    $items[] = $item;
                }
            }
        }

        if ($this->itemCount) {
            $items = array_slice($items, 0, $this->itemCount);
        }

        return Zend_Json::encode($items);
    }

    public function toArray()
    {
        $currentItems = $this->getCurrentItems();
        if ($currentItems instanceof Zend_Db_Table_Rowset_Abstract) {
            $items = $currentItems->toArray();
        } else {
            $items = array();
            foreach ($currentItems as $item) {
                $items[] = $item;
            }
        }

        if ($this->itemCount) {
            $items = array_slice($items, 0, $this->itemCount);
        }

        return $items;
    }
}