<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Test
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Class used to build and then clean out test data. It can register models that will
 * be deleted at the end of testing leaving the database in a clean state before the next
 * test run.
 *
 * @category Mcmr
 * @package Mcmr_Test
 */
class Mcmr_Test_DataBuilder
{
    protected static $_instance = null;
    private $_models = array();
    private $_cleanupSql = array();

    protected function __construct() 
    {
        if ('production' === APPLICATION_ENV) {
            throw new Exception("Cannot use test databuilder in production environment");
        }
        
        $this->setupClean();
        $this->_cleanupModels();
        $this->_cleanupSql();
        
        $caches = array(
            'database',
            'config',
            'metadata',
            'fullpage',
        );
        
        // Clear the caches ready for clean testing
        $cachemanager = Zend_Registry::get('cachemanager');
        foreach ($caches as $cachename) {
            $cache = $cachemanager->getCache($cachename);
            if (null !== $cache) {
                $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
            }
        }
    }
    
    /**
     * Cleanup all the registered models and sql
     */
    public function __destruct()
    {
        $this->_cleanupModels();
        $this->_cleanupSql();
    }
    
    /**
     * Get an instance of the unit test data builder.
     * Singleton pattern
     *
     * @return Mcmr_Test_DataBuilder 
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }
    
    /**
     * Add a model to be automatically removed at the end of testing
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return Mcmr_Test_DataBuilder 
     */
    public function addCleanupModel(Mcmr_Model_ModelAbstract $model)
    {
        $this->_models[] = $model;
        
        return $this;
    }
    
    /**
     * Add SQL to be automatically executed at the end of testing.
     *
     * @param string $sql
     * @return Mcmr_Test_DataBuilder 
     */
    public function addCleanupSql($sql)
    {
        $this->_cleanupSql[] = $sql;
        
        return $this;
    }
    
    /**
     * Executed at the start to clean up data from the previous tests
     */
    public function setupClean()
    {
    }
    
    /**
     * Delete all the models that have been registered
     */
    protected function _cleanupModels()
    {
        foreach ($this->_models as $model) {
            $mapper = $model->getMapper();
            $mapper->setObserversEnabled(false);
            $mapper->delete($model);
        }
        
        $this->_models = array();
    }
    
    /**
     * Execute all the cleanup SQL that has been registered
     */
    protected function _cleanupSql()
    {
        $adapter = Zend_Db_Table::getDefaultAdapter();
        foreach ($this->_cleanupSql as $sql) {
            $adapter->query($sql);
        }
        
        $this->_cleanupSql = array();
    }
}
