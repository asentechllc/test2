<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Test
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A class for all Selenium Tests.
 *
 * @category Mcmr
 * @package Mcmr_Test
 */
class Mcmr_Test_Selenium extends PHPUnit_Extensions_SeleniumTestCase
{
    protected $_browserUrl = null;
    
    public function __construct($name = NULL, array $data = array(), $dataName = '', array $browser = array()) 
    {
        $this->captureScreenshotOnFailure = false;
        $this->screenshotPath = SITE_PATH."/public/files/images/tests/";
        $this->screenshotUrl = $this->_browserUrl . 'files/images/tests/';
        
        $this->init();

        parent::__construct($name, $data, $dataName, $browser);
    }
    
    public function init()
    {
    }
    
    /**
     * Called before the unit tests are started.
     */
    protected function setUp()
    {
        if (null === $this->_browserUrl) {
            throw new Zend_Exception("_browserUrl is not set. Unable to set up tests");
        }
        
        $this->setBrowser("*googlechrome /usr/bin/google-chrome");
        $this->setBrowserUrl($this->_browserUrl);
        $this->autoStop = false;
        $this->start();
        
        $caches = array(
            'database',
            'config',
            'metadata',
            'fullpage',
        );
        
        // Clear the caches ready for clean testing
        $cachemanager = Zend_Registry::get('cachemanager');
        foreach ($caches as $cachename) {
            $cache = $cachemanager->getCache($cachename);
            if (null !== $cache) {
                $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
            }
        }
    }

    protected function tearDown()
    {
    }
    
    public function waitForPageToLoad($timeout)
    {
        parent::waitForPageToLoad($timeout);
        
        $this->assertNoErrors();
    }
    
    public function assertNoErrors()
    {
        $this->assertFalse($this->isTextPresent("Fatal error:"));
        $this->assertFalse($this->isTextPresent("Warning:"));
        $this->assertFalse($this->isTextPresent("Notice:"));
    }
}
