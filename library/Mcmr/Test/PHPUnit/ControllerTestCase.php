<?php
require_once 'Zend/Test/PHPUnit/ControllerTestCase.php';

abstract class Mcmr_Test_PHPUnit_ControllerTestCase extends Zend_Test_PHPUnit_ControllerTestCase
{
    
    /**
     * Retrieve test case request object
     *
     * @return Mcmr_Controller_Request_HttpTestCase
     */
    public function getRequest()
    {
        if (null === $this->_request) {
            $this->_request = new Mcmr_Controller_Request_HttpTestCase;
        }
        return $this->_request;
    }
}
