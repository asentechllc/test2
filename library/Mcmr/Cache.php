<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Cache.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 *
 * @category Mcmr
 * @package Mcmr_Cache
 */
class Mcmr_Cache extends Zend_Cache
{
    /**
     * 
     * @param $frontend
     * @param $frontendoptions
     * @return Zend_Cache_Core|Zend_Cache_Frontend
     */
    static function factory($frontend, $frontendoptions = array())
    {
        if (!isset($frontendoptions['automatic_serialization'])) {
            $frontendoptions['automatic_serialization'] = true;
        }
        
        // Get and sanitise the backend options
        $config = Zend_Registry::get('appconfig');
        $backend = $config->cache->backend->type;
        $backendoptions = $config->cache->backend->options;
        if (is_null($backendoptions)) {
            $backendoptions = array();
        } else {
            $backendoptions = $backendoptions->toArray();
        }
        
        return Zend_Cache::factory($frontend, $backend, $frontendoptions, $backendoptions);
    }

    /**
     * Flush website cache using a cURL request
     **/
    static function flushWeb($debug=false){

    	# only want to access via admin
    	if(Zend_Registry::get('authenticated-user') instanceof User_Model_User){
			
			$cfg = Zend_Registry::get('app-config');
			
			$url = (!empty($cfg->cacheFlushDomain) ? $cfg->cacheFlushDomain : $cfg->media->baseDomain) . 'default/system/cache';
			$ch = curl_init($url);
			
			if($debug)
				echo "URL: $url" . PHP_EOL;

			if(APPLICATION_ENV == 'staging' || (APPLICATION_ENV == 'production' && preg_match('/^[a-z]+:\/\/live[0-9]*\./i',$url))){
				curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
				curl_setopt($ch, CURLOPT_USERPWD, 'prelive:n0tr3adyy3t');
				
				if($debug)
					echo "Using HTTPAUTH" . PHP_EOL;

			}

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, array(
				'accessToken' => Mcmr_Acl::accessToken(),
				'cli' => 1,
				'clear[0]' => 'database',
				'clear[1]' => 'fullpage',
			));
		
			$out = curl_exec($ch);

			if($debug)
				echo "Output: $out" . PHP_EOL;
		
			if(substr($out,0,5) !== 'info:'){
	    		throw new Exception("Failed to flush website cache");
			}
			
    	}
    	else
    		throw new Exception("This function requires an authenticated user");
    	
    }
}

