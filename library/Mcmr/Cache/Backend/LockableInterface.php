<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: LockableInterface.php 1034 2010-07-15 14:58:48Z leigh $
 */

/**
 * An interface that adds locking mechanisms to cache entries.
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Backend
 */
interface Mcmr_Cache_Backend_LockableInterface
{
    /**
     * Obtain a lock on the provided cache ID. Return true on success, false
     * on failure
     *
     * @return boolean
     */
    public function lock($id);

    /**
     * Clear the lock on the provided cache ID. Return true on success, false
     * on failure
     *
     * @return boolean
     */
    public function unlock($id);
}
