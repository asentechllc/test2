<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Tagged.php 1386 2010-09-14 13:20:28Z leigh $
 */

/**
 * A class that adds support for tags. This class allows the cleaning of tagged cache items.
 * Only supports 'matchingAnyTag'
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Backend
 */
class Mcmr_Cache_Backend_Tagged extends Mcmr_Cache_Backend_Bridge
{

    /**
     * Clean some cache records
     *
     * Available modes are :
     * 'all' (default)  => remove all cache entries ($tags is not used)
     * 'old'            => unsupported
     * 'matchingTag'    => unsupported
     * 'notMatchingTag' => unsupported
     *
     * @param  string $mode Clean mode
     * @param  array  $tags Array of tags
     * @throws Zend_Cache_Exception
     * @return boolean True if no problem
     */
    public function clean($mode = Zend_Cache::CLEANING_MODE_ALL, $tags = array())
    {
        switch ($mode) {
            case Zend_Cache::CLEANING_MODE_MATCHING_ANY_TAG:
                // This will expire any record that uses any of the following tags.
                foreach ($tags as $tag) {
                    $this->_expireTag($tag);
                }
                break;

            default:
                parent::clean($mode, $tags);
        }
    }

    /**
     * Remove a cache record
     *
     * @param  string $id Cache id
     * @return boolean True if no problem
     */
    public function remove($id)
    {
        $id = $this->_getTrueId($id);

        parent::remove($id);
    }

    /**
     * Test if a cache is available for the given id and (if yes) return it (false else)
     *
     * @param  string  $id                     Cache id
     * @param  boolean $doNotTestCacheValidity If set to true, the cache validity won't be tested
     * @return string|false cached datas
     */
    public function load($id, $doNotTestCacheValidity = false)
    {
        $id = $this->_getTrueId($id);

        return parent::load($id, $doNotTestCacheValidity);
    }

    /**
     * Save some string datas into a cache record
     *
     * Note : $data is always "string" (serialization is done by the
     * core not by the backend)
     *
     * @param  string $data             Datas to cache
     * @param  string $id               Cache id
     * @param  array  $tags             Array of strings, the cache record will
     * be tagged by each string entry
     * @param  int    $specificLifetime If != false, set a specific lifetime for
     *  this cache record (null => infinite lifetime)
     * @return boolean True if no problem
     */
    public function save($data, $id, $tags = array(), $specificLifetime = false)
    {
        parent::save(serialize($tags), $id);

        // Create tag counts for any tags that don't exist
        foreach ($tags as $tag) {
            if (!parent::test($this->_prefixId('tag_' . $tag))) {
                $this->_incrementTagCount($tag);
            }
        }

        $id = $this->_getTrueId($id);

        return parent::save($data, $id, array(), $specificLifetime);
    }

    /**
     * Test if a cache is available or not (for the given id)
     *
     * @param  string $id Cache id
     * @return mixed|false (a cache is not available) or "last modified" timestamp (int) of the available cache record
     */
    public function test($id)
    {
        $id = $this->_getTrueId($id);

        return parent::test($id);
    }

    /**
     * Prefix the cache ID with the site ID
     *
     * @param string $id
     * @return string
     */
    private function _prefixId($id)
    {
        $prefix = Zend_Registry::get('app-config')->siteid;

        if (null !== $prefix) {
            $id = $prefix . '_' . $id;
        }

        return $id;
    }

    /**
     * Get the cache id using the data tags.
     *
     * @param $id Cache id
     * @return string Id that contains the real data
     */
    private function _getTrueId($id)
    {
        $taglist = unserialize(parent::load($id));

        $idtail = '';
        if (is_array($taglist)) {
            foreach ($taglist as $tag) {
                $tagcount = $this->_getTagCount($tag);

                // If there is no cached tag count use 0. 0 is never in the cache and will cause a miss
                if (!$tagcount) {
                    $tagcount = 0;
                }
                $idtail .= $tagcount . '-';
            }
        } else {
            $idtail = '-';
        }

        $id .= '_' . md5($idtail);

        // Prepend the site id if it exists. Prevent cross-site clashes
        return $this->_prefixId(md5($id));
    }

    /**
     * Expire all cache entries using this tag
     *
     * @param string $tag
     */
    private function _expireTag($tag)
    {
        return $this->_incrementTagCount($tag);
    }

    /**
     * Get the tag count value
     *
     * @param string $tag
     * @return int|false
     */
    private function _getTagCount($tag)
    {
        return parent::load($this->_prefixId('tag_' . $tag));
    }

    /**
     * Increment a tag count
     *
     * @param string $tag
     */
    private function _incrementTagCount($tag)
    {
        $count = time().microtime();

        return parent::save($count, $this->_prefixId('tag_' . $tag));
    }

}
