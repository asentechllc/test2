<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bridge.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * A class used to bridge between Mcmr cache backends and Zend cache backends
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Backend
 */
class Mcmr_Cache_Backend_Bridge extends Zend_Cache_Backend
    implements Mcmr_Cache_Backend_ExtendedInterface
{
    /**
     * Instance of the Cache backend
     *
     * @var Zend_Cache_Backend
     */
    protected $_backend = null;

    /**
     * Set the Zend Cache Backend to use
     *
     * @param Zend_Cache_Backend $backend
     * @return Mcmr_Cache_Backend_Bridge
     */
    public function setBackend(Zend_Cache_Backend $backend)
    {
        $this->_backend = $backend;

        return $this;
    }

    /**
     * Return the Zend Cache Backend
     *
     * @return Zend_Cache_Backend
     */
    public function getBackend()
    {
        return $this->_backend;
    }

    /**
     * Clean some cache records
     *
     * Available modes are :
     * 'all' (default)  => remove all cache entries ($tags is not used)
     * 'old'            => unsupported
     * 'matchingTag'    => unsupported
     * 'notMatchingTag' => unsupported
     *
     * @param  string $mode Clean mode
     * @param  array  $tags Array of tags
     * @throws Zend_Cache_Exception
     * @return boolean True if no problem
     */
    public function clean($mode = Zend_Cache::CLEANING_MODE_ALL, $tags = array())
    {
        return $this->_backend->clean($mode, $tags);
    }

    /**
     * Remove a cache record
     *
     * @param  string $id Cache id
     * @return boolean True if no problem
     */
    public function remove($id)
    {
        return $this->_backend->remove($id);
    }

    /**
     * Test if a cache is available for the given id and (if yes) return it (false else)
     *
     * @param  string  $id                     Cache id
     * @param  boolean $doNotTestCacheValidity If set to true, the cache validity won't be tested
     * @return string|false cached datas
     */
    public function load($id, $doNotTestCacheValidity = false)
    {
        $cacheItem = $this->_backend->load($id, $doNotTestCacheValidity);

        if (is_object($cacheItem) && $cacheItem instanceof Mcmr_Cache_Item) {
            $data = $cacheItem->getData();
            
            // Force a data refresh by randomly returning false
            $expireTime = $cacheItem->getExpireTime();
            if (0 < $expireTime) {
                // As we get closer to the expiry time increase the odds of a data refresh
                $max = intval(10000 * ($cacheItem->getRemainingLifetime() / $cacheItem->getLifetime()));
                if (0 === rand(0, $max)) {
                    return false;
                }
            }
        } else {
            $data = $cacheItem;
        }
        
        return $data;
    }

    /**
     * Save some string datas into a cache record
     *
     * Note : $data is always "string" (serialization is done by the
     * core not by the backend)
     *
     * @param  string $data             Datas to cache
     * @param  string $id               Cache id
     * @param  array  $tags             Array of strings, the cache record will be tagged by each string entry
     * @param  int    $specificLifetime If != false, set a specific lifetime for this cache 
     *                                  record (null => infinite lifetime)
     * @return boolean True if no problem
     */
    public function save($data, $id, $tags = array(), $specificLifetime = false)
    {
//        $cacheItem = new Mcmr_Cache_Item($data);
//        $cacheItem->setLifetime($this->getLifetime($specificLifetime));
        
        // The Cache_Item stuff may be causing a strange blanking error and "stuck" cache items. So disabling for now
        $cacheItem = $data;
        
        $success = $this->_backend->save($cacheItem, $id, $tags, $specificLifetime);
        
        return $success;
    }

    /**
     * Test if a cache is available or not (for the given id)
     *
     * @param  string $id cache id
     * @return mixed|false (a cache is not available) or "last modified" timestamp (int) of the available cache record
     */
    public function test($id)
    {
        return $this->_backend->test($id);
    }

    /**
     * Return an array of stored cache ids
     *
     * @return array array of stored cache ids (string)
     */
    public function getIds()
    {
        return $this->_backend->getIds();
    }

    /**
     * Return an array of stored tags
     *
     * @return array array of stored tags (string)
     */
    public function getTags()
    {
        return $this->_backend->getTags();
    }

    /**
     * Return an array of stored cache ids which match given tags
     *
     * In case of multiple tags, a logical AND is made between tags
     *
     * @param array $tags array of tags
     * @return array array of matching cache ids (string)
     */
    public function getIdsMatchingTags($tags = array())
    {
        return $this->_backend->getIdsMatchingTags($tags);
    }

    /**
     * Return an array of stored cache ids which don't match given tags
     *
     * In case of multiple tags, a logical OR is made between tags
     *
     * @param array $tags array of tags
     * @return array array of not matching cache ids (string)
     */
    public function getIdsNotMatchingTags($tags = array())
    {
        return $this->_backend->getIdsNotMatchingTags($tags);
    }

    /**
     * Return an array of stored cache ids which match any given tags
     *
     * In case of multiple tags, a logical AND is made between tags
     *
     * @param array $tags array of tags
     * @return array array of any matching cache ids (string)
     */
    public function getIdsMatchingAnyTags($tags = array())
    {
        return $this->_backend->getIdsMatchingAnyTags($tags);
    }

    /**
     * Return the filling percentage of the backend storage
     *
     * @return int integer between 0 and 100
     */
    public function getFillingPercentage()
    {
        return $this->_backend->getFillingPercentage();
    }

    /**
     * Return an array of metadatas for the given cache id
     *
     * The array must include these keys :
     * - expire : the expire timestamp
     * - tags : a string array of tags
     * - mtime : timestamp of last modification time
     *
     * @param string $id cache id
     * @return array array of metadatas (false if the cache id is not found)
     */
    public function getMetadatas($id)
    {
        return $this->_backend->getMetadatas($id);
    }

    /**
     * Give (if possible) an extra lifetime to the given cache id
     *
     * @param string $id cache id
     * @param int $extraLifetime
     * @return boolean true if ok
     */
    public function touch($id, $extraLifetime)
    {
        return $this->_backend->touch($id, $extraLifetime);
    }

    /**
     * Return an associative array of capabilities (booleans) of the backend
     *
     * The array must include these keys :
     * - automatic_cleaning (is automating cleaning necessary)
     * - tags (are tags supported)
     * - expired_read (is it possible to read expired cache records
     *                 (for doNotTestCacheValidity option for example))
     * - priority does the backend deal with priority when saving
     * - infinite_lifetime (is infinite lifetime can work with this backend)
     * - get_list (is it possible to get the list of cache ids and the complete list of tags)
     *
     * @return array associative of with capabilities
     */
    public function getCapabilities()
    {
        return $this->_backend->getCapabilities();
    }


    /**
     *
     * @return array
     */
    public function getServers()
    {
        if ($this->_backend instanceof Mcmr_Cache_Backend_ExtendedInterface) {
            return $this->_backend->getServers();
        } else {
            return array();
        }
    }

    /**
     *
     * @return int
     */
    public function getHitPercentage()
    {
        if ($this->_backend instanceof Mcmr_Cache_Backend_ExtendedInterface) {
            return $this->_backend->getHitPercentage();
        } else {
            return -1;
        }
    }

    public function setDirectives($directives)
    {
        return $this->_backend->setDirectives($directives);
    }
    
    public function getLifetime($specificlifetime) 
    {
        return $this->_backend->getLifetime($specificlifetime);
    }

}
