<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Apc.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * An implementation of the Apc backend that adds lock and unlock mechanisms
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Backend
 */
class Mcmr_Cache_Backend_Lockable_Apc extends Zend_Cache_Backend_Apc
    implements Mcmr_Cache_Backend_LockableInterface, Mcmr_Cache_Backend_ExtendedInterface
{
    /**
     * Time in seconds before the lock naturally expires
     */
    const EXPIRE_TIME = 2;

    /**
     * Obtain a lock on the provided cache ID. Return true on success, false
     * on failure
     *
     * @param string $id
     * @return boolean
     */
    public function lock($id)
    {
        if ($this->_options['lockable']) {
            return apc_add('lock_'.$id, 1, self::EXPIRE_TIME);
        } else {
            return true;
        }
    }

    /**
     * Release the lock on the provided cache ID. Return true on success, false
     * on failure
     *
     * @param string $id
     * @return boolean
     */
    public function unlock($id)
    {
        return apc_delete('lock_'.$id);
    }

    /**
     * Get the Apc server (Always localhost)
     *
     * @return array
     */
    public function getServers()
    {
        return array('localhost');
    }

    /**
     * Return the hit/miss percentage
     *
     * @return int
     */
    public function getHitPercentage()
    {
        $info = apc_cache_info();
        $hits = $info['num_hits'];
        $misses = $info['num_misses'];
        $total = $hits + $misses;

        if ($total > 0) {
            return round(($hits/$total)*100);
        } else {
            return -1;
        }
    }

}
