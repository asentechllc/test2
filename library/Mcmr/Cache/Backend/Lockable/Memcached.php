<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Memcached.php 1521 2010-10-11 13:51:21Z leigh $
 */

/**
 * An implementation of the Memcached backend that adds lock and unlock mechanisms
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Backend
 */
class Mcmr_Cache_Backend_Lockable_Memcached extends Zend_Cache_Backend_Memcached
    implements Mcmr_Cache_Backend_LockableInterface, Mcmr_Cache_Backend_ExtendedInterface
{
    /**
     * Time in seconds before the lock naturally expires
     */
    const EXPIRE_TIME = 2;

    /**
     * Obtain a lock on the provided cache ID. Return true on success, false
     * on failure
     *
     * @param string $id
     * @return boolean
     */
    public function lock($id)
    {
        if ($this->_options['lockable']) {
            return $this->_memcache->add('lock_'.$id, 1, 0, self::EXPIRE_TIME);
        } else {
            return true;
        }
    }

    /**
     * Release the lock on the provided cache ID. Return true on success, false
     * on failure
     *
     * @param string $id
     * @return boolean
     */
    public function unlock($id)
    {
        return $this->remove('lock_'.$id);
    }

    /**
     * Get the Memcache servers
     *
     * @return array
     */
    public function getServers()
    {
        $mems = $this->_memcache->getExtendedStats();
        $servers = array();
        foreach ($mems as $server=>$mem) {
            $servers[] = $server;
        }

        return $servers;
    }

    /**
     * Return the hit/miss percentage
     *
     * @return int
     */
    public function getHitPercentage()
    {
        $mems = $this->_memcache->getExtendedStats();
        $hits = 0;
        $misses = 0;
        foreach ($mems as $server=>$mem) {
            $hits += $mem['get_hits'];
            $misses += $mem['get_misses'];
        }

        $total = $hits + $misses;

        if ($total > 0) {
            return round(($hits/$total)*100);
        } else {
            return -1;
        }
    }

}
