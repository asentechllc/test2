<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Apc.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * An APC class that adds support for tags using a bridge to Mcmr_Cache_Backend_Tagged.
 * This class allows the cleaning of tagged cache items.
 * Only supports 'matchingAnyTag'
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Backend
 */
class Mcmr_Cache_Backend_Apc extends Mcmr_Cache_Backend_Tagged
{
    public function __construct(array $options = array())
    {
        //$this->setBackend(new Mcmr_Cache_Backend_Lockable_Apc($options));
        $this->setBackend(new Zend_Cache_Backend_Apc($options));
    }
}
