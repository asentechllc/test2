<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ExtendedInterface.php 1034 2010-07-15 14:58:48Z leigh $
 */

/**
 * An interface that adds some cache statistics
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Backend
 */
interface Mcmr_Cache_Backend_ExtendedInterface extends Zend_Cache_Backend_ExtendedInterface
{
    /**
     * Get an array of servers the cache uses
     *
     * @return array
     */
    public function getServers();

    /**
     * Return the cache hit/miss percentage
     *
     * @return int
     */
    public function getHitPercentage();
}
