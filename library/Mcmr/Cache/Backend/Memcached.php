<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Memcached.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * A memcached class that adds support for tags using a bridge to Mcmr_Cache_Backend_Tagged.
 * This class allows the cleaning of tagged cache items.
 * Only supports 'matchingAnyTag'
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Backend
 */
class Mcmr_Cache_Backend_Memcached extends Mcmr_Cache_Backend_Tagged
{
    public function __construct(array $options = array())
    {
        parent::__construct($options);
        
        //$this->setBackend(new Mcmr_Cache_Backend_Lockable_Memcached($options));
        $this->setBackend(new Zend_Cache_Backend_Memcached($options));
    }
}
