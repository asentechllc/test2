<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bridge.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * A class used to hold the cache data and extra data that may be needed by the system
 *
 * @category Mcmr
 * @package Mcmr_Cache
 * @subpackage Item
 */
class Mcmr_Cache_Item
{
    /**
     * The data to be cached
     *
     * @var mixed
     */
    protected $_data = null;
    
    /**
     * The cache item create timestamp
     *
     * @var int
     */
    protected $_createTime = null;
    
    /**
     * The timestamp this item is to expire
     *
     * @var int
     */
    protected $_lifetime = null;
    
    /**
     *
     * @param mixed $data 
     */
    public function __construct($data = null)
    {
        $this->_createTime = time();
        $this->_data = $data;
    }
    
    /**
     * Get the cache data
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * Set the cache data
     *
     * @param mixed $data
     * @return Mcmr_Cache_Item 
     */
    public function setData($data)
    {
        $this->_data = $data;
        
        return $this;
    }

    /**
     * Get the timestamp this item was created
     *
     * @return int
     */
    public function getCreateTime()
    {
        return $this->_createTime;
    }

    /**
     * Return the timestamp of when this item will expire. 0 if unknown
     *
     * @return int
     */
    public function getExpireTime()
    {
        if (null !== $this->_lifetime) {
            return $this->_createTime + $this->_lifetime;
        } else {
            return 0;
        }
    }
    
    /**
     * Get the time in seconds before this item will expire
     *
     * @return int
     */
    public function getRemainingLifetime()
    {
        $remaining = $this->getExpireTime() - time();
        if (0 > $remaining) {
            $remaining = 0;
        }
        
        return $remaining;
    }

    public function getLifetime()
    {
        return $this->_lifetime;
    }
    
    /**
     * Set the time this item will expire
     *
     * @param int $lifetime
     * @return Mcmr_Cache_Item 
     */
    public function setLifetime($lifetime)
    {
        $this->_lifetime = $lifetime;
        
        return $this;
    }

}
