<?php

/**
 * override api url in zend serivce to force https access
 *
 * @category   Mcmr
 * @package    Mcmr_Service_ShortUrl
 * @copyright  Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license    http://framework.zend.com/license/new-bsd     New BSD License
 */
class  Mcmr_Service_ShortUrl_BitLy extends Zend_Service_ShortUrl_BitLy
{
    protected $_apiUri = 'https://api-ssl.bitly.com/';
}
