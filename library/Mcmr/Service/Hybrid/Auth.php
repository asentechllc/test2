<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Thin extension to Hybrid_Auth to provide some helper methods
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @subpackage Adestra
 */
class Mcmr_Service_Hybrid_Auth extends Hybrid_Auth
{
	protected static $instance;

    public static function getConfig()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
        $config = Zend_Registry::get('user-config')->hybridauth->{$site}->toArray();
        $config['base_url'] = $view->getHelper('ServerUrl')->serverUrl(
            $view->getHelper('Url')->url(array('module'=>'user', 'controller'=>'index', 'action'=>'endpoint'),'default',true));
        return $config;

    }


    public static function getInstance() 
    {
    	if (null===self::$instance) {
	        $config = self::getConfig();
	        self::$instance = new Mcmr_Service_Hybrid_Auth( $config );
    	}
    	return self::$instance;
    }

}