<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Service class for interfacing with Adestra mail system
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @subpackage Adestra
 */
class Mcmr_Service_Adestra
{
    const ADESTRA_XMLRPC_SERVER = 'https://new.adestra.com/api/xmlrpc';
    
    protected static $_defaultOptions = array();

    /**
     *
     * @var Zend_XmlRpc_Client 
     */
    protected $_xmlRpcClient = null;
    
    protected $_username = null;
    protected $_password = null;
    protected $_account = null;
    
    public function __construct($options=null)
    {
        if (null === $options) {
            $options = self::$_defaultOptions;
        }
        
        if ($options instanceof Zend_Config) {
            $this->setConfig($options);
        } elseif (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public static function setDefaultOptions($options)
    {
        self::$_defaultOptions = $options;
    }
    
    public static function getInstance()
    {
        return new self();
    }
    
    /**
     * Set the payment options using an array
     *
     * @param array $options
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $normalized = ucfirst($key);
            $method = 'set' . $normalized;
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * Set the payment options using a Zend_Config object
     *
     * @param Zend_Config $config
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());

        return $this;
    }
    
    /**
     *
     * @return string 
     */
    public function getAccount()
    {
        return $this->_account;
    }

    /**
     *
     * @param string $_account
     * @return Mcmr_Service_Adestra 
     */
    public function setAccount($_account)
    {
        $this->_account = $_account;
        
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     *
     * @param string $username
     * @return Mcmr_Service_Adestra 
     */
    public function setUsername($username)
    {
        $this->_username = $username;
        
        return $this;
    }

    /**
     *
     * @param string $password
     * @return Mcmr_Service_Adestra 
     */
    public function setPassword($password)
    {
        $this->_password = $password;
        
        return $this;
    }
    
    /**
     *
     * @return Zend_XmlRpc_Client 
     */
    public function getXmlRpcClient()
    {
        if (null === $this->_xmlRpcClient) {
            if (null === $this->_username || null === $this->_password || null === $this->_account) {
                throw new Mcmr_Service_Adestra_Exception_Auth("Account, Username and Password must be set");
            }
            $this->_xmlRpcClient = new Zend_XmlRpc_Client(self::ADESTRA_XMLRPC_SERVER);
            $httpClient = $this->_xmlRpcClient->getHttpClient();
            $httpClient->setAuth($this->_account . '.' . $this->_username, $this->_password);
            $this->_xmlRpcClient->setHttpClient($httpClient);
        }
        
        return $this->_xmlRpcClient;
    }
    
    /**
     *
     * @param string $namespace
     * @param int $id
     * @return Mcmr_Service_Adestra_AdestraInterface 
     */
    public function get($namespace, $id, $raw = false)
    {
        $method = $namespace . '.get';
        
        $params = array('id'=>intval($id));
        $data = $this->_adestraCall($method, array($params));
        
        if ($raw) {
            return $data;
        } else {
            return $this->_buildObject($namespace, $data);
        }
    }
    
    /**
     *
     * @param string $namespace
     * @param array $params
     * @param bool $raw
     * @return array 
     */
    public function search($namespace, $params, $raw = false)
    {
        $method = $namespace . '.search';
        $data = $this->_adestraCall($method, array('search_args'=>$params));
        
        if ($raw) {
            return $data;
        } else {
            return $this->_buildObjectCollection($namespace, $data);
        }
    }
    
    /**
     *
     * @param string $namespace 
     */
    public function create(Mcmr_Service_Adestra_AdestraInterface $object)
    {
        $namespace = array_pop(explode('_', get_class($object)));
        $namespace{0} = strtolower($namespace{0}); // Lowercase first letter
        $values = $object->getValues();
        unset($values['id']);
        $params = array(
            'create_args' => $values,
        );
        $data = $this->_adestraCall($namespace.'.create', $params);
        $object->setValues($data);
        
        return $object;
    }
    
    /**
     *
     * @param string $object 
     */
    public function update(Mcmr_Service_Adestra_AdestraInterface $object)
    {
        $namespace = array_pop(explode('_', get_class($object)));
        $namespace{0} = strtolower($namespace{0}); // Lowercase first letter
        $values = $object->getValues();
        unset($values['id']);
        unset($values['workspace_id']);
        $params = array(
            'id' => $object->getId(),
            'update_args' => $values,
        );
        $data = $this->_adestraCall($namespace.'.update', $params);
        
        if ('1' != $data) {
            throw new Mcmr_Service_Adestra_Exception("$namespace update failed");
        }
        
        return $object;
    }
    
    /**
     *
     * @param string $method
     * @param array $params
     * @return array 
     */
    protected function _adestraCall($method, array $params=array())
    {
        $client = $this->getXmlRpcClient();
        $data = $client->call($method, $params);
        
        return $data;
    }
    
    /**
     *
     * @param string $name
     * @param array $data
     * @return Mcmr_Service_Adestra_AdestraInterface 
     */
    protected function _buildObject($name, $data)
    {
        $class = 'Mcmr_Service_Adestra_'.ucfirst($name);
        if (class_exists($class)) {
            $object = new $class();
            $object->populate($data);
        } else {
            throw new Mcmr_Service_Adestra_Exception("Unknown Adestra object '{$name}'");
        }
        
        return $object;
    }
    
    /**
     *
     * @param string $name
     * @param array $collectionData
     * @return array 
     */
    protected function _buildObjectCollection($name, $collectionData)
    {
        $collection = array();
        foreach ($collectionData as $data) {
            $collection[] = $this->_buildObject($name, $data);
        }
        
        return $collection;
    }
}
