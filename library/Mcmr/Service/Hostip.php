<?php

/**
 * Very simple service to access host ip
 */
class Mcmr_Service_Hostip
{
    protected $_url=null;
    protected $_cacheLifetime=null;
    protected $_overrides=array();
    private $_client = null;

    // Construct the class with the relevant credentials
    public function __construct($options = null)
    {
        if ($options instanceof Zend_Config) {
            $this->setConfig($options);
        } elseif (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Set the service options using an array
     *
     * @param array $options
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $normalized = ucfirst($key);
            $method = 'set' . $normalized;
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }

    /**
     * Set the service options using a Zend_Config object
     *
     * @param Zend_Config $config
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());

        return $this;
    }

    public function setUrl($url)
    {
        $this->_url = $url;
        return $this;
    }

    public function getUrl()
    {  
        return $this->_url;
    }

    public function setCacheLifetime($cacheLifetime)
    {
        $this->_cacheLifetime = $cacheLifetime;
        return $this;
    }

    public function getCacheLifetime()
    {  
        return $this->_cacheLifetime;
    }

    public function setOverrides($overrides)
    {
        $processedOverrides = array();
        foreach($overrides as $override) {
            $processedOverrides[$override['ip']]=$override['info'];
        }
        $this->_overrides = $processedOverrides;
        return $this;
    }

    public function getOverrides()
    {  
        return $this->_overrides;
    }

    public function getAddrInfo($ip) 
    {
        $overrides=$this->getOverrides();
        if (isset($overrides[$ip])) {
            return $overrides[$ip];
        } 
        $cacheid = 'Hostip_addrInfo_'.md5($ip);
        $tags =  array('hostip_addrinfo', 'hostip');
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        if (false == ($info = $cache->load($cacheid))) {
            $info = $this->_getInfo($ip);
            $cache->save($info, $cacheid, $tags, $this->getCacheLifetime());
        }
        return $info;
    } 


    protected function _getInfo($ip)
    {
        $client=$this->_getClient();
        $client->setParameterGet('ip', $ip);
        $response = $client->request();
        if ($response->isSuccessful()) {
            $country = trim($response->getBody());
            if ('XX'==$country) {
                $country = null;
            }
            return array('country'=>$country);
        }
        return null;
    }


    protected function _getClient()
    {
        if (null === $this->_client) {
            $this->_client = new Zend_Http_Client();
        }
        
        $this->_client->setMethod(Zend_Http_Client::GET);
        $this->_client->setUri($this->getUrl());
        $this->_client->resetParameters();
        
        return $this->_client;
    }
   
}