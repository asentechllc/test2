<?php
/**
* Exception thrown by PAINT when your code attempts to access an action
* for which it has insufficient privileges
*/
class Mcmr_Service_Pure360_SecurityException extends Zend_Exception
{
}
