<?php
/**
* Exception thrown by PAINT when the PureResponse application terminates
* unexpectedly.
*/
class Mcmr_Service_Pure360_SystemException extends Zend_Exception
{
}
