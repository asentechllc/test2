<?php

/**
 *
 * Utility class for creating and re-using a session within PAINT.  Utility
 * methods are included to implement the standard actions e.g. creating and
 * email or uploading a list.
 */
class Mcmr_Service_Pure360_PaintSession
{
    /** WSDL URL * */
    protected $_wsdlUrl = "http://paint.pure360.com/paint.pure360.com/ctrlPaint.wsdl";
    /** Id to link requests together between logging in and out * */
    protected $_contextId;
    protected $_username = null;
    protected $_password = null;
    /** Hashtable containing the data for this context * */
    protected $_contextData;

    // Construct the class with the relevant credentials
    public function __construct($options = null)
    {
        if ($options instanceof Zend_Config) {
            $this->setConfig($options);
        } elseif (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Set the payment options using an array
     *
     * @param array $options
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $normalized = ucfirst($key);
            $method = 'set' . $normalized;
            if (method_exists($this, $method)) {
                $this->$method($value);
            } else {
                $this->setAttrib($key, $value);
            }
        }

        return $this;
    }

    /**
     * Set the payment options using a Zend_Config object
     *
     * @param Zend_Config $config
     * @return Mcmr_Payment_AdapterAbstract
     */
    public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());

        return $this;
    }

    public function setUsername($username)
    {
        $this->_username = $username;

        return $this;
    }

    public function setPassword($password)
    {
        $this->_password = $password;

        return $this;
    }

    /**
     * Log into the Pure system and obtain a context id.
     * This is automatically called from the class constructor 
     * and so is probably only required if manually logging
     * out and then back in again. 
     */
    public function login()
    {
        $entityInput = null;
        $resultOutput = null;

        // Sanity check that the user name and password have been set correctly
        if (null == $this->_username || null === $this->_password) {
            throw new Mcmr_Service_Pure360_SystemException(
                "You have not set the user name and password " .
                "for your account.  Please see the PaintSession " .
                "class in the com.pure360.paint namespace and " .
                "update the values set in the constructor."
            );
        }

        // Create argument data into a hashtable
        $entityInput = array();
        $entityInput["userName"] = $this->_username;
        $entityInput["password"] = $this->_password;

        // Login 
        $resultOutput = $this->sendRequest("bus_facade_context", "login", $entityInput, null);

        // Store the context id on the class
        $this->_contextData = $resultOutput;
        $this->_contextId = $resultOutput["bus_entity_context"]["beanId"];

        return $this;
    }

    /**
     * Log out of the current context.  This will remove the context id and you won't be
     * able to issue any other requests after this other than login.
     */
    public function logout()
    {
        // No data needs to be sent to this request
        $this->sendRequest("bus_facade_context", "logout", null, null);

        $this->_contextId = null;

        return $this;
    }

    /**
     * Search for an entity by a set of search parameters and return the key fields for
     * the entity or entities found.
     */
    public function search($facadeBean, $searchParameters)
    {
        $resultOutput = null;
        $searchBean = str_replace("bus_facade", "bus_search", $facadeBean);

        // First search to see if an email already exists with this name (assumes no SMS on the account)
        $resultOutput = $this->sendRequest($facadeBean, "search", $searchParameters, null);

        // Access the data using the search bean name NOT the facade bean name
        $resultOutput = $resultOutput[$searchBean];
        $resultOutput = $resultOutput["idData"];

        return $resultOutput;
    }

    /**
     * Search for an entity using search parameters but ensure that only an exact match
     * for all parameters is returned.
     */
    public function searchExactMatch($facadeBean, $searchParameters)
    {
        $searchResults = null;
        $exactMatchData = null;
        $entityBean = str_replace("bus_facade", "bus_entity", $facadeBean);

        // Perform the general search to obtain a list of ids
        $searchResults = $this->search($facadeBean, $searchParameters);

        // Loop through the ids and call the load method until we find an exact match
        foreach ($searchResults as $loadInput) {
            $beanInst = $this->sendRequest($facadeBean, "load", $loadInput, null);

            if (!empty($beanInst)) {
                $exactMatch = true;

                foreach ($searchParameters as $paramName => $paramValue) {
                    $beanData = $beanInst[$entityBean];

                    if (!isset($beanData[$paramName]) || $beanData[$paramName] !== $paramValue) {
                        $exactMatch = false;
                    }
                }

                if ($exactMatch) {
                    $exactMatchData = $beanData;
                }
            }
        }

        if (empty($exactMatchData)) {
            throw new Mcmr_Service_Pure360_SearchException(
                array("searchExactMatch" => "No exact match found for $facadeBean")
            );
        }

        return $exactMatchData;
    }

    /**
     * Send a request to PAINT passing the required parameters
     * and returning a hashtable of hashtables as the result
     * if successful, or throw an exception if unsuccessful.
     */
    public function sendRequest($className, $processName, $entityInput,
            $processInput)
    {
        $client = null;
        $resultOutput = null;

        // Check that the context is valid
        if ($processName != "login" && $this->_contextId == null) {
            throw new Mcmr_Service_Pure360_SystemException("No context available for this request");
        }


        $client = new SoapClient($this->_wsdlUrl, array("trace" => "0"));
        $resultOutput = $client->handleRequest(
            $this->_contextId,
            $className,
            $processName,
            $entityInput,
            $processInput
        );

        switch ($resultOutput["result"]) {
            case "success":
                if (!empty($resultOutput["resultData"])) {
                    $resultOutput = $resultOutput["resultData"];
                } else {
                    // Update requests return no data back
                    $resultOutput = array();
                }
                break;

            case "bean_exception_validation":
                throw new Mcmr_Service_Pure360_ValidationException($resultOutput["resultData"]);

            case "bean_exception_security":
                throw new Mcmr_Service_Pure360_SecurityException($resultOutput["resultData"]);

            case "bean_exception_system":
                throw new Mcmr_Service_Pure360_SystemException($resultOutput["resultData"]);

            default:
                throw new Zend_Exception("Unhandled exception thrown from PAINT");
        }

        return $resultOutput;
    }

    /**
     * Return the data from the context.  This data is loaded whe
     * logging in and contains details about the login, profile and group
     */
    public function getContextData()
    {
        return $this->_contextData;
    }

    /**
     * Convert a result hashtable into a string for debug purposes
     */
    public function convertResultToDebugString($result)
    {
        $resultStr = print_r($result, true);

//        foreach ($result as $tmpKey => $tmpValue) {
//            if ($tmpValue != null && is_array($tmpValue)) {
//                $resultStr = $resultStr . "<BR/>---><BR/>[Nested Hashtable Key] [" . $tmpKey . "]"
//                    . $this->convertResultToDebugString($tmpValue);
//            } else {
//                $resultStr = $resultStr . "<BR/>Key [" . $tmpKey . "] Value [" . $tmpValue . "]";
//            }
//        }
//
//        $resultStr = $resultStr . "<BR/><---<BR/>";

        return $resultStr;
    }

}
