<?php

/**
 * Exception thrown when PAINT is unable to find an object (list/message/etc).
 * Errors are stored in a hashtable keyed by a unique reference for the field
 * or particular validation error.
 */
class Mcmr_Service_Pure360_SearchException extends Zend_Exception
{
    /** Array of errors keyed on the error field/name * */
    protected $_errors;

    /**
     * Construct the exception with a hashtable of errors
     */
    public function __construct($errors)
    {
        $this->_errors = $errors;

        parent::__construct("Validation error");
    }

    /**
     * Return the hash table or errors 
     */
    public function getErrors()
    {
        return $this->_errors;
    }

}
