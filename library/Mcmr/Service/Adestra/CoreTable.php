<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of CoreTable
 *
 * @category Mcmr
 */
class Mcmr_Service_Adestra_CoreTable extends Mcmr_Service_Adestra_AdestraAbstract
{
    
    /**
     *
     * @var string 
     */
    protected $_name = null;
    
    /**
     *
     * @var string 
     */
    protected $_description = null;
    
    /**
     *
     * @var int 
     */
    protected $_ownerUserId = null;
    
    /**
     *
     * @var int 
     */
    protected $_workspaceId = null;
    
    /**
     *
     * @var type 
     */
    protected $_tableColumns = null;
    
    /**
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @param string $name
     * @return Mcmr_Service_Adestra_CoreTable 
     */
    public function setName($name)
    {
        $this->_name = $name;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     *
     * @param string $description
     * @return Mcmr_Service_Adestra_CoreTable 
     */
    public function setDescription($description)
    {
        $this->_description = $description;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getOwnerUserId()
    {
        return $this->_ownerUserId;
    }

    /**
     *
     * @param int $ownerUserId
     * @return Mcmr_Service_Adestra_CoreTable 
     */
    public function setOwnerUserId($ownerUserId)
    {
        $this->_ownerUserId = intval($ownerUserId);
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getWorkspaceId()
    {
        return $this->_workspaceId;
    }

    /**
     *
     * @param int $workspaceId
     * @return Mcmr_Service_Adestra_CoreTable 
     */
    public function setWorkspaceId($workspaceId)
    {
        $this->_workspaceId = intval($workspaceId);
        
        return $this;
    }

    /**
     *
     * @return type 
     */
    public function getTableColumns()
    {
        return $this->_tableColumns;
    }

    /**
     *
     * @param type $tableColumns
     * @return Mcmr_Service_Adestra_CoreTable 
     */
    public function setTableColumns($tableColumns)
    {
        $this->_tableColumns = $tableColumns;
        
        return $this;
    }
}
