<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of AdestraAbstract
 *
 * @category Mcmr
 */
abstract class Mcmr_Service_Adestra_AdestraAbstract implements Mcmr_Service_Adestra_AdestraInterface
{
    /**
     *
     * @var Mcmr_Service_Adestra 
     */
    protected $_adestra = null;
    
    protected $_paramsMap = array();
    
    /**
     *
     * @var int 
     */
    protected $_id = null;

    public function __construct($data = null, Mcmr_Service_Adestra $adestra = null)
    {
        if (null === $adestra) {
            $adestra = Mcmr_Service_Adestra::getInstance();
        }
        $this->_adestra = $adestra;
        
        if (null !== $data) {
            if (is_object($data) && $data instanceof Zend_Config) {
                $data = $data->toArray();
            }
            
            $this->populate($data);
            
            $object = array_pop(explode('_', get_class($this)));
            $object{0} = strtolower($object{0}); // Lowercase first letter
            
            if (isset($data['id'])) {
                $this->populate($adestra->get($object, $data['id'], true));
            } else {
                $collection = $adestra->search($object, $data, true);
                if (is_array($collection) && isset($collection[0])) {
                    $this->populate($collection[0]);
                }
            }
        } 
    }
    
    /**
     * Set a property value.
     *
     * @param string $name
     * @param mixed $value
     * @throws Mcmr_Model_Exception
     */
    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || ! method_exists($this, $method)) {
            throw new Mcmr_Model_Exception('Invalid property');
        }
        $this->$method($value);
    }

    /**
     * Return a property value.
     *
     * @param string $name
     * @throws Mcmr_Model_Exception
     */
    public function __get($name)
    {
        $method = 'get' . ucfirst($name);
        if (!method_exists($this, $method)) {
            throw new Mcmr_Model_Exception('Invalid property '.$name);
        }
        return $this->$method();
    }
    
    /**
     *
     * @param array $data 
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function populate(array $data)
    {
        $this->setValues($data);
        
        return $this;
    }
    
    /**
     *
     * @param array $options
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setValues(array $options)
    {
        $filter = new Zend_Filter_Word_UnderscoreToCamelCase();
        
        foreach ($options as $key => $value) {
            $normalized = $filter->filter($key);
            $method = 'set' . $normalized;
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }

        return $this;
    }
    
    /**
     * Return an array of all the model properties
     *
     * @return array
     */
    public function getValues($includeattributes = false)
    {
        $return = array();
        $filter = new Zend_Filter_Word_CamelCaseToUnderscore();
        
        $vars = get_object_vars($this);
        foreach ($vars as $name => $value) {
            $name = str_replace('_', '', $name);
            if ('adestra' !== $name && 'paramsMap' !== $name) {
                $value = $this->__get($name);
                $name = strtolower($filter->filter($name));
                $return[$name] = $value;
            }
        }

        return $return;
    }    
    
    /**
     * 
     */
    public function save()
    {
        if (null === $this->_id) {
            $this->_adestra->create($this);
        } else {
            $this->_adestra->update($this);
        }
    }
    
    /**
     *
     * @return int 
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     *
     * @param int $id
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setId($id)
    {
        $this->_id = intval($id);
        
        return $this;
    }
    
    protected function _mapXmlRpcParams($method, $params)
    {
        $mappedParams = array();
        foreach ($params as $key=>$value) {
        	if(isset($this->_paramsMap[$method][$key]))
	            $mappedParams[$this->_paramsMap[$method][$key]] = $value;
        }
        
        ksort($mappedParams);
        
        return $mappedParams;
    }

    public function getAdestraService()
    {
        return $this->_adestra;
    }
}
