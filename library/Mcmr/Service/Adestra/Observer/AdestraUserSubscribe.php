<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * An observer for subscribing and unsubscribing a user to Adestra lists
 * This code has gotten quite complex. The principle is this:
 * We need to allow for people to change their email address and also only subscribe them to the lists when they confirm their email address.
 * When a used is created, we do nothing as they have not confirmed email yet.
 * When they confirm email, we subscribe them.
 * If they update their email, the system sets the new email as unconfirmed, we need to unsibscribe old email from everything, but not subscribe the new on just yet.
 * Only when they confirm, we subscribe.
 * We look at the change of email confirmation status, rather than email address change, and rely on the behaviour that when they change the emaul address, 
 * The confirmed flag goes back to unconfirmed.
 *
 * @category Mcmr
 */
class Mcmr_Service_Adestra_Observer_AdestraUserSubscribe extends Mcmr_Model_ObserverAbstract
    implements Mcmr_Model_Observer_QueueableInterface
{

    protected $_oldEmail = null;
    protected $_oldEmailconfirmed = null;
    protected $_config = null;
    protected $_lists = array();
    
    public function init()
    {
        $config = Zend_Registry::get('user-config');
        if (isset($config->adestra)) {
            $this->_config = $config->adestra;
        }
    }
    
    /**
     *
     * @param type $user 
     */
    public function preUpdate($user)
    {
        if (null===$this->_config) {
            return;
        }
        foreach ($this->_config->lists as $listName=>$list) {
            $this->_lists[$listName] = $this->_subscriptionAction($user, $list->optIn);
        }
        $this->_oldEmail = $user->getEmail();
        $this->_oldEmailconfirmed = $user->getEmailconfirmed();
    }
    
    /**
     *
     * @param type $user 
     */
    public function update($user)
    {
        if (null===$this->_config) {
            return;
        }
        $newEmail = $user->getEmail();
        $newEmailconfirmed = $user->getEmailconfirmed();
        //if the email has changed create adestra contact for the new email
        if ($this->_oldEmail!=$user->getEmail()) {
            $this->_createContact($user);
        }

        //we need to ensure we only subscribe users to email list once they confiremd their email address
        //in order to do that we need to consider their 'emailConfirmed' status before we do anything
        //we need to look at all possible combinations of this field before and after the update
        if (!$this->_oldEmailconfirmed) {
            if (!$newEmailconfirmed) {
                //if the user didn't confrim their email before and this update is not confirming the email either
                //we do nothing
                return;
            } else {
                //if this update is confirming the email, we need to subscribe CURRENT EMAIL (which should not change) them to everything they selected
                //fake all current statuses to be 'unsubscribe', this way the new email will be subscribed to whatever was selcted
                //this way they will be subscribed no matter what was set
                foreach ($this->_lists as $name => $value) {
                    $this->_lists[$name] = 'unsubscribe';
                }
                foreach ($this->_config->lists as $listName=>$list) {
                    $action = $this->_subscriptionAction($user, $list->optIn);
                    if ($action != $this->_lists[$listName]) {
                        $listSearch = $list->toArray();
                        $unsubId = null;
                        if (isset($listSearch['unsubId'])) {
                            $unsubId = $listSearch['unsubId'];
                            unset($listSearch['unsubId']);
                        }
                        unset($listSearch['optIn']);
                        
                        $this->_updateUser($listSearch, $user, $action, $unsubId);
                    }
                }
            }
        } else {
            if (!$newEmailconfirmed) {
                //if the email was confirmed before but is not confirmed now, that means they updated it and have not confirmed the new one yet. 
                //we need to unsubscribe the OLD EMAIL from everything
                foreach ($this->_config->lists as $listName=>$list) {
                    if ('subscribe'==$this->_lists[$listName]) {
                        $listSearch = $list->toArray();
                        $this->_updateUser($listSearch, $this->_oldEmail, 'unsubscribe');
                    }
                }
            } else {
                //this update didn't change email address or status, but could have still changed the subscription tick boxes, so we proceed with looking at that
                foreach ($this->_config->lists as $listName=>$list) {
                    $action = $this->_subscriptionAction($user, $list->optIn);
                    if ($action != $this->_lists[$listName]) {
                        $listSearch = $list->toArray();
                        $unsubId = null;
                        if (isset($listSearch['unsubId'])) {
                            $unsubId = $listSearch['unsubId'];
                            unset($listSearch['unsubId']);
                        }
                        unset($listSearch['optIn']);
                        
                        $this->_updateUser($listSearch, $user, $action, $unsubId);
                    }
                }
            }
        }
    }
    
    /**
     *
     * @param type $user 
     */
    public function insert($user)
    {
        if (null===$this->_config) {
            return;
        }
        $this->_createContact($user);
    }
    
    /**
     *
     * @param type $user
     * @param type $action 
     */
    protected function _updateUser($listSearch, $user, $action, $unsubId = null)
    {
        $email = is_string($user)?$user:$user->getEmail();
        $list = new Mcmr_Service_Adestra_List($listSearch);
        $contact = new Mcmr_Service_Adestra_Contact($this->_config->coreTableId, array('email'=>$email));
        
        if ('subscribe' === $action) {
            $contact->subscribe($list, $unsubId);
        } elseif ('unsubscribe' === $action) {
            $contact->unsubscribe($list, $unsubId);
        }
    }
    
    /**
     *
     * @param type $user 
     */
    protected function _createContact($user)
    {
        $contact = new Mcmr_Service_Adestra_Contact($this->_config->coreTableId);
        $contact->setEmail($user->getEmail())
            ->save();
    }
    
    /**
     * Determine the mailing list action to be performed for this user and opt in
     *
     * @param User_Model_User $user
     * @param string $optIn
     * @return string 
     */
    protected function _subscriptionAction($user, $optIn)
    {
        $observerConfig = $this->getConfig();
        $roles = array();
        if (isset($observerConfig->roles) && isset($observerConfig->roles->$optIn)) {
            $roles = $observerConfig->roles->$optIn->toArray();
        }
        
        if ($user->getAttribute($optIn) && (in_array($user->getRole(), $roles) || empty($roles))) {
            $action = 'subscribe';
        } else {
            $action = 'unsubscribe';
        }
        
        return $action;
    }
}
