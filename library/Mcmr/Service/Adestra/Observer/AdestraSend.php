<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of Adestra
 *
 * @category Mcmr
 */
class Mcmr_Service_Adestra_Observer_AdestraSend extends Mcmr_Model_ObserverAbstract
    implements Mcmr_Model_Observer_QueueableInterface
{
    protected $_config = null;
    protected $_oldState = null;
    protected $_sentStatus = null;
    
    public function init()
    {
        $this->_sentStatus = Mailqueue_Model_Mail::STATUS_QUEUED;
        
        $config = Zend_Registry::get('mailqueue-config');
        if (isset($config->adestra)) {
            $this->_config = $config->adestra;
        } else {
            throw new Mcmr_Model_Exception("Adestra Observer enabled, but no configuration found");
        }
    }
    
    public function insert($email)
    {
        if ($email instanceof Mailqueue_Model_Mail) {
            if ('pending' === $email->getStatus()) {
                $profile = $email->getName();
                $optIn = $email->getOptin();
                $config = $this->_config->$profile;

                $list = new Mcmr_Service_Adestra_List($config->lists->$optIn);

                $campaign = new Mcmr_Service_Adestra_Campaign($config->campaign);
                try {
                    $launchId = $campaign->launch($list, $email->getId(), $this->_getLaunchData($email));
                    $email->setAttribute('adestra_launchid', $launchId);
                    $email->setStatus($this->_sentStatus);
                } catch (Exception $e) {
                    $request = (string)$campaign->getAdestraService()->getXmlRpcClient()->getLastRequest();
                    $response = (string)$campaign->getAdestraService()->getXmlRpcClient()->getLastRequest();
                    $email->setStatus(Mailqueue_Model_Mail::STATUS_FAILURE);
                    $email->setAttribute('message', $e->getMessage());
                    $email->setAttribute('traceback', $e->getTraceAsString());
                    $email->setAttribute('rawRequest', $request);
                    $email->setAttribute('rawResponse', $response);
                    Mcmr_Debug::dump($e->getMessage());
                    Mcmr_Debug::dump($e->getTraceAsString());
                }

                $mapper = Mailqueue_Model_Mail::getMapper();
                $observersEnabled = $mapper->getObserversEnabled();
                $mapper->setObserversEnabled(false);
                $mapper->save($email);
                $mapper->setObserversEnabled($observersEnabled);
            }
        }
    }
    
    public function preUpdate($email)
    {
        if ($email instanceof Mailqueue_Model_Mail) {
            $this->_oldState = $email->getStatus();
        }
    }
    
    public function update($email)
    {
        if ($email instanceof Mailqueue_Model_Mail) {
            if ('pending' === $email->getStatus() && $email->getStatus() !== $this->_oldState) {
                $profile = $email->getName();
                $optIn = $email->getOptin();
                $config = $this->_config->$profile;

                $list = new Mcmr_Service_Adestra_List($config->lists->$optIn);

                $campaign = new Mcmr_Service_Adestra_Campaign($config->campaign);
                try {
                    $launchId = $campaign->launch($list, $email->getId(), $this->_getLaunchData($email));
                    $email->setAttribute('adestra_launchid', $launchId);
                    $email->setStatus($this->_sentStatus);
                } catch (Exception $e) {
                    $email->setStatus(Mailqueue_Model_Mail::STATUS_FAILURE);
                    $email->setAttribute('message', $e->getMessage());
                }

                $mapper = Mailqueue_Model_Mail::getMapper();
                $observersEnabled = $mapper->getObserversEnabled();
                $mapper->setObserversEnabled(false);

                $mapper->save($email);
                $mapper->setObserversEnabled($observersEnabled);
            }
        }
    }
    
    public function delete($email)
    {
    }
    
    protected function _queueEmail($email)
    {
        
    }
    
    protected function _getLaunchData($email)
    {}
}
