<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Service class for Adestra campaigns
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @subpackage Adestra
 */
class Mcmr_Service_Adestra_Campaign extends Mcmr_Service_Adestra_AdestraAbstract
{
    /**
     *
     * @var string 
     */
    protected $_name = null;
    
    /**
     *
     * @var string 
     */
    protected $_description = null;
    
    /**
     *
     * @var int 
     */
    protected $_ownerUserId = null;
    
    /**
     *
     * @var int 
     */
    protected $_workspaceId = null;
    
    /**
     *
     * @var int 
     */
    protected $_listId = null;
    
    /**
     *
     * @var bool
     */
    protected $_published = null;
    
    /**
     *
     * @var type string
     */
    protected $_publishedDate = null;
    
    /**
     *
     * @var bool
     */
    protected $_launched = null;
    
    /**
     *
     * @var int
     */
    protected $_filterId = null;
    
    /**
     *
     * @var string
     */
    protected $_clientRef = null;
    
    /**
     *
     * @var string
     */
    protected $_subjectLine = null;
    
    /**
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @param string $name
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setName($name)
    {
        $this->_name = $name;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     *
     * @param string $description
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setDescription($description)
    {
        $this->_description = $description;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getOwnerUserId()
    {
        return $this->_ownerUserId;
    }

    /**
     *
     * @param int $ownerUserId
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setOwnerUserId($ownerUserId)
    {
        $this->_ownerUserId = intval($ownerUserId);
        
        return $this;
    }
    
    /**
     *
     * @return int
     */
    public function getWorkspaceId()
    {
        return $this->_workspaceId;
    }

    /**
     *
     * @param int $workspaceId
     * @return Mcmr_Service_Adestra_Campaign 
     */
    public function setWorkspaceId($workspaceId)
    {
        $this->_workspaceId = intval($workspaceId);
        
        return $this;
    }
    
    /**
     *
     * @return int
     */
    public function getListId()
    {
        return $this->_listId;
    }

    /**
     *
     * @param int $listId
     * @return Mcmr_Service_Adestra_Campaign 
     */
    public function setListId($listId)
    {
        $this->_listId = intval($listId);
        
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function getPublished()
    {
        return $this->_published;
    }

    /**
     *
     * @param bool $published
     * @return Mcmr_Service_Adestra_Campaign 
     */
    public function setPublished($published)
    {
        $this->_published = (bool)$published;
        
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getPublishedDate()
    {
        return $this->_publishedDate;
    }

    /**
     *
     * @param string $publishedDate
     * @return Mcmr_Service_Adestra_Campaign 
     */
    public function setPublishedDate($publishedDate)
    {
        $this->_publishedDate = $publishedDate;
        
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function getLaunched()
    {
        return $this->_launched;
    }

    /**
     *
     * @param bool $launched
     * @return Mcmr_Service_Adestra_Campaign 
     */
    public function setLaunched($launched)
    {
        $this->_launched = (bool)$launched;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getFilterId()
    {
        return $this->_filterId;
    }

    /**
     *
     * @param int $filterId
     * @return Mcmr_Service_Adestra_Campaign 
     */
    public function setFilterId($filterId)
    {
        $this->_filterId = intval($filterId);
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getClientRef()
    {
        return $this->_clientRef;
    }

    /**
     *
     * @param int $clientRef
     * @return Mcmr_Service_Adestra_Campaign 
     */
    public function setClientRef($clientRef)
    {
        $this->_clientRef = intval($clientRef);
        
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getSubjectLine()
    {
        return $this->_subjectLine;
    }

    /**
     *
     * @param string $subjectLine
     * @return Mcmr_Service_Adestra_Campaign 
     */
    public function setSubjectLine($subjectLine)
    {
        $this->_subjectLine = $subjectLine;
        
        return $this;
    }
    
    /**
     * Launch a new campaign. The Launch ID is returned if successful
     *
     * @param Mcmr_Service_Adestra_List|int $list
     * @param string $label
     * @param array $data
     * @return int 
     */
    public function launch($list=null, $label='', array $data=array())
    {
        if (is_object($list) && $list instanceof Mcmr_Service_Adestra_List) {
            $listId = $list->getId();
        } else {
            $listId = $list;
        }
        
        $client = $this->_adestra->getXmlRpcClient();
        $launchId = $client->call(
            'campaign.launch', 
            array(
                $this->getId(), 
                array('list_id'=>$listId, 'transaction_data'=>$data, 'launch_label'=>$label),
            )
        );
        $response = $client->getLastRequest();
            
        return $launchId;
    }
}
