<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Service class for Adestra Projects
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @subpackage Adestra
 */
class Mcmr_Service_Adestra_Project extends Mcmr_Service_Adestra_AdestraAbstract
{
    /**
     *
     * @var string 
     */
    protected $_name = null;
    
    /**
     *
     * @var string 
     */
    protected $_description = null;
    
    /**
     *
     * @var int 
     */
    protected $_ownerUserId = null;
    
    /**
     *
     * @var int 
     */
    protected $_workspaceId = null;
    
    /**
     *
     * @var string 
     */
    protected $_colour = null;
    
    /**
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @param string $name
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setName($name)
    {
        $this->_name = $name;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     *
     * @param string $description
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setDescription($description)
    {
        $this->_description = $description;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getOwnerUserId()
    {
        return $this->_ownerUserId;
    }

    /**
     *
     * @param int $ownerUserId
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setOwnerUserId($ownerUserId)
    {
        $this->_ownerUserId = intval($ownerUserId);
        
        return $this;
    }
    
    /**
     *
     * @return int 
     */
    public function getWorkspaceId()
    {
        return $this->_workspaceId;
    }

    /**
     *
     * @param int $workspaceId 
     */
    public function setWorkspaceId($workspaceId)
    {
        $this->_workspaceId = $workspaceId;
        
        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getColour()
    {
        return $this->_colour;
    }

    /**
     *
     * @param string $colour
     * @return Mcmr_Service_Adestra_Project 
     */
    public function setColour($colour)
    {
        $this->_colour = $colour;
        
        return $this;
    }


}
