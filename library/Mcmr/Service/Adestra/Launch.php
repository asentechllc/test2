<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of Launch
 *
 * @category Mcmr
 */
class Mcmr_Service_Adestra_Launch extends Mcmr_Service_Adestra_AdestraAbstract
{
    /**
     *
     * @var int
     */
    protected $_campaignId = null;
    
    /**
     *
     * @var int
     */
    protected $_userId = null;
    
    /**
     *
     * @var string
     */
    protected $_dateCreated = null;
    
    /**
     *
     * @var string
     */
    protected $_dateScheduled = null;
    
    /**
     *
     * @var string
     */
    protected $_dateStarted = null;
    
    /**
     *
     * @var string
     */
    protected $_dateComplated = null;
    
    /**
     *
     * @var int
     */
    protected $_listId = null;
    
    /**
     *
     * @var string
     */
    protected $_launchLabel = null;
    
    /**
     *
     * @return int
     */
    public function getCampaignId()
    {
        return $this->_campaignId;
    }

    /**
     *
     * @param int $campaignId
     * @return Mcmr_Service_Adestra_Launch 
     */
    public function setCampaignId($campaignId)
    {
        $this->_campaignId = intval($campaignId);
        
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getUserId()
    {
        return $this->_userId;
    }

    /**
     *
     * @param int $userId
     * @return Mcmr_Service_Adestra_Launch 
     */
    public function setUserId($userId)
    {
        $this->_userId = intval($userId);
        
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDateCreated()
    {
        return $this->_dateCreated;
    }

    /**
     *
     * @param int $dateCreated
     * @return Mcmr_Service_Adestra_Launch 
     */
    public function setDateCreated($dateCreated)
    {
        $this->_dateCreated = $dateCreated;
        
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDateScheduled()
    {
        return $this->_dateScheduled;
    }

    /**
     *
     * @param string $dateScheduled
     * @return Mcmr_Service_Adestra_Launch 
     */
    public function setDateScheduled($dateScheduled)
    {
        $this->_dateScheduled = $dateScheduled;
        
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDateStarted()
    {
        return $this->_dateStarted;
    }

    /**
     *
     * @param string $dateStarted
     * @return Mcmr_Service_Adestra_Launch 
     */
    public function setDateStarted($dateStarted)
    {
        $this->_dateStarted = $dateStarted;
        
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getDateComplated()
    {
        return $this->_dateComplated;
    }

    /**
     *
     * @param string $dateComplated
     * @return Mcmr_Service_Adestra_Launch 
     */
    public function setDateComplated($dateComplated)
    {
        $this->_dateComplated = $dateComplated;
        
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getListId()
    {
        return $this->_listId;
    }

    /**
     *
     * @param int $listId
     * @return Mcmr_Service_Adestra_Launch 
     */
    public function setListId($listId)
    {
        $this->_listId = $listId;
        
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getLaunchLabel()
    {
        return $this->_launchLabel;
    }

    /**
     *
     * @param string $launchLabel
     * @return Mcmr_Service_Adestra_Launch 
     */
    public function setLaunchLabel($launchLabel)
    {
        $this->_launchLabel = $launchLabel;
        
        return $this;
    }
}
