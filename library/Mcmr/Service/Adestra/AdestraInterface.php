<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 *
 * @author leigh
 */
interface Mcmr_Service_Adestra_AdestraInterface
{
    
    public function populate(array $data);
    
    public function getId();
    public function setValues(array $options);
    public function getValues();
    
}
