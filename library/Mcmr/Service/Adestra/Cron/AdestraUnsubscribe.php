<?php

class Mcmr_Service_Adestra_Cron_AdestraUnsubscribe extends Mcmr_Cron_ModuleAbstract
{
    const DELIMITER = ',';
    const ENCLOSURE = '"';
    
    public function execute()
    {
        //lock
        if (null !== $this->_config->unsubscribe) {
            $directory = $this->_config->unsubscribe->directory;
            $optins = $this->_config->unsubscribe->optins->toArray();
            $this->processDirectory($directory, $optins);
        }
        Mcmr_Cron::clearPhase();
        //unlock
    }
    
    public function processDirectory($directory, $optins) {
        echo( "processing dir: $directory" );
        $results = array();
        $handle = opendir($directory);
        while ($file = readdir($handle)) {
            $fullName=$directory.'/'.$file;
            if (is_file($fullName)) {
                $this->processFile($fullName, $optins);
            }
        }
        closedir($handle);
    }
    
    public function processFile($file, $optins) {
        $handle = fopen($file, 'r');
        if ( false === $handle ) {
            echo( "file not found: $file\n" );
            return false;
        }
        echo( "processing: $file\n" );
        $header = fgetcsv($handle, 0, self::DELIMITER, self::ENCLOSURE);
        while (($line = fgetcsv($handle, 0, self::DELIMITER, self::ENCLOSURE)) !== false) {
            $keyedLine = array_combine($header, $line);
            foreach ($optins as $optin) {
                $this->processEmail($keyedLine,$optin);
            }
        }
        fclose($handle);
        unlink($file);
    }
    
    public function processEmail($csvRow, $optinData) {
        if ($csvRow['campaign_id']==$optinData['campaign_id'] && $csvRow['project_id']==$optinData['project_id']) {
            $mapper = User_Model_User::getMapper();
            $email = $csvRow['email'];
            $user = $mapper->findOneByField(array('email'=>$email));        
            if (null!==$user) {
                echo( "unsubscribe from: {$optinData['optin']}, email: $email\n" );
                $user->setAttribute($optinData['optin'],null);
                $mapper->save($user);
                Mcmr_Cron::phase();
            }
        }
    }
    
}
