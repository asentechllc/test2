<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Service class for Adestra users
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @subpackage Adestra
 */
class Mcmr_Service_Adestra_User extends Mcmr_Service_Adestra_AdestraAbstract
{
    /**
     *
     * @var string 
     */
    protected $_name = null;
    
    /**
     *
     * @var string 
     */
    protected $_username = null;
    
    /**
     *
     * @var string 
     */
    protected $_email = null;
    
    /**
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @param string $name
     * @return Mcmr_Service_Adestra_User 
     */
    public function setName($name)
    {
        $this->_name = $name;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     *
     * @param string $username
     * @return Mcmr_Service_Adestra_User 
     */
    public function setUsername($username)
    {
        $this->_username = $username;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     *
     * @param string $email
     * @return Mcmr_Service_Adestra_User 
     */
    public function setEmail($email)
    {
        $this->_email = $email;
        
        return $this;
    }
}
