<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Service class for Adestra lists
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @subpackage Adestra
 */

class Mcmr_Service_Adestra_List extends Mcmr_Service_Adestra_AdestraAbstract
{
    /**
     *
     * @var type 
     */
    protected $_name = null;
    
    /**
     *
     * @var type 
     */
    protected $_description = null;
    
    /**
     *
     * @var type 
     */
    protected $_ownerUserId = null;
    
    /**
     *
     * @var int
     */
    protected $_workspaceId = null;
    
    /**
     *
     * @var int
     */
    protected $_tableId = null;
    
    /**
     *
     * @var bool
     */
    protected $_managed = null;
    
    /**
     *
     * @var int
     */
    protected $_count = null;
    
    /**
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @param string $name
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setName($name)
    {
        $this->_name = $name;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     *
     * @param string $description
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setDescription($description)
    {
        $this->_description = $description;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getOwnerUserId()
    {
        return $this->_ownerUserId;
    }

    /**
     *
     * @param int $ownerUserId
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setOwnerUserId($ownerUserId)
    {
        $this->_ownerUserId = intval($ownerUserId);
        
        return $this;
    }    
    
    /**
     *
     * @return int
     */
    public function getWorkspaceId()
    {
        return $this->_workspaceId;
    }

    /**
     *
     * @param int $workspaceId
     * @return Mcmr_Service_Adestra_List 
     */
    public function setWorkspaceId($workspaceId)
    {
        $this->_workspaceId = intval($workspaceId);
        
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getTableId()
    {
        return $this->_tableId;
    }

    /**
     *
     * @param int $tableId
     * @return Mcmr_Service_Adestra_List 
     */
    public function setTableId($tableId)
    {
        $this->_tableId = $tableId;
        
        return $this;
    }

    /**
     *
     * @return bool
     */
    public function getManaged()
    {
        return $this->_managed;
    }

    /**
     *
     * @param bool $managed
     * @return Mcmr_Service_Adestra_List 
     */
    public function setManaged($managed)
    {
        $this->_managed = (bool)$managed;
        
        return $this;
    }

    /**
     *
     * @return int
     */
    public function getCount()
    {
        return $this->_count;
    }

    /**
     *
     * @param int $count
     * @return Mcmr_Service_Adestra_List 
     */
    public function setCount($count)
    {
        $this->_count = intval($count);
        
        return $this;
    }


}
