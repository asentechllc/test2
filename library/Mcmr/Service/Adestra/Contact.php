<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Service class for Adestra contacts
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @subpackage Adestra
 */
class Mcmr_Service_Adestra_Contact extends Mcmr_Service_Adestra_AdestraAbstract
{

	const	auth_session_key = 'adestra_authed_contact';

    protected $_paramsMap = array(
        'create' => array('table_id'=>0, 'contact_data'=>1, 'dedupe_field'=>2),
        'update' => array('contact_id'=>0, 'contact_data'=>1),
    );
    
    protected $_dedupeField = 'email';
    
    protected $_tableId = null;
    
    protected $_contactData = array();
    
    public function __construct($tableId, array $data = null, Mcmr_Service_Adestra $adestra = null)
    {
    
        $this->_tableId = $tableId;
        unset($data['name']); // Cannot search for contacts on 'name'
        
        if (null === $adestra) {
            $adestra = Mcmr_Service_Adestra::getInstance();
        }
        $this->_adestra = $adestra;
                
        if (null !== $data) {
            $this->populate($data);
            
            if (isset($data['email'])) {
                $client = $this->_adestra->getXmlRpcClient();
                $collection = $client->call('contact.search', array('table_id'=>$tableId, 'search_args'=>$data));

                if (is_array($collection) && isset($collection[0])) {              
                    $this->populate($collection[0]);
                }
            }
            
            else if(isset($data['id'])){
                $client = $this->_adestra->getXmlRpcClient();
                $data = $client->call('contact.get', array('id'=>$data['id']));
                $this->populate($data);
            }
        }
    }
    
    /**
     * Return contact if available via guid auth or session check
     * @return type 
     */
    public static function authenticate($tableId, $id=null,$guid=null){
    
    	$contact = null;
    
    	if($id && $guid){
    		$contact = new self($tableId,array(
    			'id' => $id,
    		));
    		
    		if($contact->getId() && $contact->authenticate_guid($guid)){
    			$_SESSION[self::auth_session_key] = $contact->getId();
    		}
    		else{
    			$contact = null;
    		}

    	}
    	
    	# check session
    	else if(isset($_SESSION[self::auth_session_key]) && $_SESSION[self::auth_session_key]){
    		$contact = new self($tableId,array(
    			'id' => $_SESSION[self::auth_session_key],
    		));
    		
    		if(!$contact->getId())
    			$contact = null;
    	}
    	
    	return $contact;
    
    }
    
    public static function deauthenticate(){
    	if(isset($_SESSION[self::auth_session_key]))
    		unset($_SESSION[self::auth_session_key]);
    }
    
    public function authenticate_guid($guid){
        $client = $this->_adestra->getXmlRpcClient();
        $data = $client->call('contact.authenticate_guid', array(
        	'public_guid' => $guid,
        	'contact_id' => $this->_id,
        ));
        
        # should return integer value 1 otherwise fail
        return $data === 1;
    }
    
    public function lists(){
        $client = $this->_adestra->getXmlRpcClient();
        $data = $client->call('contact.lists', array(
        	'id' => $this->_id,
        ));
        return is_array($data) ? $data : array();
    }
    
    public function save()
    {
        $client = $this->_adestra->getXmlRpcClient();
        
        $params = $this->getValues();
        unset($params['id']);
        
        if (null === $this->_id) {
            $method = "contact.create";
            $params = $this->_mapXmlRpcParams('create', $params);
        } else {
            $method = "contact.update";
            $params['contact_id'] = $this->_id;
            $params = $this->_mapXmlRpcParams('update', $params);
        }

        $data = $client->call($method, $params);
        $this->setId($data);
    }
    
    /**
     *
     * @param type $id 
     */
    public function subscribe($list, $unsubId = null)
    {
        $id = null;
        if ($list instanceof Mcmr_Service_Adestra_List) {
            $id = $list->getId();
        } elseif (is_numeric($list)) {
            $id = $list;
        }
        
        if (null === $id) {
            throw new Mcmr_Service_Adestra_Exception("Cannot subscribe contact to null list");
        }
        
        if (null === $this->getId()) {
            $this->save();
        }
        
        if (null !== $unsubId && !is_array($unsubId)) {
            $unsubId = array($unsubId);
        }
        
        $params = array(
            $this->getId(),
            array($id),
            $unsubId,
        );
        
        $client = $this->_adestra->getXmlRpcClient();
        $client->call('contact.subscribe', $params);
    }
    
    /**
     *
     * @param type $id 
     */
    public function unsubscribe($list, $unsubId = null)
    {
        $id = null;
        if ($list instanceof Mcmr_Service_Adestra_List) {
            $id = $list->getId();
        } elseif (is_numeric($list)) {
            $id = $list;
        }
        
        if (null === $id) {
            throw new Mcmr_Service_Adestra_Exception("Cannot unsubscribe contact from null list");
        }
        
        if (null !== $unsubId && !is_array($unsubId)) {
            $unsubId = array($unsubId);
        }
        
        $params = array(
            $this->getId(),
            array($id),
            $unsubId,
        );

        $client = $this->_adestra->getXmlRpcClient();
        $client->call('contact.unsubscribe', $params);
    }
    
    /**
     *
     * @return type 
     */
    public function getDedupeField()
    {
        return $this->_dedupeField;
    }

    /**
     *
     * @param type $dedupeField
     * @return Mcmr_Service_Adestra_Contact 
     */
    public function setDedupeField($dedupeField)
    {
        $this->_dedupeField = $dedupeField;
        
        return $this;
    }
        
    /**
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->_contactData['email'];
    }

    /**
     *
     * @param string $email 
     */
    public function setEmail($email)
    {
        $validator = new Zend_Validate_EmailAddress();
        if ($validator->isValid($email)) {
            $this->_contactData['email'] = $email;
        } else {
            throw new Mcmr_Service_Adestra_Exception("Invalid email address {$email}");
        }
        
        return $this;
    }
    
    public function setSurname($s){$this->_contactData['surname'] = $s;}
    public function setForename($s){$this->_contactData['forename'] = $s;}
    public function setCompany($s){$this->_contactData['company'] = $s;}
    public function setCountry($s){$this->_contactData['country'] = $s;}
    public function setIns2($s){$this->_contactData['ins2'] = $s;} // Industry Sector
    public function setIns3($s){$this->_contactData['ins3'] = $s;} // Job Function

    public function getSurname(){return $this->_contactData['surname'];}
    public function getForename(){return $this->_contactData['forename'];}
    public function getCompany(){return $this->_contactData['company'];}
    public function getCountry(){return $this->_contactData['country'];}
    public function getIns2(){return $this->_contactData['ins2'];} // Industry Sector
    public function getIns3(){return $this->_contactData['ins3'];} // Job Function
    
    /**
     *
     * @return type 
     */
    public function getTableId()
    {
        return $this->_tableId;
    }

    /**
     *
     * @param type $tableId
     * @return Mcmr_Service_Adestra_Contact 
     */
    public function setTableId($tableId)
    {
        $this->_tableId = intval($tableId);
        
        return $this;
    }

    /**
     *
     * @return array 
     */
    public function getContactData()
    {
        return $this->_contactData;
    }

    /**
     *
     * @param array $contactData
     * @return Mcmr_Service_Adestra_Contact 
     */
    public function setContactData(array $contactData)
    {
        $this->_contactData = $contactData;
        
        return $this;
    }


}
