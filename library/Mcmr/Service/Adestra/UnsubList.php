<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Service class for Adestra unsubscribe lists
 *
 * @category Mcmr
 * @package Mcmr_Service
 * @subpackage Adestra
 */

class Mcmr_Service_Adestra_UnsubList extends Mcmr_Service_Adestra_AdestraAbstract
{
    /**
     *
     * @var type 
     */
    protected $_name = null;
    
    /**
     *
     * @var type 
     */
    protected $_description = null;
    
    /**
     *
     * @var type 
     */
    protected $_ownerUserId = null;
    
    /**
     *
     * @var type 
     */
    protected $_workspaceId = null;
    

    /**
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @param string $name
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setName($name)
    {
        $this->_name = $name;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     *
     * @param string $description
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setDescription($description)
    {
        $this->_description = $description;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getOwnerUserId()
    {
        return $this->_ownerUserId;
    }

    /**
     *
     * @param int $ownerUserId
     * @return Mcmr_Service_Adestra_AdestraAbstract 
     */
    public function setOwnerUserId($ownerUserId)
    {
        $this->_ownerUserId = intval($ownerUserId);
        
        return $this;
    }
    
    /**
     *
     * @return type 
     */
    public function getWorkspaceId()
    {
        return $this->_workspaceId;
    }

    /**
     *
     * @param type $workspaceId
     * @return Mcmr_Service_Adestra_UnsubList 
     */
    public function setWorkspaceId($workspaceId)
    {
        $this->_workspaceId = intval($workspaceId);
        
        return $this;
    }


}
