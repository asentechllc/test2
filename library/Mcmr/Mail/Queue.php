<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Mail
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mail queue class. Used to queue messages and recipients for sending at a specified time
 *
 * @category Mcmr
 * @package Mcmr_Mail
 */
class Mcmr_Mail_Queue extends Mcmr_Mail
{
    protected static $_defaultQueueTransport = null;
    protected $_queueRecipients = array();
    protected $_sendTime = null;
    protected $_name = null;

    public function __construct($name='', $charset = 'iso-8859-1')
    {
        parent::__construct($charset);
        $this->_name = $name;
    }

    /**
     * Adds To-header and recipient, $email can be an array, or a single string address
     *
     * @param  string|array $email
     * @param  string $name
     * @return Zend_Mail Provides fluent interface
     */
    public function addTo($email, $name = '')
    {
        $this->_queueRecipients[$email] = $name;

        return parent::addTo($email, $name);
    }

    /**
     * Register the email message and recipients with the queue system
     *
     * @param Mcmr_Mail_Queue_TransportAbstract $transport
     * @param Mcmr_Mail_Queue_StoreInterface $store
     * @return Mcmr_Mail_Queue
     */
    public function register(Mcmr_Mail_Queue_TransportAbstract $transport = null,
            Mcmr_Mail_Queue_StoreInterface $store = null)
    {
        if (null === $transport) {
            if (!self::$_defaultQueueTransport instanceof Zend_Mail_Queue_TransportAbstract) {
                // @TODO default transport
            } else {
                $transport = self::$_defaultQueueTransport;
            }
        }

        if ($this->_date === null) {
            $this->setDate();
        }

        if (null === $this->_from && null !== self::getDefaultFrom()) {
            $this->setFromToDefaultFrom();
        }

        if (null === $this->_replyTo && null !== self::getDefaultReplyTo()) {
            $this->setReplyToFromDefault();
        }

        $transport->register($this, $store);

        return $this;
    }

    /**
     * Sends this email using the given transport or a previously
     * set DefaultTransport or the internal mail function if no
     * default transport had been set.
     *
     * @param  Zend_Mail_Transport_Abstract $transport
     * @throws Mcmr_Mail_Exception when developer email address is not set in non-production environment
     * @return Zend_Mail                    Provides fluent interface
     */
    public function send(Mcmr_Mail_Queue_TransportAbstract $transport = null,
            Mcmr_Mail_Queue_StoreInterface $store = null)
    {
        if (null === $transport) {
            if (!self::$_defaultQueueTransport instanceof Zend_Mail_Queue_TransportAbstract) {
                // @TODO default transport
            } else {
                $transport = self::$_defaultQueueTransport;
            }
        }

        if ($this->_date === null) {
            $this->setDate();
        }

        if (null === $this->_from && null !== self::getDefaultFrom()) {
            $this->setFromToDefaultFrom();
        }

        if (null === $this->_replyTo && null !== self::getDefaultReplyTo()) {
            $this->setReplyToFromDefault();
        }

        $transport->send($this, $store);

        return $this;
    }

    /**
     * Return list of recipient email addresses
     *
     * @return array (of strings)
     */
    public function getRecipients()
    {
        // Prevent real recipients from being added to queue if this is not a production site.
        if (!defined('APPLICATION_ENV') || 'production' !== APPLICATION_ENV) {
            $config = Zend_Registry::get('app-config');
            $this->_queueRecipients = array();
            $this->clearRecipients();

            $name = isset($config->developer->name) ? $config->developer->name : '';
            $this->addTo($config->developer->email, $name);
        }

        return $this->_queueRecipients;
    }

    /**
     * Clears list of recipient email addresses
     *
     * @return Zend_Mail Provides fluent interface
     */
    public function clearRecipients()
    {
        $this->_queueRecipients = array();

        return parent::clearRecipients();
    }

    /**
     * Set the time to send the email as a unix timestamp
     *
     * @param int $time 
     */
    public function setSendTime($time)
    {
        $this->_sendTime = (int) $time;
    }

    /**
     * Get the time this email is due to be sent
     *
     * @return int
     */
    public function getSendTime()
    {
        if (null === $this->_sendTime) {
            $this->_sendTime = time();
        }

        return $this->_sendTime;
    }

    /**
     * Set the name ID of this queue
     *
     * @param string $name 
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * Get the name of this queue
     *
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

}