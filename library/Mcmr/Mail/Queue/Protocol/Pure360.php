<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Mail
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Queue protocol class for Pure 360
 *
 * @category Mcmr
 * @package Mcmr_Mail
 */
class Mcmr_Mail_Queue_Protocol_Pure360 extends Mcmr_Mail_Queue_ProtocolAbstract
{
    private $_username = null;
    private $_password = null;
    private $_authenticated = null;
    private $_notifyUrl = null;
    private $_service = null;

    public function  __construct($options)
    {
        $this->setOptions($options);
        
        $this->_service = new Mcmr_Service_Pure360($options);
    }

    /**
     * Set the queue username login
     *
     * @param string $username
     * @return Mcmr_Mail_Queue_Protocol_Pure360 
     */
    public function setUsername($username)
    {
        $this->_username = $username;
        return $this;
    }
    
    /**
     * Set the queue login password
     *
     * @param string $password
     * @return Mcmr_Mail_Queue_Protocol_Pure360 
     */
    public function setPassword($password)
    {
        $this->_password = $password;
        
        return $this;
    }
    
    /**
     * Set the notification callback URL that events should be sent to
     *
     * @param string $notifyUrl
     * @return Mcmr_Mail_Queue_Protocol_Pure360 
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->_notifyUrl = $notifyUrl;
        
        return $this;
    }
    
    /**
     * Connect to the queue service
     */
    public function connect()
    {
        $this->_login();
    }

    /**
     * Disconnect from the queue service
     */
    public function  _disconnect()
    {
        $this->_logout();
    }

    /**
     * Create a recipient list
     *
     * @param string $queue
     * @param array $recipients
     * @return type 
     */
    public function createList($queue, $recipients)
    {
        if (null === $this->_authenticated) {
            $this->connect();
        } elseif (false === $this->_authenticated) {
            throw new Zend_Mail_Exception('Cannot add recipient. Pure360 authentication has failed');
        }

        $listDataSource = array(array('email', 'name'));
        foreach ($recipients as $email=>$name) {
            $listDataSource[] = array($email, $name);
        }
        
        $response = $this->_service->createList($queue, $listDataSource, $this->_notifyUrl);

        return $response['bus_entity_campaign_list_key']['listId'];
    }

    /**
     * Create a message on the queue service. The newly created message ID is returned
     * when successfully created
     *
     * @param string $name
     * @param string $subject
     * @param string $plain
     * @param string $html
     * @return string 
     */
    public function createMessage($name, $subject, $plain, $html)
    {
        $response = $this->_service->createEmail($name, $subject, $plain, $html);

        return $response['bus_entity_campaign_email_key']['messageId'];
    }

    /**
     * Queue the message for sending against the supplied message a queue
     * The message queue ID is returned when successful
     *
     * @param string $queue
     * @param string $message
     * @param int $deliveryTime
     * @return string 
     */
    public function send($queue, $message, $deliveryTime = null)
    {
        $response = $this->_service->createDelivery($queue, $message, $deliveryTime);

        return $response['bus_entity_campaign_delivery_key']['deliveryId'];
    }

//    public function clear($queue)
//    {
//        if (null === $this->_authenticated) {
//            $this->connect();
//        } elseif (false === $this->_authenticated) {
//            throw new Zend_Mail_Exception('Cannot clear queue. Pure360 authentication has failed');
//        }
//
//    }

    /**
     * Login to the Pure 360 service
     */
    protected function _login()
    {
        if (!$this->_authenticated) {
            try {
                $this->_service->login();
                $this->_authenticated = true;
            } catch (Exception $e) {
                $this->_authenticated = false;
    
                throw $e;
            }
        }
    }

    /**
     * Log out of the Pure 360 service
     */
    protected function _logout()
    {
        //$this->_service->logout();
        $this->_authenticated = false;
    }
}
