<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Mail
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for Queue Transport classes
 *
 * @category Mcmr
 * @package Mcmr_Mail
 */
abstract class Mcmr_Mail_Queue_TransportAbstract extends Zend_Mail_Transport_Abstract
{
    /**
     * Sends the supplied email to its recipients
     *
     * @param Mcmr_Mail_Queue $mail
     * @param Mcmr_Mail_Queue_StoreInterface $store 
     */
    public function send(Mcmr_Mail_Queue $mail, Mcmr_Mail_Queue_StoreInterface $store=null)
    {
        $this->_mail = $mail;
        
        // Send to transport!
        $this->_sendMail($store);
    }
    
    /**
     * Register an email with the queue
     *
     * @param Mcmr_Mail_Queue $mail
     * @param Mcmr_Mail_Queue_StoreInterface $store
     * @return type 
     */
    public function register(Mcmr_Mail_Queue $mail, Mcmr_Mail_Queue_StoreInterface $store=null)
    {
        $this->_mail = $mail;
        
        return $this->_registerMail($store);
    }
    
    /**
     * Set the mail queue options using an array
     *
     * @param array $options
     * @return Mcmr_Mail_Queue_TransportAbstract
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $normalized = ucfirst($key);
            $method = 'set' . $normalized;
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
        
        return $this;
    }
        
    /**
     * Set the mail queue options using a config object
     *
     * @param Zend_Config $config
     * @return Mcmr_Mail_Queue_TransportAbstract 
     */
    public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());
        
        return $this;
    }
    
    abstract protected function _registerMail(Mcmr_Mail_Queue_StoreInterface $model=null);
}
