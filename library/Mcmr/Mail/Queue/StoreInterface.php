<?php
interface Mcmr_Mail_Queue_StoreInterface
{
    public function setListid($listId);
    public function getListid();
    
    public function setMessageid($messageId);
    public function getMessageid();

    public function setDeliveryid($deliveryId);
    public function getDeliveryid();
}
