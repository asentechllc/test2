<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Mail
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Queue protocol abstract class
 *
 * @category Mcmr
 * @package Mcmr_Mail
 */abstract class Mcmr_Mail_Queue_ProtocolAbstract extends Zend_Mail_Protocol_Abstract
{
    /**
     * Set the payment options using an array
     *
     * @param array $options
     * @return Mcmr_Mail_Queue_TransportAbstract
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $normalized = ucfirst($key);
            $method = 'set' . $normalized;
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
        
        return $this;
    }
        
    /**
     * Set the protocol configuration
     *
     * @param Zend_Config $config
     * @return Mcmr_Mail_Queue_ProtocolAbstract 
     */
    public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());
        
        return $this;
    }
}
