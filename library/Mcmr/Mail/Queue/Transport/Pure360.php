<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Mail
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Queue transport class for Pure 360
 *
 * @category Mcmr
 * @package Mcmr_Mail
 */
class Mcmr_Mail_Queue_Transport_Pure360 extends Mcmr_Mail_Queue_TransportAbstract
{
    protected $_connection = null;
    protected $_username = null;
    protected $_password = null;
    protected $_notifyUrl = null;

    public function __construct($options=null)
    {
        if ($options instanceof Zend_Config) {
            $this->setConfig($options);
        } elseif (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * Set the notification callback URL
     *
     * @param type $notifyUrl
     * @return Mcmr_Mail_Queue_Transport_Pure360 
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->_notifyUrl = $notifyUrl;
        
        return $this;
    }
    
    /**
     * Set the username used to login to the queue service
     *
     * @param string $username
     * @return Mcmr_Mail_Queue_Transport_Pure360 
     */
    public function setUsername($username)
    {
        $this->_username = $username;
        
        return $this;
    }
    
    /**
     * Set the password used to login to the queue service
     *
     * @param string $password 
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }
    
    /**
     * Get the queue profile name.
     *
     * @return string
     */
    public function getProfileName()
    {
        return $this->_profileName;
    }
    
    /**
     * Sets the connection protocol instance
     *
     * @param Mcmr_Mail_Queue_Protocol_Pure360 $connection
     *
     * @return void
     */
    public function setConnection(Mcmr_Mail_Queue_Protocol_Pure360 $connection)
    {
        $this->_connection = $connection;
    }

    /**
     * Gets the connection protocol instance
     *
     * @return Zend_Mail_Protocol|null
     */
    public function getConnection()
    {
        // If sending multiple messages per session use existing adapter
        if (!($this->_connection instanceof Mcmr_Mail_Queue_Protocol_Pure360)) {
            $this->_connection = new Mcmr_Mail_Queue_Protocol_Pure360(
                array(
                    'username' =>$this->_username,
                    'password' => $this->_password,
                    'notifyUrl' => $this->_notifyUrl,
                )
            );
        }
        
        return $this->_connection;
    }

    /**
     * Register the email list and message with the queue. This will not queue them
     * for sending.
     *
     * @param Mcmr_Mail_Queue_StoreInterface $store 
     */
    protected function _registerMail(Mcmr_Mail_Queue_StoreInterface $store=null)
    {
        $connection = $this->getConnection();
        $connection->connect();

        $listId = $connection->createList($this->_mail->getName(), $this->_mail->getRecipients());
        
        $messageId = $connection->createMessage(
            $this->_mail->getName(),
            $this->_mail->getSubject(),
            $this->_mail->getOriginalBodyText(),
            $this->_mail->getOriginalBodyHtml()
        );

        if (null !== $store) {
            $store->setListid($listId);
            $store->setMessageid($messageId);
        }
    }
    
    /**
     * Take the registered email and mailing list and set a send time.
     *
     * @param Mcmr_Mail_Queue_StoreInterface $store 
     */
    protected function _sendMail(Mcmr_Mail_Queue_StoreInterface $store=null)
    {
        $connection = $this->getConnection();
        $connection->connect();

        $deliveryId = $connection->send($this->_mail->getName(), $this->_mail->getName(), $this->_mail->getSendTime());

        if (null !== $store) {
            $store->setDeliveryid($deliveryId);
        }
    }
}
