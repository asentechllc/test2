<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Mail
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Exception.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Mcmr_Mail Exception class
 *
 * @category Mcmr
 * @package Mcmr_Mail
 */
class Mcmr_Mail_Exception extends Zend_Exception
{
}
