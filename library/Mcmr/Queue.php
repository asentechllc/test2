<?php

class Mcmr_Queue extends Zend_Queue
{
    static private $_queues = array();
    static private $_instances = array();


    /**
     *
     * @param string $name
     * @return Zend_Queue
     */
    public static function getInstance($name)
    {
        if (!array_key_exists($name, self::$_instances)) {
            if (isset(self::$_queues[$name])) {
                $adapter = self::$_queues[$name]['adapter'];
                if (!array_key_exists('name', self::$_queues[$name])) {
                    self::$_queues[$name]['name'] = $name;
                }
                
                self::$_instances[$name] = new Zend_Queue($adapter, self::$_queues[$name]);
            } else {
                self::$_instances[$name] = null;
            }
        }
        
        return self::$_instances[$name];
    }
    
    public static function setQueue($name, $options)
    {
        self::$_queues[$name] = $options;
    }
}
