<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Filter used to convert ISO strings to UTF-8
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @subpackage Utf8
 */
class Mcmr_Filter_Utf8 implements Zend_Filter_Interface
{

    /**
     * Convert iso string to utf-8
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        if (is_array($value)) {
            return array_map(array($this, 'filter'), $value);
        } elseif (is_string($value)) {
            mb_substitute_character("none");
            
            return str_replace(
                array("&lt;", "&gt;"), 
                array("<", ">"), 
                htmlentities(
                    mb_convert_encoding(
                        $value, 
                        "UTF-8", 
                        mb_detect_encoding($value, "UTF-8, ISO-8859-1, ISO-8859-15, cp1251, cp1252", true)
                    ), 
                    null, 
                    "UTF-8", 
                    false
                )
            );
        } else {
            return $value;
        }
    }
}
