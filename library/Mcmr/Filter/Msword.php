<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Callback.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Filter used to replace MS Word encoded characters with normal characters
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @subpackage Callback
 */
class Mcmr_Filter_Msword implements Zend_Filter_Interface
{

    /**
     * Convert all MS encoded characters to normal characters
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        $replace = array();
        $replace['“'] = '"';
        $replace['”'] = '"';
        $replace['‘'] = "'";
        $replace['’'] = "'";
        $replace['…'] = "...";
//        $replace['£'] = '&pound;';
        $replace['–'] = '-';

        $value = strtr($value, $replace);

        return $value;
    }
}
