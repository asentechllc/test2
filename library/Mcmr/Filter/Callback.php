<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Callback.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Filter a value using a callback
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @subpackage Callback
 */
class Mcmr_Filter_Callback implements Zend_Filter_Interface
{
    private $_callback = null;

    /**
     * Create a new Mcmr_Filter_Callback object.
     *
     * @param callback $callback
     */
    public function __construct($callback)
    {
        $this->setCallback($callback);
    }

    /**
     * Set the callback function
     *
     * @param callback $callback
     * @return Mcmr_Filter_Callback
     */
    public function setCallback($callback)
    {
        $this->_callback = $callback;

        return $this;
    }

    /**
     * Filter the value using the callback function
     *
     * @param mixed $value
     * @return mixed
     */
    public function filter($value)
    {
        if (is_callable($this->_callback)) {
            return call_user_func_array($this->_callback, array($value));
        } else {
            throw new Zend_Filter_Exception('Unknown Callback');
        }
    }
}