<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Callback.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Filter a filename ensuring it has a unique name and won't overwrite any other files
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @subpackage File
 */
class Mcmr_Filter_File_Unique extends Zend_Filter_File_Rename
{
    
    /**
     * Defined by Zend_Filter_Interface
     *
     * @param  string $value Full path of file to change
     * @throws Zend_Filter_Exception
     * @return string The new filename which has been set, or false when there were errors
     */
    public function filter($value)
    {
        if (!file_exists($value)) {
            require_once 'Zend/Filter/Exception.php';
            throw new Zend_Filter_Exception("File '$value' not found");
        }

        if (!is_writable($value)) {
            throw new Zend_Filter_Exception("File '$value' is not writable");
        }

        // Get a unique filename
        $x = 1;
        do {
            $fileName = $x . '-' . $this->_sanitiseName(basename($value));
            $return = dirname($value) . DIRECTORY_SEPARATOR . $fileName;
            $x++;
        } while (file_exists($return));
        
        // Rename and return
        $result  = rename($value, $return);
        if (!$result) {
            throw new Zend_Filter_Exception("Problem while writing file '$value'");
        }

        return $return;
    }

    /**
     * sanitise the filename by removing special characters and making everything lower case
     *
     * @param type $text
     * @return type 
     */
    protected function _sanitiseName($text)
    {
        //replace any non-ascii characters like accentuated letters  with a closest ascii equivalent
        $text = @iconv("UTF-8", "ISO-8859-1//IGNORE//TRANSLIT", $text);
        
        //lowercase everything
        $text = strtolower($text);
        $text = trim($text);

        //now replaces spaces with underscores using a single underscore for every whitespace string
        $text = preg_replace('/ +/', '-', $text);

        return $text;

    }
}
