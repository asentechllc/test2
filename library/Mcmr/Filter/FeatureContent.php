<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Callback.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Filter used to process Feature Content block arrays
 *
 * @category Mcmr
 * @package Mcmr_Filter
 * @subpackage Callback
 */
class Mcmr_Filter_FeatureContent implements Zend_Filter_Interface
{

    /**
     * Run required filters on Feature Content blocks array
     *
     * @param mixed
     * @return mixed
     */
    public function filter($value)
    {
    
    	# traverse and recurse into array if required
    	if(is_array($value)){
    	
    		# remove template block
    		if(isset($value['new']))
    			unset($value['new']);
    	
    		# if it's a content block - apply filters
    		if(isset($value['type']) && isset($value['copy'])){
    			$value['copy'] = $this->_copyFilter($value['copy']);
    		}

    		# otherwise look deeper
    		else{
    			foreach($value as $k=>$v){
    				$value[$k] = $this->filter($v);
    			}
    		}
    		
    	}
    	
    	return $value;
    	
    }
    
    /**
     * Run required filters on copy blocks
     *
     * @param string
     * @return string
     */
    private function _copyFilter($copy){
    	$filter = new Mcmr_Filter_Msword;
    	return $filter->filter($copy);
    }
}
