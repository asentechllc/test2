<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Stats
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 */

/**
 * Lightweight interface for recording stats
 * 
 * @category Mcmr
 * @package Mcmr_Stats
 */
class Mcmr_Stats
{

	const	popularity_window = 14;	// 2 week popularity window

	public static function read($modelClass,$modelId=false){self::record($modelClass,'read',$modelId);}
	public static function email($modelClass,$modelId=false){self::record($modelClass,'email',$modelId);}
	public static function comment($modelClass,$modelId=false){self::record($modelClass,'comment',$modelId);}
	
	public static function record($modelClass,$stat='read',$modelId=false){
		
		# model passed in instead of class name
		if(is_object($modelClass)){
			if(method_exists($modelClass,'getId')){
				$modelId = $modelClass->getId();
				$modelClass = get_class($modelClass);
			}
			# invalid model type
			else{
				return;
			}
		}
		
		self::_put($modelClass,$stat,$modelId);
        
	}
	
	private static function _put($modelClass,$action,$id){

        $mapper = Default_Model_Stat::getMapper();
        $db = $mapper->getDbTable()->getAdapter();

		$db->query("INSERT DELAYED INTO default_stats (stat_date,stat_site,stat_modeltype,stat_modelid,stat_action,stat_count) VALUES(?,?,?,?,?,1)",array(
			date("Y-m-d"),
			'default',
			$modelClass,
			$id,
			$action
		));
		
	}
}
