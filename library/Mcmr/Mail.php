<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Mail
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Mail.php 1836 2010-11-23 14:06:47Z michal $
 */

/**
 * A Mail class. Ensures that system mail is never sent to a real client when being executed in a
 * non-production environment.
 *
 * @category Mcmr
 * @package Mcmr_Mail
 */
class Mcmr_Mail extends Zend_Mail
{
    private static $_sendcount = 0;
    private static $_floodemails = array();
    protected $_originalHtmlBody = '';
    protected $_originalTextBody = '';

    /**
     * Sends this email using the given transport or a previously
     * set DefaultTransport or the internal mail function if no
     * default transport had been set.
     *
     * @param  Zend_Mail_Transport_Abstract $transport
     * @throws Mcmr_Mail_Exception when developer email address is not set in non-production environment
     * @return Zend_Mail                    Provides fluent interface
     */
    public function send($transport = null)
    {
        self::$_sendcount++;

        //by default in environments other than production all outgoing email is
        //captured and sent to developer email address
        //this can disbaled by explicitly setting resources.mail.capture to false in any environment
        $config = Zend_Registry::get('app-config');
        if (('production' !== APPLICATION_ENV )
                && ((!isset($config->resources->mail->capture)) || ($config->resources->mail->capture === false))) {
            $recipients = $this->getRecipients();
            
            // Prevent the developer from being flooded with emails
            $floodlimit = isset($config->developer->floodlimit)?$config->developer->floodlimit:5;
            if (self::$_sendcount > $floodlimit) {
                self::$_floodemails[] = array('email'=>$recipients, 'subject' => $this->getSubject());
                return $this;
            }

            if (isset($config->developer->email)) {
                $this->clearRecipients();

                $body = $this->_originalHtmlBody.$this->_buildDevFooter($recipients);
                $this->setBodyHtml($body);

                $name = isset($config->developer->name)?$config->developer->name:'';
                $this->addTo($config->developer->email, $name);
            } else {
                throw new Mcmr_Mail_Exception('Developer email not set and not production environment');
            }
        }
        
        return parent::send($transport);
    }

    /**
     * Sets the HTML body for the message
     *
     * @param  string    $html
     * @param  string    $charset
     * @param  string    $encoding
     * @return Zend_Mail Provides fluent interface
     */
    public function setBodyHtml($html, $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
    {
        $this->_originalHtmlBody = $html;
        parent::setBodyHtml($html, $charset, $encoding);
    }

    /**
     * Sets the text body for the message.
     *
     * @param  string $txt
     * @param  string $charset
     * @param  string $encoding
     * @return Zend_Mail Provides fluent interface
    */
    public function setBodyText($txt, $charset = null, $encoding = Zend_Mime::ENCODING_QUOTEDPRINTABLE)
    {
        $this->_originalTextBody = $txt;
        return parent::setBodyText($txt, $charset, $encoding);
    }
    
    /**
     * Return the body html content without any reformatting applied to it.
     *
     * @return string
     */
    public function getOriginalBodyHtml()
    {
        return $this->_originalHtmlBody;
    }
    
    /**
     * Return the body text without any reformatting applied to it.
     *
     * @return string
     */
    public function getOriginalBodyText()
    {
        return $this->_originalTextBody;
    }
    
    /**
     * Send an email to the developer detailing information about all the emails that would
     * be sent by the system since the flood limit was hit.
     */
    public function floodAlert()
    {
        if ('production' !== APPLICATION_ENV) {
            try {
                $config = Zend_Registry::get('app-config');
                $floodlimit = isset($config->developer->floodlimit)?$config->developer->floodlimit:5;
                if (self::$_sendcount > $floodlimit) {
                    $mail = new Zend_Mail();

                    if (isset($config->developer->email)) {
                        $name = isset($config->developer->email)?$config->developer->email:'';
                        $mail->addTo($config->developer->email, $name);
                        $mail->setBodyHtml($this->_buildFloodLimitAlert());
                        $mail->setSubject('Dev environment email flood limit tripped');

                        $mail->send();
                    }
                }
            } catch(Exception $e) {
                Mcmr_Debug::dump($e->getMessage());
            }
        }
    }

    /**
     * Builds the flood alert email that is sent to the developer.
     *
     * @return string
     */
    private function _buildFloodLimitAlert()
    {
        $floodtemplate = new Zend_View();
        $floodtemplate->setScriptPath(APPLICATION_PATH . '/templates/');
        $floodtemplate->emails = self::$_floodemails;

        return $floodtemplate->render('devemail_floodalert.phtml');
    }

    /**
     * Builds the HTML footer to be attached to the developer email.
     *
     * @param array $recipients
     * @return string
     */
    private function _buildDevFooter($recipients = array())
    {
        $footertemplate = new Zend_View();
        $footertemplate->setScriptPath(APPLICATION_PATH . '/templates/');
        $footertemplate->recipients = $recipients;

        return $footertemplate->render('devemail_footer.phtml');
    }
}
