<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mcmr
 * @package    Mcmr_Exception
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: PageNotFound.php 719 2010-06-01 16:31:57Z leigh $
 */

/**
 *
 * @category   Mcmr
 * @package    Mcmr_Exception
 * @subpackage PageNotFound
 */
class Mcmr_Exception_PageNotFound extends Zend_Controller_Action_Exception
{
    public function __construct($msg='Page Not Found', $code=404, $previous=null)
    {
        parent::__construct($msg, $code, $previous);
    }
}
