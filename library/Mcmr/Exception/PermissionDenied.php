<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mcmr
 * @package    Mcmr_Exception
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: PermissionDenied.php 844 2010-06-18 14:03:50Z leigh $
 */

/**
 *
 * @category   Mcmr
 * @package    Mcmr_Exception
 * @subpackage PermissionDenied
 */
class Mcmr_Exception_PermissionDenied extends Zend_Exception
{
    public function __construct($msg='Unauthorised', $code=401, $previous=null)
    {
        parent::__construct($msg, $code, $previous);
    }
}
