<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 *
 * @category Mcmr
 */
class Mcmr_Layout extends Zend_Layout
{

	protected $_deviceLayouts;

    public function layoutExists($name)
    {
        if ($this->inflectorEnabled() && (null !== ($inflector = $this->getInflector())))
        {
            $name = $this->_inflector->filter(array('script' => $name));
        }
	    $view = $this->getView();
        if (null !== ($path = $this->getViewScriptPath())) {
            if (method_exists($view, 'addScriptPath')) {
                $view->addScriptPath($path);
            } else {
                $view->setScriptPath($path);
            }
        } elseif (null !== ($path = $this->getViewBasePath())) {
            $view->addBasePath($path, $this->_viewBasePrefix);
        }
		$result = $view->scriptExists($name);
        return $result;
    }

    public static function startMvc($options = null)
    {
    	if (null === self::$_mvcInstance) {
            Zend_Layout::$_mvcInstance = new Mcmr_Layout($options, true);
        }
	        
        if (is_string($options)) {
            self::$_mvcInstance->setLayoutPath($options);
        } elseif (is_array($options) || $options instanceof Zend_Config) {
            self::$_mvcInstance->setOptions($options);
        }

        return self::$_mvcInstance;
    }

    public function setDeviceLayouts($val) 
    {
    	$this->_deviceLayoutsa = $val;
    	return $this;
    }

    public function getDeviceLayouts() 
    {
    	return $this->_deviceLayoutsa;
    }

}
