<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Mcmr_Cron
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CircuitBreaker.php 2277 2011-03-25 15:57:35Z leigh $
 */

/**
 * A class for creating modular command line scripts.
 * 
 * @category Mcmr
 * @package Mcmr_CircuitBreaker
 */
class Mcmr_Cron
{
    private $_module = null;
    private $_moduleArgs = null;
    private $_lockFile = null;
    private $_config = null;
    private $_moduleConfig = null;
    private $_loader = null;
    private $_request = null;

    public function __construct(Zend_Config $config, $moduleName, $moduleConfig = null, $siteName='default', $moduleArgs=null)
    {
        if (null === $moduleConfig) {
            $moduleConfig = new Zend_Config(array());
        }

        $this->_module = $moduleName;
        $this->_moduleArgs = $moduleArgs;
        $this->_lockFile = SITE_PATH . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR 
                . strtolower($this->_module) .'.lock';

        $this->_config = $config;
        $this->_moduleConfig = $moduleConfig;
        Zend_Registry::set($moduleName.'-config', $moduleConfig);
        $this->_loader = new Zend_Loader_PluginLoader();

        $this->_loader->addPrefixPath('Mcmr_Cron', 'Mcmr/Cron');
        if (isset($this->_config->cron->namespace)) {
            foreach ($this->_config->cron->namespace as $namespace) {
                $this->_loader->addPrefixPath($namespace->prefix, $namespace->path);
            }
        }
        
        $this->_request = Zend_Controller_Front::getInstance()->getRequest();
        $this->_request->setSiteName($siteName);
        $this->_request->setModuleName($moduleName);
    }

    /**
     * Execute the cron module
     *
     * @param type $lockCheck 
     */
    public function execute($lockCheck = true)
    {
        // Only execute the module if a lock is obtained. Otherwise a previous instance is still running, or crashed
        if (!$lockCheck || $this->_lock()) {
            $module = $this->_loadModule();
            $module->setConfig($this->_moduleConfig);
            $module->setRequest($this->_request);
            $module->setArgs($this->_moduleArgs);
            $module->init();
            $module->execute();

            if ($lockCheck) {
                $this->_unlock();
            }
        }
    }

    public static function phase()
    {
        static $phaseCount = 0;
        $phaseCount++;
        
        $phases = array("|", "/", "-", "\\");

        printf('%s%s', chr(8), $phases[$phaseCount%4]);
    }

    public static function clearPhase()
    {
        print (chr(8));
    }

    private function _lock()
    {
        if (is_file($this->_lockFile)) {
            return false;
        }

        if ( false === touch($this->_lockFile)) {
            throw new Zend_Exception('Cannot create lock file');
        }

        return true;
    }

    private function _unlock()
    {
        if (false === unlink($this->_lockFile)) {
            throw new Zend_Exception('Unable to delete lock file');
        }
        
        return true;
    }

    private function _loadModule()
    {
        $classname = $this->_loader->load($this->_module);

        if (class_exists($classname)) {
            $module = new $classname();
            if ($module instanceof Mcmr_Cron_ModuleInterface) {
                // Module found. Return it
                return $module;
            }


            throw new Zend_Exception('Module is not a Cron module');
        }

        throw new Zend_Exception('Unable to find cron module '.$this->_module);
    }
}
