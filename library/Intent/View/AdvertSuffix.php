<?php

class Intent_View_AdvertSuffix extends Zend_View_Helper_Abstract
{
	/**
	 * Returns adver configuration suffix
	 * @param null $section determines adSection from configuration
	 * @param string $section forces adSection portion
	 * @param true $section detects adSection from tags
	 **/
    public function advertSuffix($section=null)
    {
    	$suffix = '';
    
        $config = Zend_Registry::get('default-config');
		$route = Zend_Controller_Front::getInstance()->getRouter()->getCurrentRouteName();
        
        if(isset($config->adPrefix))
        	$suffix .= $config->adPrefix;

        if ('production' !== APPLICATION_ENV) {
            $suffix .= '-'.APPLICATION_ENV.'_';
        }
        
        $tagsFound = $this->_getTags();
        $tagUsed = false;
        $tagForced = array();
        
        if(isset($config->adTagForced) && empty($section)){
        	$tagForced = $config->adTagForced->toArray();
        	foreach($tagForced as $tag){
        		if(in_array($tag,$tagsFound)){
        			$section = true;
        			break;
        		}
        	}
        }
        
        if($section && $section !== true)
        	$suffix .= $section;
        else{
       		$adSection = $this->view->adSection['section'];
        	
        	# check for tag-based adSection overrides
        	if(isset($config->adTag) && $section === true){
        		if(is_array($tags = $config->adTag->toArray())){
        			foreach($tags as $tag=>$_adSection){
        			
        				$tag = trim(strtolower($tag));
        				if(in_array($tag,$tagsFound)){
        					$adSection = $_adSection;
        					$tagUsed = true;
        				}
        				
        			}
        		}
        	}

			if(isset($config->adSuffix) && !$tagUsed){
				foreach($config->adSuffix->toArray() as $suf){
					if(isset($suf['route']) && isset($suf['suffix'])){
						try{
							if(@preg_match('/'.$suf['route'].'/',$route)){
								$adSection .= $suf['suffix'];
								break;
							}
						}
						catch(Exception $e){}
					}
				}
			}

	        $suffix .= $adSection;
	    }

        return $suffix;
    }
    
    private function _getTags(){
    
    	$tags = array();
    
		# check request
		if(preg_match('/\/tag\/(.*?)(\/|$)/',strtolower($this->view->request()->getPathInfo()),$m)){
			$tags[] = $m[1];
		}
	
		# check article tags
		if(!empty($this->view->adSection['modelTags']) && is_array($this->view->adSection['modelTags'])){
			foreach($this->view->adSection['modelTags'] as $modelTag){
				$tags[] = Mcmr_StdLib::urlize($modelTag);
			}
		}
		
		return $tags;
    }
}
