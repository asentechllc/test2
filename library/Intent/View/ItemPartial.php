<?php

class Intent_View_ItemPartial extends Zend_View_Helper_Abstract
{
    public function itemPartial($dir, $params)
    {
    	$model = $params['model'];
    	if (!$model) {
    		throw new Exception("You have to set model");
    	}
    	$pathWithType = $dir.'/'.Mcmr_Model::getPathPart($model,true,false).'.phtml';
        $partialHelper = $this->view->getHelper('partial');
        if ($this->partialExists($pathWithType)) {
            return $partialHelper->partial($pathWithType, $params);
        } else {
            $pathWithoutType = $dir.'/'.Mcmr_Model::getPathPart($model, false, false).'.phtml';
            if ($this->partialExists($pathWithoutType)) {
                return $partialHelper->partial($pathWithoutType, $params);
            } else {
                throw new Exception("Both $pathWithType and $pathWithoutType are missing.");
            }
        }
	  	
    }

    protected function partialExists($path)
    {
        return $this->view->scriptExists($path);
    }

}
