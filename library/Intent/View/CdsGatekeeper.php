<?php

class Intent_View_CdsGatekeeper extends Zend_View_Helper_Abstract
{
    protected $_cdsGatekeeper = null;
    
    public function cdsGatekeeper($originator, $type='FULL')
    {
        if (null === $this->_cdsGatekeeper) {
            $this->_cdsGatekeeper = new Intent_Service_CdsGatekeeper($originator, $type);
        } else {
            $this->_cdsGatekeeper->setOriginator($originator);
            $this->_cdsGatekeeper->setType($type);
        }
        
        return $this->_cdsGatekeeper;
    }
}
