<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData_PCR
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * EmailData all data for daily digest email specific to PCR.
 * This neede because unlike on all intent sites built so far events on music week are using events module and not news articles of event type.
 *
 * @category Intent
 * @package Intent_EmailData_PCR
 */
class Intent_EmailData_PCR_Dailydigest extends Intent_EmailData_Dailydigest
{
}