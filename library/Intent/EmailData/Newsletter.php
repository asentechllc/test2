<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * EmailData class for fetching all the data needed in a Newsletter
 *
 * @category Intent
 * @package Intent_EmailData
 */
class Intent_EmailData_Newsletter extends Intent_EmailData_EmailDataAbstract
{

    /**
     * @see Intent_EmailData_EmailDataAbstract::adestraData()
     */
    public function adestraData()
    {
        $data = parent::adestraData();

        $articleMapper = News_Model_Article::getMapper();
        $id = $this->_email->getAttribute('articles');
        if (null !== $id) {
            $article = $articleMapper->findOneByField(array('id'=>$id));
        } else {
            $article = new News_Model_Article();
        }

        $articleProcessor = Intent_EmailData_Article::getInstance($article, $this->_domain);

		$data['jotw'] = $this->_latestJobs('newsletterjotw');
        $data['jotw'] = empty($data['jotw'])
        	? $data['jotw'] = 0
        	: $data['jotw'][0];

        $data['jobs'] = $this->_latestJobs('newsletter');
        $data['articles'] = $this->_getArticles();
        $data['subject'] = $this->_newsletterSubject($this->_email);
        $data['leaderboard'] = $this->_getLeaderboard();
        $data['links']['register'] = $this->_domain.'user/index/register/journey/register';

        return $data;
    }

    protected function _getLeaderboard()
    {
        $leaderboardArray = 0;

        $url = 'newsletter';
        if ('default' !== $this->_siteName) {
            $url = $this->_siteName.'-'.$url;
        }

        $media = Media_Model_Set::getMapper()->findOneByField(array('url'=>$url));
        if ($media) {
            $leaderboard = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'leaderboard'));
            $leaderboardArray = $leaderboard->toArray();
        }

        return $leaderboardArray;
    }

    /**
     * Generate the newsletter subject line based on the article keywords.
     *
     * @param Mailqueue_Model_Mail $email
     * @return string
     */
    private function _newsletterSubject(Mailqueue_Model_Mail $email)
    {
        return $email->getSubject();
    }

    protected function _formatJob(Job_Model_Job $job){
    	$view = $this->getView();
		$jobArray = $job->toArray();
		$jobArray['location'] = $job->getLocation()?$job->getLocation()->getTitle():'';
		$jobArray['fullUrl'] = $this->_domain.$view->defaultReadUrl($job);
		$jobArray['companylogo'] = $this->_domain.$jobArray['companylogo'];
		return $jobArray;
    }

    /**
     *
     * @return array
     */
    protected function _latestJobs($orderName='spotlight', $count=5)
    {
        if ('default' !== $this->_siteName) {
            $orderKey = $this->_siteName.'_'.$orderName;
        } else {
            $orderKey = $orderName;
        }
        $order = array('ordr_'.$orderKey=>'desc');

        $jobsArray = array();
        $jobs = Job_Model_Job::getMapper()->findAllByField(
            array('published'=>1), $order, array('page'=>1, 'count'=>$count)
        );

        foreach ($jobs as $job) {
            if (!$job->getOrder($orderKey)) {
                break;
            }
            $jobArray = $this->_formatJob($job);
            $jobsArray[] = $jobArray;
        }

        if (!empty($jobsArray)) {
            return $jobsArray;
        } else {
            return 0;
        }
    }
}
