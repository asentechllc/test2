<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * EmailData class for fetching all the data needed in a Newsflash
 *
 * @category Intent
 * @package Intent_EmailData
 */
class Intent_EmailData_Newsflash extends Intent_EmailData_EmailDataAbstract
{

    /**
     * @see Intent_EmailData_EmailDataAbstract::adestraData()
     */
    public function adestraData()
    {
        $data = parent::adestraData();

        $articleMapper = News_Model_Article::getMapper();
        $id = $this->_email->getAttribute('articles');
        if (null !== $id) {
            $article = $articleMapper->findOneByField(array('id'=>$id));
        } else {
            $article = new News_Model_Article();
        }

        $jobs = $this->_getJobs();
        if (count($jobs)) {
            $data['job'] = $jobs[0];
        }

        $data['jobspotlight'] = array();
        $i = 0;
        foreach($jobs as $job){

            if($i == 2)
                break;

            $data['jobspotlight'][] = $job;
            $i++;
        }


        $data['subject'] = $this->_email->getSubject();
        $data['article'] = $this->_processArticle($article);
        $data['adverts'] = $this->_emailAdverts();
        return $data;
    }

}
