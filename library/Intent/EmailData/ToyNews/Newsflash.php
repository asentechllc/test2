<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData_Musicweek
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Dummy subclass to prevent warning on every email generated
 *
 * @category Intent
 * @package Intent_EmailData_Musicweek
 */
class Intent_EmailData_ToyNews_Newsflash extends Intent_EmailData_Newsflash
{
}