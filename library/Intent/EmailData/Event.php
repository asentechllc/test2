<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Factory class for EmailData event types
 *
 * @category Intent
 * @package Intent_EmailData
 */
class Intent_EmailData_Event
{
    static private $_instance;
    protected static $_urlHelper;
    protected $_event=null;
    protected $_domain=null;

    /**
     * Fetch an instance of the event processor based on the event type
     *
     * @param News_Model_Event $event
     * @return Intent_EmailData_Event_EventAbstract
     */
    static public function getInstance(Event_Model_Event $event, $domain)
    {
        if (null===self::$_instance) {
            self::$_instance = new Intent_EmailData_Event();
        }
        self::$_instance->setDomain($domain);
        self::$_instance->setEvent($event);
        return self::$_instance;
    }

    public static function getUrlHelper() {
        if (null===self::$_urlHelper) {
            $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            self::$_urlHelper = $view->getHelper('gossipUrl');
        }
        return self::$_urlHelper;
    }


    public function setEvent(Event_Model_Event $event)
    {
        $this->_event = $event;

        return $this;
    }

    public function setDomain($domain)
    {
        $this->_domain = $domain;

        return $this;
    }

    /**
     * Sanitise an event model and return its email data as an array ready to send
     * to Adestra
     *
     * @return array
     */
    public function eventData()
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $filter = new Mcmr_Filter_Utf8();

        $eventArray = array();
        $eventArray['url']=$this->_event->getUrl();
        $eventArray['fullUrl']=$this->getFullUrl($this->_event);
        $eventArray['title']=$this->_event->getTitle();
        $eventArray['image']=$this->_event->getImage();
        $eventArray['imageLocalPath']=$this->_event->getImage();
        $eventArray['location']=$this->_event->getAttribute('location');
        $eventArray['day'] = Mcmr_StdLib::dateToString('j', $this->_event->getStartdate());
        $eventArray['month'] = Mcmr_StdLib::dateToString('M', $this->_event->getStartdate());
        return $eventArray;
    }

    protected function getFullUrl($event)
    {
        $gossipUrl = self::getUrlHelper()->gossipUrl(array('module'=>'news','controller'=>'index','action'=>'read','model'=>$event), 'events-read', true);
        $fullUrl = $this->_domain.$gossipUrl;
        return $fullUrl;
    }


}
