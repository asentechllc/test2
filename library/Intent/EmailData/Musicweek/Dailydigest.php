<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData_Musicweek
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * EmailData all data for daily digest email specific to MusicWeek.
 * This neede because unlike on all intent sites built so far events on music week are using events module and not news articles of event type.
 *
 * @category Intent
 * @package Intent_EmailData_Musicweek
 */
class Intent_EmailData_Musicweek_Dailydigest extends Intent_EmailData_Dailydigest
{
    public function adestraData()
    {
        $data = parent::adestraData();
        
        $eventOrderBy = 'sidepanel';
        $data['events'] = $this->_latestEvents(3, $eventOrderBy);
        return $data;
    }

}