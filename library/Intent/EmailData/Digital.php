<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * EmailData class for fetching all the data needed in a digital edition
 *
 * @category Intent
 * @package Intent_EmailData
 */

class Intent_EmailData_Digital extends Intent_EmailData_EmailDataAbstract
{
    /**
     * @see Intent_EmailData_EmailDataAbstract::adestraData()
     */
    public function adestraData()
    {
        $data = parent::adestraData();

        
        $type = Product_Model_Type::getMapper()->findOneByField(array('url' => 'digital-edition'));
        $report = Product_Model_Product::getMapper()->findOneByField(array( 'type'=>$type->getId()), array( 'createdate'=>'DESC' ));
        
        $data['subject'] = $this->_email->getSubject();
        $data['latest'] = $this->_sanitiseProduct($report, false);
        $data['adverts'] = $this->_emailAdverts();
        Mcmr_Debug::dump($data);
        return $data;
    }
}
