<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * EmailData class for fetching all the data needed in a Jobalert
 *
 * @category Intent
 * @package Intent_EmailData
 */
class Intent_EmailData_Jobalert extends Intent_EmailData_EmailDataAbstract
{
    
    /**
     * @see Intent_EmailData_EmailDataAbstract::adestraData()
     */
    public function adestraData()
    {
		$view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');

        $data = parent::adestraData();
		
		$data['jobs'] = array();
		
		$type = News_Model_Type::getMapper()->findOneByField(array('url'=>'job'));

		$i = 1;
		while($jobId = $this->_email->getAttribute('job'.$i)){
			$job = Job_Model_Job::getMapper()->find($jobId);
			if($job){
				$article = new News_Model_Article;
				$article->setTypeid($type->getId());
				$article->setAttribute('jobId',$job->getId());
				$article->setUrl($job->getUrl());
				$job = $this->_processArticle($article);
				if(isset($job['job'])){
					$job['job']['image'] = $job['image'];
					$data['jobs'][] = $job['job'];
				}
			}
			$i++;
		}
		
		$data['jobArticles'] = array();
		# get 4 latests articles tagged with jobs
		$category = News_Model_Category::getMapper()->findOneByField(array('url'=>'jobs-in-games'));
		if ($category) {	
			$query = $view->queryBuild('article-newsindex', 'article', 'news')
				->addConditions(array('category'=>$category->getId()))
				->setPage(array('page'=>1, 'count'=>4));
			$articles = $view->queryAll($query,  'article', 'news');
			foreach($articles as $article){
				$data['jobArticles'][] = $this->_processArticle($article);
			}
		}
		
        $data['subject'] = $this->_email->getSubject();
        $data['adverts'] = $this->_emailAdverts();
        return $data;
    }

}
