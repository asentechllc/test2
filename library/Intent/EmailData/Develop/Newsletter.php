<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData_Develop
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Dummy subclass to prevent warning on every email generated
 *
 * @category Intent
 * @package Intent_EmailData_Develop
 */
class Intent_EmailData_Develop_Newsletter extends Intent_EmailData_Newsletter
{
}