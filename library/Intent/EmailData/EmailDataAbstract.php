<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData
 *
 * @category Intent
 * @package Intent_EmailData
 */
abstract class Intent_EmailData_EmailDataAbstract
{
    /**
     * The email object
     *
     * @var Mailqueue_Model_Mail
     */
    protected $_email = null;

    /**
     * The domain all the email links should use
     *
     * @var string
     */
    protected $_domain = null;

    /**
     * The site name for restricting data to specific sub-sites
     *
     * @var string
     */
    protected $_siteName = 'default';

    public function __construct(Mailqueue_Model_Mail $email, $siteName='default')
    {
        $this->_email = $email;
        $this->_siteName = $siteName;
        $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $this->_view = clone $frontView;
        $this->_view->setScriptPath(SITE_PATH . '/views/payment/scripts/');
        $this->_view->addScriptPath(SITE_PATH.'/views/_common');
        $this->_view->addScriptPath(SITE_PATH.'/_common');

    }

    public function getView()
    {
        return $this->_view;
    }

    protected function _getArticlesQueryName() {
        $queryName = 'article-'.$this->_email->getName();
        if ('default' !== $this->_siteName) {
            $queryName = '-'.$this->_siteName;
        }
        return $queryName;
    }

    protected function _getArticlesQuery() {
        $view = $this->getView();
        $queryName = $this->_getArticlesQueryName();
        $query = $view->queryBuild($queryName, 'article', 'news');
        return $query;
    }

    /**
     * Fetch and sanitise news articles
     *
     * @param int $count The number of articles to fetch
     * @param string $orderBy The field to order the articles by
     * @return array
     */
    protected function _getArticles()
    {
        $view = $this->getView();
        $query = $this->_getArticlesQuery();
        $models = $view->queryAll($query, 'article', 'news')->toArray();

        $modelArray = array();
        foreach ($models as $model) {
            $modelArray[] = $this->_processArticle($model);
        }

        return $modelArray;
    }

    protected function _processArticle($model)
    {
        $modelProcessor = Intent_EmailData_Article::getInstance($model, $this->_domain);
        $modelProcessor->setView($this->getView());
        return $modelProcessor->articleDataWithPlain();
    }

    /**
     *
     * @return array
     */
    protected function _getJobs()
    {
        $view = $this->getView();
        $query = 'job-'.$this->_email->getName();
        if ('default' !== $this->_siteName) {
            $query = '-'.$this->_siteName;
        }
        $models = $view->queryAll($query, 'job', 'job')->toArray();
        $modelsArray = array();
        foreach ($models as $model) {
            $modelArray = $model->toArray()->getArrayCopy();
            $modelArray['location'] = $model->getLocation()?$model->getLocation()->getTitle():'';
            $modelArray['fullUrl'] = $this->_domain.$view->defaultReadUrl($model);
            $modelArray['companylogoLocalPath'] = $modelArray['companylogo'];
            $modelArray['companylogo'] = $this->getFullImageUrl($modelArray['companylogo']);
            $modelsArray[] = $modelArray;
        }
        if (!empty($modelsArray)) {
            return $modelsArray;
        } else {
            return 0;
        }
    }

    protected function _sanitiseProduct(Product_Model_Product $product, $summariseDescription=false)
    {
        $productArray = $product->toArray()->getArrayCopy();
        if ($summariseDescription) {
            $productArray['description'] = $view->formatString()->summarise( $product->getDescription(), 10, 1 );
        }
        $productArray['imageLocalPath'] = $productArray['image'];
        $productArray['image'] = $this->getFullImageUrl($productArray['image']);
        return $productArray;
    }


    /**
     *
     * @param string $site
     * @return array
     */
    protected function _emailAdverts()
    {
        $setMapper = Media_Model_Set::getMapper();
        $fileMapper = Media_Model_File::getMapper();
        $pageMapper = Page_Model_Page::getMapper();

        $adverts = array();
        $setUrl = $this->_email->getName();
        $textUrl = $setUrl.'-advertisement';
        if ('default' !== $this->_siteName) {
            $setUrl = $this->_siteName . '-'.$setUrl;
            $textUrl = $this->_siteName . '-' . $textUrl;
        }
        $adverts['textAd'] = $pageMapper->findOneByField(array('url'=>$textUrl));
        if (is_object($adverts['textAd'])) {
            $adverts['textAd'] = $adverts['textAd']->toArray()->getArrayCopy();
        }

        // Get adverts
        $set = $setMapper->findOneByField(array('url'=>$setUrl));
        if ($set) {
            $files = $fileMapper->findAllByField(array('setid'=>$set->getId()));
            foreach ($files as $file ) {
                $advertArray = $file->toArray()->getArrayCopy();
                $advertArray['filename'] = $this->getFullImageUrl($advertArray['filename']);
                $adverts[$file->getUrl()]=$advertArray;
            }
        }
        return $adverts;
    }

    /**
     * Fetch an array of all the data required to send this email through Adestra
     *
     * @return array
     */
    public function adestraData()
    {
        $adestraData = array();
        $adestraData['year'] = date('Y');

        if ('default' !== $this->_siteName) {
            $adestraData['domain'] = Zend_Registry::get('app-config')->media->{$this->_siteName}->baseDomain;
        } else {
            $adestraData['domain'] = Zend_Registry::get('app-config')->media->baseDomain;
        }

        $this->_domain = $adestraData['domain'];

        $adestraData['links'] = $this->_getLinks();
        return $adestraData;
    }

    protected function getFullImageUrl($image)
    {
        if ('http://'!=substr($image, 0, 7)) {
            $image = $this->_domain.$image;
        }
        return $image;
    }


    protected function _getLinks()
    {
        $links = array();
        $config = Zend_Registry::get('mailqueue-config');
        if (isset($config->links)) {
            $links = $config->links->toArray();
        }
        return $links;
    }

}
