<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * EmailData class for fetching all the data needed in a daily digest
 *
 * @category Intent
 * @package Intent_EmailData
 */
class Intent_EmailData_Dailydigest extends Intent_EmailData_EmailDataAbstract
{
    /**
     * @see Intent_EmailData_EmailDataAbstract::adestraData()
     */
    public function adestraData()
    {
        $data = parent::adestraData();
        
        // Determin the order the articles should be displayed in
        $data['articles'] = $this->_getArticles();
        $data['subject'] = $this->_dailySubject($this->_email, $data['articles']);
        $data['adverts'] = $this->_emailAdverts();

        $jobs = $this->_getJobs();
        if (count($jobs)) {
            $data['job'] = $jobs[0];
        }
        
        $data['jobspotlight'] = array();
        $i = 0;
        foreach($jobs as $job){

        	if($i == 2)
        		break;
        		
        	$data['jobspotlight'][] = $job;
        	$i++;
        }
        
        if ('emailtest' !== $this->_email->getOptin()) {
            // Data collected, reset the daily digest article ordering
            $this->_resetArticleOrdering();
        }
        return $data;
    }

    protected function _resetArticleOrdering()
    {
        $mapper = News_Model_Article::getMapper();
        $query = $this->_getArticlesQuery();
        $orders = $query->getOrders()->toArray();
        foreach($orders as $order) {
            if (is_array($order)) {
                $order = $order['key'];
            }
            if ('ordr_'!=substr($order,0,5)) {
                continue;
            }
            $orderName = substr($order,5);
            $mapper->resetOrdering($orderName);
        }
    }
    
    /**
     * Generate the daily digest subject line based on the article keywords.
     *
     * @param Mailqueue_Model_Mail $email
     * @param array $articles
     * @return string
     */
    private function _dailySubject(Mailqueue_Model_Mail $email, array $articles)
    {
        $keywords = array();
        $x = 0;
        $index = 0;
        while ($x<3 && isset($articles[$index])) {
            $article = $articles[$index];
            
            if (!empty ($article['keyword'])) {
                $keywords[] = $article['keyword'];
                $x++;
            }
            $index++;
        }

        $subject = Zend_Registry::get('mailqueue-config')->adestra->{$email->getName()}->subject;
        return str_replace('%keywords%', implode(", ", $keywords), $subject);
    }   

}
