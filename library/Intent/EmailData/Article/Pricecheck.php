<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */

class Intent_EmailData_Article_Pricecheck extends Intent_EmailData_Article_ArticleAbstract
{
    /**
     * @see Intent_EmailData_Article_ArticleAbstract::articleData()
     * @return array
     */
    public function articleData()
    {
        $articleArray = parent::articleData();

        $pricecheck = Pricecheck_Model_Pricecheck::getMapper()->findOneByField(array('id'=>$this->_article->getAttribute('pricecheckId')));
        if (null !== $pricecheck) {
            $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            
            $articleArray['pricecheck'] = $pricecheck->toArray();
            $products = $pricecheck->getProducts();
            $articleArray['products'] = array();
            foreach ($products as $product) {
                $articleArray['products'][] = $product->toArray();
            }
            $game = array_shift( $products );
            if (null !== $game) {
                $articleArray['game'] = $game->toArray();
                $articleArray['game']['platforms'] = $game->getGamePlatforms();
                $bestOnline = $pricecheck->bestOnlinePrice($game->getId(), $game->getAttribute('platformid'));
                if (null !== $bestOnline) {
                    $articleArray['game']['bestOnline'] = $bestOnline->toArray();
                    $articleArray['game']['bestOnline']['price'] = $view->format()->currency($bestOnline->getPrice());
                    $articleArray['game']['bestOnline']['platform'] = $bestOnline->getPlatform()->getTitle();
                }
                $bestStore = $pricecheck->bestStorePrice($game->getId(), $game->getAttribute('platformid'));
                if (null !== $bestStore) {
                    $articleArray['game']['bestStore'] = $bestStore->toArray();
                    $articleArray['game']['bestStore']['price'] = $view->format()->currency($bestStore->getPrice());
                    $articleArray['game']['bestStore']['platform'] = $bestStore->getPlatform()->getTitle();
                }

                $developer = $game->getDeveloper();
                if (null !== $developer) {
                    $articleArray['game']['developer']['title'] = $developer->getTitle();
                }
            }
        }

        $articleArray['fullUrl'] = $this->_domain.'retail-biz/pricecheck';
        $articleArray['typeUrl'] = $this->_domain.'retail-biz/';
        
        return $articleArray;
    }
}
