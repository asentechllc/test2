<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */
class Intent_EmailData_Article_Chart extends Intent_EmailData_Article_ArticleAbstract
{
    /**
     * @see Intent_EmailData_Article_ArticleAbstract::articleData()
     * @return array
     */
    public function articleData()
    {
        $articleArray = parent::articleData();
        
        $chart = Chart_Model_Chart::getMapper()->find( $this->_article->getAttribute('chartId') );
        if (null !== $chart) {
            $platform = $chart->getPlatform();
        }
        $articleArray['chart'] = $chart->toArray();
        $articleArray['chart']['type'] = $chart->getType()->getTitle();

        unset($articleArray['chart']['platformid']);
        unset($articleArray['chart']['attributeTypes']);
        unset($articleArray['chart']['state']);
        unset($articleArray['chart']['article_id']);
        unset($articleArray['chart']['fromdate']);
        unset($articleArray['chart']['todate']);

        $count = 1;
        $articleArray['chart']['entries'] = array();
        foreach( $chart->getEntries() as $entry ) {
            $game = $entry->getGame();
            if (null !== $game) {
                // only show 5 chart entries
                if ($count > 3) break;
                $articleArray['chart']['entries'][] = $game->getTitle();
            }
            $count++;
        }

        $articleArray['fullUrl'] = $this->_domain.'retail-biz/pre-order';
        $articleArray['typeUrl'] = $this->_domain.'retail-biz/';
        
        return $articleArray;
    }
}
