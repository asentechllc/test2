<?php 
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */

class Intent_EmailData_Article_Newgear extends Intent_EmailData_Article_ArticleAbstract
{
    /**
     * @see Intent_EmailData_Article_ArticleAbstract::articleData()
     * @return array
     */
    public function articleData()
    {
        $articleArray = parent::articleData();
        $articleArray['typeUrl'] = $this->_domain.'new-gear';

    	return $articleArray;
    }

}
