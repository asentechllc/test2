<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */

class Intent_EmailData_Article_Feature extends Intent_EmailData_Article_ArticleAbstract
{
    public function articleData()
    {
        $articleArray = parent::articleData();

        $feature = $this->_article->getConnectedModel('Feature_Model_Feature');
        if (null !== $feature) {
            $articleArray['imageLocalPath'] = $feature->getImage();
	        $articleArray['image'] = $this->getFullImageUrl($feature->getImage());
	        $articleArray['authorname'] = $this->formatAuthorFeature($feature);
        }
        return $articleArray;
    }

}
