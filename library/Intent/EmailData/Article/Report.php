<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */

class Intent_EmailData_Article_Report extends Intent_EmailData_Article_ArticleAbstract
{
    /**
     * @see Intent_EmailData_Article_ArticleAbstract::articleData()
     * @return array
     */
    public function articleData()
    {
        $articleArray = parent::articleData();
        $articleArray['typeUrl'] = $this->_domain.'reports';
        
        $report = Product_Model_Product::getMapper()->findOneByField(array('id'=>$this->_article->getAttribute('productId')));
        if (null !== $report) {
            $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            
            $articleArray['report'] = $report->toArray();
            $articleArray['report']['description'] = $view->formatString()->summarise( $report->getDescription(), 20, 1 );
            $articleArray['report']['amount'] = $view->format()->currency($report->getAmount()/100);
            $articleArray['fullUrl'] = $this->_domain.'reports/read/'.$report->getUrl();
        }

        return $articleArray;
    }
}
