<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */

class Intent_EmailData_Article_Job extends Intent_EmailData_Article_ArticleAbstract
{
    /**
     * @see Intent_EmailData_Article_ArticleAbstract::articleData()
     * @return array
     */
    public function articleData()
    {
        $articleArray = parent::articleData();
        $job = Job_Model_Job::getMapper()->findOneByField(array('id'=>$this->_article->getAttribute('jobId')));
        if (null !== $job) {
            $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            $filter = new Mcmr_Filter_Utf8();

            $articleArray['job'] = $job->toArray()->getArrayCopy();
            $articleArray['job']['description'] = $filter->filter(
                $view->formatString()->summarise( $job->getDescription() , 50, 1 )
            );
            $articleArray['job']['fullUrl'] = $this->getFullUrl($job);

            $company = $job->getCompany();
            if (null !== $company) {
                $articleArray['job']['company'] = $company->toArray();
            }
            $location = $job->getLocation();
            if (null !== $location) {
                $articleArray['job']['location'] = $location->getTitle();
            }

            $articleArray['typeUrl'] = $this->_domain.'jobs';
            $articleArray['imageLocalPath'] = $job->getAttribute('companylogo');
            $articleArray['image'] = $this->getFullImageUrl($job->getAttribute('companylogo'));
        }
        return $articleArray;
    }
}
