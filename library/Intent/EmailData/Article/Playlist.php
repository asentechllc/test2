<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */

class Intent_EmailData_Article_Playlist extends Intent_EmailData_Article_ArticleAbstract
{
    /**
     * @see Intent_EmailData_Article_ArticleAbstract::articleData()
     * @return array
     */
    public function articleData()
    {
        $articleArray = parent::articleData();
        
        $playlist = Playlist_Model_Playlist::getMapper()->find($this->_article->getAttribute('playlistId'));
        if (null !== $playlist) {
            $articleArray['playlist'] = $playlist->toArray();
            $articleArray['entries'] = array();
            $count = 0;
            foreach ($playlist->getEntries() as $entry) {
                // Limit the number of entries to 5
                if (3 === $count) {
                    break;
                }
                if (null!==$entry->getTrack()) {
                    $entryData = $entry->getTrack()->toArray();
                    $entryData['position'] = $entry->getPosition();
                    $articleArray['entries'][] = $entryData;
                    $count++;
                }
            }
        }
        
        return $articleArray;
    }
}
