<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Abstract class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */
abstract class Intent_EmailData_Article_ArticleAbstract
{
    /**
     * The article object
     *
     * @var News_Model_Article
     */
    protected static $_urlHelper;
    protected $_article;
    protected $_domain = null;
    protected $_view;

    public function __construct($domain)
    {
        $this->_domain = $domain;
    }

    public function setArticle(News_Model_Article $article)
    {
        $this->_article = $article;
        return $this;
    }

    public function setView($view)
    {
        $this->_view = $view;
    }

    public function getView()
    {
        return $this->_view;
    }


    /**
     * Sanitise an article model and return its email data as an array ready to send
     * to Adestra
     *
     * @return array
     */
    public function articleData()
    {
        $filter = new Mcmr_Filter_Utf8();
        $articleArray = $this->_article->toArray()->getArrayCopy();
        unset($articleArray['createdate']);
        unset($articleArray['published']);
        unset($articleArray['featured']);
        unset($articleArray['userid']);
        unset($articleArray['userIdAuthor']);
        unset($articleArray['authorname']);
        unset($articleArray['authoremail']);
        unset($articleArray['typeid']);
        unset($articleArray['categoryids']);
        unset($articleArray['categorytitles']);
        unset($articleArray['order']);
        unset($articleArray['stream']);
        unset($articleArray['strap']);
        unset($articleArray['sites']);
        unset($articleArray['tags']);
        unset($articleArray['attributeTypes']);
        unset($articleArray['state']);
        if (array_key_exists('anonymous', $articleArray)) {
            unset($articleArray['anonymous']);
        }
        if (array_key_exists('release_id', $articleArray)) {
            unset($articleArray['release_id']);
        }

        $articleArray['authorname'] = $this->formatAuthor($this->_article);
        $articleArray['publishdate'] = $this->formatDate($articleArray['publishdate'], true);
        $articleArray['content'] = strip_tags(
            $this->getView()->formatString()->summarise( $this->_article->getContent(), 20, 1 ),
            "<p><br><i><b><li><ul><ol>"
        );
        $articleArray['plainContent'] =  strip_tags( $this->getView()->formatString()->summarise( $this->_article->getContent(), 20, 1 ));
        $articleArray['fullUrl'] = $this->getFullUrl($this->_article);
        $articleArray['imageLocalPath'] = $articleArray['image'];
        $articleArray['image'] = $this->getFullImageUrl($articleArray['image']);
        $articleArray['type'] = $this->_article->getType()->getUrl();
        $articleArray['typeTitle'] = $this->_article->getType()->getTitle();
        return $filter->filter($articleArray);
    }

    public function articleDataWithPlain()
    {
        $data = $this->articleData();
        $dataWithPlain = $data;
        foreach ($data as $key=>$value) {
            $plainKey = 'plain'.ucfirst($key);
            if (!isset($dataWithPlain[$plainKey])&&is_string($value)) {
                $dataWithPlain[$plainKey]=strip_tags($value);
            }
        }
        return $dataWithPlain;
    }

    protected function getFullUrl($article)
    {
        $fullUrl = $this->_domain.$this->getView()->defaultReadUrl($article);
        return $fullUrl;
    }

    protected function getFullImageUrl($image)
    {
        if ('http://'!=substr($image, 0, 7)) {
            $image = $this->_domain.$image;
        }
        
        # encode spaces if required (gmail struggles otherwise)
        return str_replace(' ','%20',$image);
    }

    protected function formatAuthor($model)
    {
        return $this->getView()->partial('partial/elements/email/author.phtml', array('model'=>$model));
    }

    protected function formatAuthorFeature($model)
    {
        return $this->getView()->partial('partial/elements/email/author-feature.phtml', array('model'=>$model));
    }

    protected function formatDateEvent($startDate, $endDate)
    {
        return $this->getView()->partial('partial/elements/email/date-event.phtml', array('startDate'=>$startDate, 'endDate'=>$endDate));
    }


    protected function formatDate($date, $showTime = true)
    {
        return $this->getView()->partial('partial/elements/email/date.phtml', array('date'=>$date, 'showTime'=>$showTime));
    }

}
