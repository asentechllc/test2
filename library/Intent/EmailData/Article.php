<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Factory class for EmailData article types
 *
 * @category Intent
 * @package Intent_EmailData
 */
class Intent_EmailData_Article
{
    static private $_instances = array();
    
    /**
     * Fetch an instance of the article processor based on the article type
     *
     * @param News_Model_Article $article
     * @return Intent_EmailData_Article_ArticleAbstract 
     */
    static public function getInstance(News_Model_Article $article, $domain)
    {
        $articleType = $article->getType()->getUrl();
        
        if (!isset(self::$_instances[$articleType])) {
            $className = "Intent_EmailData_Article_".ucfirst(str_replace('-', '', $articleType));
            $instance = new $className($domain);
            self::$_instances[$articleType] = $instance;
        }
        
        self::$_instances[$articleType]->setArticle($article);
        
        return self::$_instances[$articleType];
    }
}
