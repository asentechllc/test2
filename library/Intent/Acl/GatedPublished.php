<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of GatedPublished
 *
 * @category Mcmr
 */
class Intent_Acl_GatedPublished implements Zend_Acl_Assert_Interface
{
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null, Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        $published = new News_Acl_Published();
        $gated = new News_Acl_Gated();
        
        return $published->assert($acl, $role, $resource, $privilege)
            && $gated->assert($acl, $role, $resource, $privilege);
    }

}
