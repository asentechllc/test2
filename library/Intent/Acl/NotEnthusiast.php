<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Acl.php 1654 2010-10-27 10:48:37Z leigh $
 */

/**
 * Acl to prevent users marked as 'enthusiast' from commenting.
 *
 * @category Intent
 * @package Intent_Acl
 */
class Intent_Acl_NotEnthusiast implements Zend_Acl_Assert_Interface
{
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null, 
            Zend_Acl_Resource_Interface $resource = null, $privilege = null) 
    {
        $user = Zend_Registry::get('authenticated-user');

        if (null == $user) {
            return false;
        } else {
            return ('enthusiast' !== $user->getAttribute('memtype'));
        }
    }

}
