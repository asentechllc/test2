<?php
class Intent_Cron_NordicIndiaOptin extends Mcmr_Cron_ModuleAbstract
{
    
    public function execute()
    {
        $indiaCount = 0;
        
        $mapper = User_Model_User::getMapper();
        $mapper->setObserversEnabled(false);
        $users = $mapper->findAllByField(NULL, null, array('page'=>1, 'count'=>100000000));
        
        foreach ($users as $user) {
            $emailCountry = strtolower(array_pop(explode('.', $user->getEmail())));
            $userCountry = strtolower($user->getAttribute('country'));
            if ('in' === $emailCountry || 'in' === $userCountry) {
                $user->setAttribute('mcvindia', true);
                $mapper->save($user);
                $indiaCount++;
            }
            
            Mcmr_Cron::phase();
        }
        
        Mcmr_Cron::clearPhase();
        echo "{$indiaCount} users added tp India Opt In\n";
    }
}
