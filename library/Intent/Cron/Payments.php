<?php

/*
	Accepts date range + time arguments in format:
	yyyy-mm-dd:yyyy-mm-dd hh:mm
*/

class Intent_Cron_Payments extends Mcmr_Cron_ModuleAbstract
{
    public function  execute()
    {
        print "Generating Payment CSV\n";
        // Eu 1234,56****
        setlocale(LC_MONETARY, 'en_GB');

        $mapper = Payment_Model_Payment::getMapper();
        $companyMapper = Company_Model_Company::getMapper();
        $startDate = strtotime($this->_config->payments->startDate);
        $endDate = null;
        $emailOverride = null;
        
        # check arguments for date range
        if(preg_match('/(^| *)([0-9]{4}-[0-9]{2}-[0-9]{2}):([0-9]{4}-[0-9]{2}-[0-9]{2}) ([0-9]{2}:[0-9]{2})( *|$)/',$this->_args,$m)){
        	$startDate = strtotime($m[2].' '.$m[4].':00');
        	$endDate = strtotime($m[3].' '.$m[4].':00');
        	print "Start date: ".date('r',$startDate)."\n";
        	print "End date: ".date('r',$endDate)."\n";
        }
        
        # check for email override
        if(preg_match('/(^| *)([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})( *|$)/i',$this->_args,$m)){
        	$emailOverride = $m[2];
        	print "Recipient Override: {$emailOverride}\n";
        }

		$conditions = array(
			'paymentdate'=>array('value'=>$startDate, 'condition'=>'>='),
        	'status'=>Mcmr_Payment_AdapterAbstract::STATUS_OK
        );

        if($endDate){
        	$conditions['paymentdate'] = array(
        		'value' => array($startDate,$endDate),
        		'condition' => 'between',
        	);
        }

        $payments = $mapper->findAllByField(
			$conditions,
            null,
            array('page'=>1, 'count'=>1000000000)
        );

        $prefix = '';
        if (isset($this->_config->payments->idPrefix)) {
            $prefix = $this->_config->payments->idPrefix;
        }

        $site = '';
        if (isset($this->_config->payments->site)) {
            $site = $this->_config->payments->site;
        }
        
        // Generate the payments CSV
        $paymentCsv = array();
        foreach ($payments as $payment) {
            $user = $payment->getBasket()->getUser();
            $company = null;
            $address = $payment->getBasket()->getAddress('billing');
            if (null !== $user && null !== $user->getAttribute('companyid')) {
                $company = $companyMapper->find($user->getAttribute('companyid'));
            }
            if (null !== $company) {
                $companyTitle = $company->getTitle();
                $companyWebsite = $company->getAttribute('website');
                $companyPhone = $company->getAttribute('phone');
            } else {
                $companyTitle = "";
                $companyWebsite = "";
                $companyPhone = "";
            }

            $includeTax = !$payment->getTaxExempt();
                
            $items = $payment->getBasket()->getItems();
            foreach ($items as $item) {
                $model = $item->getModel();
                if (null === $model) {
                    $type = 'unknown';
                    $model = new Product_Model_Product();
                } elseif ($model instanceof Job_Model_Job) {
                    $type = 'job';
                } else {
                    $type = $model->getType()->getUrl();
                }

                $firstName='';
                $lastName='';
                if (null!==$user) { 
                    $firstName=$user->getFirstname();
                    $lastName=$user->getSurname();
                } else {
                    $addresses = $payment->getBasket()->getAddresses();
                    foreach ($addresses as $address) {
                        $firstName =  $address->firstname;
                        $lastName = $address->surname;
                        break;
                    } 
                }
                $row = array(
                    $prefix.$payment->getId(),
                    $payment->getTxid(),
                    $item->getDescription(),
                    $firstName,
                    $lastName,
                    $address->getCompany(),
                    $companyWebsite,
                    $companyPhone,
                    $address->getAddress1(),
                    $address->getAddress2(),
                    $address->getPostcode(),
                    $address->getCountry(),
                    $site,
                    money_format("%.2n", $item->getAmount(false)/100), // Ex-tax
                    money_format("%.2n", $item->getAmount($includeTax)/100), // Inc-tax 
                    $payment->getTaxExempt(),
                );

                if ($model instanceof Job_Model_Job) {
                    $row[] = date('d/m/Y', $model->getDate());
                    $row[] = date('d/m/Y', $model->getExpiredate());
                }

                if (!isset($paymentCsv[$type])) {
                    $paymentCsv[$type] = array(
                        array(
                            'id',
                            'Sage ID',
                            'Product',
                            'Forename',
                            'Surname',
                            'Company',
                            'Website',
                            'Telephone',
                            'Address 1',
                            'Address 2',
                            'Postcode',
                            'Country',
                            'Site',
                            'Cost (ex VAT)',
                            'Cost (inc VAT)',
                            'Start Date',
                            'End Date',
                            'Tax Exempt',
                        )
                    );
                }

                $paymentCsv[$type][] = $row;
            }
        }
        
        // Generate the email template
        $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view = clone $frontView;
        $view->setScriptPath(SITE_PATH . '/views/payment/scripts/');
        $view->addScriptPath(SITE_PATH.'/views/_common');
        $view->addScriptPath(SITE_PATH.'/_common');
        $html = $view->render($this->_config->email->template->html);
        $plain = $view->render($this->_config->email->template->plain);

        // Send the CSV
        foreach ($paymentCsv as $key => $csvArray) {
            if (is_object($this->_config->email->$key)) {
                $csv = Mcmr_StdLib::arrayToCsv($csvArray);

                $to = $emailOverride ? $emailOverride : $this->_config->email->$key->to->toArray();
                $filename = date('Y-m-d').'_'.$key.'_payments.csv';

                $email = new Mcmr_Mail();
                $email->addTo($to);
                $email->setSubject($this->_config->email->$key->subject);
                $email->setBodyHtml($html);
                $email->setBodyText($plain);
                $attachment = $email->createAttachment($csv);
                $attachment->filename = $filename;

                echo "{$filename} sent\n";
                print_r($to);
                $email->send();
            }
        }

        print "Done\n";
    }
}
