<?php

class Intent_Cron_MwChart_Service
{
    private $_serviceUrl = null;
    
    /**
     *
     * @var Zend_Http_Client 
     */
    private $_client = null;
    
    public function __construct($serviceUrl)
    {
        $this->_serviceUrl = $serviceUrl;
    }
    
    public function __call($name, $arguments)
    {
        $client = $this->_getClient();
        
        $uri = $this->_serviceUrl.$name.'.aspx';
        Mcmr_Debug::dump($uri);
        $client->setUri($uri);
        
        foreach ($arguments as $name=>$value) {
            $client->setParameterPost($name, $value);
        }
        
        Mcmr_Debug::dump($arguments);
        $response = $client->request('POST');
        Mcmr_Debug::dump($client->getLastRequest());
        Mcmr_Debug::dump($response->getBody());
        return $response;
    }
    
    /**
     *
     * @return Zend_Http_Client 
     */
    protected function _getClient()
    {
        if (null === $this->_client) {
            $this->_client = new Zend_Http_Client();
        }
        
        $this->_client->setMethod(Zend_Http_Client::GET);
        $this->_client->resetParameters();
        
        return $this->_client;
    }
}
