<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron script to clear locked out usest
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Intent_Cron_MwLockouts extends Mcmr_Cron_ModuleAbstract
{
    public function execute()
    {
        $cdsGatekeeper = new Intent_Observer_CdsGatekeeper('User_Model_UserMapper');
        $userMapper = User_Model_User::getMapper();
        $users = $userMapper->findAllByField(array('attr_loggedInCount'=>array( 'condition'=>'>', 'value'=>'0')), null, array('count'=>1000, 'page'=>1));
        $userCount = 0;
        $pageCount = $users->count();
        echo "$pageCount pages found\n";
        for ($page=1; $page<=$pageCount; $page++) {
            $users->setCurrentPageNumber($page);
            foreach ($users as $user) {
                $userCount++;
                $user->setAttribute( 'cdsLastLogin', 0);
                $user->setAttribute( 'loggedInCount', 0);
                $userMapper->save($user);
            }
            Mcmr_Cron::clearPhase();
            echo "Page {$page} complete\n";
        }
        
        Mcmr_Cron::clearPhase();
        echo "$userCount users checked\n";

    }
}