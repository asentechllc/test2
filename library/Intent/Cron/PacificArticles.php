<?php

class Intent_Cron_PacificArticles extends Mcmr_Cron_ModuleAbstract
{

    public function execute()
    {
        $mapper = News_Model_Category::getMapper();
        $articleMapper = News_Model_Article::getMapper();
        $category = $mapper->findOneByField(
            array('url'=>'mcv-pacific')
        );
        $articles = $category->getArticles(null, null, array('page'=>1, 'count'=>100000));
        $count = 0;
        foreach ($articles as $article) {
            $article->setSites(array('default', 'mcvpacific'));
            $articleMapper->save($article);
            
            $count++;
            Mcmr_Cron::phase();
        }

        Mcmr_Cron::clearPhase();
        echo "{$count} mcv-pacific articles copied to MCV Pacific\n";
    }

}
