<?php

class Intent_Cron_AdultArticles extends Mcmr_Cron_ModuleAbstract
{

    public function execute()
    {
        $mapper = News_Model_Category::getMapper();
        $articleMapper = News_Model_Article::getMapper();
        $category = $mapper->findOneByField(
            array('url'=>'adult')
        );
        $type = News_Model_Type::getMapper()->findOneByField(array('url'=>'adult'));
        
        $articles = $category->getArticles(null, null, array('page'=>1, 'count'=>100000));
        $count = 0;
        foreach ($articles as $article) {
            $article->setTypeid($type->getId());
            $articleMapper->save($article);
            
            $count++;
            Mcmr_Cron::phase();
        }

        Mcmr_Cron::clearPhase();
        echo "{$count} articles moved to 'adult'\n";
    }
}
