<?php

class Intent_Cron_JobResend extends Mcmr_Cron_ModuleAbstract
{
    
    public function  execute()
    {
        $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view = clone $frontView;
        $view->setScriptPath(SITE_PATH . '/views/job/scripts/');
        $view->addScriptPath(SITE_PATH.'/views/_common');

        $emailValidator = new Zend_Validate_EmailAddress();

        $mapper = Job_Model_Job::getMapper();
        // Fetch all jobs created by Abby
        $jobs = $mapper->findAllByField(
            array(
                'user'=>20, 
                'date'=>array('condition'=>'>=', 'value'=>strtotime('2011-1-1'))
            ),
            null,
            array('page'=>1, 'count'=>1000000)
        );

        $x = 0;
        foreach ($jobs as $job) {
            if ($emailValidator->isValid($job->getAttribute('externalUrl'))) {
                $this->_sendEmail($view, $job);
                $x++;
            }
        }
    }

    private function _sendEmail($view, $job)
    {
        $applications = $job->getApplications(array('page'=>1, 'count'=>100000));
        if (count($applications) <= 0) {
            return;
        }

        $view->applications = $applications;

        $emailaddress = $job->getAttribute('externalUrl');
        $validator = new Zend_Validate_EmailAddress();
        if ($validator->isValid($emailaddress)) {
            $body = $view->render('email/job-application-resend.phtml');
            
            $email = new Mcmr_Mail();
            $email->setBodyHtml($body);
            $email->setSubject("Job Applications for your Job Listing on BikeBiz");
            $email->addTo($emailaddress);

            foreach ($applications as $application) {
                if ($application->getFile()) {
                    $filename = SITE_PATH . '/public/' . $application->getFile();
                    $file = file_get_contents($filename);
                    $attachment = $email->createAttachment($file);
                    $attachment->filename = basename($application->getFile());
                }
            }

            $email->send();
            echo $job->getId()."\n";
            echo $emailaddress."\n";
        }
    }
}
