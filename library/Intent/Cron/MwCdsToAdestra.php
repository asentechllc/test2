<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron script to sync cds subscription status with adestra website opt in
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Intent_Cron_MwCdsToAdestra extends Mcmr_Cron_ModuleAbstract
{
    public function execute()
    {
        $config = Zend_Registry::get('user-config');
        $optIn = $config->adestra->lists->musicweek->optIn;
        $listId = $config->adestra->lists->musicweek->id;
        $unsubId = $config->adestra->lists->musicweek->unsubId;
        $validRole = $config->cdsgatekeeper->validRole;
        $invalidRole = $config->cdsgatekeeper->invalidRole;
        $userMapper = User_Model_User::getMapper();
        $users = $userMapper->findAllByField(
            array(
                'role'=>array('condition'=>'IN', 'value'=>array($validRole, $invalidRole)),
                'attr_cdsLastCheck'=>array('condition'=>'>=', 'value'=>strtotime('-1 day 00:00:00'))
            ),
            array('id'=>'ASC'), array('count'=>1000, 'page'=>1));
        $userCount = 0;
        $pageCount = $users->count();
        $validator = new Zend_Validate_EmailAddress();
        echo "$pageCount pages found\n";
        for ($page=1; $page<=$pageCount; $page++) {
            $users->setCurrentPageNumber($page);
            foreach ($users as $user) {
                echo $user->getUsername().": ".$user->getRole()."\n";
                if(!$validator->isValid($user->getEmail())) {
                    continue;
                }
                if ($user->getRole()==$validRole) {
                    echo("opting in\n");
                    $user->setAttribute($optIn,1);
                    $userMapper->save($user);
                } elseif ($user->getRole()==$invalidRole) {
                    echo("opting out\n");
                    $user->setAttribute($optIn,0);
                    $userMapper->save($user);

                }
                $userCount++;
                Mcmr_Cron::phase();
            }
            Mcmr_Cron::clearPhase();
            echo "Page {$page} complete\n";
        }
        
        Mcmr_Cron::clearPhase();
        echo "$userCount users checked\n";

    }
}