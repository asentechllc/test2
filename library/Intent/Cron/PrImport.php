<?php
/**
 * Script to import the Press Releases from the old intent site.
 */
class Intent_Cron_PrImport extends Mcmr_Cron_ModuleAbstract
{
    public function execute()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $multiDb = $bootstrap->getResource('multidb');
        $db = $multiDb->getDb('intent');
        
        $articleMapper = News_Model_Article::getMapper();
        $pressReleaseType = News_Model_Type::getMapper()->findOneByField(array('url'=>'press-release'));
        
        $importCount = 0;
        $pressReleases = $db->fetchAll('SELECT *, UNIX_TIMESTAMP(prrl_datetime) AS prrl_datetime FROM intent_live_press_releases');
        foreach ($pressReleases as $pressReleaseRow) {
            $article = new News_Model_Article();
            $article->setTypeid($pressReleaseType->getId())
                    ->setTitle($pressReleaseRow['prrl_name'])
                    ->setPublishdate($pressReleaseRow['prrl_datetime'])
                    ->setIntro($pressReleaseRow['prrl_teaser'])
                    ->setContent($pressReleaseRow['prrl_body'])
                    ->setAttribute('prrl_id', $pressReleaseRow['prrl_id'])
                    ->setAttribute('prrl_section', $pressReleaseRow['prrl_section'])
                    ->setPublished(true)
                    ->setUserid(1);
            
            $articleMapper->save($article);
            Mcmr_Cron::phase();
            $importCount++;
        }
        
        Mcmr_Cron::clearPhase();
        print "$importCount press releases imported\n";
    }
}