<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron script to fetch charts for MusicWeek
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Intent_Cron_MwCharts2 extends Mcmr_Cron_ModuleAbstract
{
    protected $_client = null;
    protected $_guid = null;
    
    public function init()
    {
        if (null === $this->_guid) {
            $config = $this->getConfig('mwchart');
            if (isset($config->options->guid)) {
                $this->_guid = $config->options->guid;
            } else {
                throw new Zend_Exception("Cannot create soap connection. GUID not configured");
            }
        }
    }
    
    public function execute()
    {
        $client = $this->_getClient();
        $data = $this->_requestData('RequestChartDefinitions');
        $this->_processDefinitions($data->ChartDefinition);
        
        print "Charts imported\n";
    }
    
    
    protected function _processDefinitions($definitions)
    {
        $params = array(
            'FromDate'=>"09/01/2012 00:00:00",
            'ToDate'=>"09/20/2012 23:59:59",
        );
        
        foreach ($definitions as $definition) {
            $type = $this->_getType($definition);
            //Mcmr_Debug::dump( "process definition for id: ".$definition['id'].", from: ".$params['FromDate'].", to: ".$params['ToDate']);
            $params['ChartIdentifier'] = (int)$definition['id'];
            $charts = $this->_requestData('RequestChartsForPeriod', $params);
            foreach ($charts as $chart) {
                $this->_getChart($type, $chart);
            }
        }
    }

    protected function _getType($typeData)
    {
        $type = Chart_Model_Type::getMapper()->findOneByField(array('attr_definition_id'=>(int)$typeData['id']));
        if (null === $type) {
            $type = new Chart_Model_Type();
            $type->setAttribute('definition_id', (int)$typeData['id']);
            $type->setAttribute('PubDayOfWeek', (string)$typeData->PubDayOfWeek);
            $type->setAttribute('HasMidweek', (string)$typeData->HasMidweek);
            $type->setTitle((string)$typeData->Name);
            
            Chart_Model_Type::getMapper()->save($type);
        }
        
        return $type;
    }

    protected function _getChart($type, $chartData)
    {
        $mapper = Chart_Model_Chart::getMapper();
        $mapper->setObserversEnabled(false);
        $chart = $mapper->findOneByField(array('attr_mwchart_id'=>$chartData['id']));
        if (null === $chart) {
            $chart = new Chart_Model_Chart();
            $chart->setTypeid($type->getId());
            $chart->setTitle($type->getTitle());
            $chart->setAttribute('dirty', true);
            $chart->setAttribute('mwchart_id', (int)$chartData['id']);
            $chart->setAttribute('weeknumber', (int)$chartData['weeknumber']);
            $chart->setDate(strtotime($chartData['pubdate']));
            
            $mapper->save($chart);
        }
        
        return $chart;
    }
    
    protected function _getTrack($trackData)
    {
        $trackMapper = Music_Model_Track::getMapper();
        $track = $trackMapper->findOneByField(array('attr_NewReleaseID'=>(int)$trackData->NewReleaseID));
        if (null === $track) {
            $track = new Music_Model_Track();
        }
        
        $track->setTitle((string)$trackData->TitleName);
        $track->setArtist((string)$trackData->ArtistName);
        $track->setLabel((string)$trackData->RecordLabelName);
        $track->setReleaseDate(strtotime((string)$trackData->ReleaseDate));
        $track->setAttribute('NewReleaseID', (int)$trackData->NewReleaseID);
        $track->setAttribute('MWLiveNewReleaseID', (int)$trackData);
        $track->setAttribute('IsKeyRelease', ('true' === (string)$trackData));
        $track->setAttribute('IsReissuedCatalogue', (string)$trackData);
        $track->setAttribute('IsAlternateFormat', ('true' === (string)$trackData));
        $track->setAttribute('IsSingleOrAlbumOfTheWeek', ('true' === (string)$trackData));
        $track->setAttribute('IsReviewedInMusicWeek', ('true' === (string)$trackData->IsReviewedInMusicWeek));
        $track->setAttribute('DisplayName', (string)$trackData);
        $track->setAttribute('EarliestReleaseDate', strtotime((string)$trackData->EarliestReleaseDate));
        $track->setAttribute('ArtistID', (int)$trackData->ArtistID);
        $track->setAttribute('TitleID', (int)$trackData->TitleID);
        $track->setAttribute('RecordLabelID', (int)$trackData->RecordLabelID);
        $track->setAttribute('ProductTypeID', (int)$trackData->ProductTypeID);
        
        if (2 === (int)$trackData->ProductTypeID) {
            $track->setType('album');
        } elseif (1 === (int)$trackData->ProductTypeID) {
            $track->setType('single');
        }
        
        $trackMapper->save($track);
        
        return $track;
    }
    
    protected function _getClient()
    {
        if (null === $this->_client) {
            $config = $this->getConfig('mwchart');
            $this->_client = new Intent_Cron_MwChart_Service($config->options->restUrl);
        }
        
        return $this->_client;
    }
    
    protected function _requestData($function, array $params=array())
    {
        $client = $this->_getClient();
        $params['guid'] = $this->_guid;
        $response = $client->__call($function, $params);
        
        try {
            $data = new SimpleXMLElement($response->getBody());
        } catch (Exception $e) {
            Mcmr_Debug::dump($params);
            Mcmr_Debug::dump($response);
        }
        
        return $data;
    }
}
