<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron script to fix missing/invalid BitLy links
 *
 * @category 
 * @package 
 * @subpackage 
 */
class  Intent_Cron_FixBitLy extends Mcmr_Cron_ModuleAbstract
{

	const	rate_limit_exceeded = 'RATE_LIMIT_EXCEEDED';

	protected $_bitLyService = null;

    public function execute()
    {
        echo "Fixing articles\n";
        $errorShort = false;

        $mapper = News_Model_Article::getMapper();
        $mapper->setObserversEnabled(false);

		$count = 0;
        
        foreach(array('',self::rate_limit_exceeded) as $value){
		
			$models = $mapper->findAllByField(array(
				'attr_bitLyUrl'=>$value,
			), null, array('page'=>1, 'count'=>-1));
			
			$total = $models->getTotalItemCount();

			foreach ($models as $model) {
			
				//Mcmr_Cron::phase();
				$this->_phase($count,$count.'/'.$total);
				$url = $this->_getAbsoluteUrl($model);
				$bitLy = $this->_getBitLyService();
				$short = $bitLy->shorten($url); 
				
				if(substr(strtolower($short),0,7) !== 'http://'){
					$errorShort = $short;
					break;
				}
					
				$model->setAttribute('bitLyUrl', $short);
				$model->getMapper()->save($model);
				$count++;
			
			}
			
			if($errorShort)
				break;
		}
        
        Mcmr_Cron::clearPhase();

        echo "Finished\n{$count} URLs fixed\n";
        
        if($errorShort)
        	echo "\nAborted due to invalid short URL: $errorShort\nTry again in a short while to avoid exceeding rate limit\n\n";
    }

    protected function _getAbsoluteUrl($model) {
        $baseDomain = Zend_Registry::get('app-config')->media->baseDomain;
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $relativeUrl = $view->defaultReadUrl($model);
        if ('/'==substr($baseDomain,-1)&&'/'==substr($relativeUrl,0,1)) {
            $baseDomain = substr($baseDomain, 0, -1);
        }
        $url = $baseDomain.$relativeUrl;
        return $url;
    }
    protected function _getBitLyService() {
    	if (null==$this->_bitLyService) {
    		$config = Zend_Registry::get('default-config');
    		$accessToken = $config->shorturl->bitly->accessToken;
			$this->_bitLyService = new Mcmr_Service_ShortUrl_BitLy($accessToken);
    	}
    	return $this->_bitLyService;
    }
    protected function _phase($count,$message='')
    {
        $phases = array("|", "/", "-", "\\");
        $s = $phases[$count%4].($message ? ' '.$message : '');

        printf('%s%s', $s, str_repeat(chr(8),strlen($s)));
    }
}
