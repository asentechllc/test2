<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of ListDeDupe
 *
 * @category Mcmr
 */
class Intent_Cron_ListDeDupe extends Mcmr_Cron_ModuleAbstract
{
    
    public function execute()
    {
        $this->mwNonSubscribers();
    }
        
    public function deDupe()
    {
        $csvFiles = array('mobile-ent.csv'=>3, 'pcr.csv'=>3, 'develop-user-export.csv'=>2, 'licensing-user-export.csv'=>2);
        $dedupe = array();
        
        foreach ($csvFiles as $fileName=>$field) {
            if (false !== ($handle = fopen($fileName, "r"))) {
                $outfile = 'dedupe-'.$fileName;
                if (false !== ($outHandle = fopen($outfile, 'w'))) {
                    while (false !== ($data = fgetcsv($handle, 1000, ","))) {
                        if (isset($data[$field])) {
                            $emailAddress = $data[$field];
                            if (!isset($dedupe[$emailAddress])) {
                                fwrite($outHandle, $emailAddress."\n");
                            }
                            $dedupe[$emailAddress] = true;
                            Mcmr_Cron::phase();
                        }
                    }

                    fclose($outHandle);
                }
            }
        }
        
        Mcmr_Cron::clearPhase();
    }
    
    public function mwNonSubscribers()
    {
        $subscriberFile = 'mw-digi-data-13-07-2012.csv';
        $mailingListFile = 'mwdaily.csv';
        $outfile = 'list.csv';
        
        if (false !== ($handle = fopen($subscriberFile, "r"))) {
            $validator = new Zend_Validate_EmailAddress();
            $filter = new Zend_Filter();
            $filter->addFilter(new Zend_Filter_StringTrim());
            $filter->addFilter(new Zend_Filter_StringToLower);
            
            $subscribers = array();
            while (false !== ($data = fgetcsv($handle, 1000, ","))) {
                if (isset($data[12]) && $validator->isValid($data[12])) {
                    $email = $filter->filter($data[12]);
                    $subscribers[$email] = 1;
                }
                
                Mcmr_Cron::phase();
            }
            fclose($handle);
            
            if (false !== ($handle = fopen($mailingListFile, "r"))) {
                if (false !== ($outHandle = fopen($outfile, 'w'))) {
                    while (false !== ($data = fgetcsv($handle, 1000, ","))) {
                        $email = $filter->filter($data[0]);
                        if (!isset($subscribers[$email])) {
                            fwrite($outHandle, $email."\n");
                        }
                        
                        Mcmr_Cron::phase();
                    }
                }
            }
            
            Mcmr_Cron::clearPhase();
        }
        
        echo "Done\n";
    }
}
