<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Script to generate Disqus URL Map CSV
 *
 * @category 
 * @package 
 * @subpackage 
 */
class  Intent_Cron_DisqusCsv extends Mcmr_Cron_ModuleAbstract
{
    public function execute()
    {
    
    	$mapper = News_Model_Article::getMapper();
    	$db = $mapper->getDbTable()->getAdapter();
    	$cfg = Zend_Registry::get('app-config');
    	
    	$out = fopen("php://output","w");
    	    	
    	// news URLs - attr_allowComments = Disqus
    	$view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
    	$rows = $db->query("SELECT * FROM news_articles_attr WHERE attr_name='allowComments' AND attr_value='Disqus' ORDER BY article_id DESC");
    	while($row = $rows->fetch()){
    		$model = $mapper->find($row['article_id']);
    		
    		if(!$model || !$model->getAttribute('v2_id'))
    			continue;
    			
    		$type = $model->getType();
    			
    		fputcsv($out,array(
    			$cfg->media->baseDomain.$type->getUrl().'/read/'.$model->getUrl().'/'.Mcmr_StdLib::seoId($model->getAttribute('v2_id')),
    			substr($cfg->media->baseDomain,0,-1).$view->defaultReadUrl($model),
    		));
    	}

    	exit;
    	
    }    
}
