<?php

class Intent_Cron_Dailydigest extends Mcmr_Cron_ModuleAbstract
{
    protected $_view;

    public function execute()
    {
        $config = $this->_config->dailydigest;
        
        print "Creating Daily Digest email\n";

        $this->_view = $this->_getView();
        $from = strtotime($config->timePeriod);
        $query = $this->_view->queryBuild($config->query, 'article', 'news');
        $query->addConditions(array('publishdate'=>array('condition'=>'>=', 'value'=>$from)));
        $articles = $this->_view->queryAll($query, 'article', 'news');
        echo "{$articles->getCurrentItemCount()} articles found\n";
        $threshold = $this->_config->sendThreshold;
        if ($threshold <= $articles->getCurrentItemCount()) {
            $email = new Mailqueue_Model_Mail();
            $email->setOptions($config->toArray());

            // Set the subject
            $this->_emailSubject($email, $articles);

            // Set the send time
            $email->setSendtime(strtotime($config->sendtime));

            $mapper = Mailqueue_Model_Mail::getMapper();
            $mapper->save($email);

            print "Daily Digest created with a 'pending' status\n";
        } else {
            print "Not enough articles to create Daily Digest\n";
        }
    }

    protected function _getView() 
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        return $view;
    }

    private function _emailSubject(Mailqueue_Model_Mail $email, $articles)
    {
        $keywords = array();
        foreach ($articles as $article) {
            $keyword = $article->getAttribute('keyword');
            if (!empty ($keyword)) {
                $keywords[] = $keyword;
            }
        }

        $email->setSubject(substr(str_replace('%keywords%', implode(", ", $keywords), $email->getSubject()), 0, 90));
    }
}
