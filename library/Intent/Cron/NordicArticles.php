<?php

class Intent_Cron_NordicArticles extends Mcmr_Cron_ModuleAbstract
{

    public function execute()
    {
        $mapper = News_Model_Category::getMapper();
        $articleMapper = News_Model_Article::getMapper();
        $category = $mapper->findOneByField(
            array('url'=>'mcv-nordic')
        );
        $articles = $category->getArticles(null, null, array('page'=>1, 'count'=>100000));
        $count = 0;
        foreach ($articles as $article) {
            $article->setSites(array('default', 'mcvnordic'));
            $articleMapper->save($article);
            
            $count++;
            Mcmr_Cron::phase();
        }

        Mcmr_Cron::clearPhase();
        echo "{$count} mcv-nordic articles copied to MCV Nordic";
    }

}
