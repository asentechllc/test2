<?php

class Intent_Cron_TagCleanup extends Mcmr_Cron_ModuleAbstract
{

    public function execute()
    {
        $mapper = News_Model_Category::getMapper();
        $articleMapper = News_Model_Article::getMapper();

        $duplicates = 0;
        $deleted = 0;
        $previous = '';
        $previousId = -1;
        $categories = $mapper->findAllByField(
            null, array('title' => 'asc', 'url' => 'asc'), array('page' => 1, 'count' => 1000000)
        );
        foreach ($categories as $category) {
            // Re-save to sanitise values. Ignore errors
            $mapper->save($category);
            
            Mcmr_Cron::phase();
        }
        Mcmr_Cron::clearPhase();
        
        $categories = $mapper->findAllByField(
            null, array('title' => 'asc', 'url' => 'asc'), array('page' => 1, 'count' => 1000000)
        );
        foreach ($categories as $category) {
            if ($previous == Mcmr_StdLib::urlize($category->getTitle())) {
                // This category is a duplicate of the previous category
                $articles = $category->getArticles(null, null, array('page' => 1, 'count' => 10000));
                Mcmr_Cron::clearPhase();
                echo "$previous == " . $category->getTitle() . " (" . $category->getUrl() . "): "
                . $articles->getCurrentItemCount() . "\n";

                foreach ($articles as $article) {
                    $ids = $article->getCategoryids();
                    unset($ids[$category->getId()]);
                    $ids[$previousId] = $previousId;
                    $article->setCategoryids($ids);

                    // Save the re-tagged article
                    $articleMapper->save($article);
                }

                // Delete the duplicate tag
                $mapper->delete($category);
                $duplicates++;
            } else {
                // Not a duplicate. Progress to the next tag
                $previous = Mcmr_StdLib::urlize($category->getTitle());
                $previousId = $category->getId();

                // Check it has articles attached
                $articles = $category->getArticles(null, null, array('page' => 1, 'count' => 1));
                if (0 == $articles->getCurrentItemCount()) {
                    Mcmr_Cron::clearPhase();
                    echo "Delete {$category->getTitle()}\n";
                    $mapper->delete($category);
                    $deleted++;
                }
            }

            Mcmr_Cron::phase();
        }

        Mcmr_Cron::clearPhase();
        echo "$duplicates Duplicate tags removed. $deleted empty tags removed\n";
    }

}
