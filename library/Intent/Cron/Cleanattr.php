<?php

class Intent_Cron_Cleanattr extends Mcmr_Cron_ModuleAbstract
{
    public function execute()
    {
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $resource = $bootstrap->getPluginResource('db');
        $db = $resource->getDbAdapter();
        $db->setFetchMode(Zend_Db::FETCH_ASSOC);

        $table = new Company_Model_DbTable_CompanyAttr();
        $this->_cleanTable($table, 'company_id');

        $table = new User_Model_DbTable_UserAttr();
        $this->_cleanTable($table, 'user_id');

        $table = new News_Model_DbTable_ArticleAttr();
        $this->_cleanTable($table, 'article_id');

        Mcmr_Cron::clearPhase();
    }

    private function _cleanTable($table, $primaryKey)
    {
        $rows = $table->fetchAll();
        //$sql = "SELECT * FROM `company_company_attr` ORDER BY `company_company_attr`.`company_id` ASC";

        foreach ($rows as $row) {
            Mcmr_Cron::phase();
            $row = $row->toArray();
            $tValue = @unserialize($row['attr_value']);
            if (false !== $tValue && !is_array($tValue) && !is_object($tValue)) {
                $row['attr_value'] = $tValue;
                $table->update($row, "{$primaryKey}={$row[$primaryKey]} and attr_name='{$row['attr_name']}'");
            }
        }
    }
}
