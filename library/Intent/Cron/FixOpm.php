<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron script to fetch jobs for OPM
 *
 * @category 
 * @package 
 * @subpackage 
 */
class  Intent_Cron_FixOpm extends Mcmr_Cron_ModuleAbstract
{
    /**
     * Number of days to set expiry to if 'date' is set to 0
     */
    const EXPIRE_DAYS = "+90 days";
    const OPM_COMPANY_ID = 226;

    public function execute()
    {
        echo "Fixing jobs\n";

        $validator = new Zend_Validate_EmailAddress();
        
        $mapper = Job_Model_Job::getMapper();
        $mapper->setObserversEnabled(false);
        $jobs = $mapper->findAllByField(array('companyid'=>self::OPM_COMPANY_ID), null, array('page'=>1, 'count'=>10000));
        $count = 0;
        foreach ($jobs as $job) {
            Mcmr_Cron::phase();
            
            $address = $job->getAttribute('externalUrl');
            if ($validator->isValid($address)) {
                $job->setAttribute('howtoapplymethod', 'email');
                
                try {
                    $mapper->save($job);
                    $count++;
                } catch (Exception $e) {
                    Mcmr_Cron::clearPhase();
                    echo $e->getMessage()."\n";
                }
            } else {
                echo $address . ' is not an email'."\n";
            }
        }
        
        Mcmr_Cron::clearPhase();

        echo "Finished\n{$count} jobs fixed\n";
    }
}
