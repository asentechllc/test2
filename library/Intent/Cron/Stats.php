<?php
class Intent_Cron_Stats extends Mcmr_Cron_ModuleAbstract
{
    private $_mapper = null;
    private $_db = null;
    private $_start = null;
    private $_end = null;
    
	private $_modeltype = 'News_Model_Article';

    private $_commentThreads = array();

    public function __construct()
    {
        $this->_mapper = Default_Model_Stat::getMapper();
        $this->_db = $this->_mapper->getDbTable()->getAdapter();
        $this->_db->getProfiler()->setEnabled(true);
        $profiler = new Zend_Db_Profiler(true);
        $this->_db->setProfiler($profiler);
    }

    public function setConfig(Zend_Config $config)
    {
        $this->_start = strtotime(time() - (86400 * Mcmr_Stats::popularity_window));
        $this->_end = time();
        
        return parent::setConfig($config);
    }

    public function execute()
    {
    	echo "Fetching latest comment threads\n";
    	$this->_getCommentThreads();
    	echo "Found ".count($this->_commentThreads)."\n";

        echo "Starting Statistics aggregation\n";

        // Get all the article IDs then calculate and store the totals
        $sql = 'SELECT article_id FROM news_articles ORDER BY article_publishdate desc LIMIT 200';
        $stmt = $this->_db->query($sql);

        $ids = $stmt->fetchAll();
        $x=0;
        foreach ($ids as $key=>$id) {
        
        	$modelType = $this->_modeltype;
        	$modelId = $id['article_id'];
        
        	# check if we have a special feature
        	$attribs = $this->_db->fetchAll("SELECT * FROM news_articles_attr WHERE article_id=".intval($id['article_id']));
        	foreach($attribs as $attr){
        		if($attr['attr_name'] == 'featureId'){
        			$modelType = 'Feature_Model_Feature';
        			$modelId = $attr['attr_value'];
        			break;
        		}
        	}
        
            Mcmr_Cron::phase();
            $id = $id['article_id'];
            $stats = array(
            	'read' => 0,
            	'comment' => 0,
            	'email' => 0,
            );
            foreach ($stats as $stat=>$v) {
            
            	# don't check for comments if there are no views!
            	if($stat == 'comment' && !$stats['read'])
            		continue;
            
               	$stats[$stat] = $this->_getStatCount($modelId, $stat, $modelType);
			}

			if($stats['read'] || $stats['comment'] || $stats['email']){	
				$sql = '';
				$values = array();

				foreach(array('read','comment','email') as $stat){
					if(isset($stats[$stat]) && $stats[$stat] !== false){
						$sql .= ($sql ? ',' : '').'article_num'.$stat.'=?';
						$values[] = $stats[$stat];
					}
				}
				$values[] = $id;
				
				$this->_db->query("UPDATE news_articles SET $sql WHERE article_id=?",$values);
				echo "	Cached stats for article #{$id} (".implode(',',$stats).")\n";
			}

			/*if($stats['read'] || $stats['comment'] || $stats['email']){	
				$this->_db->query("UPDATE news_articles SET article_numread={$stats['read']},article_numcomment={$stats['comment']},article_numemail={$stats['email']} WHERE article_id=".intval($id));
				echo "	Cached stats for {$modelType} #{$id} ({$stats['read']},{$stats['comment']},{$stats['email']})\n";
			}*/

            $x++;
        }
        Mcmr_Cron::clearPhase();

        echo "Statistics aggregation complete. $x articles processed\n";

        $this->_cleanStats();

        echo "Old statistics cleaned\n";

        return $this;
    }
    
    /**
     * Fetch list of most recent comment threads
     **/
    private function _getCommentThreads(){

    	$forumName = isset(Zend_Registry::get('default-config')->disqus->shortname)
    		? Zend_Registry::get('default-config')->disqus->shortname
    		: Zend_Registry::get('app-config')->siteid;

        //$client = new Zend_Http_Client("http://disqus.com/api/3.0/threads/list.json?forum={$forumName}&limit=100&api_secret=".$this->_getApiSecret());

		# uses Beta feature of disqus which only fetches comments with posts...
		# better than sifting through loads of pages!
        $client = new Zend_Http_Client("http://disqus.com/api/3.0/threads/listPopular.json?forum={$forumName}&interval=90d&limit=100&api_secret=".$this->_getApiSecret());

        $response = null;
        $error = false;
        try {
            $response = $client->request();
            $decodedResponse = json_decode($response->getBody());

            if(is_array($decodedResponse->response)){
            	foreach($decodedResponse->response as $thread){
            		$this->_commentThreads[$thread->link] = $thread->posts;
            	}
            }
            else if(is_string($decodedResponse->response)){
            	$error = $decodedResponse->response;
            }
        } catch ( Exception $e ) {
            Mcmr_Debug::dump("Exception caught in Disqus request. Probably quota issue.");
            Mcmr_Debug::dump($e->getMessage());
        }
        
        if($error)
	        echo "{$error}\n";
    }

	private function _getApiSecret(){
		return Zend_Registry::get('feed-config')->disqus->api_secret;
	}
	
    private function _getCommentCount($id,$modelType){

		$baseDomain = Zend_Registry::get('app-config')->media->baseDomain;
		$forumName = Zend_Registry::get('app-config')->siteid;

		$model = $modelType::getMapper()->find($id);
		$threadUrl = preg_replace('/\/$/','',$baseDomain).Mcmr_Model::getCanonicalReadUrl($model);
		
		# INT000-1186 less invasive comment counting implemented
		//$thread = Feed_Model_Disqus_Thread::getMapper()->findOneByField(array('forum'=>$forumName, 'link'=>$threadUrl));
		//return intval($thread->getCommentsCount());

		return isset($this->_commentThreads[$threadUrl])
			? $this->_commentThreads[$threadUrl]
			: false;
    }

    private function _getStatCount($id, $action = 'read', $modelType = null)
    {
    
    	if($action == 'comment')
    		return $this->_getCommentCount($id,$modelType);
    
        $sql = 'SELECT SUM(stat_count) as count
            FROM default_stats
            WHERE stat_modeltype =:type
            AND stat_modelid=:id
            AND stat_date BETWEEN DATE(FROM_UNIXTIME(:start)) AND DATE(FROM_UNIXTIME(:end))
            AND stat_action = :action';

        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        $params = array(
            ':type'=>$modelType ? $modelType : $this->_modeltype,
            ':id'=>$id,
            ':start'=>$this->_start,
            ':end'=>$this->_end,
            ':action'=>$action
        );

        $stmt->execute($params);
        $row = $stmt->fetch();
        $count = $row['count'];
        
        return intval($count);
    }

    private function _cleanStats()
    {
        $sql = 'DELETE FROM default_stats WHERE stat_modeltype = :type AND stat_date < DATE(FROM_UNIXTIME(:start))';
        $params = array(':type'=>$this->_modeltype, ':start'=>$this->_start);
        $stmt = new Zend_Db_Statement_Pdo($this->_db, $sql);
        $stmt->execute($params);
    }
}
