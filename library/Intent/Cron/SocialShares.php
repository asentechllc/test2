<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron script to fetch jobs for OPM
 *
 * @category 
 * @package 
 * @subpackage 
 */
class  Intent_Cron_SocialShares extends Mcmr_Cron_ModuleAbstract
{
    protected $_bitLyService = null;
    protected $_count;
    protected $_view;


    public function execute()
    {
        $this->_view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $queries = $this->_config->socialShares->queries->toArray();
        echo "Updating social shares\n";
        $this->_count = 0;
        foreach ($queries as $queryData ) {
            $queryName = $queryData['query'];
            $modelName = $queryData['model'];
            $moduleName = $queryData['module'];
            $this->_processQuery($queryName, $modelName, $moduleName);
        }
        echo "Finished\n{$this->_count} model updated\n";
    }

    protected function _processQuery($queryName, $modelName, $moduleName)
    {
        $query = $this->_view->queryBuild($queryName, $modelName, $moduleName)->addConditions(array(
            'attr_shareCountFlag'=>1))->setPage('all');
        $models = $this->_view->queryAll($query, $modelName, $moduleName);
        foreach ($models as $model) {
            echo($model->getId().': '.$model->getTitle()."\n");
            $bitLyUrl = $model->getAttribute('bitLyUrl');
            if (!$bitLyUrl) {
                $this->_createBitLyUrl($model);
            }
            $this->_updateSocialShares($model);
            $this->_count++;
        }
    }

    protected function _updateSocialShares($model)
    {
        $conditions = array('url'=>$this->_getAbsoluteUrl($model));
        $mapper = Feed_Model_Socialcount::getMapper();
        $socialCounts = $mapper->findOneByField($conditions);
        foreach ($socialCounts->getCounts() as $service => $count ) {
            $model->setAttribute('shareCount_'.$service, $count);
        }
        $model->setAttribute('shareCountTotal', $socialCounts->getTotalCount());
        $model->setAttribute('shareCountUpdate', time());
        $model->setAttribute('shareCountFlag', 0);
        $model->getMapper()->save($model);
    }

    protected function _createBitLyUrl($model) 
    {
        $url = $this->_getAbsoluteUrl($model);
        $bitLy = $this->_getBitLyService();
        $short = $bitLy->shorten($url); 
        $model->setAttribute('bitLyUrl', $short);
        $mapper = $model->getMapper();
        $model->getMapper()->save($model);
    }

    protected function _getAbsoluteUrl($model) {
        $baseDomain = Zend_Registry::get('app-config')->media->baseDomain;
        $relativeUrl = $this->_view->defaultReadUrl($model);
        if ('/'==substr($baseDomain,-1)&&'/'==substr($relativeUrl,0,1)) {
            $baseDomain = substr($baseDomain, 0, -1);
        }
        $url = $baseDomain.$relativeUrl;

        return $url;
    }

    protected function _getBitLyService() {
        if (null==$this->_bitLyService) {
            $config = Zend_Registry::get('default-config');
            $accessToken = $config->shorturl->bitly->accessToken;
            $this->_bitLyService = new Mcmr_Service_ShortUrl_BitLy($accessToken);
        }
        return $this->_bitLyService;
    }

}
