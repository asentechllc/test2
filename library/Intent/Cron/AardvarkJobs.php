<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron script to fetch jobs for AardvarkSwift
 *
 * @category 
 * @package 
 * @subpackage 
 */
class  Intent_Cron_AardvarkJobs extends Mcmr_Cron_ModuleAbstract
{
    const INDEX_ID          = 0;
    const INDEX_TITLE       = 1;
    const INDEX_DESCRIPTION = 2;
    const INDEX_DATE        = 3;
    const INDEX_LOCATION    = 4;
    const INDEX_CATEGORY    = 6;
    const INDEX_SALARY      = 7;
    
    # if category/location cannot be matched, use default if set - otherwise skip
    const USE_DEFAULT_CATEGORY = true;
    const USE_DEFAULT_LOCATION = true;

    private static $_locations = array();
    private static $_categories = array();
    protected	$_company = null,
    			$_companyTitle = null,
    			$_companyImage = null;
    
    protected	$_defaultLocation = null,
    			$_defaultCategory = null;

    /**
     * Number of days to set expiry to if 'date' is set to 0
     */
    const EXPIRE_DAYS = "+90 days";

    public function execute()
    {
        if (!isset($this->_config->jobs) || !isset($this->_config->jobs->applicationAddress)) {
            throw new Zend_Exception("Application Address not set. Application Address must be set in config");
        }
        if (!isset($this->_config->feed) || !isset($this->_config->feed->source)) {
            throw new Zend_Exception("Feed source not set. Source must be set in config");
        }
        
        if(isset($this->_config->category->default)){
            $this->_defaultCategory = Job_Model_Category::getMapper()->findOneByField(array('url'=>$this->_config->category->default));
        }

        if(isset($this->_config->location->default)){
            $this->_defaultLocation = Job_Model_Location::getMapper()->findOneByField(array('url'=>$this->_config->location->default));
        }

		$user = null;
        if (isset($this->_config->jobs) && isset($this->_config->jobs->userEmail)) {
			$user = User_Model_User::getMapper()->findOneByField(array('email'=>$this->_config->jobs->userEmail));
			if (null === $user) {
				throw new Zend_Exception("Cannot find user with email '{$this->_config->jobs->userEmail}'");
			}
		}
		
        $this->_processFeed($this->_config->feed->source, $user, $this->_config->jobs->applicationAddress);
    }

    private function _processFeed($feed, $user, $applicationEmail)
    {
        echo "Processing feed '{$feed}'\n";
        $lineCount = 0;
        $arrayCount = 0;
        $createCount = 0;
        $skipCount = 0;
        $errorCount = 0;
        $filter = new Mcmr_Filter_Msword();
        
        if (isset($this->_config->jobs->companyId)) {
            $this->_company = Company_Model_Company::getMapper()->findOneByField(array('id'=>$this->_config->jobs->companyId));
        }
        
        if(isset($this->_config->jobs->companyTitle))
        	$this->_companyTitle = $this->_config->jobs->companyTitle;

        if(isset($this->_config->jobs->companyImage))
        	$this->_companyImage = $this->_config->jobs->companyImage;

        $handle = Mcmr_StdLib::fopenReadUtf8($feed, "r");

        while ($job = fgetcsv($handle)) {
            $lineCount++;
            Mcmr_Cron::phase();
            
            try {
                $jobModel = $this->_createJob($job, $user, $applicationEmail);

                if (null !== $jobModel) {
                    Job_Model_Job::getMapper()->save($jobModel);
                    
                    $createCount++;
                } else {
                    $skipCount++;
                }
            } catch (Exception $e) {
                $errorCount++;
                
                try {
                    Mcmr_Debug::dump( "failed job id:".$job[self::INDEX_ID], 6, 'file' );
                } catch ( Exception $e ) {
                }
                echo $e->getMessage()."\n";

            }
            Mcmr_Cron::clearPhase();
        }
        fclose($handle);

        Mcmr_Cron::clearPhase();

        echo "{$lineCount} entries in feed, {$createCount} jobs created. {$skipCount} jobs skipped. {$errorCount} errors.\n";

    }

    private function _createJob($jobRow, $user, $applicationEmail)
    {
        if ( $this->_jobExists((string)$jobRow[self::INDEX_ID])) {
            return null;
        }
        
        $jobRow[self::INDEX_SALARY] = str_replace('£', '', $jobRow[self::INDEX_SALARY]);
        $jobRow[self::INDEX_SALARY] = str_replace('Ł', '', $jobRow[self::INDEX_SALARY]);
        $jobRow[self::INDEX_SALARY] = str_replace(';', '', $jobRow[self::INDEX_SALARY]);
        
        // Create the jobs
        $job = new Job_Model_Job();
        $job->setTitle((string)$jobRow[self::INDEX_TITLE]);
        $job->setDescription(nl2br((string)$jobRow[self::INDEX_DESCRIPTION]));
        $job->setPublished(true);
        $job->setExpiredate($this->_processDate($jobRow[self::INDEX_DATE]));
        $job->setCategoryid($this->_processCategory((string)$jobRow[self::INDEX_CATEGORY]));
        $job->setLocationid($this->_processLocation((string)$jobRow[self::INDEX_LOCATION]));
        $job->setSalary((string)$jobRow[self::INDEX_SALARY]);

        if($user){
	        $job->setUserid($user->getId());
	    }

        $job->setStatus('active');
        $job->setAttribute('externalUrl', $applicationEmail);
        $job->setAttribute('howtoapplymethod', 'email');
        $job->setAttribute('aardvark_id', (string)$jobRow[self::INDEX_ID] );
        $job->setAttribute('reference', (string)$jobRow[self::INDEX_ID] );
        if (null!==$this->_company) {
            $job->setAttribute('companytitle', $this->_company->getTitle());
            $job->setAttribute('companylogo', $this->_company->getImage());
        }
        else{

        	if($this->_companyTitle)
	            $job->setAttribute('companytitle', $this->_companyTitle);

        	if($this->_companyImage)
	            $job->setAttribute('companylogo', $this->_companyImage);
        	
        }

        return $job;
    }

    private function _processDate($dateString)
    {
        if ('0' == $dateString) {
            return strtotime(self::EXPIRE_DAYS);
        } else {
            $parts = explode("/", $dateString);
            if (3 !== count($parts)) {
                throw new Zend_Exception("Invalid date format");
            }
        }

        return mktime(0, 0, 0, $parts[1], $parts[0], $parts[2]);
    }

    private function _processLocation($location)
    {
        $urlString = Mcmr_StdLib::urlize(str_replace('&amp;','and',$location));

        if (!isset(self::$_locations[$urlString])) {
            if (isset($this->_config->location) && isset($this->_config->location->mapping)
                    && isset($this->_config->location->mapping->$urlString)) {
                $urlString = $this->_config->location->mapping->$urlString;
            }

            $location = $urlString ? Job_Model_Location::getMapper()->findOneByField(array('url'=>$urlString)) : $this->_defaultLocation;

            if (null !== $location) {
                self::$_locations[$urlString] = $location;
            } else if(self::USE_DEFAULT_LOCATION && $this->_defaultLocation){
                self::$_locations[$urlString] = $this->_defaultLocation;
            } else {
                throw new Zend_Exception("Cannot find location '{$urlString}'");
            }
        }

        return self::$_locations[$urlString]->getId();
    }

    private function _processCategory($category)
    {
        $urlString = Mcmr_StdLib::urlize($category);

        if (!isset(self::$_categories[$urlString])) {
            if (isset($this->_config->category) && isset($this->_config->category->mapping)
                    && isset($this->_config->category->mapping->$urlString)) {
                $urlString = $this->_config->category->mapping->$urlString;
            }

            $category = $urlString ? Job_Model_Category::getMapper()->findOneByField(array('url'=>$urlString)) : $this->_defaultCategory;

            if (null !== $category) {
                self::$_categories[$urlString] = $category;
            } else if(self::USE_DEFAULT_CATEGORY && $this->_defaultCategory){
                self::$_categories[$urlString] = $this->_defaultCategory;
            } else {
                throw new Zend_Exception("Cannot find category '{$urlString}'");
            }
        }
        
        return self::$_categories[$urlString]->getId();
    }

    private function _jobExists($opmId)
    {
        $mapper = Job_Model_Job::getMapper();
        $job = $mapper->findOneByField(array('attr_aardvark_id'=>$opmId));

        return (null !== $job);
    }
}
