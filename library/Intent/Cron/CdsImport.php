<?php

class Intent_Cron_CdsImport extends Mcmr_Cron_ModuleAbstract
{
    const DELIMITER = ',';
    const ENCLOSURE = '"';

    protected $_userMapper;
    protected $_emailValidator;

    public function execute()
    {
        $file = $this->_config->file;
        $handle = fopen($file, 'r');
        if ( false === $handle ) {
            echo( "file not found: $file\n" );
            return false;
        }
        $this->_userMapper = User_Model_User::getMapper();
        $this->_emailValidator = new Zend_Validate_EmailAddress();
        echo( "processing: $file\n" );
        $header = fgetcsv($handle, 0, self::DELIMITER, self::ENCLOSURE);
        $processed = 0;
    	$found = 0;
    	$updated = 0;
    	$notFound = 0;
        while (($line = fgetcsv($handle, 0, self::DELIMITER, self::ENCLOSURE)) !== false) {
            $csvRow = array_combine($header, $line);
	    	$urn = "00".$csvRow["Reader Reference"];
	    	$surname = $csvRow["Surname"];
	    	$email = $csvRow["Email Address"];
	    	$user = $this->_userMapper->findOneByField(array('attr_cdsUrn'=>$urn, 'surname'=>$surname));
	    	if (null!==$user) {
	    		echo "$urn: found user, csv email: $email, gossip email: ".$user->getEmail()."\n";
	    		$found++;
	            if($this->_emailValidator->isValid($email)&&!$this->_emailValidator->isValid($user->getEmail())) {
	                $user->setEmail($email);
	                $this->_userMapper->save($user);
	                $updated++;
	            }
	    	} else {
	    		$notFound++;
	    		echo "$urn: USER NOT FOUND!\n";
	    	}
	    	$processed++;
	        echo( "processed: $processed, found: $found, updated: $updated, not found: $notFound\n" );
        }

    }
}
