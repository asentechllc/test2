<?php

/*
	Resends Job applications between date range
	Requires date arguments:
	yyyy-mm-dd hh:mm to yyyy-mm-dd hh:mm
*/

class Intent_Cron_SendJobApps extends Mcmr_Cron_ModuleAbstract
{
    public function execute()
    {

		echo "\n";

		$emailOverride = null;
		# check for email override
		if(preg_match('/(^| *)([A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4})( *|$)/i',$this->_args,$m)){
			$emailOverride = $m[2];
			print "Recipient Override: {$emailOverride}\n";
		}

		# check for job override
		$jobID = 0;
		if(preg_match('/(^| *)job=([0-9]+)( *|$)/i',$this->_args,$m)){
			$jobID = intval($m[2]);
			print "Job ID: {$jobID}\n";
		}

    	if(preg_match('/(^| *)([a-z0-9\.]+) *([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}) *to *([0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2})/i',$this->_args,$m)){

			$domain = $m[2];
	    	$from = strtotime($m[3].':00');
	    	$to = strtotime($m[4].':00');
	    	
	    	echo "Resending job applications...\n";
	    	echo "\tDomain: $domain\n";
	    	echo "\tFrom: ".date('r',$from)."\n";
	    	echo "\tTo: ".date('r',$to)."\n";
	    	$_SERVER['HTTP_HOST'] = $domain;

			$conditions = array(
				'date' => array(
					'value' => array($from,$to),
		        	'condition' => 'between',
		        ),
			);
			
			if($jobID){
				$conditions['job_id'] = $jobID;
			}
			
	    	$models = Job_Model_Application::getMapper()->findAllByField(
				$conditions,
    	        null,
        	    array('page'=>1, 'count'=>1000000000)
        	);
        	
        	$sent = 0;
        	foreach($models as $model){

				// Build the email body
				$frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
				$post = Zend_Controller_Front::getInstance()->getRequest()->getPost();
		
				$view = clone $frontView;
				$view->setScriptPath(SITE_PATH . '/views/job/scripts/');
				$view->addScriptPath(SITE_PATH.'/views/_common');
				$view->addScriptPath(SITE_PATH.'/_common');
				$view->application = $model;

				$request = Zend_Controller_Front::getInstance()->getRequest();
				$siteName = $request->getSiteName();
				if ('default' === $siteName) {
					$body = $view->render('email/job-application.phtml');
				} else {
					$body = $view->render($siteName.'/email/job-application.phtml');
				}

				// Get the email subject
				$config = Zend_Registry::get('job-config');
				if (isset($config->email->application->subject)) {
					$subject = $config->email->application->subject;
					$subject = str_replace('%title%', $model->getJob()->getTitle(), $subject);
				} else {
					$subject = "New Job Application";
				}

				// Send the email to the job owner
				$emailaddress = $model->getJob()->getAttribute('externalUrl');
				$validator = new Zend_Validate_EmailAddress();
				if (!$validator->isValid($emailaddress)) {
					$user = $model->getJob()->getUser();
					$emailaddress = $user->getEmail();
				}
				
				if($emailOverride)
					$emailaddress = $emailOverride;

				$email = new Mcmr_Mail();
				$email->setBodyHtml($body);
				$email->setSubject($subject);
				$email->addTo($emailaddress);

				// Bcc an admin
				if (isset($config->email->application->admin)) {
					$email->addBcc($config->email->application->admin->toArray());
				}

				if ($model->getFile()) {
					$filename = SITE_PATH . '/public/' . $model->getFile();
					$file = file_get_contents($filename);
					$attachment = $email->createAttachment($file);
					$attachment->filename = basename($model->getFile());
				}
				$email->send();
				

				echo "\t\tEmail sent to $emailaddress\n";
        		$sent++;
        	}
        	
        	echo "Sent {$sent} application(s)\n\n";
	    	
    	}
    	else{
    		die("\nPlease supply a domain and date range in the format:\n\t--args='domain.com yyyy-mm-dd hh:mm to yyyy-mm-dd hh:mm\n\n");
    	}
    }
}
