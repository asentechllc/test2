<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Cron script to set regular user IDs if applicable from guest names
 * Only searches for articles matching writers names
 */
class  Intent_Cron_FixAuthor extends Mcmr_Cron_ModuleAbstract
{

    public function execute()
    {
        echo "Fixing articles\n";
        $errorShort = false;

		$users = User_Model_User::getMapper()->findAllByField(array(
			'role' => array(
				'condition' => 'in',
				'value' => array('admin','editor'),
			),
			'attr_theteam' => 1,
		));

		$totalFixed = 0;
		foreach($users as $user){
			$name = $user->getFullName();
			echo "\n\tFixing articles for {$name}...\n";

	        $mapper = News_Model_Article::getMapper();
    	    $mapper->setObserversEnabled(false);

			$articles = $mapper->findAllByField(array(
				'attr_author' => 'guest',
				'authorname'=>$name,
			));
			
			$fixed = 0;
			
			foreach($articles as $article){
				Mcmr_Cron::phase();
				$article->setAttribute('author','regular');
				$article->setUseridauthor($user->getId());
					$mapper->save($article);
				$fixed++;
			}

    	    Mcmr_Cron::clearPhase();

			echo "\tFixed {$fixed} article(s)\n";
			$totalFixed += $fixed;
		}

		echo "\nFixed {$totalFixed} article(s) in total\n";
		
    }
}
