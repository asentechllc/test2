<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Bikebiz
 * @package Bikebiz_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CategoryController.php 753 2010-06-07 16:18:43Z leigh $
 */

/**
 * Observer for creating payment models when a new job is created
 *
 * @category Bikebiz
 * @package Bikebiz_Observer
 */
class Intent_Observer_JobCreatePayment extends Mcmr_Model_ObserverAbstract
{
    static public $disable = false;

    public function insert(Mcmr_Model_ModelAbstract $job)
    {
        if (self::$disable) {
            return true;
        }

        $config = Zend_Registry::get('payment-config')->job;
        
        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $postData = $request->getPost();
        
        $basket = new Payment_Model_Basket();
        $basket->setOptions($config->toArray());
        $basket->setProfile('job');

        $item = new Payment_Model_Item();
        $item->setModeltype(get_class($job));
        $item->setModelid($job->getId());
        $item->setAmount($config->amount);
        $item->setTaxrate($config->taxRate);
        $basket->setItems(array($item));

        if (isset($postData['bikebizjob']['bikebizformaddress'])) {
            $deliveryAddress = new Payment_Model_Address($postData['bikebizjob']['bikebizformaddress']);
            $deliveryAddress->setType('delivery');
            $billingAddress = new Payment_Model_Address($postData['bikebizjob']['bikebizformaddress']);
            $billingAddress->setType('billing');
            $basket->setAddresses(array($billingAddress, $deliveryAddress));
        }
        
        $mapper = Payment_Model_Basket::getMapper();
        $mapper->save($basket);

        // Redirect the user to the payment page
        $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        $redirector->gotoSimple('process', 'index', 'payment', array('id'=>$basket->getId(), 'journey'=>'job'));
    }
}
