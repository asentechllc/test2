<?php

/**
 * If a user has registered with a company that is not in the hints database, create one for future users.
 */
class Intent_Observer_UserCompanyName extends Mcmr_Model_ObserverAbstract
{
    static $originalCompany = null;

    public function preUpdate( $user)
    {
        if (is_object($user)) {
            self::$originalCompany = $user->getAttribute('company');
        }
    }
    
    public function update($user)
    {
        $company=$user->getAttribute('company');
        if ($company && $company!=self::$originalCompany) {
            $this->_createCompany($company);
        }
    }

    public function insert($user) 
    {
        $company=$user->getAttribute('company');
        if ($company) {
            $this->_createCompany($company);
        }
    }

    protected function _createCompany($companyName)
    {
        $type = Company_Model_Type::getMapper()->findOneByField(array('url'=>'hint'));
        $mapper = Company_Model_Company::getMapper();
        $company = $mapper->findOneByField(array('typeid'=>$type->getId(),'title'=>$companyName));
        if (null==$company) {
            $company = new Company_Model_Company();
            $company->setTitle($companyName);
            $company->setDescription('');
            $company->setState('approved');
            $company->setTypeid($type->getId());
            $mapper->setObserversEnabled(false);
            $mapper->save($company);
            $mapper->setObserversEnabled(true);
        }
    }
}
