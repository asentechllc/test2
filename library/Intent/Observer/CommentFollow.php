<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Observer for automatically creating a 'follow' when posting a comment.
 *
 * @category Intent
 * @package Intent_Observer
 */
class Intent_Observer_CommentFollow extends Mcmr_Model_ObserverAbstract
{
    public function insert($comment)
    {
        if ($comment instanceof Comment_Model_Comment) {
            $thread = $comment->getThread();

            $conditions = array();
            $conditions['modeltype'] = get_class($thread);
            $conditions['modelid'] = $thread->getId();
            $user = Zend_Registry::get('authenticated-user');
            if (null === $user || null === $user->getId()) {
                return null;
            }
            $conditions['userid'] = $user->getId();

            $mapper = Changelog_Model_Follow::getMapper();
            if (null === ($follow = $mapper->findOneByField($conditions))) {
                $follow = new Changelog_Model_Follow();
                $follow->setModelid($thread->getId());
                $follow->setModeltype(get_class($thread));

                $mapper->save($follow);
            }
        }
    }
}
