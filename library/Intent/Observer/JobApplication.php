<?php

class Intent_Observer_JobApplication extends Mcmr_Model_ObserverAbstract
{
    public function insert($model)
    {
        // Build the email body
        $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $post = Zend_Controller_Front::getInstance()->getRequest()->getPost();
        
        $view = clone $frontView;
        $view->setScriptPath(SITE_PATH . '/views/job/scripts/');
        $view->addScriptPath(SITE_PATH.'/views/_common');
        $view->addScriptPath(SITE_PATH.'/_common');
        $view->application = $model;
        
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $siteName = $request->getSiteName();
        if ('default' === $siteName) {
            $body = $view->render('email/job-application.phtml');
        } else {
            $body = $view->render($siteName.'/email/job-application.phtml');
        }
        // Get the email subject
        $config = Zend_Registry::get('job-config');
        if (isset($config->email->application->subject)) {
            $subject = $config->email->application->subject;
            $subject = str_replace('%title%', $model->getJob()->getTitle(), $subject);
        } else {
            $subject = "New Job Application";
        }

        // Send the email to the job owner
        $emailaddress = $model->getJob()->getAttribute('externalUrl');
        $validator = new Zend_Validate_EmailAddress();
        if (!$validator->isValid($emailaddress)) {
            $user = $model->getJob()->getUser();
            $emailaddress = $user->getEmail();
        }
        
        $email = new Mcmr_Mail();
        $email->setBodyHtml($body);
        $email->setSubject($subject);
        if ($post["jobformapplication"]["email_me"]) {
            $email->addTo(
                $model->getAttribute('email'), 
                $model->getAttribute('forename').' '.$model->getAttribute('surname')
            );
            $email->addBcc($emailaddress); // Hide the company email address from the user.
        } else {
            $email->addTo($emailaddress);
        }

        // Bcc an admin
        if (isset($config->email->application->admin)) {
            $email->addBcc($config->email->application->admin->toArray());
        }

        if ($model->getFile()) {
            $filename = SITE_PATH . '/public/' . $model->getFile();
            $file = file_get_contents($filename);
            $attachment = $email->createAttachment($file);
            $attachment->filename = basename($model->getFile());
        }
        $email->send();
    }
}
