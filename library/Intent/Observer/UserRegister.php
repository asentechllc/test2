<?php

class Intent_Observer_UserRegister extends Mcmr_Model_ObserverAbstract
{
    static $originalUser = null;

    public function preUpdate( $user)
    {
        if (is_object($user)) {
            self::$originalUser = clone $user;
        }
    }
    
    public function update($user)
    {
        if (is_object(self::$originalUser) && false === self::$originalUser->getEmailconfirmed() && $user->getEmailconfirmed()) {
            $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            $view = clone $frontView;
            $view->setScriptPath(SITE_PATH . '/views/user/scripts/');
            $view->addScriptPath(SITE_PATH.'/views/_common');
            $view->addScriptPath(SITE_PATH.'/_common');
            $view->user = $user;
            
            $template = 'email/user-confirm-admin.phtml';
            $sites = $user->getSites();
            if (count($sites)==1 && 'default'!=$sites[0]) {
                $template = $sites[0].'/'.$template;
            } 

            $body = $view->render($template);

            // Get the email subject
            $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
            $config = Zend_Registry::get('user-config');
            if (isset($config->$site)) {
                $config = $config->$site;
            }
            
            if (isset($config->email->notification->register->subject)) {
                $subject = $config->email->notification->register->subject;
            } else {
                $subject = "New User";
            }

            $email = new Mcmr_Mail();
            $email->setBodyHtml($body);
            $email->setSubject($subject);
            $addresses = $config->email->notification->register->admin->toArray();
            $email->addTo($addresses);
            $email->send();
        }
    }
}
