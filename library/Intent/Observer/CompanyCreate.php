<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyCreate
 *
 * @author leigh
 */
class Intent_Observer_CompanyCreate extends Mcmr_Model_ObserverAbstract
{
    static public $disable = false;

    public function insert(Mcmr_Model_ModelAbstract $company)
    {
        if ($company instanceof Company_Model_Company) {
            // Associate the user with the companuy
            $user = $company->getUser();
            if (null !== $user) {
                $user->setAttribute('companyid', $company->getId());
                User_Model_User::getMapper()->save($user);
            }
        } else {
            return false;
        }
    }

    public function update(Mcmr_Model_ModelAbstract $company)
    {
        if ($company instanceof Company_Model_Company) {
            // Associate the user with the companuy
            $user = $company->getUser();
            if (null !== $user) {
                $user->setAttribute('companyid', $company->getId());
                User_Model_User::getMapper()->save($user);
            }
        }
    }
}
