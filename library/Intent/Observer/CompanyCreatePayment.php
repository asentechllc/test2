<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyCreatePayment
 *
 * @author leigh
 */
class Intent_Observer_CompanyCreatePayment extends Mcmr_Model_ObserverAbstract
{
    static public $disable = false;

    public function insert(Mcmr_Model_ModelAbstract $company)
    {
        if ($company instanceof Company_Model_Company) {
            return $this->_upgradeCompany($company);
        } else {
            return false;
        }
    }

    public function update(Mcmr_Model_ModelAbstract $company)
    {
        if ($company instanceof Company_Model_Company) {
            return $this->_upgradeCompany($company);
        } else {
            return false;
        }
    }

    public function _upgradeCompany(Company_Model_Company $company)
    {
        if (self::$disable) {
            return true;
        }
        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $postData = $request->getPost();

        // Save the company attributes to the user
        $userMapper = User_Model_User::getMapper();
        $user = $userMapper->find($company->getUserid());
        if (null !== $user) {
            $user->setAttribute('companyid', $company->getId());
            $user->setAttribute('company', $company->getTitle());
            $userMapper->save($user);
        }

        if ('0' == $company->getListed() || time() > $company->getAttribute('listingExpire')) {
            $typeMapper = Product_Model_Type::getMapper();
            $type = $typeMapper->findOneByField(array('url'=>'listing'));

            $basketMapper = Payment_Model_Basket::getMapper();
            $baskets = $basketMapper->findAllByField(
                array('userid'=>$user->getId(), 'status'=> Mcmr_Payment_AdapterAbstract::STATUS_OK)
            );

            foreach ($baskets as $basket) {
                foreach ($basket->getItems() as $item) {
                    $product = $item->getModel();
                    if ($product->getTypeid() === $type->getId() && 0 < $item->getRedeem()) {
                        $company->setListed(1);
                        $company->setAttribute('listingExpire', strtotime($item->getAttribute('listtime')));

                        self::$disable = true;
                        Company_Model_Company::getMapper()->save($company);
                        self::$disable = false;

                        $item->setRedeem(0);
                        Payment_Model_Item::getMapper()->save($item);
                    }
                }
            }
        }
    }
}
