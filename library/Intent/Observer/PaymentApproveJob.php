<?php

class Intent_Observer_PaymentApproveJob extends Mcmr_Model_ObserverAbstract
{
    /**
     * Executed when a payent is updated
     * 
     * @param Payment_Model_Payment $model
     */
    public function update($model)
    {
        // If a successful payment
        if ($model->getStatus() === Mcmr_Payment_AdapterAbstract::STATUS_OK) {
            $mapper = Job_Model_Job::getMapper();

            // Disable the observers on the basket mapper. Prevent 'load' observers
            Payment_Model_Basket::getMapper()->setObserversEnabled(false);
            $basket = $model->getBasket();
            Payment_Model_Basket::getMapper()->setObserversEnabled(true);
            
            $items = $basket->getItems();
            foreach ($items as $item) {
                $job = $item->getModel();
                if ($job instanceof Job_Model_Job) {
                    // Get the job's expiry date from the product
                    $product = $item->getProduct();
                    if (null !== $product) {
                        $expireDate = strtotime($product->getAttribute('listtime'));
                        if ($expireDate > time()) {
                            $job->setExpiredate($expireDate);
                        }
                    }
                    
                    $user = $job->getUser();
                    $job->setStatus('active');
                    $job->setPublished(true);
                    if (null !== $user) {
                        $job->setCompanyid($user->getAttribute('companyid'));
                    }
                    $mapper->save($job);
                }
            }
        }
    }
}
