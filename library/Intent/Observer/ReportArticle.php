<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Observer for creating News article if requested
 *
 * @category Intent
 * @package Intent_Observer
 */
class Intent_Observer_ReportArticle extends Mcmr_Model_ObserverAbstract
{

    protected $_productTypeId;
    protected $_articleTypeId;
    
    protected function getProductTypeId()
    {
        if (null == $this->_productTypeId) {
            $typeMapper = Product_Model_Type::getMapper();
            $type = $typeMapper->findOneByField(array('url'=>'report'));
            if ( null !== $type ) {
                $this->_productTypeId = $type->getId();
            }
        }
        return $this->_productTypeId;
    }

    protected function getArticleTypeId()
    {
        if (null == $this->_articleTypeId) {
            $typeMapper = News_Model_Type::getMapper();
            $type = $typeMapper->findOneByField(array('url'=>'report'));
            if (null !== $type) {
                $this->_articleTypeId = $type->getId();
            }
        }
        return $this->_articleTypeId;
    }

    public function insert(Mcmr_Model_ModelAbstract $product )
    {
        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $postData = $request->getPost();
        $config = Zend_Registry::get('product-config');

        if ( $product->getTypeId() == $this->getProductTypeId() && '1' == $postData['productformproduct']['stream']) {
            $articleMapper = News_Model_Article::getMapper();
            $type = $this->getArticleTypeId();
            if (null !== $type) {
                $article = new News_Model_Article();
                $article->setTitle($product->getTitle());
                $article->setImage($product->getImage());
                $article->setTypeid($type);
                $article->setPublishdate(($publishdate = $product->getAttribute('publishdate')) ? $publishdate : time());
                $article->setPublished(true);
                $article->setStream(true);
                $article->setAttribute('productId', $product->getId());
                $article->setAttribute('keyword', $product->getAttribute('keyword'));
                
                if (isset($config->news) && isset($config->news->sites)) {
                    $article->setSites($config->news->sites);
                }
                
                if(trim($author = $product->getAttribute('author'))){
                	$article->setAuthorname($author);
                	$article->setAttribute('author','guest');
                }
                else{
                	$article->setAttribute('author','anonymous');
                }

                $articleMapper->save($article);

                // Add the article ID to the product
                $productMapper = Product_Model_Product::getMapper();
                $product->setAttribute('article_id', $article->getId());

                // Disable the observers
                $this->setEnabled(false);
                $productMapper->save($product);
                $this->setEnabled(true);
            }
        }
    }

    public function update(Mcmr_Model_ModelAbstract $product)
    {
        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $postData = $request->getPost();
        $config = Zend_Registry::get('product-config');

        if (!isset($postData[ 'productformproduct'])) {
            return;
        }

        if (  $product->getTypeId() == $this->getProductTypeId() ) {
            if ('1' == $postData['productformproduct']['stream']) {
                $articleMapper = News_Model_Article::getMapper();
                if (null !== $product->getAttribute('article_id')) {
                    $article = $articleMapper->find($product->getAttribute('article_id'));
                } 
                if (null === $article ) {
                    $article = new News_Model_Article();
                    $article->setTypeId($this->getArticleTypeId());
                    $article->setStream(true);
                    $article->setAttribute('productId', $product->getId());
                }

                $article->setAttribute('keyword', $product->getAttribute('keyword'));
                $article->setTitle($product->getTitle());
                $article->setImage($product->getImage());
                $article->setPublished(true);
                $article->setPublishdate(time());
                $article->setStream(true);
                if (isset($config->news) && isset($config->news->sites)) {
                    $article->setSites($config->news->sites);
                }

                if(trim($author = $product->getAttribute('author'))){
                	$article->setAuthorname($author);
                	$article->setAttribute('author','guest');
                }
                else{
                	$article->setAttribute('author','anonymous');
                }

                $articleMapper->save($article);
                
                // Add the article ID to the product
                $productMapper = Product_Model_Product::getMapper();
                $product->setAttribute('article_id', $article->getId());

                // Disable the observers
                $this->setEnabled(false);
                $productMapper->save($product);
                $this->setEnabled(true);
            } elseif (null !== $product->getAttribute('article_id')) {
                // Previously posted to news stream. Disable it
                $articleMapper = News_Model_Article::getMapper();
                $article = $articleMapper->find($product->getAttribute('article_id'));
                if (null !== $article) {
                    $article->setStream(false);
                    $articleMapper->save($article);
                }
            }
        }
    }

    public function delete(Mcmr_Model_ModelAbstract $product)
    {
        // When a product is deleted, delete the article associated with it
        if (  $product->getTypeId() == $this->getProductTypeId() && null !== $product->getAttribute('article_id')) {
            $articleMapper = News_Model_Article::getMapper();
            $article = $articleMapper->find($product->getAttribute('article_id'));

            if (null !== $article) {
                $articleMapper->delete($article);
            }
        }
    }
}
