<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of DailyDigestSend
 *
 * @category Mcmr
 */
class Intent_Observer_DailyDigestSend extends Mcmr_Service_Adestra_Observer_AdestraSend
{
    private $_email = null;
    
    protected function _getLaunchData($email)
    {
        $this->_email = $email;
        $site = 'default';
        $name = $email->getName();
        
        // Change the timezone so that dates are displayed correctly.
        $origTimezone = Mcmr_Date::getDefaultTimezone();
        if (isset(Zend_Registry::get('mailqueue-config')->adestra->{$name}->timezone)) {
            $timezone = Mcmr_Date::setDefaultTimezone('Australia/Sydney'); // Change timezone to sydney for the email
        }
        
        $parts = explode('-', $name);
        if (2 === count($parts)) {
            list($site, $name) = $parts;
        }
        
        switch ($name) {
            case 'newsflash':
                $data = $this->_newsflash($site);
                break;
            
            case 'dailydigest':
                $data = $this->_dailyDigest($site);
                break;
            
            case 'digital':
                $data = $this->_digitalEdition();
                break;
            
            default:
                $data = array();
                break;
        }
        
        if ('default' !== $site) {
            $data['domain'] = Zend_Registry::get('app-config')->media->{$site}->baseDomain;
        } else {
            $data['domain'] = Zend_Registry::get('app-config')->media->baseDomain;
        }
        
        $data['facebookurl'] = Zend_Registry::get('app-config')->facebookurl;
        $data['twitterurl'] = Zend_Registry::get('app-config')->twitterurl;
        $data['linkedinurl'] = Zend_Registry::get('app-config')->linkedinurl;
        $data['siteshortname'] = Zend_Registry::get('app-config')->siteshortname;
        $data['tagline'] = Zend_Registry::get('app-config')->tagline;
        $data['tagname'] = Zend_Registry::get('app-config')->tagname;
        
        if ('emailtest' === $email->getOptin()) {
            $data['subject'] = '(TEST) ' . $data['subject'];
            $this->_sentStatus = Mailqueue_Model_Mail::STATUS_UNSENT;
        }
        $data['subject'] = html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8');
        
        Mcmr_Date::setDefaultTimezone($origTimezone); // Restore timezone
        
        return $data;
    }
    
    protected function _dailyDigest($site='default')
    {
        $data = array();
        
        // Do not reset the article ordering if this is a test email send
        $data['articles'] = $this->_latestArticles(
            20, $site, true, 
            ('emailtest' !== $this->_email->getOptin())
        );
        $data['subject'] = $this->_dailySubject($this->_email, $data['articles']);
        $data['adverts'] = $this->_dailyAdverts($site);
        $data['products'] = $this->_dailyProducts();
        $data['events'] = $this->_latestEvents(3, $site);
        $data['company'] = $this->_company();
        $data['jobs'] = $this->_jobs($site);
        $data['year'] = date('Y');

        return $data;
    }
    
    protected function _newsflash($site='default')
    {
        $articleMapper = News_Model_Article::getMapper();
        $id = $this->_email->getAttribute('articles');
        if (null !== $id) {
            $article = $articleMapper->findOneByField(array('id'=>$id));
        } else {
            $article = new News_Model_Article();
        }
        
        $data = array();
        switch ($site) {
            case 'mcvpacific':
                $data['domain'] = 'http://www.mcvpacific.com/';
                break;
            
            default :
                $data['domain'] = Zend_Registry::get('app-config')->media->baseDomain;
                break;
        }
        
        $data['subject'] = $this->_email->getSubject();
        $data['article'] = $this->_sanitiseArticle($article);
        $data['leaderboard'] = $this->_getLeaderboard($site);
        $data['articles'] = $this->_latestArticles(12, $site, false);
        $data['jobs'] = $this->_jobs($site);
        $data['company'] = $this->_company();
        $data['year'] = date('Y');
        
        return $data;
    }
    
    protected function _digitalEdition()
    {
        $media = Media_Model_Set::getMapper()->findOneByField(array('url'=>'sponsors'));
        $sponsor = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'digital-sponsor'));
        
        $type = Product_Model_Type::getMapper()->findOneByField(array('url' => 'digital-edition'));
        $report = Product_Model_Product::getMapper()->findOneByField(array( 'type'=>$type->getId()), array( 'createdate'=>'DESC' ));
        
        $data['latest'] = $this->_sanitiseDigitalEditionProduct($report);
        $data['domain'] = Zend_Registry::get('app-config')->media->baseDomain;
        $data['subject'] = $this->_email->getSubject();
        $data['adverts'] = $this->_dailyAdverts();
        $data['year'] = date('Y');
        $data['sponsor'] = false;
        if (is_object($sponsor) && (null !== $sponsor->getFilename())) {
            $data['sponsor'] = $sponsor->toArray();
        }
        
        return $data;
    }
    
    protected function _getLeaderboard($site='default')
    {
        $leaderboardArray = 0;
        
        $url = 'newsflash';
        if ('default' !== $site) {
            $url = $site.'-'.$url;
        }
        
        $media = Media_Model_Set::getMapper()->findOneByField(array('url'=>$url));
        if ($media) {
            $leaderboard = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'leaderboard'));
            $leaderboardArray = $leaderboard->toArray();
        }
        
        return $leaderboardArray;
    }
    
    protected function _dailyAdverts($site)
    {
        $adverts = array();
        $mediaUrl = 'daily-digest';
        $textUrl = 'daily-digest-advertisement';
        if ('default' !== $site) {
            $mediaUrl = $site . '-'.$mediaUrl;
            $textUrl = $site . '-' . $textUrl;
        }
        
        $adverts['textAd'] = Page_Model_Page::getMapper()->findOneByField(array('url'=>$textUrl));
        if (is_object($adverts['textAd'])) {
            $adverts['textAd'] = $adverts['textAd']->toArray();
        }
        
        // Get adverts
        $media = Media_Model_Set::getMapper()->findOneByField(array('url'=>$mediaUrl));
        if ($media) {
            $adverts['leaderboard'] = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'leaderboard'));
            if (is_object($adverts['leaderboard'])) {
                $adverts['leaderboard'] = $adverts['leaderboard']->toArray();
            }
            $adverts['mpu'] = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'mpu'));
            if (is_object($adverts['mpu'])) {
                $adverts['mpu'] = $adverts['mpu']->toArray();
            }
            $adverts['button1'] = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'large-button-1'));
            if (is_object($adverts['button1'])) {
                $adverts['button1'] = $adverts['button1']->toArray();
            }
            $adverts['button2'] = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'large-button-2'));
            if (is_object($adverts['button2'])) {
                $adverts['button2'] = $adverts['button2']->toArray();
            }
            $adverts['button3'] = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'large-button-3'));
            if (is_object($adverts['button3'])) {
                $adverts['button3'] = $adverts['button3']->toArray();
            }
            $adverts['button4'] = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'large-button-4'));
            if (is_object($adverts['button4'])) {
                $adverts['button4'] = $adverts['button4']->toArray();
            }
            $adverts['button5'] = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'large-button-5'));
            if (is_object($adverts['button5'])) {
                $adverts['button5'] = $adverts['button5']->toArray();
            }
        }
        
        // Get sponsors
        $media = Media_Model_Set::getMapper()->findOneByField(array('url'=>'sponsors'));
        if ($media) {
            $adverts['sponsor'] = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'sponsor'));
            if (is_object($adverts['sponsor'])) {
                $adverts['sponsor'] = $adverts['sponsor']->toArray();
            }
            
            $digitalSponsor = Media_Model_File::getMapper()->findOneByField(array('setid'=>$media->getId(), 'url'=>'digital-sponsor'));
            if (is_object($digitalSponsor)) {
                $adverts['digitalSponsor'] = $digitalSponsor->toArray();
            }
        }

        return $adverts;
    }
    
    protected function _dailyProducts()
    {
        $products = array();
        $type = Product_Model_Type::getMapper()->findOneByField(array('url' => 'digital-edition'));
        if (null !== $type) {
            $product = Product_Model_Product::getMapper()->findOneByField(
                array( 'type'=>$type->getId()), array( 'createdate'=>'DESC' )
            );
            if (null!==$product) {
                $products['latest'] = $this->_sanitiseProduct($product);
            }
        }

        $product = Product_Model_Product::getMapper()->findOneByField(array('featured'=>1));
        if (null!==$product) {
            $products['featured'] = $this->_sanitiseProduct($product);
        }
        
        return $products;
    }
    
    protected function _latestArticles($count, $site='default', $specificOrdering = true, $resetOrdering=true)
    {
        $latestArticlesArray = array();
        
        if ($specificOrdering) {
            $orderBy = 'dailydigest';
            if ('default' !== $site) {
                $orderBy = $site.'_dailydigest';
            }
            $order = array('ordr_'.$orderBy=>'desc', 'order'=>'asc','publishdate' => 'desc');
        } else {
            $order = array('order'=>'asc','publishdate' => 'desc');
        }
        
        $id = $this->_email->getAttribute('articles');
        $latestArticles = News_Model_Article::getMapper()->findAllByField(
            array('stream'=>'1', 'published'=>'1','id'=>array('condition'=>'<>', 'value'=>$id), 'sites'=>$site),
            $order,
            array('page'=>1, 'count'=>$count)
        );
        
        foreach ($latestArticles as $article) {
            $latestArticlesArray[] = $this->_sanitiseArticle($article);
        }
        
        if ($resetOrdering && $specificOrdering) {
            // Data collected, reset the daily digest article ordering
            $mapper = News_Model_Article::getMapper();
            $mapper->resetOrdering($orderBy);

            $this->_view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            $this->_view->getHelper('DisplayMessages')->addMessage('Article Order Reset', 'info');
        }
        
        
        return $latestArticlesArray;
    }
    
    protected function _latestEvents($count, $site='default')
    {
        $latestEventsArray = array();
        
        if ('default' !== $site) {
            $order = array('ordr_'.$site.'_eventssidepanel'=>'desc', 'publishdate'=>'desc');
        } else {
            $order = array('ordr_eventssidepanel'=>'desc', 'publishdate'=>'desc');
        }
        
        $type = News_Model_Type::getMapper()->findOneByField(array('url'=>'events'));
        $latestEvents = News_Model_Article::getMapper()->findAllByField(
            array('published'=>'1', 'typeid'=>$type->getId()), 
            $order, 
            array('page'=>1, 'count'=>$count)
        );
        
        foreach ($latestEvents as $article) {
            $latestEventsArray[] = $this->_sanitiseArticle($article);
        }
        
        return $latestEventsArray;
    }
    
    protected function _jobs($site='default')
    {
        if ('default' !== $site) {
            $orderKey = $site.'_spotlight';
        } else {
            $orderKey = 'spotlight';
        }
        $order = array('ordr_'.$orderKey=>'desc');
        
        $jobsArray = array();
        $jobs = Job_Model_Job::getMapper()->findAllByField(
            array('published'=>1), $order, array('page'=>1, 'count'=>8)
        );
        
        foreach ($jobs as $job) {
            if (!$job->getOrder($orderKey)) {
                break;
            }
            $jobArray = $job->toArray();
            $jobArray['company'] = $this->_sanitiseCompany($job->getCompany());
            $jobArray['location'] = $job->getLocation()?$job->getLocation()->getTitle():'';
            $jobsArray[] = $jobArray;
        }

        if (!empty($jobsArray)) {
            return $jobsArray;
        } else {
            return 0;
        }
    }
    
    protected function _company($useDates = false)
    {
        $conditions = array(
            'featured'=>1,
        );

        if ($useDates) {
            $conditions['featuredstart'] = array('condition'=>'<=', 'value'=>time());
            $conditions['featuredend'] = array('condition'=>'>', 'value'=>time());
        }

        $order = array('order'=>'asc');

        $company = Company_Model_Company::getMapper()->findOneByField($conditions, $order);
        $companyArray = $this->_sanitiseCompany($company);
        
        return $companyArray;
    }
    
    protected function _sanitiseArticle($article)
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $filter = new Mcmr_Filter_Utf8();
        
        $articleArray = $article->toArray();
        unset($articleArray['createdate']);
        unset($articleArray['published']);
        unset($articleArray['featured']);
        unset($articleArray['userid']);
        unset($articleArray['userIdAuthor']);
        unset($articleArray['authorname']);
        unset($articleArray['authoremail']);
        unset($articleArray['typeid']);
        unset($articleArray['categoryids']);
        unset($articleArray['categorytitles']);
        unset($articleArray['order']);
        unset($articleArray['stream']);
        unset($articleArray['strap']);
        unset($articleArray['sites']);
        unset($articleArray['tags']);
        unset($articleArray['attributeTypes']);
        unset($articleArray['state']);
        if (array_key_exists('anonymous', $articleArray)) {
            unset($articleArray['anonymous']);
        }
        if (array_key_exists('release_id', $articleArray)) {
            unset($articleArray['release_id']);
        }
        
        $author = $article->getUserAuthor();
        $articleArray['author']['name'] = $author ? 'by ' . $author->getFirstname() . " " . $author->getSurname() : '';
        $articleArray['publishdate'] = Mcmr_StdLib::dateToString('M jS Y g.ia', $articleArray['publishdate']);
        $articleArray['content'] = $filter->filter(
            strip_tags(
                $view->formatString()->summarise( $article->getContent(), 20, 1 ), 
                "<p><br><i><b><li><ul><ol>"
            )
        );
        $articleArray['url'] = $article->getUrl();
        $articleArray['type'] = $article->getType()->getUrl();
        
        switch ($articleArray['type']) {
            case 'report':
                $report = Product_Model_Product::getMapper()->findOneByField(array('id'=>$article->getAttribute('product_id')));
                if (null !== $report) {
                    $articleArray['report'] = $report->toArray();
                    $articleArray['report']['description'] = $view->formatString()->summarise( $report->getDescription(), 20, 1 );
                    $articleArray['report']['amount'] = $view->format()->currency($report->getAmount()/100);
                }
                break;
            case 'job':
                $job = Job_Model_Job::getMapper()->findOneByField(array('id'=>$article->getAttribute('job_id')));
                if (null !== $job) {
                    $articleArray['job'] = $job->toArray();
                    $articleArray['job']['description'] = $filter->filter(
                        $view->formatString()->summarise( $job->getDescription() , 50, 1 )
                    );
                    $company = $job->getCompany();
                    if (null !== $company) {
                        $articleArray['job']['company'] = $company->toArray();
                    }
                    $location = $job->getLocation();
                    if (null !== $location) {
                        $articleArray['job']['location'] = $location->getTitle();
                    }
                }
                break;
                
            case 'events':
                $articleArray['day'] = date('d', $article->getAttribute('eventStart'));
                $articleArray['month'] = strtoupper(date('M', $article->getAttribute('eventStart')));
                $articleArray['eventStart'] = Mcmr_StdLib::dateToString('M jS Y', $article->getAttribute('eventStart'));
                $articleArray['eventEnd'] = Mcmr_StdLib::dateToString('M jS Y', $article->getAttribute('eventEnd'));
                $articleArray['location'] = $filter->filter($articleArray['location']);
                break;
            
            case "pricecheck":
                $pricecheck = Pricecheck_Model_Pricecheck::getMapper()->findOneByField(array('id'=>$article->getAttribute('pricecheck_id')));
                if (null !== $pricecheck) {
                    $articleArray['pricecheck'] = $pricecheck->toArray();
                    $products = $pricecheck->getProducts();
                    $articleArray['products'] = array();
                    foreach ($products as $product) {
                        $articleArray['products'][] = $product->toArray();
                    }
                    $game = array_shift( $products );
                    if (null !== $game) {
                        $articleArray['game'] = $game->toArray();
                        $articleArray['game']['platforms'] = $game->getGamePlatforms();
                        $bestOnline = $pricecheck->bestOnlinePrice($game->getId(), $game->getAttribute('platformid'));
                        if (null !== $bestOnline) {
                            $articleArray['game']['bestOnline'] = $bestOnline->toArray();
                            $articleArray['game']['bestOnline']['price'] = '&pound;'.$bestOnline->getPrice();
                            $articleArray['game']['bestOnline']['platform'] = $bestOnline->getPlatform()->getTitle();
                        }
                        $bestStore = $pricecheck->bestStorePrice($game->getId(), $game->getAttribute('platformid'));
                        if (null !== $bestStore) {
                            $articleArray['game']['bestStore'] = $bestStore->toArray();
                            $articleArray['game']['bestStore']['price'] = '&pound;'.$bestStore->getPrice();
                            $articleArray['game']['bestStore']['platform'] = $bestStore->getPlatform()->getTitle();
                        }
                        
                        $developer = $game->getDeveloper();
                        if (null !== $developer) {
                            $articleArray['game']['developer']['title'] = $developer->getTitle();
                        }
                    }
                }
                break;
                
            case "chart":
                $chart = Chart_Model_Chart::getMapper()->find( $article->getAttribute('chart_id') );
                if (null !== $chart) {
                    $platform = $chart->getPlatform();
                }
                $articleArray['chart'] = $chart->toArray();
                $articleArray['chart']['type'] = $chart->getType()->getTitle();

                unset($articleArray['chart']['platformid']);
                unset($articleArray['chart']['attributeTypes']);
                unset($articleArray['chart']['state']);
                unset($articleArray['chart']['article_id']);
                unset($articleArray['chart']['fromdate']);
                unset($articleArray['chart']['todate']);
                
                $count = 1;
                $articleArray['chart']['entries'] = array();
                foreach( $chart->getEntries() as $entry ) {
                    $game = $entry->getGame();
                    if (null !== $game) {
                        // only show 5 chart entries
                        if ($count > 3) break;
                        $articleArray['chart']['entries'][] = $game->getTitle();
                    }
                    $count++;
                }
                
                break;
                
            case 'themed-playlist':
            case 'music-week-presents':
            case 'playlist':
                $playlist = Playlist_Model_Playlist::getMapper()->find($article->getAttribute('playlist_id'));
                if (null !== $playlist) {
                    $articleArray['playlist'] = $playlist->toArray();
                    $articleArray['entries'] = array();
                    $entriesText = array();
                    $count = 0;
                    foreach ($playlist->getEntries() as $entry) {
                        // Limit the number of entries to 5
                        if (5 === $count) {
                            break;
                        }
                        $entryData = $entry->getTrack()->toArray();
                        $entryData['position'] = $entry->getPosition();
                        $articleArray['entries'][] = $entryData;
                        $entriesText[] = $entryData['artist'];
                        $count++;
                    }
                    $articleArray['entriesText'] = implode(' / ', $entriesText);
                }
                break;
        }
        
        $filter = new Mcmr_Filter_Utf8();
        $articleArray = (array)$articleArray;
        
        return $filter->filter($articleArray);
    }
    
    protected function _sanitiseCompany($company)
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $filter = new Mcmr_Filter_Utf8();
        
        $companyArray = 0;
        if (null !== $company) {
            $companyArray = $company->toArray();
            $companyArray['description'] = $filter->filter(
                $view->formatString()->summarise($company->getDescription(), 10)
            );
        }
        
        return $companyArray;
    }
    
    protected function _sanitiseProduct($product)
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        
        $productArray = $product->toArray();
        $productArray['description'] = $view->formatString()->summarise ( $product->getDescription(), 10, 1 );
        $productArray['url'] = $view->url(array('module'=>'product', 'controller'=>'index', 'action'=>'read', 'url'=>$product->getUrl()), 'default', true);
        $productArray['createdate'] = Mcmr_StdLib::dateToString('F jS, Y', $product->getCreatedate());
        
        return $productArray;
    }
    
    protected function _sanitiseDigitalEditionProduct($product)
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        
        $productArray = $product->toArray();
        $productArray['url'] = $view->url(array('module'=>'product', 'controller'=>'index', 'action'=>'read', 'url'=>$product->getUrl()), 'default', true);
        $productArray['createdate'] = Mcmr_StdLib::dateToString('d/m/Y', $product->getCreatedate());
        
        return $productArray;
    }
    
    private function _dailySubject(Mailqueue_Model_Mail $email, $articles)
    {
        $keywords = array();
        for ($x=0; $x<3; $x++) {
            if (!isset($articles[$x])) {
                break;
            }
            $article = $articles[$x];
            
            if (!empty ($article['keyword'])) {
                $keywords[] = $article['keyword'];
            }
        }

        $subject = Zend_Registry::get('mailqueue-config')->adestra->{$email->getName()}->subject;
        return str_replace('%keywords%', implode(", ", $keywords), $subject);
    }
}
