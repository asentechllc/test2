<?php

class Intent_Observer_UserConfirm extends Mcmr_Model_ObserverAbstract
{
    static $originalUser = null;

    public function preUpdate($user)
    {
        self::$originalUser = clone $user;
    }

    public function update($user)
    {
        if ('registered' === $user->getState() && 'unconfirmed' === self::$originalUser->getState()) {
            $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            $view = clone $frontView;
            $view->setScriptPath(SITE_PATH . '/views/user/scripts/');
            $view->addScriptPath(SITE_PATH . '/views/_common');
            $view->addScriptPath(SITE_PATH . '/_common');
            $view->user = $user;

            $template = 'email/user-approved.phtml';
            $sites = $user->getSites();
            if (count($sites)==1 && 'default'!=$sites[0]) {
                $template = $sites[0].'/'.$template;
            } 

            $body = $view->render($template);

            // Get the email subject
            $config = Zend_Registry::get('user-config');
            if (isset($config->email->approved->subject)) {
                $subject = $config->email->approved->subject;
            } else {
                $subject = "User Approved";
            }

            $email = new Mcmr_Mail();
            $email->setBodyHtml($body);
            $email->setSubject($subject);
            $email->addTo($user->getEmail());
            $email->send();
        }
    }

}
