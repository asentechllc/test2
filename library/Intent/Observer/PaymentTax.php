<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Observer to determine whether the user should be charged VAT on their payment
 *
 * @category Mcmr
 */
class Intent_Observer_PaymentTax extends Mcmr_Model_ObserverAbstract
{
    
    public function preInsert($payment)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $postData = $request->getPost();
        
        if (isset ($postData['paymentformpayment'])
            && isset($postData['paymentformpayment']['address'])
            && isset($postData['paymentformpayment']['address']['taxIncluded'])
            ) {
            $taxExempt = ('1' != $postData['paymentformpayment']['address']['taxIncluded']);
            $payment->setTaxExempt($taxExempt);
            
            $user = $payment->getBasket()->getUser();
            if (null !== $user) {
                $user->setAttribute('taxExempt', $taxExempt);
                User_Model_User::getMapper()->save($user);
            }
        }
    }
}
