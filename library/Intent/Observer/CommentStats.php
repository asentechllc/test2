<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CommentStats
 *
 * @author leigh
 */
class Intent_Observer_CommentStats extends Mcmr_Model_ObserverAbstract
{
    private static $_published = null;

    protected function getStatParams($comment)
    {
        $thread = $comment->getThread();
        $timestamp = Mcmr_StdLib::time($comment->getCreatedate(), 'day');
        list($modeltype, $modelid) = explode('::', $thread->getName());
        $params = array(
            'modeltype' => $modeltype,
            'modelid' => $modelid, 'date' => $timestamp, 'action' => 'comment'
        );
        return $params;
    }

    public function insert($comment)
    {
        $params = $this->getStatParams($comment);

        if ($comment->isPublished()) {
            $statMapper = Default_Model_Stat::getMapper();
            $stat = $statMapper->findOneByField($params);
            $stat->increment();
            $statMapper->save($stat);
        }

        // Send an email to all people who have commented on this
        $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        $view = clone $frontView;
        $view->setScriptPath(SITE_PATH . '/views/comment/scripts/');
        $view->addScriptPath(SITE_PATH . '/views/_common');
        $view->addScriptPath(SITE_PATH . '/_common');
        $view->comment = $comment;

        $mapper = call_user_func(array($params['modeltype'], 'getMapper'));
        $article = $mapper->find($params['modelid']);

        $view->article = $article;


        $this->_emailRegistered($view, $comment);
        $this->_emailAdmin($view, $comment);
    }

    public function preUpdate($comment)
    {
        self::$_published = $comment->isPublished();
    }

    public function update($comment)
    {
        $params = $this->getStatParams($comment);
        $statMapper = Default_Model_Stat::getMapper();
        $stat = $statMapper->findOneByField($params);
        if (self::$_published && !$comment->isPublished()) {
            $stat->decrement();
            if ($stat->getCount() > 0) {
                $statMapper->save($stat);
            } else {
                $statMapper->delete($stat);
            }
        } else if (!self::$_published && $comment->isPublished()) {
            $stat->increment();
            $statMapper->save($stat);
        }
    }

    public function delete($comment)
    {
        if ($comment->isPublished()) {
            $params = $this->getStatParams($comment);
            $statMapper = Default_Model_Stat::getMapper();
            $stat = $statMapper->findOneByField($params);
            $stat->decrement();
            if ($stat->getCount() > 0) {
                $statMapper->save($stat);
            } else {
                $statMapper->delete($stat);
            }
        }
    }

    private function _emailRegistered($view, $comment)
    {
        $thread = $comment->getThread();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $siteName = $request->getSiteName();
        if ('default' === $siteName) {
            $body = $view->render('email/comment-notification.phtml');
        } else {
            $body = $view->render($siteName.'/email/comment-notification.phtml');
        }
        // Get the email subject
        $config = Zend_Registry::get('comment-config');
        if (isset($config->email->notification->subject)) {
            $subject = $config->email->notification->subject;
            $subject = str_replace('%title%', $comment->getTitle(), $subject);
        } else {
            $subject = "New Comment";
        }

        $authenticatedUser = Zend_Registry::get('authenticated-user');
        $authUserId = $authenticatedUser->getId();

        $thread = $comment->getThread();

        $conditions = array();
        $conditions['modeltype'] = get_class($thread);
        $conditions['modelid'] = $thread->getId();
        $mapper = Changelog_Model_Follow::getMapper();
        $follows = $mapper->findAllByField($conditions, null, array('page' => 1, 'count' => 10000));
        $sentTo = array();
        foreach ($follows as $follow) {
            if (($authUserId != $follow->getUserid())) {
                $user = User_Model_User::getMapper()->find($follow->getUserid());
                if (!in_array($user->getEmail(), $sentTo)) {
                    $email = new Mcmr_Mail();
                    $email->setBodyHtml($body);
                    $email->setSubject($subject);
                    $email->addTo($user->getEmail());
                    $email->send();
                    $sentTo[] = $user->getEmail();
                }
            }
        }
    }

    private function _emailAdmin($view, $comment)
    {
        $body = $view->render('email/comment-notification-admin.phtml');

        $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
        $config = Zend_Registry::get('comment-config');
        
        if (isset($config->$site)) {
            $config = $config->$site;
        }
        
        // Get the email subject
        if (isset($config->email->notification->subject)) {
            $subject = $config->email->notification->subject;
            $subject = str_replace('%title%', $comment->getTitle(), $subject);
        } else {
            $subject = "New Comment";
        }

        $email = new Mcmr_Mail();
        $email->setBodyHtml($body);
        $email->setSubject($subject);
        $addresses = $config->email->notification->admin->toArray();
        $email->addTo($addresses);
        $email->send();
    }

}
