<?php

class Intent_Observer_BasketCheck extends Mcmr_Model_ObserverAbstract
{
    /**
     * Called when a session basket (guest user) is merged with a database basket
     * (on user login). Here we can check every basket item and ensure they are
     * updated for newly logged in user.
     *
     * @param Payment_Model_Basket $basket
     */
    public function merge($basket)
    {
        $items = $basket->getItems();
        $user = Zend_Registry::get('authenticated-user');
        $jobMapper = Job_Model_Job::getMapper();

        foreach ($items as $item) {
            $model = $item->getModel();
            if ($model instanceof Job_Model_Job) {
                // We have a job. Ensure the user ID is correct
                if (null === $model->getUserid()) {
                    $model->setUserid($user->getId());
                    $jobMapper->save($model);
                }
            }
        }
    }
    
    /**
     * Called when a basket is loaded. Check that any company products are valid
     *
     * @param Payment_Model_Basket $basket
     */
    public function load($basket)
    {
        $this->_sanitiseBasket($basket);
    }

    public function update($basket)
    {
        $this->_sanitiseBasket($basket);
    }

    public function insert($basket)
    {
        $this->_sanitiseBasket($basket);
    }

    public function session($basket)
    {
        $this->_checkHasListing($basket);
    }

    private function _sanitiseBasket($basket)
    {
        if ($basket instanceof Payment_Model_Basket) {
            // We don't need to sanitise a basket if it has already been purchased
            if ('OK' === $basket->getStatus()) {
                return;
            }

            $user = $basket->getUser();
            $listingType = Product_Model_Type::getMapper()->findOneByField(array('url'=>'listing'));
            $companyid = $user->getAttribute('companyid');

            if (empty($companyid)) {
                $company = Company_Model_Company::getMapper()->findOneByField(array('userid'=>$user->getId()));
                if (null !== $company) {
                    $companyid = $company->getId();
                    // Bad save on user for some reason. Fix user
                    $user->setAttribute('companyid', $companyid);
                    User_Model_User::getMapper()->save($user);
                }
            }
            
            if (null !== $companyid) {
                $company = Company_Model_Company::getMapper()->find($companyid);
                $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');

                // Check the user doesn't already have a listed company
                if (null !== $company && $company->getListed() && time() < $company->getAttribute('listingExpire')) {
                    foreach ($basket->getItems() as $item) {
                        $product = $item->getModel();
                        if ($listingType && $product instanceof Product_Model_Product
                                && $product->getTypeid() === $listingType->getId()) {
                            // The user already has a listed company. Remove this from the basket
                            Payment_Model_Item::getMapper()->delete($item);

                            // Throw exception to stop other messages
                            throw new Zend_Exception('companyAlreadyListed');
                        }
                    }
                }

            } else {
                foreach ($basket->getItems() as $item) {
                    $product = $item->getModel();
                    if ($listingType && $product instanceof Product_Model_Product && $product->getTypeid() === $listingType->getId()) {
                        // The user already has a listed company. Remove this from the basket
                        Payment_Model_Item::getMapper()->delete($item);

                        // Throw exception to stop other messages
                        throw new Zend_Exception('noUserCompany');
                    }
                }
            }
            
            $this->_checkHasListing($basket);
        }
    }

    private function _checkHasListing($basket)
    {
        if ($basket instanceof Payment_Model_Basket) {
            $listingType = Product_Model_Type::getMapper()->findOneByField(array('url'=>'listing'));
            
            if($listingType){

				// Don't allow them to purchase more than one listing
				$hasListing = false;
				$items = $basket->getItems();
				foreach ($items as $key=>$item) {
					$product = $item->getModel();
					if ($product instanceof Product_Model_Product && $product->getTypeid() === $listingType->getId()) {
						if ($hasListing) {
							// The user is associated with a company, but not the owner.
							Payment_Model_Item::getMapper()->delete($item);

							unset($items[$key]);
							$basket->setItems($items);
							Payment_Model_Basket::getMapper()->save($basket);

							// Throw exception to stop other messages
							throw new Zend_Exception('listingInBasket');
						} else {
							$hasListing = true;
						}
					}
				}
			}
        }
    }
}
