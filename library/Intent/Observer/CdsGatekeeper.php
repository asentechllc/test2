<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Observer for authenticating users against CDS Gatekeeper
 *
 * @category Intent
 * @package Intent_Observer
 */
class Intent_Observer_CdsGatekeeper extends Mcmr_Model_ObserverAbstract
{
    /**
     * Authenticated mapper hook. Check the user's CDS subscription and adjust
     * as appropriate
     * 
     * @param User_Model_User $user 
     */
    public function authenticated($user)
    {
        if (is_object($user) && $user instanceof User_Model_User) {
            $this->_adjustUser($user);
        }
    }
    
    /**
     * Update mapper hook. Check the user's CDS subscription and adjust
     * as appropriate
     *
     * @param User_Model_User $user 
     */
    public function update($user)
    {
        if (is_object($user) && $user instanceof User_Model_User) {
            $this->_adjustUser($user);
        }
    }
    
    public function load($user)
    {
        if (is_object($user) && $user instanceof User_Model_User) {
            $this->_adjustUser($user);
        }
    }
    
    /**
     * Adjust the user permission according to their CDS subscription status
     * 
     * @param User_Model_User $user
     * @param type $forceCheck
     */
    protected function _adjustUser(User_Model_User $user, $forceCheck = false)
    {
        $config = $this->getConfig();

        if (isset($config->ignoreRoles)) {
            foreach ($config->ignoreRoles as $ignoreRole) {
                if ($user->getRole() === $ignoreRole) {
                    // User of this role does not need to be validated
                    return true;
                }
            }
        }
        
        if ($user->getAttribute('cdsDisabled')) {
            // This user has their CDS Subscription check disabled.
            return true;
        }

        $lastCheck = $user->getAttribute('cdsLastCheck');
        $currentRole = $user->getRole();
        $timeout = time() - $config->timeout;
        if ((null === $lastCheck) || 
            ($lastCheck < $timeout) || 
            ($currentRole == $config->invalidRole) ||
            ($forceCheck)) {
            Mcmr_Debug::dump("cds check");
            $gatekeeper = new Intent_Service_CdsGatekeeper($this->getConfig()->originator);
            try {
                if ($gatekeeper->subscriptionValid($user)) {
                    $role = $config->validRole;
                } else {
                    $role = $config->invalidRole;
                }

                $user->setRole($role);
                $user->setAttribute('cdsLastCheck', time());

                $mapper = User_Model_User::getMapper();
                $mapper->setObserversEnabled(false);
                $mapper->save($user);
                $mapper->setObserversEnabled(true);
            } catch (Exception $e) {
                Mcmr_Debug::dump($e);
            }
        }
    }
}
