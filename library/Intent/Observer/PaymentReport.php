<?php

class Intent_Observer_PaymentReport extends Mcmr_Model_ObserverAbstract
{
    static public $disable = false;

    /**
     * Executed when a payent is updated
     * 
     * @param Payment_Model_Payment $model
     */
    public function update($model)
    {
        if (self::$disable) {
            return true;
        }
        // If a successful payment
        if ($model->getStatus() === Mcmr_Payment_AdapterAbstract::STATUS_OK) {
            $typeMapper = Product_Model_Type::getMapper();
            $type = $typeMapper->findOneByField(array('url'=>'report'));

            // Disable the observers on the basket mapper. Prevent 'load' observers
            Payment_Model_Basket::getMapper()->setObserversEnabled(false);
            $basket = $model->getBasket();
            Payment_Model_Basket::getMapper()->setObserversEnabled(true);

            $items = $basket->getItems();
            foreach ($items as $item) {
                $report = $item->getModel();
                if ($report instanceof Product_Model_Product && $report->getTypeid() === $type->getId()) {
                    // Find the report this user owns.
                    $user = $basket->getUser();

                    $item->setRedeem(0);
                    Payment_Model_Item::getMapper()->save($item);

                    // Send the confirmation email
                    $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
                    $view = clone $frontView;
                    $view->setScriptPath(SITE_PATH . '/views/payment/scripts/');
                    $view->addScriptPath(SITE_PATH.'/views/_common');
                    $view->addScriptPath(SITE_PATH.'/_common');
                    $view->report = $report;
                    $view->user = $user;
                    $view->basket = $basket;
                    $view->item = $item;
                    $view->payment = $model;
                    
                    $request = Zend_Controller_Front::getInstance()->getRequest();
                    $siteName = $request->getSiteName();
                    if ('default' === $siteName) {
                        $body = $view->render('email/payment-notification-report.phtml');
                    } else {
                        $body = $view->render($siteName.'/email/payment-notification-report.phtml');
                    }

                    // Get the email subject
                    $config = Zend_Registry::get('payment-config');
                    if (isset($config->email->notification->report->subject)) {
                        $subject = $config->email->notification->report->subject;
                        $subject = str_replace('%title%', $report->getTitle(), $subject);
                    } else {
                        $subject = "Report Purchase Complete";
                    }

                    if (null !== $user) {
                        $emailAddress = $user->getEmail();
                    } else {
                        $emailAddress = $basket->getAddress()->getAttribute('email');
                    }
                    
                    $email = new Mcmr_Mail();
                    $email->setBodyHtml($body);
                    $email->setSubject($subject);
                    $email->addTo($emailAddress);
                    if (isset($config->email->notification->report->admin)) {
                        $email->addBcc($config->email->notification->report->admin->toArray());
                    }

                    $email->send();
                    
                }
            }
        }
    }
    
    protected function _sendNotifiction()
    {
        
    }
}
