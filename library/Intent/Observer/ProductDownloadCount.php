<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of ProductDownloadCount
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Intent_Observer_ProductDownloadCount extends Mcmr_Model_ObserverAbstract
{
    protected static $_recorded = false;

    public function load($product)
    {
        $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
        if ('product' === $view->Request()->getParam('module')
             && 'index' === $view->request()->getParam('controller')
             && 'download' === $view->request()->getParam('action')) {

            if (!self::$_recorded) {
                $view->recordStat($product, 'download');
                
                self::$_recorded = true;
            }
        }
    }
}
