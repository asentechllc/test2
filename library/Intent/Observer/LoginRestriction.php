<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Observer for authenticating users against CDS Gatekeeper
 *
 * @category Intent
 * @package Intent_Observer
 */
class Intent_Observer_LoginRestriction extends Mcmr_Model_ObserverAbstract
{
    const FIELD_LOGINCOUNT = 'loggedInCount';
    const FIELD_MAXLOGGEDIN = 'maxLoggedIn';
    const FIELD_LASTLOGIN = 'cdsLastLogin';
    const DEFAULT_TIMEOUT = 3660;
    
    public function login($user)
    {
        if (is_object($user) && $user instanceof User_Model_User) {
            $config = $this->getConfig();
            
            $maxLoggedIn = $user->getAttribute(self::FIELD_MAXLOGGEDIN);
            if (!$maxLoggedIn) {
                $maxLoggedIn = 1;
            }
            
            $loggedInCount = $user->getAttribute(self::FIELD_LOGINCOUNT);
            if (!$loggedInCount) {
                $loggedInCount = 1;
            } else {
                $loggedInCount++;
            }
            
            $loggedOut = false;
            if ($loggedInCount > $maxLoggedIn) {
                $timeout = isset($config->timeout)?$config->timeout:self::DEFAULT_TIMEOUT;
                if ($user->getAttribute(self::FIELD_LASTLOGIN)+$timeout > time()) {
                    $this->setEnabled(false);
                    User_Model_User::getMapper()->logout();
                    $this->setEnabled(true);

                    $user = new User_Model_User();
                    $user->setRole('guest');
                    Zend_Registry::set('authenticated-user', $user);

                    throw new User_Model_Exception_LoginFailed('loginFailedLoginRestriction');

                    $view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
                    $view->getHelper('DisplayMessages')->addMessage('loginFailedLoginRestriction', 'warn');
                    $view->redirectNow(
                        array('module'=>'user', 'controller'=>'index','action'=>'login'), 'default'
                    );
                    
                    $loggedOut = true;
                }
            }
            if (!$loggedOut) {
                $user->setAttribute(self::FIELD_LOGINCOUNT, $loggedInCount);
                $user->setAttribute(self::FIELD_LASTLOGIN, time());
                
                $this->setEnabled(false);
                User_Model_User::getMapper()->save($user);
                $this->setEnabled(true);
            }
        }
    }
    
    public function logout($user)
    {
        if (is_object($user) && $user instanceof User_Model_User) {
            $loggedInCount = $user->getAttribute(self::FIELD_LOGINCOUNT);
            if ($loggedInCount) {
                $loggedInCount--;
            } else {
                $loggedInCount = 0;
            }
            $user->setAttribute(self::FIELD_LOGINCOUNT, $loggedInCount);
            $user->setAttribute(self::FIELD_LASTLOGIN, 0);
            
            $this->setEnabled(false);
            User_Model_User::getMapper()->save($user);
            $this->setEnabled(true);
        }
    }
}
