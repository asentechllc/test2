<?php

class Intent_Observer_CompanyApprove extends Mcmr_Model_ObserverAbstract
{
    static $originalCompany = null;

    public function preUpdate($company)
    {
        self::$originalCompany = clone $company;
    }

    public function update($company)
    {
        if ('approved' === $company->getState() && 'unapproved' === self::$originalCompany->getState()) {
            $user = $company->getUser();
            if (null !== $user) {
                $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
                $view = clone $frontView;
                $view->setScriptPath(SITE_PATH . '/views/company/scripts/');
                $view->addScriptPath(SITE_PATH . '/views/_common');
                $view->addScriptPath(SITE_PATH . '/_common');
                $view->company = $company;
                $body = $view->render('email/company-approved.phtml');

                // Get the email subject
                $config = Zend_Registry::get('company-config');
                if (isset($config->email->approved->subject)) {
                    $subject = $config->email->approved->subject;
                } else {
                    $subject = "Company Approved";
                }

                $email = new Mcmr_Mail();
                $email->setBodyHtml($body);
                $email->setSubject($subject);
                $email->addTo($user->getEmail());
                $email->send();
            }
        }
    }

}
