<?php
/**
 *
 */
class Intent_Observer_ArticleNotify extends Mcmr_Model_ObserverAbstract
{
    static private $_published = null;

    public function insert($article)
    {
        if ($article instanceof News_Model_Article) {
            // Only create the follow if the article is published
            if ($article->isPublished()) {
                // Create a notification for everyone associated with the article's companies
                $this->_addFollow($article, $article->getAttribute('company_ids'));
            }
        }
    }

    public function preUpdate($article)
    {
        if ($article instanceof News_Model_Article) {
            self::$_published = $article->isPublished();
        }
    }

    public function update($article)
    {
        if ($article instanceof News_Model_Article) {
            // If the article is being published add the user's follow
            if ($article->isPublished() && self::$_published !== $article->isPublished()) {
                $this->_addFollow($article, $article->getAttribute('company_ids'));
            }
        }
    }

    private function _addFollow(News_Model_Article $article, $companyIds)
    {
        $userMapper = User_Model_User::getMapper();
        $followMapper = Changelog_Model_Follow::getMapper();
        $commentMapper = Comment_Model_Thread::getMapper();

        // Follows are on the article thread. Fetch and attach to that
        $thread = $commentMapper->findOrCreateThread($article);

        foreach ($companyIds as $id) {
            if (!empty($id)) {
                $users = $userMapper->findAllByField(array('attr_companyid'=>$id));
                foreach ($users as $user) {
                    // Create the follow entry.
                    $follow = new Changelog_Model_Follow();
                    $follow->setModeltype(get_class($thread));
                    $follow->setModelid($thread->getId());
                    $follow->setUpdates(1);
                    $follow->setUserid($user->getId());

                    $followMapper->save($follow);
                }
            }
        }
    }
}
