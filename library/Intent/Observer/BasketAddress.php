<?php

class Intent_Observer_BasketAddress extends Mcmr_Model_ObserverAbstract
{
    /** 
     * Called when a basket is inserted into dataabase. 
     * If a user is logged in at this point the details already present in their account will be copied over to billing address.
     */
    public function insert($basket)
    {
        $user = Zend_Registry::get('authenticated-user');
        if (null !== $user && 'guest' !== $user->getRole() && null==$basket->getAddress('billing')) {
            $address = new Payment_Model_Address();
            $address->setFirstname($user->getFirstname());
            $address->setSurname($user->getSurname());
            $address->setAttribute('email', $user->getEmail());
            $address->setCompany($user->getAttribute('company'));
            $address->setCountry($user->getAttribute('country'));
            $address->setType('billing');

            $address->setAddress1('');
            $address->setAddress2('');
            $address->setPostcode('');
            $address->setCity('');
            $address->setPhone('');

            $basket->setAddresses(array($address));

            $basketMapper = Payment_Model_Basket::getMapper();
            $this->setEnabled(false);
            $basketMapper->save($basket);
            $this->setEnabled(true);
        }
    }

}
