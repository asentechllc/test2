<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Observer for creating News article if requested
 *
 * @category Intent
 * @package Intent_Observer
 */
class Intent_Observer_CampaignPublishDate extends Mcmr_Model_ObserverAbstract
{

    protected $_articleTypeId;
    
    protected function getCampaignTypeId()
    {
        if (null == $this->_articleTypeId) {
            $typeMapper = News_Model_Type::getMapper();
            $type = $typeMapper->findOneByField(array('url'=>'campaign'));
            if ( null !== $type ) {
                $this->_articleTypeId = $type->getId();
            }
        }
        return $this->_articleTypeId;
    }

    public function update(Mcmr_Model_ModelAbstract $article )
    {
        $front = Zend_Controller_Front::getInstance();
        $request = $front->getRequest();
        $postData = $request->getPost();

        if (  $article->getTypeId() == $this->getCampaignTypeId() && '1' == $postData['newsformarticle']['stream'] ) {
            $article->setPublishdate(time());
            $articleMapper = News_Model_Article::getMapper();
            $this->setEnabled(false);
            $articleMapper->save($article);
            $this->setEnabled(true);
        }
    }

}
