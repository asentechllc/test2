<?php
/**
 * This works exactly the same for games and companies
 * @see ArticleCompanyJoin
 */
class Intent_Observer_ArticleGameJoin extends Mcmr_Model_ObserverAbstract
{

    static private $_preUpdate = null;

    public function insert($article)
    {
        if ($article instanceof News_Model_Article && $article->isPublished()) {
            $this->_addArticle($article, $article->getAttribute('game_ids'));
        }
    }
    
    
    public function preUpdate($article) 
    {
        self::$_preUpdate = clone $article;
    }

    public function update($article)
    {
        if ($article instanceof News_Model_Article) {
            if ($article->isPublished() ) {
                $this->_removeArticle($article, self::$_preUpdate->getAttribute('game_ids'));
                $this->_addArticle($article, $article->getAttribute('game_ids'));
            } else {
                $this->_removeArticle($article, self::$_preUpdate->getAttribute('game_ids'));
            }
        }
    }
    
    public function delete($article)
    {
        if (is_object(self::$_preUpdate)) {
            $this->_removeArticle($article, self::$_preUpdate->getAttribute('game_ids'));
        }
    }

    private function _addArticle(News_Model_Article $article, $gameIds)
    {
        if (!is_array($gameIds)) {
            return;
        }
        $articleId = $article->getId();
        $gameMapper = Game_Model_Game::getMapper();
        foreach ($gameIds as $id) {
            $game = $gameMapper->find($id);
            if ($game) {
                $articleIds = $game->getAttribute('article_ids');
                if (!is_array($articleIds)) {
                    $articleIds =  array();
                }
                if (!in_array($articleId, $articleIds)) {
                    array_unshift($articleIds, $articleId);
                }
                $game->setAttribute('article_ids', $articleIds);
                $gameMapper->save($game);
            }
        }
    }

    private function _removeArticle(News_Model_Article $article, $gameIds)
    {
        if (!is_array($gameIds)) {
            return;
        }
        $articleId = $article->getId();
        $gameMapper = Game_Model_Game::getMapper();
        foreach ($gameIds as $id) {
            $game = $gameMapper->find($id);
            if ($game) {
                $articleIds = $game->getAttribute('article_ids');
                if (!is_array($articleIds)) {
                    $articleIds =  array();
                }
                if (($index = array_search($articleId, $articleIds)) !== false) {
                    array_splice($articleIds, $index, 1);
                }
                $game->setAttribute('article_ids', $articleIds);
                $gameMapper->save($game);
            }
        }
    }
}
