<?php

/**
 * New 'integrated' observer on payment. Handles job auto approval and a single email with 'receipt'.
 * Doesn't handle directory purchases anymore, as we don't have those.
 */
class Intent_Observer_PaymentReceipt extends Mcmr_Model_ObserverAbstract
{
    /**
     * Executed when a payent is updated
     * 
     * @param Payment_Model_Payment $model
     */
    public function update($model)
    {
        // If a successful payment
        if ($model->getStatus() === Mcmr_Payment_AdapterAbstract::STATUS_OK) {
            $jobMapper = Job_Model_Job::getMapper();

            // Disable the observers on the basket mapper. Prevent 'load' observers
            Payment_Model_Basket::getMapper()->setObserversEnabled(false);
            $basket = $model->getBasket();
            Payment_Model_Basket::getMapper()->setObserversEnabled(true);
            
            $items = $basket->getItems();
            foreach ($items as $item) {
                $itemModel = $item->getModel();
                if ($itemModel instanceof Job_Model_Job) {
                    // Get the job's expiry date from the product
                    $product = $item->getProduct();
                    if (null !== $product) {
                        $expireDate = strtotime($product->getAttribute('listtime'));
                        if ($expireDate > time()) {
                            $itemModel->setExpiredate($expireDate);
                        }
                    }
                    
                    $itemModel->setStatus('active');
                    $itemModel->setPublished(true);

                    $jobMapper->save($itemModel);
                }
            }

            // Send the confirmation email
            $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
            $view = clone $frontView;
            $view->setScriptPath(SITE_PATH . '/views/payment/scripts/');
            $view->addScriptPath(SITE_PATH.'/views/_common');
            $view->addScriptPath(SITE_PATH.'/_common');
            $view->basket = $basket;
            $view->payment = $model;

			$types = array();
            $items = $basket->getItems();
            foreach ($items as $item) {
                $obj = $item->getModel();
                if ($obj instanceof Job_Model_Job) {
                    $view->item = $item;
                    $view->job = $obj;

                    $types[] = 'job';
                }
                else if($obj instanceof Product_Model_Product){
                	if($type = $obj->getType()){
                		$types[] = $type->getUrl();
	                }
                }
            }
            $types = array_unique($types);
            
            $type = count($types) == 1 ? $types[0] : 'mixed';

            $request = Zend_Controller_Front::getInstance()->getRequest();
            $siteName = $request->getSiteName();
            if ('default' === $siteName) {
                $body = $view->render('email/payment-notification-receipt.phtml');
            } else {
                $body = $view->render($siteName.'/email/payment-notification-receipt.phtml');
            }

            $siteHumanName = Default_Model_Site::getMapper()->findOneByField(array('name'=>$siteName))->getDisplayname();
            // Get the email subject
            $defaultSubject = "Payment complete";
            $config = Zend_Registry::get('payment-config');
            if (isset($config->email->notification->receipt->subject)) {
            	if(is_object($config->email->notification->receipt->subject)){
            		$subject = $config->email->notification->receipt->subject->toArray();
           			$subject = isset($subject[$type]) ? $subject[$type] : $defaultSubject;
            	}
            	else{
	                $subject = $config->email->notification->receipt->subject;
    	        }
   	            $subject = str_replace('%site%', $siteHumanName, $subject);
            } else {
                $subject = $defaultSubject;
            }

            $emailAddress = $basket->getAddress()->getAttribute('email');

            $email = new Mcmr_Mail();
            $email->setBodyHtml($body);
            $email->setSubject($subject);
            $email->addTo($emailAddress);
            if (isset($config->email->notification->receipt->admin)) {
                $email->addBcc($config->email->notification->receipt->admin->toArray());
            }
            $email->send();
        }
    }
}
