<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * An observer for loading the chart entries from the chart service if the data isn't fresh
 *
 * @category Intent
 * @package Intent_Observer
 */
class Intent_Observer_MwChartLoader extends Mcmr_Model_ObserverAbstract
{
    private $_client = null;
    protected $_guid = null;
    protected $_curId = 1;
    
    public function init()
    {
        if (null === $this->_guid) {
            $config = $this->getConfig();
            if (isset($config->options->guid)) {
                $this->_guid = $config->options->guid;
            } else {
                throw new Zend_Exception("Cannot create connection. GUID not configured");
            }
        }
    }
    
    public function load($chart)
    {
        if (is_object($chart) && $chart instanceof Chart_Model_Chart) {
            $dirty = $chart->getAttribute('dirty');
            if ("no"!==$dirty) {
                if ('1' == $chart->getAttribute('IsMidWeek')) {
                    $this->_loadMidweek($chart);
                } else {
                    $this->_loadEntries($chart);
                }
            }
        }
    }
    
    protected function _loadEntries(Chart_Model_Chart $chart)
    {
        $data = $this->_requestData('RequestChartID', array('ChartID'=>$chart->getAttribute('mwchart_id')));
        if (isset($data->entries->entry)) {
            $chart->clearEntries();
            
            foreach ($data->entries->entry as $entry) {
                $chart = $this->_populateEntry($chart, $entry);
            }
            
            $chart->setAttribute('dirty', "no");
            
            $mapper = Chart_Model_Chart::getMapper();
            $this->setEnabled(false);
            $mapper->save($chart);
            $this->setEnabled(true);
        }
        
        return $chart;
    }
    
    protected function _loadMidweek(Chart_Model_Chart $chart)
    {
        $date = date('m/d/Y');
        $data = $this->_requestData(
            'RequestChartData', 
            array('ChartIdentifier'=>$chart->getAttribute('mwchart_id'), 'RequestedDate'=>$date, 'IsMidWeek'=>'true')
        );
        if (isset($data->entries->entry)) {
            $chart->clearEntries();
            
            foreach ($data->entries->entry as $entry) {
                $this->_populateEntryMidweek($chart, $entry);
            }
        }
        $chart->setAttribute('weeknumber', date('W'));
        
        return $chart;
    }

    protected function _buildEntry($entry) {
        $entryObj = new Chart_Model_Entry();
        $entryObj->setPosition((int)$entry->TW);
        $entryObj->setAttribute('title', (string)$entry->Title);
        $entryObj->setAttribute('lastWeek', (int)$entry->LW);
        $entryObj->setAttribute('weeks', (int)$entry->Weeks);
        $entryObj->setAttribute('artist', (string)$entry->Artist);
        $entryObj->setAttribute('producer', (string)$entry->Producer);
        $entryObj->setAttribute('publisher', (string)$entry->Publisher);
        $entryObj->setAttribute('songWriter', (string)$entry->SongWriter);
        $entryObj->setAttribute('recordLabel', (string)$entry->RecordLabel);
        $entryObj->setAttribute('catalogueNumber', (string)$entry->CatalogueNumber);
        $entryObj->setAttribute('distributor', (string)$entry->Distributor);
        $entryObj->setAttribute('totalPlays', (string)$entry->TotalPlays);
        $entryObj->setAttribute('playsIncrease', (string)$entry->PlaysIncrease);
        $entryObj->setAttribute('totalAudience', (string)$entry->TotalAudience);
        $entryObj->setAttribute('audienceIncrease', (string)$entry->AudienceIncrease);
        $entryObj->setAttribute('theOfficialUKSinglesChart_TW', (string)$entry->TheOfficialUKSinglesChart_TW);
        $entryObj->setAttribute('change', (string)$entry->Change);
        $entryObj->setAttribute('total', (string)$entry->Total);
        $entryObj->setAttribute('corporateGroup', (string)$entry->CorporateGroup);
        $entryObj->setAttribute('freeText1', (string)$entry->FreeText1);
        $entryObj->setAttribute('freeText2', (string)$entry->FreeText2);
        $entryObj->setAttribute('freeText3', (string)$entry->FreeText3);
        $entryObj->setAttribute('freeText4', (string)$entry->FreeText4);
        $entryObj->setAttribute('freeText5', (string)$entry->FreeText5);
        $entryObj->setAttribute('freeText6', (string)$entry->FreeText6);
        $entryObj->setAttribute('freeText7', (string)$entry->FreeText7);
        $entryObj->setAttribute('freeText8', (string)$entry->FreeText8);
        $entryObj->setAttribute('freeText9', (string)$entry->FreeText9);
        $entryObj->setAttribute('freeText10', (string)$entry->FreeText10);
        return $entryObj;        
    }
    
    protected function _populateEntry($chart, $entry)
    {
        $entryObj = $this->_buildEntry($entry);
        $chart->addEntry($entryObj);
        
        return $chart;
    }
    
    protected function _populateEntryMidweek($chart, $entry)
    {
        $entryObj = $this->_buildEntry($entry);
        $entryObj->setId($this->_curId);
        $this->_curId++;
        $chart->addEntry($entryObj);
        return $chart;
    }
    
    protected function _requestData($function, array $params=array())
    {
        $client = $this->_getClient();
        $params['guid'] = $this->_guid;
        $response = $client->__call($function, $params);
        
        $data = null;
        try {
            if ('No data was found for your request' !== $response->getBody()) {
                $data = new SimpleXMLElement($response->getBody());
            }
        } catch (Exception $e) {
            Mcmr_Debug::dump($params);
            Mcmr_Debug::dump($response);
        }
        
        return $data;
    }
    
    protected function _getClient()
    {
        if (null === $this->_client) {
            $config = $this->getConfig();
            $this->_client = new Intent_Cron_MwChart_Service($config->options->restUrl);
        }
        
        return $this->_client;
    }
}
