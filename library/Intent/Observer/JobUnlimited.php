<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * An observer for publishing jobs if the user's company has unlimited job postings.
 *
 * @category Intent
 * @package Intent_Observer
 */
class Intent_Observer_JobUnlimited extends Mcmr_Model_ObserverAbstract
{
    static public $disable = false;
    
    private $_view = null;
    private $_request = null;

    public function __construct()
    {
        $front = Zend_Controller_Front::getInstance();
        $this->_request = $front->getRequest();
        $this->_view = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');
    }

    /**
     * Publish a job if the user's company can post unlimited jobs
     *
     * @param Job_Model_Job $job
     */
    public function load($job)
    {
        if (self::$disable) {
            return true;
        }
        
        // Checks for a post action on model load (typically a read action) and publishes if necessary
        if ($job instanceof Job_Model_Job && $this->_request->isPost()) {
            $user = Zend_Registry::get('authenticated-user');
            if (null !== $user && '1' == $user->getAttribute('unlimitedJobs')) {
                if ('1' == $this->_request->getParam('jobPublish', '0') && !$job->isPublished()) {
                    // This company has unlimited job postings. Publish the job.
                    $job->setPublished(true);
                    $job->setStatus('active');
                    $job->setCompanyid($user->getAttribute('companyid'));

                    try {
                        $mapper = Job_Model_Job::getMapper();
                        self::$disable = true;
                        $mapper->save($job);
                        self::$disable = false;

                        $this->_view->getHelper('DisplayMessages')->addMessage('jobPublished', 'info');
                    } catch (Exception $e) {
                        $this->_view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            }
        }
    }
}
