<?php
/**
 * News article stores companies associated with it in company_ids attrbiute.
 * Company stores associated news articles in article_ids attribute.
 * We need to make sure that these two are synchronized. In the admin interface
 * companies are added/removed whenb editing article
 * hence this job is done on update of a news article.
 */
class Intent_Observer_ArticleCompanyJoin extends Mcmr_Model_ObserverAbstract
{

    static private $_preUpdate = null;

    public function insert($article)
    {
        if ($article instanceof News_Model_Article && $article->isPublished()) {
            $this->_addArticle($article, $article->getAttribute('company_ids'));
        }
    }
    
    
    public function preUpdate($article) 
    {
        self::$_preUpdate = clone $article;
    }

    public function update($article)
    {
        if ($article instanceof News_Model_Article) {
            if ($article->isPublished() ) {
                $this->_removeArticle($article, self::$_preUpdate->getAttribute('company_ids'));
                $this->_addArticle($article, $article->getAttribute('company_ids'));
            } else {
                $this->_removeArticle($article, self::$_preUpdate->getAttribute('company_ids'));
            }
        }
    }
    
    public function delete($article)
    {
        if (is_object(self::$_preUpdate)) {
            $this->_removeArticle($article, self::$_preUpdate->getAttribute('company_ids'));
        }
    }

    private function _addArticle(News_Model_Article $article, $companyIds)
    {
        if (!is_array($companyIds)) {
            return;
        }
        $articleId = $article->getId();
        $companyMapper = Company_Model_Company::getMapper();
        foreach ($companyIds as $id) {
            $company = $companyMapper->find($id);
            if ($company) {
                $articleIds = $company->getAttribute('article_ids');
                if (!is_array($articleIds)) {
                    $articleIds =  array();
                }
                if (!in_array($articleId, $articleIds)) {
                    array_unshift($articleIds, $articleId);
                }
                $company->setAttribute('article_ids', $articleIds);
                $companyMapper->save($company);
            }
        }
    }

    private function _removeArticle(News_Model_Article $article, $companyIds)
    {
        if (!is_array($companyIds)) {
            return;
        }
        $articleId = $article->getId();
        $companyMapper = Company_Model_Company::getMapper();
        foreach ($companyIds as $id) {
            $company = $companyMapper->find($id);
            if ($company) {
                $articleIds = $company->getAttribute('article_ids');
                if (!is_array($articleIds)) {
                    $articleIds =  array();
                }
                if (($index = array_search($articleId, $articleIds)) !== false) {
                    array_splice($articleIds, $index, 1);
                }
                $company->setAttribute('article_ids', $articleIds);
                $companyMapper->save($company);
            }
        }
    }
}
