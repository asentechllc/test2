<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Observer for fetching all the data necessary for sending Intent emails through Adestra
 *
 * @category Intent
 * @package Intent_Observer
 */

class Intent_Observer_MailqueueSend extends Mcmr_Service_Adestra_Observer_AdestraSend
{
    
    protected function _getLaunchData($email)
    {
        $site=Zend_Registry::get('app-config')->sitename;
        $subsite = 'default';
        $type = $email->getName();
        
        // Change the timezone so that dates are displayed correctly.
        $origTimezone = Mcmr_Date::getDefaultTimezone();
        if (isset(Zend_Registry::get('mailqueue-config')->adestra->{$type}->timezone)) {
            $timezone = Mcmr_Date::setDefaultTimezone(
                Zend_Registry::get('mailqueue-config')->adestra->{$type}->timezone
            ); // Change timezone for the email
        }
        
        $parts = explode('-', $type);
        if (2 === count($parts)) {
            list($subsite, $type) = $parts;
        }

        $emailData = Intent_EmailData::getInstance($email, $type, $subsite, $site);
        $data = $emailData->adestraData();
        
        if ('emailtest' === substr($email->getOptin(), 0, 9)) {
            $data['subject'] = '(TEST) ' . $data['subject'];
            $this->_sentStatus = Mailqueue_Model_Mail::STATUS_UNSENT;
        }
        $data['subject'] = html_entity_decode($data['subject'], ENT_QUOTES, 'UTF-8');
        
        Mcmr_Date::setDefaultTimezone($origTimezone); // Restore timezone
        
        return $data;
    }
}
