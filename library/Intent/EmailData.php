<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_EmailData
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Factory class for EmailData. Fetch the appropriate email data object for the email type
 *
 * @category Intent
 * @package Intent_EmailData
 */

class Intent_EmailData
{
    
    static public function getInstance(Mailqueue_Model_Mail $email, $type, $subsite='default', $site=null)
    {
        $instance = null;
        //try site specific data building class first
        //Zend_Loader generates a warning in the logs when class_exists is called and returns false
        // http://zend-framework-community.634137.n4.nabble.com/using-class-exists-without-warnings-in-1-8-td658874.html
        if (null!==$site) {
            $classname = 'Intent_EmailData_'.$site.'_'.ucfirst($type);
            if (class_exists($classname)) {
                $instance = new $classname($email, $subsite);
            }
        }
       
        if (null===$instance) {
            $classname = 'Intent_EmailData_'.ucfirst($type);
            if (class_exists($classname)) {
                $instance = new $classname($email, $subsite);
            }
        }

        if (null===$instance) {
            throw new Zend_Exception("Unknown email type $type, site $site");
        }
        return $instance;
    }
}
