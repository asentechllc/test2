<?php

class Intent_Form_Job extends Job_Form_Job
{
    public function __construct($options = null)
    {
        if (!is_array($options) && $options instanceof Zend_Config) {
            $options = $options->toArray();
        } elseif (!is_array($options)) {
            $options = array();
        }

        $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'job_form_job.ini';

        parent::__construct($options);
    }

    public function isValid($options)
    {
        // Change the validators depending on the application method
        $element = $this->getElement('attr_externalUrl');
        if (null !== $element) {
            switch ($options['jobformjob']['attr_howtoapplymethod']) {
                case 'email':
                    $element->clearValidators();
                    $validator = new Zend_Validate_EmailAddress();
                    $validator->setMessage("'%value%' is not a valid email address", 'emailAddressInvalid');
                    $validator->setMessage("'%value%' is not a valid email address", 'emailAddressInvalidFormat');
                    $validator->setMessage("'%value%' is not a valid email address", 'emailAddressInvalidHostname');
                    $element->addValidator($validator, true);
                    break;

                case 'web':
                    $element->clearValidators();
                    $validator = new Zend_Validate_Callback(array('Zend_Uri', 'check'));
                    $validator->setMessage("'%value%' is not a valid URL", 'callbackValue');
                    $element->addValidator($validator, true);
                    break;
            }
        }

        return parent::isValid($options);
    }
}
