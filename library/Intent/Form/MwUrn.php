<?php

class Intent_Form_MwUrn extends Mcmr_Form
{
    public function init()
    {
        $this->setName('intentFormMwUrn')->setElementsBelongTo('news-form-article');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }    
}
