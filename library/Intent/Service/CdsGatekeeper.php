<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Intent
 * @package Intent_Observer
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Service class for communicating with CDS gatekeeper
 *
 * @category Intent
 * @package Intent_Service
 */
class Intent_Service_CdsGatekeeper
{
    const SERVICE_URN = 'https://www.subscription.co.uk/gatekeeper/mwquery.asp';
    const FIELD_CDS_URN = 'cdsUrn';
    const FIELD_CDS_STATUS = 'cdsStatus';
    const FIELD_CDS_EXPIREDATE = 'cdsExpire';
    const FIELD_CDS_STARTDATE = 'cdsStartDate';
    const FIELD_CDS_JOINDATE = 'cdsJoinDate';
    const FIELD_CDS_SUBSCRIPTIONTYPE = 'cdsSubscriptionType';
    const FIELD_CDS_COPIES = 'cdsCopies';
    const FIELD_CDS_ISSUES = 'cdsIssues';
    
    const FIELD_CDS_TITLE = 'title';
    const FIELD_CDS_JOBTITLE = 'jobtitle';
    const FIELD_CDS_COMPANY = 'company';
    const FIELD_CDS_DEPARTMENT = 'department';
    const FIELD_CDS_ADDRESS1 = 'address1';
    const FIELD_CDS_ADDRESS2 = 'address2';
    const FIELD_CDS_ADDRESS3 = 'address3';
    const FIELD_CDS_TOWN = 'town';
    const FIELD_CDS_COUNTY = 'county';
    const FIELD_CDS_COUNTRY = 'country';
    const FIELD_CDS_POSTCODE = 'postcode';
    const FIELD_CDS_TELEPHONE = 'telephone';
    const FIELD_CDS_FAX = 'fax';
    
    private $_originator = null;
    private $_type = null;
    
    public function __construct($originator, $type='FULL')
    {
        $this->_originator = (int)$originator;
        $this->_type = $type;
    }
    
    /**
     *
     * @return int
     */
    public function getOriginator()
    {
        return $this->_originator;
    }

    /**
     *
     * @param int $originator
     * @return Intent_Service_CdsGatekeeper 
     */
    public function setOriginator($originator)
    {
        $this->_originator = $originator;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     *
     * @param string $type
     * @return Intent_Service_CdsGatekeeper 
     */
    public function setType($type)
    {
        $this->_type = $type;
        
        return $this;
    }

    /**
     * Fetch all of the subscription details for the supplied URN and surname
     *
     * @param string $urn
     * @param string $surname
     * @return SimpleXMLElement 
     */
    public function subscriptionDetails(User_Model_User $user)
    {
        $urn = $user->getAttribute(self::FIELD_CDS_URN);
        $surname = $user->getSurname();

        $subscriptionData = $this->rawSubscriptionDetails($urn, $surname);
        
        if (isset($subscriptionData->SUBSCRIPTION)) {
            $status = (string)$subscriptionData->SUBSCRIPTION->Status;
            $subscriptionType = (string)$subscriptionData->SUBSCRIPTION->Subtype;
            $user->setAttribute(self::FIELD_CDS_SUBSCRIPTIONTYPE, $subscriptionType);
            if ('none'!=strtolower($subscriptionType)) {
                $copies = (string)$subscriptionData->SUBSCRIPTION->Copies;
                $issues = (string)$subscriptionData->SUBSCRIPTION->Issues;
                
                $startDate = $this->_dateToUnixtime((string)$subscriptionData->SUBSCRIPTION->CurrentStart);
                $joinDate = $this->_dateToUnixtime((string)$subscriptionData->SUBSCRIPTION->DateJoined);
                $expireDate = $this->_dateToUnixtime((string)$subscriptionData->SUBSCRIPTION->ExpiryDate);
                
                $user->setAttribute(self::FIELD_CDS_STATUS, $status);
                $user->setAttribute(self::FIELD_CDS_EXPIREDATE, $expireDate);
                $user->setAttribute(self::FIELD_CDS_COPIES, $copies);
                $user->setAttribute(self::FIELD_CDS_ISSUES, $issues);
                $user->setAttribute(self::FIELD_CDS_STARTDATE, $startDate);
                $user->setAttribute(self::FIELD_CDS_JOINDATE, $joinDate);
                if (isset($subscriptionData->NAME)) {
                    $name = $subscriptionData->NAME;
                    $user->setAttribute(self::FIELD_CDS_TITLE, (string)$name->Title);
                    $user->setFirstname((string)$name->Firstname);
                    $user->setSurname((string)$name->Surname);
                    $user->setAttribute(self::FIELD_CDS_JOBTITLE, (string)$name->Jobtitle);
                    $user->setAttribute(self::FIELD_CDS_COMPANY, (string)$name->Company);
                    $user->setAttribute(self::FIELD_CDS_DEPARTMENT, (string)$name->Department);
                }
                
                if (isset($subscriptionData->DELIVERYADDRESS)) {
                    $address = $subscriptionData->DELIVERYADDRESS;
                    $user->setAttribute(self::FIELD_CDS_ADDRESS1, (string)$address->Ad1);
                    $user->setAttribute(self::FIELD_CDS_ADDRESS2, (string)$address->Ad2);
                    $user->setAttribute(self::FIELD_CDS_ADDRESS3, (string)$address->Ad3);
                    $user->setAttribute(self::FIELD_CDS_TOWN, (string)$address->Town);
                    $user->setAttribute(self::FIELD_CDS_COUNTY, (string)$address->County);
                    $user->setAttribute(self::FIELD_CDS_COUNTRY, (string)$address->Country);
                    $user->setAttribute(self::FIELD_CDS_POSTCODE, (string)$address->Postcode);
                }
                
                if (isset($subscriptionData->CONTACT)) {
                    $contact = $subscriptionData->CONTACT;
                    $user->setAttribute(self::FIELD_CDS_TELEPHONE, (string)$contact->Telephone);
                    $user->setAttribute(self::FIELD_CDS_FAX, (string)$contact->Fax);
                    $user->setEmail((string)$contact->Email);
                }

            }
        }
        
        return $user;
    }
    
    public function rawSubscriptionDetails($urn, $surname)
    {
        $responseBody = "<?xml version='1.0' standalone='yes'?><SUBSCRIBER></SUBSCRIBER>";
        Mcmr_Debug::dump("Checking $urn, $surname", 6,'file',true);
        if ($urn && $surname) {
            $cacheId = 'CdsGatekeeper_rawSubscriptionDetails_'.md5($urn.", ".$surname);
            $cache = Zend_Registry::get('cachemanager')->getCache('database');
            if (!$cache->test($cacheId)) {
                $client = new Zend_Http_Client(self::SERVICE_URN);
                $client->setParameterPost('type', $this->_type)
                    ->setParameterPost('originator', $this->_originator)
                    ->setParameterPost('urn', $urn)
                    ->setParameterPost('surname', $surname);
                try {
                    $response = $client->request('POST');
                    if($response->isSuccessful()) {
                        $responseBody = $response->getBody();
                    }
                } catch (Exception $e ) { 
                    //if we cannot get, we simply return empty
                }
                $tags =  array('cdsgatekeeper_'.md5($urn.", ".$surname), 'cdsgatekeeper');
                $cache->save($responseBody, $cacheId, $tags);
            } else {
                $responseBody = $cache->load($cacheId);
            }
        } else {
            Mcmr_Debug::dump("Details missing", 6,'file',true);
        }
        $xmlResponse = new SimpleXMLElement($responseBody);
        return $xmlResponse;
    }
    
    /**
     * Check if the urn and surname has a valid subscription
     *
     * @param string $urn
     * @param string $surname
     * @return bool 
     */
    public function subscriptionValid(User_Model_User $user)
    {
        $user = $this->subscriptionDetails($user);
        
        $status = $user->getAttribute(self::FIELD_CDS_STATUS);
        $expireDate = $user->getAttribute(self::FIELD_CDS_EXPIREDATE);
        
        return ('CURRENT' === $status)
            || ('LAPSED' === $status && time() < $expireDate);
    }
    
    /**
     * Convert the date returned by CDS into a unix timestamp
     *
     * @param type $dateString
     * @return int
     */
    private function _dateToUnixtime($dateString)
    {
        $unixtime = 0;
        $parts = explode("/",$dateString);
        if (3 === count($parts)) {
            $unixtime = mktime(0, 0, 0, $parts[1], $parts[0], $parts[2]);
        }
        
        return $unixtime;
    }
}
;