<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Zend
 * @package Zend_Cache
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: McmrMemcached.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * Nasty hack to allow the Cache Manager to use the Mcmr Memcache class
 *
 * @category Zend
 * @package Zend_Cache
 * @subpackage Backend
 */
class Zend_Cache_Backend_McmrMemcached extends Mcmr_Cache_Backend_Memcached
{}