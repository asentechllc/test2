<?php
function __getTime()
{
    $time = microtime();
    $time = explode(" ", $time);
    $time = $time[1] + $time[0];
    return $time;
}

function __moduleConfig($module)
{
    $file = SITE_PATH.DIRECTORY_SEPARATOR.'configs'.DIRECTORY_SEPARATOR.'cron'.DIRECTORY_SEPARATOR.$module.'.ini';
    if (is_file($file)) {
        return new Mcmr_Config_Ini($file, APPLICATION_ENV);
    } else {
        return null;
    }
}

$__start = __getTime();

require_once 'Zend/Loader/Autoloader.php';
$loader = Zend_Loader_Autoloader::getInstance();

// Define some CLI options
$__getopt = new Zend_Console_Getopt(array(
    'gossip|g=s'  =>'Path to Gossip Application',
    'site|s=s'    =>'Path to site',
    'publicsite|p=s'    =>'Path to site',
    'env|e-s'     => 'Execution environment (defaults to production)',
    'module|m=s'  => 'Cron module to execute',
    'args|a=s' => 'Cron module arguments',
    'sitename|n=s'  => 'Site Name',
    'quiet|q'     => 'Execute silently',
    'lock|l'      => 'Lock and check the process',
    'help|?|h'      => 'Help -- usage message',
));

try {
    $__getopt->parse();
} catch (Zend_Console_Getopt_Exception $e) {
    // Bad options passed: report usage
    echo $e->getUsageMessage();
    return false;
}

// If help requested, report usage message
if ($__getopt->getOption('h')) {
    echo $__getopt->getUsageMessage();
    return true;
}

// Initialize values based on presence or absence of CLI options
$__gossip = $__getopt->getOption('g');
$__site = $__getopt->getOption('s');
$__publicSite = $__getopt->getOption('p');
$__env      = (null !== $__getopt->getOption('e'))?$__getopt->getOption('e'):'production';
$__quiet = (null !== $__getopt->getOption('q'))?$__getopt->getOption('q'):false;
$__module = $__getopt->getOption('m');
$__sitename = (null !== $__getopt->getOption('n'))?$__getopt->getOption('n'):'default';
$__lock = null !== $__getopt->getOption('l');
$__args = $__getopt->getOption('a');

if (null === $__module) {
    echo "Module name not provided.\n\n";
    echo $__getopt->getUsageMessage();
    return false;
}

// Set up the environment variables and include paths.
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', (null !== $__gossip) ? $__gossip.'/application' : realpath(dirname(__FILE__) . '/../application'));

defined('SITE_PATH')
    || define('SITE_PATH', (null !== $__site) ? $__site : realpath(dirname(__FILE__)));

defined('PUBLIC_SITE_PATH')
    || define('PUBLIC_SITE_PATH', (null !== $__publicSite) ? $__publicSite : SITE_PATH);

defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', $__env);

define('APPLICATION_STATS', false);

if (!is_dir(APPLICATION_PATH)) {
    echo 'Gossip install not found in path: '.APPLICATION_PATH.PHP_EOL;
    return false;
}

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    realpath(SITE_PATH . '/library'),
    get_include_path(),
)));

// Initialize Zend_Application and bootstrap
$__application = new Zend_Application(
    APPLICATION_ENV,
    SITE_PATH . '/configs/application.ini'
);

$__bootstrap = $__application->getBootstrap();

$__bootstrap->bootstrap();
// Manually set the bootstrap object in the front controller.
$__controller = Zend_Controller_Front::getInstance();
$__controller->setParam('bootstrap', $__bootstrap);
// Manually add the default route 
$__router = $__controller->getRouter();
$__default_route = new Zend_Controller_Router_Route_Module( array() );
$__router->addRoute('default', $__default_route );


if ($__quiet) {
    ob_start();
}

// Fetch confuguration and execute the module
$__config = new Mcmr_Config_Ini(SITE_PATH.DIRECTORY_SEPARATOR.'configs'.DIRECTORY_SEPARATOR.'cron.ini', APPLICATION_ENV);
$__cron = new Mcmr_Cron($__config, $__module, __moduleConfig($__module), $__sitename,$__args);
$__cron->execute($__lock);

$__end = __getTime();
$__executeTime = $__end - $__start;

echo "Total execution time: {$__executeTime} seconds \n";

if ($__quiet) {
    ob_end_clean();
}

return true;
