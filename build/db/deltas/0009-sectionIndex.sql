ALTER TABLE `news_articles` ADD INDEX (`section_id`);

--//@UNDO

ALTER TABLE `news_articles` DROP INDEX `section_id`;
