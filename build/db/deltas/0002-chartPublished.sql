ALTER TABLE `chart_charts` ADD `chart_published` tinyint(1) NOT NULL DEFAULT 0;

--//@UNDO

ALTER TABLE `chart_charts` DROP `chart_published`;
