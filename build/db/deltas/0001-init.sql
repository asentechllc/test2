CREATE TABLE IF NOT EXISTS `changelog_followers` (
  `follow_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `follow_modeltype` varchar(32) NOT NULL,
  `follow_modelid` int(11) NOT NULL,
  `follow_date` datetime NOT NULL,
  `follow_updates` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`follow_id`),
  KEY `user_id` (`user_id`,`follow_modeltype`,`follow_modelid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `changelog_records` (
  `record_id` int(11) NOT NULL AUTO_INCREMENT,
  `record_date` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `record_type` varchar(32) CHARACTER SET latin1 NOT NULL,
  `item_id` int(11) NOT NULL,
  `record_action` varchar(16) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`record_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_parentId` int(11) DEFAULT '0',
  `category_url` varchar(128) NOT NULL,
  `category_title` varchar(256) NOT NULL,
  `category_description` text,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_categories_attr` (
  `category_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`category_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_categories_ordr` (
  `category_id` int(11) NOT NULL,
  `ordr_name` varchar(64) NOT NULL,
  `ordr_value` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`ordr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_charts` (
  `chart_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role` varchar(32) NOT NULL DEFAULT 'guest',
  `type_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `chart_date` datetime DEFAULT NULL,
  `platform_id` int(11) DEFAULT NULL,
  `chart_title` varchar(512) NOT NULL,
  PRIMARY KEY (`chart_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_charts_attr` (
  `chart_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`chart_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_entries` (
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `chart_id` int(11) NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  `entry_position` int(11) NOT NULL,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_entries_attr` (
  `entry_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`entry_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `type_url` varchar(32) NOT NULL,
  `type_title` varchar(32) NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_url` (`type_url`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `chart_types_attr` (
  `type_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`type_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `company_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_parentId` int(11) DEFAULT '0',
  `category_title` varchar(64) CHARACTER SET latin1 NOT NULL,
  `category_url` varchar(32) CHARACTER SET latin1 NOT NULL,
  `category_description` text CHARACTER SET latin1,
  `category_order` int(11) DEFAULT '0',
  PRIMARY KEY (`category_id`),
  KEY `category_parentId` (`category_parentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `company_companies` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `company_state` varchar(16) NOT NULL,
  `company_listed` tinyint(1) DEFAULT '0',
  `company_featured` tinyint(1) NOT NULL,
  `company_featuredstart` datetime DEFAULT NULL,
  `company_featuredend` datetime DEFAULT NULL,
  `company_recruiter` tinyint(1) DEFAULT NULL,
  `company_recruiterstart` date DEFAULT NULL,
  `company_recruiterend` date DEFAULT NULL,
  `company_recruiterorder` int(11) NOT NULL,
  `company_order` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `company_title` varchar(128) CHARACTER SET latin1 NOT NULL,
  `company_description` text CHARACTER SET latin1 NOT NULL,
  `company_image` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `company_companycategories` (
  `company_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`company_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `company_company_attr` (
  `company_id` int(11) NOT NULL,
  `attr_name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `attr_value` text CHARACTER SET latin1,
  `attr_type` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`company_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `company_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_url` varchar(128) NOT NULL,
  `type_title` varchar(128) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `default_performance` (
  `performance_id` int(11) NOT NULL AUTO_INCREMENT,
  `performance_page` varchar(32) CHARACTER SET latin1 NOT NULL,
  `performance_date` date NOT NULL,
  `performance_pagetype` varchar(8) CHARACTER SET latin1 NOT NULL,
  `performance_peakmemory` int(11) NOT NULL,
  `performance_memory` int(11) NOT NULL,
  `performance_objects` int(11) NOT NULL,
  `performance_cachehit` int(11) NOT NULL,
  `performance_cachemiss` int(11) NOT NULL,
  `performance_generatetime` int(11) NOT NULL,
  `performance_requests` int(11) NOT NULL,
  PRIMARY KEY (`performance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `default_registry` (
  `registry_id` int(11) NOT NULL AUTO_INCREMENT,
  `registry_module` varchar(32) NOT NULL,
  `registry_name` varchar(64) NOT NULL,
  `registry_value` text,
  PRIMARY KEY (`registry_id`),
  KEY `registry_module` (`registry_module`,`registry_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `default_stats` (
  `stat_id` int(11) NOT NULL AUTO_INCREMENT,
  `stat_date` date NOT NULL,
  `stat_site` varchar(16) NOT NULL DEFAULT 'default',
  `stat_modeltype` varchar(32) NOT NULL,
  `stat_modelid` int(11) NOT NULL,
  `stat_action` varchar(16) CHARACTER SET utf8 NOT NULL,
  `stat_count` int(11) NOT NULL,
  PRIMARY KEY (`stat_id`),
  KEY `stat_site` (`stat_site`,`stat_date`,`stat_modeltype`,`stat_modelid`,`stat_action`),
  KEY `stat_action` (`stat_action`),
  KEY `stat_modelid` (`stat_modelid`),
  KEY `stat_modeltype` (`stat_modeltype`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Contain access statistics for all application models';

CREATE TABLE IF NOT EXISTS `event_bookings` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `booking_date` datetime NOT NULL,
  `booking_active` tinyint(1) DEFAULT NULL,
  `booking_email` varchar(128) DEFAULT NULL,
  `booking_token` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`booking_id`),
  UNIQUE KEY `event_id` (`event_id`,`user_id`),
  UNIQUE KEY `event_id_2` (`event_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event_booking_attr` (
  `booking_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`booking_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event_events` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `event_url` varchar(128) NOT NULL,
  `event_state` varchar(32) NOT NULL DEFAULT 'active',
  `location_id` smallint(6) NOT NULL,
  `event_title` varchar(512) NOT NULL,
  `event_description` text NOT NULL,
  `event_venue` varchar(255) DEFAULT NULL,
  `event_address1` varchar(125) DEFAULT NULL,
  `event_address2` varchar(125) DEFAULT NULL,
  `event_town` varchar(20) DEFAULT NULL,
  `event_county` varchar(20) DEFAULT NULL,
  `event_postcode` varchar(10) DEFAULT NULL,
  `event_startdate` datetime NOT NULL,
  `event_enddate` datetime NOT NULL,
  `event_spaces` smallint(6) DEFAULT NULL,
  `event_published` tinyint(1) NOT NULL,
  `event_publishdate` datetime NOT NULL,
  `event_image` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`event_id`),
  UNIQUE KEY `event_url` (`event_url`),
  KEY `type_id` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event_events_attr` (
  `event_id` int(11) NOT NULL,
  `attr_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `attr_value` text CHARACTER SET latin1,
  `attr_type` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`event_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event_events_ordr` (
  `event_id` int(11) NOT NULL,
  `ordr_name` varchar(64) NOT NULL,
  `ordr_value` int(11) DEFAULT NULL,
  PRIMARY KEY (`event_id`,`ordr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event_locations` (
  `location_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `location_title` varchar(255) NOT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event_programs` (
  `program_id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `program_title` varchar(255) NOT NULL,
  `program_description` text NOT NULL,
  `program_startdate` datetime NOT NULL,
  `program_enddate` datetime NOT NULL,
  `speaker_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`program_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `event_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_url` varchar(32) NOT NULL,
  `type_title` varchar(32) NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_url` (`type_url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `feed_entries` (
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_name` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  `entry_guid` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  `entry_title` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  `entry_link` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  `entry_pubDate` datetime DEFAULT NULL,
  `entry_description` text CHARACTER SET latin1,
  PRIMARY KEY (`entry_id`),
  KEY `feed_name` (`feed_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Save feed items to this table';

CREATE TABLE IF NOT EXISTS `job_applications` (
  `application_id` int(11) NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `application_date` datetime NOT NULL,
  `application_description` text,
  `application_file` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`application_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `job_applications_attr` (
  `application_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`application_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `job_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_url` varchar(128) NOT NULL,
  `category_title` varchar(128) NOT NULL,
  `category_description` text,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `job_jobs` (
  `job_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `job_published` tinyint(1) NOT NULL,
  `job_featured` tinyint(1) NOT NULL,
  `job_featuredstart` datetime DEFAULT NULL,
  `job_featuredend` datetime DEFAULT NULL,
  `job_order` int(11) NOT NULL,
  `job_url` varchar(512) NOT NULL,
  `job_title` varchar(512) NOT NULL,
  `job_description` text,
  `job_location` varchar(128) DEFAULT NULL,
  `job_salary` varchar(64) NOT NULL,
  `job_date` datetime NOT NULL,
  `job_expiredate` datetime NOT NULL,
  `job_type` varchar(32) DEFAULT NULL,
  `job_email` varchar(128) DEFAULT NULL,
  `job_status` varchar(32) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`job_id`),
  KEY `job_url` (`job_url`(128))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Store all jobs for jobs module';

CREATE TABLE IF NOT EXISTS `job_jobs_attr` (
  `job_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`job_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `job_jobs_ordr` (
  `job_id` int(11) NOT NULL,
  `ordr_name` varchar(64) NOT NULL,
  `ordr_value` int(11) NOT NULL,
  PRIMARY KEY (`job_id`,`ordr_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for defining multiple order fields for jobs';

CREATE TABLE IF NOT EXISTS `job_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_title` varchar(128) NOT NULL,
  `location_url` varchar(128) NOT NULL,
  `location_description` text,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `location_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `location_url` varchar(128) CHARACTER SET latin1 NOT NULL,
  `region_id` int(11) NOT NULL,
  `location_title` varchar(64) CHARACTER SET latin1 NOT NULL,
  `location_link` varchar(128) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `location_regions` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `region_url` varchar(128) CHARACTER SET latin1 NOT NULL,
  `region_title` varchar(64) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mailqueue_mail` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_transport` varchar(32) NOT NULL,
  `mail_name` varchar(32) NOT NULL,
  `mail_subject` varchar(128) NOT NULL,
  `mail_plain` longtext,
  `mail_html` longtext NOT NULL,
  `mail_status` varchar(16) NOT NULL,
  `mail_sendtime` datetime NOT NULL,
  `mail_optin` varchar(64) NOT NULL,
  `mail_messageid` varchar(128) DEFAULT NULL,
  `mail_listid` varchar(128) DEFAULT NULL,
  `mail_deliveryid` varchar(128) DEFAULT NULL,
  `mail_processstart` datetime DEFAULT NULL,
  PRIMARY KEY (`mail_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mailqueue_mail_attr` (
  `mail_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`mail_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `mailqueue_recipient` (
  `recipient_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_id` int(11) NOT NULL,
  `recipient_name` varchar(128) NOT NULL,
  `recipient_email` varchar(128) NOT NULL,
  PRIMARY KEY (`recipient_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `media_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_filename` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `file_type` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `file_description` text CHARACTER SET latin1,
  `file_date` datetime NOT NULL,
  `file_status` varchar(15) CHARACTER SET latin1 NOT NULL,
  `user_id` int(11) NOT NULL,
  `set_id` int(11) NOT NULL,
  `file_url` varchar(64) CHARACTER SET latin1 NOT NULL,
  `file_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `media_file_attr` (
  `file_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`file_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `media_file_tags` (
  `file_id` int(11) NOT NULL,
  `tag_name` varchar(32) NOT NULL,
  PRIMARY KEY (`file_id`,`tag_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `media_set` (
  `set_id` int(11) NOT NULL AUTO_INCREMENT,
  `set_title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `set_url` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `set_description` text CHARACTER SET latin1,
  `set_date` datetime DEFAULT NULL,
  `set_name` varchar(255) DEFAULT NULL,
  `set_modeltype` varchar(255) CHARACTER SET latin1 NOT NULL,
  `set_modelid` int(11) NOT NULL,
  `set_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`set_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `media_set_attr` (
  `set_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`set_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `media_set_types` (
  `set_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `set_type_url` varchar(32) CHARACTER SET latin1 NOT NULL,
  `set_type_title` varchar(32) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`set_type_id`),
  UNIQUE KEY `set_type_url` (`set_type_url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_articles` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role` varchar(32) NOT NULL DEFAULT 'guest',
  `article_createdate` datetime NOT NULL,
  `article_publishdate` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_id_author` int(11) DEFAULT NULL,
  `article_authorname` varchar(32) DEFAULT NULL,
  `article_authoremail` varchar(128) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `article_order` int(11) NOT NULL,
  `article_stream` tinyint(1) NOT NULL,
  `article_published` tinyint(1) NOT NULL,
  `article_url` varchar(512) NOT NULL,
  `article_title` varchar(512) NOT NULL,
  `article_strap` varchar(512) DEFAULT NULL,
  `article_intro` text,
  `article_content` text,
  `article_image` varchar(255) DEFAULT NULL,
  `article_featured` tinyint(1) NOT NULL,
  `article_sites` set('default') NOT NULL DEFAULT 'default',
  `article_numread` int(11) NOT NULL,
  `article_numcomment` int(11) NOT NULL,
  `article_numemail` int(11) NOT NULL,
  PRIMARY KEY (`article_id`),
  KEY `article_id` (`article_id`),
  KEY `article_publishdate` (`article_publishdate`),
  KEY `article_url` (`article_url`(255)),
  KEY `type_id` (`type_id`),
  KEY `article_stream` (`article_stream`,`article_published`),
  KEY `article_numread` (`article_numread`),
  KEY `article_numcomment` (`article_numcomment`),
  KEY `article_numemail` (`article_numemail`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_articles_attr` (
  `article_id` int(11) NOT NULL,
  `attr_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `attr_value` text CHARACTER SET latin1,
  `attr_type` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`article_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_articles_ordr` (
  `article_id` int(11) NOT NULL,
  `ordr_name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `ordr_value` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`ordr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table for defining multiple order fields for articles';

CREATE TABLE IF NOT EXISTS `news_article_categories` (
  `article_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`article_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_article_sets` (
  `article_id` int(11) NOT NULL,
  `set_id` int(11) NOT NULL,
  `articleset_order` int(11) DEFAULT NULL,
  PRIMARY KEY (`article_id`,`set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_featured` tinyint(4) DEFAULT NULL,
  `category_order` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `category_url` varchar(64) NOT NULL,
  `category_title` varchar(64) NOT NULL,
  `category_sites` set('default') NOT NULL DEFAULT 'default',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `category_url` (`category_url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_categories_attr` (
  `category_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`category_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_categories_ordr` (
  `category_id` int(11) NOT NULL,
  `ordr_name` varchar(64) NOT NULL,
  `ordr_value` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`ordr_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for defining multiple order fields for news categories';

CREATE TABLE IF NOT EXISTS `news_sets` (
  `set_id` int(11) NOT NULL AUTO_INCREMENT,
  `set_title` varchar(32) NOT NULL,
  `set_url` varchar(32) NOT NULL,
  PRIMARY KEY (`set_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_tags` (
  `article_id` int(11) NOT NULL,
  `tag_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`article_id`,`tag_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `news_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_url` varchar(32) CHARACTER SET latin1 NOT NULL,
  `type_title` varchar(32) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_url` (`type_url`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `page_pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_content` mediumtext NOT NULL,
  `page_url` varchar(512) NOT NULL,
  `page_title` varchar(512) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `page_createdate` datetime NOT NULL,
  `page_updatedate` datetime NOT NULL,
  `page_order` int(10) NOT NULL,
  `page_parentid` int(11) NOT NULL,
  `page_locked` tinyint(1) NOT NULL,
  `page_published` tinyint(1) NOT NULL,
  `page_childtemplate` varchar(32) DEFAULT NULL,
  `page_childrenallowed` tinyint(1) NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `page_pages_attr` (
  `page_id` int(11) NOT NULL,
  `attr_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `attr_value` text CHARACTER SET latin1,
  `attr_type` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`page_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `payment_addresses` (
  `address_id` int(11) NOT NULL AUTO_INCREMENT,
  `basket_id` int(11) NOT NULL,
  `address_type` varchar(16) NOT NULL,
  `address_firstname` varchar(64) NOT NULL,
  `address_surname` varchar(64) NOT NULL,
  `address_address1` varchar(64) NOT NULL,
  `address_address2` varchar(64) NOT NULL,
  `address_postcode` varchar(16) NOT NULL,
  `address_state` varchar(16) DEFAULT NULL,
  `address_city` varchar(16) NOT NULL,
  `address_country` varchar(16) DEFAULT NULL,
  `address_phone` varchar(16) NOT NULL,
  `address_company` varchar(128) DEFAULT NULL,
  `address_taxExempt` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`address_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `payment_addresses_attr` (
  `address_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`address_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `payment_baskets` (
  `basket_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `basket_status` varchar(16) NOT NULL,
  `basket_createdate` datetime NOT NULL,
  `basket_paymentdate` datetime NOT NULL,
  `basket_profile` varchar(16) NOT NULL,
  PRIMARY KEY (`basket_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `payment_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `basket_id` int(11) NOT NULL,
  `item_modelid` int(11) NOT NULL,
  `item_modeltype` varchar(32) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_type` varchar(64) DEFAULT NULL,
  `item_amount` int(11) NOT NULL,
  `item_taxrate` float NOT NULL,
  `item_description` text,
  `item_count` int(11) NOT NULL DEFAULT '1',
  `item_redeem` int(11) NOT NULL DEFAULT '0',
  `item_token` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `payment_payments` (
  `payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `basket_id` int(11) NOT NULL,
  `payment_createdate` datetime NOT NULL,
  `payment_currency` varchar(8) NOT NULL,
  `payment_gateway` varchar(16) NOT NULL,
  `payment_ipaddress` varchar(16) DEFAULT NULL,
  `payment_paymentdate` datetime NOT NULL,
  `payment_profile` varchar(32) NOT NULL,
  `payment_responseobject` text NOT NULL,
  `payment_status` varchar(16) NOT NULL,
  `payment_message` text,
  `payment_txid` varchar(128) DEFAULT NULL,
  `payment_taxExempt` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`payment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `payment_payments_attr` (
  `payment_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`payment_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `product_amounts` (
  `product_id` int(11) NOT NULL,
  `amount_name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `amount_value` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`amount_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `product_categories` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` varchar(32) CHARACTER SET latin1 NOT NULL,
  `category_title` varchar(64) CHARACTER SET latin1 NOT NULL,
  `category_url` varchar(32) CHARACTER SET latin1 NOT NULL,
  `category_description` text CHARACTER SET latin1,
  `category_image` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `product_downloads` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `download_url` varchar(64) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `download_email` varchar(128) DEFAULT NULL,
  `download_date` datetime NOT NULL,
  PRIMARY KEY (`download_id`),
  KEY `product_id` (`product_id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `product_productcategories` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `product_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role` varchar(32) NOT NULL DEFAULT 'guest',
  `type_id` int(11) NOT NULL,
  `product_title` varchar(512) NOT NULL,
  `product_url` varchar(512) NOT NULL,
  `product_description` text CHARACTER SET latin1,
  `product_image` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `product_imagealt` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `product_featured` tinyint(1) NOT NULL,
  `product_taxrate` float DEFAULT NULL,
  `product_modeltype` varchar(64) DEFAULT NULL,
  `product_modelid` int(11) DEFAULT NULL,
  `product_download` varchar(128) DEFAULT NULL,
  `product_redeem` int(11) NOT NULL DEFAULT '0',
  `product_createdate` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `product_products_attr` (
  `product_id` int(11) NOT NULL,
  `attr_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `attr_value` text CHARACTER SET latin1 NOT NULL,
  `attr_type` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`product_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `product_products_ordr` (
  `product_id` int(11) NOT NULL,
  `ordr_name` varchar(64) NOT NULL,
  `ordr_value` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`ordr_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for defining multiple order fields for products';

CREATE TABLE IF NOT EXISTS `product_relatedproducts` (
  `product_id` int(11) NOT NULL,
  `product_relatedid` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`product_relatedid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `product_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_title` varchar(64) CHARACTER SET latin1 NOT NULL,
  `type_url` varchar(32) CHARACTER SET latin1 NOT NULL,
  `type_description` text CHARACTER SET latin1,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `snippet_snippets` (
  `snippet_id` int(11) NOT NULL AUTO_INCREMENT,
  `snippet_title` varchar(255) NOT NULL,
  `snippet_url` varchar(255) DEFAULT NULL,
  `snippet_order` int(11) NOT NULL DEFAULT '0',
  `snippet_published` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`snippet_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `snippet_snippets_attr` (
  `snippet_id` int(11) NOT NULL,
  `attr_name` varchar(32) NOT NULL,
  `attr_value` text NOT NULL,
  `attr_type` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`snippet_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_password` varchar(128) NOT NULL,
  `user_username` varchar(128) DEFAULT NULL,
  `user_email` varchar(128) DEFAULT NULL,
  `user_emailconfirmed` tinyint(1) NOT NULL,
  `user_salt` varchar(8) NOT NULL,
  `user_createdate` datetime NOT NULL,
  `user_lastlogin` datetime DEFAULT NULL,
  `user_state` varchar(16) NOT NULL DEFAULT '''unregistered''',
  `user_role` varchar(16) NOT NULL DEFAULT '''guest''',
  `user_token` varchar(128) DEFAULT NULL,
  `user_tokenexpire` datetime DEFAULT NULL,
  `user_firstname` varchar(128) DEFAULT NULL,
  `user_surname` varchar(128) DEFAULT NULL,
  `user_timezone` varchar(32) DEFAULT 'Europe/London',
  `user_sites` set('default') NOT NULL DEFAULT 'default',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_email` (`user_email`),
  KEY `user_role` (`user_role`),
  KEY `user_username` (`user_username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_users_attr` (
  `user_id` int(11) NOT NULL,
  `attr_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `attr_value` text CHARACTER SET latin1 NOT NULL,
  `attr_type` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`user_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_users_ordr` (
  `user_id` int(11) NOT NULL,
  `ordr_name` varchar(64) NOT NULL,
  `ordr_value` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`ordr_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Table for defining multiple order fields for users';

--//@UNDO

