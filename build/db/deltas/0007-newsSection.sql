CREATE TABLE IF NOT EXISTS `news_sections` (
  `section_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `section_order` int(11) DEFAULT NULL,
  `section_url` varchar(64) NOT NULL,
  `section_title` varchar(64) NOT NULL,
  PRIMARY KEY (`section_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `news_articles` ADD section_id INT(11) NOT NULL DEFAULT 0 AFTER category_id;

--//@UNDO

DROP TABLE `news_sections`;
ALTER TABLE `news_articles` DROP section_id;
