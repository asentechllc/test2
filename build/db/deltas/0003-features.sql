CREATE TABLE `feature_features` (
  `feature_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role` varchar(32) NOT NULL DEFAULT 'guest',
  `feature_createdate` datetime NOT NULL,
  `feature_publishdate` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_id_author` int(11) DEFAULT NULL,
  `feature_authorname` varchar(64) DEFAULT NULL,
  `feature_authorimage` varchar(128) DEFAULT NULL,
  `feature_partnername` varchar(64) DEFAULT NULL,
  `feature_partnerimage` varchar(128) DEFAULT NULL,
  `feature_authortype` enum('anonymous','regular','guest','partner') DEFAULT 'regular',
  `type_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `feature_order` int(11) NOT NULL,
  `feature_draft` tinyint(1) NOT NULL,
  `feature_published` tinyint(1) NOT NULL,
  `feature_url` varchar(512) NOT NULL DEFAULT '',
  `feature_title` varchar(512) NOT NULL DEFAULT '',
  `feature_content` longtext,
  `feature_image` varchar(255) DEFAULT NULL,
  `feature_sites` set('default') DEFAULT 'default',
  `feature_numread` int(11) NOT NULL,
  `feature_numcomment` int(11) NOT NULL,
  `feature_numemail` int(11) NOT NULL,
  PRIMARY KEY (`feature_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE `feature_features_attr` (
  `feature_id` int(11) NOT NULL,
  `attr_name` varchar(32) CHARACTER SET latin1 NOT NULL,
  `attr_value` text CHARACTER SET latin1,
  `attr_type` varchar(32) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`feature_id`,`attr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `feature_features_ordr` (
  `feature_id` int(11) NOT NULL,
  `ordr_name` varchar(64) CHARACTER SET latin1 NOT NULL,
  `ordr_value` int(11) NOT NULL,
  PRIMARY KEY (`feature_id`,`ordr_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Table for defining multiple order fields for articles';

CREATE TABLE `feature_feature_categories` (
  `feature_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`feature_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--//@UNDO

DROP TABLE `feature_features`;
DROP TABLE `feature_features_attr`;
DROP TABLE `feature_features_ordr`;
DROP TABLE `feature_feature_categories`;

