CREATE TABLE `settings` (
  `group` varchar(16) NOT NULL DEFAULT '',
  `setting` varchar(32) NOT NULL DEFAULT '',
  `value` text,
  PRIMARY KEY (`group`,`setting`),
  KEY `group` (`group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--//@UNDO

DROP TABLE `settings`;

