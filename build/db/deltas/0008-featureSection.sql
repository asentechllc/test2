ALTER TABLE `feature_features` ADD section_id INT(11) NOT NULL DEFAULT 0 AFTER category_id;

--//@UNDO

ALTER TABLE `feature_features` DROP section_id;
