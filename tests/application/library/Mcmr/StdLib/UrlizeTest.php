<?php

class UrlizeTest extends ControllerTestCase
{
    public function testEndingHyphen()
    {
        $url = Mcmr_StdLib::urlize('Apps Of The Day: YouTube Remote, Halo Waypoint, Martha Stewart and more...');
        $this->_testUrl($url);
    }
    
    
    protected function _testUrl($url) 
    {
        $this->_testAllLower($url);
        $this->_testAllowedCharacters($url);
        $this->_testSingleSeparator($url);
        $this->_testIdempotence($url);
    }
    
    /**
     * Test if the url is all lower case
     */
    protected function _testAllLower($url) 
    {
        $lower = strtolower($url);
        $this->assertEquals($lower, $url);
    }
    
    /**
     * Test if there are no characters other than letters, digits and hyphen
     */
    protected function _testAllowedCharacters($url) 
    {
        $forbidenCharsRemoved = preg_replace('/[^a-z0-9\-]/', '', $url);
        $this->assertEquals($forbidenCharsRemoved, $url);
    }
    
    protected function _testSingleSeparator($url) 
    {
        $multipleSeparatorsRemoved = preg_replace('/\-+/', '-', $url);
        $this->assertEquals($multipleSeparatorsRemoved, $url);
    }
    
    /**
     * Test that running urlize on a string again doesn't change it anymore
     */
    protected function _testIdempotence($url) 
    {
        $this->assertEquals($url, Mcmr_StdLib::urlize($url));
    }
    
}
