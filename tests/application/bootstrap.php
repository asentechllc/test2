<?php
error_reporting( E_ALL );

// Flag to display application statistics
defined('APPLICATION_STATS')
    || define('APPLICATION_STATS', (isset($_GET['APPLICATION_STATS']) ? $_GET['APPLICATION_STATS'] : getenv('APPLICATION_STATS') ? getenv('APPLICATION_STATS') : '0'));

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../../application'));

defined('SITE_PATH')
    || define('SITE_PATH', realpath(dirname(__FILE__) . '/../website/'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',  'testing');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

//require_once "Zend/Loader.php";
//Zend_Loader::registerAutoload();

/**
 * Zend_Application
 */
require_once 'Zend/Application.php';

require_once 'modules/ControllerTestCase.php';
require_once 'Mcmr/Test/DataBuilder.php';
require_once 'DataBuilder.php';
