<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   
 * @package    _ControllerTestCase
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ControllerTestCase.php 756 2010-06-08 12:00:11Z leigh $
 */

require_once 'Zend/Application.php';
require_once 'Mcmr/Test/PHPUnit/ControllerTestCase.php';
require_once 'PHPUnit/Extensions/Database/DataSet/FlatXmlDataSet.php';

/**
 * Description of ControllerTestCase
 *
 * @category   
 * @package    _ControllerTestCase
 * @subpackage 
 */
class ControllerTestCase extends Mcmr_Test_PHPUnit_ControllerTestCase
{
    static protected $_initilised = array();

    /**
     * @var Zend_Application
     */
    protected $_application = null;

    public function setUp()
    {
        $this->bootstrap = array($this, 'appBootstrap');

        parent::setUp();
    }

    public function appBootstrap()
    {
        $this->_application = new Zend_Application(APPLICATION_ENV, SITE_PATH . '/configs/application.ini');
        $this->_application->bootstrap();
        $this->getFrontController()->setParam('bootstrap', $this->_application->getBootstrap());

        // Create a dummy logged in user
        $user = new User_Model_User();
        $user->setId(1);
        Zend_Registry::set('authenticated-user', $user);
    }

    /**
     * Scrapes the form and retrieves the CSRF value using the provided DOM ID
     *
     * @param string $id
     * @return string
     */
    public function getFormCsrf($id)
    {
        $this->assertQuery("#$id");
        
        $html = $this->getResponse()->getBody();
        $dom = new Zend_Dom_Query($html);
        return $dom->query("#{$id}")->current()->getAttribute('value');
    }

    /**
     * Get the ID of the newly created model. Scrapped from the redirect string
     *
     * @return int
     */
    public function getCreateId()
    {
        $headers = $this->getResponse()->getHeaders();
        $redirect = null;
        foreach ($headers as $row) {
            if ('Location' == $row['name']) {
                $redirect = $row['value'];
                break;
            }
        }

        if (null === $redirect) {
            $this->assertTrue(false, 'No redirection location. Unable to find ID');
            return;
        }

        $pieces = explode('/', $redirect);
        $id = intval(array_pop($pieces));

        if (!$id) {
            $this->assertTrue(false, 'Unable to find ID');
            return;
        }

        return $id;
    }

    /**
     * Return a test DB connection
     *
     * @return Zend_Test_PHPUnit_Db_Connection
     */
    protected function _getDbConnection()
    {
        $db = Zend_Db::factory('Pdo_Mysql', array(
            'host'=>'localhost',
            'username'=>'test_gossip',
            'password'=>'test_gossip',
            'dbname'=>'test_gossip',
            ));

        $connection = new Zend_Test_PHPUnit_Db_Connection($db, 'test_gossip');
        
        return $connection;
    }

    /**
     * Take a XML file and initilise the database for the tests
     *
     * @param string $file
     */
    protected function _initialiseDatabase($file)
    {
        if (!isset(self::$_initilised[$file])) {
            self::$_initilised[$file] = true;
            $connection = $this->_getDbConnection();
            $databaseTester = new Zend_Test_PHPUnit_Db_SimpleTester($connection);

            $databaseFixture = new PHPUnit_Extensions_Database_DataSet_FlatXmlDataSet(
                    SITE_PATH . '/data/' . $file
                    );
            $databaseTester->setupDatabase($databaseFixture);
        }
    }
}
