<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of CategoryModelTest
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Company2_CategoryModelTest extends ControllerTestCase
{
    protected $_categories = array();
    protected $_articles = array();
    
    public function testCompanyCategory()
    {
        $model = new Company_Model_Category();
        $this->assertEquals('virgin', $model->state());

        $mapper = Company_Model_Category::getMapper();
        $model->setTitle('Category');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
        $this->assertEquals('clean', $model->state());
        $this->assertEquals('category', $model->getUrl());
    }

    public function testCategoryRead()
    {
        DataBuilder::getInstance()->companyCategories();
        
        $mapper = Company_Model_Category::getMapper();
        $model = $mapper->findOneByField(array('url'=>'db-parent-0'));
        $this->assertNotNull($model);

        $this->assertEquals('clean', $model->state());
        $this->assertEquals('DB Parent 0', $model->getTitle());
    }

    public function testCategoryUpdate()
    {
        DataBuilder::getInstance()->companyCategories();
        
        $mapper = Company_Model_Category::getMapper();
        $model = $mapper->findOneByField(array('url'=>'db-parent-0'));
        $this->assertNotNull($model);

        $model->setTitle('DB Parent 0 Edit');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());

        // Re-load the model and test the values
        $model = null;
        $mapper = Company_Model_Category::getMapper();
        $model = $mapper->findOneByField(array('url'=>'db-parent-0'));
        $this->assertNotNull($model);

        $this->assertEquals('db-parent-0', $model->getUrl());
        $this->assertEquals('DB Parent 0 Edit', $model->getTitle());

    }
    
    public function testCategoryChildCreate()
    {
        // Create two new sub-categories
        $mapper = Company_Model_Category::getMapper();
        $parent = new Company_Model_Category();
        $parent->setTitle('Parent');

        try {
            $mapper->save($parent);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
        
        $child1 = new Company_Model_Category();
        $child1->setParentId($parent->getId());
        $child1->setTitle("Child 1");
        
        try {
            $mapper->save($child1);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
        
        $child2 = new Company_Model_Category();
        $child2->setParentId($parent->getId());
        $child2->setTitle("Child 2");
        
        try {
            $mapper->save($child2);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
    }
    
    public function testCategoryParent()
    {
        DataBuilder::getInstance()->companyTypes();
        DataBuilder::getInstance()->companyCategories();
        
        $mapper = Company_Model_Category::getMapper();
        $child1 = $mapper->findOneByField(array('url'=>'db-child-1-1'));
        
        $this->assertNotNull($child1);
        $this->assertEquals('clean', $child1->state());
        $this->assertEquals('DB Child 1 1', $child1->getTitle());
        
        $parent = $child1->getParent();
        
        $this->assertNotNull($parent);
        $this->assertEquals('clean', $parent->state());
        $this->assertEquals('DB Parent 1', $parent->getTitle());
    }
    
    public function testCategoryChildren()
    {
        $mapper = Company_Model_Category::getMapper();
        $parent = $mapper->findOneByField(array('url'=>'db-parent-1'));
        $this->assertNotNull($parent);

        $this->assertEquals('clean', $parent->state());
        $this->assertEquals('DB Parent 1', $parent->getTitle());
        
        $children = $parent->getChildren();
        
        $this->assertNotNull($children);
        $this->assertEquals('Mcmr_Paginator', get_class($children));
        $this->assertEquals(3, $children->getCurrentItemCount());
    }
}
