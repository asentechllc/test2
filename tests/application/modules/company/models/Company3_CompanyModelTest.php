<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of CompanyModelTest
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Company3_CompanyModelTest extends ControllerTestCase
{
    public function testCompanySave()
    {
        DataBuilder::getInstance()->CompanyTypes();
        
        $model = new Company_Model_Company();
        $mapper = Company_Model_Company::getMapper();

        $this->assertEquals('Company_Model_CompanyMapper', get_class($mapper));
        $this->assertEquals('virgin', $model->state());

        $model->setTitle("Test Company");
        $model->setDescription("Company Description");
        $model->setAttribute('test', 'test attribute');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $timeNow = time();
        $this->assertEquals('clean', $model->state());
        $this->assertEquals('1', $model->getUserid());
        $this->assertEquals("Test Company", $model->getTitle());
        $this->assertEquals("test attribute", $model->getAttribute('test'));

        $model = new Company_Model_Company();
        $model->setTitle("Test Company");
        $model->setAttribute('test', 'test attribute for duplicate');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());
    }

    public function testCompanyLoad()
    {
    }

    public function testCompanyUpdate()
    {
    }
}
