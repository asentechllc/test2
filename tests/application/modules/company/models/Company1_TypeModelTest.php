<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of TypeModelTest
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Company1_TypeModelTest extends ControllerTestCase
{
    public function testNewsType()
    {
        $model = new Company_Model_Type();
        $this->assertEquals('virgin', $model->state());

        $mapper = Company_Model_Type::getMapper();
        $model->setTitle('Type');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
        $this->assertEquals('clean', $model->state());
        $this->assertEquals('type', $model->getUrl());
    }

    public function testTypeRead()
    {
        $mapper = Company_Model_Type::getMapper();
        $model = $mapper->findOneByField(array('url'=>'type'));
        $this->assertNotNull($model);

        $this->assertEquals('clean', $model->state());
        $this->assertEquals('Type', $model->getTitle());
    }

    public function testTypeUpdate()
    {
        $mapper = Company_Model_Type::getMapper();
        $model = $mapper->findOneByField(array('url'=>'type'));
        $this->assertNotNull($model);

        $model->setTitle('Type Edit');
        
        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());

        // Re-load the model and test the values
        $model = null;
        $mapper = Company_Model_Type::getMapper();
        $model = $mapper->findOneByField(array('url'=>'type'));
        $this->assertNotNull($model);

        $this->assertEquals('type', $model->getUrl());
        $this->assertEquals('Type Edit', $model->getTitle());

    }
}
