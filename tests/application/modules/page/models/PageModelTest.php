<?php

class PageModelTest extends ControllerTestCase
{
    public function testPageModel()
    {
        $model = new Page_Model_Page();
        $this->assertEquals('virgin', $model->state());
    }

    public function testPageModelSave()
    {
        $model = new Page_Model_Page();
        $mapper = Page_Model_Page::getMapper();

        $this->assertEquals('virgin', $model->state());
        $this->assertEquals('Page_Model_PageMapper', get_class($mapper));

        $model->setTitle('Test Page');
        $model->setContent('This is the content for the test page');
        $model->setAttribute('test', 'test attributes');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());
        $this->assertEquals('test-page', $model->getUrl());
    }

    public function testPageLoad()
    {
        $mapper = Page_Model_Page::getMapper();
        $model = $mapper->findOneByField(array('url'=>'test-page'));
        $this->assertNotNull($model);

        $this->assertEquals('clean', $model->state());
        $this->assertEquals('Test Page', $model->getTitle());
        $this->assertEquals('This is the content for the test page', $model->getContent());
        $this->assertEquals('test attributes', $model->getAttribute('test'));
    }

    public function testPageUpdate()
    {
        $mapper = Page_Model_Page::getMapper();
        $model = $mapper->findOneByField(array('url'=>'test-page'));
        $this->assertNotNull($model);
        $this->assertEquals('clean', $model->state());

        $model->setTitle('Test Page edit');
        $model->setUrl('Test Page edit');
        $model->setContent('This is the content for the test page with an edit');
        $model->setAttribute('test', 'test attributes edit');
        
        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());

        // Re-load the page and test the values
        $model = null;
        $mapper = Page_Model_Page::getMapper();
        $model = $mapper->findOneByField(array('url'=>'test-page-edit'));
        
        $this->assertNotNull($model);
        $this->assertEquals('clean', $model->state());
        $this->assertEquals('Test Page edit', $model->getTitle());
        $this->assertEquals('This is the content for the test page with an edit', $model->getContent());
        $this->assertEquals('test attributes edit', $model->getAttribute('test'));
    }

    public function testDuplicateUrl()
    {
//        $model = new Page_Model_Page();
//        $mapper = Page_Model_Page::getMapper();
//
//        $this->assertEquals('virgin', $model->state());
//
//        $model->setTitle('Test Page');
//        $model->setContent('This is a duplicate url page');
//        $model->setUrl('test-page-edit');
//
//        try {
//            $mapper->save($model);
//            $this->fail("Was able to save a duplicate URL page");
//        } catch (Exception $e) {
//        }
    }
}
