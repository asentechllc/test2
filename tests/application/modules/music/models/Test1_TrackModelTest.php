<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of TrackModelTest
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Test1_TrackModelTest extends ControllerTestCase
{
    public function testMusicTrack()
    {
        $model = new Music_Model_Track();
        $this->assertEquals('virgin', $model->state());

        $mapper = Music_Model_Track::getMapper();
        $model->setTitle('test');
        $model->setArtist('test artist');
        $model->setLabel('test label');
        $model->setReleaseDate(100000);

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
        $this->assertEquals('clean', $model->state());
    }

    public function testTrackRead()
    {
        $mapper = Music_Model_Track::getMapper();
        $model = $mapper->findOneByField(array('title'=>'test'));
        $this->assertNotNull($model);

        $this->assertEquals('clean', $model->state());
        $this->assertEquals('test', $model->getTitle());
        $this->assertEquals('test artist', $model->getArtist());
        $this->assertEquals('test label', $model->getLabel());
        $this->assertEquals(100000, $model->getReleaseDate());
    }

    public function testTrackUpdate()
    {
        $mapper = Music_Model_Track::getMapper();
        $model = $mapper->findOneByField(array('title'=>'test'));
        $this->assertNotNull($model);

        $model->setTitle('test edit');
        $model->setLabel('test label edit');
        
        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());

        // Re-load the model and test the values
        $model = null;
        $mapper = Music_Model_Track::getMapper();
        $model = $mapper->findOneByField(array('title'=>'test edit'));
        $this->assertNotNull($model);

        $this->assertEquals('test edit', $model->getTitle());
        $this->assertEquals('test label edit', $model->getLabel());

    }
}
