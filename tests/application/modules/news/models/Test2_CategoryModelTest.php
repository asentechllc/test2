<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of CategoryModelTest
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Test2_CategoryModelTest extends ControllerTestCase
{
    protected $_categories = array();
    protected $_articles = array();
    
    public function testNewsCategory()
    {
        $model = new News_Model_Category();
        $this->assertEquals('virgin', $model->state());

        $mapper = News_Model_Category::getMapper();
        $model->setTitle('Category');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
        $this->assertEquals('clean', $model->state());
        $this->assertEquals('category', $model->getUrl());
    }

    public function testCategoryRead()
    {
//        $mapper = News_Model_Category::getMapper();
//        $model = $mapper->findOneByField(array('url'=>'category'));
//        $this->assertNotNull($model);
//
//        $this->assertEquals('clean', $model->state());
//        $this->assertEquals('Category', $model->getTitle());
    }

    public function testCategoryUpdate()
    {
        $mapper = News_Model_Category::getMapper();
        $model = $mapper->findOneByField(array('url'=>'category'));
        $this->assertNotNull($model);

        $model->setTitle('Category Edit');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());

        // Re-load the model and test the values
        $model = null;
        $mapper = News_Model_Category::getMapper();
        $model = $mapper->findOneByField(array('url'=>'category-edit'));
        $this->assertNotNull($model);

        $this->assertEquals('category-edit', $model->getUrl());
        $this->assertEquals('Category Edit', $model->getTitle());

    }
}
