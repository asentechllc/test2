<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of ArticleModelTest
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Test3_ArticleModelTest extends ControllerTestCase
{
    public function testArticleModel()
    {
        $model = new News_Model_Article();
        $this->assertEquals('virgin', $model->state());
    }

    public function testArticleSave()
    {
        DataBuilder::getInstance()->newsTypes();
        
        $model = new News_Model_Article();
        $mapper = News_Model_Article::getMapper();

        $this->assertEquals('News_Model_ArticleMapper', get_class($mapper));
        $this->assertEquals('virgin', $model->state());

        $model->setTitle("Test Article");
        $model->setIntro("Intro paragraph");
        $model->setContent("Main content");
        $model->setStrap("Strapline");
        $model->setAttribute('test', 'test attribute');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $timeNow = time();
        $this->assertEquals('clean', $model->state());
        $this->assertEquals('1', $model->getUserid());
        $this->assertTrue(($model->getCreatedate() === $timeNow) || ($model->getCreatedate() === ($timeNow-1)));
        $this->assertTrue(($model->getPublishdate() === $timeNow) || ($model->getPublishdate() === ($timeNow-1)));
        $this->assertFalse($model->getPublished());
        $this->assertFalse($model->isPublished());
        $this->assertFalse($model->getFeatured());
        $this->assertFalse($model->getStream());
        $this->assertEquals('test-article', $model->getUrl());
        $this->assertEquals("Test Article", $model->getTitle());
        $this->assertEquals("Intro paragraph", $model->getIntro());
        $this->assertEquals("Main content", $model->getContent());
        $this->assertEquals("Strapline", $model->getStrap());
        $this->assertEquals("test attribute", $model->getAttribute('test'));
        $this->assertEquals('Zend_Paginator', get_class($model->getContent(TRUE)));

        // Test duplicate title unique url
        $model = new News_Model_Article();
        $model->setTitle("Test Article");
        $model->setIntro("Intro paragraph for duplicate");
        $model->setContent("Main content for duplicate");
        $model->setStrap("Strapline for duplicate");
        $model->setAttribute('test', 'test attribute for duplicate');

        // At this point the url should be the same as the previous model
        $this->assertEquals('test-article', $model->getUrl());
        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());
        $this->assertEquals('test-article-2', $model->getUrl());
    }

    public function testArticleLoad()
    {
        $mapper = News_Model_Article::getMapper();
        $model = $mapper->findOneByField(array('url'=>'test-article'));
        $this->assertNotNull($model);

        $this->assertEquals("Test Article", $model->getTitle());
        $this->assertEquals("Intro paragraph", $model->getIntro());
        $this->assertEquals("Main content", $model->getContent());
        $this->assertEquals("Strapline", $model->getStrap());
        $this->assertEquals("test attribute", $model->getAttribute('test'));
        $this->assertEquals('Zend_Paginator', get_class($model->getContent(TRUE)));

        $mapper = News_Model_Article::getMapper();
        $model = $mapper->findOneByField(array('url'=>'test-article-2'));
        
        $this->assertNotNull($model);
        $this->assertEquals("Test Article", $model->getTitle());
        $this->assertEquals("Intro paragraph for duplicate", $model->getIntro());
        $this->assertEquals("Main content for duplicate", $model->getContent());
        $this->assertEquals("Strapline for duplicate", $model->getStrap());
        $this->assertEquals("test attribute for duplicate", $model->getAttribute('test'));
        $this->assertEquals('Zend_Paginator', get_class($model->getContent(TRUE)));

    }

    public function testArticleUpdate()
    {
        $mapper = News_Model_Article::getMapper();
        $model = $mapper->findOneByField(array('url'=>'test-article'));
        $this->assertNotNull($model);
        $this->assertEquals('clean', $model->state());

        $model->setTitle("Test Article Edit");
        $model->setIntro("Intro paragraph Edit");
        $model->setContent("Main content Edit");
        $model->setStrap("Strapline Edit");
        $model->setAttribute('test', 'test attribute edit');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());

        // Re-load the model and test the values
        $model = null;
        $mapper = News_Model_Article::getMapper();
        $model = $mapper->findOneByField(array('url'=>'test-article'));

        $this->assertEquals("Test Article Edit", $model->getTitle());
        $this->assertEquals("Intro paragraph Edit", $model->getIntro());
        $this->assertEquals("Main content Edit", $model->getContent());
        $this->assertEquals("Strapline Edit", $model->getStrap());
        $this->assertEquals("test attribute edit", $model->getAttribute('test'));
        $this->assertEquals('Zend_Paginator', get_class($model->getContent(TRUE)));
    }

    public function testArticlePublished()
    {
        $mapper = News_Model_Article::getMapper();
        $model = $mapper->findOneByField(array('url'=>'test-article'));
        $this->assertNotNull($model);
        $this->assertEquals('clean', $model->state());

        // Article not published, date in the future
        $model->setPublishdate(strtotime("+1 day"));
        $model->setPublished(false);
        $this->assertFalse($model->isPublished());

        $model->setPublished(true);
        $this->assertFalse($model->isPublished());
        
        $model->setPublishdate(strtotime("-1 day"));
        $model->setPublished(false);
        $this->assertFalse($model->isPublished());

        $model->setPublished(true);
        $this->assertTrue($model->isPublished());
    }
}
