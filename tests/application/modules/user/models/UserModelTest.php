<?php

class UserModelTest extends ControllerTestCase
{
    public function testUserModel()
    {
        $user = new User_Model_User();
        $this->assertEquals('User_Model_User', get_class($user));
        $this->assertEquals('virgin', $user->state());
    }

    public function testUserModelSave()
    {
        $model = new User_Model_User();
        $mapper = User_Model_User::getMapper();

        $this->assertEquals('virgin', $model->state());
        $this->assertEquals('User_Model_UserMapper', get_class($mapper));

        $model->setEmail("test@test.com");
        $model->setFirstname("Test");
        $model->setSurname("User");
        $model->setPassword('testing');
        $model->setRole('user');
        $model->setState('registered');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());

        // Test duplicate email failure
        $model = new User_Model_User();
        
        $model->setEmail("test@test.com");
        $model->setFirstname("Test");
        $model->setSurname("User");
        $model->setPassword('testing');
        $model->setRole('user');
        $model->setState('registered');

        try {
            $mapper->save($model);
            $this->fail("New user saved with a duplicate email");
        } catch (Exception $e) {
        }

        $this->assertEquals('virgin', $model->state());
    }

    public function testUserModelUpdate()
    {
        $mapper = User_Model_User::getMapper();
        $model = $mapper->findOneByField(array('email'=>'test@test.com'));
        $this->assertNotNull($model);
        $this->assertEquals('clean', $model->state());

        $model->setFirstname('Test Edit');
        $model->setSurname("User Edit");

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
        $this->assertEquals('clean', $model->state());

        $model = null;
        $mapper = User_Model_User::getMapper();
        $model = $mapper->findOneByField(array('email'=>'test@test.com'));
        $this->assertNotNull($model);

        $this->assertEquals('Test Edit', $model->getFirstname());
        $this->assertEquals('User Edit', $model->getSurname());
    }

    public function testUserAuthentication()
    {
        $mapper = User_Model_User::getMapper();
        $credentials = array(
            'username' => 'test@test.com',
            'password' => 'fakePassword',
        );

        $model = $mapper->authenticate($credentials);
        $this->assertNull($model);
        $this->assertNull(Zend_Registry::get('authenticated-user')->getId());

        $credentials = array(
            'username' => 'test@test.com',
            'password' => 'testing',
        );
        $model = $mapper->authenticate($credentials);
        $this->assertNotNull($model);
        $this->assertNotNull(Zend_Registry::get('authenticated-user'));
        $this->assertEquals('test@test.com', $model->getEmail());
        $this->assertEquals('test@test.com', Zend_Registry::get('authenticated-user')->getEmail());

        try {
            $mapper->logout();
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()}");
        }
        $this->assertNull(Zend_Registry::get('authenticated-user')->getId());
    }

    public function testUserSearch()
    {
        try {
            $this->_setupUsers();
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $mapper = User_Model_User::getMapper();
        try {
            $models = $mapper->findAllByField(array('firstname'=>'Test'));
            $this->assertNotNull($models);
            $this->assertInstanceOf('Mcmr_Paginator', $models);
            $this->assertEquals(2, $models->getTotalItemCount());

            $models = null;
            $condition['email'] = array('condition'=>'LIKE', 'value'=>"%google%" );
            $models = $mapper->findAllByField($condition);
            $this->assertNotNull($models);
            $this->assertInstanceOf('Mcmr_Paginator', $models);
            $this->assertEquals(2, $models->getTotalItemCount());

        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
    }

    protected function _setupUsers()
    {
        // Create dummy users
        $users = array(
            array(
                'email'=>'test@mcmr.ws',
                'firstname'=>'Test',
                'surname'=>'Mcmr',
            ),
            array(
                'email'=>'test@google.com',
                'firstname'=>'Test',
                'surname'=>'Google',
            ),
            array(
                'email'=>'user@google.com',
                'firstname'=>'User',
                'surname'=>'Name',
            ),
            array(
                'email'=>'test@yahoo.com',
                'firstname'=>'Bob',
                'surname'=>'Smith',
            ),
            array(
                'email'=>'person@company.org',
                'firstname'=>'Julie',
                'surname'=>'Person',
            ),
            array(
                'email'=>'ttt@aaa.net',
                'firstname'=>'Ttt',
                'surname'=>'Aaa',
            ),
        );

        $mapper = User_Model_User::getMapper();
        foreach ($users as $userRow) {
            $model = new User_Model_User($userRow);
            $mapper->save($model);
        }
    }
}


