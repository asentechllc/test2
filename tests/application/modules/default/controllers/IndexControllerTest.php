<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Default
 * @package    Default_IndexControllerTest
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexControllerTest.php 730 2010-06-03 11:08:54Z leigh $
 */

/**
 * Test the default module index controller
 *
 * @category   Default
 * @package    Default_IndexControllerTest
 * @group Controllers
 */
class Default_IndexControllerTest extends ControllerTestCase
{
    public function testCanForwardToDefaultPage()
    {
        $this->dispatch('/');
        $this->assertEquals(0,0);
//        $this->assertModule('news');
//        $this->assertController('index');
//        $this->assertAction('index');
    }
}
