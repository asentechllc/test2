<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of RegistryModelTest
 *
 * @category 
 * @package 
 * @subpackage 
 */
class Test1_RegistryModelTest extends ControllerTestCase
{
    public function testDefaultRegistry()
    {
        $model = new Default_Model_Registry();
        $this->assertEquals('virgin', $model->state());

        $mapper = Default_Model_Registry::getMapper();
        $model->setName('test');
        $model->setValue('test value');

        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught: {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }
        $this->assertEquals('clean', $model->state());
    }

    public function testRegistryRead()
    {
        $mapper = Default_Model_Registry::getMapper();
        $model = $mapper->findOneByField(array('module'=>'default', 'name'=>'test'));
        $this->assertNotNull($model);

        $this->assertEquals('clean', $model->state());
        $this->assertEquals('test value', $model->getValue());
    }

    public function testRegistryUpdate()
    {
        $mapper = Default_Model_Registry::getMapper();
        $model = $mapper->findOneByField(array('module'=>'default', 'name'=>'test'));
        $this->assertNotNull($model);

        $model->setValue('test value edit');
        
        try {
            $mapper->save($model);
        } catch (Exception $e) {
            $this->fail("An unexpected exception has been caught {$e->getMessage()} {$e->getFile()}({$e->getLine()})");
        }

        $this->assertEquals('clean', $model->state());

        // Re-load the model and test the values
        $model = null;
        $mapper = Default_Model_Registry::getMapper();
        $model = $mapper->findOneByField(array('module'=>'default', 'name'=>'test'));
        $this->assertNotNull($model);

        $this->assertEquals('test value edit', $model->getValue());

    }
}
