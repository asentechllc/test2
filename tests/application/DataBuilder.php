<?php

class DataBuilder extends Mcmr_Test_DataBuilder
{
    protected $_newsCategories = null;
    protected $_newsTypes = null;
    protected $_newsArticles = null;
    protected $_companyTypes = null;
    protected $_companyCategories = null;
    
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        
        return self::$_instance;
    }
    
    public function newsTypes()
    {
        if (null === $this->_newsTypes) {
            $this->_newsTypes = array();
            $mapper = News_Model_Type::getMapper();

            for ($x=0; $x<3; $x++) {
                $model = new News_Model_Type();
                $model->setTitle("Type {$x}");
                $mapper->save($model);

                $this->_newsTypes[$x] = $model;
            }
        }
        
        return $this->_newsTypes;
    }
    
    public function newsCategories()
    {
        if (null === $this->_newsCategories) {
            $this->_newsCategories = array();
            $mapper = News_Model_Category::getMapper();
            
            for ($x=0; $x < 10; $x++) {
                $model = new News_Model_Category();
                $model->setTitle("Category {$x}");
                $mapper->save($model);
                
                $this->_newsCategories[$x] = $model;
            }
        }
        
        return $this->_newsCategories;
    }
    
    public function newsArticles()
    {
        if (null === $this->_newsArticles) {
            $types = $this->newsTypes();
            $categories = $this->newsCategories();

            $mapper = News_Model_Article::getMapper();

            foreach ($types as $typeNum=>$type) {
                for ($x=0; $x < 30; $x++) {
                    $article = new News_Model_Article();
                    $article->setTitle("Type {$typeNum} Article {$x}");
                    $article->setTypeid($this->_newsTypes[0]->getId());
                    $article->setContent("Test content for article {$x}");
                    
                    $index = $x % count($categories);
                    $category1 = $categories[$index];
                    $index = ($x+1) % count($categories);
                    $category2 = $categories[$index];
                    $index = ($x+2) % count($categories);
                    $category3 = $categories[$index];
                    
                    $article->setCategoryids(
                        array($category1->getId(), $category2->getId(), $category3->getId())
                    );
                    
                    $mapper->save($article);
                    
                    $this->_newsArticles[] = $article;
                }
            }
        }
        
        return $this->_newsArticles;
    }
    
    public function companyTypes()
    {
        if (null === $this->_companyTypes) {
            $this->_companyTypes = array();
            $mapper = Company_Model_Type::getMapper();

            for ($x=0; $x<3; $x++) {
                $model = new Company_Model_Type();
                $model->setTitle("DB Type {$x}");
                $mapper->save($model);

                $this->_companyTypes[$x] = $model;
            }
        }
        
        return $this->_companyTypes;
    }
    
    public function companyCategories()
    {
        if (null === $this->_companyCategories) {
            $this->_companyCategories = array();
            $mapper = Company_Model_Category::getMapper();
            
            for ($x=0; $x < 3; $x++) {
                $parent = new Company_Model_Category();
                $parent->setTitle("DB Parent {$x}");
                $mapper->save($parent);
                
                $this->_companyCategories[] = $parent;
                
                for ($y=0; $y < 3; $y++) {
                    $child = new Company_Model_Category();
                    $child->setParentId($parent->getId());
                    $child->setTitle("DB Child {$x} {$y}");
                    $mapper->save($child);

                    $this->_companyCategories[] = $child;
                }
            }
        }
        
        return $this->_companyCategories;
    }
}
