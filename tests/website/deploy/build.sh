#!/bin/bash
# Project build script.
# Uses Phing for project build and deployment
# $Id: build.sh 610 2010-06-01 12:18:09Z leigh $

PHING=/usr/bin/phing

if [ -f "$PHING" ]; then
    if [ -z "$GOSSIP_PATH" ]; then
        echo "Environment variable GOSSIP_PATH not set"
        exit;
    fi

    $PHING -buildfile $GOSSIP_PATH/deploy/build.xml -Dgossip.path=$GOSSIP_PATH -Denvironment=test
else
    echo "phing not found"
fi
