<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Snippet
 * @package Snippet_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * Bootstrap
 *
 * @category Snippet
 * @package Snippet_Bootstrap
 */
class Snippet_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');

        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Snippet_View_Helper');
    }
}