<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Snippet
 * @package    Snippet_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: SnippetIndex.php 711 2010-05-28 09:35:13Z leigh $
 */

/**
 * A view helper to retrieve a list of snippets
 *
 * @category   Snippet
 * @package    Snippet_View
 * @subpackage Helper
 */
class Snippet_View_Helper_SnippetIndex extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve an index of news articles based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function snippetIndex($conditions = array(), $order=null, $page=null, $partial = null)
    {
        $mapper = Snippet_Model_Snippet::getMapper();
        $snippets = $mapper->findAllByField($conditions, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('snippets'=>$snippets));
                }
            }

            return $this->view->partial($partial, array('snippets'=>$snippets));
        }

        return $snippets;
    }
}
