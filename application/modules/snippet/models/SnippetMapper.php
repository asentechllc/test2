<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Snippet
 * @package Snippet_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: SnippetMapper.php 1994 2010-12-30 14:40:04Z michal $
 */

/**
 * Mapper class for all Snippet models
 *
 * @category Snippet
 * @package Snippet_Model
 * @subpackage SnippetMapper
 */
class Snippet_Model_SnippetMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Snippet_Model_DbTable_Snippet';
    protected $_dbAttrTableClass = 'Snippet_Model_DbTable_SnippetAttr';
    protected $_columnPrefix = 'snippet_';
    protected $_modelClass = 'Snippet_Model_Snippet';
    protected $_cacheIdPrefix = 'SnippetSnippet';


    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'snippet_id':
            case 'snippetid':
            case 'snippet':
                $field = 'snippet_id';
                break;
            default:
                $field = $this->_columnPrefix.$field;
                break;
        }
        return $field;
    }
}
