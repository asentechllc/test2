<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Snippet
 * @package Snippet_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Snippet.php 1994 2010-12-30 14:40:04Z michal $
 */

/**
 * Model class for all Snippet Snippets
 *
 * @category Snippet
 * @package Snippet_Model
 * @subpackage Snippet
 */
class Snippet_Model_Snippet extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Snippet_Model_SnippetMapper';

    protected $_id = null;
    protected $_title = null;
    protected $_url = null;
    protected $_order = 1;
    protected $_published = null;

    private $_games = null;

    /**
     * Return mapper for model
     *
     * @return Snippet_Model_SnippetMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Snippet_Model_Snippet
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the Snippet name
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the platform title
     *
     * @param string $title
     * @return Snippet_Model_Snippet
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the Snippet order
     *
     * @return string
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * Set the platform order
     *
     * @param string $order
     * @return Snippet_Model_Snippet
     */
    public function setOrder($order)
    {
        $this->_order = $order;

        return $this;
    }

    /**
     * Get the Snippet url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->_url;
    }

    /**
     * Set the snippet url
     *
     * @param string $order
     * @return Snippet_Model_Snippet
     */
    public function setUrl($url)
    {
        $this->_url = $url;

        return $this;
    }

    /**
     * Get the Snippet published state
     *
     * @return string
     */
    public function getPublished()
    {
        return $this->_published;
    }

    /**
     * Set the snippet published state
     *
     * @param string $order
     * @return Snippet_Model_Snippet
     */
    public function setPublished($published)
    {
        $this->_published = $published;

        return $this;
    }
    
    public function isPublished() 
    {
        return $this->getPublished();
    }

}
