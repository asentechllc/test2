<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Snippet
 * @package    Snippet_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: SnippetAttr.php 718 2010-06-01 13:33:37Z leigh $
 */

/**
 * Description of ArticleAttr
 *
 * @category   Snippet
 * @package    Snippet_Model
 * @subpackage DbTable
 */
class Snippet_Model_DbTable_SnippetAttr extends Mcmr_Db_Table_Abstract implements Mcmr_Model_AttrInterface
{
    protected $_name = 'snippet_snippets_attr';
    protected $_primary = array('snippet_id', 'attr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insert($this->_rowData);
        } else {
            parent::insert($data);
        }
    }

    public function setAttrName($name)
    {
        $this->_rowData['attr_name'] = $name;

        return $this;
    }

    public function setAttrValue($value)
    {
        $this->_rowData['attr_value'] = $value;

        return $this;
    }

    public function setAttrId($id)
    {
        $this->_rowData['snippet_id'] = $id;

        return $this;
    }

    public function setAttrType($type)
    {
        $this->_rowData['attr_type'] = $type;

        return $this;
    }
}
