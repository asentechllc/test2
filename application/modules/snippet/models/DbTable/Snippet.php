<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Snippet
 * @package Snippet_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Platform.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * DB Table for the SnippetModel
 *
 * @category Snippet
 * @package Snippet_Model
 * @subpackage DbTable
 */
class Snippet_Model_DbTable_Snippet extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'snippet_snippets';
    protected $_primary = 'snippet_id';
}
