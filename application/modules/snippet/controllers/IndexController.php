<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Snippet
 * @package Snippet_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2051 2011-01-13 17:47:47Z michal $
 */

/**
 * Main Controller for the Snippet module. CRUD operations on snippets.
 *
 * @category Snippet
 * @package Snippet_IndexController
 */
class Snippet_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('ContextSwitch');
            $contextSwitch->addContext('html', array())
                ->addContext(
                    'csv', array(
                        'headers' => array('Content-Type' => 'text/csv'),
                        'suffix' => 'csv',
                    )
                );

            $contextSwitch->addActionContext('export', 'csv')
                    ->initContext();

            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                    ->addActionContext('index', 'json')
                    ->addActionContext('index', 'html')
                    ->addActionContext('create', 'html')
                    ->addActionContext('create', 'json')
                    ->addActionContext('register', 'html')
                    ->addActionContext('update', 'html')
                    ->addActionContext('update', 'json')
                    ->addActionContext('delete', 'html')
                    ->addActionContext('delete', 'json')
                    ->initContext();
        } catch (Exception $e) {

        }
    }

    /**
     * Show all snippets
     */
    public function indexAction()
    {
        if (null === $this->_request->getParam('id', null)) {
            $mapper = Snippet_Model_Snippet::getMapper();
            $fields = array();
            if ($this->_request->getParam('snippets_published') !== null) {
                $fields['published'] = $this->_request->getParam('snippets_published');
            }
            $order = array('order' => 'asc');
            $page = array(
                'page' => $this->_request->getParam('page', 1),
                'count' => $this->_request->getParam(
                    'count', Zend_Registry::get('snippet-config')->snippets->itemCountPerPage
                )
            );
            $this->view->snippets = $mapper->findAllByField($fields, $order, $page);
        } else {
            $this->_forward('read');
        }
    }

//indexAction

    public function orderAction()
    {
        $conditions = array('published' => 1);
        $order = array('order' => 'asc');
        $count = Zend_Registry::get('snippet-config')->snippets->itemCount;
        $mapper = Snippet_Model_Snippet::getMapper();
        $page['page'] = 1;
        $page['count'] = $count;

        $snippets = $mapper->findAllByField($conditions, $order, $page);

        $form = new Snippet_Form_Order(null, $snippets);

        $this->view->form = $form;
        $this->view->snippets = $snippets;

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                try {
                    $values = $form->getValues(true);
                    $snippets = $mapper->findAllByField(
                        array('published' => 1), null, array('page' => 1, 'count' => 100000)
                    );
                    foreach ($snippets as $snippet) {
                        $snippet->setOrder(100001);
                        $mapper->save($snippet);
                    }
                    foreach ($values['snippets'] as $order => $id) {
                        $snippet = $mapper->find($id);
                        $snippet->setOrder($order);
                        $mapper->save($snippet);
                    }
                } catch (Exception $e) {
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    $this->view->error = true;
                }
                $this->view->getHelper('DisplayMessages')->addMessage('Snippets Saved', 'info');
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('Incorrect Form Values', 'warn');
                $this->view->error = true;
            }
        }
    }

    /**
     * Show create snippet form on GET or process create request on POST.
     */
    public function createAction()
    {

        $form = Mcmr_Form::getInstance('Snippet_Form_Snippet');
        $this->view->form = $form;

        $snippetMapper = Snippet_Model_Snippet::getMapper();

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                $values = $form->getValues(true);
                unset($values['id']);
                $snippet = new Snippet_Model_Snippet();
                $snippet->setOptions($values);
                try {
                    $this->view->error = false;
                    $snippetMapper->save($snippet);
                    $this->view->getHelper('DisplayMessages')->addMessage('Snippet Saved', 'info');
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid form values', 'error');
            }
        }
    }

//createAction

    /**
     * Show a snippet identified by id or a url key.
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        if (null !== $id) {
            $mapper = Snippet_Model_Snippet::getMapper();
            $snippet = $mapper->find($id);
            if (null === $snippet) {
                throw new Mcmr_Exception_PageNotFound('Snippet not found');
            }
            $this->view->snippet = $snippet;
        } else {
            $this->_forward('index');
        }
    }

//readAction

    /**
     * Show update snippet form on GET or process update requeston POST.
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {

            $form = new Snippet_Form_Snippet();
            $this->view->form = $form;

            $mapper = Snippet_Model_Snippet::getMapper();
            $snippet = $mapper->find($id);

            if (null === $snippet) {
                throw new Mcmr_Exception_PageNotFound('Snippet not found');
            }

            if ($this->_request->getParam('thread_id') !== null) {
                $this->view->thread = Snippet_Model_Thread::getMapper()
                                ->findOneByField(array('id' => $this->_request->getParam('thread_id')));
            }

            $this->view->snippet = $snippet;

            if ($this->_request->isPost()) {
                $post = $this->_request->getPost();
                unset($post['snippetformsnippet']['id']);
                $snippetvalues = $snippet->getOptions(true);
                $post['snippetformsnippet'] = array_merge($snippetvalues, $post['snippetformsnippet']);

                if ($form->isValid($post)) {
                    $values = $form->getValues(true);
                    $snippet->setOptions($values);

                    try {
                        $this->view->error = false;
                        $mapper->save($snippet);
                        $this->view->getHelper('DisplayMessages')->addMessage('snippetSaved', 'info');
                        $this->view->getHelper('Redirect')->notify('update', $snippet);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                } else {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage('invalidForm', 'warn');
                }
            } else {
                $form->populate($snippet->getOptions());
            }
        } else {
            $this->_forward('index');
        }
    }

//updateAction

    /**
     * Show delete snippet confirmation on GET or delte snippet on POST.
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Snippet_Model_Snippet::getMapper();
            $snippet = $mapper->find($id);

            if (null === $snippet) {
                throw new Mcmr_Exception_PageNotFound('Snippet not found');
            }

            $this->view->snippet = $snippet;

            if ($this->_request->isPost()) {
                try {
                    $mapper->delete($snippet);
                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('snippetDeleted', 'info');
                    $this->view->getHelper('Redirect')->notify('delete', $snippet);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        } else {
            $this->_forward('index');
        }
    }

//deleteAction
}

//Snippet_IndexController
