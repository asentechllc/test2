<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Snippet
 * @package Snippet_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Page_Form_Page.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Form for a comment.
 *
 * @category Snippet
 * @package Snippet_Form
 * @subpackage Snippet
 */
class Snippet_Form_Snippet extends Mcmr_Form
{

    public function init()
    {
        parent::init();
        
        $this->setName('snippetformsnippet')->setElementsBelongTo('snippet-form-snippet');
        $this->setMethod('post');
    } //init


} //Snippet_Form_Snippet 
