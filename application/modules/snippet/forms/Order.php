<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Snippet
 * @package Snippet_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Chart.php 1921 2010-12-13 17:37:32Z michal $
 */

/**
 * Form class for snippet bulk update
 *
 * @category Snippet
 * @package Snippet_Form
 * @subpackage Snippets
 */
class Snippet_Form_Order extends Mcmr_Form
{
    protected $_snippets;

    public function __construct($options, $snippets)
    {
        $this->_snippets = $snippets;
        parent::__construct($options);
    }

    public function init()
    {
        parent::init();
        $this->setName('snippetformorder')->setElementsBelongTo('snippet-form-order');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        $count = $this->_snippets->getTotalItemCount();

        $subform = new Mcmr_Form();
        $subform->setElementsBelongTo('snippets');


        $displayGroupElements = array();
        for ($i = 1; $i <= $count; $i++) {
            $element = array(
                    'options' => array(
                            'label' => 'Snippet ' . $i,
                            'required' => true,
                            'value' => $this->_snippets->getItem($i)->getId(),
                            'class' => 'sortable'
                    ),
                    'optionSource' => array(
                            'mapper' => 'Snippet_Model_Snippet',
                            'conditions' => array('published' => 1),
                            'order' => array('order' => 'asc'),
                            'page' => array('page' => '1', 'count' => '100000'),
                            'displayField' => 'title',
                            'valueField' => 'id',
                    ));
            $element = $this->_processElement($element);
            $subform->addElement('Select', "$i", $element['options']);
            $displayGroupElements[] = "$i";
        }

        if (isset($displayGroupElements[0])) {
            $subform->addDisplayGroup($displayGroupElements, 'articles', array('class' => 'sortable-items'));
        }

        $this->addSubForm($subform, 'snippets');

        // Add some CSRF protection
        $this->addElement(
            'hash', 'csrf', array('salt' => 'unique')
        );
    }

}
