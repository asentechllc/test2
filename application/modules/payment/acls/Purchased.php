<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * ACL Assertion to ensure the user only views items they have purchased
 *
 * @category Payment
 * @package Payment_Acl
 * @subpackage Purchased
 */
class Payment_Acl_Purchased implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if the authenticated user owns this payment
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the user object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
        }

        if (null !== $id) {
            $mapper = Payment_Model_Item::getMapper();
            $item = $mapper->find($id);
            $basket = $item->getBasket();

            // If the item's basket has an OK status the item has been purchased
            return (Mcmr_Payment_AdapterAbstract::STATUS_OK === $basket->getStatus());
        } else {
            return false;
        }
    }
}
