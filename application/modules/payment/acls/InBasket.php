<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * ACL Assertion to ensure the user can only modify items in their basket
 *
 * @category Payment
 * @package Payment_Acl
 * @subpackage InBasket
 */
class Payment_Acl_InBasket implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if the item is in a basket owned by the user
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        $mapper = Payment_Model_Basket::getMapper();
        $basket = $mapper->findBasket();
        $user = Zend_Registry::get('authenticated-user');
        $controller = Zend_Controller_Front::getInstance();

        // If the user isn't logged in delete the ID from the parameters. Guest users don't delete via ID
        if (null === $user || null === $user->getId()) {
            $controller->getRequest()->setParam('id', null);

            return true;
        }

        // Get the ID of the user object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
        }

        if (null !== $id) {
            $items = $basket->getItems();
            foreach ($items as $item) {
                if ($id === $item->getId()) {
                    return true;
                }
            }

            return false;
        } else {
            // There is no ID, so allow it
            return true;
        }
    }
}
