<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of PurchaseToken
 *
 * @category Mcmr
 */
class Payment_Acl_PurchaseToken implements Zend_Acl_Assert_Interface
{
    
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null, Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
            $token = $params['token'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
            $token = $controller->getRequest()->getParam('token', null);
        }

        if (null !== $id) {
            $mapper = Payment_Model_Item::getMapper();
            $item = $mapper->find($id);
            
            return (
                (null !== $item)
                && ($item->getToken() === $token)
            );
        }
    }
}
