<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * ACL Assertion to ensure the user only views payments they own. Will force the
 * payments to only the authenticated user as well by setting the 'id' request parameter
 *
 * @category Payment
 * @package Payment_Acl
 * @subpackage Owner
 */
class Payment_Acl_Owner implements Zend_Acl_Assert_Interface
{
    
    /**
     * Returns true if and only if the authenticated user owns this payment
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        $controller = Zend_Controller_Front::getInstance();
        $user = Zend_Registry::get('authenticated-user');

        // Not authenticated. Deny all access
        if (null === $user) {
            return false;
        }

        // Get the ID of the basket object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $id = $controller->getRequest()->getParam('id', null);
        }

        if (null !== $id) {
            $mapper = Payment_Model_Basket::getMapper();
            $basket = null !== $id?$mapper->find($id):null;

            return (null !== $basket && ($basket->getUserid() === $user->getId()));
        } else {
            // There is no ID, so allow it, but force only this user
            $controller->getRequest()->setParam('user', $user->getId());

            return true;
        }
    }
}

