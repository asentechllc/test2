<?php
class Payment_PaymentController extends Zend_Controller_Action
{
    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('ContextSwitch');
            $contextSwitch->addContext('html', array())
                ->addContext(
                    'csv', array(
                        'headers'=>array('Content-Type'=>'text/csv'),
                        'suffix'=>'csv',
                    )
                );

            $contextSwitch->addActionContext('index', 'csv')
                    ->initContext();
        } catch (Exception $e) {
        }
    }

    public function indexAction()
    {
        // Order articles, newest to oldest
        $order = array('createdate' => 'desc', 'paymentdate' => 'desc');

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('payment-config')->baskets->itemCountPerPage);

        $fields['userid'] = $this->_getParam('user', null);
        $fields['status'] = $this->_getParam('status', null);
        $startDate = $this->_getParam('startDate', null);
        $endDate = $this->_getParam('endDate', null);
        if (null !== $startDate && null !== $endDate) {
            $fields['paymentdate'] = array('condition'=>'between', 'value'=>array($startDate, $endDate));
        }

        $mapper = Payment_Model_Payment::getMapper();
        $this->view->payments = $mapper->findAllByField($fields, $order, $page);
    }
}
