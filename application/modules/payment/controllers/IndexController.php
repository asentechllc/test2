<?php

/**
 * Index Controller for the Payment module
 *
 * @category Payment
 * @package Payment_IndexController
 * @subpackage 
 */
class Payment_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('ContextSwitch');
            $contextSwitch->addContext('html', array())
                ->addContext(
                    'csv', array(
                        'headers'=>array('Content-Type'=>'text/csv'),
                        'suffix'=>'csv',
                    )
                );

            $contextSwitch->addActionContext('index', 'csv')
                ->initContext();
        } catch (Exception $e) {

        }
    }

    public function indexAction()
    {
        // Order articles, newest to oldest
        $order = array('createdate' => 'desc', 'paymentdate' => 'desc');


        $fields['userid'] = $this->_getParam('user', null);
        $startDate = $this->_getParam('startDate', null);
        $endDate = $this->_getParam('endDate', null);
        if ( is_numeric( $startDate ) && is_numeric( $endDate )) {
            $fields['createdate'] = array('condition'=>'between', 'value'=>array($startDate, $endDate));
        }

        $page['page'] = $this->_getParam('page', 1);
        if ('csv' == $this->_helper->getHelper('ContextSwitch')->getCurrentContext()) {
            $page['count'] = $this->_getParam('count', Zend_Registry::get('payment-config')->baskets->itemCountPerPage);
        } else {
            $page['count'] = $this->_getParam('count', 1000000 );
        }

        $mapper = Payment_Model_Basket::getMapper();
        $this->view->baskets = $mapper->findAllByField($fields, $order, $page);
    }

    public function createAction()
    {
        $type = $this->getRequest()->getParam('type');
        if (null === $type) {
            throw new Mcmr_Exception_PageNotFound('Payment Type not supplied. Cannot load configuration');
        }

        $config = Zend_Registry::get('payment-config');
        if (!isset($config->$type)) {
            throw new Mcmr_Exception_PageNotFound('Cannot find payment profile');
        }

        // Set the view script to be the article type
        $this->_helper->viewRenderer('create/' . $type);

        $form = $this->_helper->loadForm('Payment', array('type' => $type));
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getParams())) {
                $values = $form->getValues(true);

                $basket = new Payment_Model_Basket();
                $basket->setOptions($values);
                $basket->setProfile($type);

                if (isset($values['address'])) {
                    // Build an address model from the form
                    $deliveryAddress = new Payment_Model_Address($values['address']);
                    $deliveryAddress->setType('delivery');
                    $billingAddress = clone $deliveryAddress;
                    $billingAddress->setType('billing');
                    $basket->setAddresses(array($deliveryAddress, $billingAddress));
                }

                try {
                    $mapper = Payment_Model_Basket::getMapper();
                    $mapper->save($basket);
                } catch (Exception $e) {

                }
            }
        } else {
            // Pre-populate the form from request variables
            $form->populate($this->_request->getParams());
        }
    }

    public function readAction()
    {
        $id = $this->_request->getParam('id', null);

        $mapper = Payment_Model_Basket::getMapper();
        if (null !== $id) {
            try {
                $basket = $mapper->find($id);
            } catch (Exception $e) {
                $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
            }
            
            if (null !== $basket) {
                $this->view->basket = $basket;
            } else {
                throw new Mcmr_Exception_PageNotFound('Basket not found');
            }
        } else {
            try {
                $this->view->basket = $mapper->findBasket();
            } catch (Exception $e) {
                $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                $this->view->basket = $mapper->findBasket();
            }
        }
    }

    public function updateAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (null !== $id) {
            $mapper = Payment_Model_Basket::getMapper();
            $basket = $mapper->find($id);
            $this->view->basket = $basket;

            if (null !== $basket) {
                $form = $this->_helper->loadForm('Basket');
                $this->view->form = $form;
                $form->populate($basket->getOptions(true));

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $basket->setOptions($values);
                        
                        try {
                            $mapper->save($basket);
                            
                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('basketSaved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $basket);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Payment Basket Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (null !== $id) {

        } else {
            throw new Mcmr_Exception_PageNotFound('Payment ID Does not exist');
        }
    }

    public function processAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (null !== $id) {
            $mapper = Payment_Model_Basket::getMapper();
            if ('session' === $id) {
                $basket = $mapper->findBasket();
            } else {
                $basket = $mapper->find($id);
            }
            $this->view->basket = $basket;
            
            if (null !== $basket) {
                $form = $this->_helper->loadForm('Basket');
                $this->view->form = $form;
                
                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $basket->setOptions($values);
                        
                        // Make sure an address is attached to the basket
                    } else {
                        $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
                    }
                } else {
                    $form->populate($basket->getOptions(true));
                }
                //initiate payment irrespectively if the request is post or get
                //this doesn't do anything other thatn display the payment form in an iframe
                if (null !== $basket->getAddress('billing')) {
                    // If this is a session basket we need to get a real ID and save it to the database.
                    if ('session' === $basket->getId()) {
                        $basket->setId(null);
                        $mapper->save($basket);
                    }
                    
                    try {
                        $config = Zend_Registry::get('payment-config');
                        $payment = $basket->doPayment($config);
                        $mapper->save($basket);

                        $this->view->payment = $payment;
                        $this->view->error = false;
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }

            } else {
                throw new Mcmr_Exception_PageNotFound('Payment ID Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function callbackAction()
    {
        if ($this->_request->isPost()) {
            $id = Zend_Filter::filterStatic($this->_request->getParam('VendorTxCode', null), "Digits");

            // Get the payment details
            $mapper = Payment_Model_Payment::getMapper();
            $payment = $mapper->find($id);

            $config = Zend_Registry::get('payment-config');
            try {
                $response = $payment->doCallback($config, $this->_request->getPost());
                $mapper->save($payment);


                if (Mcmr_Payment_Response::STATUS_OK === $payment->getStatus()) {
                    $basket = $payment->getBasket();
                    $basket->setStatus(Mcmr_Payment_Response::STATUS_OK);
                    $basket->setPaymentdate(time());
                    Payment_Model_Basket::getMapper()->save($basket);
                }
            } catch (Exception $e) {
                Mcmr_Debug::dump($e, 6, 'file');
            }

            echo $response;
            exit;
        }
    }

    public function successAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (null !== $id) {
            $mapper = Payment_Model_Basket::getMapper();
            $basket = $mapper->find($id);
            $this->view->basket = $basket;
            
            // Clear the session basket
            $mapper->clearSessionBasket();
        }
    }

    public function failureAction()
    {
        $id = $this->getRequest()->getParam('id');
        if (null !== $id) {
            $mapper = Payment_Model_Basket::getMapper();
            $basket = $mapper->find($id);
            $this->view->basket = $basket;
        }
    }
}
