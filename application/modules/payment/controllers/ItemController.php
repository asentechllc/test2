<?php

class Payment_ItemController extends Zend_Controller_Action
{

    public function readAction()
    {

    }

    public function updateAction()
    {

    }

    public function deleteAction()
    {
        $form = $this->_helper->loadForm('ItemDelete');
        $this->view->form = $form;

        if ($this->_request->isPost()) {

            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $mapper = Payment_Model_Basket::getMapper();
                $basket = $mapper->findBasket();
                $basket->deleteItem($values['modeltype'], $values['modelid']);

                try {
                    $mapper->save($basket);
                    $this->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('itemDeleted', 'info');
                } catch (Exception $e) {
                    $this->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidDelete', 'warn');
            }
        }
    }

    public function purchaseAction()
    {
        $form = $this->_helper->loadForm('Purchase');
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $basketMapper = Payment_Model_Basket::getMapper();
                $basket = $basketMapper->findBasket();

                try {
                    $values = $form->getValues(true);
                    $item = new Payment_Model_Item();
                    $item->purchase($basket, $this->_getProduct($values));

                    $basketMapper->save($basket);
                    $this->view->error = false;
                    $this->view->item = $item;
                    $this->view->basket = $basket;
                    $this->view->getHelper('DisplayMessages')->addMessage('itemAdded', 'info');
                    $this->view->getHelper('Redirect')->notify('purchase', $item);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        } else {
            $this->view->error = true;
            $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
        }
    }

    public function downloadAction()
    {
        $id = $this->_request->getParam('id', null);
        if (null !== $id) {
            $mapper = Payment_Model_Item::getMapper();
            $item = $mapper->find($id);
            if (null !== $item) {
                $model = $item->getModel();
                if ($model instanceof Payment_Model_ProductInterface) {
                    $download = $model->getDownload();
                    if (null !== $download) {
                        $config = Zend_Registry::get('payment-config');
                        $downloadPath = $config->item->downloadPath;

                        $file = $downloadPath . DIRECTORY_SEPARATOR . $download;
                        $this->_response->setHeader('Content-type', Mcmr_StdLib::getFileType($file));
                        $this->_response->setHeader('Content-Disposition', 'attachment; filename="'.$download.'"');
                        $this->_response->sendHeaders();
                        readfile($file);
                        
                        $model->recordDownload();
                        $mapper->save($item);
                        
                        // Exit as the file has been output
                        exit;
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Item Not Found');
            }
        } else {
            throw new Mcmr_Exception_PageNotFound('Item Not Found');
        }
    }

    private function _getProduct($options)
    {
        if (!empty($options['profile'])) {
            // Get the product from the configuration.
            $profile = $options['profile'];
            $config = Zend_Registry::get('payment-config');
            if (isset ($config->items->$profile)) {
                return $config->items->$profile;
            }
        } elseif (!empty($options['purchase'])) {
            $class = $options['purchase'];
            $mapper = call_user_func(array($class,"getMapper"));
            $model = $mapper->find($options['purchaseid']);

            if ($model instanceof Payment_Model_ProductInterface) {
                // Populate the product model with the object they are purchasing.
                // Do not populate if it is explicitly set
                $type = $model->getModeltype();
                if (!empty($options['model']) && empty($type)) {
                    $model->setModeltype($options['model']);
                } elseif (empty($type)) {
                    // If there is no model assume the product object is what is being purchased
                    $model->setModeltype(get_class($model));
                }

                $id = $model->getModelid();
                if (!empty($options['modelid']) && empty($id)) {
                    $model->setModelid($options['modelid']);
                } elseif (empty($id)) {
                    // If there is no model assume the product object is what is being purchased
                    $model->setModelid($model->getId());
                }

                // Ensure we created a valid product model
                if (null === $model->getModel()) {
                    throw new Zend_Exception('Invalid product. Unable to purchase');
                }

            } else {
                throw new Zend_Exception('Cannot purchase non-product "'.$options['purchase'].'"');
            }

            return $model;
        } else {
            throw new Zend_Exception("Unable to purchase unknown item");
        }
    }
}
