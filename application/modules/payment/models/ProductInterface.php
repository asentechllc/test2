<?php

interface Payment_Model_ProductInterface
{

    public function getModeltype();
    public function setModeltype($model);

    public function getModelid();
    public function setModelid($id);

    public function getModel();

    public function getAmount();

    public function getTaxrate();

    public function getDownload();

    public function getRedeem();
}
