<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all Payment Items
 *
 * @category Payment
 * @package Payment_Model
 * @subpackage Item
 */
class Payment_Model_Item extends Mcmr_Model_ModelAbstract implements Mcmr_Payment_Data_ItemInterface
{
    static protected $_mapperclass = 'Payment_Model_ItemMapper';
    
    protected $_id = null;
    protected $_basketid = null;
    protected $_modelid = null;
    protected $_modeltype = null;
    protected $_productid = null;
    protected $_producttype = null;
    protected $_count = null;
    protected $_token = null;
    
    private $_maxcount = null;

    /**
     * The item price including tax. Always in pence/cents
     *
     * @var int
     */
    protected $_amount = null;
    protected $_taxrate = null;
    protected $_description = null;
    protected $_redeem = null;

    private $_model = null;
    private $_product = null;
    
    
    /**
     * Return mapper for model
     *
     * @return Payment_Model_ItemMapper
     */
    static function getMapper()
    {
        return parent::getMapper(self::$_mapperclass);
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param int $id
     * @return Payment_Model_Item
     */
    public function setId($id)
    {
        $this->_id = $id;
        
        return $this;
    }

    /**
     * @param $_paymentid the $_paymentid to set
     */
    public function setBasketid($basketid)
    {
        $this->_basketid = $basketid;
        
        return $this;
    }

    /**
     * @return the $_paymentid
     */
    public function getBasketid()
    {
        return $this->_basketid;
    }
    
    /**
     * @return int
     */
    public function getModelid()
    {
        return $this->_modelid;
    }

    /**
     * @param int $modelid
     * @return Payment_Model_Item
     */
    public function setModelid($modelid)
    {
        $this->_modelid = $modelid;
        
        return $this;
    }

    /**
     * @return string
     */
    public function getModeltype()
    {
        return $this->_modeltype;
    }

    /**
     * @param string $modeltype
     * @return Payment_Model_Item
     */
    public function setModeltype($modeltype)
    {
        $this->_modeltype = $modeltype;
        
        return $this;
    }

    /**
     * Get the product ID for this item
     *
     * @return int
     */
    public function getProductid()
    {
        return $this->_productid;
    }
    
    /**
     * Set the product ID
     *
     * @param int $id
     * @return Payment_Model_Item 
     */
    public function setProductid($id)
    {
        $this->_productid = (int)$id;
        
        return $this;
    }
    
    /**
     * Get the class name for the product
     *
     * @return string
     */
    public function getProducttype()
    {
        return $this->_producttype;
    }
    
    /**
     * Set the product type
     *
     * @param string $producttype
     * @return Payment_Model_Item 
     */
    public function setProducttype($producttype)
    {
        $this->_producttype = $producttype;
        
        return $this;
    }
    
    /**
     * @return int
     */
    public function getAmount($incTax = true)
    {
        if (null === $this->_amount) {
            $this->_amount = 0;
        }
        
        if (!$incTax) {
            return round($this->_amount / (1 + $this->getTaxrate()/100));
        } else {
            return (int)$this->_amount;
        }
    }

    /**
     * @param int $amount
     * @return Payment_Model_Item
     */
    public function setAmount($amount, $incTax = true)
    {
        if (!$incTax) {
            $amount = $amount * (1 + $this->getTaxrate()/100);
        }

        $this->_amount = (int)$amount;
        
        return $this;
    }
    
    /**
     * @param float
     * @return Payment_Model_Item
     */
    public function setTaxrate($taxrate)
    {
        $this->_taxrate = $taxrate;
        
        return $this;
    }

    /**
     * @return float
     */
    public function getTaxrate()
    {
        if (null === $this->_taxrate) {
            $this->_taxrate = 0;
        }
        
        return $this->_taxrate;
    }

    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }

    public function getDescription()
    {
        if (null === $this->_description) {
            $model = $this->getModel();
            if (is_object($model) && method_exists($model, 'getTitle')) {
                $this->_description = $model->getTitle();
            } else {
                $this->_description = '';
            }
        }

        return $this->_description;
    }

    /**
     * Get the number of times this item can be redeemed
     *
     * @return int
     */
    public function getRedeem()
    {
        if (null === $this->_redeem) {
            $this->_redeem = 0;
        }

        return $this->_redeem;
    }

    /**
     * Set the number of times this item can be redeemed
     *
     * @param int $redeem
     */
    public function setRedeem($redeem)
    {
        $this->_redeem = (int)$redeem;

        return $this;
    }


    /**
     * Get the number of this item
     *
     * @return int
     */
    public function getCount()
    {
        if (null === $this->_count) {
            $this->_count = 1;
        }

        return $this->_count;
    }

    /**
     * Set the number of items being purchased
     *
     * @param int $count
     * @return Payment_Model_Item
     */
    public function setCount($count)
    {
        if ($count <= $this->getMaxcount()) {
            $this->_count = (int)$count;
        } else {
            $this->_count = $this->getMaxcount();
        }

        return $this;
    }

    /**
     * Get the maximum number of items that can be purchased
     *
     * @return int
     */
    public function getMaxcount()
    {
        if (null === $this->_maxcount) {
            $this->_maxcount = 1;
        }

        return $this->_maxcount;
    }

    /**
     * Set the maximum number of items that can be purchased
     *
     * @param int $maxcount
     * @return Payment_Model_Item 
     */
    public function setMaxcount($maxcount)
    {
        $this->_maxcount = (int)$maxcount;

        return $this;
    }

    public function getBasket()
    {
        $id = $this->getBasketid();
        if (null !== $id) {
            $mapper = Payment_Model_Basket::getMapper();
            return $mapper->find($id);
        } else {
            return null;
        }
    }

    /**
     * Get an instance of the model being purchased
     *
     * @return Mcmr_Model_ModelAbstract
     */
    public function getModel()
    {
        if (null === $this->_model) {
            $className = $this->getModeltype();
            $mapper = call_user_func(array($className, 'getMapper'));
            $this->_model = $mapper->find($this->getModelid());
        }

        return $this->_model;
    }

    /**
     * Get the product purchased for this item
     *
     * @return Payment_Model_ProductInterface 
     */
    public function getProduct()
    {
        if (null === $this->_product) {
            $className = $this->getProducttype();
            $id = $this->getProductid();
            if (null !== $className && null !== $id) {
                $mapper = call_user_func(array($className, 'getMapper'));
                $this->_product = $mapper->find($id);
            }
        }

        return $this->_product;
    }

    public function getTotalAmount($incTax=true, $units = 'pound')
    {
        $amount = $this->getAmount($incTax) * $this->getCount();

        switch ($units) {
            case 'pound':
            case 'dollar':
                $amount /= 100;
                break;
        }

        return $amount;
    }
    
    /**
     *
     * @return string 
     */
    public function getToken()
    {
        if (null === $this->_token) {
            $this->_token = Mcmr_StdLib::randomString(16, 32);
        }
        
        return $this->_token;
    }

    /**
     *
     * @param string $token
     * @return Payment_Model_Item 
     */
    public function setToken($token)
    {
        $this->_token = $token;
        
        return $this;
    }

        public function redeem($count = 1)
    {
        $redeem = $this->getRedeem();
        $redeem -= $count;

        if (0 > $redeem) {
            throw new Zend_Exception("This item cannot be redeemed. Count exceeded");
        }

        $this->setRedeem($redeem);

        return $this;
    }

    /**
     * Create this item and add it to the basket
     *
     * @param array $product
     */
    public function purchase(Payment_Model_Basket $basket, $product, $count = 1)
    {
        $this->_buildItem($product);

        $this->setCount($count);
        $basket->addItem($this);

        return $this;
    }

    /**
     * Function to check if the user has purchased this item previously
     *
     * @param mixed $product
     * @param User_Model_User $user
     * @return bool
     */
    public function hasPurchased($product, $user = null)
    {
        if (null === $user) {
            $user = Zend_Registry::get('authenticated-user');
            if (null === $user->getId()) {
                return false;
            }
        }

        $this->_buildItem($product);
        $mapper = self::getMapper();

        return $mapper->hasPurchased($this, $user);
    }

    private function _buildItem($product)
    {
        if (is_array($product)) {
            $this->setOptions($product);
        } elseif ($product instanceof Zend_Config) {
            $this->setOptions($product->toArray());
        } elseif ($product instanceof Payment_Model_ProductInterface) {
            $this->setProductid($product->getId());
            $this->setProducttype(get_class($product));
            $this->setModeltype($product->getModeltype());
            $this->setModelid($product->getModelid());
            $this->setAmount($product->getAmount());
            $this->setTaxrate($product->getTaxrate());
            $this->setRedeem($product->getRedeem());
        } else {
            throw new Mcmr_Model_Exception('Unable to build product ' . print_r($product, true));
        }

        return $this;
    }

    public function getQuantity()
    {
        return $this->getCount();
    }

    public function getTax()
    {
        return $this->getAmount() - $this->getAmount(false);
    }
}

