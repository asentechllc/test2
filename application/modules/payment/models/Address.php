<?php

class Payment_Model_Address extends Mcmr_Payment_Data_Address
{
    static protected $_mapperclass = 'Payment_Model_AddressMapper';
    
    protected $_id = null;
    protected $_basketid = null;
    protected $_type = null;

    /**
     * Return mapper for model
     *
     * @return Payment_Model_AddressMapper
     */
    static function getMapper()
    {
        return parent::getMapper(self::$_mapperclass);
    }

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * @param int $paymentid
     */
    public function setBasketid($basketid)
    {
        $this->_basketid = $basketid;

        return $this;
    }

    /**
     * @return int
     */
    public function getBasketid()
    {
        return $this->_basketid;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->_type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

}
