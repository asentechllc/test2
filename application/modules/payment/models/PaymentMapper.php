<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: PaymentMapper.php 1950 2010-12-17 12:45:04Z www-data $
 */

/**
 * Mapper class for all Payment models
 *
 * @category Payment
 * @package Payment_Model
 * @subpackage PaymentMapper
 */
class Payment_Model_PaymentMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Payment_Model_DbTable_Payment';
    protected $_dbAttrTableClass = 'Payment_Model_DbTable_PaymentAttr';

    protected $_columnPrefix = 'payment_';
    protected $_modelClass = 'Payment_Model_Payment';
    protected $_cacheIdPrefix = 'Payment';

    public function  __construct()
    {
        parent::__construct();

        $this->addObserverNamespace(array('prefix' => 'Payment_Model_Observer', 'path'=>dirname(__FILE__).'/Observer'));
        $this->registerObserver('PaymentBasket');
    }

    /**
     * Converts a row returned by database query to an instance of model class.
     * @param Zend_Db_Table_Row_Abstract $row
     * @param instance of the model class
     */
    protected function _convertRowToModel( $row ) 
    {
        if (!empty($row->payment_responseobject)) {
            $row->payment_responseobject = unserialize($row->payment_responseobject);
        } else {
            $row->payment_responseobject = null;
        }
        $model = parent::_convertRowToModel($row);
        
        return $model;
    }
    
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'basket':
            case 'basketid':
            case 'basket_id':
                $field = 'basket_id';
                break;
            
            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return strtolower($field);
    }
    
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'payment_items':
                return null;
                break;
            
            case 'payment_addresses':
                return null;
                break;
                
            case 'payment_paymentdate':
            case 'payment_createdate':
                if (is_array($value)) {
                    foreach ($value as $key=>$v) {
                        $value[$key] = $this->_sanitiseValue($field, $v);
                    }
                } else {
                    $value = new Zend_Db_Expr("FROM_UNIXTIME('{$value}')");
                }
                break;

            case 'payment_responseobject':
                $value = serialize($value);
                break;
        }
        
        return $value;
    }
    
    /**
     * Create a list of fields to be read form the databse in SELECT query.
     *
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(payment_paymentdate) AS payment_paymentdate";
        $fields[] = "UNIX_TIMESTAMP(payment_createdate) AS payment_createdate";

        return $fields;
    }
}
