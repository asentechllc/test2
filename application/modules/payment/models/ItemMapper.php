<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all Payment Items
 *
 * @category Payment
 * @package Payment_Model
 * @subpackage ItemMapper
 */
class Payment_Model_ItemMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Payment_Model_DbTable_Item';
    //protected $_dbAttrTableClass = '';
    protected $_columnPrefix = 'item_';
    protected $_modelClass = 'Payment_Model_Item';
    protected $_cacheIdPrefix = 'PaymentItem';

    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;
            
            case 'basket':
            case 'basketid':
            case 'basket_id':
                $field = 'basket_id';
                break;
            
            case 'product_id':
            case 'productid':
            case 'product':
                $field = 'product_id';
                break;
            
            case 'product_type':
            case 'producttype':
                $field = 'product_type';
                break;
            
            case 'basket_status':
                break;
                
            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }

    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        switch ($field) {
            case 'basket_status':
                $select->setIntegrityCheck(false);
                $select->join('payment_baskets', 'payment_baskets.basket_id = payment_items.basket_id');
                $select->where('payment_baskets.basket_status = ?', $value);
                break;
            
            default:
                $select = parent::_addCondition($select, $field, $value);
                break;
        }
        
        return $select;
    }
    
    /**
     * Return the number of times the product has been purchased
     * 
     * @param Mcmr_Model_ModelAbstract $model
     * @return type 
     */
    public function totalPurchases(Mcmr_Model_ModelAbstract $model)
    {
        $table = $this->getDbTable();
        $select = $table->select();
        $select->setIntegrityCheck(false);

        $select->from($table, array('purchases' => 'sum(item_count)'));
        $select->join('payment_baskets', 'payment_baskets.basket_id = payment_items.basket_id');
        $select->where('item_modeltype = ?', get_class($model));
        $select->where('item_modelid = ?', $model->getId());
        $select->where('payment_baskets.basket_status = ?', 'OK');

        $stmt = $select->query();
        $res = $stmt->fetch();
        
        return empty($res['purchases'])?0:(int)$res['purchases'];
    }
    
    /**
     * Function to check if the item has been purchased previously
     *
     * @param Payment_Model_Item $item
     * @return bool
     */
    public function hasPurchased(Payment_Model_Item $item, $user)
    {
        return false;
    }
}
