<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * An observer to change the status of the basket when a payment is processed
 *
 * @category Payment
 * @package Payment_Model
 * @subpackage Observer
 */
class Payment_Model_Observer_PaymentBasket extends Mcmr_Model_ObserverAbstract
{
    /**
     * Set the basket status to OK if the payment is OK
     *
     * @param Payment_Model_Payment $payment
     */
    public function update($payment)
    {
        if ($payment instanceof Payment_Model_Payment) {
            if (Mcmr_Payment_AdapterAbstract::STATUS_OK === $payment->getStatus()) {
                $basket = $payment->getBasket();
                $basket->setStatus($payment->getStatus());

                $mapper = Payment_Model_Basket::getMapper();
                $mapper->save($basket);
            }
        }
    }
}
