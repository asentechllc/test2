<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: PaymentMapper.php 1312 2010-09-05 15:17:00Z leigh $
 */

/**
 * Mapper class for all Payment models
 *
 * @category Payment
 * @package Payment_Model
 * @subpackage BasketMapper
 */
class Payment_Model_BasketMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Payment_Model_DbTable_Basket';
    //protected $_dbAttrTableClass = 'Payment_Model_DbTable_PaymentAttr';

    protected $_columnPrefix = 'basket_';
    protected $_modelClass = 'Payment_Model_Basket';
    protected $_cacheIdPrefix = 'PaymentBasket';

    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        $options = $model->getOptions();
        $data = array();
        foreach ($options as $field=>$value) {
            $field = $this->_sanitiseField($field);
            $value = $this->_sanitiseValue($field, $value);
            if (null !== $value) {
                $data[$field] = $value;
            }
        }

        if (null === ($id = $model->getId())) {
            // Perform an insert
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);

            $this->_saveAddresses($model);
            $this->_saveItems($model);

            // Notify Observers
            $this->notify('insert', $model);
        } elseif ('session' === ($id = $model->getId())) {
            // Save this basket to the session
            $session = $this->_getSession();
            $session->basketItems = serialize($model->getItems());

            $this->notify('session', $model);
        } else {
            // Perform an update
            $this->getDbTable()->update($data, array($this->_columnPrefix.'id=?'=>$id));

            $this->_saveAddresses($model);
            $this->_saveItems($model);

            // Notify observers
            $this->notify('update', $model);
        }

        return $this;
    }
    
    /**
     * Find the most recent unpaid basket
     *
     * @return Payment_Model_Basket
     */
    public function findBasket()
    {
        // If the user is logged in get the basket from the DB. Otherwise get it from the session
        $user = Zend_Registry::get('authenticated-user');
        $session = $this->_getSession();
        if (null !== $user && 'guest' !== $user->getRole()) {
            
            $basket = $this->findOneByField(
                array('status'=>array('value'=>'OK', 'condition'=>'<>'), 'userid'=>$user->getId()),
                array('createdate'=>'desc')
            );
            $save = false;
            if (null!==$basket) {
                $this->loadItems($basket); 
            } else  {
                $basket = new Payment_Model_Basket();
                $save = true;
            }
            
            // If there is a basket in session combine it with the db basket
            if (null !== $session->basketItems) {
                $items = unserialize($session->basketItems);
                $this->clearSessionBasket();
                foreach ($items as $item) {
                    $basket->addItem($item);
                }

                $this->notify('merge', $basket);
                $save = true;
            }

            if ($save) {
                $this->save($basket);
            }
        } else {
            // Load the basket from session, or create a new empty one
            $basket = new Payment_Model_Basket();
            $basket->setId('session'); // Set id to 'session' so that it is not saved to the db
            if (null !== $session->basketItems) {
                $basket->setItems(unserialize($session->basketItems));
            }
        }

        return $basket;
    }

    public function clearSessionBasket()
    {
        $session = $this->_getSession();
        unset($session->basketItems);
        
        return $this;
    }
    
    /**
     * Converts a row returned by database query to an instance of model class.
     * @param Zend_Db_Table_Row_Abstract $row
     * @param instance of the model class
     */
    protected function _convertRowToModel( $row ) 
    {
        $model = parent::_convertRowToModel($row);
        if (null !== $model) {
            $this->_loadAddresses($model);
            $this->loadItems($model);
        }
        return $model;
    }
    
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;
                
            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return strtolower($field);
    }
    
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'basket_items':
                return null;
                break;
            
            case 'basket_addresses':
                return null;
                break;

            case 'basket_paymentdate':
            case 'basket_createdate':
                if (is_array($value)) {
                    foreach ($value as $key=>$v) {
                        $value[$key] = $this->_sanitiseValue($field, $v);
                    }
                } else {
                    $value = new Zend_Db_Expr("FROM_UNIXTIME('{$value}')");
                }
                break;

            case 'basket_responseobject':
                $value = serialize($value);
                break;
        }
        
        return $value;
    }
    
    /**
     * Create a list of fields to be read form the databse in SELECT query.
     *
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(basket_createdate) AS basket_createdate";
        $fields[] = "UNIX_TIMESTAMP(basket_paymentdate) AS basket_paymentdate";

        return $fields;
    }
    
    protected function _loadAddresses($model)
    {
        $mapper = Payment_Model_Address::getMapper();
        $addresses = $mapper->findAllByField(
            array('basketid'=>$model->getId()), null, array('page'=>1, 'count'=>100000)
        );
        
        $model->setAddresses($addresses->getCurrentItems());
    }
    
    protected function _saveAddresses($model)
    {
        $addresses = $model->getAddresses();
        
        $mapper = Payment_Model_Address::getMapper();
        foreach ($addresses as $address) {
            $address->setBasketid($model->getId());
            $mapper->save($address);
        }
    }
    
    protected function loadItems($model)
    {
        $mapper = Payment_Model_Item::getMapper();
        $items = $mapper->findAllByField(array('basketid'=>$model->getId()), null, array('page'=>1, 'count'=>100000));
        
        $model->setItems($items->getCurrentItems());
    }
    
    protected function _saveItems($model)
    {
        $items = $model->getItems();

        $ids = array();
        $mapper = Payment_Model_Item::getMapper();
        foreach ($items as $item) {
            $ids[] = $item->getId();
            $item->setBasketid($model->getId());
            $mapper->save($item);
        }
    }

    private function _getSession()
    {
        return new Zend_Session_Namespace('Payment_Basket');
    }
}
