<?php

class Payment_Model_AddressMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Payment_Model_DbTable_Address';
    protected $_dbAttrTableClass = 'Payment_Model_DbTable_AddressAttr';
    protected $_columnPrefix = 'address_';
    protected $_modelClass = 'Payment_Model_Address';
    protected $_cacheIdPrefix = 'PaymentAddress';
    
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'basket':
            case 'basketid':
            case 'basket_id':
                $field = 'basket_id';
                break;
            
            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }

    protected function _sanitiseValues($values)
    {
        unset($values['address_taxIncluded']);
        return $values;
    }
}
