<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Payment
 * @package    Payment_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: PaymentAttr.php 718 2010-06-01 13:33:37Z leigh $
 */

/**
 * Attribute table for payments
 *
 * @category   Payment
 * @package    Payment_Model
 * @subpackage DbTable
 */
class Payment_Model_DbTable_PaymentAttr extends Mcmr_Db_Table_Abstract implements Mcmr_Model_AttrInterface
{
    protected $_name = 'payment_payments_attr';
    protected $_primary = array('payment_id', 'attr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insert($this->_rowData);
        } else {
            parent::insert($data);
        }
    }

    public function setAttrName($name)
    {
        $this->_rowData['attr_name'] = $name;

        return $this;
    }

    public function setAttrValue($value)
    {
        $this->_rowData['attr_value'] = $value;

        return $this;
    }

    public function setAttrId($id)
    {
        $this->_rowData['payment_id'] = $id;

        return $this;
    }

    public function setAttrType($type)
    {
        $this->_rowData['attr_type'] = $type;

        return $this;
    }
}
