<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Payment.php 1227 2010-08-16 10:44:48Z leigh $
 */

/**
 * DB Table for the Payment Model
 *
 * @category Payment
 * @package Payment_Model
 * @subpackage DbTable
 */
class Payment_Model_DbTable_Payment extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'payment_payments';
    protected $_primary = 'payment_id';
}
