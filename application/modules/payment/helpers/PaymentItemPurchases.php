<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View Helper for retrieving the purchases for the product
 *
 * @category Payment
 * @package Payment_View
 * @subpackage Helper
 */
class Payment_View_Helper_PaymentItemPurchases extends Zend_View_Helper_Abstract
{
    /**
     * Fetch all the purchases fpr the supplied model
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return int
     */
    public function paymentItemPurchases(Mcmr_Model_ModelAbstract $model, $order=null, $page=null)
    {
        $mapper = Payment_Model_Item::getMapper();
        $conditions = array(
            'modelid' => $model->getId(),
            'modeltype' => get_class($model),
            'basket_status' => 'OK',
        );
        
        return $mapper->findAllByField($conditions, $order, $page);
    }
}
