<?php
class Payment_View_Helper_PaymentBasketIndex extends Zend_View_Helper_Abstract
{
    public function paymentBasketIndex($conditions = array(), $order=null, $page=null, $partial = null)
    {
        $mapper = Payment_Model_Basket::getMapper();
        $baskets = $mapper->findAllByField($conditions, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('baskets'=>$baskets));
                }
            }

            return $this->view->partial($partial, array('baskets'=>$baskets));
        }

        return $baskets;
    }
}
