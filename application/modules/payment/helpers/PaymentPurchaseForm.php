<?php


class Payment_View_Helper_PaymentPurchaseForm extends Zend_View_Helper_Abstract
{
    public function paymentPurchaseForm($purchase, $model=null)
    {
        if ($purchase instanceof Payment_Model_ProductInterface) {
            $form = new Payment_Form_Purchase();

            $info = array();
            $info['purchase'] = get_class($purchase);
            $info['purchaseid'] = $purchase->getId();
            if (null !== $model) {
                $info['model'] = get_class($model);
                $info['modelid'] = $model->getId();
            }
            
            $form->populate($info);
        } else {
            throw new Zend_Exception('Cannot purchase this model');
        }

        return $form;
    }
}
