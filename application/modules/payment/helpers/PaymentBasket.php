<?php
class Payment_View_Helper_PaymentBasket extends Zend_View_Helper_Abstract
{
    public function paymentBasket()
    {
        $mapper = Payment_Model_Basket::getMapper();
        
        return $mapper->findBasket();
    }
}
