<?php


class Payment_View_Helper_PaymentDeleteForm extends Zend_View_Helper_Abstract
{
    public function paymentDeleteForm($model)
    {
        if ($model instanceof Payment_Model_Item) {
            $form = new Payment_Form_ItemDelete();

            $info = array();
            $info['modeltype'] = $model->getModeltype();
            $info['modelid'] = $model->getModelid();

            $form->populate($info);
        } else {
            throw new Zend_Exception('Cannot purchase this model');
        }

        return $form;
    }
}
