<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Payment
 * @package Payment_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View Helper for retrieving the number of times a product has been downloaded
 *
 * @category Payment
 * @package Payment_View
 * @subpackage Helper
 */
class Payment_View_Helper_PaymentItemPurchaseCount extends Zend_View_Helper_Abstract
{
    /**
     * Fetch the number of times the supplied model has been downloaded through the payment module
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return int
     */
    public function paymentItemPurchaseCount(Mcmr_Model_ModelAbstract $model)
    {
        $mapper = Payment_Model_Item::getMapper();
        return $mapper->totalPurchases($model);
    }
}
