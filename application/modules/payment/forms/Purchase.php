<?php
class Payment_Form_Purchase extends Mcmr_Form
{
    public function init()
    {
        $this->setName('paymentformpurchase')->setElementsBelongTo('payment-form-purchase');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
        $this->setAction('/payment/item/purchase');

        $this->addElement('hidden', 'purchase');
        $this->addElement('hidden', 'purchaseid');
        $this->addElement('hidden', 'model');
        $this->addElement('hidden', 'modelid');
        $this->addElement('hidden', 'profile');
    }
}
