<?php
class Payment_Form_ItemDelete extends Mcmr_Form
{
    public function init()
    {
        $this->setName('paymentformitemdelete')->setElementsBelongTo('payment-form-itemdelete');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
        $this->setAction('/payment/item/delete');

        $this->addElement('hidden', 'modeltype');
        $this->addElement('hidden', 'modelid');
    }
}
