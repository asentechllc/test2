<?php
class Payment_Form_Address extends Mcmr_Form
{
    public function init()
    {
        $this->setName('paymentformaddress')->setElementsBelongTo('payment-form-address');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }
}
