<?php
/**
 * Form class
 *
 * @category Payment
 * @package Payment_Form
 * @subpackage Payment
 */
class Payment_Form_Basket extends Mcmr_Form
{
    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $type) {
            // We have an payment type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'payment'
            . DIRECTORY_SEPARATOR . 'payment_form_payment-'.strtolower($type).'.ini';
        }

        parent::__construct($options);
    }
    
    public function init()
    {
        $this->setName('paymentformpayment')->setElementsBelongTo('payment-form-payment');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        $formLoader = Zend_Controller_Action_HelperBroker::getStaticHelper('LoadForm');
        $addressForm = $formLoader->loadForm('Address');
        $addressForm->setElementsBelongTo('address');
        $this->addSubForm($addressForm, 'address');

        // Add some CSRF protection
//        $this->addElement('hash', 'csrf', array(
//            'salt' => 'unique'));
    }

    public function populate(array $values)
    {
        if (isset($values['addresses'][0]) && $values['addresses'][0] instanceof Payment_Model_Address) {
            $form = $this->getSubForm('address');
            if (null !== $form) {
                $form->populate($values['addresses'][0]->getOptions(true));
            }
        }
        
        parent::populate($values);
    }
}
