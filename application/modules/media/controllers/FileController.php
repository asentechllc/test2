<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Media
 * @package Media_FileController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * File Controller for the Media module.
 *
 * @category Media
 * @package Media_FileController
 */
class Media_FileController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            
        } catch (Exception $e) {
            
        }
    }

    /**
     * Get a list of files
     */
    public function indexAction()
    {
        $fields = array();
        if (null !== ($setid = $this->_request->getParam('setid'))) {
            $fields['setid'] = $setid;
            $this->view->setid = $setid;
        }

        $setUrl = $this->_request->getParam('seturl');
        if (null !== $setUrl) {
            $mapper = Media_Model_Set::getMapper();
            $set = $mapper->findOneByField(array('url' => $setUrl));
            if (null !== $set) {
                $fields['setid'] = $set->getId();
                $this->view->setid = $set->getId();
            }
        }

        $order = array('date' => 'desc');

        $page = array();
        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('media-config')->media->itemCountPerPage);

        $mapper = Media_Model_File::getMapper();
        $this->view->files = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Get a particular file detail
     */
    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Media_Model_File::getMapper();
            if (null !== ($file = $mapper->find($id))) {
                $this->view->file = $file;
            } else {
                throw new Mcmr_Exception_PageNotFound('File does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Add a new file
     */
    public function createAction()
    {
        $type = $this->_request->getParam('type', null);
        $form = $this->_helper->loadForm('File', array('type' => $type));
        $this->view->form = $form;
        $this->view->type = $type;

        $setMapper = Media_Model_Set::getMapper();
        $setId = $this->_request->getParam('setid', null);
        $this->view->setid = $setId;
        $set = $setMapper->find($setId);
        if ($set === null) {
            throw new Zend_Exception('Trying to create a file without a set!');
        }
        $this->view->set = $set;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $values['setid'] = $setId;
                $file = new Media_Model_File();
                $mapper = Media_Model_File::getMapper();
                $file->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($file);

                    $this->view->getHelper('DisplayMessages')->addMessage('File Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $file);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'warn');
            }
        }
    }

    /**
     * Update file details
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Media_Model_File::getMapper();
            if (null !== ($file = $mapper->find($id))) {
                // get resource type - youtube or file
                $filename = $file->getFilename();
                if ((null == $filename) && (null != $file->getAttribute('embededcode'))) {
                    $type = 'youtube';
                } elseif (null != $filename) {
                    $type = 'file';
                } else {
                    $type = '';
                }

                $this->view->type = $type;
                $this->view->file = $file;

                $form = $this->_helper->loadForm('File', array('type' => $type));
                $form->populate($file->getOptions(true));
                $this->view->form = $form;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $file->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($file);

                            $this->view->getHelper('DisplayMessages')->addMessage('Changes Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $file);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($file->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('File does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete an file
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Media_Model_File::getMapper();
            if (null !== ($file = $mapper->find($id))) {
                $this->view->file = $file;
                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($file);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('File Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $file);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('File does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

}