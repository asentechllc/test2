<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Media
 * @package Media_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Index Controller for the Media module.
 *
 * @category Media
 * @package Media_IndexController
 */
class Media_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            
        } catch (Exception $e) {

        }
    }

    /**
     * Get a list of sets
     */
    public function indexAction()
    {
        $fields = array();

        $order = array('title' => 'desc');

        $page = array();
        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('media-config')->media->itemCountPerPage);

        $mapper = Media_Model_Set::getMapper();
        $this->view->sets = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Get a particular set detail
     */
    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Media_Model_Set::getMapper();
            if (null !== ($set = $mapper->find($id))) {
                $this->view->set = $set;
            } else {
                throw new Mcmr_Exception_PageNotFound('Set does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Add a new set
     */
    public function createAction()
    {
        $form = new Media_Form_Set();
        $this->view->form = $form;
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $set = new Media_Model_Set();
                $mapper = Media_Model_Set::getMapper();
                $set->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($set);

                    $this->view->getHelper('DisplayMessages')->addMessage('Set Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $set);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Update set details
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Media_Form_Set();
            $this->view->form = $form;

            $mapper = Media_Model_Set::getMapper();
            if (null !== ($set = $mapper->find($id))) {
                $this->view->set = $set;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $set->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($set);

                            $this->view->getHelper('DisplayMessages')->addMessage('Set Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $set);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($set->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Set does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete an set
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Media_Model_Set::getMapper();
            if (null !== ($set = $mapper->find($id))) {
                $this->view->set = $set;
                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($set);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Set Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $set);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Set does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

}