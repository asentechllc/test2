<?php
/** 
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Media
 * @package Media_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all media files
 *
 * @category Media
 * @package Media_Model
 * @subpackage File
 */
class Media_Model_File extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Media_Model_FileMapper';
    
    protected $_id = null;
    protected $_url = null;
    protected $_filename = null;
    protected $_type = null;
    protected $_description = null;
    protected $_userid = null;
    protected $_date = null;
    protected $_status = null;
    protected $_setid = null;
    
    private $_urlchanged = false;
    private $_userObject = null;
    private $_setObject = null;
    private $_speaker = null;
    
    /**
     * Return mapper for model
     *
     * @return Media_Model_FileMapper
     */
    static function getMapper($mapperclass=null)
    {
        return parent::getMapper(self::$_mapperclass);
    } //getMapper
    

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }
    
    /**
     * Set the ID
     *
     * @param int $id
     * @return Media_Model_File
     */
    public function setId($id)
    {
        $this->_id = $id;
        
        return $this;
    } //setId
    
    /**
     * Get the file's string ID
     *
     * @return string 
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getFilename());
        }
        
        return $this->_url;
    }
    
    /**
     * Set the file's string ID
     *
     * @param string $url
     * @return Media_Model_File 
     */
    public function setUrl($url)
    {
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }
        
        $this->_url = $url;
        
        return $this;
    }
    
    /**
     * Get media filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->_filename;
    }
    
    /**
     * Set media filename
     *
     * @param string $filename
     * @return Media_Model_File
     */
    public function setFilename($filename)
    {
        $this->_filename = $filename;
        
        return $this;
    } //setFilename
    

    /**
     * Get media file type
     *
     * @return string
     */
    public function getType()
    {
        if (null === $this->_type) {
            $config = Zend_Registry::get('media-config');
            $basePath = $config->file->basePath;
            $file = $basePath . $this->getFilename();
            if (is_file($file)) {
                $this->_type = Mcmr_StdLib::getFileType($file);
            } else {
                $this->_type = '';
            }
        }
        
        return $this->_type;
    }
    
    /**
     * Set media filetype
     *
     * @param string $filetype
     * @return Media_Model_File
     */
    public function setType($filetype)
    {
        $this->_type = $filetype;
        
        return $this;
    } //setFiletype
    

    /**
     * Get the file owner user id
     * @return int
     */
    public function getUserid()
    {
        if (null === $this->_userid) {
            $this->_userid = Zend_Registry::get('authenticated-user')->getId();
        }
        
        return $this->_userid;
    } //getUserid
    

    /**
     * Set file owner userid
     *
     * @param int $userid
     * @return Media_Model_File
     */
    public function setUserid($userid)
    {
        $this->_userid = $userid;
        return $this;
    } //setUserid
    

    /**
     * Get file description.
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    } //getDescription
    

    /**
     * Set file description.
     * @param string $desc file description
     * @return Media_Model_File
     */
    public function setDescription($desc)
    {
        $this->_description = $desc;
        return $this;
    } //setDescription
    

    /**
     * Return file creation timestamp.
     * 
     * @return int
     */
    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = time();
        }
        
        return $this->_date;
    } //getDate
    

    /**
     * Set file creation timesatmp
     * @param int $date. 
     * @return Media_Model_File
     */
    public function setDate($date)
    {
        $this->_date = (int) $date;
        
        return $this;
    } //setDate
    

    /**
     * Get media file status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->_status;
    }
    
    /**
     * Set media file status
     *
     * @param string $status
     * @return Media_Model_File
     */
    public function setStatus($status)
    {
        $this->_status = $status;
        
        return $this;
    } //setStatus
    

    /**
     * Get the file set id
     * @return int
     */
    public function getSetid()
    {
        return $this->_setid;
    } //getSetid
    

    /**
     * Set file set id
     *
     * @param int $setid
     * @return Media_Model_File
     */
    public function setSetid($setid)
    {
        $this->_setid = $setid;
        return $this;
    } //setSetid
    

    /**
     * Return the user who created the file.
     * @return User_Model_User
     */
    public function getUser()
    {
        if ($this->_userObject === null) {
            $mapper = User_Model_User::getMapper();
            $this->_userObject = $mapper->find($this->_userid);
        }
        return $this->_userObject;
    } //getUser
    

    /**
     * Return the media set that this file belongs too.
     * @return Media_Model_Set
     */
    public function getSet()
    {
        if ($this->_setObject === null) {
            $mapper = Media_Model_Set::getMapper();
            $this->_setObject = $mapper->find($this->_setid);
        }
        return $this->_setObject;
    } //getSet
    

    /**
     * Return speaker user object
     * @return User_Model_User
     */
    public function getSpeaker()
    {
        if ($this->_speaker === null) {
            $speakerId = $this->getAttribute('speaker');
            if (null !== $speakerId) {
                $mapper = User_Model_User::getMapper();
                $speaker = $mapper->find($speakerId);
                $this->_speaker = $speaker;
            }
        }
        
        return $this->_speaker;
    }
    
    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }
        
        return $changed;
    }
    
    /**
     * Check if the file is of a type
     * @param string pdf|image|zip|doc|ppt
     */
    public function is_a($type)
    {
        $fileType = $this->getType();
        switch ($type) {
            case 'pdf':
                return ($fileType == 'pdf') || ($fileType == 'application/pdf');
                break;
            case 'image':
                return ($fileType ==
                     'jpg') || ($fileType == 'gif') || ($fileType == 'png') ||
                     ($fileType == 'tif') || ($fileType == 'image/png') ||
                     ($fileType == 'image/gif') || ($fileType == 'image/jpeg') ||
                     ($fileType == 'image/tiff');
                break;
            case 'zip':
                return ($fileType == 'zip') || ($fileType == 'application/zip');
                break;
            case 'doc':
                return ($fileType ==
                     'doc') || ($fileType == 'docm') || ($fileType == 'docx') ||
                     ($fileType == 'application/msword') ||
                     ($fileType ==
                     'application/vnd.openxmlformats-officedocument.wordprocessingml.document');
                break;
            case 'ppt':
                return ($fileType ==
                     'ppt') || ($fileType == 'pptm') || ($fileType == 'pptx') ||
                     ($fileType == 'application/vnd.ms-powerpoint') ||
                     ($fileType ==
                     'application/vnd.openxmlformats-officedocument.presentationml.presentation');
                break;
            default:
                return false;
        }
    }
} //Media_Model_File 