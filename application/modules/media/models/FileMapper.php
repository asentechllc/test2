<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Media
 * @package Media_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for media files.
 *
 * @category Media
 * @package Media_Model
 * @subpackage Mapper
 */
class Media_Model_FileMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Media_Model_DbTable_File';
    protected $_dbAttrTableClass = 'Media_Model_DbTable_FileAttr';
    protected $_columnPrefix = 'file_';
    protected $_modelClass = 'Media_Model_File';
    protected $_cacheIdPrefix = 'Media_File';

    /**
     * @see Mcmr_Model_GenericMapper::save()
     */
    public function  save(Mcmr_Model_ModelAbstract $model)
    {
        // Ensure the URL is unique
        if (null === $model->getId() || $model->urlChanged()) {
            $model->setUrl($this->_getUniqueUrl($model));
        }

        parent::save($model);
    }

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseField()
     */
    protected function _sanitiseField($field)
    {
        switch (strtolower($field)) {
            case 'set':
            case 'setid':
            case 'set_id':
                $field = 'set_id';
                break;
            
            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;
            
            default:
                $field = $this->_columnPrefix.$field;
                break;
        }
        return $field;
    } //_sanitiseField
    
    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseValue()
     */
    protected function _sanitiseValue( $field, $value ) 
    {
        switch ( $field ) {
        case 'file_date':
            return new Zend_Db_Expr("FROM_UNIXTIME({$value})");
        default:
            return $value;
        }
    } //_sanitiseValue

    /**
     * @see Mcmr_Model_GenericMapper::_selectFields()
     */
    protected function _selectFields()
    {
        $all = parent::_selectFields();
        $all[] = "UNIX_TIMESTAMP(file_date) AS file_date";
        return $all;
    } //_selectFields

    /**
     * Ensure that the model has a unique URL. If the URL is taken generate
     * a new one and set that to the model
     *
     * @param News_Model_Article $model
     * @return string
     */
    private function _getUniqueUrl($model)
    {
        $url = $model->getUrl();
        if ($this->_urlExists($url)) {
            $urlTail = 2;
            $newUrl = Mcmr_StdLib::urlize($url . ' ' . $urlTail);
            while ($this->_urlExists($newUrl)) {
                $urlTail++;
                $newUrl = $url . '-' . $urlTail;
            }
            $model->setUrl($newUrl);

            return $newUrl;
        }

        return $url;
    }

    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($url)
    {
        return (null !== $this->findOneByField(array('url'=> $url)));
    }

} //Media_Model_FileMapper
