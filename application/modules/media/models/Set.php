<?php
/** 
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Media
 * @package Media_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all Media Sets
 *
 * @category Media
 * @package Media_Model
 * @subpackage Set
 */
class Media_Model_Set extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Media_Model_SetMapper';
    
    protected $_id = null;
    protected $_title = null;
    protected $_url = null;
    protected $_description = null;
    protected $_date = null;
    protected $_name = null;
    protected $_modeltype = null;
    protected $_modelid = null;
    
    private $_urlchanged = false;
    private $_files = null;
    private $_fileCount = null;
    
    /**
     * Return mapper for model
     *
     * @return Media_Model_SetMapper
     */
    static function getMapper($mapperclass=null)
    {
        return parent::getMapper(self::$_mapperclass);
    } //getMapper
    

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }
    
    /**
     * Set the ID
     *
     * @param int $id
     * @return Media_Model_Set
     */
    public function setId($id)
    {
        $this->_id = $id;
        
        return $this;
    } //setId
    

    /**
     * Get the media set's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }
    
    /**
     * Set media set's title
     *
     * @param string $title
     * @return Media_Model_Set
     */
    public function setTitle($title)
    {
        $this->_title = $title;
        
        return $this;
    } //setTitle
    
    /**
     * Get the media set URL ID string
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }
        
        return $this->_url;
    }
    
    /**
     * Set the media set's URL ID String
     *
     * @param string $url
     * @return Media_Model_Set 
     */
    public function setUrl($url)
    {
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }
        
        $this->_url = $url;
        
        return $this;
    }
    
    /**
     * Get the media set's description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }
    
    /**
     * Set media set's description
     *
     * @param string $desc
     * @return Media_Model_Set
     */
    public function setDescription($desc)
    {
        $this->_description = $desc;
        
        return $this;
    } //setDescription
    

    /**
     * Get the name.
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    } //getName
    

    /**
     * Set the name.
     * @param string $arg the new name
     * @return Media_Model_Set
     */
    public function setName($arg)
    {
        $this->_name = $arg;
        if (strpos($arg, '::') !== false) {
            $parts = explode('::', $arg);
            $this->_modeltype = $parts[0];
            $this->_modelid = $parts[1];
        }
        return $this;
    } //setName
    

    /**
     * Get the model name
     * @return string
     */
    public function getModeltype()
    {
        return $this->_modeltype;
    } //getModeltype
    

    public function setModeltype($type)
    {
        $this->_modeltype = $type;
        return $this;
    }
    
    /**
     * Get the model id
     * @return string
     */
    public function getModelid()
    {
        return $this->_modelid;
    } //getModelid
    
    /**
     * Set the media set's model ID
     *
     * @param type $id
     * @return Media_Model_Set 
     */
    public function setModelid($id)
    {
        $this->_modelid = $id;
        return $this;
    }
    
    /**
     * Check if this media set is for a model
     *
     * @return bool
     */
    public function isForModel()
    {
        return $this->getModeltype() != null;
    }
    
    /**
     * Return set's creation timestamp.
     * 
     * @return int
     */
    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = time();
        }
        
        return $this->_date;
    } //getDate
    

    /**
     * Set create timesatmp
     * @param int $unixtimestamp  
     * @return Media_Model_Set
     */
    public function setDate($unixtimestamp)
    {
        $this->_date = (int)$unixtimestamp;
        
        return $this;
    } //setDate
    

    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }
        
        return $changed;
    }
    
    /**
     * Get files in this media set
     * 
     * @return Zend_Paginator
     */
    public function getFiles($fields = null, $order = null, $page = null)
    {
        if ($this->_files === null) {
            $mapper = Media_Model_File::getMapper();
            if ($fields === null) {
                $fields = array();
            }
            $fields['setid'] = $this->getId();
            if ($order === null) {
                $order = array('date'=>'DESC');
            }
            $this->_files = $mapper->findAllByField($fields, $order, $page);
        }
        return $this->_files;
    } //getFiles
    

    /**
     * Count all files in a set.
     * 
     * @return int
     */
    public function countFiles($fields = null)
    {
        if ($this->_fileCount === null) {
            $mapper = Media_Model_File::getMapper();
            if ($fields === null) {
                $fields = array();
            }
            $fields['setid'] = $this->getId();
            $this->_fileCount = $mapper->countByField($fields);
        }
        return $this->_fileCount;
    } //countFiles
    

    /**
     * Generates standarized set name for model
     * 
     * @return string
     */
    public static function getNameForModel($modelObj)
    {
        if (! ($modelObj instanceof Mcmr_Model_ModelAbstract)) {
            throw new Mcmr_Model_Exception(
                'Trying to use set with non-model object'
            );
        }
        return get_class($modelObj) . '::' . $modelObj->getId();
    }
    
    /**
     * Return model object
     * 
     * @return Mcmr_Model_ModelAbstract
     */
    public function getModelObject()
    {
        $modelType = $this->getModeltype();
        if (null !== $modelType) {
            $model = new $modelType();
            return $model->getMapper()->find($this->getModelid());
        } else {
            return null;
        }
    }
} //Media_Model_Set