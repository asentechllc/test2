<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Media
 * @package Media_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for media set.
 *
 * @category Media
 * @package Media_Model
 * @subpackage Mapper
 */
class Media_Model_SetMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Media_Model_DbTable_Set';
    protected $_columnPrefix = 'set_';
    protected $_modelClass = 'Media_Model_Set';
    protected $_cacheIdPrefix = 'Media_Set';

    public function save(Mcmr_Model_ModelAbstract $model)
    {
        // Ensure the URL is unique
        if (null === $model->getId() || $model->urlChanged()) {
            $model->setUrl($this->_getUniqueUrl($model));
        }

        parent::save($model);
    }

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseField()
     */
    protected function _sanitiseField($field)
    {
        switch (strtolower($field)) {
            default:
                $field = $this->_columnPrefix . $field;
                break;
        }
        return $field;
    } //_sanitiseField

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseValue()
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'set_date':
                return new Zend_Db_Expr("FROM_UNIXTIME({$value})");
            default:
                return $value;
        }
    } //_sanitiseValue

    /**
     * @see Mcmr_Model_GenericMapper::_selectFields()
     */
    protected function _selectFields()
    {
        $all = parent::_selectFields();
        $all[] = "UNIX_TIMESTAMP(set_date) AS set_date";
        return $all;
    } //_selectFields

    /**
     * Finds a set for given file or create one if not found.
     * @param Mcmr_Model_Abastract $model 
     */
    public function findOrCreateSet(Mcmr_Model_ModelAbstract $model)
    {

        //generate the name from model. e.g. Event_Model_Event::26
        $name = Media_Model_Set::getNameForModel($model);

        //find existing set
        $set = $this->findOneByField(array('name' => $name));

        if ($set) {
            return $set;
        } else {
            $set = new Media_Model_Set();
            $set->setName($name);
            $set->setTitle($model->getTitle());
            $set->setDate(time());
            $this->save($set);
            return $set;
        }
    } //findOrCreateSet

    /**
     * Prevent deletion of set with files.
     * @see Mcmr_Model_GenericMapper::delete
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        if (count($model->getFiles())) {
            throw new Mcmr_Model_Exception('Cannot delete set with files');
        }
        parent::delete($model);
    } //delete

    /**
     * Ensure that the model has a unique URL. If the URL is taken generate
     * a new one and set that to the model
     *
     * @param News_Model_Article $model
     * @return string
     */
    private function _getUniqueUrl($model)
    {
        $url = $model->getUrl();
        if ($this->_urlExists($url)) {
            $urlTail = 2;
            $newUrl = Mcmr_StdLib::urlize($url . ' ' . $urlTail);
            while ($this->_urlExists($newUrl)) {
                $urlTail++;
                $newUrl = $url . '-' . $urlTail;
            }
            $model->setUrl($newUrl);

            return $newUrl;
        }

        return $url;
    }

    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($url)
    {
        return (null !== $this->findOneByField(array('url' => $url)));
    }

} //Media_Model_SetMapper
