<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Media
 * @package Media_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Database table class for the media set
 *
 * @category media
 * @package Media_Model
 * @subpackage DbTable
 */
class Media_Model_DbTable_Set extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'media_set';
    protected $_primary = 'set_id';
}
