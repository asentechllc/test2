<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Media
 * @package    Set_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 1311 2010-09-05 13:29:36Z leigh $
 */

/**
 * Description of Media Set form
 *
 * @category   Media
 * @package    Set_Form
 * @subpackage Set
 */
class Media_Form_Set extends Mcmr_Form
{
    public function init()
    {
        $this->setName('mediaformset')->setElementsBelongTo('media-form-set');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
        
        // Add some CSRF protection
        $this->addElement('hash', 'mediasetcsrf', array('salt' => 'unique'));
    }
}