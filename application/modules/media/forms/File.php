<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Media
 * @package    File_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 1311 2010-09-05 13:29:36Z leigh $
 */

/**
 * Description of file form
 *
 * @category   Media
 * @package    File_Form
 * @subpackage File
 */
class Media_Form_File extends Mcmr_Form
{
    public function  __construct($options=null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'media'
            . DIRECTORY_SEPARATOR . 'media_form_file-'.strtolower($type).'.ini';
        }
        parent::__construct($options);
    }

    public function init()
    {
        $this->setName('mediaformfile')->setElementsBelongTo('media-form-file');
        $this->setMethod('post');
        $this->setEnctype('multipart/form-data');
        
        // Add some CSRF protection
        $this->addElement(
            'hash', 'mediafilecsrf', array(
                'salt' => 'unique'
            )
        );
    }
}