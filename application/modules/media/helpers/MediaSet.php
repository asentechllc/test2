<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: NewsArticle.php 1186 2010-08-04 11:43:44Z leigh $
 */

/**
 * A View Helper for retrieving a single media set
 *
 * @category Media
 * @package Media_View
 * @subpackage Helper
 */
class Media_View_Helper_MediaSet extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve a media set based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function mediaSet($conditions = array(), $order=null, $partial = null)
    {
        $mapper = Media_Model_Set::getMapper();
        $set = $mapper->findOneByField($conditions, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('set'=>$set));
                }
            }

            return $this->view->partial($partial, array('set'=>$set));
        }

        return $set;
    }
}
