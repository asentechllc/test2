<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Media
 * @package Media_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Find or create a media set for a given model object
 *
 * @category Media
 * @package Mdeia_View
 * @subpackage 
 */
class Media_View_Helper_GetMediaSet extends Zend_View_Helper_Abstract
{

    /**
     * Find or create a media set for a given model object
     * 
     * @param $model: e.g. NEWS_MODEL_ARTICLE object  
     * @return Mdeia_Model_Set object
     */
    public function getMediaSet($modelObject)
    {
        $mapper = Media_Model_Set::getMapper();
        $set = $mapper->findOrCreateSet($modelObject);

        return $set;
    }

//getMdeiaSet
}

//Media_View_Helper_GetMediaSet 
