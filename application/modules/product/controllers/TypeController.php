<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_TypeController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: TypeController.php 2306 2011-03-31 10:43:22Z leigh $
 */

/**
 * TypeController for the Product module. Looks after all product types.
 *
 * @category Product
 * @package Product_TypeController
 */
class Product_TypeController extends Mcmr_Controller_Action
{
    /**
     * Index Action for all types
     */
    public function indexAction()
    {
        $type = $this->_request->getParam('type', null);
        if (null !== $type) {
            $this->_helper->viewRenderer('index/'.$type);
        }

        $mapper = Product_Model_Type::getMapper();
        $this->view->types = $mapper->findAllByField();
    }

    /**
     * Create a new type
     */
    public function createAction()
    {
        $form = new Product_Form_Type();
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $type = new Product_Model_Type();
                $mapper = Product_Model_Type::getMapper();
                $type->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($type);

                    $this->view->getHelper('DisplayMessages')->addMessage('Product Type Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $type);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Read a type.
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        
        if (null !== $id || null !== $url) {
            $mapper = Product_Model_Type::getMapper();
            // Get the product type from the ID or the URL provided
            $type = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            $this->view->type = $type;

            if (null !== $url) {
                $this->_helper->viewRenderer('read/'.$url);
            }

            if (null === $type) {
                throw new Mcmr_Exception_PageNotFound('Type not found');
            }
            
	        $year = $this->_request->getParam('year', date('Y'));
	        
	        $mapper = Product_Model_Product::getMapper();
			$this->view->issue = $mapper->find(intval($this->_request->getParam('issue', null)));

	        $db = $mapper->getDbTable()->getAdapter();
	        $years = $db->query("SELECT DISTINCT YEAR(product_createdate) AS year FROM product_products ORDER BY year DESC")->fetchAll();
	        $this->view->years = array();
	        foreach($years as $row)
	        	$this->view->years[$row['year']] = $row['year'];
	        	
	        # no year specified - default to latest
	        if(!$year && !empty($row))
	        	$year = $row['year'];
	        # no year specified and no digital editions -- fallback to current year to avoid infinite loop
	        else if(!$year)
	        	$year = date('Y');
	        	
	        $this->view->year = $year;
	        	
			// only show the correct issue for year and type			
			if($this->view->issue && (date('Y',$this->view->issue->getCreatedate()) != $this->view->year || $this->view->issue->getTypeid() != $type->getId())){
				$this->view->issue = null;
			}
			
	        
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a type
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Product_Form_Type();
            $this->view->form = $form;

            $mapper = Product_Model_Type::getMapper();
            if (null !== ($type = $mapper->find($id))) {
                $this->view->type = $type;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $type->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($type);

                            $this->view->getHelper('DisplayMessages')->addMessage('Product Type Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $type);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($type->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Product Type Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a type
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Product_Model_Type::getMapper();
            if (null !== ($type = $mapper->find($id))) {
                $this->view->type = $type;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($type);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Product Type Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $type);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Product Type Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
}
