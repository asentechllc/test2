<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_CategoryController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CategoryController.php 1130 2010-07-23 11:48:38Z leigh $
 */

/**
 * CategoryController for the Product module. Looks after all product categories.
 *
 * @category Product
 * @package Product_CategoryController
 */
class Product_CategoryController extends Zend_Controller_Action
{
    /**
     * Index Action for all categories
     */
    public function indexAction()
    {
        $mapper = Product_Model_Category::getMapper();
        $this->view->categories = $mapper->findAllByField();
    }

    /**
     * Create a new category
     */
    public function createAction()
    {
        $form = new Product_Form_Category();
        $this->view->form = $form;

        // Populate the form from request variables
        $form->populate($this->_request->getParams());

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $category = new Product_Model_Category();
                $mapper = Product_Model_Category::getMapper();
                $category->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($category);

                    $this->view->getHelper('DisplayMessages')->addMessage('Product Category Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $category);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Read a category.
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);

        if (null !== $id || null !== $url) {
            $mapper = Product_Model_Category::getMapper();
            // Get the product category from the ID or the URL provided
            $category = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            $this->view->category = $category;

            if (null !== $category) {
                $this->_helper->viewRenderer('read/'.$category->getType()->getUrl());
            } else {
                throw new Mcmr_Exception_PageNotFound('Category not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a category
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Product_Form_Category();
            $this->view->form = $form;

            $mapper = Product_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $this->view->category = $category;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $category->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($category);

                            $this->view->getHelper('DisplayMessages')->addMessage('Product Category Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $category);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($category->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Product Category Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a category
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Product_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $this->view->category = $category;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($category);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Product Category Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $category);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Product Category Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
}
