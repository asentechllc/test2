<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_DownloadController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: DownloadController.php 2306 2011-03-31 10:43:22Z leigh $
 */

/**
 * DownloadController for the Product module. Looks after all product types.
 *
 * @category Product
 * @package Product_DownloadController
 */
class Product_DownloadController extends Zend_Controller_Action
{
    /**
     * Index Action for all types
     */
    public function indexAction()
    {
        $type = $this->_request->getParam('type', null);
        if (null !== $type) {
            $this->_helper->viewRenderer('index/'.$type);
        }

        $mapper = Product_Model_Download::getMapper();
        $this->view->types = $mapper->findAllByField();
    }

    /**
     * Create a new type
     */
    public function createAction()
    {
        $productId = $this->_request->getParam('productid', null);
        $productUrl = $this->_request->getParam('producturl', null);
        $productMapper = Product_Model_Product::getMapper();
        $product = (null!==$productId)?$productMapper->find($productId):$productMapper->findOneByField(array('url'=>$productUrl));
        if (null!==$product) {
            $this->_request->setParam('productid', $product->getId());
            $form = new Product_Form_Download();
            $this->view->form = $form;
            $this->view->product = $product;
            if ($this->_request->isPost()) {
                if ($form->isValid($this->_request->getPost())) {
                    $values = $form->getValues(true);

                    $download = new Product_Model_Download();
                    $mapper = Product_Model_Download::getMapper();
                    $download->setOptions($values);
                    try {
                        $this->view->error = false;
                        $mapper->save($download);
                        $this->view->getHelper('DisplayMessages')->addMessage('downloadCreated', 'info');
                        $this->view->getHelper('Redirect')->notify('create', $download);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                } else {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'error');
                }
            }
        } else {
            throw new Mcmr_Exception_PageNotFound('Product not found');
        }
    }

    /**
     * Read a download.
     */
    public function readAction()
    {
        $url = $this->_request->getParam('url', null);
        $id = $this->_request->getParam('id', null);
        if (null !== $url || null!==$id) {
            $mapper = Product_Model_Download::getMapper();
            // Get the product download from the ID or the URL provided
            $download = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            if (null === $download) {
                throw new Mcmr_Exception_PageNotFound('Download not found');
            }
            $this->view->download = $download;
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a download
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Product_Form_Download();
            $this->view->form = $form;
            $mapper = Product_Model_Download::getMapper();
            if (null !== ($download = $mapper->find($id))) {
                $this->view->download = $download;
                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $download->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($download);

                            $this->view->getHelper('DisplayMessages')->addMessage('downloadUpdated', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $download);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($download->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('downloadNotFound');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a download
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Product_Model_Download::getMapper();
            if (null !== ($download = $mapper->find($id))) {
                $this->view->download = $download;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($download);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('downloadDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $download);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('downloadNotFound');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function downloadAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        if (null !== $id || null !== $url) {
            $mapper = Product_Model_Download::getMapper();
            // Get the product from the ID or the URL provided
            $download = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            if (null !== $download) {
                $product = $download->getProduct();
                $file = $product->getDownload();
                if (null !== $file) {
                    $config = Zend_Registry::get('product-config');
                    $downloadPath = $config->product->downloadPath;

                    $filePath = $downloadPath . DIRECTORY_SEPARATOR . $file;
                    $this->_response->setHeader('Content-type', Mcmr_StdLib::getFileType($filePath));
                    $this->_response->setHeader('Content-Disposition', 'attachment; filename="'.$file.'"');
                    $this->_response->sendHeaders();
                    readfile($file);
                    // Exit as the file has been output
                    exit;
                } else {
                    throw new Mcmr_Exception_PageNotFound('Product Not Found');
                }
            }
        }
    }

}
