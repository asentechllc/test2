<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2427 2011-05-12 14:19:53Z leigh $
 */

/**
 * TypeController for the Product module. Looks after all models.
 *
 * @category Product
 * @package Product_IndexController
 */
class Product_IndexController extends Mcmr_Controller_Action
{

	public function init(){
        $type = $this->_request->getParam('type',false);
        if(!$type && ($default = Zend_Registry::get('product-config')->defaultType))
	    	$this->_request->setParam('type',$default);
	    	
		parent::init();
	}

	public function magazineAction(){
    	$this->_helper->_layout->setLayout('one-column');
	}
	
    /**
     * Create a new model
     */
    public function createAction()
    {
        $type = $this->_request->getParam('type', Zend_Registry::get('product-config')->defaultType);
        $typeMapper = Product_Model_Type::getMapper();

        if (!empty($type)) {
            $type = $typeMapper->findOneByField(array('url'=>$type));
            if (null == $type) {
                throw new Mcmr_Exception_PageNotFound("Product type '{$type}' not found");
            }

            // Set the view script to be the article type
            $this->_helper->viewRenderer('create/'.$type->getUrl());

            $form = new Product_Form_Product(null, $type->getUrl());
            $this->view->form = $form;

            // Populate the form from request variables
            $form->populate($this->_request->getParams());

            if ($this->_request->isPost()) {
                if ($form->isValid($this->_request->getPost())) {
                    try {
                        $model = new Product_Model_Product();
                        $model->setOptions($form->getValues(true));
                        $model->setTypeid($type->getId());

                        $mapper = Product_Model_Product::getMapper();
                        $mapper->save($model);

                        $this->view->model = $model;
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Product Saved', 'info');
                        $this->view->getHelper('Redirect')->notify('create', $model);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        throw $e;
                    }
                } else {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage('Invalid form values', 'error');
                }
            }
        } else {
            $this->view->form = null;
            $this->view->types = $typeMapper->findAllByField();
        }
    }

    protected function _readAfterFind()
    {
        parent::_readAfterFind();
        $this->_helper->viewRenderer('read/'.$this->_model->getType()->getUrl());
    }

    /**
     * Update a model
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Product_Model_Product::getMapper();
            $model = $mapper->find($id);
            $this->view->model = $model;

            if (null !== $model) {
                $form = new Product_Form_Product(null, $model->getType()->getUrl());
                $this->view->form = $form;
                if ($this->_request->isPost()) {
                    $post = $this->_request->getPost();
                    $modelvalues = $model->getOptions(true);
                    $post['productformproduct'] = array_merge($modelvalues, $post['productformproduct']);
                    if ($form->isValid($post)) {
                        $values = $form->getValues(true);
                        $model->setOptions($values);
                        try {
                            $mapper->save($model);
                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('Product Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $model);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('Incorrect Form Values', 'warn');
                    }
                } else {
                    $values = $model->getOptions(true);
                    $form->populate($values);
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Product Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a model
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Product_Model_Product::getMapper();
            if (null !== ($model = $mapper->find($id))) {
                $this->view->model = $model;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($model);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Product Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $model);
                        $this->view->article = null;
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');;
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Product Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function downloadAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        if (null !== $id || null !== $url) {
            $mapper = Product_Model_Product::getMapper();
            // Get the model from the ID or the URL provided
            $model = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));

            if (null !== $model) {
                $download = $model->getDownload();
                if (null !== $download) {
                    $config = Zend_Registry::get('product-config');
                    $downloadPath = $config->model->downloadPath;

                    $file = $downloadPath . DIRECTORY_SEPARATOR . $download;
                    $this->_response->setHeader('Content-type', Mcmr_StdLib::getFileType($file));
                    $this->_response->setHeader('Content-Disposition', 'attachment; filename="'.$download.'"');
                    $this->_response->sendHeaders();
                    readfile($file);

                    $model->recordDownload();
                    
                    // Exit as the file has been output
                    exit;
                } else {
                    throw new Mcmr_Exception_PageNotFound('Product Not Found');
                }
            }
        }
    }
    
}
