<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ProductCategoryIndex.php 1104 2010-07-22 11:44:57Z leigh $
 */

/**
 * A View Helper for retrieving an index of product categories
 *
 * @category Product
 * @package Product_View
 * @subpackage Helper
 */
class Product_View_Helper_ProductCategoryIndex extends Zend_View_Helper_Abstract
{

    public function productCategoryIndex($conditions = array(), $order=null, $page=null, $partial = null)
    {
        if (isset($conditions['typename'])) {
            $mapper = Product_Model_Type::getMapper();
            $type = $mapper->findOneByField(array('url' => $conditions['typename']));

            if (null !== $type) {
                $conditions['type'] = $type->getId();
            }
            unset($conditions['typename']);
        }

        $mapper = Product_Model_Category::getMapper();
        $categories = $mapper->findAllByField($conditions, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('categories'=>$categories));
                }
            }

            return $this->view->partial($partial, array('categories'=>$categories));
        }

        return $categories;
    }
}