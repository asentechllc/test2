<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Product
 * @package    Product_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of Products based on the conditions
 *
 * @category   Product
 * @package    Product_View
 * @subpackage Helper
 */
class Product_View_Helper_ProductDownloadSize extends Zend_View_Helper_Abstract
{
    public function productDownloadSize( $product )
    {
        $download = $product->getDownload();
        if (null !== $download) {
            $config = Zend_Registry::get('product-config');
            $downloadPath = $config->product->downloadPath;

            $file = $downloadPath . DIRECTORY_SEPARATOR . $download;
            $size = @filesize($file);
            if ( false !== $size ) {
                $units = array(' B', ' KB', ' MB', ' GB' );
                for ($i = 0; $size >= 1024 && $i < 4; $i++) $size /= 1024;
                return round($size, 1).$units[$i];
            } else {
                return '';
            }
        }
    }
}
