<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ProductTypeSingle.php 1104 2010-07-22 11:44:57Z leigh $
 */

/**
 * A View Helper for retrieving a single product
 *
 * @category Product
 * @package Product_View
 * @subpackage Helper
 */
class Product_View_Helper_ProductProduct extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve a single product type based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function productProduct($conditions = array(), $order = null, $partial = null)
    {
        $mapper = Product_Model_Product::getMapper();
        $product = $mapper->findOneByField($conditions, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('product'=>$product));
                }
            }

            return $this->view->partial($partial, array('product'=>$product));
        }

        return $product;
    }
}