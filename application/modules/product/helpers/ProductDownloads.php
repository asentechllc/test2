<?php

class Product_View_Helper_ProductDownloads extends Zend_View_Helper_Abstract
{
    /**
     * Fetch the number of times the supplied model has been downloaded through the payment module
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return int
    */
    public function productDownloads(Product_Model_Product $model, $order=null, $page=null)
    {
        $mapper = Product_Model_Download::getMapper();
        
        $conditions = array(
            'product' => $model->getId(),
        );
        
        return $mapper->findAllByField($conditions, $order, $page);
    }
}
