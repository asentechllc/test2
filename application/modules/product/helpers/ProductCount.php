<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Product
 * @package    Product_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of Products based on the conditions
 *
 * @category   Product
 * @package    Product_View
 * @subpackage Helper
 */
class Product_View_Helper_ProductCount extends Zend_View_Helper_Abstract
{
    public function ProductCount($conditions = null)
    {
        $mapper = Product_Model_Product::getMapper();

        return $mapper->count($conditions);
    }
}
