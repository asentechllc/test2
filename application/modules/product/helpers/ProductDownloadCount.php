<?php

class Product_View_Helper_ProductDownloadCount extends Zend_View_Helper_Abstract
{
    /**
     * Fetch the number of times the supplied model has been downloaded through the payment module
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return int
    */
    public function productDownloadCount(Product_Model_Product $model)
    {
        $mapper = Product_Model_Product::getMapper();
        
        return $mapper->countDownloads($model);
    }
}
