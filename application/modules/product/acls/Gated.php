<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 */

/**
 * ACL to control access to gated content.
 *
 * @category Product
 * @package Product_Acl
 * @subpackage Gated
 */
class Product_Acl_Gated implements Zend_Acl_Assert_Interface
{
    
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null, Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
            $url = $controller->getRequest()->getParam('url', null);
        }

        if (null !== $id || null !== $url) {
            $mapper = Product_Model_Product::getMapper();
            $product = null !== $id?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            if ( null === $product ) {
                throw new Mcmr_Exception_PageNotFound('Product not found');
            }
            
            $role = $product->getUserrole();
            $userRole = Zend_Registry::get('authenticated-user')->getRole();
            $newResource = $resource->getResourceId() . '-' . $product->getId();
            $acl->add(new Zend_Acl_Resource($newResource));
            $acl->allow($role, $newResource, $privilege);
            
            return $acl->isAllowed($userRole, $newResource, $privilege);
        } else {
            // There is no ID, so allow it
            
            return true;
        }
    }
}
