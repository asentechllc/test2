<?php

/**
 * This ACL checks id *the downloa object* is a download for a free product.
 */
class Product_Acl_FreeDownload2  implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if the product is free to download
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the product object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
            $url = null;
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
            $url = $controller->getRequest()->getParam('url', null);
        }
        
        if (null !== $id || null !== $url) {
            $mapper = Product_Model_Download::getMapper();
            $download = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            if (null !== $download) {
                $product=$download->getProduct();
                $user = Zend_Registry::get('authenticated-user');
                return (0 == $product->getAmount($user));
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
