<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Product.php 1928 2010-12-14 16:13:31Z leigh $
 */

/**
 * Form class for Products
 *
 * @category Product
 * @package Product_Form
 * @subpackage Product
 */
class Product_Form_Product extends Mcmr_Form
{
    protected $_formType = null;

    public function __construct($options = null, $type = null)
    {
        if (null == $options & null != $type) {
            $type = strtolower($type);
            // We have an article type. Try and load the config file for this specific type.
            try {
                $configfile = 'forms' . DIRECTORY_SEPARATOR . 'product'
                    . DIRECTORY_SEPARATOR . 'product_form_product-'.$type.'.ini';
                $options = Mcmr_Config_Ini::getInstance($configfile);
                $options = $options->toArray();
            } catch (Exception $e) {
                Mcmr_Debug::dump("Cannot load form for type '{$type}' ".$e->getMessage());
            }
        }

        $this->_formType = $type;

        parent::__construct($options);
    }

    public function init()
    {
        $this->setName('productformproduct')->setElementsBelongTo('product-form-product');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }

    public function postInit()
    {
        $this->addElement(
            'Submit', 'submit', array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );
    }

    public function getType()
    {
        return $this->_formType;
    }

}
