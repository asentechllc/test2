<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Download.php 1238 2010-08-17 12:58:36Z leigh $
 */

/**
 * Form class for Categories
 *
 * @category Product
 * @package Product_Form
 * @subpackage Download
 */
class Product_Form_Download extends Mcmr_Form
{
    public function init()
    {
        $this->setName('productformdownload')->setElementsBelongTo('product-form-download');
        $this->setMethod('post');
        // Add some CSRF protection
        /*
        $this->addElement('hash', 'csrf', array('salt' => 'unique'));
        */
        $this->addElement(
            'Submit', 'submit', array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );
    }
    
    public function postInit()
    {
        $productidElement = $this->getElement('productid');
        if (null !== $productidElement) {
            $front = Zend_Controller_Front::getInstance();
            $request = $front->getRequest();
            $productId = $request->getParam('productid', null);
            $productidElement->setValue($productId);
        }
    }
}
