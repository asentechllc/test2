<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Product
 * @package    Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Order.php 2196 2011-02-15 13:03:05Z michal $
 */

/**
 * A form for changing the display order of products
 *
 * @category   Product
 * @package    Form
 * @subpackage Order
 */
class Product_Form_Order extends Mcmr_Form
{
    private $_products = array();
    private $_type = null;
    private $_models = null;

    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR
                . 'product_form_order-'.strtolower($type).'.ini';
            $this->_type = $type;
        }

        parent::__construct($options);
    }

    public function postInit()
    {
        $this->setName('productformorder')->setElementsBelongTo('product-form-order');
        $this->setMethod('post');


        // Get the type this is limited to and set conditions accordingly.
        $time = isset($this->_products['fromTime'])?$this->_products['fromTime']:'-1 month';
        $conditions = array();
        if (isset($this->_products['type'])) {
            $mapper = Product_Model_Type::getMapper();
            $type =  $mapper->findOneByField(array('url'=>$this->_products['type']));

            if (null !== $type) {
                $conditions = array(
                    'typeid'=>$type->getId()
                );
            }
        }
        $order = array('title' => 'asc' );
        if (null !== $this->_type) {
            $order = array('ordr_'.$this->_type=>'desc', 'title' => 'asc' );
        }
        // Get the products.
        $mapper = Product_Model_Product::getMapper();
        $page['page'] = 1;
        $page['count'] = isset($this->_products['count'])?$this->_products['count']:10;
        $optionCount = isset($this->_articles['optionCount'])?$this->_articles['optionCount']:100;
        
        $products = $mapper->findAllByField($conditions, $order, $page);
        $this->_models = $products;
        $subform = new Mcmr_Form();
        $subform->setElementsBelongTo('products');

        $x = 1;
        $displayGroupElements = array();
        $typeValue = 'chronological';
        foreach ($products as $product) {
            $productOrder = $product->getOrder($this->_type);
            if ((null !== $productOrder)&&(0 !== $productOrder)) {
                $typeValue = 'specific';
            }

            $value = ((null !== $productOrder) && (0 !== $productOrder))?$product->getId():'';
            $element = array(
                'options' => array(
                    'label' => 'Product ' . ($x),
                    'required' => true,
                    'value' => $value,
                    'class' => 'sortable product_search'
                ),
                'optionSource' => array(
                    'mapper' => 'Product_Model_Product',
                    'conditions' => $conditions,
                    'order' => array('title' => 'asc' ),
                    'page' => array('page' => '1', 'count' => $optionCount),
                    'valueField' => 'id',
                    'allowNull'=>true,
                    'allowNullDisplay'=>'Please Select...'
                )
            );
            $element = $this->_processElement($element);
            $subform->addElement('Select', "$x", $element['options']);
            $displayGroupElements[] = "$x";
            $x++;
        }

        if (isset($displayGroupElements[0])) {
            $subform->addDisplayGroup($displayGroupElements, 'products', array('class'=>'sortable-items'));
        }

        $this->addSubForm($subform, 'products');
        // Set the value of the 'orderby' field
        $orderby = $this->getElement('orderby');
        if (null !== $orderby) {
            $orderby->setValue($typeValue);
        }
    }

    public function setProducts($products)
    {
        $this->_products = $products;
    }

    public function getProducts()
    {
        return $this->_products;
    }
    
    public function getProductModels()
    {
        return $this->_models;
    }

}
