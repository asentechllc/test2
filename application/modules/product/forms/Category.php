<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Category.php 1238 2010-08-17 12:58:36Z leigh $
 */

/**
 * Form class for Categories
 *
 * @category Product
 * @package Product_Form
 * @subpackage Category
 */
class Product_Form_Category extends Mcmr_Form
{
    public function init()
    {
        $this->setName('productformcategory')->setElementsBelongTo('product-form-category');
        $this->setMethod('post');
    }
    
    public function postInit()
    {
        $this->addElement(
            'Submit', 'submit', array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );

        // Add some CSRF protection
        $this->addElement('hash', 'csrf', array('salt' => 'unique'));
    }
}
