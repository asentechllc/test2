<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 1238 2010-08-17 12:58:36Z leigh $
 */

/**
 * Form class for Types
 *
 * @category Product
 * @package Product_Form
 * @subpackage Type
 */
class Product_Form_Type extends Mcmr_Form
{
    public function init()
    {
        $this->setName('productformtype')->setElementsBelongTo('product-form-type');
        $this->setMethod('post');

        $this->addElement(
            'Text', 'title', array(
                'label' => 'Title',
                'required' => true,
                'filters'    => array('StringTrim'),
                'invalidMessage' => 'Title must be specified',
            )
        );

        $this->addElement(
            'Submit', 'submit', array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );

        // Add some CSRF protection
        $this->addElement(
            'hash', 'csrf', array(
                'salt' => 'unique'
            )
        );
    }
}
