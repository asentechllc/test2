<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ProductAttr.php 718 2010-06-01 13:33:37Z leigh $
 */

/**
 * Description of ProductAttr
 *
 * @category Product
 * @package Product_Model
 * @subpackage DbTable
 */
class Product_Model_DbTable_ProductOrdr extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'product_products_ordr';
    protected $_primary = array('product_id', 'ordr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insert($this->_rowData);
        } else {
            parent::insert($data);
        }
    }

    public function setOrdrName($name)
    {
        $this->_rowData['ordr_name'] = $name;

        return $this;
    }

    public function setOrdrValue($value)
    {
        $this->_rowData['ordr_value'] = (int)$value;

        return $this;
    }

    public function setOrdrId($id)
    {
        $this->_rowData['product_id'] = $id;

        return $this;
    }
}
