<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Product.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * DB Table for the Product Model
 *
 * @category Product
 * @package Product_Model
 * @subpackage DbTable
 */
class Product_Model_DbTable_Product extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'product_products';
    protected $_primary = 'product_id';
}
