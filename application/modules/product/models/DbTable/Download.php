<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * DB Table for the Product download statistics
 *
 * @category Product
 * @package Product_Model
 * @subpackage DbTable
 */
class Product_Model_DbTable_Download extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'product_downloads';
    protected $_primary = 'download_id';
}
