<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CategoryMapper.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * Mapper class for all Category models
 *
 * @category Product
 * @package Product_Model
 * @subpackage CategoryMapper
 */
class Product_Model_CategoryMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Product_Model_DbTable_Category';
    protected $_columnPrefix = 'category_';
    protected $_modelClass = 'Product_Model_Category';
    protected $_cacheIdPrefix = 'ProductCat';

    public function  __construct()
    {
        parent::__construct();

        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
    }

    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'type':
            case 'typeid':
            case 'type_id':
                $field = 'type_id';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }
}
