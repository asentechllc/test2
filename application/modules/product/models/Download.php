<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Product.php 2398 2011-05-05 10:13:07Z leigh $
 */

/**
 * Model class for all Download records
 *
 * @category Product
 * @package Product_Model
 * @subpackage Download
 */
class Product_Model_Download extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Product_Model_DownloadMapper';

    /**
     * Product ID
     * @var int
     */
    protected $_id = null;
    protected $_productid = null;
    protected $_userid = null;
    protected $_date = null;
    protected $_email = null;
    protected $_url = null;

    private $_user = null;
    private $_product = null;
    
    /**
     * Return mapper for model
     *
     * @return Product_Model_ProductMapper
     */
    static function getMapper()
    {
        return parent::getMapper(self::$_mapperclass);
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Product_Model_Product
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }
    
    /**
     *
     * @return int 
     */
    public function getProductid() 
    {
        return $this->_productid;
    }

    /**
     *
     * @param int $productid
     * @return Product_Model_Download 
     */
    public function setProductid($productid) 
    {
        $this->_productid = (int)$productid;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getUserid() 
    {
        if (null === $this->_userid) {
            $user = Zend_Registry::get('authenticated-user');
            $this->_userid = $user->getId();
        }
        
        return $this->_userid;
    }

    /**
     *
     * @param int $userid
     * @return Product_Model_Download 
     */
    public function setUserid($userid) 
    {
        $this->_userid = (int)$userid;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getUrl() 
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::uniqueToken();
        }
        
        return $this->_url;
    }

    /**
     *
     * @param int $userid
     * @return Product_Model_Download 
     */
    public function setUrl($userid) 
    {
        $this->_url = (int)$userid;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getDate() 
    {
        if (null === $this->_date) {
            $this->_date = time();
        }
        
        return $this->_date;
    }

    /**
     *
     * @param int $date
     * @return Product_Model_Download 
     */
    public function setDate($date) 
    {
        $this->_date = (int)$date;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getEmail() 
    {
        if (null === $this->_email) {
            $this->_email = $this->getUser()->getEmail();
        }
        return $this->_email;
    }

    /**
     *
     * @param string $email
     * @return Product_Model_Download 
     */
    public function setEmail($email) 
    {
        $this->_email = $email;
        
        return $this;
    }

    /**
     *
     * @return User_Model_User 
     */
    public function getUser()
    {
        if (null===$this->_user) {
            $mapper = User_Model_User::getMapper();
            $this->_user = $mapper->find($this->getUserid());
        }
        return $this->_user;
    }
    
    /**
     *
     * @return Product_Model_Product 
     */
    public function getProduct()
    {

        if (null===$this->_product) {
            $mapper = Product_Model_Product::getMapper();
            $this->_product = $mapper->find($this->getProductid());
        }
        return $this->_product;
    }
    
}
