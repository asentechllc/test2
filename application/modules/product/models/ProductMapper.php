<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ProductMapper.php 2329 2011-04-05 10:27:23Z michal $
 */

/**
 * Mapper class for all Product models
 *
 * @category Product
 * @package Product_Model
 * @subpackage ProductMapper
 */
class Product_Model_ProductMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Product_Model_DbTable_Product';
    protected $_columnPrefix = 'product_';
    protected $_modelClass = 'Product_Model_Product';
    protected $_cacheIdPrefix = 'Product';
    protected $_dbAttrTableClass = 'Product_Model_DbTable_ProductAttr';
    protected $_dbOrdrTableClass = 'Product_Model_DbTable_ProductOrdr';

    public function  __construct()
    {
        parent::__construct();

        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
    }

    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        // Check the product URL
        if (null === ($id = $model->getId())) {
            $x = 2;
            while ($this->_urlExists($model->getUrl())) {
                $model->setUrl($model->getTitle() . ' ' . $x);
                $x++;
            }
        } else {
            // If the URL has changed we don't want any duplicates
            if ($model->urlChanged() && $this->_urlExists($model->getUrl())) {
                $x = 2;
                while ($this->_urlExists($model->getUrl())) {
                    $model->setUrl($model->getTitle() . ' ' . $x);
                    $x++;
                }
            }
        }
        
        parent::save($model);

        $this->_saveCategories($model);
        $this->_saveRelated($model);
        $this->_saveAmounts($model);

        return $this;
    }

    /**
     * Delete the model information from the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        $this->_deleteCategories($model);
        $this->_deleteRelated($model);

        return parent::delete($model);
    }

    protected function _convertRowToModel($row)
    {
        $model = parent::_convertRowToModel($row);

        if (null !== $model) {
            $this->loadCategories($model);
            $this->loadRelated($model);
            $this->loadAmounts($model);
        }

        return $model;
    }

    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'user_role':
            case 'userrole':
            case 'role':
                $field = 'user_role';
                break;
            
            case 'type':
            case 'typeid':
            case 'type_id':
                $field = 'type_id';
                break;

            case 'categoryids':
            case 'category':
                $field = 'category_id';
                break;

            case 'relatedids':
            case 'related':
                $field = 'related_id';
                break;
            
            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }

    protected function _sanitiseValues($data)
    {
        unset($data['category_id']);
        unset($data['related_id']);
        unset($data['product_amounts']);
        return $data;
    }

    protected function _sanitiseValue( $field, $value ) 
    {
        switch ( $field ) {
        case 'product_createdate':
            return new Zend_Db_Expr("FROM_UNIXTIME({$value})");
        default:
            return $value;
        }
    } //_sanitiseValue

    protected function _selectFields()
    {
        $all = parent::_selectFields();
        $all[] = "UNIX_TIMESTAMP(product_createdate) AS product_createdate";
        return $all;
    } //_selectFields

    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        switch ($field) {
            case 'category':
            case 'category_id':
            case 'categoryid':
                $select->setIntegrityCheck(false);
                $select->join(
                    array('product_productcategories'=>'product_productcategories'),
                    "product_products.product_id = product_productcategories.product_id"
                );
                $select->where('product_productcategories.category_id = ?', $value);

                return $select;
                break;

            case 'relatedid':
            case 'related':
                $select->setIntegrityCheck(false);
                $select->join(
                    array('product_relatedproducts'=>'product_relatedproducts'),
                    "product_products.product_id = product_relatedproducts.product_id"
                );
                $select->where('product_relatedproducts.product_relatedid = ?', $value);

                return $select;
                break;

            default:
                return parent::_addCondition($select, $field, $value);
                break;
        }
    }

    /**
     * Save the product categories
     *
     * @param Product_Model_Product $model
     * @return Product_Model_ProductMapper
     */
    private function _saveCategories(Product_Model_Product $model)
    {
        $this->_deleteCategories($model);
        $table = new Mcmr_Db_Table('product_productcategories');
        $data['product_id'] = $model->getId();

        foreach ($model->getCategoryids() as $category) {
            $data['category_id'] = $category;
            $table->insert($data);
        }

        return $this;
    }

    /**
     * Delete the product categories
     *
     * @param array $condition
     * @return Product_Model_ProductMapper
     */
    private function _deleteCategories($model)
    {
        $condition = array('product_id=?'=>$model->getId());
        $table = new Mcmr_Db_Table('product_productcategories');
        $table->delete($condition);

        return $this;
    }

    /**
     * Load the product categories
     *
     * @param Product_Model_Product $model
     * @return Product_Model_ProductMapper
     */
    public function loadCategories( $model)
    {
        $table = new Mcmr_Db_Table('product_productcategories');
        $rows = $table->fetchAll(array('product_id=?'=>$model->getId()));

        $categories = array();
        foreach ($rows as $row) {
            $categories[] = $row['category_id'];
        }

        $model->setCategoryids($categories);

        return $this;
    }

    /**
     * Save the related products
     *
     * @param Product_Model_Product $model
     * @return Product_Model_ProductMapper
     */
    private function _saveRelated(Product_Model_Product $model)
    {
        $this->_deleteRelated($model);
        $table = new Mcmr_Db_Table('product_relatedproducts');
        $data['product_id'] = $model->getId();

        foreach ($model->getRelatedids() as $relatedid) {
            $data['product_relatedid'] = $relatedid;
            $table->insert($data);
        }

        return $this;
    }

    /**
     * Delete the related products
     *
     * @param array $condition
     * @return Product_Model_ProductMapper
     */
    private function _deleteRelated($model)
    {
        $table = new Mcmr_Db_Table('product_relatedproducts');
        $table->delete(array('product_id=?'=>$model->getId()));
        $table->delete(array('product_relatedid=?'=>$model->getId()));

        return $this;
    }

    /**
     * Load the related products
     *
     * @param Product_Model_Product $model
     * @return Product_Model_ProductMapper
     */
    public function loadRelated(Product_Model_Product $model)
    {
        $table = new Mcmr_Db_Table('product_relatedproducts');
        $rows = $table->fetchAll(array('product_id=?'=>$model->getId()));

        $relatedids = array();
        foreach ($rows as $row) {
            $relatedids[] = $row['product_relatedid'];
        }

        $model->setRelatedids($relatedids);
        
        return $this;
    }

    private function _saveAmounts(Product_Model_Product $model)
    {
        $this->_deleteAmounts($model);
        $table = new Mcmr_Db_Table('product_amounts');
        $data['product_id'] = $model->getId();

        foreach ($model->getAmounts() as $name=>$amount) {
            $data['amount_name'] = $name;
            $data['amount_value'] = $amount;
            $table->insert($data);
        }

        return $this;
        
    }

    private function _deleteAmounts(Product_Model_Product $model)
    {
        $condition = array('product_id=?'=>$model->getId());
        $table = new Mcmr_Db_Table('product_amounts');
        $table->delete($condition);

        return $this;
    }

    public function loadAmounts(Product_Model_Product $model)
    {
        $table = new Mcmr_Db_Table('product_amounts');
        $rows = $table->fetchAll(array('product_id=?'=>$model->getId()));

        $amounts = array();
        foreach ($rows as $row) {
            $amounts[$row['amount_name']] = $row['amount_value'];
        }

        $model->setAmounts($amounts);

        return $this;
    }

    public function recordDownload(Product_Model_Product $model)
    {
        $user = Zend_Registry::get('authenticated-user');
        
        $table = new Product_Model_DbTable_Download();
        $data = array(
            'product_id' => $model->getId(),
            'user_id' => $user->getId(),
            'download_date' => new Zend_Db_Expr("FROM_UNIXTIME(".time().")"),
        );
        $table->insert($data);
        
        return $this;
    }
    
    public function countDownloads(Product_Model_Product $model)
    {
        $table = new Product_Model_DbTable_Download();
        $select = $table->select();
        $select->from($table, array('downloads' => 'count(*)'));
        $select->where('product_id = ?', $model->getId());
        $stmt = $select->query();
        $res = $stmt->fetch();
        
        return $res['downloads'];
    }
    
    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($url)
    {
        return (null !== $this->findOneByField(array('url'=> $url)));
    }

}
