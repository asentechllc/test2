<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: TypeMapper.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * Mapper class for all Type models
 *
 * @category Product
 * @package Product_Model
 * @subpackage TypeMapper
 */
class Product_Model_TypeMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Product_Model_DbTable_Type';
    protected $_columnPrefix = 'type_';
    protected $_modelClass = 'Product_Model_Type';
    protected $_cacheIdPrefix = 'ProductType';

    public function  __construct()
    {
        parent::__construct();

        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
    }
}