<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Product.php 2398 2011-05-05 10:13:07Z leigh $
 */

/**
 * Model class for all Products
 *
 * @category Product
 * @package Product_Model
 * @subpackage Product
 */
class Product_Model_Product extends Mcmr_Model_Ordered implements Payment_Model_ProductInterface
{
    static protected $_mapperclass = 'Product_Model_ProductMapper';

    /**
     * Product ID
     * @var int
     */
    protected $_id = null;

    /**
     * The minimum role to view this content. Requires use of the 'Gated' ACLs
     * 
     * @var string
     */
    protected $_userrole = null;
    
    /**
     * Product type ID
     *
     * @var int
     */
    protected $_typeid = null;

    /**
     * Product Title
     * @var string
     */
    protected $_title = null;

    /**
     * URL string
     * @var string
     */
    protected $_url = null;

    /**
     * Product Description
     * @var string
     */
    protected $_description = null;

    /**
     * Product Image file name
     * @var string
     */
    protected $_image = null;

    /**
     * Image ALT text
     * @var string
     */
    protected $_imagealt = null;

    /**
     * Product featured flag
     * @var boolean
     */
    protected $_featured = null;

    /**
     * An array of categories
     *
     * @var array
     */
    protected $_categoryids = null;

    /**
     * An array of related product ids
     *
     * @var array
     */
    protected $_relatedids = null;

    /**
     * The taxrate for this product
     *
     * @var float
     */
    protected $_taxrate = null;

    /**
     * Model classname if this product is a system model
     *
     * @var string
     */
    protected $_modeltype = null;

    /**
     * The model ID if this product is a system model
     *
     * @var int
     */
    protected $_modelid = null;

    protected $_download = null;

    protected $_amounts = null;

    protected $_redeem = null;

    protected $_createdate = null;

    /**
     * Product Type model object
     * @var Product_Model_Type
     */
    private $_type = null;

    /**
     * All the related products
     *
     * @var array
     */
    private $_related = null;

    /**
     * The categories this product is in
     *
     * @var array
     */
    private $_categories = null;

    /**
     * Flag to indicate the URL string has been changed
     *
     * @var bool
     */
    private $_urlchanged = false;

    /**
     * Return mapper for model
     *
     * @return Product_Model_ProductMapper
     */
    static function getMapper($mapperclass=null)
    {
        return parent::getMapper(self::$_mapperclass);
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Product_Model_Product
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the minimum role required to view this product.
     * Default 'guest'
     *
     * @return string 
     */
    public function getUserrole()
    {
        if (null === $this->_userrole) {
            $this->_userrole = 'guest';
        }
        
        return $this->_userrole;
    }

    /**
     * Set the minimum role required to view this product.
     *
     * @param string $role
     * @return Product_Model_Product 
     */
    public function setUserrole($role)
    {
        $this->_userrole = $role;
        
        return $this;
    }
    
    /**
     * Get the Type ID
     *
     * @return int
     */
    public function getTypeid()
    {
        return $this->_typeid;
    }

    /**
     * Set the Type ID
     *
     * @param int $typeid
     * @return Product_Model_Product
     */
    public function setTypeid($typeid)
    {
        $this->_typeid = $typeid;

        return $this;
    }

    /**
     * Get the Product Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the Product Title
     *
     * @param string $title
     * @return Product_Model_Product
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the URL string
     *
     * @return string
     */
    public function getUrl()
    {
        if ((null === $this->_url) && (null !== $this->_title)) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
            $this->_urlchanged = true;
        }

        return $this->_url;
    }

    /**
     * Set the URL string
     *
     * @param string $url
     * @return Product_Model_Product
     */
    public function setUrl($url)
    {
        $this->_url = Mcmr_StdLib::urlize($url);

        return $this;
    }

    /**
     * Get the product description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the product description
     *
     * @param string $description
     * @return Product_Model_Product
     */
    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }

    /**
     * Get the product Image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * Set the product Image
     *
     * @param strint $image
     * @return Product_Model_Product
     */
    public function setImage($image)
    {
        $this->_image = $image;

        return $this;
    }

    /**
     * Get the image ALT text
     *
     * @return string
     */
    public function getImagealt()
    {
        return $this->_imagealt;
    }

    /**
     * Set the image ALT text
     *
     * @param string $imagealt
     * @return Product_Model_Product
     */
    public function setImagealt($imagealt)
    {
        $this->_imagealt = $imagealt;

        return $this;
    }

    /**
     * Get product featured flag
     *
     * @return boolean
     */
    public function getFeatured()
    {
        if (null === $this->_featured) {
            $this->_featured = false;
        }

        return $this->_featured;
    }

    /**
     * Set product featured flag
     *
     * @param boolean $featured
     * @return Product_Model_Product
     */
    public function setFeatured($featured)
    {
        $this->_featured = (bool)$featured;

        return $this;
    }

    /**
     * Return an array of the product category IDs
     *
     * @return array
     */
    public function getCategoryids()
    {
        if (null === $this->_categoryids) {
            $this->_categoryids = array();
        }
        return $this->_categoryids;
    }

    /**
     * Set the category IDs
     *
     * @param array|int $categoryids
     * @return Product_Model_Product
     */
    public function setCategoryids($categoryids)
    {
        if (null === $categoryids) {
            $categoryids = array();
        } elseif (!is_array($categoryids)) {
            if (is_numeric($categoryids)) {
                $categoryids = array($categoryids);
            } else {
                $categoryids = null;
            }
        }

        $this->_categoryids = $categoryids;
        $this->_categories = null;

        return $this;
    }

    /**
     * Return an array of related product IDs
     *
     * @return array
     */
    public function getRelatedids()
    {
        if (null === $this->_relatedids) {
            $this->_relatedids = array();
        }

        return $this->_relatedids;
    }

    /**
     * Set the related product IDs
     *
     * @param array $relatedids
     * @return Product_Model_Product
     */
    public function setRelatedids($relatedids)
    {
        if (null === $relatedids) {
            $relatedids = array();
        }

        $this->_relatedids = $relatedids;

        return $this;
    }

    /**
     * Get the taxrate this product is sold at
     *
     * @return float
     */
    public function getTaxrate()
    {
        if (null === $this->_taxrate) {
            $config = Zend_Registry::get('product-config');
            if (isset($config->products) && isset($config->products->taxrate)) {
                $this->_taxrate = (float)$config->products->taxrate;
            } else {
                $this->_taxrate = 0;
            }
        }

        return (float)$this->_taxrate;
    }

    /**
     * Set the product's tax rate
     *
     * @param float $taxrate
     * @return Product_Model_Product
     */
    public function setTaxrate($taxrate)
    {
        $this->_taxrate = $taxrate;

        return $this;
    }

    /**
     * Get the products price in pence/cents.
     *
     * @return int
     */
    public function getAmount(User_Model_User $user=null, $incTax = true)
    {
        if (null === $user) {
            $user = Zend_Registry::get('authenticated-user');
        }

        $this->getAmounts(); // Initialise the amounts

        if (null !== $user) {
            $role = $user->getRole();
        } else {
            $role = 'default';
        }

        $amountIncTax = null;
        if (isset($this->_amounts[$role])) {
            $amountIncTax=$this->_amounts[$role];
        } elseif (isset($this->_amounts['default'])) {
            $amountIncTax=$this->_amounts['default'];
        } else {
            return null;
        }
        if ($incTax) {
            return $amountIncTax;
        } else {
            return round($amountIncTax / (1 + $this->getTaxrate()/100));
        }
    }

    /**
     * Set the product's price in pence/cents including tax
     *
     * @param int $amount
     * @return Product_Model_Product
     */
    public function setAmount($name, $amount)
    {
        $this->_amounts[$name] = $amount;

        return $this;
    }

    /**
     * Get the amounts for all the different user groups
     *
     * @return array
     */
    public function getAmounts()
    {
        if (null === $this->_amounts) {
            self::getMapper()->loadAmounts($this);
        }

        return $this->_amounts;
    }

    /**
     * Set the amounts for the different user groups
     *
     * @param array $amounts
     * @return Product_Model_Product
     */
    public function setAmounts($amounts)
    {
        $this->_amounts = $amounts;

        return $this;
    }

    /**
     * Get the number of times this product can be redeemed
     *
     * @return int
     */
    public function getRedeem()
    {
        if (null === $this->_redeem) {
            $this->_redeem = 0;
        }

        return $this->_redeem;
    }

    /**
     * Set the number of times this product can be redeemed
     *
     * @param int $redeem
     * @return Product_Model_Product
     */
    public function setRedeem($redeem)
    {
        $this->_redeem = (int)$redeem;

        return $this;
    }

    /**
     * The class name of the model that this product referes to.
     * Default to this class if not set
     *
     * @return string
     */
    public function getModeltype()
    {
        return $this->_modeltype;
    }

    /**
     * Set the model classname that this product refers to.
     *
     * @param string $modeltype
     * @return Product_Model_Product
     */
    public function setModeltype($modeltype)
    {
        $this->_modeltype = $modeltype;

        return $this;
    }

    /**
     * Get the model's ID
     * Default to this ID if not set.
     *
     * @return int
     */
    public function getModelid()
    {
        return $this->_modelid;
    }

    /**
     * Set the model's ID
     *
     * @param int $modelid
     * @return Product_Model_Product
     */
    public function setModelid($modelid)
    {
        $this->_modelid = $modelid;

        return $this;
    }

    /**
     * Get the download file
     *
     * @return string
     */
    public function getDownload()
    {
        return $this->_download;
    }

    /**
     * Set the download file
     *
     * @param string $download
     * @return Product_Model_Product
     */
    public function setDownload($download)
    {
        $this->_download = $download;

        return $this;
    }

    /**
     * Get an instance of the product model
     *
     * @return Mcmr_Model_ModelAbstract
     */
    public function getModel()
    {
        $className = $this->getModeltype();
        $mapper = call_user_func(array($className, 'getMapper'));
        
        return $mapper->find($this->getModelid());
    }

    public function getType()
    {
        if (null === $this->_type) {
            $mapper = Product_Model_Type::getMapper();
            $this->_type = $mapper->find($this->getTypeid());
        }

        return $this->_type;
    }

    public function getRelated()
    {
        if (null === $this->_related) {
            $this->_related = array();

            $ids = $this->getRelatedids();
            $mapper = Product_Model_Product::getMapper();
            foreach ($ids as $id) {
                $this->_related[] = $mapper->find($id);
            }
        }

        return $this->_related;
    }

    public function getCategories()
    {
        if (null === $this->_categories) {
            $this->_categories = array();
            $ids = $this->getCategoryids();
            $mapper = Product_Model_Category::getMapper();
            foreach ($ids as $id) {
                $this->_categories[] = $mapper->find($id);
            }
        }

        return $this->_categories;
    }


    /**
     * Get the product creation date as a timestamp
     *
     * @return int
     */
    public function getCreatedate()
    {
        if (null === $this->_createdate) {
            $this->_createdate = time();
        }

        return $this->_createdate;
    }

    /**
     * Set the creation date as a timestamp
     *
     * @param int $createdate
     * @return Product_Model_Product
     */
    public function setCreatedate($createdate)
    {
        $this->_createdate = intval($createdate);

        return $this;
    }

    /**
     * Record a download statistic.
     * 
     * @return Payment_Model_Item 
     */
    public function recordDownload()
    {
        $download = new Product_Model_Download();
        $download->setProductid($this->getId());
        
        Product_Model_Download::getMapper()->save($download);
        
        return $this;
    }
    
    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }
}
