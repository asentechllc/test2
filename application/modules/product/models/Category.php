<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Category.php 1104 2010-07-22 11:44:57Z leigh $
 */

/**
 * Model class for all Categorys
 *
 * @category Product
 * @package Product_Model
 * @subpackage Category
 */
class Product_Model_Category extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Product_Model_CategoryMapper';

    protected $_id = null;
    protected $_typeid = null;
    protected $_title = null;
    protected $_url = null;
    protected $_description = null;
    protected $_image = null;

    /**
     * Category Type model object
     * @var Product_Model_Type
     */
    private $_type = null;

    /**
     * Return mapper for model
     *
     * @return Product_Model_CategoryMapper
     */
    static function getMapper()
    {
        return parent::getMapper(self::$_mapperclass);
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Product_Model_Category
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the category type id
     *
     * @return int
     */
    public function getTypeid()
    {
        return $this->_typeid;
    }

    /**
     * Set the category type ID
     *
     * @param int $typeid
     * @return Product_Model_Category
     */
    public function setTypeid($typeid)
    {
        $this->_typeid = $typeid;

        return $this;
    }

    /**
     * Get the category title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the Category Title
     *
     * @param string $title
     * @return Product_Model_Category
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the URL string
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }

        return $this->_url;
    }

    /**
     * Set the URL string
     *
     * @param string $url
     * @return Product_Model_Category
     */
    public function setUrl($url)
    {
        $this->_url = Mcmr_StdLib::urlize($url);

        return $this;
    }

    /**
     * Get the category description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the category description
     *
     * @param string $description
     * @return Product_Model_Category
     */
    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }

    /**
     * Get the image filename
     *
     * @return string
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * Set the image filename
     *
     * @param string $image
     * @return Product_Model_Category
     */
    public function setImage($image)
    {
        $this->_image = $image;

        return $this;
    }

    /**
     * Get the type model of this category
     *
     * @return Product_Model_Type
     */
    public function getType()
    {
        if (null === $this->_type) {
            $mapper = Product_Model_Type::getMapper();
            $this->_type = $mapper->find($this->getTypeid());
        }

        return $this->_type;
    }
}
