<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 2306 2011-03-31 10:43:22Z leigh $
 */

/**
 * Model class for all Types
 *
 * @category Product
 * @package Product_Model
 * @subpackage Type
 */
class Product_Model_Type extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Product_Model_TypeMapper';

    protected $_id = null;
    protected $_title = null;
    protected $_url = null;
    protected $_description = null;

    /**
     * Return mapper for model
     *
     * @return Product_Model_TypeMapper
     */
    static function getMapper($mapperclass=null)
    {
        return parent::getMapper(self::$_mapperclass);
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Product_Model_Type
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the type title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the type title
     *
     * @param string $title
     * @return Product_Model_Type
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the URL string
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }

        return $this->_url;
    }

    /**
     * Set the URL string
     *
     * @param string $url
     * @return Product_Model_Product
     */
    public function setUrl($url)
    {
        $this->_url = Mcmr_StdLib::urlize($url);

        return $this;
    }

    /**
     * Get the type description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the type description
     *
     * @param string $description
     * @return Product_Model_Type
     */
    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }

    /**
     * Fetch the products for this product type
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @return Zend_Paginator
     */
    public function getProducts($conditions=null, $order=null, $page = null)
    {
        $mapper = Product_Model_Product::getMapper();

        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['typeid'] = $this->getId();

        // Order products, newest to oldest
        if (null === $order) {
            $order = array('createdate'=>'desc');
        }

        $products = $mapper->findAllByField($conditions, $order, $page);

        return $products;
    }

}
