<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Product
 * @package Product_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CategoryMapper.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * Mapper class for all Download models
 *
 * @category Product
 * @package Product_Model
 * @subpackage DownloadMapper
 */
class Product_Model_DownloadMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Product_Model_DbTable_Download';
    protected $_columnPrefix = 'download_';
    protected $_modelClass = 'Product_Model_Download';
    protected $_cacheIdPrefix = 'ProductDownload';

    public function  __construct()
    {
        parent::__construct();

        $this->registerObserver('Cache');
    }

    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;

            case 'product':
            case 'productid':
            case 'product_id':
                $field = 'product_id';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }
    
    protected function _sanitiseValue($field, $value) 
    {
        switch ( $field ) {
            case 'download_date':
                return new Zend_Db_Expr("FROM_UNIXTIME({$value})");
                break;
                
            default:
                return $value;
                break;
        }
    }
}
