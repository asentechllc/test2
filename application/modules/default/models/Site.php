<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Simpe model class for site.
 *
 * @category Default
 * @package Default_Model
 * @subpackage Stats
 */
class Default_Model_Site extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Default_Model_SiteMapper';

    protected $_name;
    protected $_displayname;
    protected $_url;
    protected $_countries;


    /**
     * Return mapper for model
     *
     * @return Default_Model_StatMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }

    
    public function getDisplayname()
    {
        return $this->_displayname;
    }

    public function setDisplayname($displayname)
    {
        $this->_displayname = $displayname;
        return $this;
    }

    public function getUrl()
    {
        return $this->_url;
    }

    public function setUrl($url)
    {
        $this->_url = $url;
        return $this;
    }

    public function getCountries()
    {
        return $this->_countries;
    }

    public function setCountries($countries)
    {
        $this->_countries = $countries;
        return $this;
    }

}
