<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Simpe model class for the registry.
 *
 * @category Default
 * @package Default_Model
 * @subpackage Registry
 */
class Default_Model_Registry extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Default_Model_RegistryMapper';

    /**
     *
     * @var int 
     */
    protected $_id = null;
    
    /**
     *
     * @var string 
     */
    protected $_module = null;
    
    /**
     *
     * @var string 
     */
    protected $_name = null;
    
    /**
     *
     * @var string 
     */
    protected $_value = null;

    /**
     * Return mapper for model
     *
     * @return Default_Model_StatMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     *
     * @return int 
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     *
     * @param int $id
     * @return Default_Model_Registry 
     */
    public function setId($id)
    {
        $this->_id = intval($id);
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getModule()
    {
        if (null === $this->_module) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $this->_module = $request->getModuleName();
            if (null === $this->_module) {
                $this->_module = 'default';
            }
        }
        
        return $this->_module;
    }

    /**
     *
     * @param string $module
     * @return Default_Model_Registry 
     */
    public function setModule($module)
    {
        $this->_module = $module;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     *
     * @param string $name
     * @return Default_Model_Registry 
     */
    public function setName($name)
    {
        $this->_name = $name;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     *
     * @param string $value
     * @return Default_Model_Registry 
     */
    public function setValue($value)
    {
        $this->_value = $value;
        
        return $this;
    }
}
