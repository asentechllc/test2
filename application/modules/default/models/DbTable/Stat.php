<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Article.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * DbTable class for statistics
 *
 * @category Default
 * @package Default_Model
 * @subpackage DbTable
 */
class Default_Model_DbTable_Stat extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'default_stats';
    protected $_primary = 'stat_id';
}
