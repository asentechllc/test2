<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Default
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for stats models
 *
 * @category   Default
 * @package    Model
 * @subpackage StatMapper
 */
class Default_Model_SiteMapper extends Mcmr_Model_MapperAbstract
{
    protected $_sites = null;

    /**
     * Sites cannot be saved. This function always throws an exception
     * 
     * @throws Mcmr_Model_Exception
     * @param Mcmr_Model_ModelAbstract $model 
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        throw new Mcmr_Model_Exception('Site models cannot be saved');
    }

    /**
     * Sites cannot be deleted. This function always throws an exception
     *
     * @throws Mcmr_Model_Exception
     * @param Mcmr_Model_ModelAbstract $model 
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        throw new Mcmr_Model_Exception('Site models cannot be deleted');
    }

    /**
     * For the moment only find all is supported. This function always throws an exception
     *
     * @throws Mcmr_Model_Exception
     * @see Mcmr_Model_MapperAbstract::findOneByField()
     */
    public function findOneByField($condition = null, $order=null, $page=1)
    {
        $sites = $this->_getSites();
        foreach ($sites as $site ) {
            if (isset($condition['name']) && $condition['name']==$site->getName() ) {
                return $site;
            }
            if (isset($condition['displayname']) && $condition['displayname']==$site->getDisplayname() ) {
                return $site;
            }
        }
    }

    public function findOneByCountry($country) {
        $sites = $this->_getSites();
        foreach($sites as $site) {
            $countries = $site->getCountries();
            if (null!==$countries && in_array($country, $countries)) {
                return $site;
            }
        }
        return null;
    }

    public function findOneByName($name) {
        $sites = $this->_getSites();
        foreach($sites as $site) {
            if ($site->getName()==$name) {
                return $site;
            }
        }
        return null;
    }

    /**
     * @see Mcmr_Model_MapperAbstract::findAllByField()
     */
    public function findAllByField($condition = null, $order = null, $page = 1)
    {
        $paginator = new Mcmr_Paginator(new Zend_Paginator_Adapter_Array($this->_getSites()));
        return $paginator;
    }

    protected function _getSites() {
        if (null===$this->_sites) {
            $config = Zend_Registry::get('default-config');
            $this->_sites = array();
            if ( null !== $config->sites ) {
                $withAll = isset($condition['withAll']) && $condition['withAll'];
                if ( $withAll ) {
                    $this->_sites[] = new Default_Model_Site(array('name' => '*', 'displayname' => 'All'));
                }
                $sites = $config->sites->toArray();
                foreach ( $sites as $key => $value ) {
                    if (is_string($value)) {
                        $this->_sites[] = new Default_Model_Site(array('name' => $key, 'displayname' => $value));
                    } else {
                        $this->_sites[] = new Default_Model_Site($value);
                    }
                }
            } else {
                $this->_sites[] = new Default_Model_Site(array('name' => 'default', 'displayname' => 'Default'));
            }
        }
        return $this->_sites;
    }
    

}
