<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Default
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for stats models
 *
 * @category   Default
 * @package    Model
 * @subpackage StatMapper
 */
class Default_Model_StatMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Default_Model_DbTable_Stat';
    protected $_columnPrefix = 'stat_';
    protected $_modelClass = 'Default_Model_Stat';
    protected $_cacheIdPrefix = 'Stat';

    public function getCountRange($start, $end)
    {
        if ($start > $end) {
            throw new Mcmr_Model_Exception('Start date cannot be later than the end date');
        }

        
    }

    /**
     * @see Mcmr_Model_GenericMapper::findOneByField()
     */
    public function findOneByField($condition = null)
    {
        $model = parent::findOneByField($condition);
        
        if (null === $model) {
            $model = new Default_Model_Stat();
            $model->setOptions($condition);
        }

        return $model;
    }

    /**
     * Delete all stats associated with the model
     *
     * @param Mcmr_Model_MapperAbstract $model
     */
    public function deleteAll($model)
    {
        // Stats do not support multi-key models
        if (!is_array($model->getId())) {
            $this->getDbTable()->delete(
                array(
                    $this->_columnPrefix.'modeltype=?'=> get_class($model),
                    $this->_columnPrefix.'modelid=?'=>$model->getId()
                )
            );
        }

        return $this;
    }

    /**
     * Increment the stat by the count.
     *
     * @param Default_Model_Stat $model
     * @param int $count
     * @return Default_Model_StatMapper 
     */
    public function increment(Default_Model_Stat $model, $count=1)
    {
        $count = intval($count);
        
        if (Mcmr_Model_ModelAbstract::STATE_INITIALISED === $model->state()
                || Mcmr_Model_ModelAbstract::STATE_VIRGIN === $model->state()) {
            $model->increment($count);
            $this->save($model);
        } else {
            $data = array(
                'stat_count' => new Zend_Db_Expr('stat_count=stat_count + '.$count),
            );
            
            $key = $this->_keyCondition($this->getDbTable(), $model->getId());
            $this->getDbTable()->update($data, $key);
        }
        
        return $this;
    }
    
    /**
     * Convert field value before inserting into databse query.
     *
     * @param string $field field name passed to be able to tell which filed you are dealing with.
     * @param mixed $value field value as stored in  the object. Process if necessary and return.
     * @return Zend_Db_Expr|string|int field value in the format suitable for a database query.
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'date':
            case 'stat_date':
                $value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
                break;
        }

        return $value;
    }

    /**
     * Create a list of fields to be read form the databse in SELECT query.
     *
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields($select=null)
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(stat_date) AS stat_date";
        $fields[] = "SUM(stat_count) as stat_sum";

        $select->group('stat_modelid');

        return $fields;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_addCondition()
     */
    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        switch ($field) {
            case 'stat_daterange':
                $select->where(
                    "stat_date BETWEEN DATE(FROM_UNIXTIME({$value[0]})) AND DATE(FROM_UNIXTIME({$value[1]}))"
                );
                break;

            default:
                parent::_addCondition($select, $field, $value);

        }

        return $select;
    }
}
