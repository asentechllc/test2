<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for access stats.
 * This class will record and return any system model access stats that are recorded
 * against it
 *
 * @category Default
 * @package Default_Model
 * @subpackage Stats
 */
class Default_Model_Stat extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Default_Model_StatMapper';

    protected $_id = null;
    protected $_date = null;
    protected $_site = null;
    protected $_modeltype = null;
    protected $_modelid = null;
    protected $_action = null;
    protected $_count = null;

    /**
     * Return mapper for model
     *
     * @return Default_Model_StatMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }
    
    /**
     * Get the model unique ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the model unique id
     *
     * @param int $id
     * @return Default_Model_Stats
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the date of these statistics
     *
     * @return int
     */
    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = time();
        }

        return $this->_date;
    }

    /**
     * Set the date of these statistics
     *
     * @param int $date
     * @return Default_Model_Stats
     */
    public function setDate($date)
    {
        $this->_date = $date;

        return $this;
    }

    /**
     * Get the site this stat is recording against
     *
     * @return string
     */
    public function getSite()
    {
        if (null === $this->_site) {
            $this->_site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
        }
        
        return $this->_site;
    }

    public function setSite($site)
    {
        $this->_site = $site;
        
        return $this;
    }
        
    /**
     * Get the model class that these statistics relate to
     *
     * @return string
     */
    public function getModeltype()
    {
        return $this->_modeltype;
    }

    /**
     * Set the model classname
     *
     * @param string $modelType
     * @return Default_Model_Stats
     */
    public function setModeltype($modelType)
    {
        $this->_modeltype = $modelType;

        return $this;
    }

    /**
     * Get the ID of the model these statistics refer to
     *
     * @return int
     */
    public function getModelid()
    {
        return $this->_modelid;
    }

    /**
     * Set the ID of the model these statistics refer to.
     *
     * @param int $modelId
     * @return Default_Model_Stats
     */
    public function setModelid($modelId)
    {
        $this->_modelid = $modelId;

        return $this;
    }

    /**
     * Get the action for this statistic
     *
     * @return string
     */
    public function getAction()
    {
        if (null === $this->_action) {
            $this->_action = 'read';
        }

        return $this->_action;
    }

    /**
     * Set the action string
     *
     * @param string $action
     * @return Default_Model_Stat
     */
    public function setAction($action)
    {
        $this->_action = $action;

        return $this;
    }


    /**
     * Get the statistics count
     *
     * @return int
     */
    public function getCount()
    {
        if (null === $this->_count) {
            $this->_count = 0;
        }

        return $this->_count;
    }

    /**
     * Set the stats count
     *
     * @param int $count
     * @return Default_Model_Stats 
     */
    public function setCount($count)
    {
        $this->_count = (int)$count;

        return $this;
    }

    /**
     * Increment the access count
     *
     * @return Default_Model_Stat
     */
    public function increment($by=1)
    {
        $count = $this->getCount();
        $count+=$by;
        $this->setCount($count);

        return $this;
    }

    /**
     * Decrement these stats
     *
     * @return Default_Model_Stat 
     */
    public function decrement($by=1)
    {
        $count = $this->getCount();
        $count-=$by;
        if ($count<=0) {
            $count=0;
        }
        $this->setCount($count);

        return $this;
    }


    /**
     * Get the total count between two dates
     *
     * @param int $start
     * @param int $end
     * @return int
     */
    public function getCountRange($start, $end)
    {
        return 0;
    }
}
