<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Default
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for registry models
 *
 * @category   Default
 * @package    Model
 * @subpackage RegistryMapper
 */
class Default_Model_RegistryMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Default_Model_DbTable_Registry';
    protected $_columnPrefix = 'registry_';
    protected $_modelClass = 'Default_Model_Registry';
    protected $_cacheIdPrefix = 'DefaultRegistry';

    
    public function fetch($name, $module=null)
    {
        if (null === $module) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $module = $request->getModuleName();
        }
        
        $mapper = Default_Model_Registry::getMapper();
        
        $item = $mapper->findOneByField(array('name'=>$name, 'module'=>$module));
        
        if (null === $item) {
            $item = new Default_Model_Registry();
            $item->setModule($module)
                    ->setName($name);
            
        }
        
        return $item;
    }
}
