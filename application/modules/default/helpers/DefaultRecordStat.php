<?php

class Default_View_Helper_DefaultRecordStat extends Zend_View_Helper_Abstract
{
    public function defaultRecordStat(Mcmr_Model_ModelAbstract $model, $stat, $site=null)
    {
        if (null === $site) {
            $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
        }
        $mapper = Default_Model_Stat::getMapper();
        $stat = $mapper->findOneByField(
            array(
                'site'=>$site,
                'modeltype'=>get_class($model),
                'modelid'=>$model->getId(),
                'date'=>  strtotime('today'),
                'action'=>$stat
            )
        );
        $stat->increment();
        $mapper->save($stat);
    }
}
