<?php

class Default_View_Helper_DefaultBitLy extends Zend_View_Helper_Abstract
{
	protected $_bitLyService = null;

    public function defaultBitLy($model)
    {
    	if (null===($short=$model->getAttribute('bitLyUrl'))) {
    		$url = $this->view->defaultReadUrl($model, true);
			$bitLy = $this->_getBitLyService();
			$short = $bitLy->shorten($url); 
			$model->setAttribute('bitLyUrl', $short);
			$model->getMapper()->save($model);
    	}
    	return $short;
    }

    protected function _getBitLyService() {
    	if (null==$this->_bitLyService) {
    		$config = Zend_Registry::get('default-config');
    		$accessToken = $config->shorturl->bitly->accessToken;
			$this->_bitLyService = new Mcmr_Service_ShortUrl_BitLy($accessToken);
    	}
    	return $this->_bitLyService;
    }
}
