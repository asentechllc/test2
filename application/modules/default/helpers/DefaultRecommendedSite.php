<?php

class Default_View_Helper_DefaultRecommendedSite extends Zend_View_Helper_Abstract
{

    protected $_cookieKey='geoRedirectDisable';
	protected $_hostipService=null;
	protected $_siteMapper=null;
    protected $_request=null;
    protected $_namespace=null;
    protected $_config=null;

    public function defaultRecommendedSite() 
    {
        return $this;
    }

    public function getRecommendedSite() 
    {
        $request = $this->getRequest();
        $user = Zend_Registry::get('authenticated-user');
        //never suggest a redirection to a logged in user
        if (null!==$user && 'guest'!==$user->getRole())
        {
            return null;
        }
        if ($this->isDisabled())
        {
            return null;
        }
        $remoteAddr = $request->getClientIp(true);
        $remoteInfo = $this->getAddrInfo($remoteAddr);
        $remoteCountry = $remoteInfo['country'];
        if (null!==$remoteCountry) {
            return $this->getSiteByCountry($remoteCountry);;
        }
        return null;
    }

    public function getCurrentSite() 
    {
        $request = $this->getRequest();
        $siteName = $request->getSiteName();
        return $this->getSiteByName($siteName);
    }

    public function disable() 
    {
        $expiry = time() + $this->getConfig()->cookieLifetime;
        setcookie( $this->getCookieKey(), true, $expiry);
    }

    public function isDisabled() 
    {
        $request = $this->getRequest();
        return $request->getCookie($this->getCookieKey());
    }

    protected function getCookieKey()
    {
        return $this->_cookieKey;
    }

    protected function getAddrInfo($ip) 
    {
    	if (null==$this->_hostipService) {
            $config = $this->getConfig()->hostip;
            $this->_hostipService = new Mcmr_Service_Hostip($config);
    	}
    	return $this->_hostipService->getAddrInfo($ip);
    }

    protected function getSiteByCountry($country)
    {
    	return $this->getSiteMapper()->findOneByCountry($country);
    }

    protected function getSiteByName($name)
    {
        return $this->getSiteMapper()->findOneByName($name);
    }

    protected function getSiteMapper() 
    {
        if (null===$this->_siteMapper) {
            $this->_siteMapper = Default_Model_Site::getMapper();
        }
        return $this->_siteMapper;

    }

    protected function getConfig() 
    {
        if (null===$this->_config){
            $this->_config = Zend_Registry::get('default-config')->georedirect;
        }
        return $this->_config;
    }

    protected function getNamespace() 
    {
        if (null===$this->_namespace){
            $this->_namespace = new Zend_Session_Namespace('RecommendedSite'); 
        } 
    }


    protected function getRequest() 
    {
        if (null===$this->_request) {
            $this->_request = Zend_Controller_Front::getInstance()->getRequest();
        } 
        return $this->_request;

    }

}
