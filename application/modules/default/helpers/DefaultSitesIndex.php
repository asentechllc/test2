<?php

class Default_View_Helper_DefaultSitesIndex extends Zend_View_Helper_Abstract
{
    public function defaultSitesIndex( $condition=array() )
    {
        $sites = Default_Model_Site::getMapper()->findAllByField( $condition );
        return $sites;
    }
}
