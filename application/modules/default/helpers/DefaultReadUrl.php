<?php

class Default_View_Helper_DefaultReadUrl extends Zend_View_Helper_Abstract
{

	/**
	 * @param $absolute false: (default) relative, true: absolute to current server url, string: absolute to the domain specified by string
	 */
    public function defaultReadUrl($model, $absolute=false)
    {
    	$canonical = Mcmr_Model::getCanonicalReadUrl($model);
    	if (false===$absolute) {
    		return $canonical;
    	} elseif (true===$absolute) {
    		return $this->view->serverUrl($canonical);
    	} else  {
    		return $abolute.$canonical;
    	}
    }
}
