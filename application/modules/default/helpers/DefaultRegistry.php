<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of DefaultRegistry
 *
 * @category Mcmr
 */
class Default_View_Helper_DefaultRegistry extends Zend_View_Helper_Abstract
{
    
    public function defaultRegistry($name=null, $module=null)
    {
        // Fluent interface if there is no supplied name and module.
        if (null === $name && null === $module) {
            return $this;
        } else {
            return $this->fetch($name, $module);
        }
    }
    
    public function fetch($name, $module=null)
    {
        if (null === $module) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $module = $request->getModuleName();
        }
        
        $mapper = Default_Model_Registry::getMapper();
        
        $item = $mapper->findOneByField(array('name'=>$name, 'module'=>$module));
        
        if (null === $item) {
            $item = new Default_Model_Registry();
        }
        
        return $item;
    }
    
    public function save(Default_Model_Registry $registry)
    {
        $mapper = Default_Model_Registry::getMapper();
        $mapper->save($registry);
        
        return $this;
    }
}
