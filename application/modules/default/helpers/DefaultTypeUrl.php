<?php

class Default_View_Helper_DefaultTypeUrl extends Zend_View_Helper_Abstract
{

	/**
	 * A bit of a cheat but it works, avoids multiple routes confusing matters in Bootstrap
	 */
    public function defaultTypeUrl($model, $absolute=false)
    {
    	$canonical = explode('/',$this->view->defaultReadUrl($model,false));
    	$canonical = '/'.$canonical[1];

    	return $absolute ? $this->view->serverUrl($canonical) : $canonical;
    }
}
