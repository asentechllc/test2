<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Preview.php 2361 2011-04-18 08:23:26Z michal $
 */

/**
 * Allows access to system calls from gossip admin system
 *
 * @category News
 * @package News_Acl
 * @subpackage Preview
 */
class Default_Acl_System implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if valid preview token has been supplied
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {

		$fc = Zend_Controller_Front::getInstance();
		$request = $fc->getRequest();

		if($request->isPost()){
			$data = $request->getParams();
			if(isset($data['accessToken'])){
				$challenge = explode(',',base64_decode($data['accessToken']));
				if(count($challenge) == 2){
					$mapper = User_Model_User::getMapper();
					$user = $mapper->find(intval($challenge[0]));
					if($user){
						$token = sha1($user->getEmail().$user->getSalt());
						return $token == $challenge[1];
					}
				}
			}
		}

    	throw new Mcmr_Exception_PermissionDenied('Access Denied');
    
    }
}
