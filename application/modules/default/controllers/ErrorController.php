<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_ErrorController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ErrorController.php 2150 2011-02-03 11:44:52Z leigh $
 */

/**
 * Error Controller for the Default module
 *
 * @category Default
 * @package Default_ErrorController
 */
class Default_ErrorController extends Zend_Controller_Action
{

    public function init()
    {
        // Set the type if this is a page not found exception
        $errors = $this->_getParam('error_handler');
        if ($errors->exception instanceof Mcmr_Exception_PageNotFound && (404 == $errors->exception->getCode())) {
            $errors->type = Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION;
        }
    }

    public function errorAction()
    {
        $this->getResponse()->clearBody();
        $request = $this->getRequest();
        if ('default'!==$request->getSiteName()) {
            $this->_helper->layout->setLayout('error-'.$request->getSiteName());
        } else {
            $this->_helper->layout->setLayout('error');
        }
        $errors = $this->_getParam('error_handler');

        $this->view->exception = $errors->exception;
        $this->view->request   = $errors->request;

        switch ($errors->type) { 
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
        
                // 404 error -- controller or action not found
                $this->getResponse()->setHttpResponseCode(404);
                $this->view->message = 'Page not found';
                break;
            default:
                switch ($errors->exception->getCode()) {
                    case 400:
                    case 401:
                    case 403:
                    case 404:
                        $statusCode = $errors->exception->getCode();
                        break;
                    
                    default:
                        if (($errors->exception instanceof Zend_Acl_Exception)
                                || ($errors->exception instanceof Mcmr_Exception_PermissionDenied)) {
                            $statusCode = 200;
                        } else {
                            $statusCode = 500;
                            $this->_getInfo($errors);
                            $this->_notify();
                        }
                        break;
                }
                
                // application error 
                $this->getResponse()->setHttpResponseCode($statusCode);
                $this->view->message = 'Application error';
                $this->view->code = $statusCode;
                break;
        }
    }

    private function _notify()
    {
        // We only need this on production sites. Otherwise it is all onscreen
        if ('production' !== APPLICATION_ENV) {
            return;
        }

        $address = Zend_Registry::get('app-config')->error->email->address;
        $subject = Zend_Registry::get('app-config')->error->email->subject;
        
        if (null !== $address) {
            $view = clone $this->view;
            $view->setScriptPath(APPLICATION_PATH . '/templates/');
            $body = $view->render('devemail_error.phtml');
            
            $email = new Mcmr_Mail();
            $email->setBodyHtml($body);
            $email->setSubject($subject);
            $email->addTo($address);
            $email->send();
        }
    }

    private function _getInfo($errors)
    {
        $bootstrap = $this->getInvokeArg('bootstrap');
        if (null == $bootstrap) {
            return;
        }
        
        $db = $bootstrap->getResource('db');
        if (null !== $db) {
            $profiler = $db->getProfiler();
            
            $queryProfile = $profiler->getLastQueryProfile();
            if (null !== $queryProfile && $queryProfile) {
                $this->view->query = $queryProfile->getQuery();
                $this->view->queryParams = $queryProfile->getQueryParams();
            } else {
                $this->view->query = 'N/A';
                $this->view->queryParams = 'N/A';
            }
        }
        $session = new Zend_Session_Namespace();

        if (!empty($_SERVER['SERVER_ADDR'])) {
            $this->view->serverAddr = $_SERVER['SERVER_ADDR'];
        }

        if (!empty($_SERVER['HTTP_USER_AGENT'])) {
            $this->view->userAgent = $_SERVER['HTTP_USER_AGENT'];
        }

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            $this->view->requestType = $_SERVER['HTTP_X_REQUESTED_WITH'];
        }

        if (!empty($_SERVER['HTTP_REFERER'])) {
            $this->view->requestUri = $_SERVER['HTTP_REFERER'];
        }

        $this->view->session = $_SESSION;
    }
}

