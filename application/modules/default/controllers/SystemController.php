<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_SystemController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: SystemController.php 1433 2010-09-23 09:48:36Z leigh $
 */

/**
 * A controller for checking various parts of the system. Cache management and Server setup checks
 *
 * @category Default
 * @package Default_SystemController
 */
class Default_SystemController extends Zend_Controller_Action
{
    private $_settings = array(
            'display_errors' => array(
                    'expected' => 'Off',
                    'required' => true,
                    'message' => 'This must be disabled on Production sites.'
            ),
            'register_globals' => array(
                    'expected' => 'Off',
                    'required' => true,
                    'message' => 'This is a security risk. It must be disabled'
            ),
            'magic_quotes_gpc' => array(
                    'expected' => 'Off',
                    'required' => true,
                    'message' => 'This can cause data to be saved incorrectly. It must be disabled.'
            ),
            'register_long_arrays' => array(
                    'expected' => 'Off',
                    'required' => false,
                    'message' => 'Should be disabled for performance'
            ),
            'register_argc_argv' => array(
                    'expected' => 'Off',
                    'required' => false,
                    'message' => 'Should be disabled for performance'
            ),
            'memory_limit' => array(
                    'expected' => 64,
                    'required' => false,
                    'message' => '64M is the recommended minimum'
            ),
    );
    private $_extensions = array(
            'pdo' => array(
                    'required' => true,
                    'message' => 'PDO Must be enabled for all Database storage'
            ),
            'pdo_mysql' => array(
                    'required' => true,
                    'message' => 'MySQL is the database server used'
            ),
            'pdo_sqlite' => array(
                    'required' => false,
                    'message' => 'Optionally used as a caching system'
            ),
            'gd' => array(
                    'required' => false,
                    'message' => 'Used to dynamically resize images'
            ),
            'simplexml' => array(
                    'required' => true,
                    'message' => 'Some configuration files are XML. This must be enabled'
            ),
            'memcache' => array(
                    'required' => false,
                    'message' => 'Prefered database caching system.'
            ),
            'apc' => array(
                    'required' => false,
                    'message' => 'APC is the recommended op code cache.
                        It can be used as a system cache in place of memcache as well'
            ),
            'openssl' => array(
                    'required' => false,
                    'message' => 'This must be enabled in order to send email via SMTP using TLS'
            ),
    );

    public function init()
    {
        // This is required if this is production. Otherwise just a warning
        $this->_settings['display_errors']['required'] = ('production' === APPLICATION_ENV);
    }

    public function indexAction()
    {

    }

    /**
     * Check the server settings to ensure everything is set up for the system
     */
    public function checkAction()
    {
        $this->view->zfVersion = Zend_Version::VERSION;
        $this->_checkExtensions();
        $this->_checkSettings();
    }

    /**
     * Generate PHP errors. To ensure errors are being reported correctly for the environment
     */
    public function errorAction()
    {
        // Display the view now. The errors will prevent this from happening later
        echo $this->view->render('system/error.phtml');

        // Generate an E_STRICT, E_NOTICE, E_WARN, E_FATAL
        function change(&$var)
        {
            $var++;
        }

        $var = 1;
        change(++$var); // E_STRICT
        trigger_error('Non existant error type', FOO); // E_NOTICE && E_WARN
        bar($var); // E_FATAL
        exit;
    }

    /**
     * Display an error page. To ensure errors are being reported correctly for the environment
     */
    public function exceptionAction()
    {
        throw new Exception(
            'This is a dummy error page. Ensure system information is not displayed on production sites'
        );
    }

    /**
     * Check and manage the system caches
     */
    public function cacheAction()
    {
        $cachemanager = Zend_Registry::get('cachemanager');

        if ($this->_request->isPost()) {
            $values = $this->_request->getParams();

            if (isset($values['clear']) && is_array($values['clear'])) {
                foreach ($values['clear'] as $cachename) {
                    $cache = $cachemanager->getCache($cachename);
                    if (null !== $cache) {
                        $cache->clean(Zend_Cache::CLEANING_MODE_ALL);
                    }
                    
                    $this->view->getHelper('DisplayMessages')->addMessage("{$cachename} cache flushed");
                }

            }
        }

		if(!empty($values['cli'])){
			$messages = $this->view->getHelper('DisplayMessages')->getMessages();

			foreach($messages as $type=>$message)
				echo $type.': '.implode(', ',$message).PHP_EOL;

			echo 'Finished.'.PHP_EOL;
			exit;
		}

        $this->view->caches = array();
        $this->view->caches['database'] = $cachemanager->getCacheTemplate('database');
        //$this->view->caches['session'] = $cachemanager->getCacheTemplate('session');
        $this->view->caches['config'] = $cachemanager->getCacheTemplate('config');
        $this->view->caches['metadata'] = $cachemanager->getCacheTemplate('metadata');
        $this->view->caches['fullpage'] = $cachemanager->getCacheTemplate('fullpage');

        $this->view->caches['database']['status'] = $this->_cacheStatus($cachemanager->getCache('database'));
        //$this->view->caches['session']['status'] = $this->_cacheStatus($cachemanager->getCache('session'));
        $this->view->caches['config']['status'] = $this->_cacheStatus($cachemanager->getCache('config'));
        $this->view->caches['metadata']['status'] = $this->_cacheStatus($cachemanager->getCache('metadata'));
        $this->view->caches['fullpage']['status'] = $this->_cacheStatus($cachemanager->getCache('fullpage'));
    }

    public function imagecacheAction()
    {
        $path = SITE_PATH . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'cimages' . DIRECTORY_SEPARATOR;
        if ($this->_request->isPost()) {
            // Delete files
            $files = $this->_request->getParam('file');
            foreach ($files as $file) {
                $file = str_replace('/', '', str_replace('..', '', $file));
                if (is_file($path . $file)) {
                    unlink($path . $file);
                }
            }
        }

        $files = array();
        if (is_dir($path)) {
            if (false !== ($dirHandle = opendir($path))) {
                while (false !== ($file = readdir($dirHandle))) {
                    if (('.' !== $file[0])) {
                        $files[] = $file;
                    }
                }
            }
        }
        $this->view->files = $files;
    }

    /**
     * Fetch the cache status and statistics
     *
     * @param Zend_Cache $cache
     * @return array
     */
    private function _cacheStatus($cache)
    {
        $status = array();
        if (null !== $cache) {
            $backend = $cache->getBackend();
            $status['Lifetime'] = $backend->getLifetime(false) . ' seconds';
            try {
                $status['Filling Percentage'] = $backend->getFillingPercentage() . '%';

                if ($backend instanceof Mcmr_Cache_Backend_ExtendedInterface) {
                    $status['Hit Percentage'] = $backend->getHitPercentage() . '%';
                    $status['Servers'] = $backend->getServers();
                }
            } catch (Exception $e) {
                $status['Error'] = $e->getMessage();
            }
        }

        return $status;
    }

    /**
     * Check the necessary PHP Extensions are loaded
     */
    private function _checkExtensions()
    {
        $loaded = array();
        foreach ($this->_extensions as $extension => $info) {
            $info['loaded'] = extension_loaded($extension);
            $loaded[$extension] = $info;
        }

        $this->view->extensions = $loaded;
    }

    /**
     * Ensure the PHP settings are correct
     */
    private function _checkSettings()
    {

        $settings = array();
        foreach ($this->_settings as $setting => $info) {
            $value = ini_get($setting);

            $expected = $info['expected'];
            $info['value'] = $value;
            if ('On' == $info['expected']) {
                $expected = ('On' == $info['expected']);
                $value = ('On' == $value || '1' == $value || true === $value);
                $info['value'] = $value ? 'On' : 'Off';
            } elseif ('Off' == $info['expected']) {
                $expected = !('Off' == $info['expected']);
                $value = ('On' == $value || '1' == $value || true === $value);
                $info['value'] = $value ? 'On' : 'Off';
            }

            if ('memory_limit' == $setting) {
                $pass = (intval($value) >= $expected);
            } else {
                $pass = ($value == $expected);
            }

            $info['pass'] = $pass;
            $settings[$setting] = $info;
        }

        $this->view->settings = $settings;
    }

}
