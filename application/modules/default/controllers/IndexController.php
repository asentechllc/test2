<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2002 2011-01-04 00:03:45Z leigh $
 */

/**
 * Default_IndexController
 *
 * The Purpose of this controller is to do nothing but forward the request onto the real default page
 * as determined by the configuration
 *
 * @category Default
 * @package Default_IndexController
 */
class Default_IndexController extends Mcmr_Controller_Action
{
    public function init()
    {
    
    	parent::init();
    
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->setAutoJsonSerialization(false)
            ->addActionContext('index', 'json')
            ->addActionContext('index', 'html')
            ->addActionContext('read', 'json')
            ->addActionContext('read', 'html')
            ->initContext();

        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        $contextSwitch->setContext(
            'plist', array(
                'headers'=>array('Content-Type'=>'text/xml'),
                'suffix'=>'plist',
            )
        );

        $contextSwitch->addActionContext('read', 'plist')
                ->initContext();
    }

    public function indexAction()
    {

        $config = Zend_Registry::get('default-config');

		# do we have a default config?
        if($config) {
			$module = $config->default->module;
			$controller = $config->default->controller;
			$action = $config->default->action;
			$params = isset($config->default->params)?$config->default->params->toArray():null;
		} else{
			$module = 'default';
			$controller = 'index';
			$action = 'read';
			$params = array('url'=>'home');
		}

		# avoid infinite loop
		if(!($action == 'index' && $controller == 'index' && $module == 'default'))
	        $this->_forward($action, $controller, $module, $params);

    }

    public function readAction()
    {
        if (null !== ($page = $this->_request->getParam('url', null))) {
            $this->_helper->viewRenderer('read/'.Mcmr_StdLib::urlize($page));
        } else {
            throw new Mcmr_Exception_PageNotFound();
        }
    }

    /**
     * Preview a post
     */
    public function previewAction()
    {
    
    	$cfg = $this->view->config('default')->preview->toArray();
    	
  		foreach($cfg as $formId=>$config){
  			$data = $this->_request->getParam($formId);
  			if($data)break;
  		}
  		
  		if(!$data)
    		throw new Exception('Invalid preview data');
    
   		# fix dates
   		foreach($data as $k=>$v){
			if(substr($k,-4) == 'date'){
				$data[$k] = mktime(
					isset($data[$k]['hour']) ? intval($data[$k]['hour']) : 0,
					isset($data[$k]['minutes']) ? intval($data[$k]['minutes']) : 0,
					0,
					isset($data[$k]['month']) ? intval($data[$k]['month']) : 0,
					isset($data[$k]['day']) ? intval($data[$k]['day']) : 0,
					isset($data[$k]['year']) ? intval($data[$k]['year']) : 0
				);
			}
		}

		$this->_model = new $config['model'];
		$this->_model->setId(0);
		
		if(empty($data['title']) || !trim($data['title']))
			$data['title'] = 'Untitled';
		
		$this->_model->setOptions($data);

		$this->view->model = $this->_model;

		if(isset($config['module']))
			$this->_request->setModuleName($config['module']);

		if(isset($config['viewPath']))
			$this->view->addScriptPath(SITE_PATH.'/views/'.$config['viewPath']);

    	$this->_helper->_layout->setLayout('one-column');

		if(isset($config['viewScript']))
			$this->render($config['viewScript'],null,true);


    }

}

