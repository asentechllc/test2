<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Job
 * @package Job_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 1308 2010-09-03 15:22:16Z leigh $
 */

/**
 * Bootstrap
 *
 * @category Job
 * @package Job_Bootstrap
 */
class Job_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');

        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Job_View_Helper');
    }
}
