<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application
 *
 * @author leigh
 */
class Job_Form_Application extends Mcmr_Form
{
    public function init()
    {
        $this->setName('jobformapplication')->setElementsBelongTo('job-form-application');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        // Add some CSRF protection
        $this->addElement('hash', 'csrf', array('salt' => 'unique'));
    }

    public function postInit()
    {
        $element = $this->getElement('jobid');
        if (null !== $element) {
            $front = Zend_Controller_Front::getInstance();
            $request = $front->getRequest();
            $jobid = $request->getParam('jobid', null);

            $element->setValue($jobid);
        }
    }

}
