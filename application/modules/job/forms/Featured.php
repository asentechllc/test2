<?php

class Job_Form_Featured extends Mcmr_Form
{
    public function init()
    {
        $this->setName('jobformfeatured')->setElementsBelongTo('job-form-featured');
        $this->setMethod('post');
        
        $config = Zend_Registry::get('job-config');
        $count = isset($config->numberFeatured)?$config->numberFeatured:4;
        
        // Get the featured jobs
        $mapper = Job_Model_Job::getMapper();
        $condition = null;
        $order = array('featured'=>'desc', 'order'=>'asc');
        $page=array('page'=>1, 'count'=>$count);
        $jobs = $mapper->findAllByField($condition, $order, $page);
        
        $subform = new Zend_Form_SubForm();
        $subform->setElementsBelongTo('jobs');
        
        $x=0;
        foreach ($jobs as $job) {
            $element = array(
                    'options'=>array(
                        'label'=>'Job '.($x+1),
                        'required'=>false,
                        'value'=>$job->getId(),
                    ),
                    'optionSource'=>array(
                        'mapper'=>'Job_Model_Job',
                        'valueField'=>'id',
                        'displayField'=>'title',
                    ));
            $element = $this->_processElement($element);
            $subform->addElement('Select', "$x", $element['options']);
            $x++;
        }
        
        $this->addSubForm($subform, 'jobs');
        
        $this->addElement(
            'Submit', 'submit',
            array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );

        
        // Add some CSRF protection
        $this->addElement(
            'hash', 'csrf',
            array(
                'salt' => 'unique'
            )
        );
    }
}
