<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Job_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of Job
 *
 * @category   Job
 * @package    Job_Form
 * @subpackage Category
 */
class Job_Form_Category extends Mcmr_Form
{
    public function init()
    {
        $this->setName('jobformcategory')->setElementsBelongTo('job-form-category');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

    }

    public function postInit()
    {
    }
}
