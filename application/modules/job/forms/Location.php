<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @Location   Job
 * @package    Job_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of Job
 *
 * @Location   Job
 * @package    Job_Form
 * @subpackage Location
 */
class Job_Form_Location extends Mcmr_Form
{
    public function init()
    {
        $this->setName('jobformlocation')->setElementsBelongTo('job-form-location');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

    }

    public function postInit()
    {
    }
}
