<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Job_Form_Job
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 1792 2010-11-17 17:09:21Z leigh $
 */

/**
 * Description of Job
 *
 * @category   Job
 * @package    Job_Form
 * @subpackage Job
 */
class Job_Form_Job extends Mcmr_Form
{
    public function init()
    {
        $this->setName('jobformjob')->setElementsBelongTo('job-form-job');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        $this->addElement(
            'Submit', 'submit', array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );
    }

    public function postInit()
    {
    }
    
    /**
     * Bit of a hack, but time restrictions and all that
     * Check upload elements and mark as unrequired if we already have an uploaded file
     * If file is already uploaded, filename will be passed through in POST data
     **/
    public function isValid($data){	
    	foreach($this->getElements() as $element){
    		if($element instanceof Mcmr_Form_Element_Upload){
    			if(isset($data[$this->getName()]) && isset($data[$this->getName()][$element->getName()]) && $data[$this->getName()][$element->getName()]){
    				$element->setRequired(false);
    			}
    		}
    	}
    	return parent::isValid($data);
    }
}
