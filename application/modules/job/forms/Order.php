<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Order.php 2359 2011-04-15 11:15:41Z leigh $
 */

/**
 * A form for changing the display order of the job jobs
 *
 * @category   Job
 * @package    Form
 * @subpackage Order
 */
class Job_Form_Order extends Mcmr_Form_Order
{
}
