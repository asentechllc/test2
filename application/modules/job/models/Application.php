<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all jobs
 *
 * @category   Job
 * @package    Model
 * @subpackage Application
 */
class Job_Model_Application extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Job_Model_ApplicationMapper';

    protected $_id = null;
    protected $_description = null;
    protected $_userid = null;
    protected $_jobid = null;
    protected $_date = null;
    protected $_file = null;

    private $_user = null;
    private $_job = null;

    /**
     * Return mapper for model
     *
     * @return Job_Model_ApplicationMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Return the Job ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the job ID
     *
     * @param int $id
     * @return Job_Model_Application
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the job description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the job description
     *
     * @param string $description
     * @return Job_Model_Application
     */
    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }

    /**
     * Get this application's job ID
     *
     * @return int
     */
    public function getJobid()
    {
        return $this->_jobid;
    }

    /**
     * Set thie application's job ID
     *
     * @param int $jobid
     * @return Job_Model_Application 
     */
    public function setJobid($jobid)
    {
        $this->_jobid = $jobid;
        $this->_job = null;

        return $this;
    }

    /**
     * Get this application's user ID. If not set it will default to the authenticated user
     * If there is no authenticated user it will be set to 0
     *
     * @return int
     */
    public function getUserid()
    {
        if (null === $this->_userid) {
            $user = Zend_Registry::get('authenticated-user');
            if (null !== $user->getId()) {
                $this->_userid = $user->getId();
            } else {
                $this->_userid = 0;
            }
        }
        
        return $this->_userid;
    }

    /**
     * Set the application's user ID
     *
     * @param int $userid
     * @return Job_Model_Application 
     */
    public function setUserid($userid)
    {
        $this->_userid = $userid;
        $this->_user = null;

        return $this;
    }

    /**
     * Get the application date as a unix timestamp
     *
     * @return int
     */
    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = time();
        }
        
        return $this->_date;
    }

    /**
     * Set the job application date as a unix timestamp
     *
     * @param int $date
     * @return Job_Model_Application 
     */
    public function setDate($date)
    {
        $this->_date = (int)$date;

        return $this;
    }

    /**
     * Get the attached file's filename
     *
     * @return string
     */
    public function getFile()
    {
        return $this->_file;
    }

    /**
     * Set the attached file's filename
     *
     * @param string $file
     * @return Job_Model_Application 
     */
    public function setFile($file)
    {
        $this->_file = $file;

        return $this;
    }

    /**
     * Get the application user as a user model
     *
     * @return User_Model_User
     */
    public function getUser()
    {
        if (null === $this->_user) {
            $this->_user = User_Model_User::getMapper()->find($this->getUserid());
        }
        
        return $this->_user;
    }

    /**
     * Get the application job model
     *
     * @return Job_Model_Job
     */
    public function getJob()
    {
        if (null === $this->_job) {
            $this->_job = Job_Model_Job::getMapper()->find($this->getJobid());
        }

        return $this->_job;
    }
}
