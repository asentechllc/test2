<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 2268 2011-03-23 16:52:47Z leigh $
 */

/**
 * Model class for all jobs
 *
 * @category   Job
 * @package    Model
 * @subpackage Job
 */
class Job_Model_Job extends Mcmr_Model_Ordered
{
    static protected $_mapperclass = 'Job_Model_JobMapper';

    protected $_id = null;
    protected $_url = null;
    protected $_featured = null;
    protected $_featuredstart = null;
    protected $_featuredend = null;
    protected $_title = null;
    protected $_description = null;
    protected $_salary = null;
    protected $_date = null;
    protected $_expiredate = null;
    protected $_type = null;
    protected $_email = null;
    protected $_published = null;
    protected $_status = null;
    protected $_companyid = null;
    protected $_categoryid = null;
    protected $_locationid = null;
    protected $_userid = null;

    private $_user = null;
    private $_category = null;
    private $_location = null;
    private $_company = null;
    private $_applications = null;
    private $_urlchanged = false;

    /**
     * Return mapper for model
     *
     * @return Job_Model_JobMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Return the Job ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the job ID
     *
     * @param int $id
     * @return Job_Model_Job
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Return the URL ID for the Job
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }
        
        return $this->_url;
    }

    /**
     * Set the URL ID for the Job
     *
     * @param string $url
     * @return Job_Model_Job
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }

    /**
     * Return the featured flag
     *
     * @return bool
     */
    public function getFeatured()
    {
        if (null === $this->_featured) {
            $this->_featured = false;
        }
        
        return $this->_featured;
    }

    /**
     * Set the featured flag
     *
     * @param bool $featured
     * @return Job_Model_Job
     */
    public function setFeatured($featured)
    {
        $this->_featured = $featured;

        return $this;
    }
    
    /**
     * Set the featured end date as a unix timestamp
     *
     * @param int $featuredend
     * @return Job_Model_Job
     */
    public function setFeaturedend($featuredend)
    {
        $this->_featuredend = (int)$featuredend;
        
        return $this;
    }

    /**
     * Get the featured end date as a unix timestamp
     *
     * @return int
     */
    public function getFeaturedend()
    {
        if (null === $this->_featuredend) {
            $this->_featuredend = 0;
        }

        return $this->_featuredend;
    }

    /**
     * Set the featured start date as a unix timestamp
     *
     * @param int $featuredstart
     * @return Job_Model_Job
     */
    public function setFeaturedstart($featuredstart)
    {
        $this->_featuredstart = (int)$featuredstart;
        
        return $this;
    }

    /**
     * Get the featured start date as a unix timestamp
     *
     * @return int
     */
    public function getFeaturedstart()
    {
        if (null === $this->_featuredstart) {
            $this->_featuredstart = 0;
        }
        
        return $this->_featuredstart;
    }
    
    /**
     * Get the job title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the job title
     *
     * @param string $title
     * @return Job_Model_Job
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the job description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the job description
     *
     * @param string $description
     * @return Job_Model_Job
     */
    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }

    /**
     * Get the job salary
     *
     * @return string
     */
    public function getSalary()
    {
        return $this->_salary;
    }

    /**
     * Set the job salary
     *
     * @param string $salary
     * @return Job_Model_Job
     */
    public function setSalary($salary)
    {
        $this->_salary = $salary;

        return $this;
    }

    /**
     * Get the job creation date as a unix timestamp
     *
     * @return int
     */
    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = strtotime('today');
        }
        
        return $this->_date;
    }

    /**
     * Set the create date with a unix timestamp
     *
     * @param int $date
     * @return Job_Model_Job
     */
    public function setDate($date)
    {
        $this->_date = (int)$date;

        return $this;
    }

    /**
     * Get the expiry date as a unix timestamp
     *
     * @return int
     */
    public function getExpiredate()
    {
        if (null === $this->_expiredate) {
            $config = Zend_Registry::get('job-config');
            if (isset($config->jobs->defaultExpiryTime)) {
                $this->_expiredate = strtotime($config->jobs->defaultExpiryTime);
            } else {
                $this->_expiredate = strtotime("today");
            }
        }

        return $this->_expiredate;
    }

    /**
     * Set the job expiry date as a unix timestamp
     *
     * @param int $expiredate
     * @return Job_Model_Job
     */
    public function setExpiredate($expiredate)
    {
        $this->_expiredate = (int)$expiredate;

        return $this;
    }

    /**
     * Get the job type
     *
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Set the job type
     *
     * @param string $type
     * @return string
     */
    public function setType($type)
    {
        $this->_type = $type;

        return $this;
    }

    /**
     * Get the job application email address
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * Set the job application email address
     *
     * @param string $email
     * @return Job_Model_Job
     */
    public function setEmail($email)
    {
        $this->_email = $email;

        return $this;
    }

    /**
     * Fetch the job published flag
     *
     * @return bool
     */
    public function getPublished()
    {
        if (null === $this->_published) {
            $this->_published = false;
        }

        return intval($this->_published);
    }

    /**
     * Set the job published flag
     *
     * @param bool $published
     * @return Job_Model_Job 
     */
    public function setPublished($published)
    {
        $this->_published = (true == $published);

        return $this;
    }

    /**
     * Get the job status string
     *
     * @return string
     */
    public function getStatus()
    {
        if (null === $this->_status) {
            $this->_status = 'inactive';
        }

        return $this->_status;
    }

    /**
     * Set the job status
     *
     * @param string $status
     * @return Job_Model_Job
     */
    public function setStatus($status)
    {
        $this->_status = $status;

        return $this;
    }

    /**
     * Get the company ID who posted this job
     *
     * @return int
     */
    public function getCompanyid()
    {
        return $this->_companyid;
    }

    /**
     * Set the company ID
     *
     * @param int $companyid
     * @return Job_Model_Job
     */
    public function setCompanyid($companyid)
    {
        $this->_company = null;
        $this->_companyid = $companyid;

        return $this;
    }

    /**
     * Fetch the job category ID
     *
     * @return int
     */
    public function getCategoryid()
    {
        return $this->_categoryid;
    }

    /**
     * Set the job category ID
     *
     * @param int $categoryid
     * @return Job_Model_Job
     */
    public function setCategoryid($categoryid)
    {
        $this->_category = null;
        $this->_categoryid = $categoryid;

        return $this;
    }

    /**
     * Get the job location ID
     *
     * @return int
     */
    public function getLocationid()
    {
        return $this->_locationid;
    }

    /**
     * Set the job location ID
     *
     * @param int $locationid
     * @return Job_Model_Job
     */
    public function setLocationid($locationid)
    {
        $this->_location = null;
        $this->_locationid = $locationid;

        return $this;
    }

    /**
     * Set the job's user ID
     *
     * @param int $userid
     * @return Job_Model_Job 
     */
    public function setUserid($userid)
    {
        $this->_userid = $userid;
        
        return $this;
    }
    
    /**
     * Get the job's user ID. If not set it will default to the authenticated user
     *
     * @return type 
     */
    public function getUserid()
    {
        if (null === $this->_userid && Mcmr_Model_ModelAbstract::STATE_CLEAN !== $this->state()) {
            $user = Zend_Registry::get('authenticated-user');
            if (null !== $user) {
                $this->_userid = $user->getId();
            }
        }
        
        return $this->_userid;
    }

    /**
     * Get the job location model
     *
     * @return Job_Model_Location
     */
    public function getLocation()
    {
        if (null === $this->_location) {
            $mapper = Job_Model_Location::getMapper();

            $this->_location = $mapper->find($this->getLocationid());
        }
        
        return $this->_location;
    }

    /**
     * Get the category location model
     *
     * @return Job_Model_Category
     */
    public function getCategory()
    {
        if (null === $this->_category) {
            $mapper = Job_Model_Category::getMapper();

            $this->_category = $mapper->find($this->getCategoryid());
        }

        return $this->_category;
    }

    /**
     * Get the company model
     *
     * @return Company_Model_Company
     */
    public function getCompany()
    {
        if (null === $this->_company && $this->getCompanyid()) {
            $mapper = Company_Model_Company::getMapper();
            $this->_company = $mapper->find($this->getCompanyid());
        }

        return $this->_company;
    }

    /**
     * Get the this job's user as a user model
     *
     * @return User_Model_User
     */
    public function getUser()
    {
        $id = $this->getUserid();
        if (null !== $id) {
            $mapper = User_Model_User::getMapper();
            $this->_user = $mapper->find($id);
        }

        return $this->_user;
    }

    /**
     * Fetch the applications for this job
     *
     * @param int|array $page
     * @return Mcmr_Paginator
     */
    public function getApplications($page = 1)
    {
        if (!is_array($page)) {
            $page = array('page'=>$page, 'count'=>10);
        }

        return Job_Model_Application::getMapper()->findAllByField(array('job'=>$this->getId()), null, $page);
    }

    /**
     * Determine whether this job is featured or not.
     * 
     * @return bool
     */
    public function isFeatured()
    {
        $now = time();
        return $this->getFeatured() && ($this->getFeaturedstart()<$now) && ($this->getFeaturedend()>$now);
    }

    public function isPublished()
    {
        return $this->getPublished();
    }

    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }

    public function getCurrent()
    {
        $time = time();
        if ($time<$this->getExpiredate() && $time>=$this->getDate()) {
            return true;
        }
        return false;
    }

    public function isCurrent()
    {
        return $this->getCurrent();
    }

    public function getCurrentDescription() 
    {
        if ($this->getCurrent()) {
            return 'current';
        } else {
            $time = time();
            if ($time>=$this->getExpiredate()) {
                return 'expired';
            } else {
                return 'future';
            }
        }
    }
}
