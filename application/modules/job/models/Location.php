<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all jobs
 *
 * @category   Job
 * @package    Model
 * @subpackage Location
 */
class Job_Model_Location extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Job_Model_LocationMapper';

    protected $_id = null;
    protected $_url = null;
    protected $_title = null;
    protected $_description = null;

    /**
     * Return mapper for model
     *
     * @return Job_Model_LocationMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Return the Job ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the job ID
     *
     * @param int $id
     * @return Job_Model_Location
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Return the URL ID for the Job
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }
        
        return $this->_url;
    }

    /**
     * Set the URL ID for the Job
     *
     * @param string $url
     * @return Job_Model_Location
     */
    public function setUrl($url)
    {
        $this->_url = Mcmr_StdLib::urlize($url);

        return $this;
    }

    /**
     * Get the job title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the job title
     *
     * @param string $title
     * @return Job_Model_Location
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the job description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the job description
     *
     * @param string $description
     * @return Job_Model_Location
     */
    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }

}
