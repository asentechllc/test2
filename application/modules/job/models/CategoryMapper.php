<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for job models
 *
 * @category   Job
 * @package    Model
 * @subpackage CategoryMapper
 */
class Job_Model_CategoryMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Job_Model_DbTable_Category';
    protected $_columnPrefix = 'category_';
    protected $_modelClass = 'Job_Model_Category';
    protected $_cacheIdPrefix = 'JobCategory';

    public function  __construct()
    {
        parent::__construct();

        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
    }
}