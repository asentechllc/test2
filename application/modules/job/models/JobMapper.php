<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: JobMapper.php 2153 2011-02-04 12:43:28Z leigh $
 */

/**
 * Mapper class for job models
 *
 * @category   Job
 * @package    Model
 * @subpackage JobMapper
 */
class Job_Model_JobMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Job_Model_DbTable_Job';
    protected $_dbAttrTableClass = 'Job_Model_DbTable_JobAttr';
    protected $_dbOrdrTableClass = 'Job_Model_DbTable_JobOrdr';
    protected $_modelClass = 'Job_Model_Job';
    protected $_columnPrefix = 'job_';
    protected $_cacheIdPrefix = 'Job';

    protected function _sanitiseValues($data) 
    {
        unset($data['job_order']);
        return $data;
    }

    /**
     * Convert a readable fieldname into the database fieldname
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'category':
            case 'categoryid':
            case 'category_id':
                $field = 'category_id';
                break;

            case 'location':
            case 'locationid':
            case 'location_id':
                $field = 'location_id';
                break;

            case 'company':
            case 'companyid':
            case 'company_id':
                $field = 'company_id';
                break;

            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }

    /**
     * Convert field value before inserting into databse query. 
     * 
     * @param string $field field name passed to be able to tell which filed you are dealing with.
     * @param mixed $value field value as stored in  the object. Process if necessary and return.
     * @return Zend_Db_Expr|string|int field value in the format suitable for a database query. 
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'job_date':
            case 'date':
            case 'job_expiredate':
            case 'expiredate':
            case 'job_featuredstart':
            case 'featuredstart':
            case 'job_featuredend':
            case 'featuredend':
                if (is_int($value))
                    $value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
                elseif (is_array($value)) {
                    foreach ($value as $key => $item) {
                        if (is_int($item)) {
                            $value[$key] = new Zend_Db_Expr("FROM_UNIXTIME({$item})");
                        }
                    }
                }
                break;
            case 'job_featured':
                $value = (int)$value;
                break;
        }

        return $value;
    }
    
    /** 
     * Create a list of fields to be read form the databse in SELECT query.
     * 
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(job_date) AS job_date";
        $fields[] = "UNIX_TIMESTAMP(job_expiredate) AS job_expiredate";
        $fields[] = "UNIX_TIMESTAMP(job_featuredstart) AS job_featuredstart";
        $fields[] = "UNIX_TIMESTAMP(job_featuredend) AS job_featuredend";
        return $fields;
    }

    /**
     * Add a condition to the select query
     *
     * @param Zend_Db_Select $select
     * @param string $field
     * @param string $value
     * @return Zend_Db_Select
     */
    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        if ('job_current' === $field) {
            $time = time();
            if ($value==1) {
                $select->where('FROM_UNIXTIME(?) < job_expiredate', $time);
                $select->where('FROM_UNIXTIME(?) >= job_date', $time);
            } else {
                $select->where('(FROM_UNIXTIME(?) >= job_expiredate) OR (FROM_UNIXTIME(?) < job_date)', $time, $time);
            }
        } else {
            return parent::_addCondition($select, $field, $value);
        }
    }
    
}
