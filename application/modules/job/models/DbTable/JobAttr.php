<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   
 * @package    _JobAttr
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: JobAttr.php 1203 2010-08-09 16:36:14Z leigh $
 */

/**
 * Description of JobAttr
 *
 * @category   
 * @package    _JobAttr
 * @subpackage 
 */
class Job_Model_DbTable_JobAttr extends Mcmr_Db_Table_Abstract implements Mcmr_Model_AttrInterface
{
    protected $_name = 'job_jobs_attr';
    protected $_primary = array('job_id', 'attr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insertIgnore($this->_rowData);
        } else {
            parent::insertIgnore($data);
        }
    }

    public function setAttrName($name)
    {
        $this->_rowData['attr_name'] = $name;

        return $this;
    }

    public function setAttrValue($value)
    {
        $this->_rowData['attr_value'] = $value;

        return $this;
    }

    public function setAttrId($id)
    {
        $this->_rowData['job_id'] = $id;

        return $this;
    }

    public function setAttrType($type)
    {
        $this->_rowData['attr_type'] = $type;

        return $this;
    }
}
