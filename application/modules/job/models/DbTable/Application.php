<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Class for the job location DB table
 *
 * @category   Job
 * @package    Model
 * @subpackage DbTable
 */
class Job_Model_DbTable_Application extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'job_applications';
    protected $_primary = 'application_id';
}
