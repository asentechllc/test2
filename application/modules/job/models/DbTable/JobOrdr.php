<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Job
 * @package Job_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: JobAttr.php 718 2010-06-01 13:33:37Z leigh $
 */

/**
 * Description of JobAttr
 *
 * @category Job
 * @package Job_Model
 * @subpackage DbTable
 */
class Job_Model_DbTable_JobOrdr extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'job_jobs_ordr';
    protected $_primary = array('job_id', 'ordr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insert($this->_rowData);
        } else {
            parent::insert($data);
        }
    }

    public function setOrdrName($name)
    {
        $this->_rowData['ordr_name'] = $name;

        return $this;
    }

    public function setOrdrValue($value)
    {
        $this->_rowData['ordr_value'] = (int)$value;

        return $this;
    }

    public function setOrdrId($id)
    {
        $this->_rowData['job_id'] = $id;

        return $this;
    }
}
