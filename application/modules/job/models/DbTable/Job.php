<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 1203 2010-08-09 16:36:14Z leigh $
 */

/**
 * Class for the job DB table
 *
 * @category   Job
 * @package    Model
 * @subpackage DbTable
 */
class Job_Model_DbTable_Job extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'job_jobs';
    protected $_primary = 'job_id';
}
