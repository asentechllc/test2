<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Job
 * @package Job_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Owner.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * A Zend_Acl assertion class. Ensures that only the owner of the job object can edit the object
 *
 * @category User
 * @package User_Acl
 * @subpackage Owner
 */
class Job_Acl_SessionOwner implements Zend_Acl_Assert_Interface
{

    /**
     * Returns true if and only if the authenticated user owns the job
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the user object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id');
        }

        $job = Job_Model_Job::getMapper()->find($id);
        $user = Zend_Registry::get('authenticated-user');
        if (null == $job) {
            return false;
        } else {
            return ($user->getId() == $job->getUserid());
        }
    }

}