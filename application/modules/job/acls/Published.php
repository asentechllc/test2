<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Job
 * @package Job_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of ViewPublished
 *
 * @category Job
 * @package Job_Acl
 * @subpackage ViewPublished
 */
class Job_Acl_Published implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if the job is published
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
            $url = $controller->getRequest()->getParam('url', null);
        }

        if (null !== $id || null !== $url) {
            $mapper = Job_Model_Job::getMapper();
            $job = null !== $id?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));

            // If the user owns the job let them view it.
            $user = Zend_Registry::get('authenticated-user');
            if ((null !== $user) && (null !== $job)) {
                if ($user->getId() == $job->getUserid()) {
                    return true;
                }
            }

            return (null !== $job && $job->isPublished());
        } else {
            // There is no ID, so allow it, but force only published
            $controller->getRequest()->setParam('published', '1');
            return true;
        }

    }
}
