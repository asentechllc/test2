<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Job_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of jobs based on the conditions
 *
 * @category   Job
 * @package    Job_View
 * @subpackage Helper
 */
class Job_View_Helper_JobIndex extends Zend_View_Helper_Abstract
{
    public function jobIndex($condition = null, $order=null, $page=null, $partial = null)
    {
        $mapper = Job_Model_Job::getMapper();
        $jobs = $mapper->findAllByField($condition, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('jobs'=>$jobs));
                }
            }

            return $this->view->partial($partial, array('jobs'=>$jobs));
        }

        return $jobs;
    }
}
