<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Job
 * @package    Job_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of jobs based on the conditions
 *
 * @category   Job
 * @package    Job_View
 * @subpackage Helper
 */
class Job_View_Helper_JobCount extends Zend_View_Helper_Abstract
{
    public function jobCount($condition = null)
    {
        $mapper = Job_Model_Job::getMapper();

        return $mapper->count($condition);;
    }
}
