<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Job
 * @package Job_ApplicationController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 *
 * @category Job
 * @package Job_ApplicationController
 */
class Job_ApplicationController extends Zend_Controller_Action
{

    public function indexAction()
    {
        $fields = array();
        $order = array('title'=>'asc');

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('job-config')->applications->itemCountPerPage);

        $mapper = Job_Model_Application::getMapper();
        $this->view->applications = $mapper->findAllByField($fields, $order, $page);
    }

    public function createAction()
    {
        $form = $this->_helper->loadForm('Application');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $application = new Job_Model_Application($form->getValues(true));

                try {
                    $mapper = Job_Model_Application::getMapper();
                    $mapper->save($application);

                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('applicationSaved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $application);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('applicationInvalidForm', 'warn');
            }
        }
    }

    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Job_Model_Application::getMapper();
            if (null !== ($application = $mapper->find($id))) {
                $this->view->application = $application;
            } else {
                throw new Mcmr_Exception_PageNotFound('Application Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Job_Form_Application();
            $this->view->form = $form;

            $mapper = Job_Model_Application::getMapper();
            if (null !== ($application = $mapper->find($id))) {
                $this->view->application = $application;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $application->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($application);

                            $this->view->getHelper('DisplayMessages')->addMessage('applicationSaved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $application);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('applicationInvalidForm', 'warn');
                    }
                } else {
                    $form->populate($application->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Application Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Job_Model_Application::getMapper();
            if (null !== ($application = $mapper->find($id))) {
                $this->view->application = $application;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($application);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Job Application Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $application);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Application Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
}
