<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Job
 * @package Job_CategoryController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Category Controller for the Job module
 *
 * @category Job
 * @package Job_CategoryController
 */
class Job_CategoryController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $fields = array();
        $fields['title'] = $this->_request->getParam('title', null);

        // Allow search on partial string. If it contains a % assume a LIKE search
        if (false !== strpos($fields['title'], '%')) {
            $fields['title'] = array('condition'=>'LIKE', 'value'=>$fields['title']);
        }

        $order = array('title'=>'asc');

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('job-config')->jobs->itemCountPerPage);

        $mapper = Job_Model_Category::getMapper();
        $this->view->categories = $mapper->findAllByField($fields, $order, $page);
    }


    /**
     * Create a new Category. Display form on GET. Create on POST.
     */
    public function createAction()
    {
        $form = new Job_Form_Category();
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $category = new Job_Model_Category();
                $mapper = Job_Model_Category::getMapper();
                $category->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($category);

                    $this->view->getHelper('DisplayMessages')->addMessage('Job Category Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $category);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Display a Job Category
     */
    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Job_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $this->view->category = $category;
            } else {
                throw new Mcmr_Exception_PageNotFound('Category Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a Job Category. Display form on GET. Save on POST
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Job_Form_Category();
            $this->view->form = $form;

            $mapper = Job_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $this->view->category = $category;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $category->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($category);

                            $this->view->getHelper('DisplayMessages')->addMessage('Job Category Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $category);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($category->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Category Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a Job Category. Display confirmation on GET. Delete on POST.
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Job_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $this->view->category = $category;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($category);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Job Category Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $category);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Category Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
}