<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @Location Job
 * @package Job_LocationController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Location Controller for the Job module
 *
 * @Location Job
 * @package Job_LocationController
 */
class Job_LocationController extends Zend_Controller_Action
{
    public function indexAction()
    {
        $fields = array();
        $fields['title'] = $this->_request->getParam('title', null);

        // Allow search on partial string. If it contains a % assume a LIKE search
        if (false !== strpos($fields['title'], '%')) {
            $fields['title'] = array('condition'=>'LIKE', 'value'=>$fields['title']);
        }

        $order = array('title'=>'asc');

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('job-config')->jobs->itemCountPerPage);

        $mapper = Job_Model_Location::getMapper();
        $this->view->locations = $mapper->findAllByField($fields, $order, $page);
    }


    /**
     * Create a new Location. Display form on GET. Create on POST.
     */
    public function createAction()
    {
        $form = new Job_Form_Location();
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $location = new Job_Model_Location();
                $mapper = Job_Model_Location::getMapper();
                $location->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($location);

                    $this->view->getHelper('DisplayMessages')->addMessage('jobLocationSaved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $location);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Display a Job Location
     */
    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Job_Model_Location::getMapper();
            if (null !== ($location = $mapper->find($id))) {
                $this->view->location = $location;
            } else {
                throw new Mcmr_Exception_PageNotFound('Location Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a Job Location. Display form on GET. Save on POST
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Job_Form_Location();
            $this->view->form = $form;

            $mapper = Job_Model_Location::getMapper();
            if (null !== ($location = $mapper->find($id))) {
                $this->view->location = $location;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $location->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($location);

                            $this->view->getHelper('DisplayMessages')->addMessage('jobLocationSaved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $location);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($location->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Location Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a Job Location. Display confirmation on GET. Delete on POST.
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Job_Model_Location::getMapper();
            if (null !== ($location = $mapper->find($id))) {
                $this->view->location = $location;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($location);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Job Location Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $location);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Location Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
}