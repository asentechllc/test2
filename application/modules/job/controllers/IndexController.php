<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Job
 * @package Job_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2420 2011-05-10 16:19:55Z michal $
 */

/**
 * Index Controller for the Job module
 *
 * @category Job
 * @package Job_IndexController
 */
class Job_IndexController extends Mcmr_Controller_Action
{
    
    public function init()
    {
        parent::init();
        $contextSwitch = $this->_helper->getHelper('contextSwitch');
        $contextSwitch
            ->addContext(
                'rss', 
                array(
                    'headers'=>array('Content-Type'=>'application/rss+xml'), 
                    'suffix'=>'rss'
                )
            )
            ->addActionContext('index', 'rss')->initContext();
        
        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext
            ->setAutoJsonSerialization(false)
            ->addActionContext('register', 'html')
            ->addActionContext('register', 'json')
            ->initContext();
    }

    protected function _processIndexConditions(&$conditions) 
    {
        parent::_processIndexConditions($conditions);
        $this->_setRequestParams($conditions, array('categoryid', 'locationid', 'current'));
        $this->_setCompanyTitleCondition($conditions);
    }

    protected function _setCompanyTitleCondition(&$conditions) 
    {
        if ($lcompanytitle=$this->_request->getParam('lcompanytitle')) {
            $conditions['attr_companytitle'] = array('condition'=>'LIKE', 'value'=>"%{$lcompanytitle}%");
        } elseif ($companytitle=$this->_request->getParam('companytitle')) {
            if (false === strpos($companytitle, '%')) {
                $conditions['attr_companytitle'] = $companytitle;
            } else {
                $conditions['attr_companytitle'] = array('condition'=>'LIKE', 'value'=>$companytitle);
            }
        }
    }

    
    /**
     * Create a new Job. Display for on GET, save on POST
     */
    public function createAction()
    {
        $form = $this->_helper->loadForm('Job');
        $this->view->form = $form;
        
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $this->_model = new Job_Model_Job();
                $this->_model->setOptions($form->getValues(true));
                
                try {
                    $this->_mapper->save($this->_model);
                    
                    $this->view->model = $this->_model;
                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')
                        ->addMessage('jobSaved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $this->_model);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')
                        ->addMessage($e->getMessage(), 'error');
                    throw $e;
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')
                    ->addMessage('invalidFormValues', 'warn');
            }
        }
    }
    
    protected function _readAfterFind()
    {
        $pageName = $this->_request->getParam('pageName', null);
        if (null===$pageName) {
            $pageName = $this->_request->getParam('page', '');
        }
        if (null!==$pageName) {
            $this->_helper->viewRenderer('read/'.$pageName);
        }
    }

    
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            if (null !== ($this->_model = $this->_mapper->find($id))) {
                $form = new Job_Form_Job();
                $form->populate($this->_model->getOptions(true));
                $this->view->form = $form;
                $this->view->model = $this->_model;
                
                if ($this->_request->isPost()) {
                    // Merge the existing data with the form values. This allows for partial updates.
                    $post = $this->_request->getPost();
                    $modelvalues = $this->_model->getOptions(true);
                    $post['jobformjob'] = array_merge(
                        $modelvalues, $post['jobformjob']
                    );
                    
                    if ($form->isValid($post)) {
                        $values = $form->getValues(true);
                        $this->_model->setOptions($values);
                        
                        try {
                            $this->_mapper->save($this->_model);
                            
                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')
                                ->addMessage('jobSaved', 'info');
                            $this->view->getHelper('Redirect')
                                ->notify('update', $this->_model);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')
                                ->addMessage($e->getMessage(), 'error');
                        }
                    
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')
                            ->addMessage('invalidFormValues', 'warn');
                    }
                
                }
            
            } else {
                throw new Mcmr_Exception_PageNotFound('Job Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
    
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            if (null !== ($this->_model = $this->_mapper->find($id))) {
                $this->view->model = $this->_model;
                
                if ($this->_request->isPost()) {
                    try {
                        $this->_mapper->delete($this->_model);
                        $this->view->model = null;
                        
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')
                            ->addMessage('jobDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify(
                            'delete', $this->_model
                        );
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')
                            ->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Job Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
    
    public function featuredAction()
    {
        $this->view->models = $this->_mapper->findAllByField(
            array('featured'=>'1'), array('order'=>'asc')
        );
        
        $form = $this->_helper->loadForm('Featured');
        $this->view->form = $form;
        
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $this->_mapper->resetFeatured();
                if (is_array($values['jobs'])) {
                    foreach ($values['jobs'] as $order => $id) {
                        $this->_model = $this->_mapper->find($id);
                        $this->_model->setFeatured(true);
                        $this->_model->setOrder($order);
                        try {
                            $this->_mapper->save($this->_model);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')
                                ->addMessage($e->getMessage(), 'error');
                        }
                    }
                }
                
                $this->view->getHelper('DisplayMessages')
                    ->addMessage('featuredJobsSet', 'info');
            } else {
                $this->view->getHelper('DisplayMessages')
                    ->addMessage('invalidFormValues', 'warn');
            
            }
        }
    }
    
    public function clearfeaturedAction()
    {
        $this->view->models = $this->_mapper->findAllByField(
            array('featured'=>'1'), array('order'=>'asc')
        );
        
        if ($this->_request->isPost()) {
            $this->_mapper->resetFeatured();
            $this->view->getHelper('DisplayMessages')
                ->addMessage('featuredJobsSet', 'info');
        }
    }
}
