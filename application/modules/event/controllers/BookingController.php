<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_BookingController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Booking Controller for the Event module.
 *
 * @category Event
 * @package Event_BookingController
 */
class Event_BookingController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('ContextSwitch');
            $contextSwitch->addContext('html', array())
                ->addContext(
                    'csv',
                    array(
                        'headers' => array('Content-Type' => 'text/csv'),
                        'suffix' => 'csv',
                    )
                );

            $contextSwitch->addActionContext('index', 'csv')
                    ->initContext();
            $this->_redirector = $this->_helper->getHelper('Redirector');
        } catch (Exception $e) {
            
        }
    }

    /**
     * Get a list of booking
     */
    public function indexAction()
    {
        $mapper = Event_Model_Booking::getMapper();
        $fields = array();

        $order = array('active' => 'desc', 'date' => 'asc');

        //if csv is requested we don't do pagination
        if ('csv' != $this->_getParam('format')) {
            $count = isset(Zend_Registry::get('event-config')->events->itemCountPerPage)
                    ? Zend_Registry::get('event-config')->events->itemCountPerPage:10;
            $count = $this->_getParam('count', $count);
            $page = array(
                    'page' => $this->_getParam('page', 1),
                    'count' => $count,
            );
        } else {
            $page = array('page' => 1, 'count' => 10000000);
        }
        $eventIds = null;
        if (null !== ($eventId = $this->_request->getParam('eventid', null))) {
            $eventIds = array($eventId);
        } elseif (null !== ($locationid = $this->_request->getParam('locationid', null) )) {
            $eventMapper = Event_Model_Event::getMapper();
            $events = $eventMapper->findAllByField(array('locationid' => $locationid));
            $eventIds = array();
            foreach ($events as $event) {
                $eventIds[] = $event->getId();
            }
            // get all books for these event ids.
        }
        if (null !== $eventIds && count($eventIds)) {
            $fields = array('event_id' => array('condition' => 'IN', 'value' => $eventIds));
        } else {
            $fields = array();
        }
        $this->view->bookings = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Get a particular booking details
     */
    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Booking::getMapper();
            if (null !== ($booking = $mapper->find($id))) {
                $this->view->booking = $booking;
            } else {
                throw new Mcmr_Exception_PageNotFound('Booking does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Book a place for an event
     */
    public function createAction()
    {
        if (null !== ($eventId = $this->_request->getParam('eventid', null))) {
            // check if there space available for new booking
            $eventMapper = Event_Model_Event::getMapper();
            $event = $eventMapper->find($eventId);
            $spaces = $event->getSpaces();
            $numberofattendees = $event->getNoOfAttendees();

            if ($spaces && $numberofattendees >= $spaces) {
                $this->view->getHelper('DisplayMessages')->addMessage('This event has been fully booked', 'warn');
                $this->_redirector->gotoSimple('read', 'index', 'event', array('id' => $eventId));
            } else {
                $form = new Event_Form_Booking();
                $element = $form->getElement('eventid');
                if (null !== $element) {
                    $element->setValue($eventId);
                }
                        
                $this->view->event = $event;
                $this->view->form = $form;
                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $values['event_id'] = $eventId;

                        try {
                            $mapper = Event_Model_Booking::getMapper();
                            $booking = $mapper->findOneByField(
                                array('email' => $form->getValue('email'), 'event_id' => $eventId)
                            );
                            $booked = false;
                            if (null === $booking) {
                                $booking = new Event_Model_Booking();
                                $booking->setOptions($values);
                                $mapper->save($booking);
                                $booked = true;
                            }
                            
                            if ($booked) {
                                $this->view->error = false;

                                $session = new Zend_Session_Namespace('gossip');
                                $session->bookingid = $booking->getId();
                                $this->view->getHelper('DisplayMessages')->addMessage('bookingSaved', 'info');
                                $this->view->getHelper('Redirect')->notify('create', $booking);
                            } else {
                                $this->view->getHelper('DisplayMessages')
                                    ->addMessage('You have already booked this event.', 'warn');
                            }
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')
                                ->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('invalidBookingValues', 'warn');
                    }
                }
            }
        } else {
            $this->_forward('index', 'index', 'event');
        }
    }

    /**
     * Update booking details
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Booking::getMapper();
            if (null !== ($booking = $mapper->find($id))) {
                $form = new Event_Form_Booking();
                $form->populate($booking->getOptions(true));

                $this->view->booking = $booking;
                $this->view->form = $form;

                if ($this->_request->isPost()) {
                    // Merge the existing booking information to allow partial post updates
                    $post = $this->_request->getPost();
                    $bookingvalues = $booking->getOptions(true);
                    $post['eventformbooking'] = array_merge($bookingvalues, $post['eventformbooking']);

                    if ($form->isValid($post)) {
                        $booking->setOptions($form->getValues(true));

                        try {
                            $mapper->save($booking);

                            $this->view->getHelper('DisplayMessages')->addMessage('Booking Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $booking);
                        } catch (Exception $e) {
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'warn');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Booking does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Cancel an booking
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Booking::getMapper();
            if (null !== ($booking = $mapper->find($id))) {
                $this->view->booking = $booking;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($booking);
                        $this->view->error = false;

                        $this->view->getHelper('DisplayMessages')->addMessage('Booking Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $booking);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Booking does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Booking confirmation action
     */
    public function confirmationAction()
    {
        // get booking id from sessio pool and reset the session data to null.
        $session = new Zend_Session_Namespace('gossip');
        $bookingid = $session->bookingid;
        $session->bookingid = null;
        if (null !== $bookingid) {
            $mapper = Event_Model_Booking::getMapper();
            if (null !== ($booking = $mapper->find($bookingid))) {
                $eventId = $booking->getEventid();
                $eventMapper = Event_Model_Event::getMapper();
                $event = $eventMapper->find($eventId);

                $this->view->booking = $booking;
                $this->view->event = $event;
            } else {
                throw new Mcmr_Exception_PageNotFound('Booking does not exist');
            }
        } else {
            $this->_redirect('/');
        }
    }

    /**
     * Find a booking by email and token if they want to cancel by they  lost the cancellation link.
     */
    public function findAction()
    {
        $form = new Event_Form_BookingFind();
        $this->view->form = $form;
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $email = $form->getValue('email');
                $token = $form->getValue('token');
                $mapper = Event_Model_Booking::getMapper();
                $booking = $mapper->findOneByField(array('email' => $email, 'token' => $token, 'active' => 1));
                if (null !== $booking) {
                    $this->_redirector->gotoSimple(
                        "cancel", 'booking', 'event', 
                        array('email' => $email, 'token' => $token)
                    );
                } else {
                    $this->view->getHelper('DisplayMessages')->addMessage('Booking not found', 'warn');
                }
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid form values', 'warn');
            }
        }
    }

    /**
     * Cancel a booking  
     */
    public function cancelAction()
    {
        $token = $this->_request->getParam('token', '');
        $email = $this->_request->getParam('email', '');
        $mapper = Event_Model_Booking::getMapper();
        $booking = $mapper->findOneByField(array('email' => $email, 'token' => $token, 'active' => 1));
        if (null !== $booking) {
            $form = new Event_Form_BookingCancel();

            $this->view->booking = $booking;
            $this->view->form = $form;

            if ($this->_request->isPost()) {
                if ($form->isValid($this->_request->getPost())) {
                    $booking->setActive(0);
                    $mapper->save($booking);
                    $this->view->getHelper('Redirect')->notify('cancel', $booking);
                } else {
                    $this->view->getHelper('DisplayMessages')->addMessage('Invalid form values', 'warn');
                }
            }
        } else {
            throw new Mcmr_Exception_PageNotFound('Booking does not exist');
        }
    }

}
