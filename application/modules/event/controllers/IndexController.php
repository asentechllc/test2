<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Index Controller for the Event module.
 *
 * @category Event
 * @package Event_IndexController
 */
class Event_IndexController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('ContextSwitch');
            $contextSwitch->addContext('html', array())
                ->addContext(
                    'ics',
                    array(
                        'headers' => array('Content-Type' => 'text/calendar'),
                        'suffix' => 'ics',
                    )
                );

            $contextSwitch
                ->addActionContext('read', 'ics')
                ->initContext();

            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                ->addActionContext('index', 'json')
                ->addActionContext('index', 'html')
                ->addActionContext('create', 'html')
                ->addActionContext('create', 'json')
                ->addActionContext('update', 'html')
                ->addActionContext('update', 'json')
                ->addActionContext('delete', 'html')
                ->addActionContext('delete', 'json')
                ->addActionContext('read', 'html')
                ->addActionContext('read', 'json')
                ->initContext();
        } catch (Exception $e) {
        }
    }

    /**
     * Get a list of events
     */
    public function indexAction()
    {
        $fields = array();
        if (( $location = $this->_request->getParam('location') ) !== null) {
            $fields['location'] = $location;
        }
        $fields['published'] = $this->_request->getParam('published');

        if ((null !== $this->_request->getParam('fromdate')) && (null !== $this->_request->getParam('todate'))) {
            if (preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})/', $this->_request->getParam('fromdate'), $fromdate)) {
                $from = mktime(0, 0, 0, $fromdate[2], $fromdate[1], $fromdate[3]);
            }

            if (preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d{2,4})/', $this->_request->getParam('fromdate'), $todate)) {
                $to = mktime(23, 59, 59, $todate[2], $todate[1], $todate[3]);
            }

            if (isset($from) && isset($to))
                $fields['startdate'] = array('condition' => 'between', 'value' => array($from, $to));
        }
        if (null !== ($typeUrl = $this->_request->getParam('type'))) {
            $type = Event_Model_Type::getMapper()->findOneByField(array('url'=>$typeUrl));
            if (null !== $type) {
                $fields['typeid'] = $type->getId();
            }
        }
        
        if ($this->_request->getParam('current')) {
            $fields['enddate'] = array('value' => time(), 'condition'=>'>=');
        }

        if (null == ($order = $this->_request->getParam('order'))) {
            $order = array('startdate' => 'desc');
        }

        $page = array();
        $page['page'] = $this->_getParam('page', 1);
        $count = isset(Zend_Registry::get('event-config')->events->itemCountPerPage)
                ?Zend_Registry::get('event-config')->events->itemCountPerPage:10;
        $page['count'] = $this->_getParam('count', $count);

        $mapper = Event_Model_Event::getMapper();
        $this->view->events = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Get a particular event detail
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        
        if (null !== $id || null !== $url) {
            $mapper = Event_Model_Event::getMapper();
            // Get the event from the ID or the URL.
            $event = (null !== $id) ? $mapper->find($id)
                 : $mapper->findOneByField(array('url'=>$url));
            if (null !== $event) {
                if (is_object($event->getType())) {
                    $this->_helper->viewRenderer('read/'.$event->getType()->getUrl());
                }
                
                $this->view->event = $event;
            } else {
                throw new Mcmr_Exception_PageNotFound('Event does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Add a new event
     */
    public function createAction()
    {
        $type = $this->_request->getParam('type', Zend_Registry::get('event-config')->defaultType);
        $typeMapper = Event_Model_Type::getMapper();
        
        $typeModel = $typeMapper->findOneByField(array('url'=>$type));
        if (null == $typeModel) {
            throw new Mcmr_Exception_PageNotFound("Event type '{$type}' not found");
        }
        
        $form = $this->_helper->loadForm('Event', array('type'=>$typeModel->getUrl()));
        $this->view->form = $form;
        $this->view->isSave = false;
        if ($this->_request->isPost()) {
            $this->view->isSave = true;
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $event = new Event_Model_Event();
                $mapper = Event_Model_Event::getMapper();
                $event->setOptions($values);
                $event->setTypeid($typeModel->getId());
                
                $this->view->event = $event;
                try {
                    $mapper->save($event);
                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('eventSaved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $event);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidEventValues', 'warn');
            }
        }
    }

    /**
     * Update event details
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Event_Form_Event();
            $this->view->form = $form;

            $mapper = Event_Model_Event::getMapper();
            if (null !== ($event = $mapper->find($id))) {
                $this->view->event = $event;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $event->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($event);

                            $this->view->getHelper('DisplayMessages')->addMessage('Event Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $event);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($event->getOptions(true));
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Event does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Cancel an event
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Event::getMapper();
            if (null !== ($event = $mapper->find($id))) {
                $this->view->event = $event;
                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($event);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Event Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $event);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Event does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * An action to register a user against the event
     */
    public function registerAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Event::getMapper();
            if (null !== ($event = $mapper->find($id))) {
                $this->view->event = $event;
                if ($this->_request->isPost()) {
                    try {
                        $user = Zend_Registry::get('authenticated-user');
                        $event->register($user);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Event does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }
    
    /**
     * Modify the event index display order. Display form on GET. Save on POST
     */
    public function orderAction()
    {
        $type = $this->_request->getParam('type', Zend_Registry::get('event-config')->order->default);

        $form = $this->_helper->loadForm('Order', array('type'=>$type ));
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $mapper = Event_Model_Event::getMapper();

                // Reset the ordering before resaving the order
                try {
                    $mapper->resetOrdering($type);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
                
                if ('chronological' == $values['orderby']) {
                    $this->view->getHelper('DisplayMessages')->addMessage('Ordering Reset to Chronological', 'info');
                } else {
                    foreach ($values['events'] as $order=>$id) {
                        $event = $mapper->find($id);
                        if (null !== $event) {
                            $event->setOrder(1000-$order, $type); // Done in reverse to accomodate null values
                            try {
                                $mapper->save($event);
                            } catch (Exception $e) {
                                $this->view->error = true;
                                $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                            }
                        }
                    }
                    $this->view->getHelper('DisplayMessages')->addMessage('Event Ordering set to Specific', 'info');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Form values were invalid', 'warn');
            }
        }
    }
    
}