<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_ProgramController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Program Controller for the Event module.
 *
 * @category Event
 * @package Event_ProgramController
 */
class Event_ProgramController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('ContextSwitch');
            $contextSwitch->addContext('html', array())
                ->addContext(
                    'ics', array(
                        'headers' => array('Content-Type' => 'text/calendar'),
                        'suffix' => 'ics',
                    )
                );

            $contextSwitch
                ->addActionContext('read', 'ics')
                ->initContext();
            
            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                ->addActionContext('index', 'json')
                ->addActionContext('index', 'html')
                ->addActionContext('create', 'html')
                ->addActionContext('create', 'json')
                ->addActionContext('update', 'html')
                ->addActionContext('update', 'json')
                ->addActionContext('delete', 'html')
                ->addActionContext('delete', 'json')
                ->addActionContext('read', 'html')
                ->addActionContext('read', 'json')
                ->initContext();
            
        } catch (Exception $e) {
        }
    }

    /**
     * Get a list of programs
     */
    public function indexAction()
    {
        if (null !== ($eventId = $this->_request->getParam('eventid', null))) {
            $event = Event_Model_Event::getMapper()->find($eventId);
            if (null === $event) {
                $this->_forward('index', 'index', 'event');
            }

            $fields = array();
            $fields['event_id'] = $eventId;

            $order = array('startdate' => 'asc');

            $page = array();
            $page['page'] = $this->_getParam('page', 1);
            $count = isset(Zend_Registry::get('event-config')->events->itemCountPerPage)
                    ?Zend_Registry::get('event-config')->events->itemCountPerPage:10;
            $page['count'] = $this->_getParam('count', $count);

            $mapper = Event_Model_Program::getMapper();
            $this->view->programs = $mapper->findAllByField($fields, $order, $page);
            $this->view->event = $event;
        } else {
            $this->_forward('index', 'index', 'event');
        }
    }

    /**
     * Get a particular program detail
     */
    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Program::getMapper();
            if (null !== ($program = $mapper->find($id))) {
                $this->view->program = $program;
            } else {
                throw new Mcmr_Exception_PageNotFound('Event Program does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Add a new program
     */
    public function createAction()
    {
        if (null !== ($eventId = $this->_request->getParam('eventid', null))) {
            $form = new Event_Form_Program();
            $eventMapper = Event_Model_Event::getMapper();
            $this->view->form = $form;
            $this->view->event = $eventMapper->find($eventId);

            if ($this->_request->isPost()) {
                if ($form->isValid($this->_request->getPost())) {
                    $values = $form->getValues(true);
                    $values['event_id'] = $eventId;

                    $program = new Event_Model_Program();
                    $mapper = Event_Model_Program::getMapper();
                    $program->setOptions($values);
                    try {
                        $this->view->error = false;
                        $mapper->save($program);

                        $this->view->getHelper('DisplayMessages')->addMessage('Event Program Saved', 'info');
                        $this->view->getHelper('Redirect')->notify('create', $program);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            }
        } else {
            $this->_forward('index', 'index', 'event');
        }
    }

    /**
     * Update program details
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Event_Form_Program();
            $this->view->form = $form;

            $mapper = Event_Model_Program::getMapper();
            if (null !== ($program = $mapper->find($id))) {
                $this->view->program = $program;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $program->setOptions($values);
                        try {
                            $this->view->error = false;
                            $mapper->save($program);

                            $this->view->getHelper('DisplayMessages')->addMessage('Event Program Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $program);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($program->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Event program does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a program
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Program::getMapper();
            if (null !== ($program = $mapper->find($id))) {
                $this->view->program = $program;
                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($program);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Event Program Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $program);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Event Program Does Not Exist');
            }
        } else {
            $this->_forward('index');
        }
    }

}