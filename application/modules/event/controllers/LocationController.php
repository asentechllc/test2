<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_LocationController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Location Controller for the Event module.
 *
 * @category Event
 * @package Event_LocationController
 */
class Event_LocationController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                    ->addActionContext('index', 'json')
                    ->addActionContext('index', 'html')
                    ->addActionContext('create', 'html')
                    ->addActionContext('create', 'json')
                    ->addActionContext('update', 'html')
                    ->addActionContext('update', 'json')
                    ->addActionContext('delete', 'html')
                    ->addActionContext('delete', 'json')
                    ->addActionContext('read', 'html')
                    ->addActionContext('read', 'json')
                    ->initContext();
        } catch (Exception $e) {
            
        }
    }

    /**
     * Get a list of locations
     */
    public function indexAction()
    {
        $mapper = Event_Model_Location::getMapper();
        $this->view->locations = $mapper->findAllByField();
    }

    /**
     * Get a particular location detail
     */
    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Location::getMapper();
            if (null !== ($location = $mapper->find($id))) {
                $this->view->location = $location;
            } else {
                throw new Mcmr_Exception_PageNotFound('Event Location does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Add a new location
     */
    public function createAction()
    {
        $form = new Event_Form_Location();
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $location = new Event_Model_Location();
                $mapper = Event_Model_Location::getMapper();
                $location->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($location);
                    $this->view->location = $location;
                    $this->view->getHelper('DisplayMessages')->addMessage('Event Location Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $location);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Update location details
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Event_Form_Location();
            $this->view->form = $form;

            $mapper = Event_Model_Location::getMapper();
            if (null !== ($location = $mapper->find($id))) {
                $this->view->location = $location;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $location->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($location);

                            $this->view->getHelper('DisplayMessages')->addMessage('Event Location Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $location);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($location->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Event Location does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a location
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Event_Model_Location::getMapper();
            if (null !== ($location = $mapper->find($id))) {
                $this->view->location = $location;
                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($location);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Event Location Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $location);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Event Location does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

}