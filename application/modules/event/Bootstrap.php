<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php $
 */

/**
 * Bootstrap
 *
 * @category Event
 * @package Event_Bootstrap
 */
class Event_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');

        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Event_View_Helper');
    }
}
