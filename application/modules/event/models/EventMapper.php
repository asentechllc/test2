<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for event models
 *
 * @category   Event
 * @package    Model
 * @subpackage EventMapper
 */
class Event_Model_EventMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Event_Model_DbTable_Event';
    protected $_dbAttrTableClass = 'Event_Model_DbTable_EventAttr';
    protected $_dbOrdrTableClass = 'Event_Model_DbTable_EventOrdr';
    protected $_columnPrefix = 'event_';
    protected $_modelClass = 'Event_Model_Event';
    protected $_cacheIdPrefix = 'Event';

    public function __construct()
    {
        parent::__construct();

        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
    }
    
    /**
     * Convert a readable fieldname into the database fieldname
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'location':
            case 'locationid':
            case 'location_id':
                $field = 'location_id';
                break;

            case 'type':
            case 'typeid':
            case 'type_id':
                $field = 'type_id';
                break;
            
            default:
                $field = $this->_columnPrefix . $field;
                break;
        }

        return $field;
    }

    /**
     * Convert field value before inserting into databse query. 
     * 
     * @param string $field field name passed to be able to tell which filed you are dealing with.
     * @param mixed $value field value as stored in  the object. Process if necessary and return.
     * @return Zend_Db_Expr|string|int field value in the format suitable for a database query. 
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'event_startdate':
            case 'event_enddate':
            case 'event_publishdate':
                if (is_int($value))
                    $value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
                elseif (is_array($value)) {
                    foreach ($value as $key => $item) {
                        if (is_int($item)) {
                            $value[$key] = new Zend_Db_Expr("FROM_UNIXTIME({$item})");
                        }
                    }
                }
                break;
        }

        return $value;
    }

    /**
     * Create a list of fields to be read form the databse in SELECT query.
     *
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(event_startdate) AS event_startdate";
        $fields[] = "UNIX_TIMESTAMP(event_enddate) AS event_enddate";
        $fields[] = "UNIX_TIMESTAMP(event_publishdate) AS event_publishdate";
        return $fields;
    }
}