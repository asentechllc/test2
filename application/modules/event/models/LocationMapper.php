<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for event location models
 *
 * @category   Event
 * @package    Model
 * @subpackage LocationMapper
 */
class Event_Model_LocationMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Event_Model_DbTable_Location';
    protected $_columnPrefix = 'location_';
    protected $_modelClass = 'Event_Model_Location';
    protected $_cacheIdPrefix = 'Location';
}