<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all events
 *
 * @category   Event
 * @package    Model
 * @subpackage Event
 */
class Event_Model_Event extends Mcmr_Model_Ordered
{
    static protected $_mapperclass = 'Event_Model_EventMapper';
    protected $_id = null;
    protected $_typeid = null;
    protected $_url = null;
    protected $_state = null;
    protected $_locationid = null;
    protected $_title = null;
    protected $_description = null;
    protected $_venue = null;
    protected $_address1 = null;
    protected $_address2 = null;
    protected $_town = null;
    protected $_county = null;
    protected $_postcode = null;
    protected $_spaces = null;
    protected $_published = null;
    protected $_publishdate = null;
    protected $_startdate = null;
    protected $_enddate = null;
    protected $_image = null;
    
    private $_location = null;
    private $_numberofattendees = null;
    private $_soldout = null;
    private $_programs = null;
    private $_bookings = null;
    private $_urlchanged = false;

    /**
     * Return mapper for model
     *
     * @return Event_Model_EventMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Set Event ID
     *
     * @param int $id
     * @return Event_Model_Event
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }
    
    public function getTypeid()
    {
        return $this->_typeid;
    }

    public function setTypeid($typeid)
    {
        $this->_typeid = $typeid;
        
        return $this;
    }
    
    /**
     * Return Event ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Return the URL ID for the Job
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }
        
        return $this->_url;
    }

    /**
     * Set the URL ID for the Job
     *
     * @param string $url
     * @return Job_Model_Job
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }
    
    /**
     * Get the event state
     *
     * @return string 
     */
    public function getState()
    {
        if (null === $this->_state) {
            $this->_state = 'active';
        }
        
        return $this->_state;
    }

    /**
     * Return the event state
     *
     * @param string $state
     * @return Event_Model_Event 
     */
    public function setState($state)
    {
        $this->_state = (string)$state;
        
        return $this;
    }

        
    /**
     * Set event locationid
     * 
     * @param int $locationid
     * @return Event_Model_Event
     */
    public function setLocationid($locationid)
    {
        $this->_locationid = $locationid;

        return $this;
    }

    /**
     * Return event locatonid
     * 
     * @return int
     */
    public function getLocationid()
    {
        if (null === $this->_locationid) {
            $this->_locationid = 0;
        }
        
        return $this->_locationid;
    }

    /**
     * Set event title
     * 
     * @param string $title
     * @return Event_Model_Event
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Return event title
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set event description
     * 
     * @param string $desc
     * @return Event_Model_Event
     */
    public function setDescription($desc)
    {
        $this->_description = $desc;

        return $this;
    }

    /**
     * Return event description
     * 
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set event venue
     * 
     * @param string $venue
     * @return Event_Model_Event
     */
    public function setVenue($venue)
    {
        $this->_venue = $venue;

        return $this;
    }

    /**
     * Return event venue
     * 
     * @return string
     */
    public function getVenue()
    {
        return $this->_venue;
    }

    /**
     * Set event address1
     * 
     * @param string $addr1
     * @return Event_Model_Event
     */
    public function setAddress1($addr)
    {
        $this->_address1 = $addr;

        return $this;
    }

    /**
     * Return event address1
     * 
     * @return string
     */
    public function getAddress1()
    {
        return $this->_address1;
    }

    /**
     * Set event address2
     * 
     * @param string $addr2
     * @return Event_Model_Event
     */
    public function setAddress2($addr)
    {
        $this->_address2 = $addr;

        return $this;
    }

    /**
     * Return event address2
     * 
     * @return string
     */
    public function getAddress2()
    {
        return $this->_address2;
    }

    /**
     * Set event town
     * 
     * @param string $town
     * @return Event_Model_Event
     */
    public function setTown($town)
    {
        $this->_town = $town;

        return $this;
    }

    /**
     * Return event town
     * 
     * @return string
     */
    public function getTown()
    {
        return $this->_town;
    }

    /**
     * Set event county
     * 
     * @param string $county
     * @return Event_Model_Event
     */
    public function setCounty($county)
    {
        $this->_county = $county;

        return $this;
    }

    /**
     * Return event county
     * 
     * @return string
     */
    public function getCounty()
    {
        return $this->_county;
    }

    /**
     * Set event postcode
     * 
     * @param string $postcode
     * @return Event_Model_Event
     */
    public function setPostcode($postcode)
    {
        $this->_postcode = $postcode;

        return $this;
    }

    /**
     * Return event postcode
     * 
     * @return string
     */
    public function getPostcode()
    {
        return $this->_postcode;
    }

    /**
     * Set number of spaces for booking for an event
     *
     * @param int $spaces
     * @return Event_Model_Event
     */
    public function setSpaces($spaces)
    {
        $this->_spaces = $spaces;

        return $this;
    }

    /**
     * Get event total spaces for booking
     *
     * @return int
     */
    public function getSpaces()
    {
        return $this->_spaces;
    }

    /**
     * Set event published
     *
     * @param int published
     * @return Event_Model_Event
     */
    public function setPublished($published)
    {
        $this->_published = $published;

        return $this;
    }

    /**
     * Get event published
     *
     * @return int
     */
    public function getPublished()
    {
        return $this->_published;
    }

    /**
     * Set event publishdate as a unix timestamp
     *
     * @param int $publishdate
     * @return Event_Model_Event
     */
    public function setPublishdate($publishdate)
    {
        $this->_publishdate = (int) $publishdate;

        return $this;
    }

    /**
     * Get event publish date unix timestamp
     *
     * @return int
     */
    public function getPublishdate()
    {
        return $this->_publishdate;
    }

    /**
     * Set event datehaschanged
     *
     * @param int $datehaschanged
     * @return Event_Model_Event
     */
    public function setDatehaschanged($datehaschanged)
    {
        $this->_datehaschanged = $datehaschanged;

        return $this;
    }

    /**
     * Get event datehaschanged
     *
     * @return int
     */
    public function getDatehaschanged()
    {
        return $this->_datehaschanged;
    }

    /**
     * Set event start datetime as a unix timestamp
     *
     * @param int $startdate
     * @return Event_Model_Event
     */
    public function setStartdate($startdate)
    {
        $this->_startdate = (int) $startdate;

        return $this;
    }

    /**
     * Get event start datetime as a unix timestamp
     *
     * @return int
     */
    public function getStartdate()
    {
        return $this->_startdate;
    }

    /**
     * Set event end datetime as a unix timestamp
     *
     * @param int $enddate
     * @return Event_Model_Event
     */
    public function setEnddate($enddate)
    {
        $this->_enddate = (int) $enddate;

        return $this;
    }

    /**
     * Get event end datetime as a unix timestamp
     *
     * @return int
     */
    public function getEnddate()
    {
        if (null === $this->_enddate) {
            $this->_enddate = $this->_startdate;
        }

        return $this->_enddate;
    }

    /**
     * Get event location
     * 
     * @return Event_Model_Location
     */
    public function getLocation()
    {
        if (null === $this->_location && $this->getLocationid()) {
            $mapper = Event_Model_Location::getMapper();
            $this->_location = $mapper->find($this->getLocationid());
        }

        return $this->_location;
    }

    /**
     * Get soldout flag. 
     * Changed to a 'computed field'. When requested it will compare the number
     * of place available to the number of active bookings made.
     * 
     * @return bool
     */
    public function getSoldout()
    {
        if (null === $this->_soldout) {
            $attendees = $this->getNoOfAttendees();
            if ($attendees >= $this->getSpaces()) {
                $this->_soldout = true;
            } else {
                $this->_soldout = false;
            }
        }
        return $this->_soldout;
    }

    /**
     * Get programs for an event
     * 
     * @return Event_Model_Program
     */
    public function getPrograms()
    {
        if (null === $this->_programs) {
            $this->_programs = Event_Model_Program::getMapper()
                ->findAllByField(array('event_id' => $this->getId()), array('starttime' => 'asc'));
        }

        return $this->_programs;
    }

    /**
     * Get bookings for an event
     * 
     * @return Event_Model_Booking
     */
    public function getBookings()
    {
        if (null === $this->_bookings) {
            $this->_bookings = Event_Model_Booking::getMapper()
                ->findAllByField(array('eventid' => $this->getId()));
        }

        return $this->_bookings;
    }

    /**
     * Get number of attendees for the event.
     *
     * @return int
     */
    public function getNoOfAttendees()
    {
        if (null === $this->_numberofattendees) {
            $this->_numberofattendees = Event_Model_Booking::getMapper()
                ->countByField(array('event_id' => $this->getId(), 'active' => 1));
        }

        return $this->_numberofattendees;
    }
    
    /**
     *
     * @return string
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     *
     * @param string $image
     * @return Event_Model_Event 
     */
    public function setImage($image)
    {
        $this->_image = $image;
        
        return $this;
    }

    public function getType()
    {
        $mapper = Event_Model_Type::getMapper();
        return $mapper->find($this->getTypeid());
    }
    
    /**
     * Register the user with this event
     *
     * @param int|User_Model_User $user 
     */
    public function register($user)
    {
        if (is_int($user)) {
            $user = User_Model_User::getMapper()->find($user);
        }
        
        if (!is_object($user) || !$user instanceof User_Model_User) {
            throw new Zend_Exception("User provided is not a user model or a valid user ID");
        }
        
        $userId = $user->getId();
        $booking = Event_Model_Booking::getMapper()->findOneByField(array('eventid'=>$this->getId(), 'userid'=>$user->getId()));
        if (null !== $booking) {
            throw new Zend_Exception("User already booked into this event");
        }
        
        $booking = new Event_Model_Booking();
        $booking->setUserid($user->getId())
                ->setEventid($this->getId());
        Event_Model_Booking::getMapper()->save($booking);
    }

    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }
}