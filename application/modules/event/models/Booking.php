<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all events
 *
 * @category   Event
 * @package    Model
 * @subpackage Booking
 */
class Event_Model_Booking extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Event_Model_BookingMapper';
    
    protected $_id = null;
    protected $_eventid = null;
    protected $_userid = null;
    protected $_date = null;
    protected $_email = null;
    protected $_token = null;
    
    private $_user = null;
    private $_event = null;

    /**
     * Return mapper for model
     *
     * @return Event_Model_BookingBookingMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Set Booking ID
     *
     * @param int $id
     * @return Event_Model_Booking
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Return Booking ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set userid
     *
     * @param int $userid
     * @return Event_Model_Booking
     */
    public function setUserid($userid)
    {
        $this->_userid = $userid;
        $this->_user = null;
        return $this;
    }

    /**
     * Get userid
     *
     * @return int
     */
    public function getUserid()
    {
        if (null === $this->_userid) {
            $this->_userid = Zend_Registry::get('authenticated-user')->getId();
        }

        return $this->_userid;
    }

    /**
     * Set event id
     *
     * @param int $eventid
     * @return Event_Model_Booking
     */
    public function setEventid($eventid)
    {
        $this->_eventid = $eventid;
        $this->_event = null;
        return $this;
    }

    /**
     * Get event id
     *
     * @return int
     */
    public function getEventid()
    {
        return $this->_eventid;
    }

    /**
     * Return Booking date unix timestamp
     *
     * @return int
     */
    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = time();
        }

        return $this->_date;
    }

    /**
     * Set Booking Date as a unix timestamp
     *
     * @param int $date
     * @return Event_Model_Booking
     */
    public function setDate($date)
    {
        $this->_date = (int) $date;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getEmail()
    {
        if (null === $this->_email) {
            $user = $this->getUser();
            if (null !== $user) {
                $this->_email = $user->getEmail();
            }
        }
        
        return $this->_email;
    }

    /**
     * Set the email address for this booking
     *
     * @param string $email
     * @throws Mcmr_Model_Exception
     * @return Event_Model_Booking 
     */
    public function setEmail($email)
    {
        $validate = new Zend_Validate_EmailAddress();
        if ($validate->isValid($email)) {
            $this->_email = $email;
        } else {
            throw new Mcmr_Model_Exception("Invalid email address '{$email}'");
        }
        
        return $this;
    }

        
    /**
     * Get a unique pass token. Used to access a booking without the user being specifically logged in
     *
     * @return string
     */
    public function getToken()
    {
        if (null === $this->_token) {
            $this->_token = Mcmr_StdLib::randomString(12, 16);
        }
        
        return $this->_token;
    }

    /**
     * Set the booking token
     *
     * @param string $token
     * @return Event_Model_Booking 
     */
    public function setToken($token)
    {
        $this->_token = $token;
        
        return $this;
    }

    /**
     * Get user details of the speaker
     * 
     * @return User_Model_User
     */
    public function getUser()
    {
        if (null === $this->_user && $this->getUserid()) {
            $this->_user = User_Model_User::getMapper()->find($this->getUserid());
        }

        return $this->_user;
    }

    /**
     * Get event this booking is for.
     * 
     * @return Event_Model_Event
     */
    public function getEvent()
    {
        if (null === $this->_event && $this->getEventid()) {
            $this->_event = Event_Model_Event::getMapper()->find($this->getEventid());
        }

        return $this->_event;
    }
}