<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_Model
 * @copyright  Copyright (c) 2012 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Database table class for the event types table
 *
 * @category Event
 * @package Event_Model
 * @subpackage DbTable
 */
class Event_Model_DbTable_Type extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'event_types';
    protected $_primary = 'type_id';
}
