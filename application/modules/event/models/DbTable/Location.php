<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Class for the location DB table
 *
 * @category   Event
 * @package    Model
 * @subpackage DbTable
 */
class Event_Model_DbTable_Location extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'event_locations';
    protected $_primary = 'location_id';
}