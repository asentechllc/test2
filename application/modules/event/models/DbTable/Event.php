<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Class for the event DB table
 *
 * @category   Event
 * @package    Model
 * @subpackage DbTable
 */
class Event_Model_DbTable_Event extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'event_events';
    protected $_primary = 'event_id';
}