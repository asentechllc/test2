<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all events
 *
 * @category   Event
 * @package    Model
 * @subpackage Location
 */
class Event_Model_Location extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Event_Model_LocationMapper';
    
    protected $_id = null;
    protected $_title = null;

    /**
     * Return mapper for model
     *
     * @return Event_Model_LocationMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Return location ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set location ID
     *
     * @param int $id
     * @return Event_Model_Location
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Set location name
     * 
     * @param string $title
     * @return Event_Model_Location
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Return location name
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

}