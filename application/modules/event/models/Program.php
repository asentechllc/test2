<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all events
 *
 * @category   Event
 * @package    Model
 * @subpackage Program
 */
class Event_Model_Program extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Event_Model_ProgramMapper';
    
    protected $_id = null;
    protected $_eventid = null;
    protected $_title = null;
    protected $_description = null;
    protected $_startdate = null;
    protected $_enddate = null;
    protected $_userid = null;
    private $_user = null;
    private $_event = null;

    /**
     * Return mapper for model
     *
     * @return Event_Model_ProgramMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Return Program ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set Program ID
     *
     * @param int $id
     * @return Event_Model_Program
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Set program title
     * 
     * @param string $title
     * @return Event_Model_Program
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Return program title
     * 
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set program eventid
     * 
     * @param int $eventid
     * @return Event_Model_Program
     */
    public function setEventid($eventid)
    {
        $this->_eventid = $eventid;

        return $this;
    }

    /**
     * Return program eventid
     * 
     * @return int
     */
    public function getEventid()
    {
        return $this->_eventid;
    }

    /**
     * Set the event start datetime as a unix timestamp
     *
     * @param int $startdate
     * @return Event_Model_Program
     */
    public function setStartdate($startdate)
    {
        $this->_startdate = (int) $startdate;

        return $this;
    }

    /**
     * Get event start datetime as a unix timestamp
     *
     * @return int
     */
    public function getStartdate()
    {
        return $this->_startdate;
    }

    /**
     * Set the event end datetime as a unix timestamp
     *
     * @param int $enddate
     * @return Event_Model_Program
     */
    public function setEnddate($enddate)
    {
        $this->_enddate = (int) $enddate;

        return $this;
    }

    /**
     * Get event end datetime as a unix timestamp
     *
     * @return int
     */
    public function getEnddate()
    {
        return $this->_enddate;
    }

    /**
     * Get speaker's userid
     *
     * @return int
     */
    public function getUserid()
    {

        return $this->_userid;
    }

    /**
     * Set speaker's userid
     *
     * @param int $userid
     * @return Event_Model_Program
     */
    public function setUserid($userid)
    {
        $this->_userid = $userid;

        return $this;
    }

    /**
     * Get speaker's user details
     * 
     * @return User_Model_User
     */
    public function getSpeaker()
    {
        if (null === $this->_user && $this->getUserid()) {
            $this->_user = User_Model_User::getMapper()->find($this->getUserid());
        }

        return $this->_user;
    }

    /**
     * Get user details of the speaker
     * 
     * @return User_Model_User
     */
    public function getUser()
    {
        if (null === $this->_user && $this->getUserid()) {
            $this->_user = User_Model_User::getMapper()->find($this->getUserid());
        }

        return $this->_user;
    }

    /**
     * Get event this program is for.
     * 
     * @return Event_Model_Event
     */
    public function getEvent()
    {
        if (null === $this->_event && $this->getEventid()) {
            $this->_event = Event_Model_Event::getMapper()->find($this->getEventid());
        }

        return $this->_event;
    }
}