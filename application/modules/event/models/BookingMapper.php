<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for event models
 *
 * @category   Event
 * @package    Model
 * @subpackage BookingMapper
 */
class Event_Model_BookingMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Event_Model_DbTable_Booking';
    protected $_dbAttrTableClass = 'Event_Model_DbTable_BookingAttr';
    protected $_columnPrefix = 'booking_';
    protected $_modelClass = 'Event_Model_Booking';
    protected $_cacheIdPrefix = 'Booking';

    public function  __construct()
    {
        parent::__construct();

        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
    }
    
    /**
     * Convert a readable fieldname into the database fieldname
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'event':
            case 'eventid':
            case 'event_id':
                $field = 'event_id';
                break;

            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }

    /**
     * Convert field value before inserting into databse query. 
     * 
     * @param string $field field name passed to be able to tell which filed you are dealing with.
     * @param mixed $value field value as stored in  the object. Process if necessary and return.
     * @return Zend_Db_Expr|string|int field value in the format suitable for a database query. 
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'booking_date':
                $value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
                break;
        }

        return $value;
    }
    
    /** 
     * Create a list of fields to be read form the databse in SELECT query.
     * 
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(booking_date) AS booking_date";
        return $fields;
    }
    
}