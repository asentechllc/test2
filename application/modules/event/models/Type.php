<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 1543 2010-10-14 11:18:30Z jay $
 */

/**
 * Model class for all Event types
 *
 * @category Event
 * @package Event_Model
 * @subpackage Type
 */
class Event_Model_Type extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Event_Model_TypeMapper';

    protected $_id = null;
    protected $_url = null;
    protected $_title = null;

    private $_urlchanged = false;
    
    /**
     * Return mapper for model
     *
     * @return Event_Model_TypeMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Event_Model_Type
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }
    
    /**
     * Get the URL string ID for this event type
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }
        
        return $this->_url;
    }
    
    /**
     * Set the URL String ID for this event type
     *
     * @param string $url
     * @return Event_Model_Type 
     */
    public function setUrl($url)
    {
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }
        $this->_url = $url;
        
        return $this;
    }
    
    /**
     * Get the Type Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the Type Title
     *
     * @param <type> $title
     * @return Event_Model_Type
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Return the events in this event type
     *
     * @param int|array $page
     * @return Zend_Paginator
     */
    public function getEvents($conditions=null, $order=null, $page = null)
    {
        $mapper = Event_Model_Event::getMapper();

        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['typeid'] = $this->getId();

        $events = $mapper->findAllByField($conditions, $order, $page);

        return $events;
    }
    
    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }
}


