<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 1238 2010-08-17 12:58:36Z leigh $
 */

/**
 * Form class for all event types
 *
 * @category Event
 * @package Event_Form
 * @subpackage Type
 */
class Event_Form_Type extends Mcmr_Form
{
    public function init()
    {
        $this->setName('eventformtype')->setElementsBelongTo('event-form-type');
        $this->setMethod('post');
    }
}
