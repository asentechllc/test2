<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Event_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 1311 2010-09-05 13:29:36Z leigh $
 */

/**
 * Description of Event Location form
 *
 * @category   Event
 * @package    Event_Form
 * @subpackage Location
 */
class Event_Form_Location extends Mcmr_Form
{
    public function init()
    {
        $this->setName('eventformlocation')->setElementsBelongTo('event-form-location');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }
}