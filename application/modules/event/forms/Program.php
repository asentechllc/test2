<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Event_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 1311 2010-09-05 13:29:36Z leigh $
 */

/**
 * Description of Event Program form
 *
 * @category   Event
 * @package    Event_Form
 * @subpackage Program
 */
class Event_Form_Program extends Mcmr_Form
{
    public function init()
    {
        $this->setName('eventformprogram')->setElementsBelongTo('event-form-program');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }
}