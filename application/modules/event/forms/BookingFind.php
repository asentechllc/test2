<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Event_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 1311 2010-09-05 13:29:36Z leigh $
 */

/**
 * Description of Event Booking form
 *
 * @category   Event
 * @package    Event_Form
 * @subpackage Booking
 */
class Event_Form_BookingFind extends Mcmr_Form
{
    public function init()
    {
        $this->setName('eventformbookingfind')->setElementsBelongTo('event-form-booking-find');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        // Add some CSRF protection
        $this->addElement(
            'hash', 'eventformcsrf', array('salt' => 'unique')
        );
    }
}