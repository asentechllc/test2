<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A form for changing the display order of the event events
 *
 * @category   Event
 * @package    Form
 * @subpackage Order
 */
class Event_Form_Order extends Mcmr_Form
{
    private $_events = array();
    private $_type = null;
    private $_models = null;

    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }

        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'event'
                . DIRECTORY_SEPARATOR . 'event_form_order-'.strtolower($type).'.ini';
            $this->_type = $type;
        }

        parent::__construct($options);
    }

    public function postInit()
    {
        $this->setName('eventformorder')->setElementsBelongTo('event-form-order');
        $this->setMethod('post');


        // Get the type this is limited to and set conditions accordingly.
        $timestring = isset($this->_events['fromTime'])?$this->_events['fromTime']:'-1 month';
        $time = Mcmr_StdLib::time($timestring, 'day');
        $conditions = array( 'publishdate'=>array( 'value'=>$time, 'condition'=>'>=' ));
        if (isset($this->_events['conditions'])) {
            $conditions = array_merge($this->_events['conditions'], $conditions);
        }
        
        $order = array('publishdate' => 'desc', 'startdate' => 'desc');
        if (null !== $this->_type) {
            $order = array(
                'ordr_'.$this->_type=>'desc',
                'publishdate' => 'desc',
                'startdate' => 'desc'
            );
        }
        
        // Get the events.
        $mapper = Event_Model_Event::getMapper();
        $page['page'] = 1;
        $page['count'] = isset($this->_events['count'])?$this->_events['count']:10;
        $optionCount = isset($this->_events['optionCount'])?$this->_events['optionCount']:100;
        
        $events = $mapper->findAllByField($conditions, $order, $page);
        $this->_models = $events;

        $subform = new Mcmr_Form();
        $subform->setElementsBelongTo('events');

        $x = 1;
        $displayGroupElements = array();
        $typeValue = 'chronological';
        foreach ($events as $event) {
            $eventOrder = $event->getOrder($this->_type);
            if ((null !== $eventOrder) && (0 !== $eventOrder)) {
                $typeValue = 'specific';
            }

            $value = ((null !== $eventOrder) && (0 !== $eventOrder))?$event->getId():'';
            $element = array(
                'options' => array(
                    'label' => 'Event ' . ($x),
                    'required' => true,
                    'value' => $value,
                    'class' => 'sortable'
                ),
                'optionSource' => array(
                    'mapper' => 'Event_Model_Event',
                    'conditions' => $conditions,
                    'order' => array('publishdate' => 'desc', 'startdate' => 'desc'),
                    'page' => array('page' => '1', 'count' => $optionCount),
                    'valueField' => 'id',
                    'allowNull'=>true,
                    'allowNullDisplay'=>'Please Select...'
                    ));
            $element = $this->_processElement($element);
            $subform->addElement('Select', "$x", $element['options']);
            $displayGroupElements[] = "$x";
            $x++;
        }

        if (isset($displayGroupElements[0])) {
            $subform->addDisplayGroup($displayGroupElements, 'events', array('class'=>'sortable-items'));
        }

        $this->addSubForm($subform, 'events');

        // Set the value of the 'orderby' field
        $orderby = $this->getElement('orderby');
        if (null !== $orderby) {
            $orderby->setValue($typeValue);
        }
    }

    public function setEvents($events)
    {
        $this->_events = $events;
    }

    public function getEvents()
    {
        return $this->_events;
    }
    
    public function getEventModels()
    {
        return $this->_models;
    }

}
