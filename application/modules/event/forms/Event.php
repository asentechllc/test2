<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Event_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Job.php 1311 2010-09-05 13:29:36Z leigh $
 */

/**
 * Description of Event form
 *
 * @category   Event
 * @package    Event_Form
 * @subpackage Event
 */
class Event_Form_Event extends Mcmr_Form
{
    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'event' 
            . DIRECTORY_SEPARATOR . 'event_form_event-'.strtolower($type).'.ini';

        }

        parent::__construct($options);
    }
    
    public function init()
    {
        $this->setName('eventformevent')->setElementsBelongTo('event-form-event');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }
}