<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Only show published pages to guest users
 *
 * @category Page
 * @package Page_Acl
 * @subpackage ViewPublished
 */
class Event_Acl_Visible implements Zend_Acl_Assert_Interface
{

    /**
     * Returns true if and only if the event is not hidden
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the user object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        $controller = Zend_Controller_Front::getInstance();

        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $id = $controller->getRequest()->getParam('id', null);
        }

        if (null !== $id) {
            $mapper = Event_Model_Event::getMapper();
            $event = $mapper->find($id);
            return (null !== $event && $event->getStatus()->getTitle() != 'hidden');
        } else {
            //we want this to be set even for reqests for single page to be able to show their subpages correctly
            $mapper = Event_Model_Status::getMapper();
            $statuses = $mapper->findAllByField(null, null, array('page' => 1, 'count' => 100000));
            $statusNames = array();
            foreach ($statuses as $status) {
                if ('hidden' != $status->getTitle()) {
                    $statusNames[] = $status->getTitle();
                }
            }
            $controller->getRequest()->setParam('status', implode(',', $statusNames));
            return true;
        }
    }

}
