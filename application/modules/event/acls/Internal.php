<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Only show published pages to guest users
 *
 * @category Event
 * @package Event_Acl
 * @subpackage ViewInternal
 */
class Event_Acl_Internal implements Zend_Acl_Assert_Interface
{

    /**
     * Returns true if and only if the event the booking is supposed to be for non-external event
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the user object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();

        $controller = Zend_Controller_Front::getInstance();

        if (null !== $params && isset($params['eventid'])) {
            $id = $params['eventid'];
        } else {
            $id = $controller->getRequest()->getParam('eventid', null);
        }

        if (null !== $id) {
            $mapper = Event_Model_Event::getMapper();
            $event = $mapper->find($id);
            
            return (null !== $event && !$event->getAttribute('external') );
        } else {
            return false;
        }
    }

}
