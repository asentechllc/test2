<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Event_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of Events based on the conditions
 *
 * @category   Event
 * @package    Event_View
 * @subpackage Helper
 */
class Event_View_Helper_EventRead extends Zend_View_Helper_Abstract
{
    public function eventRead($condition = null, $order=null, $partial = null)
    {
        $mapper = Event_Model_Event::getMapper();
        $event = $mapper->findOneByField($condition, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('event'=>$event));
                }
            }

            return $this->view->partial($partial, array('event'=>$event));
        }

        return $event;
    }
}
