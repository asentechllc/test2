<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View Helper for retrieving an index of events
 *
 * @category Event
 * @package Event_View
 * @subpackage Helper
 */
class Event_View_Helper_EventIndex extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve an index of events based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function eventIndex($conditions = array(), $order=null, $page=null, $partial = null)
    {
        $mapper = Event_Model_Event::getMapper();
        $events = $mapper->findAllByField($conditions, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('events'=>$events));
                }
            }

            return $this->view->partial($partial, array('event'=>$events));
        }

        return $events;
    }
}
