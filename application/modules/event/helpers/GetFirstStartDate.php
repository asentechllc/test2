<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Event_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to get the earlist start date of all events
 *
 * @category   Event
 * @package    Event_View
 * @subpackage Helper
 */
class Event_View_Helper_GetFirstStartDate extends Zend_View_Helper_Abstract
{

    /**
     * @return the earlist start date.
     */
    public function getFirstStartDate()
    {
        $mapper = Event_Model_Event::getMapper();
        $events = $mapper->findAllByField(array(), array('startdate' => 'asc'), 1)->getIterator();
        $firstEventStartDatetime = $events[0]->getStartdate();
        $dateinfo = getdate($firstEventStartDatetime);
        return sprintf("%02d", $dateinfo['mday']) 
            . '/' . sprintf("%02d", $dateinfo['mon']) 
            . '/' . sprintf("%d", $dateinfo['year']);
    }

}