<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Event
 * @package Event_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View Helper for retrieving event question
 *
 * @category Event
 * @package Event_View
 * @subpackage Helper
 */
class Event_View_Helper_GetEventQuestion extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve event question
     *
     * @param int $eventId
     * @return array|string
     */
    public function getEventQuestion($eventId)
    {
        $mapper = Event_Model_Event::getMapper();
        $event = $mapper->find($eventId);

        return $event->getQuestion();
    }
}
