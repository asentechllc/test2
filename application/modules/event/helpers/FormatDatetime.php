<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Event
 * @package    Event_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to convert unix timestamp to uk date and time format
 *
 * @category   Event
 * @package    Event_View
 * @subpackage Helper
 */
class Event_View_Helper_FormatDatetime extends Zend_View_Helper_Abstract
{

    /**
     * @return an EventDatetime object.
     */
    public function formatDatetime($unixtimestamp)
    {
        $datetime = new EventDatetime($unixtimestamp);
        return $datetime;
    }

}

class EventDatetime
{
    private $_dateinfo;

    /**
     * @param int $unixtimestamp
     */
    public function __construct($unixtimestamp)
    {

        $this->_dateinfo = getdate($unixtimestamp);
    }

    /**
     * @return string (dd/mm/yy)
     */
    public function getDate()
    {

        return sprintf("%02d", $this->_dateinfo['mday']) 
            . '/' . sprintf("%02d", $this->_dateinfo['mon'])
            . '/' . sprintf("%02d", $this->_dateinfo['year']);
    }

    /**
     * @return string (hh:mm)
     */
    public function getTime()
    {

        return sprintf("%02d", $this->_dateinfo['hours']) . ':' . sprintf("%02d", $this->_dateinfo['minutes']);
    }

}