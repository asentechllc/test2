<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Feature
 * @package Feature_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: FeatureMapper.php 2416 2011-05-10 14:55:53Z leigh $
 */

/**
 * Mapper class for all feature models
 *
 * @category Feature
 * @package Feature_Model
 * @subpackage FeatureMapper
 */
class Feature_Model_FeatureMapper extends Mcmr_Model_GenericMapper
{

    protected $_dbTableClass = 'Feature_Model_DbTable_Feature';
    protected $_dbAttrTableClass = 'Feature_Model_DbTable_FeatureAttr';
    protected $_dbOrdrTableClass = 'Feature_Model_DbTable_FeatureOrdr';
    protected $_modelClass = 'Feature_Model_Feature';
    protected $_columnPrefix = 'feature_';
    protected $_cacheIdPrefix = 'Feature';
    protected $_cols = null;

	/**
	 * Find draft for current user (create if required)
	 * Each user will have a single draft where data is stored before the article is saved
	 **/
	public function findDraft(){

		$user = Zend_Registry::get('authenticated-user');

		$models = $this->findAllByField(
			array(
				'userid' => $user->getId(),
				'draft' => 1,
			)
		);
		
		# return found model
		if($models->getTotalItemCount()){
			foreach($models as $model){
				break;
			}
		}
		else{
			$model = new Feature_Model_Feature();
			$model->url = '';
			$model->title = '';
			$model->draft = true;
			$model->userid = $user->getId();
			$this->save($model);
		}
		
		return $model;
	}

    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
		# unmark as a draft once saved with a title
		if($model->draft && strlen($model->title)){
			$model->draft = 0;
		}
		
		parent::save($model);
	}




    protected function _sanitiseValues($data)
    {
        unset($data['feature_blocks']);
        unset($data['feature_categories']);
        unset($data['feature_categoryids']);
        unset($data['feature_categorytitles']);
        unset($data['feature_typeid']);
        unset($data['feature_userid']);
        unset($data['feature_userrole']);
        unset($data['feature_tags']);
        unset($data['feature_userIdAuthor']);
        unset($data['feature_order']);
        unset($data['feature_topsectionid']);
        unset($data['feature_subsectionid']);

        return $data;
    }

    protected function _sanitiseValue( $field, $value ) 
    {
        switch ( $field ) {
			case 'feature_sites':
				$value = implode(',', $value);
				break;
			case 'createdate':
			case 'feature_createdate':
			case 'publishdate':
			case 'feature_publishdate':
				if (is_int($value))
					$value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
				elseif (is_array($value)) {
					foreach ($value as $key => $item) {
						if (is_int($item)) {
							$value[$key] = new Zend_Db_Expr("FROM_UNIXTIME({$item})");
						}
					}
				}
				break;
			case 'sites':
			case 'setids':
			case 'categoryids':
			case 'categorytitles':
				$value = null;
				break;
			case 'feature_content':
				$newValue = array();
				if(is_array($value)){
					foreach($value as $v)
						$newValue[] = $v;
				}
				$value = base64_encode(serialize($newValue));
				unset($newValue);
				break;
        }
        return $value;
    } //_sanitiseValue


    protected function _postSave(Mcmr_Model_ModelAbstract $model)
    {
        $this->_saveTags($model);
        $this->_saveCategories($model);
    }
    /**
     * Delete the model information from the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return Feature_Model_FeatureMapper
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        $id = $model->getId();
        $this->_deleteTags(array('feature_id=?' => $id));
        $this->_deleteCategories(array('feature_id=?' => $id));
        return parent::delete($model);
    }

    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(feature_createdate) AS feature_createdate";
        $fields[] = "UNIX_TIMESTAMP(feature_publishdate) AS feature_publishdate";
        return $fields;
    }

    protected function _convertRowToModel($row)
    {
        $model = parent::_convertRowToModel($row);

        if (null !== $model) {
            $model->setUserIdAuthor($row['user_id_author']);
            $model->setUserrole($row['user_role']);
            $model->content = unserialize(base64_decode($model->content));
        }

        return $model;
    }



    /**
     * Returns an array of tags including their name, and the number of times it has been used
     *
     * @return array
     */
    public function getTagCounts()
    {
        $cacheid = 'FeatureFeature_tagCount';
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($tags = $cache->load($cacheid))) {
            $table = new Mcmr_Db_Table('news_tags');
            $select = $table->select();
            $select->from(array('tags' => 'news_tags'), array('name' => 'tag_name', 'count' => 'COUNT(*)'))
                    ->group('tags.tag_name');
            $tags = $table->fetchAll($select);

            $cache->save($tags, $cacheid, array('feature_model_feature_index'));
        }

        return $tags;
    }

    /**
     * Takes Zend_Db_Table rows and maps them to the model.
     *
     * @param array $rows
     * @return array
     */
    public function filter($rows)
    {
        $models = array();
        foreach ($rows as $row) {
            // Use the mapper find. This is more taxing on first load. But afterwards every model will be cached.
            // This prevents a full DB hit if a single model is updated.
            $model = $this->find($row['feature_id']);
            if (null !== $model) {
                $models[] = $model;
            }
        }

        return $models;
    }

    /**
     * Add a condition to the select query
     *
     * @param Zend_Db_Select $select
     * @param string $field
     * @param string $value
     * @return Zend_Db_Select
     */
    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        $originalValue = $value;
        if (is_array($value)) {
            $condition = $value['condition'];
            $value = $value['value'];
        } else {
            $condition = '=';
        }
        if ('feature_published' == $field) {
            $time = time();
            if ($value) {
                $select->where('feature_features.feature_publishdate <= FROM_UNIXTIME(?)', $time );
                $select->where('feature_features.feature_publishdate > FROM_UNIXTIME(86400)');
                $select->where('feature_features.feature_published = 1');
            } else {
                $select->where(
                    'feature_features.feature_publishdate > FROM_UNIXTIME(?) '
                    . ' OR feature_features.feature_publishdate = FROM_UNIXTIME(0) '
                    . 'OR feature_features.feature_published = 0',
                    $time
                );
            }
        } elseif ('category_id' == $field) {
            $select->setIntegrityCheck(false);
            if (is_array($value)) {
                if (!empty($value)) {
                    $select->join(
                        array('category' => 'feature_feature_categories'),
                        "category.feature_id = feature_features.feature_id"
                    )->group('feature_features.feature_id');
                    $select->where('category.category_id IN (?)', $value);
                }
            } else {
                $select->join(
                    array('category' => 'feature_feature_categories'),
                    "category.feature_id = feature_features.feature_id"
                )->group('feature_features.feature_id');
                $select->where('category.category_id = ?', $value);
            }
        } elseif ('feature_sites' == $field) {
            Mcmr_Debug::dump('sites condition!');
            $this->_addSitesCondition( $select, $field, $value );
        } else {
            parent::_addCondition($select, $field, $originalValue);
        }

        return $select;
    }

    /**
     * Saves the models categories to the database
     *
     * @param News_Model_Article $model 
     * @return News_Model_ArticleMapper
     */
    private function _saveCategories(Feature_Model_Feature $model)
    {
        $this->_deleteCategories(array('feature_id=?' => $model->getId()));
        $table = new Mcmr_Db_Table('feature_feature_categories');
        $data['feature_id'] = $model->getId();

        foreach ($model->getCategoryids() as $category) {
            $data['category_id'] = $category;
            $table->insertIgnore($data);
        }

        return $this;
    }

    /**
     * Load the categories from the database into the model
     *
     * @param Feature_Model_Feature $model
     * @return Feature_Model_FeatureMapper
     */
    public function loadCategories(Feature_Model_Feature $model)
    {
        if (null !== $model->getId()) {
            $cacheid = 'FeatureFeature_Categories' . $this->_getCacheId(array('id' => $model->getId()));
            $cache = Zend_Registry::get('cachemanager')->getCache('database');

            if (false === ($categories = $cache->load($cacheid))) {
                $table = new Mcmr_Db_Table('feature_feature_categories');
                $rows = $table->fetchAll(array('feature_id=?' => $model->getId()));

                $categories = array();
                foreach ($rows as $row) {
                    $categories[$row['category_id']] = $row['category_id'];
                }

                $cache->save($categories, $cacheid, array('feature_model_feature_id_' . $model->getId()));
            }

            $model->setCategoryids($categories);
        }

        return $this;
    }

    /**
     * Delete the categories from the database
     *
     * @param array $condition
     * @return Feature_Model_FeatureMapper
     */
    private function _deleteCategories(array $condition)
    {
        $table = new Mcmr_Db_Table('feature_feature_categories');
        $table->delete($condition);

        return $this;
    }

    /**
     * Takes an array of tags and saves them to the database for the model
     *
     * @param Feature_Model_Feature $model
     * @return Feature_Model_FeatureMapper
     */
    private function _saveTags(Feature_Model_Feature $model)
    {
        $this->_deleteTags(array('feature_id=?' => $model->getId()));
        $table = new Mcmr_Db_Table('news_tags');
        $data['feature_id'] = $model->getId();

        foreach ($model->getTags() as $tag) {
            $data['tag_name'] = $tag;
            $table->insert($data);
        }

        return $this;
    }

    /**
     * Load the tags from the database and put them in the model
     *
     * @param Feature_Model_Feature $model 
     */
    public function loadTags(Feature_Model_Feature $model)
    {
        $cacheid = 'FeatureFeature_Tags' . $this->_getCacheId(array('id' => $model->getId()));
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false === ($tags = $cache->load($cacheid))) {
            $table = new Mcmr_Db_Table('news_tags');

            $tags = array();
            $rows = $table->fetchAll(array('article_id=?' => $model->getId()));
            foreach ($rows as $row) {
                $tags[] = $row['tag_name'];
            }

            $cache->save($tags, $cacheid, array('feature_model_feature_id_' . $model->getId()));
        }

        $model->setTags($tags);
    }

    /**
     * Deletes tags from the DB based on the conditions
     *
     * @param array $where
     * @return Feature_Model_FeatureMapper
     */
    private function _deleteTags(array $where)
    {
        /*$table = new Mcmr_Db_Table('news_tags');
        $table->delete($where);*/

        return $this;
    }

    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($url)
    {
        return (null !== $this->findOneByField(array('url' => $url)));
    }

    /**
     * Takes a field name and converts it into the database field name
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        $field = strtolower($field);
        switch ($field) {
            case 'user':
            case 'user_id':
            case 'userid':
                $field = 'user_id';
                break;

            case 'type':
            case 'typeid':
            case 'type_id':
                $field = 'type_id';
                break;

            case 'section':
            case 'sectionid':
            case 'section_id':
                $field = 'section_id';
                break;

            case 'category':
            case 'category_id':
            case 'categoryid':
                $field = 'category_id';
                break;

            case 'set':
            case 'set_id':
            case 'setid':
                $field = 'set_id';
                break;

            case 'feature_useridauthor':
            case 'useridauthor':
                $field = 'user_id_author';
                break;

            case 'feature_userrole':
            case 'userrole':
                $field = 'user_role';
                break;

            case 'content':
                $field = 'feature_content';
                break;

            default:
                if (0 !== strncmp('attr_', $field, 5)) {
                    $field = 'feature_' . $field;
                }
                break;
        }
        return $field;
    }

    /**
     * Get the maximum amount of time a fetch all can be cached for. An index may
     * need to expire early dues to timed articles
     *
     * @return int|null 
     */
    protected function _maxCacheLifetime(Zend_Cache_Core $cache, $conditions)
    {
        $now = time();
        $select = $this->getDbTable()->select();
        $select->from($this->getDbTable(), $this->_selectFields())
                ->where('feature_publishdate >= FROM_UNIXTIME(?)', $now)
                ->where('feature_published = 1')
                ->order('feature_publishdate asc');
        
        if (!empty($condition)) {
            foreach ($condition as $field => $value) {
                if (!in_array($field, array('published', 'publishdate'))) {
                    $select = $this->_addCondition($select, $field, $value);
                }
            }
        }
        
        $next = $this->getDbTable()->fetchRow($select);
        
        if ($next) {
            $cacheLifetime = array(
                $cache->getBackend()->getLifetime(false), 
                ($next->feature_publishdate - $now),
            );
            
            return min($cacheLifetime);
        } else {
            return null;
        }
    }
}
