<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2002 2011-01-04 00:03:45Z leigh $
 */

/**
 * Default_IndexController
 *
 * The Purpose of this controller is to do nothing but forward the request onto the real default page
 * as determined by the configuration
 *
 * @category Default
 * @package Default_IndexController
 */
class Feature_IndexController extends Mcmr_Controller_Action
{

    public static function getModelName()
    {
        return "feature";
    }
    
    public function indexAction(){
    	if($this->isAdmin())
    	    $this->view->redirectNow('/');
    	    
    	parent::indexAction();
    }

    public function readAction(){
    	$this->_helper->_layout->setLayout('one-column');
    	parent::readAction();
    }

    /**
     * Create a new news item. Display a form on GET. Create on POST.
     **/
    public function updateAction()
    {
    
        if (empty($this->view->model) && null !== ($id = $this->_request->getParam('id', null))) {
            $this->_model = $this->_mapper->find($id);
            $this->view->model = $this->_model;
		}  
		
		$form = $this->_helper->loadForm('Feature');
		$this->view->form = $form;

		// Populate the form from request variables
		$form->populate($this->_request->getParams());

		if ($this->_request->isPost()) {
		
			// Merge the existing news data with the form values. This allows for partial updates.
			$post = $this->_request->getPost();
			$modelvalues = $this->_model->getOptions(true);
			$post['featureformfeature'] = array_merge($modelvalues, $post['featureformfeature']);
		
			if ($form->isValid($post)) {
				$values = $form->getValues(true);
				$this->_model->setOptions($values);

				try {
					$this->_mapper->save($this->_model);
					$this->view->error = false;
					$this->view->getHelper('DisplayMessages')->addMessage('Article Saved', 'info');
					$this->view->getHelper('Redirect')->notify('update', $this->_model);
				} catch (Exception $e) {
					$this->view->error = true;
					$this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
				}
			} else {
				$this->view->error = true;
				$this->view->getHelper('DisplayMessages')->addMessage('Incorrect Form Values', 'warn');
			}
		} else {
		
			# auto-populate author
			if($this->_model->draft){
				$this->_model->userIdAuthor = $this->_model->userid;
			}
			
			$values = $this->_model->getOptions(true);
			$values['tags'] = implode(',', $values['tags']);

			$form->populate($values);
		}

		# set preview token to authorise preview
		if($el = $form->getElement('previewToken'))
			$el->setValue(Mcmr_Acl::accessToken());
    }    

    /**
     * Create a new news item. Display a form on GET. Create on POST.
     **/
    public function createAction()
    {

    	# check for draft version for this user
		$user = Zend_Registry::get('authenticated-user');
		
		$this->_model = $this->view->model = $this->_mapper->findDraft();
		$this->_helper->viewRenderer('update');
		$this->updateAction();
    }    

    /**
     * Publish a news item
     */
    public function publishAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {

        	$from = $this->_request->getParam('from', '/');
        	
            if (null !== ($this->_model = $this->_mapper->find($id))) {
                $this->view->model = $this->_model;

                if ($this->_request->isPost()) {
                    try {
                        $this->_model->publish();
                        $this->_mapper->save($this->_model);

						$this->view->redirect('publish', $from);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Article Published', 'info');
                        $this->view->getHelper('Redirect')->notify('publish', $this->_model);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Article Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Unpublish a news item
     */
    public function unpublishAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {

        	$from = $this->_request->getParam('from', '/');
        	
            $time = $this->_request->getParam('publishDate', time()+31536000); // Default 1 year
            if (null !== ($this->_model = $this->_mapper->find($id))) {
                $this->view->model = $this->_model;

                if ($this->_request->isPost()) {
                    try {
                        $this->_model->unpublish();
                        $this->_mapper->save($this->_model);

						$this->view->redirect('publish', $from);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Article Un-Published', 'info');
                        $this->view->getHelper('Redirect')->notify('publish', $this->_model);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Article Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

}

