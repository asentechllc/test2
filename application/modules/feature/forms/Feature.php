<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Feature.php 1752 2010-11-10 16:12:40Z leigh $
 */

/**
 * Form class for Features
 *
 * @category Feature
 * @package Feature_Form
 * @subpackage Feature
 */
class Feature_Form_Feature extends Mcmr_Form
{
    public function __construct($options = null)
    {
        parent::__construct($options);
    }

    public function init()
    {
        $this->setName('featureformfeature')->setElementsBelongTo('feature-form-feature');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }

    public function postInit()
    {
        // If we have an author field without a value set the value to the logged in user
        if (null !== ($element = $this->getElement('userIdAuthor'))) {
            $value = $element->getValue();
            if (null == $element->getValue()) {
                $user = Zend_Registry::get('authenticated-user');
                $element->setValue($user->getId());
            }
        }

    }
}
