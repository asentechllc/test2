<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Published.php 2361 2011-04-18 08:23:26Z michal $
 */

/**
 * Description of Published
 *
 * @category News
 * @package News_Acl
 * @subpackage Published
 */
class News_Acl_Published implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if the news article is published
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the user object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
            $seoId = $controller->getRequest()->getParam('seoId', null);
            $url = $controller->getRequest()->getParam('url', null);
        }

        if (!empty($id) || !empty($url) || !empty($seoId)) {

        	if(!empty($seoId)){
        		$id = Mcmr_StdLib::realId($seoId);
        	}

            $mapper = News_Model_Article::getMapper();

            $article = null !== $id?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));

            if ( null === $article ) {
                throw new Mcmr_Exception_PageNotFound('Article not found');
            }
            return $article->isPublished();
        } else {
            // There is no ID, so allow it, but force only published
            $controller->getRequest()->setParam('published', '1');
            return true;
        }

    }
}
