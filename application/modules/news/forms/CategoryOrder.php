<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   News
 * @package    Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Order.php 2359 2011-04-15 11:15:41Z leigh $
 */

/**
 * A form for changing the display order of the news categories
 *
 * @category   News
 * @package    Form
 * @subpackage Order
 */
class News_Form_CategoryOrder extends Mcmr_Form_Order
{

    protected function _addSingleElement($subform, $index, $value)
    {
        $elementConfig = array(
            'options' => array(
                'class' => 'sortable newsformorder-category'
            )
        );
        $elementConfig['options']['label'] = "Entry ".$index;
        $elementConfig['options']['value'] =  $value;
        $element = $this->_processElement($elementConfig);
        $subform->addElement('Text', $index, $element['options']);
    }

}
