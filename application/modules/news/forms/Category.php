<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Category.php 2175 2011-02-09 17:01:23Z leigh $
 */

/**
 * Description of Category
 *
 * @category News
 * @package News_Form
 * @subpackage Category
 */
class News_Form_Category extends Mcmr_Form
{
    public function init()
    {
        $this->setName('newsformcategory')->setElementsBelongTo('news-form-category');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }
}
