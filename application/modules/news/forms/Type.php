<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package News
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 1400 2010-09-15 13:52:21Z leigh $
 */

/**
 * Description of Type
 *
 * @category Mcmr
 * @package News
 * @subpackage Type
 */
class News_Form_Type extends Mcmr_Form
{
    public function init()
    {
        $this->setName('newsformtype')->setElementsBelongTo('news-form-type');
        $this->setMethod('post');

        $this->addElement(
            'Text', 'title', array(
                'label' => 'Title',
                'required' => true,
                'filters'    => array('StringTrim'),
                'invalidMessage' => 'Title must be specified',
            )
        );

        $this->addElement(
            'Submit', 'submit', array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );

        // Add some CSRF protection
        $this->addElement(
            'hash', 'csrf', array(
                'salt' => 'unique'
            )
        );
    }
}
