<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Article.php 1752 2010-11-10 16:12:40Z leigh $
 */

/**
 * Form class for Articles
 *
 * @category News
 * @package News_Form
 * @subpackage Article
 */
class News_Form_Article extends Mcmr_Form
{
    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'news' 
            . DIRECTORY_SEPARATOR . 'news_form_article-'.strtolower($type).'.ini';

        }

        parent::__construct($options);
    }

    public function init()
    {
        $this->setName('newsformarticle')->setElementsBelongTo('news-form-article');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        // Add some CSRF protection
//        $this->addElement('hash', 'csrf', array(
//            'salt' => 'unique'));
    }

    public function postInit()
    {
        // If we have an author field without a value set the value to the logged in user
        if (null !== ($element = $this->getElement('userIdAuthor'))) {
            $value = $element->getValue();
            if (null == $element->getValue()) {
                $user = Zend_Registry::get('authenticated-user');
                $element->setValue($user->getId());
            }
        }

    }
}
