<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   News
 * @package    Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Order.php 2359 2011-04-15 11:15:41Z leigh $
 */

/**
 * A form for changing the display order of the news articles
 *
 * @category   News
 * @package    Form
 * @subpackage Order
 */
class News_Form_Order extends Mcmr_Form_Order
{

}
