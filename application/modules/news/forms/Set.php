<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package News
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of Set
 *
 * @category Mcmr
 * @package News
 * @subpackage Set
 */
class News_Form_Set extends Mcmr_Form
{
    public function init()
    {
        $this->setName('newsformset')->setElementsBelongTo('news-form-set');
        $this->setMethod('post');

        // Add some CSRF protection
        $this->addElement('hash', 'csrf', array('salt' => 'unique'));
    }
}
