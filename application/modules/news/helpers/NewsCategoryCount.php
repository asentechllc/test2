<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   News
 * @package    News_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of articles based on the conditions
 *
 * @category   News
 * @package    News_View
 * @subpackage Helper
 */
class News_View_Helper_NewsCategoryCount extends Zend_View_Helper_Abstract
{
    public function newsCategoryCount($conditions = null)
    {
        $mapper = News_Model_Category::getMapper();

        return $mapper->count($conditions);
    }
}
