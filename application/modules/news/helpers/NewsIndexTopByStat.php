<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: NewsIndex.php 663 2010-05-19 13:47:29Z leigh $
 */

/**
 * A View Helper for retrieving an index of news articles
 *
 * @category News
 * @package News_View
 * @subpackage Helper
 */
class News_View_Helper_NewsIndexTopByStat extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve top news articles based on the stat's action. IE 'read'
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function newsIndexTopByStat($action, $page, $startdate, $enddate=null, $partial = null)
    {
        $mapper = News_Model_Article::getMapper();
        $articles = $mapper->findTopByStat($action, $page, $startdate, $enddate);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('articles'=>$articles));
                }
            }

            return $this->view->partial($partial, array('articles'=>$articles));
        }

        return $articles;
    }
}
