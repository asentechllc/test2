<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: NewsTagCloud.php 711 2010-05-28 09:35:13Z leigh $
 */

/**
 * A View helper used to retrieve a tag cloud for the news articles
 *
 * @category News
 * @package News_View
 * @subpackage 
 */
class News_View_Helper_NewsTagCloud extends Zend_View_Helper_Abstract
{
    public function newsTagCloud()
    {
        $mapper = News_Model_Article::getMapper();
        $tags = $mapper->getTagCounts();

        $cloud = new Zend_Tag_Cloud();
        foreach ($tags as $tag) {
            $cloud->appendTag(
                array(
                    'title'=>$tag->name, 
                    'weight'=>$tag->count,
                    'params' => array('url' => '?tag='.$tag->name)
                )
            );
        }

        return $cloud;
    }
}
