<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: NewsTypeIndex.php 1257 2010-08-20 10:53:48Z leigh $
 */

/**
 * A View Helper for retrieving an index of news types
 *
 * @category News
 * @package News_View
 * @subpackage Helper
 */
class News_View_Helper_NewsTypeRead extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve an index of news types based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function newsTypeRead($conditions = array(), $order=null, $partial = null)
    {
        $mapper = News_Model_Type::getMapper();
        $type = $mapper->findOneByField($conditions, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('type'=>$type));
                }
            }

            return $this->view->partial($partial, array('type'=>$type));
        }

        return $type;
    }
}
