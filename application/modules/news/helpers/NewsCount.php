<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   News
 * @package    News_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of articles based on the conditions
 *
 * @category   News
 * @package    News_View
 * @subpackage Helper
 */
class News_View_Helper_NewsCount extends Zend_View_Helper_Abstract
{
    public function newsCount($conditions = null)
    {
        $mapper = News_Model_Article::getMapper();
        if (!isset($conditions['sites'])) {
            $conditions['sites'] = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
        }

        return $mapper->count($conditions);
    }
}
