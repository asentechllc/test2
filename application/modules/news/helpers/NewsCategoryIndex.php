<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View Helper for retrieving an index of news categories
 *
 * @category News
 * @package News_View
 * @subpackage Helper
 */
class News_View_Helper_NewsCategoryIndex extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve an index of news categories based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function newsCategoryIndex($conditions = array(), $order=null, $page=null, $partial = null)
    {
        $mapper = News_Model_Category::getMapper();

        $categories = $mapper->findAllByField($conditions, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('categories'=>$categories));
                }
            }

            return $this->view->partial($partial, array('categories'=>$categories));
        }

        return $categories;
    }
}
