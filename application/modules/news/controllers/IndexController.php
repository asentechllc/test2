<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2224 2011-02-24 11:54:37Z leigh $
 */

/**
 * IndexController for the News module. Looks after all article models.
 *
 * @category News
 * @package News_IndexController
 */
class News_IndexController extends Mcmr_Controller_Action
{
    public static function getModelName()
    {
        return "article";
    }

    public function init()
    {
        parent::init();
        $contextSwitch = $this->_helper->getHelper('contextSwitch');

        if (!$contextSwitch->hasContext('rss')) {
            $contextSwitch->addContext(
                'rss', array(
                    'headers'=>array('Content-Type'=>'application/rss+xml'),
                    'suffix'=>'rss',
                )
            );
        }
        if (!$contextSwitch->hasContext('plist')) {
            $contextSwitch->addContext(
                'plist', array(
                    'headers'=>array('Content-Type'=>'text/xml'),
                    'suffix'=>'plist',
                )
            );
        }
        $contextSwitch->addActionContext('index', 'rss');
        $contextSwitch->addActionContext('index', 'plist');
        $contextSwitch->addActionContext('read', 'plist');
        $contextSwitch->initContext();
    }

    protected function _processIndexConditions(&$conditions) 
    {
        parent::_processIndexConditions($conditions);
        $this->_setRequestParams($conditions, array('publishStart', 'publishEnd', 'userIdAuthor', 'tag', 'stream'));
        $conditions['sites'] = $this->_request->getParam('siteid', $this->_request->getSiteName());
        if (null!==($category=$this->_request->getParam('category'))) {
            $this->_category = $category;
            $conditions['categoryid'] = array(
                'condition'=>'IN',
                'value' => Mcmr_Model::idsForUrls('News_Model_Category', $category)
            );
        }
        
        if($section = $this->_request->getParam('section')){
        	$section = explode('/',$section);
        	$parent = News_Model_Section::getMapper()->findOneByField(array('url'=>$section[0]));
        	if($parent){
        		if(isset($section[1])){
		        	$child = News_Model_Section::getMapper()->findOneByField(array('url'=>$section[1],'parent_id'=>$parent->getId()));
			    	$conditions['section'] = $child->getId();
		        }
		        else{
			    	$conditions['section'] = array(
			    		'condition' => 'in',
				    	'value' => $parent->getIds(),
				    );
		        }
		        	
        	}
        }

    }

    protected function _indexAfterFind()
    {
        $type = null;
        if (is_array($this->_type)) {
            if (1==count($this->_type)) {
                $type = $this->_type[0];
            }
        } else {
            $type = $this->_type;
        }
        if (null!==$type) {
            $this->_helper->viewRenderer('index/'.$type);
        } elseif (null!==($ptype=$this->_request->getParam('ptype'))) {
            $this->_helper->viewRenderer('index/'.$ptype);
        }
    }

    /**
     * Create a new news item. Display a form on GET. Create on POST.
     */
    public function createAction()
    {
        $type = $this->_request->getParam('type', Zend_Registry::get('news-config')->defaultType);
        $typeMapper = News_Model_Type::getMapper();
        
        if (!empty($type)) {
            $type = $typeMapper->findOneByField(array('url'=>$type));
            if (null == $type) {
                throw new Mcmr_Exception_PageNotFound("Article type '{$type}' not found");
            }

            // Set the view script to be the article type
            $this->_helper->viewRenderer('create/'.$type->getUrl());

            $this->_form = $this->_loadForm();
            $this->view->form = $this->_form;

            // Populate the form from request variables
            $this->_form->populate($this->_request->getParams());

            if ($this->_request->isPost()) {
                if ($this->_form->isValid($this->_request->getPost())) {
                    try {
                        $this->_model = new News_Model_Article();
                        $this->_model->setOptions($this->_form->getValues(true));
                        $this->_model->setTypeid($type->getId());

                        $this->_mapper->save($this->_model);

                        $this->view->model = $this->_model;
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Article Saved', 'info');
                        $this->view->getHelper('Redirect')->notify('create', $this->_model);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        throw $e;
                    }
                } else {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage('Invalid form values', 'error');
                }
            }

			# set preview token to authorise preview
			if($el = $this->_form->getElement('previewToken'))
				$el->setValue($this->previewToken());

			# set preview type
			if($el = $this->_form->getElement('previewType'))
				$el->setValue($type->id);
            
        } else {
            $this->view->form = null;
            $this->view->types = $typeMapper->findAllByField();
        }
    }

    protected function _readAfterFind()
    {   
        parent::_readAfterFind();
        $this->_helper->viewRenderer('read/'.$this->_model->getType()->getUrl());
        // Set the paginator page
        $page = $this->_request->getParam('page', 1);
        $this->_model->getContent(true)->setCurrentPageNumber($page);
    }

    protected function _updateAfterFind()
    {
        $this->_helper->viewRenderer('update/'.$this->_model->getType()->getUrl());
    }
    

    protected function _updateBeforeSave()
    {
    }

    protected function _updateNotPost()
    {
        $this->_values['tags'] = implode(',', $this->_values['tags']);
    }

    protected function _updateBeforeView()
    {
        # declare image filename for preview purposes
        if(($el = $this->_form->getElement('previewImage')) && ($image = $this->_model->getImage()))
            $el->setValue($image);

        # set preview token to authorise preview
        if($el = $this->_form->getElement('previewToken'))
            $el->setValue($this->previewToken());

        # set preview type
        if($el = $this->_form->getElement('previewType'))
            $el->setValue($this->_model->getType()->id);
    }

    protected function _orderBeforeView()
    {
        $this->_helper->viewRenderer('order/'.$this->_type);
    }

    /**
     * Preview a news item
     */
    public function previewAction()
    {
   		$data = $this->_request->getParam('newsformarticle');
   		
   		# fix dates
   		foreach($data as $k=>$v){
			if(substr($k,-4) == 'date'){
				$data[$k] = mktime(
					isset($data[$k]['hour']) ? intval($data[$k]['hour']) : 0,
					isset($data[$k]['minutes']) ? intval($data[$k]['minutes']) : 0,
					0,
					isset($data[$k]['month']) ? intval($data[$k]['month']) : 0,
					isset($data[$k]['day']) ? intval($data[$k]['day']) : 0,
					isset($data[$k]['year']) ? intval($data[$k]['year']) : 0
				);
			}
		}

		$this->_model = new News_Model_Article;
		$this->_model->setId(0);

		# set article type
		if(!empty($data['previewType'])){
			$this->_model->setTypeid(intval($data['previewType']));
		}

		# set article section
		if(!empty($data['sectionid'])){
			$this->_model->setSectionid(intval($data['sectionid']));
		}
		
		# set posted data
		foreach(array(
			'title',
			'intro',
			'categorytitles',
			'publishdate',
			'content',
			'userIdAuthor',
			'userId',
			'image',
		) as $field){
			$fn = 'set'.ucfirst($field);
			$this->_model->$fn(isset($data[$field]) ? $data[$field] : '');
		}

		# set posted attributes
		foreach($data as $k=>$v){
			if(substr($k,0,5) == 'attr_'){
				$this->_model->setAttribute(substr($k,5),$v);
			}
		}

		$this->view->model = $this->_model;

		$viewPath = 'read';
		foreach($this->view->getScriptPaths() as $path){
			if(file_exists($path.'read/'.$this->_model->getType()->getUrl())){
				$viewPath = 'read/'.$this->_model->getType()->getUrl();
				break;
			}
		}
		$this->_helper->viewRenderer($viewPath);

		// Set the paginator page
		$page = $this->_request->getParam('page', 1);
		$this->_model->getContent(true)->setCurrentPageNumber($page);
    }

    
    private function previewToken(){
		$user = Zend_Registry::get('authenticated-user');
		$token = $user->getId().','.sha1($user->getEmail().$user->getSalt());
		return base64_encode($token);		
    }

    /**
     * Publish a news item
     */
    public function publishAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {

        	$from = $this->_request->getParam('from', '/');
        	
            if (null !== ($this->_model = $this->_mapper->find($id))) {
                $this->view->model = $this->_model;

                if ($this->_request->isPost()) {
                    try {
                        $this->_model->publish();
                        $this->_mapper->save($this->_model);

						$this->view->redirect('publish', $from);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Article Published', 'info');
                        $this->view->getHelper('Redirect')->notify('publish', $this->_model);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Article Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Unpublish a news item
     */
    public function unpublishAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {

        	$from = $this->_request->getParam('from', '/');
        	
            $time = $this->_request->getParam('publishDate', time()+31536000); // Default 1 year
            if (null !== ($this->_model = $this->_mapper->find($id))) {
                $this->view->model = $this->_model;

                if ($this->_request->isPost()) {
                    try {
                        $this->_model->unpublish();
                        $this->_mapper->save($this->_model);

						$this->view->redirect('publish', $from);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Article Published', 'info');
                        $this->view->getHelper('Redirect')->notify('publish', $this->_model);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Article Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }


}
