<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_SetController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Set controller for the news module. Looks after all news Sets.
 *
 * @category News
 * @package News_SetController
 */
class News_SetController extends Zend_Controller_Action
{
    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('contextSwitch');

            $contextSwitch->addContext(
                'rss', array(
                    'headers'=>array('Content-Type'=>'application/rss+xml'),
                    'suffix'=>'rss',
                )
            );

            $contextSwitch->addActionContext('read', 'rss')
                    ->initContext();
        } catch (Exception $e) {
        }
    }
    
    /**
     * Index of all news Sets
     */
    public function indexAction()
    {
        // Order articles, newest to oldest
        $order = array('title'=>'asc');

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('news-config')->articles->itemCountPerPage);
        
        $mapper = News_Model_Set::getMapper();
        $this->view->sets = $mapper->findAllByField(null, $order, $page);
    }

    /**
     * Create a new news Set. Display form on GET. Create on POST.
     */
    public function createAction()
    {
        $form = $this->_helper->loadForm('Set');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $set = new News_Model_Set();
                $mapper = News_Model_Set::getMapper();
                $set->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($set);

                    $this->view->getHelper('DisplayMessages')->addMessage('News Set Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $set);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Display a single news set
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        
        if (null !== $id || null !== $url) {
            $mapper = News_Model_Set::getMapper();
            // Get the news set from the ID or the URL provided
            $set = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            $this->view->set = $set;

            if (null !== $set) {
                // Set the view script to be the article set
                $this->_helper->viewRenderer('read/'.$set->getUrl());
            }
            if (null === $set) {
                throw new Mcmr_Exception_PageNotFound('Set not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a news set. Display a form on GET. Update on POST.
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = $this->_helper->loadForm('Set');
            $this->view->form = $form;

            $mapper = News_Model_Set::getMapper();
            if (null !== ($set = $mapper->find($id))) {
                $this->view->set = $set;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $set->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($set);

                            $this->view->getHelper('DisplayMessages')->addMessage('News Set Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $set);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($set->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('News Set Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a news set. Display confirmation on GET. Delete on POST.
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = News_Model_Set::getMapper();
            if (null !== ($set = $mapper->find($id))) {
                $this->view->set = $set;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($set);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('News Set Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $set);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('News Set Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
}
