<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_TypeController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: TypeController.php 2345 2011-04-11 15:07:15Z leigh $
 */

/**
 * Type controller for the news module. Looks after all news types.
 *
 * @category News
 * @package News_TypeController
 */
class News_SectionController extends Mcmr_Controller_Action
{

    public function init()
    {
        parent::init();
        $contextSwitch = $this->_helper->getHelper('contextSwitch');

        if (!$contextSwitch->hasContext('plist')) {
            $contextSwitch->addContext(
                'plist', array(
                    'headers'=>array('Content-Type'=>'text/xml'),
                    'suffix'=>'plist',
                )
            );
        }
        $contextSwitch->addActionContext('index', 'plist');
        $contextSwitch->addActionContext('read', 'plist');
        $contextSwitch->initContext();
    }

    protected function _readAfterFind()
    {
        parent::_readAfterFind();
        $this->_helper->viewRenderer('read/'.$this->_model->getUrl());
    }
    
    public function readAction(){
    
    	# get sections based on both parent and section URLs
        $parentUrl = $this->_request->getParam('parentUrl', null);
        $url = $this->_request->getParam('url', null);

		if($parentUrl && $url){
			$parent = $this->_mapper->findOneByField(array('url'=>$parentUrl));
			
			if($parent){
				$this->_model = $this->_mapper->findOneByField(array(
					'url'=>$url,
					'parent_id'=>$parent->getId(),
				));
			}
			
			if($parent && $this->_model)
				$this->_request->setParam('id',$this->_model->getId());
			else
                throw new Mcmr_Exception_PageNotFound('Not found');
                
            
		}
    
    	parent::readAction();
    }

}
