<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_CategoryController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CategoryController.php 2315 2011-03-31 18:07:51Z michal $
 */

/**
 * Category controller for the news module. Looks after all category models
 *
 * @category News
 * @package News_CategoryController
 */
class News_CategoryController extends Mcmr_Controller_Action
{

    public function init()
    {
        parent::init();
        $contextSwitch = $this->_helper->getHelper('contextSwitch');

        if (!$contextSwitch->hasContext('rss')) {
            $contextSwitch->addContext(
                'rss', array(
                    'headers'=>array('Content-Type'=>'application/rss+xml'),
                    'suffix'=>'rss',
                )
            );
        }
        if (!$contextSwitch->hasContext('plist')) {
            $contextSwitch->addContext(
                'plist', array(
                    'headers'=>array('Content-Type'=>'text/xml'),
                    'suffix'=>'plist',
                )
            );
        }
        $contextSwitch->addActionContext('index', 'rss');
        $contextSwitch->addActionContext('index', 'plist');
        $contextSwitch->addActionContext('read', 'plist');
        $contextSwitch->initContext();
    }


    protected function _processIndexConditions(&$conditions) 
    {
        parent::_processIndexConditions($conditions);
        $this->_setRequestParams($conditions, array('publishStart', 'publishEnd', 'userIdAuthor', 'tag', 'stream'));
        $conditions['sites'] = $this->_request->getParam('siteid', $this->_request->getSiteName());
    }

    /**
     * Create a news category. Display form on GET. Save on POST
     */
    public function createAction()
    {
        $this->_form = $this->_loadForm();
        $form = $this->_form;
        $this->view->form = $this->_form;
        $this->view->error = false;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $this->_model = new News_Model_Category();
                $this->_model->setOptions($values);
                try {
                    $mapper = News_Model_Category::getMapper();
                    $mapper->save($this->_model);
                    $this->view->model = $this->_model;

                    $this->view->getHelper('DisplayMessages')->addMessage('Category saved', 'info');
//                    $this->view->getHelper('Redirect')->notify('create', $this->_model);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'error');
            }
        }
    }

    /**
     * Delete a news category. Confirm on GET. Delete on POST
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = News_Model_Category::getMapper();
            if (null !== ($this->_model = $mapper->find($id))) {
                $this->view->model = $this->_model;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($this->_model);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Category deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $this->_model);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Category Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
    
    protected static function getOrderFormName()
    {
        return 'CategoryOrder';
    }

}
