<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ArticleMapper.php 2416 2011-05-10 14:55:53Z leigh $
 */

/**
 * Mapper class for all news article models
 *
 * @category News
 * @package News_Model
 * @subpackage ArticleMapper
 */
class News_Model_ArticleMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'News_Model_DbTable_Article';
    protected $_dbAttrTableClass = 'News_Model_DbTable_ArticleAttr';
    protected $_dbOrdrTableClass = 'News_Model_DbTable_ArticleOrdr';
    protected $_modelClass = 'News_Model_Article';
    protected $_columnPrefix = 'article_';
    protected $_cacheIdPrefix = 'Article';
    protected $_cols = null;

    protected function _sanitiseValues($data)
    {
        unset($data['article_categories']);
        unset($data['article_categoryids']);
        unset($data['article_categorytitles']);
        unset($data['article_typeid']);
        unset($data['article_userid']);
        unset($data['article_userrole']);
        unset($data['article_tags']);
        unset($data['article_userIdAuthor']);
        unset($data['article_order']);
        unset($data['article_topsectionid']);
        unset($data['article_subsectionid']);
        
        return $data;
    }

    protected function _sanitiseValue( $field, $value ) 
    {
        switch ( $field ) {
        case 'article_sites':
            if (is_array($value)) {
                $value = implode(',', $value);
            }
            break;
        case 'createdate':
        case 'article_createdate':
        case 'publishdate':
        case 'article_publishdate':
            if (is_int($value))
                $value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
            elseif (is_array($value)) {
                foreach ($value as $key => $item) {
                    if (is_int($item)) {
                        $value[$key] = new Zend_Db_Expr("FROM_UNIXTIME({$item})");
                    }
                }
            }
            break;
        case 'sites':
        case 'setids':
        case 'categoryids':
        case 'categorytitles':
            $value = null;
            break;
        }
        return $value;
    } //_sanitiseValue


    protected function _postSave(Mcmr_Model_ModelAbstract $model)
    {
        $this->_saveTags($model);
        $this->_saveCategories($model);
    }
    /**
     * Delete the model information from the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return News_Model_ArticleMapper
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        $id = $model->getId();
        $this->_deleteTags(array('article_id=?' => $id));
        $this->_deleteCategories(array('article_id=?' => $id));
        return parent::delete($model);
    }

    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(article_createdate) AS article_createdate";
        $fields[] = "UNIX_TIMESTAMP(article_publishdate) AS article_publishdate";
        return $fields;
    }

    protected function _convertRowToModel($row)
    {
        $model = parent::_convertRowToModel($row);

        if (null !== $model) {
            $model->setUserIdAuthor($row['user_id_author']);
            $model->setUserrole($row['user_role']);
        }

        return $model;
    }



    /**
     * Returns an array of tags including their name, and the number of times it has been used
     *
     * @return array
     */
    public function getTagCounts()
    {
        $cacheid = 'NewsArticle_tagCount';
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($tags = $cache->load($cacheid))) {
            $table = new Mcmr_Db_Table('news_tags');
            $select = $table->select();
            $select->from(array('tags' => 'news_tags'), array('name' => 'tag_name', 'count' => 'COUNT(*)'))
                    ->group('tags.tag_name');
            $tags = $table->fetchAll($select);

            $cache->save($tags, $cacheid, array('news_model_article_index'));
        }

        return $tags;
    }

    /**
     * Takes Zend_Db_Table rows and maps them to the model.
     *
     * @param array $rows
     * @return array
     */
    public function filter($rows)
    {
        $models = array();
        foreach ($rows as $row) {
            // Use the mapper find. This is more taxing on first load. But afterwards every model will be cached.
            // This prevents a full DB hit if a single model is updated.
            $model = $this->find($row['article_id']);
            if (null !== $model) {
                $models[] = $model;
            }
        }

        return $models;
    }

    /**
     * Add a condition to the select query
     *
     * @param Zend_Db_Select $select
     * @param string $field
     * @param string $value
     * @return Zend_Db_Select
     */
    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        $originalValue = $value;
        if (is_array($value)) {
            $condition = $value['condition'];
            $value = $value['value'];
        } else {
            $condition = '=';
        }
        if ('article_published' == $field) {
            $time = time();
            if ($value) {
                $select->where('news_articles.article_publishdate <= FROM_UNIXTIME(?)', $time );
                $select->where('news_articles.article_publishdate > FROM_UNIXTIME(86400)');
                $select->where('news_articles.article_published = 1');
            } else {
                $select->where(
                    'news_articles.article_publishdate > FROM_UNIXTIME(?) '
                    . ' OR news_articles.article_publishdate = FROM_UNIXTIME(0) '
                    . 'OR news_articles.article_published = 0',
                    $time
                );
            }
        } elseif ('article_tag' == $field) {
            if (null !== $value) {
                $tags = explode(',', $value);
                $select->setIntegrityCheck(false);
                foreach ($tags as $tag) {
                    $tempTable = str_replace(' ', '', $tag);
                    $select->join(
                        array($tempTable => 'news_tags'),
                        "news_articles.article_id = {$tempTable}.article_id"
                    );
                    $select->where($tempTable . '.tag_name = ?', $tag);
                }
            }
        } elseif ('category_id' == $field) {
            $select->setIntegrityCheck(false);
            if (is_array($value)) {
                if (!empty($value)) {
                    $select->join(
                        array('category' => 'news_article_categories'),
                        "category.article_id = news_articles.article_id"
                    );
                    $select->where('category.category_id IN (?)', $value);
		            $select->group("news_articles.article_id");
                }
            } else {
                $select->join(
                    array('category' => 'news_article_categories'),
                    "category.article_id = news_articles.article_id"
                );
                $select->where('category.category_id = ?', $value);
            }
        } elseif ('set_id' == $field) {
            $select->setIntegrityCheck(false);
            if (is_array($value)) {
                if (!empty($value)) {
                    $select->join(
                        array('set' => 'news_article_categories'),
                        "set.article_id = news_articles.article_id"
                    );
                    $select->where('set.set_id IN (?)', $value);
                }
            } else {
                $select->join(array('set' => 'news_article_sets'), "set.article_id = news_articles.article_id");
                $select->where('set.set_id = ?', $value);
            }
        } elseif ('article_sites' == $field) {
            $this->_addSitesCondition( $select, $field, $value );
        } else {
            parent::_addCondition($select, $field, $originalValue);
        }

        return $select;
    }

    /**
     * Saves the models categories to the database
     *
     * @param News_Model_Article $model 
     * @return News_Model_ArticleMapper
     */
    private function _saveCategories(News_Model_Article $model)
    {
        $this->_deleteCategories(array('article_id=?' => $model->getId()));
        $table = new Mcmr_Db_Table('news_article_categories');
        $data['article_id'] = $model->getId();

        foreach ($model->getCategoryids() as $category) {
            $data['category_id'] = $category;
            $table->insertIgnore($data);
        }

        return $this;
    }

    /**
     * Load the categories from the database into the model
     *
     * @param News_Model_Article $model
     * @return News_Model_ArticleMapper
     */
    public function loadCategories(News_Model_Article $model)
    {
        if (null !== $model->getId()) {
            $cacheid = 'NewsArticle_Categories' . $this->_getCacheId(array('id' => $model->getId()));
            $cache = Zend_Registry::get('cachemanager')->getCache('database');

            if (false === ($categories = $cache->load($cacheid))) {
                $table = new Mcmr_Db_Table('news_article_categories');
                $rows = $table->fetchAll(array('article_id=?' => $model->getId()));

                $categories = array();
                foreach ($rows as $row) {
                    $categories[$row['category_id']] = $row['category_id'];
                }

                $cache->save($categories, $cacheid, array('news_model_article_id_' . $model->getId()));
            }

            $model->setCategoryids($categories);
        }

        return $this;
    }

    /**
     * Delete the categories from the database
     *
     * @param array $condition
     * @return News_Model_ArticleMapper
     */
    private function _deleteCategories(array $condition)
    {
        $table = new Mcmr_Db_Table('news_article_categories');
        $table->delete($condition);

        return $this;
    }

    /**
     * Takes an array of tags and saves them to the database for the model
     *
     * @param News_Model_Article $model
     * @return News_Model_ArticleMapper
     */
    private function _saveTags(News_Model_Article $model)
    {
        $this->_deleteTags(array('article_id=?' => $model->getId()));
        $table = new Mcmr_Db_Table('news_tags');
        $data['article_id'] = $model->getId();

        foreach ($model->getTags() as $tag) {
            $data['tag_name'] = $tag;
            $table->insert($data);
        }

        return $this;
    }

    /**
     * Load the tags from the database and put them in the model
     *
     * @param News_Model_Article $model 
     */
    public function loadTags(News_Model_Article $model)
    {
        $cacheid = 'NewsArticle_Tags' . $this->_getCacheId(array('id' => $model->getId()));
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false === ($tags = $cache->load($cacheid))) {
            $table = new Mcmr_Db_Table('news_tags');

            $tags = array();
            $rows = $table->fetchAll(array('article_id=?' => $model->getId()));
            foreach ($rows as $row) {
                $tags[] = $row['tag_name'];
            }

            $cache->save($tags, $cacheid, array('news_model_article_id_' . $model->getId()));
        }

        $model->setTags($tags);
    }

    /**
     * Deletes tags from the DB based on the conditions
     *
     * @param array $where
     * @return News_Model_ArticleMapper
     */
    private function _deleteTags(array $where)
    {
        $table = new Mcmr_Db_Table('news_tags');
        $table->delete($where);

        return $this;
    }

    /**
     * Takes a field name and converts it into the database field name
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        $field = strtolower($field);
        switch ($field) {
            case 'user':
            case 'user_id':
            case 'userid':
                $field = 'user_id';
                break;

            case 'user_id_author':
            case 'useridauthor':
            case 'userauthor':
                $field = 'user_id_author';
                break;

            case 'type':
            case 'typeid':
            case 'type_id':
                $field = 'type_id';
                break;

            case 'section':
            case 'sectionid':
            case 'section_id':
                $field = 'section_id';
                break;

            case 'category':
            case 'category_id':
            case 'categoryid':
                $field = 'category_id';
                break;

            case 'set':
            case 'set_id':
            case 'setid':
                $field = 'set_id';
                break;

            case 'article_useridauthor':
            case 'useridauthor':
                $field = 'user_id_author';
                break;

            case 'article_userrole':
            case 'userrole':
                $field = 'user_role';
                break;

            default:
                if (0 !== strncmp('attr_', $field, 5)) {
                    $field = 'article_' . $field;
                }
                break;
        }
        return $field;
    }

    /**
     * Get the maximum amount of time a fetch all can be cached for. An index may
     * need to expire early dues to timed articles
     *
     * @return int|null 
     */
    protected function _maxCacheLifetime(Zend_Cache_Core $cache, $conditions)
    {
        $now = time();
        $select = $this->getDbTable()->select();
        $select->from($this->getDbTable(), $this->_selectFields())
                ->where('article_publishdate >= FROM_UNIXTIME(?)', $now)
                ->where('article_published = 1')
                ->order('article_publishdate asc');
        
        if (!empty($condition)) {
            foreach ($condition as $field => $value) {
                if (!in_array($field, array('published', 'publishdate'))) {
                    $select = $this->_addCondition($select, $field, $value);
                }
            }
        }
        
        $next = $this->getDbTable()->fetchRow($select);
        
        if ($next) {
            $cacheLifetime = array(
                $cache->getBackend()->getLifetime(false), 
                ($next->article_publishdate - $now),
            );
            
            return min($cacheLifetime);
        } else {
            return null;
        }
    }

    /*
    protected function _postAllOrder($select) 
    {
        Mcmr_Debug::dump((string)$select);
        return $select;
    }

    protected function _postOneOrder($select) 
    {
        Mcmr_Debug::dump((string)$select);
        return $select;
    }
    */
}
