<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A news article set model class
 *
 * @category News
 * @package News_Model
 * @subpackage Set
 */
class News_Model_Set extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'News_Model_SetMapper';
    
    /**
     * Primary Key
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * The Url key for this set
     *
     * @var string
     */
    protected $_url = null;

    /**
     * Human readable Title
     *
     * @var string
     */
    protected $_title = null;

    protected $_articleids = null;

    private $_urlchanged = false;

    /**
     * Return mapper for model
     *
     * @return News_Model_SetMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * 
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     *
     * @param int $id
     * @return News_Model_Set
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }
        
        return $this->_url;
    }

    /**
     *
     * @param string $url
     * @return News_Model_Set
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }
        
        $this->_url = $url;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     *
     * @param string $title
     * @return News_Model_Set
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        if (null == $this->_url) {
            $this->setUrl($title);
        }

        return $this;
    }

    public function getArticleids()
    {
        if (null === $this->_articleids) {
            $this->_articleids = array();
        }

        return $this->_articleids;
    }

    public function setArticleids($articleids)
    {
        $this->_articleids = $articleids;

        return $this;
    }

    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }

    /**
     * Return the articles in this news set
     *
     * @param int|array $page
     * @return Zend_Paginator
     */
    public function getArticles($conditions=null, $order=null, $page = null)
    {
        $mapper = News_Model_Article::getMapper();
        
        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['setid']=$this->getId();
        
        // Order articles, newest to oldest
        if (null === $order) {
            $order = array('order'=>'asc', 'publishdate'=>'desc', 'createdate'=>'desc');
        }
        $articles = $mapper->findAllByField($conditions, $order, $page);

        return $articles;
    }
}
