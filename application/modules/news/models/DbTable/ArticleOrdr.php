<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ArticleAttr.php 718 2010-06-01 13:33:37Z leigh $
 */

/**
 * Description of ArticleAttr
 *
 * @category News
 * @package News_Model
 * @subpackage DbTable
 */
class News_Model_DbTable_ArticleOrdr extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'news_articles_ordr';
    protected $_primary = array('article_id', 'ordr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insertIgnore($this->_rowData);
        } else {
            parent::insertIgnore($data);
        }
    }

    public function setOrdrName($name)
    {
        $this->_rowData['ordr_name'] = $name;

        return $this;
    }

    public function setOrdrValue($value)
    {
        $this->_rowData['ordr_value'] = (int)$value;

        return $this;
    }

    public function setOrdrId($id)
    {
        $this->_rowData['article_id'] = $id;

        return $this;
    }
}
