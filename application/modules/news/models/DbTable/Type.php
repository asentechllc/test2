<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 696 2010-05-24 16:38:48Z leigh $
 */

/**
 * Database table class for the article types table
 *
 * @category News
 * @package News_Model
 * @subpackage DbTable
 */
class News_Model_DbTable_Type extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'news_types';
    protected $_primary = 'type_id';
}
