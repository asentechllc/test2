<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Article.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Description of Article
 *
 * @category News
 * @package News_Model
 * @subpackage DbTable
 */
class News_Model_DbTable_Article extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'news_articles';
    protected $_primary = 'article_id';
}
