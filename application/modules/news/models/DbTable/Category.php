<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Category.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * DbTable for Category. Describes the database table
 *
 * @category News
 * @package News_Model
 * @subpackage DbTable
 */
class News_Model_DbTable_Category extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'news_categories';
    protected $_primary = 'category_id';
}
