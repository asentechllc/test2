<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ArticleAttr.php 718 2010-06-01 13:33:37Z leigh $
 */

/**
 * Description of ArticleAttr
 *
 * @category News
 * @package News_Model
 * @subpackage DbTable
 */
class News_Model_DbTable_ArticleAttr extends Mcmr_Db_Table_Abstract implements Mcmr_Model_AttrInterface
{
    protected $_name = 'news_articles_attr';
    protected $_primary = array('article_id', 'attr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insertIgnore($this->_rowData);
        } else {
            parent::insertIgnore($data);
        }
    }

    public function setAttrName($name)
    {
        $this->_rowData['attr_name'] = $name;

        return $this;
    }

    public function setAttrValue($value)
    {
        $this->_rowData['attr_value'] = $value;

        return $this;
    }

    public function setAttrId($id)
    {
        $this->_rowData['article_id'] = $id;

        return $this;
    }

    public function setAttrType($type)
    {
        $this->_rowData['attr_type'] = $type;

        return $this;
    }
}
