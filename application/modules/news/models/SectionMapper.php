<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all Section models
 *
 * @category News
 * @package News_Model
 * @subpackage SectionMapper
 */
class News_Model_SectionMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'News_Model_DbTable_Section';
    protected $_columnPrefix = 'section_';
    protected $_modelClass = 'News_Model_Section';
    protected $_cacheIdPrefix = 'NewsSection';

    
    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        $options = $model->getOptions();
        $data = array();
        foreach ($options as $field=>$value) {
            $field = $this->_sanitiseField($field);
            $value = $this->_sanitiseValue($field, $value);
            if (null !== $value) {
                $data[$field] = $value;
            }
        }

        if (null === ($id = $model->getId())) {
            // Perform an insert
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);
            $this->_saveArticles($model);

            // Notify Observers
            $this->notify('insert', $model);
        } else {
            // Perform an update
            $this->getDbTable()->update($data, array($this->_columnPrefix.'id=?'=>$id));
            $this->_saveArticles($model);

            // Notify observers
            $this->notify('update', $model);
        }

        return $this;
    }

    /**
     * Takes a field name and converts it into the database field name
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
    	switch($field){
    		case 'parent_id':
    			break;

    		default:
    			$field = parent::_sanitiseField($field);
    	}
    	
    	return $field;
	}
	
    protected function _convertRowToModel($row)
    {
        $model = parent::_convertRowToModel($row);

        if (null !== $model) {
            $model->setParentId($row['parent_id']);
        }

        return $model;
    }
}
