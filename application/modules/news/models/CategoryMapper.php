<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CategoryMapper.php 2142 2011-01-31 17:16:40Z michal $
 */

/**
 * Mapper class for all news article category models
 *
 * @category News
 * @package News_Model
 * @subpackage CategoryMapper
 */
class News_Model_CategoryMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'News_Model_DbTable_Category';
    protected $_dbAttrTableClass = 'News_Model_DbTable_CategoryAttr';
    protected $_dbOrdrTableClass = 'News_Model_DbTable_CategoryOrdr';
    protected $_modelClass = 'News_Model_Category';
    protected $_columnPrefix = 'category_';
    protected $_cacheIdPrefix = 'NewsCategory';



    protected function _sanitiseValues($data) 
    {
        unset($data['category_typeid']);
        unset($data['category_order']);
        return $data;
    }


    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'type':
            case 'typeid':
            case 'type_id':
                return 'type_id';
                break;
            default:
                return parent::_sanitiseField( $field ) ;
        }
    } //_sanitiseValue



    protected function _sanitiseValue( $field, $value ) 
    {
        switch ( $field ) {
        case 'category_sites':
            if (is_array($value)) {
                $value = implode(',', $value);
            }
            break;
        default:
            $value = parent::_sanitiseValue( $field, $value ) ;
        }
        return $value;
    } //_sanitiseValue



    public function findOrCreate($title)
    {
        $url = Mcmr_StdLib::urlize($title);
        $category = $this->findOneByfield(array('url' => $url));
        if (null === $category) {
            $category = new News_Model_Category();
            $category->setTitle($title);
            $this->save($category);
        }
        return $category;
    }

    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($model)
    {
        $url = $model->getUrl();
        $id = $model->getId();
        
        if (null !== $id) {
            return (null !== $this->findOneByField(array('url' => $url, 'id'=>array('condition'=>'<>', 'value'=>$id))));
        } else {
            return (null !== $this->findOneByField(array('url' => $url)));
        }
    }
   

    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        $originalValue = $value;
        if (is_array($value)) {
            $condition = $value['condition'];
            $value = $value['value'];
        } else {
            $condition = '=';
        }
        if  ('category_sites' == $field) {
            $this->_addSitesCondition( $select, $field, $value );
        } else {
            parent::_addCondition($select, $field, $originalValue);
        }
        return $select;
    }


}
