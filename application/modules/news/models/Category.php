<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Category.php 1888 2010-12-01 16:12:45Z leigh $
 */

/**
 * Model class for all news categories
 *
 * @category News
 * @package News_Model
 * @subpackage Category
 */
class News_Model_Category extends Mcmr_Model_Ordered
{
    static protected $_mapperclass = 'News_Model_CategoryMapper';

    protected $_id = null;
    protected $_featured = null;
    protected $_typeid = null;
    protected $_url = null;
    protected $_title = null;
    protected $_sites = null;

    private $_urlchanged = false;

    /**
     * Return mapper for model
     *
     * @return News_Model_CategoryMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Return the category ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the category ID
     *
     * @param int $id
     * @return News_Model_Category
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }

    /**
     * Get the featured flag. If not set it will default to false
     *
     * @return bool 
     */
    public function getFeatured()
    {
        if (null === $this->_featured) {
            $this->_featured = false;
        }

        return intval ($this->_featured);
    }

    /**
     * Set whether this category is featured or not
     *
     * @param bool $featured
     * @return News_Model_Category 
     */
    public function setFeatured($featured)
    {
        $this->_featured = (true == $featured);
        
        return $this;
    }

    /**
     * Return the ID of this category Type
     *
     * @return int
     */
    public function getTypeid()
    {
        return $this->_typeid;
    }

    /**
     * Set the category type id
     *
     * @param int $typeid
     * @return News_Model_Category
     */
    public function setTypeid($typeid)
    {
        $this->_typeid = intval($typeid);

        return $this;
    }

    /**
     * Get the Category URL
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $url = Mcmr_StdLib::urlize($this->getTitle());

            if ($url !== $this->_url) {
                $this->_urlchanged = true;
            }
            
            $this->_url = $url;
        }
        
        return $this->_url;
    }

    /**
     * Set the category URL
     *
     * @param string $url
     * @return News_Model_Category
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }

    /**
     * Get the Category title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the category Title
     *
     * @param string $title
     * @return News_Model_Category
     */
    public function setTitle($title)
    {
        // Change the URL with the title change
        if (null !== $this->_title) {
            $this->setUrl($title);
        }
        
        $this->_title = trim($title);

        return $this;
    }

    
    /**
     * Get the sites that the category is published on. 
     * The sites are returned as a comma separated string of site identifiers.
     *
     * @return string 
     */
    public function getSites() 
    {
        //if nothing is set then assume the article is published on the current site
        if (null === $this->_sites) {
            $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
            $this->_sites = array($site);
        }
        return $this->_sites;
    }
    
    
    /**
     * Set the sites that the article is published on. 
     *
     */
    public function setSites($sites)
    {
        if (is_string($sites)) {
            $sites = explode(',', $sites);
        }
        $this->_sites = $sites;
    }
    
    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }
        
        return $changed;
    }

    /**
     * Return the articles in this news type
     *
     * @param int|array $page
     * @return Zend_Paginator
     */
    public function getArticles($conditions=null, $order=null, $page = null)
    {
        $mapper = News_Model_Article::getMapper();

        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['categoryid'] = $this->getId();
        
        if (!isset($conditions['sites'])) {
            $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
            $conditions['sites'] = $site;
        }

        // Order articles, newest to oldest
        if (null === $order) {
            $order = array('order'=>'asc', 'publishdate'=>'desc', 'createdate'=>'desc');
        }
        
        $articles = $mapper->findAllByField($conditions, $order, $page);

        return $articles;
    }
}
