<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all Set models
 *
 * @category News
 * @package News_Model
 * @subpackage SetMapper
 */
class News_Model_SetMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'News_Model_DbTable_Set';
    protected $_columnPrefix = 'set_';
    protected $_modelClass = 'News_Model_Set';
    protected $_cacheIdPrefix = 'NewsSet';

    
    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        $options = $model->getOptions();
        $data = array();
        foreach ($options as $field=>$value) {
            $field = $this->_sanitiseField($field);
            $value = $this->_sanitiseValue($field, $value);
            if (null !== $value) {
                $data[$field] = $value;
            }
        }

        if (null === ($id = $model->getId())) {
            // Perform an insert
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);
            $this->_saveArticles($model);

            // Notify Observers
            $this->notify('insert', $model);
        } else {
            // Perform an update
            $this->getDbTable()->update($data, array($this->_columnPrefix.'id=?'=>$id));
            $this->_saveArticles($model);

            // Notify observers
            $this->notify('update', $model);
        }

        return $this;
    }
    
    /**
     * @see Mcmr_Model_GenericMapper::save()
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'set_articleids':
                $value = null;
                break;
        }

        return $value;
    }
    
    /**
     * Converts a row returned by database query to an instance of model class.
     * @param Zend_Db_Table_Row_Abstract $row
     * @param instance of the model class
     */
    protected function _convertRowToModel( $row )
    {
        $model = parent::_convertRowToModel($row);
        $this->_loadArticles($model);

        return $model;
    }

    /**
     * Load the articles for the set into the set
     *
     * @param type $model
     * @return News_Model_SetMapper 
     */
    protected function _loadArticles($model)
    {
        $table = new Mcmr_Db_Table('news_article_sets');
        $rows = $table->fetchAll(array('set_id=?'=>$model->getId()));

        $articles = array();
        foreach ($rows as $row) {
            $articles[] = $row['article_id'];
        }

        $model->setArticleids($articles);

        return $this;
    }

    /**
     * Save the set article association
     *
     * @param type $model
     * @return News_Model_SetMapper 
     */
    protected function _saveArticles($model)
    {
        $this->_deleteArticles($model);
        $table = new Mcmr_Db_Table('news_article_sets');
        $data['set_id'] = $model->getId();

        foreach ($model->getArticleids() as $article) {
            $data['article_id'] = $article;
            $table->insert($data);
        }

        return $this;
        
    }

    /**
     * Delete the article set association
     * 
     * @param type $model
     * @return News_Model_SetMapper 
     */
    protected function _deleteArticles($model)
    {
        $table = new Mcmr_Db_Table('news_article_sets');
        $table->delete(array('set_id=?'=>$model->getId()));

        return $this;
    }
}
