<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 1391 2010-09-14 14:23:59Z leigh $
 */

/**
 * Model class for news type models
 *
 * @category News
 * @package News_Model
 * @subpackage Type
 */
class News_Model_Type extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'News_Model_TypeMapper';
    
    /**
     * Primary Key
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * The Url key for this type
     *
     * @var string
     */
    protected $_url = null;

    /**
     * Human readable Title
     *
     * @var string
     */
    protected $_title = null;

    private $_urlchanged = false;

    /**
     * Return mapper for model
     *
     * @return News_Model_TypeMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the type unique ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the type unique ID
     *
     * @param int $id
     * @return News_Model_Type
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }

    /**
     * Get the type string ID
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }
        
        return $this->_url;
    }

    /**
     * Set the type string ID
     *
     * @param string $url
     * @return News_Model_Type
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }
        
        $this->_url = $url;

        return $this;
    }

    /**
     * Get the type title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the type title
     *
     * @param string $title
     * @return News_Model_Type
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }

    /**
     * Return the articles in this news type
     *
     * @param int|array $page
     * @return Zend_Paginator
     */
    public function getArticles($conditions=null, $order=null, $page = null)
    {
        $mapper = News_Model_Article::getMapper();
        
        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['typeid']=$this->getId();
        
        if (!isset($conditions['sites'])) {
            $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
            $conditions['sites'] = $site;
        }
        
        // Order articles, newest to oldest
        if (null === $order) {
            $order = array('order'=>'asc', 'publishdate'=>'desc', 'createdate'=>'desc');
        }
        $articles = $mapper->findAllByField($conditions, $order, $page);

        return $articles;
    }
}
