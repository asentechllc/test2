<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Article.php 2390 2011-05-03 10:44:30Z leigh $
 */

/**
 * Model class for all news articles
 *
 * @category News
 * @package News_Model
 * @subpackage Article
 */
class News_Model_Article extends Mcmr_Model_ArticleAbstract
{
    const PAGE_BREAK = '<!-- pagebreak -->';

    static protected $_mapperclass = 'News_Model_ArticleMapper';
    
    private $_urlchanged = false;
    private $_type = null;
    private $_user = null;
    private $_userAuthor = null;
    private $_categoryModels = null;
    private $_contentPaginator = null;
    
    protected $_id = null;
    protected $_userrole = null;
    protected $_createdate = null;
    protected $_publishdate = null;
    protected $_published = null;
    protected $_featured = null;
    protected $_userid = null;
    protected $_userIdAuthor = null;
    protected $_authorname = null;
    protected $_authoremail = null;
    protected $_typeid = null;
    protected $_categoryids = null;
    //red herring for the 'introspection' to trigger reading the titles
    protected $_categorytitles = null;
    protected $_stream = null;
    protected $_url = null;
    protected $_title = null;
    protected $_strap = null;
    protected $_intro = null;
    protected $_content = null;
    protected $_image = null;
    protected $_sites = null;
    protected $_tags = array();

    protected $_numcomment = 0;
    protected $_numread = 0;
    protected $_numemail = 0;

    /**
     * Return mapper for model
     *
     * @return News_Model_ArticleMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Return the Article ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the Article ID
     *
     * @param int $id
     * @return News_Model_Article
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }

    /**
     * Get the minimum role required to view this article.
     * Default 'guest'
     *
     * @return string 
     */
    public function getUserrole()
    {
        if (null === $this->_userrole) {
            $this->_userrole = 'guest';
        }
        
        return $this->_userrole;
    }

    /**
     * Set the minimum role required to view this article.
     *
     * @param string $role
     * @return News_Model_Article 
     */
    public function setUserrole($role)
    {
        $this->_userrole = $role;
        
        return $this;
    }

        
    /**
     * Get the article creation date as a timestamp
     *
     * @return int
     */
    public function getCreatedate()
    {
        if (null === $this->_createdate) {
            $this->_createdate = time();
        }

        return $this->_createdate;
    }

    /**
     * Set the creation date as a timestamp
     *
     * @param int $createdate
     * @return News_Model_Article
     */
    public function setCreatedate($createdate)
    {
        $this->_createdate = intval($createdate);

        return $this;
    }

    /**
     * Get the published flag.
     *
     * @return boolean
     */
    public function getPublished()
    {
        if (null === $this->_published) {
            $this->_published = false;
        }

        return intval($this->_published);
    }

    /**
     * Set the published flag
     *
     * @param boolean $published
     */
    public function setPublished($published)
    {
        $this->_published = $published;
    }

    /**
     * Get the publish date as a timestamp
     *
     * @return int
     */
    public function getPublishdate()
    {
        if (null === $this->_publishdate) {
            $this->_publishdate = time();
        }

        return $this->_publishdate;
    }

    /**
     * Set the publish date as a timestamp
     *
     * @param int $publishdate
     * @return News_Model_Article
     */
    public function setPublishdate($publishdate)
    {
        $this->_publishdate = intval($publishdate);

        return $this;
    }

    /**
     * Return the featured flag
     *
     * @return bool
     */
    public function getFeatured()
    {
        if (null === $this->_featured) {
            $this->_featured = false;
        }

        return intval($this->_featured);
    }

    /**
     * Set the featured flag
     *
     * @param bool $featured
     * @return News_Model_Article
     */
    public function setFeatured($featured)
    {
        $this->_featured = intval($featured);
        return $this;
    }

    /**
     * Get the User ID of the person who created this article
     *
     * @return int
     */
    public function getUserid()
    {
        if (null === $this->_userid) {
            $this->_userid = Zend_Registry::get('authenticated-user')->getId();
        }
        return $this->_userid;
    }

    /**
     * Set the publisher user
     *
     * @param int $userid
     * @return News_Model_Article
     */
    public function setUserid($userid)
    {
        $this->_userid = $userid;

        return $this;
    }

    /**
     * Get the Author user ID
     *
     * @return int
     */
    public function getUserIdAuthor()
    {
        if (null === $this->_userIdAuthor) {
            if (null!==($user=Zend_Registry::get('authenticated-user'))) {
                $this->_userIdAuthor = $user->getId();
            }
        }
        return $this->_userIdAuthor;
    }

    /**
     * Set the author user ID
     *
     * @param int $userIdAuthor
     * @return News_Model_Article
     */
    public function setUserIdAuthor($userIdAuthor)
    {
        $this->_userIdAuthor = $userIdAuthor;

        return $this;
    }

    /**
     * Get the Author's name
     *
     * @return string
     */
    public function getAuthorname()
    {
        return $this->_authorname;
    }

    /**
     * Set the Author's name
     *
     * @param string $authorname
     * @return News_Model_Article
     */
    public function setAuthorname($authorname)
    {
        $this->_authorname = $authorname;

        return $this;
    }

    /**
     * Get the Author's email address
     *
     * @return string
     */
    public function getAuthoremail()
    {
        return $this->_authoremail;
    }

    /**
     * Set the Author's email address
     *
     * @param string $authoremail
     * @return News_Model_Article
     */
    public function setAuthoremail($authoremail)
    {
        $this->_authoremail = $authoremail;

        return $this;
    }

    /**
     * Get the article Type ID
     *
     * @return int
     */
    public function getTypeid()
    {
        if (null === $this->_typeid) {
            $mapper = News_Model_Type::getMapper();
            $type = $mapper->findOneByField();
            if (null !== $type) {
                $this->_typeid = $type->getId();
            }
        }
        return $this->_typeid;
    }

    /**
     * Set the article Type ID
     *
     * @param int $typeid
     * @return News_Model_Article
     */
    public function setTypeid($typeid)
    {
        // If the typeid changes reset the type object
        if ($typeid !== $this->_typeid) {
            $this->_type = null;
        }

        $this->_typeid = $typeid;

        return $this;
    }

    public function getCategorytitles()
    {
        $models = $this->getCategories();
        $titles = array();
        if (null !== $models) {
            foreach ($models as $categoryModel) {
                if (null !== $categoryModel) {
                    $titles[] = $categoryModel->getTitle();
                }
            }
            return implode(',', $titles);
        } else {
            return null;
        }
    }

    public function setCategorytitles($categories)
    {
        if (is_string($categories)) {
            $categories = explode(',', $categories);
        }
        $categoryMapper = News_Model_Category::getMapper();
        $ids = array();
        foreach ($categories as $categoryTitle) {
            $categoryModel = $categoryMapper->findOrCreate($categoryTitle);
            $ids[] = $categoryModel->getId();
        }
        $this->setCategoryids($ids);
        return $this;
    }

    /**
     * Get the article Category ID
     *
     * @return int
     */
    public function getCategoryids()
    {
        if (null === $this->_categoryids) {
            // Lazy load the categories. Don't fetch from storage unless requested
            $this->getMapper()->loadCategories($this);
        }

       return $this->_categoryids;
    }

    /**
     * Set the article Category ID
     *
     * @param array $categories
     * @return News_Model_Article
     */
    public function setCategoryids($categories)
    {
        if (null === $categories) {
            $categories = array();
        }

        if (!is_array($categories)) {
            $categories = explode(',', $categories);
        }

        $categories = array_unique( $categories );
        $this->_categoryids = $categories;
        return $this;
    }

    /**
     * Display this article in the News Steam
     *
     * @return boolean
     */
    public function getStream()
    {
        if (null === $this->_stream) {
            $this->_stream = false;
        }

        return $this->_stream;
    }

    /**
     * Set whether to display this article in the News Stream
     *
     * @param boolean $stream
     * @return News_Model_Article
     */
    public function setStream($stream)
    {
        $this->_stream = $stream;

        return $this;
    }

    /**
     * Get the article URL string
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url && null !== $this->_title) {
            $this->_url = Mcmr_StdLib::urlize($this->_title);
            $this->_urlchanged = true;
        }

        return $this->_url;
    }

    /**
     * Set the URL string
     *
     * @param string $url
     * @return News_Model_Article
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }

    /**
     * Get the article title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the article title
     *
     * @param string $title
     * @return News_Model_Article
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the article strap line
     *
     * @return string
     */
    public function getStrap()
    {
        return $this->_strap;
    }

    /**
     * Set the article strap line
     *
     * @param string $strap
     * @return News_Model_Article
     */
    public function setStrap($strap)
    {
        $this->_strap = $strap;

        return $this;
    }

    /**
     * Get the article introduction
     *
     * @return string
     */
    public function getIntro()
    {
        if (null === $this->_intro) {
            $this->_intro = '';
        }

        return $this->_intro;
    }

    /**
     * Set the article introduction
     *
     * @param string $intro
     * @return News_Model_Article
     */
    public function setIntro($intro)
    {
        $this->_intro = $intro;

        return $this;
    }

    /**
     * Get the article content. Setting $paginate to true will return the data
     * as a Zend_Paginator object
     *
     * @param bool $paginate Return a Zend_Paginator object
     * @return string|Zend_Paginator
     */
    public function getContent($paginate = false)
    {
        if ($paginate) {
            if (null == $this->_contentPaginator) {
                $pages = explode(self::PAGE_BREAK, $this->_content);
                $this->_contentPaginator = Mcmr_Paginator::factory($pages);
                $this->_contentPaginator->setItemCountPerPage(1);
            }

            return $this->_contentPaginator;
        }

        return $this->_content;
    }

    /**
     * Set the article content
     *
     * @param string $content
     * @return News_Model_Article
     */
    public function setContent($content)
    {
        $this->_contentPaginator = null;
        $this->_content = $content;

        return $this;
    }

    /**
     * Get the article main image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * Set the article main image
     *
     * @param string $image
     * @return News_Model_Article
     */
    public function setImage($image)
    {
        $this->_image = $image;

        return $this;
    }

    /**
     * Get an array of all the article tags
     *
     * @return array
     */
    public function getTags()
    {
        if (null === $this->_tags) {
            // Lazy load the tags. Don't fetch from storage unless requested
            $this->getMapper()->loadTags($this);
        }

        return $this->_tags;
    }

    
    /**
     * Get the sites that the article is published on. 
     * The sites are returned as a comma separated string of site identifiers.
     *
     * @return array 
     */
    public function getSites() 
    {
        //if nothing is set then assume the article is published on the current site
        if (null === $this->_sites) {
            $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
            $this->_sites = array($site);
        }
        return $this->_sites;
    }
    
    
    /**
     * Set the sites that the article is published on. 
     *
     */
    public function setSites($sites)
    {
        if (is_string($sites)) {
            $sites = explode(',', $sites);
        }
        $this->_sites = $sites;
    }

    /**
     * Set the tags for this article
     *
     * @param string|array $tags
     * @return News_Model_Article
     */
    public function setTags($tags)
    {
        if (is_array($tags)) {
            $this->_tags = $tags;
        } elseif (is_string($tags)) {
            $this->_tags = explode(',', $tags);
        } else {
            throw new Mcmr_Model_Exception('Invalid value for Article Tags');
        }

        return $this;
    }

    /**
     *
     * @return News_Model_Type
     */
    public function getType()
    {
        if (null === $this->_type) {
            $mapper = News_Model_Type::getMapper();
            $this->_type = $mapper->find($this->getTypeid());
        }

        return $this->_type;
    }

    /**
     *
     * @return User_Model_User
     */
    public function getUser()
    {
        if (null === $this->_user) {
            $mapper = User_Model_User::getMapper();
            $this->_user = $mapper->find($this->getUserid());
        }

        return $this->_user;
    }

    /**
     * Get the author object
     *
     * @return User_Model_User
     */
    public function getUserAuthor()
    {
        if (null === $this->_userAuthor) {
            $userid = $this->getUserIdAuthor();
            if ($userid) {
                $mapper = User_Model_User::getMapper();
                $this->_userAuthor = $mapper->find($userid);
            }
        }

        return $this->_userAuthor;
    }

    /**
     * Returns an array of all the category model objects
     *
     * @return array
     */
    public function getCategories()
    {
        if (null === $this->_categoryModels) {
            $mapper = News_Model_Category::getMapper();
            $this->_categoryModels = array();
            $ids = $this->getCategoryids();
            if (count($ids)) {
                $this->_categoryModels = $mapper->findAllByfield(array('id'=>array('condition'=>'in', 'value'=>$ids)))->toArray();
            } else {
                $this->_categoryModels = array();
            }
        }

        return $this->_categoryModels;
    }

    /**
     * Fetch the related articles.
     *
     * @param bool $published Only display the published articles
     * @param string|array $sites The sites the articles should be fetched from
     * @return Mcmr_Paginator 
     */
    public function getRelatedByCategory($published = true, $sites=null, $count=3)
    {
        $categoryids = $this->getCategoryids();
        if (!empty($categoryids)) {
            $mapper = News_Model_Article::getMapper();
            $fields = array(
                    'category' => array('condition' => 'IN', 'value' => $categoryids),
                    'id' => array('condition' => '<>', 'value' => $this->getId()),
            );
            
            if (null !== $sites) {
                $fields['sites'] = $sites;
            }
            
            if ($published) {
                $fields['published'] = true;
            }
            
            $order = array('publishdate' => 'desc');
            return $mapper->findAllByField($fields, $order,array('page'=>1,'count'=>$count));
        } else {
            return new Mcmr_Paginator(new Zend_Paginator_Adapter_Array(array()));
        }
    }

    public function getNumcomment(){return $this->_numcomment;}
    public function getNumread(){return $this->_numread;}
    public function getNumemail(){return $this->_numemail;}

    public function setNumcomment($n){$this->_numcomment = $n; return $this;}
    public function setNumread($n){$this->_numread = $n; return $this;}
    public function setNumemail($n){$this->_numemail = $n; return $this;}

    /**
     * Unpublish the article
     *
     * @return News_Model_Article
     */
    public function unpublish()
    {
        $this->_published = false;

        return $this;
    }

    /**
     * Publish the article
     *
     * @return News_Model_Article
     */
    public function publish()
    {
        $this->_published = true;

        return $this;
    }

    /**
     * Whether the article has been published yet
     *
     * @return boolean
     */
    public function isPublished()
    {
        return (time() > $this->_publishdate && $this->_publishdate !== 0 && $this->_published);
    }

    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }

    public function getControllerName()
    {
        return "index";
    }

}
