<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A news article section model class
 *
 * @category News
 * @package News_Model
 * @subpackage Section
 */
class News_Model_Section extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'News_Model_SectionMapper';

    /**
     * Primary Key
     *
     * @var int
     */
    protected $_id = null;

    /**
     * The Url key for this section
     *
     * @var string
     */
    protected $_url = null;

    /**
     * Human readable Title
     *
     * @var string
     */
    protected $_title = null;
    protected $_parentid = null;

    private $_parent = null;

    private $_urlchanged = false;

    /**
     * Return mapper for model
     *
     * @return News_Model_SectionMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->_parentid;
    }

    /**
     *
     * @param int $id
     * @return News_Model_Section
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }

    /**
     *
     * @param int $id
     * @return News_Model_Section
     */
    public function setParentId($id)
    {
        $this->_parentid = intval($id);

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }

        return $this->_url;
    }

    /**
     *
     * @param string $url
     * @return News_Model_Set
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     *
     * @param string $title
     * @return News_Model_Section
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        if (null == $this->_url) {
            $this->setUrl($title);
        }

        return $this;
    }

    /**
     * Get parent section
     * @return string
     */
    public function getParent()
    {
        if (null === $this->_parent) {
            $this->_parent = self::getMapper()->find($this->_parentid);
        }

        return $this->_parent;
    }

    /**
     * Get child sections
     * @return
     **/
     public function getChildren(){
     	return self::getMapper()->findAllByField(array(
     		'parent_id' => $this->getId(),
     	),array(
            'order' => 'asc',
        ))->toArray();
     }

     public static function getParents(){
     	return self::getMapper()->findAllByField(array(
     		'parent_id' => 0,
     	),array(
     		'order' => 'asc',
     	))->toArray();
     }

     public function getIds(){
     	$ids = array($this->getId());
     	$children = $this->getChildren();
     	foreach($children as $child){
     		$ids[] = $child->getId();
     	}
     	return $ids;
     }

}
