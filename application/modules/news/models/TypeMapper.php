<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: TypeMapper.php 694 2010-05-24 14:47:27Z leigh $
 */

/**
 * Description of TypeMapper
 *
 * @category News
 * @package News_Model
 * @subpackage TypeMapper
 */
class News_Model_TypeMapper extends Mcmr_Model_MapperAbstract
{
    protected $_dbTableClass = 'News_Model_DbTable_Type';

    public function __construct()
    {
        parent::__construct();
        
        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
    }

    /**
     * Save a news type model to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return News_Model_TypeMapper 
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        // Sanitise all model data for the database
        $data = $this->_addColumnPrefix($model->getOptions(), 'type_');
        
        if (null === ($id = $model->getId())) {
            if ($this->_urlExists($model->getUrl())) {
                throw new Mcmr_Model_Exception("URL already exists. Url must be unique");
            }

            // Brand new record. Insert it.
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);

            $model->state(Mcmr_Model_ModelAbstract::STATE_CLEAN);

            // Notify observers
            $this->notify('insert', $model);
        } else {
            if ($model->urlChanged() && $this->_urlExists($model->getUrl())) {
                throw new Mcmr_Model_Exception("URL already exists. Url must be unique");
            }
            
            $this->getDbTable()->update($data, array('type_id=?' => $id));
            
            $model->state(Mcmr_Model_ModelAbstract::STATE_CLEAN);
            
            // Notify observers
            $this->notify('update', $model);
        }

        return $this;
    }

    /**
     * Delete the type information from the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        $id = $model->getId();
        $this->getDbTable()->delete(array('type_id=?' => $id));

        // @todo Move news items to another type, or delete the news items.

        // Notify observers
        $this->notify('delete', $model);

        return $this;
    }

    /**
     * Find a type based on the condition
     *
     * @param array $condition
     * @return News_Model_Type
     */
    public function findOneByField($condition = null)
    {
        $cacheid = 'NewsType_findOneByField'.$this->_getCacheId($condition);
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($type = $cache->load($cacheid))) {
            $table = $this->getDbTable();
            $select = $table->select();

            if (null !== $condition) {
                foreach ($condition as $field => $value) {
                    $select->where('type_' . $field . ' = ?', $value);
                }
            }

            $row = $table->fetchRow($select);

            if (is_object($row)) {
                $row = $row->toArray();

                $type = new News_Model_Type();
                $type->setOptions($this->_stripColumnPrefix($row, 'type_'));

                $type->state(Mcmr_Model_ModelAbstract::STATE_CLEAN);
                $cache->save($type, $cacheid, array('news_model_type_id_'.$type->getId()));
            } else {
                $type = null;

                $cache->save($type, $cacheid);
            }

            $this->notify('cacheMiss', $type);
        }

        if (null !== $type) {
            // Notify the observers of a load
            $this->notify('load', $type);
        }

        return $type;
    }

    /**
     * Get a paginator object based on the condition
     *
     * @param array|null $condition
     * @param array|null $order
     * @param array|int $page
     * @return Zend_Paginator
     */
    public function findAllByField($condition = null, $order=null, $page = 1)
    {
        if (is_array($page)) {
            $countPerPage = $page['count'];
            $page = $page['page'];
        } else {
            $countPerPage = 10;
        }
        
        // A Bug in Zend_Paginator means the cache is not working. We have to do it ourselves.
        // http://framework.zend.com/issues/browse/ZF-6989
        $cachecondition = $condition;
        $cachecondition['page'] = $page;
        $cachecondition['count'] = $countPerPage;
        $cacheid = 'NewsType_findAllByField'.$this->_getCacheId($cachecondition, $order);
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($paginator = $cache->load($cacheid))) {
            $select = $this->getDbTable()->select();

            if (is_array($condition)) {
                foreach ($condition as $field => $value) {
                    $select->where('type_' . $field . ' = ?', $value);
                }
            }

            if (!empty($order)) {
                $orderclause = array();
                foreach ($order as $field=>$direction) {
                    $orderclause[] = "type_$field $direction";
                }

                $select->order($orderclause);
            }

            // Load the paginator. Use this->filter to map items to individual module objects
            $paginator = new Mcmr_Paginator(new Zend_Paginator_Adapter_DbTableSelect($select));
            $paginator->setItemCountPerPage($countPerPage);
            $paginator->setFilter(new Mcmr_Filter_Callback(array($this, 'filter')));
            $paginator->setCurrentPageNumber($page);
            $paginator->getCurrentItems(); // Force the items to be retrieved from the database now for the cache

            $cache->save($paginator, $cacheid, array('news_model_type_index'));

            $this->notify('cacheMiss', $paginator);
        }

        // Notify observers of a paginator load
        $this->notify('load', $paginator);
        
        return $paginator;
    }

    /**
     * Takes Zend_Db_Table rows and maps them to the model.
     *
     * @param array $rows
     * @return array
     */
    public function filter($rows)
    {
        $models = array();
        foreach ($rows as $row) {
            // Use the mapper find. This is more taxing on first load. But afterwards every user will be cached.
            // This prevents a full DB hit if a single user is updated.
            $model = $this->find($row['type_id']);

            $models[] = $model;
        }

        return $models;
    }

    /**
     * Check if the URL string exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($url)
    {
        return (null !== $this->findOneByField(array('url'=> $url)));
    }
}
