<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 840 2010-06-17 13:50:49Z leigh $
 */

/**
 * Bootstrap
 *
 * @category News
 * @package News_Bootstrap
 */
class News_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');

        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'News_View_Helper');
    }

    public function _initRoute()
    {
        $router = $this->getApplication()->getResource('frontcontroller')->getRouter();
        $context = new Zend_Controller_Router_Route_Regex(
            '(?html|xml|rss|json)', array('format'=>'html'), array(1=>'format')
        );

        $route = new Zend_Controller_Router_Route(
            'news/index/:action/:url',
            array(
                'module'=>'news',
                'controller'=>'index',
            )
        );
        //$chain = $route->chain($context,'.');
        $router->addRoute('news', $route);
    }
}
