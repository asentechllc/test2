<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Category
 * @package    Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Order.php 2196 2011-02-15 13:03:05Z michal $
 */

/**
 * A form for changing the display order of categories
 *
 * @category   Category
 * @package    Form
 * @subpackage Order
 */
class Chart_Form_CategoryOrder extends Mcmr_Form
{
    private $_categories = array();
    private $_type = null;
    private $_models = null;

    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'chart' . DIRECTORY_SEPARATOR
                . 'category_form_order-'.strtolower($type).'.ini';
            $this->_type = $type;
        }

        parent::__construct($options);
    }

    public function postInit()
    {
        $this->setName('categoryformorder')->setElementsBelongTo('category-form-order');
        $this->setMethod('post');


        // Get the type this is limited to and set conditions accordingly.
        $conditions = array();
        if (isset($this->_categories['conditions'])) {
            $conditions = $this->_categories['conditions'];
        }
        $order = array('title' => 'asc');
        // Get the categories.
        $mapper = Chart_Model_Category::getMapper();
        $page['page'] = 1;
        $page['count'] = isset($this->_categories['count'])?$this->_categories['count']:10;
        $optionCount = isset($this->_articles['optionCount'])?$this->_articles['optionCount']:100;
        
        $categories = $mapper->findAllByField($conditions, $order, $page);
        $this->_models = $categories;
        $subform = new Mcmr_Form();
        $subform->setElementsBelongTo('categories');

        $x = 1;
        $displayGroupElements = array();
        $typeValue = 'alphabetical';
        foreach ($categories as $category) {
            $categoryOrder = $category->getOrder($this->_type);
            if ((null !== $categoryOrder)&&(0 !== $categoryOrder)) {
                $typeValue = 'specific';
            }

            $value = ((null !== $categoryOrder) && (0 !== $categoryOrder))?$category->getId():'';
            $element = array(
                'options' => array(
                    'label' => 'Category ' . ($x),
                    'required' => true,
                    'value' => $value,
                    'class' => 'sortable category_search'
                ),
                'optionSource' => array(
                    'mapper' => 'Chart_Model_Category',
                    'conditions' => $conditions,
                    'order' => array('title' => 'asc'),
                    'page' => array('page' => '1', 'count' => $optionCount),
                    'valueField' => 'id',
					'displayField' => 'title',
					'allowNull'=>true,
                    'allowNullDisplay'=>'Please Select...'
                )
            );
            $element = $this->_processElement($element);
            $subform->addElement('Select', "$x", $element['options']);
            $displayGroupElements[] = "$x";
            $x++;
        }

        if (isset($displayGroupElements[0])) {
            $subform->addDisplayGroup($displayGroupElements, 'categories', array('class'=>'sortable-items'));
        }

        $this->addSubForm($subform, 'categories');
        // Set the value of the 'orderby' field
        $orderby = $this->getElement('orderby');
        if (null !== $orderby) {
            $orderby->setValue($typeValue);
        }
    }

    public function setCategories($categories)
    {
        $this->_categories = $categories;
    }

    public function getCategories()
    {
        return $this->_categories;
    }
    
    public function getCategoryModels()
    {
        return $this->_models;
    }

}
