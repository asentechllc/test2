<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Chart.php 2376 2011-04-21 10:23:28Z leigh $
 */

/**
 * Form class for Charts
 *
 * @category Chart
 * @package Chart_Form
 * @subpackage Chart
 */
class Chart_Form_Chart extends Mcmr_Form
{
    private $_chartElements = array();
    private $_type = null;

    public function  __construct($options=null, $elements=null )
    {
        if (null !== $elements) {
            $this->_chartElements = $elements;
        } elseif (isset($options['elements'])) {
            $this->_chartElements = $options['elements'];
        } else {
            $config = Zend_Registry::get('chart-config');
            $this->_chartElements = array_fill(0, $config->numEntries, array());
        }

        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $this->_type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $this->_type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'chart'
            . DIRECTORY_SEPARATOR . 'chart_form_chart-'.strtolower($this->_type).'.ini';
        }

        parent::__construct($options);
    }

    public function init()
    {
        parent::init();
        $this->setName('chartformchart')->setElementsBelongTo('chart-form-chart');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        foreach ($this->_chartElements as $x=>$element) {
            $entryform = new Chart_Form_Entry(array('type'=>$this->_type));
            $entryform->setElementsBelongTo("entries[$x]");
            $entryform->removeDecorator('Form');
            if (!empty($element)) {
                //$element = $this->_chartElements[$x];
                if (is_object($element)) {
                    $element = $element->getOptions(true);
                }
                $entryform->populate($element);
            } else {
                $entryform->populate(array('position'=>($x+1)));
            }

            $this->addSubForm($entryform, 'entry'.$x);
        }

        $this->addElement(
            'Submit', 'submit',
            array(
                'ignore' => true ,
                'label' => 'Save Page'
            )
        );

        // Add some CSRF protection
        /*
        
        $this->addElement('hash', 'csrf', array(
            'salt' => 'unique'));
            
        */

    }


    
    
}
