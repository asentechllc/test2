<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Form class for Chart categories
 *
 * @category Chart
 * @package Chart_Form
 * @subpackage Category
 */
class Chart_Form_Category extends Mcmr_Form
{
    public function  init()
    {
        $this->setName('chartformcategory')->setElementsBelongTo('chart-form-category');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }
}
