<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Form class for Chart Types
 *
 * @category Chart
 * @package Chart_Form
 * @subpackage Type
 */
class Chart_Form_Type extends Mcmr_Form
{
    public function  init()
    {
        $this->setName('chartformtype')->setElementsBelongTo('chart-form-type');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }
}
