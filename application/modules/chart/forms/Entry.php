<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Entry.php 2374 2011-04-21 09:49:44Z leigh $
 */

/**
 * Form class for Chart Entries
 *
 * @category Chart
 * @package Chart_Form
 * @subpackage Entry
 */
class Chart_Form_Entry extends Mcmr_Form
{
    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'chart'
            . DIRECTORY_SEPARATOR . 'chart_form_entry-'.strtolower($type).'.ini';
        }

        parent::__construct($options);
    }
    
    public function init()
    {
        $this->setName('chartformentry')->setElementsBelongTo('chart-form-entry');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }

}
