<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Chart
 * @package    Chart_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Fetch a single chart type
 *
 * @category   Chart
 * @package    Chart_View
 * @subpackage Helper
 */
class Chart_View_Helper_ChartTypeRead extends Zend_View_Helper_Abstract
{
    /**
     * Display a single chart type
     *
     * @param string $partial Template file for this view helper
     */
    public function chartTypeRead($conditions, $order=null, $partial=null)
    {
        $mapper = Chart_Model_Type::getMapper();
        $type = $mapper->findOneByField($conditions, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('type'=>$type));
                }
            }

            return $this->view->partial($partial, array('type'=>$type));
        }

        return $type;
    }
}
