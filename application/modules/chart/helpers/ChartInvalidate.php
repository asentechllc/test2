<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Chart
 * @package    Chart_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ChartRead.php 2204 2011-02-17 16:02:41Z leigh $
 */

/**
 * Invalidate a chart.
 * This helper encapsulate all logic of marking chart as dirty.
 * This is done on an external request, so the helper checks for a post request, a correct secret passed, 
 *
 * @category   Chart
 * @package    Chart_View
 * @subpackage Helper
 */
class Chart_View_Helper_ChartInvalidate extends Zend_View_Helper_Abstract
{
    /**
     * Invaidate a chart. All input read form the request directly.
     *
     * @return null on success, error message otheriwise
     */
    public function chartInvalidate()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        if (!$request->isPost()) {
            return "You have to use POST";
        }
        $secret = $request->getPost('secret');
        if (null==$secret || $secret != $this->_getSecret()) {
            return "Missing or incorrect secret";
        }
        $chartId = $request->getPost('chart');
        if (null==$chartId) {
            return "Missing chart id";
        }
        $result = $this->_findAndInvalidateChart($chartId);
        if (!$result) {
            return "Missing chart";
        } 
        return null;
    }

    protected function _getSecret() 
    {
        $config = Zend_Registry::get('chart-config');
        if (null != $config->api && null != $config->api->secret) {
            return $config->api->secret;
        }
        return null;
    }

    protected function _findAndInvalidateChart($chartId) 
    {
        $mapper = Chart_Model_Chart::getMapper();
        $chart = $mapper->findOneByField(array('attr_mwchart_id'=>$chartId));
        if (null===$chart) {
            return false;
        }
        $chart->setAttribute('dirty', 'yes');
        $mapper->save($chart);
        return true;
    }
}
