<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Chart
 * @package    Chart_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ChartRead.php 2204 2011-02-17 16:02:41Z leigh $
 */

/**
 * Display a single chart category
 *
 * @category   Chart
 * @package    Chart_View
 * @subpackage Helper
 */
class Chart_View_Helper_ChartCategoryRead extends Zend_View_Helper_Abstract
{
    /**
     * Display a single chart
     *
     * @param int $id The Chart ID
     * @param string $partial Template file for this view helper
     */
    public function chartCategoryRead($conditions, $order=null, $partial=null)
    {
        $mapper = Chart_Model_Category::getMapper();
        $category = $mapper->findOneByField($conditions, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('category'=>$category));
                }
            }

            return $this->view->partial($partial, array('category'=>$category));
        }

        return $category;
    }
}
