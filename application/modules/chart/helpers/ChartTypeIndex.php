<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ChartTypeIndex.php 1104 2010-07-22 11:44:57Z leigh $
 */

/**
 * A View Helper for retrieving an index of product types
 *
 * @category Chart
 * @package Chart_View
 * @subpackage Helper
 */
class Chart_View_Helper_ChartTypeIndex extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve an index of product types based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function chartTypeIndex($conditions = array(), $order=null, $page=null, $partial = null)
    {
        $mapper = Chart_Model_Type::getMapper();
        $types = $mapper->findAllByField($conditions, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('types'=>$types));
                }
            }

            return $this->view->partial($partial, array('categories'=>$types));
        }
        return $types;
    }

}