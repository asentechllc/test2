<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Controller for all Chart categories
 *
 * @category Chart
 * @package Chart_CategoryController
 */
class Chart_CategoryController extends Zend_Controller_Action
{
    public function init()
    {
        try {
            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                            ->addActionContext('index', 'json')
                            ->addActionContext('index', 'html')
                            ->addActionContext('create', 'html')
                            ->addActionContext('create', 'json')
                            ->addActionContext('update', 'html')
                            ->addActionContext('update', 'json')
                            ->addActionContext('read', 'html')
                            ->addActionContext('read', 'json')
                            ->initContext();
        } catch (Exception $e) {

        }
    }

    /**
     * Display an index of chart categories
     */
    public function indexAction()
    {
        $fields = array();

        $parentId = $this->_getParam('parentId');
        if (null !== $parentId) {
            $fields['parentid'] = (int)$parentId;
        }
        
        $order = array();
        $orderField = $this->_getParam('order');
        if (null !== $orderField) {
            $order[$orderField] = 'desc';
        }
        $order['title'] = 'asc';

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('chart-config')->categories->itemCountPerPage);

        $mapper = Chart_Model_Category::getMapper();
        
        $this->view->categories = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Create a new chart category
     */
    public function createAction()
    {
        $form = $this->_helper->loadForm('Category');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $category = new Chart_Model_Category();
                $mapper = Chart_Model_Category::getMapper();
                $category->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($category);

                    $this->view->getHelper('DisplayMessages')->addMessage('chartCategorySaved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $category);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
            }
        }
    }

    /**
     * Read a single chart category
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        if (null !== $id || null !== $url) {
            $mapper = Chart_Model_Category::getMapper();
            $category = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            if (null !== $category) {
                $this->view->category = $category;
            } else {
                throw new Mcmr_Exception_PageNotFound('Chart Category does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a single chart category
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
        $form = $this->_helper->loadForm('Category');
            $this->view->form = $form;

            $mapper = Chart_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $this->view->category = $category;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $category->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($category);

                            $this->view->getHelper('DisplayMessages')->addMessage('chartCategorySaved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $category);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
                    }
                } else {
                    $form->populate($category->getOptions(true));
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Chart Category does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a single chart category
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Chart_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $this->view->category = $category;
                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($category);
                        
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('chartCategoryDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $category);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Chart Category does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }
    
    /**
     * Modify the user index display order. Display form on GET. Save on POST
     */
    public function orderAction()
    {
        $default = Zend_Registry::get('chart-config')->category->order?Zend_Registry::get('chart-config')->category->order->default:'default';
        $type = $this->_request->getParam('type', $default);
        $form = $this->_helper->loadForm('CategoryOrder', array('type'=>$type));
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $mapper = Chart_Model_Category::getMapper();

                // Reset the ordering before resaving the order
                try {
                    $mapper->resetOrdering($type);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }

                if (array_key_exists('orderby', $values) && 'alphabetical' == $values['orderby']) {
                    $this->view->getHelper('DisplayMessages')->addMessage('Ordering Reset to Alphabetical', 'info');
                } else {
                    foreach ($values['categories'] as $order=>$id) {
                        $category = $mapper->find($id);
                        if (null !== $category) {
                            $category->setOrder(1000-$order, $type); // Order is done in reverse to accomodate null values
                            try {
                                $mapper->save($category);
                            } catch (Exception $e) {
                                $this->view->error = true;
                                $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                            }
                        }
                    }
                    $this->view->getHelper('DisplayMessages')->addMessage('Ordering set to Specific', 'info');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Form values were invalid', 'warn');
            }
        }
    }    
}
