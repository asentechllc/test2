<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2375 2011-04-21 10:22:08Z leigh $
 */

/**
 * Index Controller for the Chart module
 *
 * @category Chart
 * @package Chart_IndexController
 * @subpackage 
 */
class Chart_IndexController extends Mcmr_Controller_Action
{
    public function init()
    {
        parent::init();
        $contextSwitch = $this->_helper->getHelper('ContextSwitch');
        $contextSwitch->addContext('html', array())
            ->addContext(
                'csv',
                array(
                    'headers'=>array('Content-Type'=>'text/csv'),
                    'suffix'=>'csv',
                )
            );

        $contextSwitch->addActionContext('read', 'csv')
                ->initContext();
        
    }

    /**
     * Create a new Chart. Display form on GET, Save on POST
     */
    public function createAction()
    {
        $typeUrl = $this->_getParam('type', Zend_Registry::get('chart-config')->defaultType);
        $type = Chart_Model_Type::getMapper()->findOneByField(array('url' => $typeUrl));
        if (null !== $type) {
            $typeUrl = $type->getUrl();
        } else {
            $typeUrl = '';
        }
        $form = $this->_helper->loadForm('Chart', array('type' => $typeUrl));
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $model = new Chart_Model_Chart();
                $values = $form->getValues(true);

                // We need to get the entries out of the post
                $post = $this->_request->getPost();
                $values['entries'] = $post['chartformchart']['entries'];
                $model->setOptions($values);
                if (null !== $type) {
                    $model->setTypeid($type->getId());
                }
                $this->view->model = $model;

                try {
                    Chart_Model_Chart::getMapper()->save($model);
                    $this->view->getHelper('DisplayMessages')->addMessage('Chart Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $model);
                    $this->view->error = false;
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Incorrect Form Values', 'warn');
            }
        }
    }

    protected function _readAfterFind()
    {
        parent::_readAfterFind();
        $this->_helper->viewRenderer('read/'.$this->_model->getType()->getUrl());
    }

    /**
     * Update a chart. Display form on GET. Save on POST
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Chart_Model_Chart::getMapper();
            $model = $mapper->find($id);
            if (null === $model) {
                throw new Mcmr_Exception_PageNotFound('Chart not found');
            }
            $this->view->model = $model;

            // Load the form based on the chart type (if set)
            $type = '';
            if (null != $model->getType() && $model->getType() instanceof Chart_Model_Type) {
                $type = $model->getType()->getUrl();
            }
            $form = $this->_helper->loadForm(
                'Chart',
                array('type' => $type, 'elements' => $this->view->model->getEntries())
            );
            $this->view->form = $form;

            if ($this->_request->isPost()) {
                if ($form->isValid($this->_request->getPost())) {
                    $values = $form->getValues(true);

                    // We need to get the entries out of the post
                    $post = $this->_request->getPost();
                    $values['entries'] = $post['chartformchart']['entries'];
                    $model->setOptions($values);
                    $this->view->model = $model;

                    try {
                        $mapper->save($model);
                        $this->view->getHelper('DisplayMessages')->addMessage('Chart Saved', 'info');
                        $this->view->getHelper('Redirect')->notify('update', $model);
                        $this->view->error = false;
                    } catch (Exception $e) {
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        $this->view->error = true;
                    }
                } else {
                    $this->view->getHelper('DisplayMessages')->addMessage('Incorrect Form Values', 'warn');
                    $this->view->error = true;
                }
            } else {
                $form->populate($model->getOptions(true));
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a Chart. Display confirmation on GET. Delete on POST
     */
    public function deleteAction()
    {
        $id = $this->_request->getParam('id', null);

        if (null !== $id) {
            $mapper = Chart_Model_Chart::getMapper();
            if (null !== ($model = $mapper->find($id))) {
                $this->view->model = $model;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($model);
                        $this->view->getHelper('DisplayMessages')->addMessage('Chart Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $model);
                    } catch (Exception $e) {
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessages(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Chart Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

}
