<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Controller for all Chart Types
 *
 * @category Chart
 * @package Chart_TypeController
 */
class Chart_TypeController extends Zend_Controller_Action
{
    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('ContextSwitch');
            $contextSwitch->addContext('html', array())
                ->addContext(
                    'csv',
                    array(
                        'headers'=>array('Content-Type'=>'text/csv'),
                        'suffix'=>'csv',
                    )
                );

            $contextSwitch->addActionContext('read', 'csv')
                    ->initContext();
            
            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                            ->addActionContext('index', 'json')
                            ->addActionContext('index', 'html')
                            ->addActionContext('create', 'html')
                            ->addActionContext('create', 'json')
                            ->addActionContext('update', 'html')
                            ->addActionContext('update', 'json')
                            ->addActionContext('read', 'html')
                            ->addActionContext('read', 'json')
                            ->initContext();
        } catch (Exception $e) {

        }
    }

    /**
     * Display an index of chart types
     */
    public function indexAction()
    {
        $fields = array();

        $order = array('title'=>'asc');

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('chart-config')->types->itemCountPerPage);

        $mapper = Chart_Model_Type::getMapper();
        
        $this->view->types = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Create a new chart type
     */
    public function createAction()
    {
        $form = $this->_helper->loadForm('Type');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $type = new Chart_Model_Type();
                $mapper = Chart_Model_Type::getMapper();
                $type->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($type);

                    $this->view->getHelper('DisplayMessages')->addMessage('chartTypeSaved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $type);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
            }
        }
    }

    /**
     * Read a single chart type
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        if (null !== $id || null !== $url) {
            $mapper = Chart_Model_Type::getMapper();
            $type = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            if (null !== $type) {
                $this->_helper->viewRenderer('read/'.$type->getUrl());
                $this->view->type = $type;
            } else {
                throw new Mcmr_Exception_PageNotFound('Chart Type does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a single chart type
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
        $form = $this->_helper->loadForm('Type');
            $this->view->form = $form;

            $mapper = Chart_Model_Type::getMapper();
            if (null !== ($type = $mapper->find($id))) {
                $this->view->type = $type;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $type->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($type);

                            $this->view->getHelper('DisplayMessages')->addMessage('chartTypeSaved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $type);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
                    }
                } else {
                    $form->populate($type->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Chart Type does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a single chart type
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Chart_Model_Type::getMapper();
            if (null !== ($type = $mapper->find($id))) {
                $this->view->type = $type;
                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($type);
                        
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('chartTypeDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $type);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Chart Type does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }
}
