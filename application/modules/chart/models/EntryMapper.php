<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: EntryMapper.php 1909 2010-12-09 19:05:43Z michal $
 */

/**
 * Mapper class for all Game models
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage EntryMapper
 */
class Chart_Model_EntryMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Chart_Model_DbTable_Entry';
    protected $_dbAttrTableClass = 'Chart_Model_DbTable_EntryAttr';
    protected $_columnPrefix = 'entry_';
    protected $_modelClass = 'Chart_Model_Entry';
    protected $_cacheIdPrefix = 'ChartEntry';

    /**
     * Convert a readable fieldname into the database fieldname
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'chart_id':
            case 'chartid':
            case 'chart':
                $field = 'chart_id';
                break;

            case 'game_id':
            case 'gameid':
            case 'game':
                $field = 'game_id';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }
        return $field;
    }
}
