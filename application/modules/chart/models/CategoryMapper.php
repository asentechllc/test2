<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all Chart categories
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage CategoryMapper
 */
class Chart_Model_CategoryMapper extends Mcmr_Model_GenericMapper
{

    protected $_dbTableClass = 'Chart_Model_DbTable_Category';
    protected $_dbAttrTableClass = 'Chart_Model_DbTable_CategoryAttr';
    protected $_dbOrdrTableClass = 'Chart_Model_DbTable_CategoryOrdr';
    protected $_columnPrefix = 'category_';
    protected $_modelClass = 'Chart_Model_Category';
    protected $_cacheIdPrefix = 'ChartCategory';
    
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        // Ensure the URL is unique
        if (null === $model->getId() || $model->urlChanged()) {
            $model->setUrl($this->getUniqueUrl($model));
        }
        
        parent::save($model);
    }
}
