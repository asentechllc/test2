<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all Chart categorys
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage Category
 */
class Chart_Model_Category extends Mcmr_Model_Ordered
{
    static protected $_mapperclass = 'Chart_Model_CategoryMapper';

    protected $_id = null;
    protected $_parentId = null;
    protected $_title = '';
    protected $_url = null;
    protected $_description = null;
    
    private $_urlchanged = false;
    /**
     * Return mapper for model
     *
     * @return Chart_Model_CategoryMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }
    
    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Chart_Model_Category
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }

    /**
     * Get the ID of the parent category
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->_parentId;
    }

    /**
     * Set the ID of the parent category
     *
     * @param int $parentId
     * @return Chart_Model_Category 
     */
    public function setParentId($parentId)
    {
        $this->_parentId = $parentId;
        
        return $this;
    }

        
    /**
     * Get the model title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the title
     *
     * @param int $title
     * @return Chart_Model_Category
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the category description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the category description
     *
     * @param string $description
     * @return Chart_Model_Category 
     */
    public function setDescription($description)
    {
        $this->_description = $description;
        
        return $this;
    }
        
    /**
     * Return the URL ID for the Job
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }

        return $this->_url;
    }

    /**
     * Set the URL ID for the category
     *
     * @param string $url
     * @return Chart_Model_Category
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }

    /**
     * Return the charts in this chart category
     *
     * @param int|array $page
     * @return Zend_Paginator
     */
    public function getCharts($conditions=null, $order=null, $page = null)
    {
        $mapper = Chart_Model_Chart::getMapper();

        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['categoryid'] = $this->getId();

        $charts = $mapper->findAllByField($conditions, $order, $page);

        return $charts;
    }
    
    /**
     * Return the chart types in this chart category
     *
     * @param int|array $page
     * @return Zend_Paginator
     */
    public function getTypes($conditions=null, $order=null, $page = null)
    {
        $mapper = Chart_Model_Type::getMapper();

        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['categoryid'] = $this->getId();

        $types = $mapper->findAllByField($conditions, $order, $page);

        return $types;
    }
    
    /**
     * Get the parent Category
     * 
     * @return Chart_Model_Category
     */
    public function getParent()
    {
        $mapper = self::getMapper();
        
        $id = $this->getParentId();
        if (0 !== $id) {
            return $mapper->find($id);
        } else {
            return null;
        }
    }
    
    /**
     * Fetch all the children of this category
     *
     * @param int $count 
     * @param string $order
     * @return Zend_Paginator
     */
    public function getChildren($count = 10, $order = null)
    {
        $mapper = self::getMapper();

        return $mapper->findAllByField(array('parentId'=>$this->getId()), $order, $count);
    }
    
    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }

}
