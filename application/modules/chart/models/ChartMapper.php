<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ChartMapper.php 2370 2011-04-18 17:01:47Z leigh $
 */

/**
 * Mapper class for all Chart models
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage GameMapper
 */
class Chart_Model_ChartMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Chart_Model_DbTable_Chart';
    protected $_dbAttrTableClass = 'Chart_Model_DbTable_ChartAttr';
    protected $_columnPrefix = 'chart_';
    protected $_modelClass = 'Chart_Model_Chart';
    protected $_cacheIdPrefix = 'Chart';

    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {

        $options = $model->getOptions();
        $data = array();
        foreach ($options as $field=>$value) {
            $field = $this->_sanitiseField($field);
            $value = $this->_sanitiseValue($field, $value);
            $data[$field] = $value;
        }
        $data = $this->_sanitiseValues($data);

        if (null === ($id = $model->getId())) {
            // Perform an insert
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);

            if (null !== $this->_dbAttrTableClass) {
                $this->_saveAttributes($model);
            }

            $this->_saveEntries($model);

            // Notify Observers
            $this->notify('insert', $model);
        } else {
            $this->notify('preUpdate', $this->find($model->getId()));
            // Perform an update
            $this->getDbTable()->update($data, array($this->_columnPrefix.'id=?'=>$id));

            if (null !== $this->_dbAttrTableClass) {
                $this->_deleteAttributes(array($this->_columnPrefix.'id=?'=>$id));
                $this->_saveAttributes($model);
            }

            $this->_deleteEntries($model);
            foreach ($model->getEntries() as $entry) {
                $entry->state(Mcmr_Model_ModelAbstract::STATE_INITIALISED);
            }
            $this->_saveEntries($model);
            
            // Notify observers
            $this->notify('update', $model);
        }

        return $this;
    }

    /**
     * Find the previous chart to the provided instance
     *
     * @param Chart_Model_Chart $model
     * @return Chart_Model_Chart
     */
    public function findPrevious(Chart_Model_Chart $chart)
    {
        $cacheid = 'Chart_Previous'.$chart->getId();
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($previous = $cache->load($cacheid))) {
            $previous = new Chart_Model_Chart();
            $table = $this->getDbTable();
            $select = $table->select()
                    ->from(
                        $table,
                        array(Zend_Db_Table_Select::SQL_WILDCARD, 'UNIX_TIMESTAMP(chart_date) as chart_date',)
                    )
                    ->where('chart_date < FROM_UNIXTIME(?)', $chart->getDate())
                    ->where('type_id = ?', $chart->getTypeid())
                    ->where('platform_id = ?', $chart->getPlatformid())
                    ->where('chart_id <> ?', $chart->getId())
                    ->order('chart_date DESC');
            $row = $table->fetchRow($select);
            if (is_object($row)) {
                $row = $row->toArray();
                $previous->setOptions($this->_stripColumnPrefix($row, $this->_columnPrefix));
                $this->_loadEntries($previous);
            }

            $this->notify('cacheMiss', $previous);
            $cache->save($previous, $cacheid, array('chart_model_chart_id_'.$previous->getId()));
        }
        $this->notify('load', $previous);

        return $previous;
    }
    
    /**
     * Fetch all of the chart entries from storage and put them into the chart model
     *
     * @param Chart_Model_Chart $chart
     * @return Chart_Model_ChartMapper 
     */
    public function loadEntries(Chart_Model_Chart $chart)
    {
        $entrymapper = Chart_Model_Entry::getMapper();
        $entries = $entrymapper->findAllByField(
            array('chartid'=>$chart->getId()),
            array('position'=>'asc'),
            array( 'page'=>1, 'count'=>1000000)
        );
        $chart->setEntries($entries->getCurrentItems());

        return $this;
    }
    
    /**
     * Convert a readable fieldname into the database fieldname
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'userrole':
            case 'user_role':
            case 'role':
                $field = 'user_role';
                break;
            
            case 'platformid':
            case 'platform':
                $field = 'platform_id';
                break;
            
            case 'typeid':
            case 'type':
                $field = 'type_id';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }
        
        return $field;
    }

    /**
     * Load all of the chart entry models out of the database
     *
     * @param Chart_Model_Chart $chart
     * @return Chart_Model_ChartMapper 
     * @deprecated
     */
    private function _loadEntries(Chart_Model_Chart $chart)
    {
        return $this->loadEntries($chart);
    }

    /**
     * Save the chart entries
     *
     * @param Chart_Model_Chart $chart
     * @return Chart_Model_ChartMapper
     */
    private function _saveEntries(Chart_Model_Chart $chart)
    {
        $entrymapper = Chart_Model_Entry::getMapper();
        $entries = $chart->getEntries();
        foreach ($entries as $entry) {
            if ($entry instanceof Chart_Model_Entry) {
                $entry->setChartid($chart->getId());
                $entrymapper->save($entry);
            }
        }

        return $this;
    }

    /**
     * Delete all the chart entry models out of the database
     *
     * @param Chart_Model_Chart $chart
     * @return Chart_Model_ChartMapper 
     */
    private function _deleteEntries(Chart_Model_Chart $chart)
    {
        $entrymapper = Chart_Model_Entry::getMapper();
        $entries = $entrymapper->findAllByfield(
            array( 'chartid'=>$chart->getId()),
            null,
            array( 'page'=>1, 'count'=>1000000)
        );
        foreach ($entries as $entry) {
            $entrymapper->delete($entry);
        }
        
        return $this;
    }

    /**
     * Convert field value before inserting into databse query. 
     * 
     * @param string $field field name passed to be able to tell which filed you are dealing with.
     * @param mixed $value field value as stored in  the object. Process if necessary and return.
     * @return Zend_Db_Expr|string|int field value in the format suitable for a database query. 
     */
    protected function _sanitiseValue( $field, $value ) 
    {
        switch ( $field ) {
        case 'chart_date':
            return new Zend_Db_Expr("FROM_UNIXTIME({$value})");
        default:
            return $value;
        }
    } //_sanitiseValue

    /**
     * Create a list of fields to be read form the databse in SELECT query.
     * By default read all fields using sql wildcard.
     * Subclasses can append to this if some calculated fields are required or replace with a list of specific fields.
     * 
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        $all = parent::_selectFields();
        $all[] = "UNIX_TIMESTAMP(chart_date) AS chart_date";
        return $all;
    } //_selectFields
    
}
