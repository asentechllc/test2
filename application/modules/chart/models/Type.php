<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all Chart Types
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage Type
 */
class Chart_Model_Type extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Chart_Model_TypeMapper';

    protected $_id = null;
    protected $_categoryid = null;
    protected $_title = '';
    protected $_url = null;

    private $_urlchanged = false;
    
    /**
     * Return mapper for model
     *
     * @return Chart_Model_TypeMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }
    
    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Get the category ID
     *
     * @return int
     */
    public function getCategoryid()
    {
        return $this->_categoryid;
    }

    /**
     * Set the category ID
     *
     * @param int $categoryid
     * @return Chart_Model_Chart 
     */
    public function setCategoryid($categoryid)
    {
        $this->_categoryid = $categoryid;
        
        return $this;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Chart_Model_Type
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }

    /**
     * Get the model title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the title
     *
     * @param int $title
     * @return Chart_Model_Type
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Return the URL ID for the Job
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }

        return $this->_url;
    }

    /**
     * Set the URL ID for the Job
     *
     * @param string $url
     * @return Job_Model_Job
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }

    /**
     * Fetch the category model this type is a member of.
     *
     * @return Chart_Model_Category 
     */
    public function getCategory()
    {
        if ($this->getCategoryid()) {
            return Chart_Model_Category::getMapper()->find($this->getCategoryid());
        } else {
            return null;
        }
    }
    
    /**
     * Return the charts in this chart type
     *
     * @param int|array $page
     * @return Zend_Paginator
     */
    public function getCharts($conditions=null, $order=null, $page = null)
    {
        $mapper = Chart_Model_Chart::getMapper();

        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['typeid'] = $this->getId();

        $charts = $mapper->findAllByField($conditions, $order, $page);

        return $charts;
    }

    public function getChart($conditions=null, $order=null)
    {
        $mapper = Chart_Model_Chart::getMapper();

        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['typeid'] = $this->getId();

        $chart = $mapper->findOneByField($conditions, $order);

        return $chart;
    }

    public function getLatestChart($conditions=null)
    {
        $order = array('date' => 'desc');
        
        return $this->getChart($conditions, $order);
    }
    
    public function getOldestChart($conditions=null)
    {
        $order = array('date' => 'asc');
        
        return $this->getChart($conditions, $order);
    }
    
    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }
}
