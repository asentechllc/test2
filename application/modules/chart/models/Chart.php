<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Chart.php 2183 2011-02-10 11:54:06Z leigh $
 */

/**
 * Model class for all Charts
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage Chart
 */
class Chart_Model_Chart extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Chart_Model_ChartMapper';

    private $_urlchanged = false;

    /**
     * Chart ID
     * @var int
     */
    protected $_id = null;
    protected $_published = null;
    protected $_title = null;
    protected $_url = null;
    
    /**
     * The minimum role to view this content. Requires use of the 'Gated' ACLs
     * 
     * @var string
     */
    protected $_userrole = null;
    
    /**
     * ID of the chart type
     * @var int
     */
    protected $_typeid = null;
    
    /**
     * ID of the chart category
     * @var int
     */
    protected $_categoryid = null;
    
    /**
     * Date the chart was created as a unix timestamp
     * @var int
     */
    protected $_date = null;
    
    /**
     * Title of the chart
     * @var string
     */
    
    /**
     * ID of the chart platform
     * @var int
     */
    protected $_platformid = null;
    
    private $_entries = array();
    private $_previous = null;

    /**
     * Return mapper for model
     *
     * @return Chart_Model_ChartMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Chart_Model_Chart
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }

    /**
     * Get the minimum role required to view this chart.
     * Default 'guest'
     *
     * @return string 
     */
    public function getUserrole()
    {
        if (null === $this->_userrole) {
            $this->_userrole = 'guest';
        }
        
        return $this->_userrole;
    }

    /**
     * Set the minimum role required to view this product.
     *
     * @param string $role
     * @return Chart_Model_Chart 
     */
    public function setUserrole($role)
    {
        $this->_userrole = $role;
        
        return $this;
    }
    
    /**
     * Return the Type ID for this chart
     *
     * @return int
     */
    public function getTypeid()
    {
        if (null === $this->_typeid) {
            $this->_typeid = 0;
        }

        return $this->_typeid;
    }
    
    /**
     * Set the chart's type ID
     *
     * @param int $typeid
     * @return Chart_Model_Chart
     */
    public function setTypeid($typeid)
    {
        $this->_typeid = intval($typeid);

        return $this;
    }

    /**
     * Get the model title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the title
     *
     * @param int $title
     * @return Chart_Model_Chart
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Return Unix timestamp of the chart date
     *
     * @return int
     */
    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = time();
        }
        
        return $this->_date;
    }

    /**
     * Set the chart date as a timestamp
     *
     * @param int $date
     * @return Chart_Model_Chart
     */
    public function setDate($date)
    {
        $this->_date = $date;

        return $this;
    }

    /**
     * Return the platform ID
     *
     * @return int
     */
    public function getPlatformid()
    {
        return $this->_platformid;
    }

    /**
     * Set the platform ID
     *
     * @param int $platformid
     * @return Chart_Model_Chart
     */
    public function setPlatformid($platformid)
    {
        $this->_platformid = intval($platformid);

        return $this;
    }

    /**
     * Return the chart platform
     *
     * @return Game_Model_Platform
     */
    public function getPlatform()
    {
        if ($this->getPlatformid()) {
            return Game_Model_Platform::getMapper()->find($this->getPlatformid());
        } else {
            return null;
        }
    }

    /**
     * Return an array of all the chart entries. Each element is an Entry Model
     * Object
     *
     * @return array
     */
    public function getEntries()
    {
        if (empty($this->_entries)) {
            self::getMapper()->loadEntries($this);
        }
        
        // Ensure the entries are in the position order
        ksort($this->_entries);
        
        return $this->_entries;
    }

    /**
     * Set the chart entries. Each entry must be an associative array with the entry
     * information, or an Entry Model object.
     *
     * Exception thrown if data is of wrong type
     *
     * @param array $entries
     * @return Chart_Model_Chart
     * @throws Mcmr_Model_Exception
     */
    public function setEntries($entries)
    {
        if ( null === $entries ) {
            return;
        }
        // Convert any array entries into entry models
        foreach ($entries as $entry) {
            $this->addEntry($entry);
        }

        return $this;
    }

    /**
     *
     * @param Chart_Model_Entry|array $entry 
     */
    public function addEntry($entry)
    {
        if (is_array($entry)) {
            $entryObj = new Chart_Model_Entry();
            $entryObj->setOptions($entry, true);
            $entryObj->setChart($this);
            $this->_entries[$entry['position']] = $entryObj;
        } elseif ($entry instanceof Chart_Model_Entry) {
            $this->_entries[$entry->getPosition()] = $entry;
        } else {
            throw new Mcmr_Model_Exception('Chart Entries must be an array or an Entry Model');
        }
        
        return $this;
    }
    
    public function clearEntries()
    {
        $this->_entries = array();
        
        return $this;
    }
    
    /**
     * Return the chart that was created previous to this one
     *
     * @return Chart_Model_Chart
     */
    public function getPrevChart()
    {
        $mapper = Chart_Model_Chart::getMapper();
        return $mapper->findPrevious($this);
    }

    /**
     * Return the chart type model for this chart
     *
     * @return Chart_Model_Type
     */
    public function getType()
    {
        if (null !== $this->getTypeid()) {
            return Chart_Model_Type::getMapper()->find($this->getTypeid());
        } else {
            return null;
        }
    }
    
    public function getPrevious() {
        if (null === $this->_previous) {
            $this->_previous = self::getMapper()->findPrevious($this);
        }
        return $this->_previous;
    }

    /**
     * Fetch the job published flag
     *
     * @return bool
     */
    public function getPublished()
    {
        if (null === $this->_published) {
            $this->_published = false;
        }

        return $this->_published;
    }

    /**
     * Set the job published flag
     *
     * @param bool $published
     * @return Job_Model_Job 
     */
    public function setPublished($published)
    {
        $this->_published = (true == $published);

        return $this;
    }

    public function isPublished()
    {
        return $this->getPublished();
    }

    public function getUrl()
    {
        if (null === $this->_url && null !== $this->_title) {
            $this->_url = Mcmr_StdLib::urlize($this->_title);
            $this->_urlchanged = true;
        }

        return $this->_url;
    }

    /**
     * Set the URL string
     *
     * @param string $url
     * @return News_Model_Article
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }
    


}
