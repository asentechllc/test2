<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Entry.php 2186 2011-02-11 13:09:35Z leigh $
 */

/**
 * Model class for all Chart Entries
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage Chart
 */
class Chart_Model_Entry extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Chart_Model_EntryMapper';

    protected $_id = null;
    protected $_chartid = null;
    protected $_gameid = null;
    protected $_position = null;

    private $_prevPosition = null;
    private $_game = null;
    private $_prevChart = null;
    private $_chart = null;

    /**
     * Return mapper for model
     *
     * @return Chart_Model_EntryMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Chart_Model_Entry
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the chart id this entry belongs to
     *
     * @return int
     */
    public function getChartid()
    {
        return $this->_chartid;
    }

    /**
     * Set the chart if this entry belongs to
     *
     * @param int $chartid
     * @return Chart_Model_Entry
     */
    public function setChartid($chartid)
    {
        $this->_chartid = $chartid;

        return $this;
    }

    /**
     * Get the Game ID
     *
     * @return int
     */
    public function getGameid()
    {
        return $this->_gameid;
    }

    /**
     * Set the Game ID
     *
     * @param int $gameid
     * @return Chart_Model_Entry
     */
    public function setGameid($gameid)
    {
        $this->_gameid = $gameid;

        return $this;
    }

    /**
     * Return the entry's chart position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->_position;
    }

    /**
     * Set the entry's chart position
     *
     * @param int $position
     * @return Chart_Model_Entry
     */
    public function setPosition($position)
    {
        $this->_position = $position;

        return $this;
    }

    /**
     * Get the entry's position from the previous chart. If it doesn't have a previous
     * position false is returned.
     *
     * @return int|bool
     */
    public function getPrevPosition()
    {
        if (null === $this->_prevPosition) {
            if (null === $this->_prevChart) {
                $mapper = Chart_Model_Chart::getMapper();
                $this->_prevChart = $this->getChart()->getPrevious();
            }
            $this->_prevPosition = false;
            $entries = $this->_prevChart->getEntries();
            if (is_array($entries)) {
                foreach ($entries as $entry) {
                    if ($entry->getGameid() == $this->getGameid()) {
                        $this->_prevPosition = $entry->getPosition();
                        break;
                    }
                }
            }
        }
        return $this->_prevPosition;
    }

    /**
     * Return the chart model for this entry
     *
     * @return Chart_Model_Chart 
     */
    public function getChart()
    {
        if (null === $this->_chart) {
            $this->_chart = Chart_Model_Chart::getMapper()->find($this->getChartid());
        }

        return $this->_chart;
    }
    
    public function setChart($chart)
    {
        $this->_chart = $chart;
        return $this;
    }

    /**
     * Get the game object for this entry
     *
     * @return Game_Model_Game
     */
    public function getGame()
    {
        $mapper = Game_Model_Game::getMapper();
        $id = $this->getGameid();
        if (null !== $id) {
            return $mapper->find($id);
        } else {
            return null;
        }
    }
}
