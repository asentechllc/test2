<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Chart.php 711 2010-05-28 09:35:13Z leigh $
 */

/**
 * DB Table for the Game Model
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage DbTable
 */
class Chart_Model_DbTable_Chart extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'chart_charts';
    protected $_primary = 'chart_id';
}
