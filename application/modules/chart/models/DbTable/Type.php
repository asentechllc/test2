<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Db Table for the chart type models
 *
 * @category Chart
 * @package Chart_Model
 * @subpackage DbTable
 */
class Chart_Model_DbTable_Type extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'chart_types';
    protected $_primary = 'type_id';
}
