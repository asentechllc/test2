<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Chart
 * @package Chart_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 */

/**
 * ACL to control access to gated content.
 *
 * @category Chart
 * @package Chart_Acl
 * @subpackage Gated
 */
class Chart_Acl_Gated implements Zend_Acl_Assert_Interface
{
    
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null, Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
        }

        if (null !== $id) {
            $mapper = Chart_Model_Chart::getMapper();
            $chart = $mapper->find($id);
            if ( null === $chart ) {
                throw new Mcmr_Exception_PageNotFound('Chart not found');
            }
            
            $role = $chart->getUserrole();
            $userRole = Zend_Registry::get('authenticated-user')->getRole();
            $newResource = $resource->getResourceId() . '-' . $chart->getId();
            $acl->add(new Zend_Acl_Resource($newResource));
            $acl->allow($role, $newResource, $privilege);
            
            return $acl->isAllowed($userRole, $newResource, $privilege);
        } else {
            // There is no ID, so allow it
            
            return true;
        }
    }
}
