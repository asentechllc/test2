<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Contact
 * @package Contact_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Bootstrap
 *
 * @category Contact
 * @package Contact_Bootstrap
 */
class Contact_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    /**
     * @see Mcmr_Application_Module_Bootstrap
     */
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');
            $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Contact_View_Helper');
    }
}
