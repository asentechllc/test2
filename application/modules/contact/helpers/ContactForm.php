<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Contact
 * @package Contact_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper used to output a contact us form
 *
 * @category Contact
 * @package Contact_View
 * @subpackage 
 */
class Contact_View_Helper_ContactForm extends Zend_View_Helper_Abstract
{

    public function contactForm($name='default')
    {
        $config = $this->_getConfig($name);
        if (isset($config['form'])) {
            $options = array('type' => $config['form']);
        } else {
            $options = null;
        }
        $form = Mcmr_Form::getInstance('Contact_Form_Contact', $options);
        $form->configure(array('module' => 'contact', 'controller' => 'index', 'action' => 'index', 'name' => $name));
        return $form;
    }

    protected function _getConfig($name = 'default')
    {
        $configfile = 'modules' . DIRECTORY_SEPARATOR . 'contact.ini';
        $options = Mcmr_Config_Ini::getInstance($configfile);
        $options = $options->toArray();
        if (isset($options[$name])) {
            return $options[$name];
        } elseif (isset($options['default'])) {
            return $options['default'];
        } else {
            throw new Exception("Could't retrieve either '$name' or 'default' configuration for Contact Us module");
        }
    }

}
