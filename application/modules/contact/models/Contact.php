<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Contact
 * @package Contact_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 657 2010-05-18 10:20:03Z leigh $
 */

/**
 * Model for Contact Us data
 *
 * @category Contact
 * @package Contact_Model
 * @subpackage Contact
 */
class Contact_Model_Contact extends Mcmr_Model_ModelAbstract
{
    protected $_id = null;
    protected $_fromName = null;
    protected $_fromEmail = null;
    protected $_fromUserId = null;
    protected $_toName = null;
    protected $_toEmail = null;
    protected $_toUserId = null;
    protected $_template = null;

    /**
     *
     * @var User_Model_User 
     */
    private $_fromUserModel = null;
    
    /**
     *
     * @var User_Model_User
     */
    private $_toUserModel = null;
    
    /**
     * Return mapper for model
     *
     * @return Page_Model_TypeMapper
     */
    static function getMapper()
    {
        return null;
    }

    /**
     * Get the unique ID for this model
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the unique ID for this model
     *
     * @param int $id
     * @return Page_Model_Type
     */
    public function setId($id)
    {
        $this->_id = intval($id);

        return $this;
    }
    
    /**
     *
     * @return type 
     */
    public function getFromName()
    {
        if (null === $this->_fromEmail) {
            $user = $this->getFromUser();
            if (null !== $user) {
                $this->_fromName = $user->getFirstname() . ' ' . $user->getSurname();
            }
        }
        
        return $this->_fromName;
    }

    /**
     *
     * @param type $name
     * @return Contact_Model_Contact 
     */
    public function setFromName($name)
    {
        $this->_fromName = $name;
        
        return $this;
    }

    /**
     *
     * @return type 
     */
    public function getFromEmail()
    {
        if (null === $this->_fromEmail) {
            $user = $this->getFromUser();
            if (null !== $user) {
                $this->_fromEmail = $user->getEmail();
            }
        }
        
        return $this->_fromEmail;
    }

    /**
     *
     * @param type $email
     * @return Contact_Model_Contact 
     */
    public function setFromEmail($email)
    {
        $this->_fromEmail = $email;
        
        return $this;
    }

    /**
     *
     * @return type 
     */
    public function getFromUserId()
    {
        return $this->_fromUserId;
    }

    /**
     *
     * @param type $userId
     * @return Contact_Model_Contact 
     * @throws Mcmr_Model_Exception
     */
    public function setFromUserId($userId)
    {
        if (null === $userId) {
            return $this;
        }
        $this->_fromUserId = intval($userId);
        $this->_fromUserModel = null;
        $this->_fromEmail = null;
        $this->_fromName = null;
        
        if (null === $this->getFromUser()) {
            throw new Mcmr_Model_Exception("Cannot find user with ID '{$userId}'");
        }
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getToName()
    {
        if (null === $this->_toName) {
            $user = $this->getToUser();
            if (null !== $user) {
                $this->_toName = $user->getFirstname() . ' ' . $user->getSurname();
            }
        }
        
        return $this->_toName;
    }

    /**
     *
     * @param string $name
     * @return Contact_Model_Contact 
     */
    public function setToName($name)
    {
        $this->_toName = $name;
        
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getToEmail()
    {
        if (null === $this->_toEmail) {
            $user = $this->getToUser();
            if (null !== $user) {
                $this->_toEmail = $user->getEmail();
            }
        }
        
        return $this->_toEmail;
    }

    /**
     *
     * @param string $email
     * @return Contact_Model_Contact 
     */
    public function setToEmail($email)
    {
        $this->_toEmail = $email;
        
        return $this;
    }

    /**
     *
     * @return int 
     */
    public function getToUserId()
    {
        return $this->_toUserId;
    }

    /**
     *
     * @param int $toUserId
     * @return Contact_Model_Contact 
     */
    public function setToUserId($toUserId)
    {
        if (null === $toUserId) {
            return $this;
        }
        $this->_toUserId = intval($toUserId);
        $this->_toUserModel = null;
        $this->_toEmail = null;
        $this->_toName = null;
        
        if (null === $this->getToUser()) {
            throw new Mcmr_Model_Exception("Cannot find user with ID '{$userId}'");
        }
        
        return $this;
    }
   
    /**
     *
     * @return type 
     */
    public function getTemplate()
    {
        return $this->_template;
    }

    /**
     *
     * @param type $template
     * @return Contact_Model_Contact 
     */
    public function setTemplate($template)
    {
        $this->_template = $template;
        
        return $this;
    }
    
    public function getFromUser()
    {
        if (null !== $this->_fromUserId && null === $this->_fromUserModel) {
            $this->_fromUserModel = User_Model_User::getMapper()->find($this->_fromUserId);
        }
        
        return $this->_fromUserModel;
    }
    
    public function getToUser()
    {
        if (null !== $this->_toUserId && null === $this->_toUserModel) {
            $this->_toUserModel = User_Model_User::getMapper()->find($this->_toUserId);
        }
        
        return $this->_toUserModel;
    }
    
    public function send()
    {
        
    }
}
