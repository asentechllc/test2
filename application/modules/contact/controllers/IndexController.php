<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Contact
 * @package Contact_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Index Controller for the Page module
 *
 * @category Page
 * @package Page_IndexController
 */
class Contact_IndexController extends Mcmr_Controller_Action
{

	public function init(){
        $this->view->url = '/contact-us';
		parent::init();
	}

    public function readAction(){
    	$user = User_Model_User::getMapper()->find(Mcmr_StdLib::realId($this->_request->getParam('seoId', null)));
    	if($user && $user->getAttribute('contactus') && $user->getUrl() == $this->_request->getParam('url', null)){
    		$this->view->user = $user;

    		$this->view->articles = News_Model_Article::getMapper()->findAllByField(array(
    			'useridauthor' => $user->getId()
    		),array(
    			'publishdate' => 'DESC',
    		),array(
    			'page' => 1,
    			'count' => 10,
    		));
    	}
    }

    public function indexAction()
    {

		$name = $this->_request->getParam('name', 'default');
        $config = $this->_getConfig($name);
        if (isset($config['form'])) {
            $options = array('type' => $config['form']);
        } else {
            $options = null;
        }
        $form = Mcmr_Form::getInstance('Contact_Form_Contact', $options);
        $this->view->form = $form;
        $formName = $form->getName();
        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();

            if ($form->isValid($post)) {
                $data = $form->getValues(true);

                if ($this->_isSpam($data)) {
                    $spamCatcherConfig = $this->_getConfig('spamCatcher');
                    $recipients = $spamCatcherConfig['email'];
                } else {
                    $recipients = $config['email'];
                }

                foreach ($recipients as $email) {
                    $this->_sendEmail($email, $data, $config);
                }

                $this->view->data = $data;

                $this->view->error = false;
                $this->view->getHelper('DisplayMessages')->addMessage('messageSent', 'info');
                Zend_Controller_Action_HelperBroker::getStaticHelper('redirector')
                    ->gotoUrl($form->getValue('redirect_success'));
            } else {
                Mcmr_Form::saveInstance($form, $options);
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');

                # forward using route if available
                if(isset($config['form']) && isset($config['url'])){
	                $this->_forward('read','index','default',array('url'=>$config['url'], 'form'=>$config['form']));
	            }
	            else{
	                Zend_Controller_Action_HelperBroker::getStaticHelper('redirector')
    	                ->gotoUrl($form->getUnfilteredValue('redirect_fail'));
                }
            }
        }
    }

    protected function _sendEmail($emailConfig, $postData,$config)
    {
        $fromUserId = null;
        $toUserId = null;
        switch ($emailConfig['from']['source']) {
            case 'form':
                $fromEmail = null;
                $fromName = null;
                $fromUserId = null;
                if (isset($emailConfig['from']['email'])&&isset($postData[$emailConfig['from']['email']])) {
                    $fromEmail = $postData[$emailConfig['from']['email']];
                }
                if (isset($emailConfig['from']['name'])&&isset($postData[$emailConfig['from']['name']])) {
                    $fromName = $postData[$emailConfig['from']['name']];
                }
                if (isset($emailConfig['from']['userId'])&&isset($postData[$emailConfig['from']['userId']])) {
                    $fromUserId = $postData[$emailConfig['from']['userId']];
                }
                if (null===$fromUserId&&(null===$fromEmail||null===$fromName)){
                    throw new Exception("Missing sender detail");
                }
                break;
            case 'config':
                $fromEmail = $emailConfig['from']['email'];
                $fromName = $emailConfig['from']['name'];
                if (isset($emailConfig['from']['userId'])) {
                    $fromUserId = $emailConfig['from']['userId'];
                }
                break;
        }
        switch ($emailConfig['to']['source']) {
            case 'form':
                $toEmail = null;
                $toName = null;
                $toUserId = null;
                if (isset($emailConfig['to']['email'])&&isset($postData[$emailConfig['to']['email']])) {
                    $toEmail = $postData[$emailConfig['to']['email']];
                }
                if (isset($emailConfig['to']['name'])&&isset($postData[$emailConfig['to']['name']])) {
                    $toName = $postData[$emailConfig['to']['name']];
                }
                if (isset($emailConfig['to']['userId'])&&isset($postData[$emailConfig['to']['userId']])) {
                    $toUserId = $postData[$emailConfig['to']['userId']];
                }
                if (null===$toUserId&&(null===$toEmail||null===$toName)){
                    throw Exception("Missing receiver detail");
                }
                break;
            case 'config':
                $toEmail = $emailConfig['to']['email'];
                $toName = $emailConfig['to']['name'];
                if (isset($emailConfig['to']['userId'])) {
                    $toUserId = $emailConfig['to']['userId'];
                }
                break;
        }
        $contact = new Contact_Model_Contact();
        $contact->setToEmail($toEmail)
                ->setToName($toName)
                ->setToUserId($toUserId)
                ->setFromEmail($fromEmail)
                ->setFromName($fromName)
                ->setFromUserId($fromUserId);

        $templateName = $emailConfig['template'];
        $template = clone $this->view;
        $template->clearVars();
        $template->setScriptPath(SITE_PATH . '/views/contact/scripts/');
        $template->addScriptPath(SITE_PATH . '/views/_common');

		foreach(array('formName'=>'contact form','messageField'=>'story','messageTitle'=>'Story') as $key=>$value)
			$template->$key = isset($config[$key]) ? $config[$key] : $value;

        foreach ($postData as $key => $value) {
            $template->$key = $value;
        }

        $html = $template->render("{$templateName}.html.phtml");
        $text = $template->render("{$templateName}.text.phtml");
        $subject = $template->render("{$templateName}.subject.phtml");

        $email = new Mcmr_Mail();
        $email->setBodyHtml($html);
        $email->setBodyText($text);
        $email->setSubject($subject);

        $email->addTo($contact->getToEmail(), $contact->getToName());
        $email->setFrom($contact->getFromEmail(), $contact->getFromName());

        $email->send();
    }

    protected function _getConfig($name = 'default')
    {
        $configfile = 'modules' . DIRECTORY_SEPARATOR . 'contact.ini';
        $options = Mcmr_Config_Ini::getInstance($configfile);
        $options = $options->toArray();
        if (isset($options[$name])) {
            return $options[$name];
        } elseif (isset($options['default'])) {
            return $options['default'];
        } else {
            throw new Exception("Could't retrieve either '$name' or 'default' configuration for Contact Us module");
        }
    }

    /**
     * Verify if the submitted contact form is possibly spam and return the result
     * @param  array  $postData     The POST data from the submitted contact form
     * @return boolean              Is the Submission Spam?
     */
    protected function _isSpam($postData)
    {
        $result = false;
        if (isset($postData['sc_']) && strlen($postData['sc_']) > 0) {
            $result = true;
        }

        return $result;
    }

}
