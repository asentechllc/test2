<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Contact
 * @package Page_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Page_Form_Page.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Form for contact us
 *
 * @category Contact
 * @package Contact_Form
 * @subpackage Contact
 */
class Contact_Form_Contact extends Mcmr_Form_Redirected
{

    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }
        if (null != $type) {
            // We have an contact form type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'contact'
            . DIRECTORY_SEPARATOR . 'contact_form_contact-'.strtolower($type).'.ini';
        } else {
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR
            . 'contact' . DIRECTORY_SEPARATOR . 'contact_form_contact.ini';
        }

        parent::__construct($options);
    }
    

    public function init()
    {
        parent::init();
        // Add some CSRF protection
        //$this->addElement('hash', 'contactcsrf', array( 'salt' => 'unique'));
        $this->setName('contactformcontact');//->setElementsBelongTo('contact-form-contact');
        $this->setMethod('post');
    } //init
    
    
}
