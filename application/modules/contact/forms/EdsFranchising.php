<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Contact
 * @package Page_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Page_Form_Page.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Form for contact us
 *
 * @category Contact
 * @package Contact_Form
 * @subpackage Contact
 */
class Contact_Form_EdsFranchising extends Mcmr_Form_Redirected
{

    public function init()
    {
        parent::init();
        // Add some CSRF protection
        $this->addElement(
            'hash', 'contactcsrf', array('salt' => 'unique')
        );
        
        $this->setName('contactformedsfranchising')->setElementsBelongTo('contact-form-edsfranchising');
        $this->setMethod('post');
    } //init
    
    
}
