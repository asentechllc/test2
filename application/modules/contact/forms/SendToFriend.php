<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Contact
 * @package Page_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Page_Form_Page.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Form for send to friend
 *
 * @category Contact
 * @package Contact_Form
 * @subpackage Contact
 */
class Contact_Form_SendToFriend extends Mcmr_Form_Redirected
{

    public function init()
    {
        parent::init();
        // Add some CSRF protection
        $this->addElement(
            'hash', 'sendtofriendcsrf', array('salt' => 'unique')
        );
        $this->setName('contactformsendtofriend')
            ->setElementsBelongTo('contact-form-sendtofriend');
        $this->setMethod('post');
    }

//init

    public function postInit()
    {
        parent::postInit();
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $url = $request->getRequestUri();
        $element = $this->getElement('url');
        if (null !== $element) {
            $element->setValue($url);
        }
    }

//postInit
}
