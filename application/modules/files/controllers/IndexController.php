<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2002 2011-01-04 00:03:45Z leigh $
 */

/**
 * Default_IndexController
 *
 * The Purpose of this controller is to do nothing but forward the request onto the real default page
 * as determined by the configuration
 *
 * @category Default
 * @package Default_IndexController
 */
class Files_IndexController extends Zend_Controller_Action
{

	function init(){
		parent::init();

		$bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
	    $this->options = $bootstrap->getOptions();
	}
	
	public function imagesAction(){
	
		$modelClass = $this->_request->getParam('modelClass', false);
		$modelId = $this->_request->getParam('modelId', false);

		$this->view->model = null;		
		if($modelClass && $modelId){
			$this->view->model = $modelClass::getMapper()->find($modelId);
		}
	
    	$dir = $this->options['media']['dir']['root'];
    	$url = $this->options['media']['url']['root'];

    	$this->view->images = array();
    	$this->view->captions = array();
    	
    	$_rootPath = preg_replace('/[^\/]+$/','',$this->_request->getParam('rootPath', false));
		if($_path = $this->_request->getParam('path', false)){
			$_path = $_rootPath.$_path;
			$url = $url.'/'.$_path;
			$path = $dir.'/'.$_path.'/';
			if(is_dir($path)){
				$dh = opendir($path);
				while($file = readdir($dh)){
					if(!is_dir($path.$file)){
						$size = @getimagesize($path.$file);
						if($size){
							$img = array(
								'src' => $url.'/'.$file,
								'filename' => $file,
								'defaultCaption' => preg_replace('/\.[^\.]+$/','',$file),
							);
							$this->view->images[] = $img;
							if(!isset($this->view->captions[$img['filename']]))
								$this->view->captions[$img['filename']] = $img['defaultCaption'];
						}
					}
				}
			}
		}
    	$this->_helper->layout->disableLayout();
	}

    /**
     * Main elFinder File Browser
     **/
    public function indexAction(){}
    
    /**
     * Action for elFinder popup
     **/
    public function popupAction(){
    	$this->view->tinymce = $this->_request->getParam('tinymce', 0);
    	$this->view->input = $this->_request->getParam('input', null);

		# configurable parameters
    	$this->view->path = $this->_request->getParam('path', null);
    	$this->view->width = $this->_request->getParam('width', null);
    	$this->view->height = $this->_request->getParam('height', null);
    	$this->view->maxWidth = $this->_request->getParam('maxWidth', null);
    	$this->view->maxHeight = $this->_request->getParam('maxHeight', null);
    	$this->view->minWidth = $this->_request->getParam('minWidth', null);
    	$this->view->minHeight = $this->_request->getParam('minHeight', null);
    	$this->view->ignoreSizeWarning = $this->_request->getParam('ignoreSizeWarning', null);
    	$this->view->showSizeWarning = $this->_request->getParam('showSizeWarning', null);
		$this->view->baseUrl = $this->options['media']['url']['root'];

    	# if we have specified ignoreSizeWarning -- showSizeWarning should be true
    	if($this->view->showSizeWarning === null && $this->view->ignoreSizeWarning !== null)
    		$this->view->showSizeWarning = true;

    	$this->_helper->layout->setLayout('elfinder');
    }
    
    public function dirchooserAction(){
    	$this->view->editor = $this->_request->getParam('editor', null);
    	$this->popupAction();
    }
    
    /**
     * elFinder connector
     **/
    public function elfinderAction(){

		error_reporting(0); // Set E_ALL for debuging
		
    	$elFinderPath = APPLICATION_PATH.'/../library/ThirdParty/ElFinder/';

    	$dir = $this->options['media']['dir']['root'];
    	$url = $this->options['media']['url']['root'];
    	
    	$rootSuffix = preg_replace('/[^a-z0-9_\-\/]+/i','',$this->_request->getParam('rootSuffix',''));
    	$rootSuffix = preg_replace('/\/{2,}/i','/',$rootSuffix);
    	if($rootSuffix){
    		$dir .= $rootSuffix[0] == '/' ? $rootSuffix : '/'.$rootSuffix;
    		$url .= $rootSuffix[0] == '/' ? $rootSuffix : '/'.$rootSuffix;
    	}
    	
    	# check if folder exists, if not create it
    	if(!is_dir($dir)){
    		mkdir($dir,0755,true);
    	}
    	
		include_once $elFinderPath.'elFinderConnector.class.php';
		include_once $elFinderPath.'elFinder.class.php';
		include_once $elFinderPath.'elFinderVolumeDriver.class.php';
		include_once $elFinderPath.'elFinderVolumeLocalFileSystem.class.php';

		// Required for MySQL storage connector
		// include_once $elFinderPath.'elFinderVolumeMySQL.class.php';
		// Required for FTP connector support
		// include_once $elFinderPath.'elFinderVolumeFTP.class.php';
		
		/**
		 * Simple function to demonstrate how to control file access using "accessControl" callback.
		 * This method will disable accessing files/folders starting from  '.' (dot)
		 *
		 * @param  string  $attr  attribute name (read|write|locked|hidden)
		 * @param  string  $path  file path relative to volume root directory started with directory separator
		 * @return bool|null
		 **/
		function access($attr, $path, $data, $volume) {
			return strpos(basename($path), '.') === 0       // if file/folder begins with '.' (dot)
				? !($attr == 'read' || $attr == 'write')    // set read+write to false, other (locked+hidden) set to true
				:  null;                                    // else elFinder decide it itself
		}
		
		$opts = array(
			// 'debug' => true,
			'roots' => array(
				array(
					'driver'        => 'LocalFileSystem',   // driver for accessing file system (REQUIRED)
					'path'          => $dir,         // path to files (REQUIRED)
					'URL'           => $url, // URL to files (REQUIRED)
					'accessControl' => 'access',             // disable and hide dot starting files (OPTIONAL)
					'uploadOverwrite' => false,
				)
			)
		);
		
		// run elFinder
		$connector = new elFinderConnector(new elFinder($opts));
		$connector->run();
    }
}

