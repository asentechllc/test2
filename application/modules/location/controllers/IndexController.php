<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Location
 * @package
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 *
 *
 * @category   Location
 * @package
 * @subpackage 
 */
class Location_IndexController extends Zend_Controller_Action
{

    /**
     *
     */
    public function indexAction()
    {
        $fields = array();
        $order = array();

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('location-config')->locations->itemCountPerPage);

        $mapper = Location_Model_Location::getMapper();
        $this->view->locations = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     *
     */
    public function createAction()
    {
        $form = $this->_helper->loadForm('Location');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $location = new Location_Model_Location();
                $location->setOptions($form->getValues(true));

                try {
                    $mapper = Location_Model_Location::getMapper();
                    $mapper->save($location);

                    $this->view->location = $location;
                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('Location Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $location);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    throw $e;
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'error');
            }
        }
    }

    /**
     *
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);

        if (null !== $id) {
            $mapper = Location_Model_Location::getMapper();

            $location = $mapper->find($id);
            if (null !== $location) {
                $this->view->location = $location;
            } else {
                throw new Mcmr_Exception_PageNotFound('Location Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     *
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Location_Model_Location::getMapper();
            if (null !== ($location = $mapper->find($id))) {
                $form = $this->_helper->loadForm('Location');
                $form->populate($location->getOptions(true));
                $this->view->form = $form;
                $this->view->location = $location;

                if ($this->_request->isPost()) {
                    // Merge the existing data with the form values. This allows for partial updates.
                    $post = $this->_request->getPost();
                    $locationValues = $location->getOptions(true);
                    $post['locationformlocation'] = array_merge($locationValues, $post['locationformlocation']);

                    if ($form->isValid($post)) {
                        $values = $form->getValues(true);
                        $location->setOptions($values);

                        try {
                            $mapper->save($location);

                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('Location Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $location);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }

                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'warn');
                    }

                }

            } else {
                throw new Mcmr_Exception_PageNotFound('Location Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * 
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Location_Model_Location::getMapper();
            if (null !== ($location = $mapper->find($id))) {
                $this->view->location = $location;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($location);
                        $this->view->location = null;

                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('locationDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $location);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Location Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
}
