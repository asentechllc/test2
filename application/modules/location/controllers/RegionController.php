<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Location
 * @package
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 *
 *
 * @category   Location
 * @package
 * @subpackage
 */
class Location_RegionController extends Zend_Controller_Action
{
    /**
     *
     */
    public function indexAction()
    {
        $fields = array();
        $order = array();

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('location-config')->regions->itemCountPerPage);

        $mapper = Location_Model_Region::getMapper();
        $this->view->regions = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     *
     */
    public function createAction()
    {
        $form = $this->_helper->loadForm('Region');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $location = new Location_Model_Region();
                $location->setOptions($form->getValues(true));

                try {
                    $mapper = Location_Model_Region::getMapper();
                    $mapper->save($location);

                    $this->view->location = $location;
                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('Location Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $location);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    throw $e;
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'error');
            }
        }
    }

    /**
     *
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);

        if (null !== $id) {
            $mapper = Location_Model_Region::getMapper();

            $region = $mapper->find($id);
            if (null !== $region) {
                $this->view->region = $region;
            } else {
                throw new Mcmr_Exception_PageNotFound('Region Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     *
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Location_Model_Region::getMapper();
            if (null !== ($region = $mapper->find($id))) {
                $form = $this->_helper->loadForm('Region');
                $form->populate($region->getOptions(true));
                $this->view->form = $form;
                $this->view->region = $region;

                if ($this->_request->isPost()) {
                    // Merge the existing data with the form values. This allows for partial updates.
                    $post = $this->_request->getPost();
                    $regionValues = $region->getOptions(true);
                    $post['locationformregion'] = array_merge($regionValues, $post['locationformregion']);

                    if ($form->isValid($post)) {
                        $values = $form->getValues(true);
                        $region->setOptions($values);

                        try {
                            $mapper->save($region);

                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('regionSaved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $region);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }

                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'warn');
                    }

                }

            } else {
                throw new Mcmr_Exception_PageNotFound('Region Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * 
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Location_Model_Region::getMapper();
            if (null !== ($region = $mapper->find($id))) {
                $this->view->region = $region;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($region);
                        $this->view->region = null;

                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('regionDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $region);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Region Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
}
