<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Location
 * @package    Location_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all location region models
 *
 * @category   Location
 * @package    Location_Model
 * @subpackage RegionMapper
 */
class Location_Model_RegionMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Location_Model_DbTable_Region';
    protected $_columnPrefix = 'region_';
    protected $_modelClass = 'Location_Model_Region';
    protected $_cacheIdPrefix = 'LocationRegion';

    /**
     * @see Mcmr_Model_GenericMapper::save()
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        // Ensure the URL is unique
        if (null === $model->getId() || $model->urlChanged()) {
            $model->setUrl($this->_getUniqueUrl($model));
        }

        parent::save($model);
    }

    /**
     * Ensure that the model has a unique URL. If the URL is taken generate
     * a new one and set that to the model
     *
     * @param News_Model_Article $model
     * @return string
     */
    private function _getUniqueUrl($model)
    {
        $url = $model->getUrl();
        if ($this->_urlExists($url)) {
            $urlTail = 2;
            $newUrl = Mcmr_StdLib::urlize($url . ' ' . $urlTail);
            while ($this->_urlExists($newUrl)) {
                $urlTail++;
                $newUrl = $url . '-' . $urlTail;
            }
            $model->setUrl($newUrl);

            return $newUrl;
        }

        return $url;
    }

    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($url)
    {
        return (null !== $this->findOneByField(array('url'=> $url)));
    }

}
