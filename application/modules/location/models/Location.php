<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category    Location
 * @package     Location_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Class for all location models
 *
 * @category    Location
 * @package     Location_Model
 * @subpackage  Location
 */
class Location_Model_Location extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Location_Model_LocationMapper';

    protected $_id = null;
    protected $_url = null;
    protected $_regionid = null;
    protected $_title = null;
    protected $_link = null;

    private $_region = null;
    private $_urlchanged = false;
    
    /**
     * Return mapper for model
     *
     * @return Location_Model_LocationMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the ID for this location
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID for this location
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Get the URL string
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url && null !== $this->_title) {
            $this->_url = Mcmr_StdLib::urlize($this->_title);
            $this->_urlchanged = true;
        }

        return $this->_url;
    }

    /**
     * Set the URL string
     *
     * @param string $url
     * @return Location_Model_Location
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }
    
    /**
     * Get the region ID
     *
     * @return int
     */
    public function getRegionid()
    {
        return $this->_regionid;
    }

    /**
     * Set the location's region ID
     *
     * @param int $regionid
     * @return Location_Model_Location
     */
    public function setRegionid($regionid)
    {
        $this->_region = null;
        $this->_regionid = (int)$regionid;

        return $this;
    }

    /**
     * Get the location title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the location title
     *
     * @param string $title
     * @return Location_Model_Location
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the location link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->_link;
    }

    /**
     * Set the location Link
     *
     * @param string $link
     * @return Location_Model_Location
     */
    public function setLink($link)
    {
        $this->_link = $link;

        return $this;
    }

    /**
     * Get the location region
     *
     * @return Location_Model_Region
     */
    public function getRegion()
    {
        return $this->_region;
    }

    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }
}
