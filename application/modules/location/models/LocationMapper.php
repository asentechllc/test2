<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category    Location
 * @package     Location_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all location models
 *
 * @category    Location
 * @package     Location_Model
 * @subpackage LocationMapper
 */
class Location_Model_LocationMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Location_Model_DbTable_Location';
    protected $_columnPrefix = 'location_';
    protected $_modelClass = 'Location_Model_Location';
    protected $_cacheIdPrefix = 'Location';

    /**
     * @see Mcmr_Model_GenericMapper::save()
     */
    public function  save(Mcmr_Model_ModelAbstract $model)
    {
        // Ensure the URL is unique
        if (null === $model->getId() || $model->urlChanged()) {
            $model->setUrl($this->_getUniqueUrl($model));
        }

        parent::save($model);
    }
    
    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseField()
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'region':
            case 'regionid':
            case 'region_id':
                $field = 'region_id';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }


    /**
     * Ensure that the model has a unique URL. If the URL is taken generate
     * a new one and set that to the model
     *
     * @param News_Model_Article $model
     * @return string
     */
    private function _getUniqueUrl($model)
    {
        $url = $model->getUrl();
        if ($this->_urlExists($url)) {
            $urlTail = 2;
            $newUrl = Mcmr_StdLib::urlize($url . ' ' . $urlTail);
            while ($this->_urlExists($newUrl)) {
                $urlTail++;
                $newUrl = $url . '-' . $urlTail;
            }
            $model->setUrl($newUrl);

            return $newUrl;
        }

        return $url;
    }

    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($url)
    {
        return (null !== $this->findOneByField(array('url'=> $url)));
    }
}
