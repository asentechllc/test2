<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Location
 * @package
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 *
 *
 * @category   Location
 * @package
 * @subpackage
 */
class Location_Model_DbTable_Location extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'location_locations';
    protected $_primary = 'location_id';
}
