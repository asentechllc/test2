<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category    Location
 * @package     Location_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all location regions
 *
 * @category    Location
 * @package     Location_Model
 * @subpackage Region
 */
class Location_Model_Region extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Location_Model_RegionMapper';

    protected $_id = null;
    protected $_title = null;
    protected $_url = null;

    private $_urlchanged = false;

    /**
     * Return mapper for model
     *
     * @return Location_Model_LocationMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the ID for this region
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID for this region
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->_id = (int)$id;
    }

    /**
     * Get the URL string
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url && null !== $this->_title) {
            $this->_url = Mcmr_StdLib::urlize($this->_title);
            $this->_urlchanged = true;
        }

        return $this->_url;
    }

    /**
     * Set the URL string
     *
     * @param string $url
     * @return Location_Model_Region
     */
    public function setUrl($url)
    {
        // Strip all whitespace.
        $url = Mcmr_StdLib::urlize($url);
        if (null !== $this->_url && ($url !== $this->_url)) {
            $this->_urlchanged = true;
        }

        $this->_url = $url;

        return $this;
    }
    
    /**
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     *
     * @param string $title
     * @return Location_Model_Region
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Returns true is the URL field has been modified
     *
     * @param boolean $reset
     * @return boolean
     */
    public function urlChanged($reset = false)
    {
        $changed = $this->_urlchanged;
        if ($reset) {
            $this->_urlchanged = false;
        }

        return $changed;
    }
}
