<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Location
 * @package
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 *
 *
 * @category   Location
 * @package
 * @subpackage 
 */
class Location_Form_Location extends Mcmr_Form
{
    public function init()
    {
        $this->setName('locationformlocation')->setElementsBelongTo('location-form-location');
        $this->setMethod('post');
        $this->setEnctype("multipart/form-data");
        
    }
}
