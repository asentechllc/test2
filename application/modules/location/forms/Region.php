<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Location
 * @package
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 *
 *
 * @category   Location
 * @package
 * @subpackage
 */
class Location_Form_Region extends Mcmr_Form
{
    public function init()
    {
        $this->setName('locationformregion')->setElementsBelongTo('location-form-region');
        $this->setMethod('post');
        $this->setEnctype("multipart/form-data");
    }
}
