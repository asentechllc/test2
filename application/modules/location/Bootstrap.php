<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Location
 * @package Location_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 1308 2010-09-03 15:22:16Z leigh $
 */

/**
 * Bootstrap
 *
 * @category Location
 * @package Location_Bootstrap
 */
class Location_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');

        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Location_View_Helper');
    }
}