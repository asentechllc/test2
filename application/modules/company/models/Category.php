<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Category.php 1104 2010-07-22 11:44:57Z leigh $
 */

/**
 * Model class for all Categorys
 *
 * @category Company
 * @package Company_Model
 * @subpackage Category
 */
class Company_Model_Category extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Company_Model_CategoryMapper';

    protected $_id = null;
    protected $_parentId = null;
    protected $_title = null;
    protected $_url = null;
    protected $_order = null;

    /**
     * Category Type model object
     * @var Company_Model_Type
     */
    private $_type = null;

    /**
     * Return mapper for model
     *
     * @return Company_Model_CategoryMapper
     */
    static function getMapper()
    {
        return parent::getMapper(self::$_mapperclass);
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Company_Model_Category
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the parent Category ID
     *
     * @return int
     */
    public function getParentId()
    {
        if (null === $this->_parentId) {
            $this->_parentId = 0;
        }
        
        return $this->_parentId;
    }

    /**
     * Set the parent Category ID
     *
     * @param int $parentId
     * @return Company_Model_Category 
     */
    public function setParentId($parentId)
    {
        $this->_parentId = (int)$parentId;
        
        return $this;
    }

    /**
     * Get the category title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the Category Title
     *
     * @param string $title
     * @return Company_Model_Category
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the URL string
     *
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url) {
            $this->_url = Mcmr_StdLib::urlize($this->getTitle());
        }

        return $this->_url;
    }

    /**
     * Set the URL string
     *
     * @param string $url
     * @return Company_Model_Category
     */
    public function setUrl($url)
    {
        $this->_url = Mcmr_StdLib::urlize($url);

        return $this;
    }
    
    
    /**
     * Get the order number
     *
     * @return int
     */
    public function getOrder()
    {
        if (null === $this->_order) {
            $this->_order = 0;
        }
        return $this->_order;
    }

    /**
     * Set the order number
     *
     * @param int $order
     * @return Company_Model_Category
     */
    public function setOrder($order)
    {
        $this->_order = $order;
        return $this;
    }

    /**
     * Return the companies in this company category
     *
     * @param int|array $page
     * @return Zend_Paginator
     */
    public function getCompanies($conditions=null, $order=null, $page = null)
    {
        $mapper = Company_Model_Company::getMapper();

        if (null === $conditions) {
            $conditions = array();
        }
        $conditions['categoryid'] = $this->getId();

        if (null === $order) {
            $order = array('title'=>'asc');
        }
        
        $companies = $mapper->findAllByField($conditions, $order, $page);

        return $companies;
    }
    
    /**
     * Get the parent Category
     * 
     * @return Company_Model_Category
     */
    public function getParent()
    {
        $mapper = self::getMapper();
        
        $id = $this->getParentId();
        if (0 !== $id) {
            return $mapper->find($id);
        } else {
            return null;
        }
    }
    
    /**
     * Fetch all the children of this category
     *
     * @param int $count 
     * @param string $order
     * @return Zend_Paginator
     */
    public function getChildren($count = 10, $order = null)
    {
        $mapper = self::getMapper();

        return $mapper->findAllByField(array('parentId'=>$this->getId()), $order, $count);
    }
}
