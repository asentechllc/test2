<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Type.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Description of Type
 *
 * @category Company
 * @package Company_Model
 * @subpackage DbTable
 */
class Company_Model_DbTable_Type extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'company_types';
    protected $_primary = 'type_id';
}
