<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: TypeMapper.php 2178 2011-02-10 11:20:22Z leigh $
 */

/**
 * Mapper class for all Company types
 *
 * @category Company
 * @package Company_Model
 * @subpackage TypeMapper
 */
class Company_Model_TypeMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Company_Model_DbTable_Type';
    //protected $_dbAttrTableClass = 'Company_Model_DbTable_TypeAttr';
    protected $_columnPrefix = 'type_';
    protected $_modelClass = 'Company_Model_Type';
    protected $_cacheIdPrefix = 'CompanyType';
    
    /**
     * @see Mcmr_Model_GenericMapper:save()
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        // If this is an update
        if (null !== $model->getId()) {
            // If the URL has changed we don't want any duplicates
            if ($model->urlChanged()) {
                $model->setUrl($this->_getUniqueUrl($model));
            }
        } else {
            $model->setUrl($this->_getUniqueUrl($model));
        }
        
        return parent::save($model);
    }
    
    /**
     * Ensure that the model has a unique URL. If the URL is taken generate
     * a new one and set that to the model
     *
     * @param Company_Model_Type $model
     * @return string
     */
    private function _getUniqueUrl($model)
    {
        $url = $model->getUrl();
        if ($this->_urlExists($url)) {
            $urlTail = 2;
            $newUrl = Mcmr_StdLib::urlize($url . ' ' . $urlTail);
            while ($this->_urlExists($newUrl)) {
                $urlTail++;
                $newUrl = $url . '-' . $urlTail;
            }
            $model->setUrl($newUrl);

            return $newUrl;
        }

        return $url;
    }

    /**
     * Takes a URL field and checks if that URL already exists
     *
     * @param string $url
     * @return bool
     */
    private function _urlExists($url)
    {
        return (null !== $this->findOneByField(array('url'=> $url)));
    }

}
