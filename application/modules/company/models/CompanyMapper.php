<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CompanyMapper.php 2425 2011-05-11 16:10:00Z michal $
 */

/**
 * Mapper class for all Company models
 *
 * @category Company
 * @package Company_Model
 * @subpackage CompanyMapper
 */
class Company_Model_CompanyMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Company_Model_DbTable_Company';
    protected $_dbAttrTableClass = 'Company_Model_DbTable_CompanyAttr';
    protected $_columnPrefix = 'company_';
    protected $_modelClass = 'Company_Model_Company';
    protected $_cacheIdPrefix = 'Company';

    public function findOneByField($condition = null, $order = null)
    {
        $company = parent::findOneByField($condition, $order);

        // Return an empty company if this one is deleted
        if (null !== $company && 'deleted' === $company->getState()) {
            $company = new Company_Model_Company();
        }

        return $company;
    }

    /**
     * @see Mcmr_Model_GenericMapper::save()
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        parent::save($model);

        $this->_saveCategoryids($model);
        return $this;
    }

    /**
     * Delete the model information from the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        // Mark the company as deleted in the database
        $model->setState('deleted');

        // Disable observers before performing this save then restore them
        $observersEnabled = $this->getObserversEnabled();
        $this->setObserversEnabled(false);
        $this->save($model);
        $this->setObserversEnabled($observersEnabled);

        // Notify observers
        $this->notify('delete', $model);

        return $this;
    }

    /**
     * Reset the featured companies
     * 
     * @return Company_Model_CompanyMapper
     */
    public function resetFeatured()
    {
        // This is less efficient DB wise. But this way will reset all the correct cache elements
        $companies = $this->findAllByField(array('featured' => 1), null, array('page' => 1, 'count' => 100000));
        foreach ($companies as $company) {
            $company->setOrder(null);
            $company->setFeatured(false);
            $this->save($company);
        }

        return $this;
    }

    /**
     * Reset the recruiter companies
     *
     * @return Company_Model_CompanyMapper
     */
    public function resetRecruiter()
    {
        // This is less efficient DB wise. But this way will reset all the correct cache elements
        $select = $this->getDbTable()
                        ->select()
                        ->where('company_recruiter = ?', 1);
        $rows = $this->getDbTable()->fetchAll($select);

        foreach ($rows as $row) {
            $company = $this->find($row['company_id']);
            $company->setRecruiterorder(null);
            $company->setRecruiter(false);
            $this->save($company);
        }

        return $this;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_convertRowToModel()
     */
    protected function _convertRowToModel($row)
    {
        $model = parent::_convertRowToModel($row);

        if (null !== $model) {
            $this->loadCategoryids($model);
        }

        return $model;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseField()
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'type':
            case 'typeid':
            case 'type_id':
                $field = 'type_id';
                break;

            case 'categoryids':
            case 'category':
                $field = 'category_id';
                break;

            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;

            default:
                $field = $this->_columnPrefix . $field;
                break;
        }

        return $field;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseValue()
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'company_featuredend':
            case 'company_featuredstart':
            case 'company_recruiterend':
            case 'company_recruiterstart':
                if (!empty($value)) {
                    $value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
                }
                break;

            case 'company_featured':
                $value = (int) $value;
                break;
        }

        return $value;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseValues()
     */
    protected function _sanitiseValues($data)
    {
        unset($data['category_id']);
        return $data;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_selectFields()
     */
    protected function _selectFields()
    {
        $all = parent::_selectFields();
        $all[] = "UNIX_TIMESTAMP(company_featuredstart) AS company_featuredstart";
        $all[] = "UNIX_TIMESTAMP(company_featuredend) AS company_featuredend";
        $all[] = "UNIX_TIMESTAMP(company_recruiterstart) AS company_recruiterstart";
        $all[] = "UNIX_TIMESTAMP(company_recruiterend) AS company_recruiterend";

        return $all;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_addCondition()
     */
    protected function _addCondition(Zend_Db_Select $select, $field, $value)
    {
        switch ($field) {
            case 'company_category':
            case 'company_category_id':
            case 'company_categoryid':
                $select->setIntegrityCheck(false);
                $select->join(
                    array('company_companycategories' => 'company_companycategories'),
                    "company_companies.company_id = company_companycategories.company_id"
                );
                $select->where('company_companycategories.category_id = ?', $value);

                return $select;
                break;

            default:
                return parent::_addCondition($select, $field, $value);
                break;
        }
    }

    /**
     * @see Mcmr_Model_GenericMapper::_postAllConditions()
     */
    protected function _postAllConditions(Zend_Db_Select $select)
    {
        // Do not ever fetch a deleted company in an index
        $select->where('company_state <> ?', 'deleted');

        return $select;
    }

    /**
     * Save the company category ids
     *
     * @param Company_Model_Company $model
     * @return Company_Model_CompanyMapper
     */
    private function _saveCategoryids(Company_Model_Company $model)
    {
        $this->_deleteCategoryids($model);
        $table = new Mcmr_Db_Table('company_companycategories');
        $data['company_id'] = $model->getId();

        foreach ($model->getCategoryids() as $category) {
            $data['category_id'] = $category;
            $table->insert($data);
        }

        return $this;
    }

    /**
     * Delete the company category ids
     *
     * @param array $condition
     * @return Company_Model_CompanyMapper
     */
    private function _deleteCategoryids($model)
    {
        $condition = array('company_id=?' => $model->getId());
        $table = new Mcmr_Db_Table('company_companycategories');
        $table->delete($condition);

        return $this;
    }

    /**
     * Load the company category ids.
     *
     * @param Company_Model_Company $model
     * @return Company_Model_CompanyMapper
     */
    public function loadCategoryids($model)
    {
        $table = new Mcmr_Db_Table('company_companycategories');
        $rows = $table->fetchAll(array('company_id=?' => $model->getId()));

        $categories = array();
        foreach ($rows as $row) {
            $categories[] = $row['category_id'];
        }

        $model->setCategoryids($categories);

        return $this;
    }

}
