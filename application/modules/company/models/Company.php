<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Company.php 2281 2011-03-28 10:38:04Z michal $
 */

/**
 * Model class for all Games
 *
 * @category Company
 * @package Company_Model
 * @subpackage Company
 */
class Company_Model_Company extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Company_Model_CompanyMapper';


    protected $_categoryids = null;
    protected $_id = null;
    protected $_userid = null;
    protected $_typeid = null;
    protected $_state = null;
    protected $_title = null;
    protected $_description = null;
    protected $_listed = null;
    protected $_featured = null;
    protected $_featuredstart = null;
    protected $_featuredend = null;
    protected $_recruiter = null;
    protected $_recruiterstart = null;
    protected $_recruiterend = null;
    protected $_order = null;
    protected $_recruiterorder = null;
    protected $_image = null;

    private $_user = null;
    private $_type = null;
    private $_categories = null;

    /**
     * Return mapper for model
     *
     * @return Company_Model_CompanyMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Company_Model_Company
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the ID of the user who owns this company
     *
     * @return int
     */
    public function getUserid()
    {
        if (null === $this->_userid && Mcmr_Model_ModelAbstract::STATE_CLEAN !== $this->state()) {
            $user = Zend_Registry::get('authenticated-user');
            if (null !== $user) {
                $this->_userid = $user->getId();
            }
        }
        
        return $this->_userid;
    }

    /**
     * Set user ID of the user who owns this company
     *
     * @param int $userid
     * @return Company_Model_Company
     */
    public function setUserid($userid)
    {
        $this->_userid = $userid;

        return $this;
    }

    /**
     * Get the Company Type ID
     *
     * @return int
     */
    public function getTypeid()
    {
        if (null === $this->_typeid) {
            $this->_typeid = -1;
        }

        return $this->_typeid;
    }

    /**
     * Set the company type ID
     *
     * @param int $typeid
     * @return Company_Model_Company
     */
    public function setTypeid($typeid)
    {
        $this->_typeid = intval($typeid);

        return $this;
    }

    /**
     * Get the state of this company. Default value of 'unapproved'
     *
     * @return string
     */
    public function getState()
    {
        if (null === $this->_state) {
            $this->_state = 'unapproved';
        }
        
        return $this->_state;
    }
    
    /**
     * Set the state of this model
     *
     * @param string $state 
     */
    public function setState($state)
    {
        $this->_state = $state;
    }
    
    /**
     * Get the company title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the company title
     *
     * @param <type> $title
     * @return Company_Model_Company
     */
    public function setTitle($title)
    {
        $this->_title = $title;

        return $this;
    }

    /**
     * Get the company description
     *
     * @return string
     */
    public function getDescription()
    {
        if (null === $this->_description) {
            $this->_description = '';
        }

        return $this->_description;
    }

    /**
     * Set the company description
     *
     * @param string $description
     * @return Company_Model_Company
     */
    public function setDescription($description)
    {
        $this->_description = $description;

        return $this;
    }
    
    /**
     * Set the featured order
     *
     * @param int $order
     * @return Company_Model_Company
     */
    public function setOrder($order)
    {
        if (null === $order) {
            $this->_order = $order;
        } else {
            $this->_order = (int)$order;
        }
        
        return $this;
    }

    /**
     * Get the featured order
     *
     * @return int
     */
    public function getOrder()
    {
        if (null === $this->_order) {
            $this->_order = 1000;
        }
        
        return $this->_order;
    }

    /**
     * Get the recruiter display order
     *
     * @return int
     */
    public function getRecruiterorder()
    {
        if (null === $this->_recruiterorder) {
            $this->_recruiterorder = 1000;
        }

        return $this->_recruiterorder;
    }

    /**
     * Set the recruiter display order
     *
     * @param int $recruiterorder
     * @return Company_Model_Company
     */
    public function setRecruiterorder($recruiterorder)
    {
        $this->_recruiterorder = $recruiterorder;

        return $this;
    }

    /**
     * Get the company listed flag
     *
     * @return bool
     */
    public function getListed()
    {
        if (null === $this->_listed) {
            $this->_listed = false;
        }

        return $this->_listed;
    }

    /**
     * Set the company listed flag
     *
     * @param bool $listed
     * @return Company_Model_Company
     */
    public function setListed($listed)
    {
        $this->_listed = (bool)$listed;

        return $this;
    }


    /**
     * Set company featured or not
     *
     * @param bool 
     * @return Company_Model_Company
     */
    public function setFeatured($featured)
    {
        $this->_featured = $featured;
        
        return $this;
    }

    /**
     * Get flag for whether company is featured or not
     *
     * @return bool
     */
    public function getFeatured()
    {
        if (null === $this->_featured) {
            $this->_featured = false;
        }
        
        return $this->_featured;
    }
    
    /**
     * Set the featured end date as a unix timestamp
     *
     * @param int $featuredend
     * @return Company_Model_Company
     */
    public function setFeaturedend($featuredend)
    {
        $this->_featuredend = intval($featuredend);
        
        return $this;
    }

    /**
     * Get the featured end date as a unix timestamp
     *
     * @return int
     */
    public function getFeaturedend()
    {
        if (null === $this->_featuredend) {
            $this->_featuredend = time();
        }
        return $this->_featuredend;
    }

    /**
     * Set the featured start date as a unix timestamp
     *
     * @param int $featuredstart
     * @return Company_Model_Company
     */
    public function setFeaturedstart($featuredstart)
    {
        $this->_featuredstart = intval($featuredstart);
        
        return $this;
    }

    /**
     * Get the featured start date as a unix timestamp
     *
     * @return int
     */
    public function getFeaturedstart()
    {
        if (null === $this->_featuredstart) {
            $this->_featuredstart = time();
        }

        return $this->_featuredstart;
    }

    /**
     * Get Recruiter flag
     *
     * @return boolean
     */
    public function getRecruiter()
    {
        return $this->_recruiter;
    }

    /**
     * Set the recruiter flag
     *
     * @param bool $recruiter
     * @return Company_Model_Company
     */
    public function setRecruiter($recruiter)
    {
        $this->_recruiter = (bool)$recruiter;

        return $this;
    }

    /**
     * Get the recruiter start date
     *
     * @return int
     */
    public function getRecruiterstart()
    {
        return $this->_recruiterstart;
    }

    /**
     * Set the recruiter start date as unix timestamp
     *
     * @param int $recruiterstart
     * @return Company_Model_Company
     */
    public function setRecruiterstart($recruiterstart)
    {
        $this->_recruiterstart = (int)$recruiterstart;

        return $this;
    }

    /**
     * Get the recruiter end date as unix timestamp
     *
     * @return int
     */
    public function getRecruiterend()
    {
        return $this->_recruiterend;
    }

    /**
     * Set the recruiter end date as a unix timestamp
     *
     * @param int $recruiterend
     * @return Company_Model_Company
     */
    public function setRecruiterend($recruiterend)
    {
        $this->_recruiterend = (int)$recruiterend;

        return $this;
    }

    /**
     * Return a company type model object
     *
     * @return Company_Model_Type
     */
    public function getType()
    {
        if (null === $this->_type) {
            $this->_type = Company_Model_Type::getMapper()->find($this->_typeid);
        }

        return $this->_type;
    }

    /**
     * Set the image for this company
     *
     * @param string $image
     * @return Company_Model_Company 
     */
    public function setImage($image)
    {
        $this->_image = $image;

        return $this;
    }

    /**
     * Get the image for this company
     *
     * @return string
     */
    public function getImage()
    {
        return $this->_image;
    }

    /**
     * Return a user model object of the user who owns this company
     *
     * @return User_Model_User
     */
    public function getUser()
    {
        if (null === $this->_user) {
            $id = $this->getUserid();
            if (null !== $id) {
                $mapper = User_Model_User::getMapper();
                $this->_user = $mapper->find($id);
            }
        }

        return $this->_user;
    }

    /**
     * Determine whether this company is featured or not.
     * 
     * @return bool
     */
    public function isFeatured()
    {
        $now = time();
        
        return $this->getFeatured() && ($this->getFeaturedstart()<$now) && ($this->getFeaturedend()>$now);
    }
    
    

    /**
     * Get category ids
     */
    public function getCategoryids()
    {
        if (null === $this->_categoryids) {
            $this->_categoryids = array();
        }
        return $this->_categoryids;
    }

    /**
     * Set the category IDs
     *
     * @param array|int $categoryids
     * @return Product_Model_Product
     */
    public function setCategoryids($categoryids)
    {
        if ( !$categoryids) {
            $categoryids = array();
        } elseif (!is_array($categoryids)) {
            if (is_numeric($categoryids)) {
                $categoryids = array($categoryids);
            } else {
                $categoryids = null;
            }
        }

        $this->_categoryids = $categoryids;
        $this->_categories = null;

        return $this;
    }

    /**
     * Return categories. Lazy loading
     */
    public function getCategories()
    {
        if (null === $this->_categories) {
            $this->_categories = array();
            $ids = $this->getCategoryids();
            $mapper = Company_Model_Category::getMapper();
            foreach ($ids as $id) {
                $category = $mapper->find($id);
                if (null !== $category) {
                    $this->_categories[] = $category;
                }
            }
        }
        return $this->_categories;
    }

    
}
