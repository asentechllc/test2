<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CategoryMapper.php 1077 2010-07-20 15:20:18Z leigh $
 */

/**
 * Mapper class for all Category models
 *
 * @category Company
 * @package Company_Model
 * @subpackage CategoryMapper
 */
class Company_Model_CategoryMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Company_Model_DbTable_Category';
    protected $_columnPrefix = 'category_';
    protected $_modelClass = 'Company_Model_Category';
    protected $_cacheIdPrefix = 'CompanyCat';

    public function  __construct()
    {
        parent::__construct();

        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
    }

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseField()
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }
    
    /**
     * Fetch an array of all the categories to use as select Options
     *
     * @param int $parentId
     * @param string $indent
     * @return array 
     */
    public function categoryOptions($parentId = 0, $indent = '')
    {
        $options = array();
        
        $categories = $this->findAllByField(array('parentId'=>$parentId), array('title'=>'asc'), array('page'=>1, 'count'=>1000));
        foreach ($categories as $category) {
            $options[$category->getId()] = $indent . $category->getTitle();
            $childOptions = $this->categoryOptions($category->getId(), $indent.'&nbsp;&nbsp;&nbsp;-&nbsp;');

            // Cannot use array_merge, it destroys the array index
            foreach ($childOptions as $key=>$option) {
                $options[$key] = $option;
            }
        }
        
        return $options;
    }
}
