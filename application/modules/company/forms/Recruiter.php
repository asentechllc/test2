<?php

class Company_Form_Recruiter extends Mcmr_Form
{
    public function init()
    {
        $this->setName('companyformrecruiter')->setElementsBelongTo('company-form-recruiter');
        $this->setMethod('post');
        
        $config = Zend_Registry::get('company-config');
        $count = isset($config->numberFeatured)?$config->numberFeatured:4;
        
        // Get the recruiter companies
        $mapper = Company_Model_Company::getMapper();
        $condition = null;
        $order = array('recruiter'=>'desc', 'recruiterorder'=>'asc');
        $page=array('page'=>1, 'count'=>$count);
        $companies = $mapper->findAllByField($condition, $order, $page);
        
        $subform = new Zend_Form_SubForm();
        $subform->setElementsBelongTo('companies');
        
        $x=0;
        foreach ($companies as $company) {
            $element = array(
                    'options'=>array(
                        'label'=>'company '.($x+1),
                        'required'=>true,
                        'value'=>$company->getId(),
                    ),
                    'optionSource'=>array(
                        'table'=>'company_companies',
                        'valueField'=>'company_id',
                        'displayField'=>'company_title',
                    ));
            $element = $this->_processElement($element);
            $subform->addElement('Select', "$x", $element['options']);
            $x++;
        }
        
        $this->addSubForm($subform, 'companies');
        
        $this->addElement(
            'Submit', 'submit',
            array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );

        
        // Add some CSRF protection
        $this->addElement(
            'hash', 'csrf',
            array(
                'salt' => 'unique'
            )
        );
    }
}
