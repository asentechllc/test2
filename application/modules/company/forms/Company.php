<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Company.php 1792 2010-11-17 17:09:21Z leigh $
 */

/**
 * Form class
 *
 * @category Company
 * @package Company_Form
 * @subpackage Company
 */
class Company_Form_Company extends Mcmr_Form
{
    public function init()
    {
        parent::init();
        $this->setName('companyformcompany')->setElementsBelongTo('company-form-company');
        $this->setMethod('post');
        $this->setEnctype("multipart/form-data");

        $this->addElement(
            'Submit', 'submit',
            array(
                'ignore' => true ,
                'label' => 'Save'
            )
        );
    }

    public function postInit()
    {
    }
}
