<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Form class for Company bulk update
 *
 * @category Company
 * @package Company_Form
 * @subpackage Order
 */
class Company_Form_Order extends Mcmr_Form
{
    private $_companies = null;
    /*
      private $_models = null;
     */

    public function postInit()
    {
        $this->setName('companyformorder')->setElementsBelongTo('company-form-order');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');

        $subform = new Mcmr_Form();
        $subform->setElementsBelongTo('companies');

        $rowCount = $this->_getRowCount();
        $companies = $this->_getInitialCompanies($rowCount);

        $x = 1;
        $displayGroupElements = array();
        for ($i = 1; $i <= $rowCount; $i++) {
            if ($i <= $companies->gettotalItemCount()) {
                $item = $companies->getItem($i);
                $value = $item->getId();
            } else {
                $value = '';
            }
            $element = array(
                    'options' => array(
                            'label' => 'Company ' . $x,
                            'required' => true,
                            'value' => $value,
                            'class' => 'sortable'
                    ),
                    'optionSource' => $this->getOptionSourceConfig()
            );
            $element = $this->_processElement($element);
            $subform->addElement('Select', "$i", $element['options']);
            $displayGroupElements[] = "$i";
        }


        if (isset($displayGroupElements[0])) {
            $subform->addDisplayGroup($displayGroupElements, 'companies', array('class' => 'sortable-items'));
        }

        $this->addSubForm($subform, 'companies');
    }

    protected function getOptionSourceConfig()
    {
        if (null !== $this->_companies && isset($this->_companies['optionSource'])) {
            return $this->_companies['optionSource'];
        } else {
            return array(
                    'mapper' => 'Company_Model_Company',
                    'conditions' => array(),
                    'order' => array('title' => 'ASC'),
                    'page' => array('page' => '1', 'count' => '100000'),
                    'displayField' => 'title',
                    'valueField' => 'id',
                    'allowNull' => true,
                    'allowNullDisplay' => 'Please Select...');
        }
    }

    public function setCompanies($companies)
    {
        $this->_companies = $companies;
    }

    public function getCompanies()
    {
        return $this->_companies;
    }

    /*
      public function getCompanyModels()
      {
      return $this->_models;
      }
     */

    protected function _getRowCount()
    {
        if (null !== $this->_companies && isset($this->_companies['count'])) {
            return $this->_companies['count'];
        } else {
            return 10;
        }
    }

    protected function _getInitialCompanies($rowCount)
    {
        $order = array('order' => 'asc');
        $mapper = Company_Model_Company::getMapper();
        $page['page'] = 1;
        $page['count'] = $rowCount;
        $companies = $mapper->findAllByField(array('featured' => 1), $order, $page);
        return $companies;
    }

}
