<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Category.php 2175 2011-02-09 17:01:23Z leigh $
 */

/**
 * Description of Category
 *
 * @category Company
 * @package Company_Form
 * @subpackage Category
 */
class Company_Form_Category extends Mcmr_Form
{
    public function init()
    {
        $this->setName('companyformcategory')->setElementsBelongTo('company-form-category');
        $this->setMethod('post');
        // Add some CSRF protection
        $this->addElement(
            'hash', 'csrf', array('salt' => 'unique')
        );
    }
}
