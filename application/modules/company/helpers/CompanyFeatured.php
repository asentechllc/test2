<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Company
 * @package    Company_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of companies based on the conditions
 *
 * @category   Company
 * @package    Company_View
 * @subpackage Helper
 */
class Company_View_Helper_CompanyFeatured extends Zend_View_Helper_Abstract
{

    public function companyFeatured($limit = 7, $useDates = true)
    {
        $mapper = Company_Model_Company::getMapper();
        
        $conditions = array(
            'featured'=>1,
        );

        if ($useDates) {
            $conditions['featuredstart'] = array('condition'=>'<=', 'value'=>time());
            $conditions['featuredend'] = array('condition'=>'>', 'value'=>time());
        }

        $order = array('order'=>'asc');

        return $mapper->findAllByField($conditions, $order, array('count'=>$limit, 'page'=>1));
    }
}
