<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyTypeIndex
 *
 * @author leigh
 */
class Company_View_Helper_CompanyTypeIndex extends Zend_View_Helper_Abstract
{
    public function companyTypeIndex($condition = null, $order=null, $page=null, $partial = null)
    {
        $mapper = Company_Model_Type::getMapper();
        $types = $mapper->findAllByField($condition, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('types'=>$types));
                }
            }

            return $this->view->partial($partial, array('types'=>$types));
        }

        return $types;
    }
}
