<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CompanyCategoryIndex
 *
 * @author leigh
 */
class Company_View_Helper_CompanyCategoryIndex extends Zend_View_Helper_Abstract
{
    public function companyCategoryIndex($condition = null, $order=null, $page=null, $partial = null)
    {
        $mapper = Company_Model_Category::getMapper();
        $categories = $mapper->findAllByField($condition, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('categories'=>$categories));
                }
            }

            return $this->view->partial($partial, array('categories'=>$categories));
        }

        return $categories;
    }
}
