<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Company
 * @package    Company_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of Companys based on the conditions
 *
 * @category   Company
 * @package    Company_View
 * @subpackage Helper
 */
class Company_View_Helper_CompanyRead extends Zend_View_Helper_Abstract
{
    public function companyRead($condition = null, $order=null, $partial = null)
    {
        $mapper = Company_Model_Company::getMapper();
        $company = $mapper->findOneByField($condition, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('company'=>$company));
                }
            }

            return $this->view->partial($partial, array('company'=>$company));
        }

        return $company;
    }
}
