<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Company
 * @package    Company_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View helper to retrieve the number of companies based on the conditions
 *
 * @category   Company
 * @package    Company_View
 * @subpackage Helper
 */
class Company_View_Helper_CompanyCount extends Zend_View_Helper_Abstract
{
    public function companyCount($conditions=null)
    {
        $mapper = Company_Model_Company::getMapper();

        return $mapper->count($conditions);
    }
}
