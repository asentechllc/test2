<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 1308 2010-09-03 15:22:16Z leigh $
 */

/**
 * Bootstrap
 *
 * @category Company
 * @package Company_Bootstrap
 */
class Company_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');

        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Company_View_Helper');
    }
}