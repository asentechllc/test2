<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_CategoryController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: CategoryController.php 2231 2011-03-02 15:35:35Z michal $
 */

/**
 * Category controller for the company module. Looks after all category models
 *
 * @category Company
 * @package Company_CategoryController
 */
class Company_CategoryController extends Zend_Controller_Action
{

    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('contextSwitch');

            $contextSwitch->addContext(
                'rss',
                array(
                    'headers'=>array('Content-Type'=>'application/rss+xml'),
                    'suffix'=>'rss',
                )
            )
            ->addContext(
                'plist',
                array(
                    'headers'=>array('Content-Type'=>'text/xml'),
                    'suffix'=>'plist',
                )
            );

            $contextSwitch->addActionContext('index', 'rss')
                    ->addActionContext('index', 'plist')
                    ->addActionContext('read', 'plist')
                    ->initContext();

            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                    ->addActionContext('index', 'json')
                    ->addActionContext('index', 'html')
                    ->addActionContext('delete', 'json')
                    ->addActionContext('delete', 'html')
                    ->addActionContext('update', 'json')
                    ->addActionContext('update', 'html')
                    ->addActionContext('create', 'json')
                    ->addActionContext('create', 'html')
                    ->initContext();
    
        } catch (Exception $e) {
        }
    }

    /**
     * Display an index of company categories
     */
    public function indexAction()
    {
        $fields = array();
        $fields['title'] = $this->_request->getParam('title', null);
        $fields['parentId'] = $this->_request->getParam('parentId', null);
        
        // Allow search on partial string. If it contains a % assume a LIKE search
        if (false !== strpos($fields['title'], '%')) {
            $fields['title'] = array('condition'=>'LIKE', 'value'=>$fields['title']);
        }
        
        $order = array('order'=>'asc', 'title'=>'asc');
        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('company-config')->categories->itemCountPerPage);
        
        $mapper = Company_Model_Category::getMapper();
        $this->view->categories = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Create a company category. Display form on GET. Save on POST
     */
    public function createAction()
    {
        $form = new Company_Form_Category();
        $this->view->form = $form;
        $this->view->error = false;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $category = new Company_Model_Category();
                $category->setOptions($values);
                try {
                    $mapper = Company_Model_Category::getMapper();
                    $mapper->save($category);
                    $this->view->category = $category;

                    $this->view->getHelper('DisplayMessages')->addMessage('categorySaved', 'info');
//                    $this->view->getHelper('Redirect')->notify('create', $category);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'error');
            }
        }
    }

    /**
     * Display a single company category.
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);

        if (null !== $id || null !== $url) {
            $mapper = Company_Model_Category::getMapper();

            // Get the company category from the ID or the URL provided
            $category = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            if (null !== $category) {
                $this->view->category = $category;
            } else {
                throw new Mcmr_Exception_PageNotFound('Category not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a company category. Display form on GET. Save on POST
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Company_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $form = new Company_Form_Category();
                $form->populate($category->getOptions(true));
                $this->view->form = $form;
                $this->view->category = $category;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $category->setOptions($values);
                        try {
                            $mapper = Company_Model_Category::getMapper();
                            $mapper->save($category);

                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('categorySaved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $category);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Category Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a company category. Confirm on GET. Delete on POST
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Company_Model_Category::getMapper();
            if (null !== ($category = $mapper->find($id))) {
                $this->view->category = $category;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($category);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Category deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $category);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Category Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }
    
    /**
     * Action to change order field on a number of categories at once.
     */
    public function orderAction() 
    {

        $conditions = array( 'featured' => 1 );
        $order = array('order' => 'asc');
        $count = 1000000;
        $mapper = Company_Model_Category::getMapper();
        $page['page'] = 1;
        $page['count'] = $count;

        $categories = $mapper->findAllByField($conditions, $order, $page);

        $form = new Company_Form_CategoryOrder(null, $categories);

        $this->view->form = $form;
        $this->view->categories = $categories;
        
        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                try {
                    $values = $form->getValues(true);
                    $categories = $mapper->findAllByField(
                        array('featured' => 1),
                        null,
                        array('page'=>1, 'count'=>100000)
                    );
                    foreach ($categories as $categories) {
                        $categories->setOrder(100001);
                        $mapper->save($categories);
                    }
                    foreach ($values[ 'categories' ] as $order => $id) {
                        $categories = $mapper->find($id);
                        $categories->setOrder($order);
                        $mapper->save($categories);
                        
                    }
                } catch (Exception $e) {
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    $this->view->error = true;
                }
                $this->view->getHelper('DisplayMessages')->addMessage('Categories Saved', 'info');
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('Incorrect Form Values', 'warn');
                $this->view->error = true;
            }
        }
    }

}
