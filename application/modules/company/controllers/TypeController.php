<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_TypeController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: TypeController.php 2178 2011-02-10 11:20:22Z leigh $
 */

/**
 * Description of TypeController
 *
 * @category Company
 * @package Company_TypeController
 */
class Company_TypeController extends Zend_Controller_Action
{
    public function init()
    {
        try {
            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                            ->addActionContext('index', 'json')
                            ->addActionContext('index', 'html')
                            ->addActionContext('create', 'html')
                            ->addActionContext('create', 'json')
                            ->addActionContext('update', 'html')
                            ->addActionContext('update', 'json')
                            ->addActionContext('delete', 'html')
                            ->addActionContext('delete', 'json')
                            ->addActionContext('read', 'html')
                            ->addActionContext('read', 'json')
                            ->initContext();
        } catch (Exception $e) {

        }
    }

    /**
     * Display an index of company types.
     */
    public function indexAction()
    {
        $fields = array();

        $order = array('title'=>'asc');

        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('company-config')->types->itemCountPerPage);
        
        $mapper = Company_Model_Type::getMapper();
        $this->view->types = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Create a new company type. Display form on GET. Save on POST.
     */
    public function createAction()
    {
        $form = new Company_Form_Type();
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);

                $type = new Company_Model_Type();
                $mapper = Company_Model_Type::getMapper();
                $type->setOptions($values);
                try {
                    $this->view->error = false;
                    $mapper->save($type);

                    $this->view->getHelper('DisplayMessages')->addMessage('Company Type Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $type);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Display a single company type.
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        if (null !== $id || null !== $url) {
            $mapper = Company_Model_Type::getMapper();
            $type = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            if (null !== $type) {
                $this->view->type = $type;
            } else {
                throw new Mcmr_Exception_PageNotFound('Company Type does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Update a company type. Display form on GET. Save on POST.
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $form = new Company_Form_Type();
            $this->view->form = $form;

            $mapper = Company_Model_Type::getMapper();
            if (null !== ($type = $mapper->find($id))) {
                $this->view->type = $type;

                if ($this->_request->isPost()) {
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);
                        $type->setOptions($values);

                        try {
                            $this->view->error = false;
                            $mapper->save($type);

                            $this->view->getHelper('DisplayMessages')->addMessage('Company Type Saved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $type);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    }
                } else {
                    $form->populate($type->getOptions());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Company Type does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a company type. Display confirmation on GET. Delete on POST.
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Company_Model_Type::getMapper();
            if (null !== ($type = $mapper->find($id))) {
                $this->view->type = $type;
                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($type);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Company Type Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $type);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Company Type does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }
}
