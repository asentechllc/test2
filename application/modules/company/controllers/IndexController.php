<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Company
 * @package Company_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2363 2011-04-18 09:29:37Z leigh $
 */

/**
 * Index Controller for the Company module
 *
 * @category Company
 * @package Company_IndexController
 */
class Company_IndexController extends Zend_Controller_Action
{
    
    public function init()
    {
        try {
            $contextSwitch = $this->_helper->getHelper('ContextSwitch');
            $contextSwitch->addContext('html', array())
                ->addContext(
                    'csv',
                    array(
                        'headers'=>array('Content-Type'=>'text/csv'),
                        'suffix'=>'csv',
                    )
                );

            $contextSwitch->addActionContext('index', 'csv')
                    ->initContext();
            
            $ajaxContext = $this->_helper->getHelper('AjaxContext');
            $ajaxContext->setAutoJsonSerialization(false)
                ->addActionContext('index', 'json')
                ->addActionContext('index', 'html')
                ->addActionContext('create', 'html')
                ->addActionContext('create', 'json')
                ->addActionContext('update', 'html')
                ->addActionContext('update', 'json')
                ->addActionContext('read', 'html')
                ->addActionContext('read', 'json')
                ->initContext();
        } catch (Exception $e) {
        }
    }

    /**
     * Display an index of companies
     */
    public function indexAction()
    {
        $fields = array();
        if ($this->_request->getParam('ltitle', false)) {
            $fields['title'] = '%' . $this->_request->getParam('ltitle') . '%';
        } else {
            $fields['title'] = $this->_request->getParam('title', null);
        }
        // Allow search on partial string. If it contains a % assume a LIKE search
        if (false !== strpos($fields['title'], '%')) {
            $fields['title'] = array('condition'=>'LIKE', 'value'=>$fields['title']);
        }
        if (false !== $this->_request->getParam('q', false)) {
            $fields['title'] = array('condition'=>'LIKE', 'value'=>$this->_request->getParam('q').'%');
        }
        if ($this->_request->getParam('type', false)) {
            $type = Company_Model_Type::getMapper()->findOneByField(array('url'=>$this->_request->getParam('type', false)));
            $fields['typeid'] = $type->getId();
        } else {
            $fields['typeid'] = $this->_request->getParam('typeid', null);
        }
        $fields['categoryid'] = $this->_request->getParam('categoryid', null);
        $fields['state'] = $this->_request->getParam('state', null);
        $fields['listed'] = $this->_request->getParam('listed', null);

        $order = array('title'=>'asc');
        
        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('company-config')->companies->itemCountPerPage);
        
        $mapper = Company_Model_Company::getMapper();
        $this->view->companies = $mapper->findAllByField($fields, $order, $page);
    }

    /**
     * Create a new company. Display form on GET. Save on POST.
     */
    public function createAction()
    {
        $form = $this->_helper->loadForm('Company');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $mapper = Company_Model_Company::getMapper();
                $company = new Company_Model_Company();
                $company->setOptions($form->getValues(true));

                try {
                    $mapper->save($company);
                    $this->view->company = $company;
                    $this->view->error = false;

                    $this->view->getHelper('DisplayMessages')->addMessage('companySaved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $company);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
            }
        }
    }

    /**
     * Display a single company.
     */
    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Company_Model_Company::getMapper();
            if (null !== ($company = $mapper->find($id))) {
                if (null === $company->getId()) {
                    throw new Mcmr_Exception_PageNotFound("Company does not exist");
                }
                
                $this->view->company = $company;
            } else {
                throw new Mcmr_Exception_PageNotFound('Company does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Upate a company. Display form on GET. Save on POST.
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Company_Model_Company::getMapper();
            if (null !== ($company = $mapper->find($id))) {
                $form = $this->_helper->loadForm('Company');
                $form->populate($company->getOptions(true));

                $this->view->company = $company;
                $this->view->form = $form;

                if ($this->_request->isPost()) {
                    // Merge the existing company information to allow partial post updates
                    $post = $this->_request->getPost();
                    $companyvalues = $company->getOptions(true);
                    $post['companyformcompany'] = array_merge($companyvalues, $post['companyformcompany']);
                    if ($form->isValid($post)) {
                        $company->setOptions($form->getValues(true));

                        try {
                            $mapper->save($company);

                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('companySaved', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $company);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Company does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Delete a company. Display confirmation on GET. Delete on POST.
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Company_Model_Company::getMapper();
            if (null !== ($company = $mapper->find($id))) {
                $this->view->company = $company;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($company);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('companyDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $company);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Company does not exist');
            }
        } else {
            $this->_forward('index');
        }
        
    }

    public function orderAction()
    {
        $form = new Company_Form_Order();

        $this->view->form = $form;

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                try {
                    $mapper = Company_Model_Company::getMapper();
                    $mapper->resetFeatured();

                    $values = $form->getValues(true);
                    $orderedIds = array();
                    foreach ($values[ 'companies' ] as $order => $id) {
                        //do not assign order to the same company twice
                        //if the user selects the same company in the form, it's the first occuracne that should count
                        if (isset($orderedIds[$id])) {
                            continue;
                        }
                        $orderedIds[ $id ] = 1;
                        $company = $mapper->find($id);
                        if (null !== $company) {
                            $company->setOrder($order);
                            $company->setFeatured(true);
                            $mapper->save($company);
                        }
                    }
                    //after order is set the form needs recreating with the correct values
                    $this->view->form = new Company_Form_Order();
                } catch (Exception $e) {
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    $this->view->error = true;
                }
                $this->view->getHelper('DisplayMessages')->addMessage('orderSaved', 'info');
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
                $this->view->error = true;
            }
        }
    }

    public function orderrecruiterAction()
    {
        $form = new Company_Form_Order();

        $this->view->form = $form;

        if ($this->_request->isPost()) {
            $post = $this->_request->getPost();
            if ($form->isValid($post)) {
                try {
                    $mapper = Company_Model_Company::getMapper();
                    $mapper->resetRecruiter();

                    $values = $form->getValues(true);
                    foreach ( $values[ 'companies' ] as $order => $id ) {
                        $company = $mapper->find($id);
                        if (null !== $company) {
                            $company->setRecruiterorder($order);
                            $company->setRecruiter(true);
                            $mapper->save($company);
                        }
                    }
                } catch (Exception $e) {
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    $this->view->error = true;
                }
                $this->view->getHelper('DisplayMessages')->addMessage('orderSaved', 'info');
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
                $this->view->error = true;
            }
        }
    }

    public function featuredAction()
    {
        $mapper = Company_Model_Company::getMapper();
        $this->view->companies = $mapper->findAllByField(array('featured'=>'1'), array('order'=>'asc'));
        
        $form = $this->_helper->loadForm('Featured');
        $this->view->form = $form;
        
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $mapper->resetFeatured();
                foreach ($values['companies'] as $order=>$id) {
                    $company = $mapper->find($id);
                    $company->setFeatured(true);
                    $company->setOrder($order);
                    try {
                        $mapper->save($company);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
                
                $this->view->getHelper('DisplayMessages')->addMessage('featuredCompaniesSet', 'info');
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
                
            }
        }
        
    }

    public function clearfeaturedAction()
    {
        $mapper = Company_Model_Company::getMapper();
        $this->view->companies = $mapper->findAllByField(array('featured'=>'1'), array('order'=>'asc'));

        if ($this->_request->isPost()) {
            $mapper->resetFeatured();
            $this->view->getHelper('DisplayMessages')->addMessage('featuredCompaniesReset', 'info');
        }
    }

    public function recruiterAction()
    {
        $mapper = Company_Model_Company::getMapper();
        $this->view->companies = $mapper->findAllByField(array('recruiter'=>'1'), array('order'=>'asc'));

        $form = $this->_helper->loadForm('Recruiter');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $mapper->resetRecruiter();
                foreach ($values['companies'] as $order=>$id) {
                    $company = $mapper->find($id);
                    $company->setRecruiter(true);
                    $company->setRecruiterorder($order);
                    try {
                        $mapper->save($company);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }

                $this->view->getHelper('DisplayMessages')->addMessage('recruiterCompaniesSet', 'info');
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');

            }
        }

    }

    public function clearrecruiterAction()
    {
        $mapper = Company_Model_Company::getMapper();
        $this->view->companies = $mapper->findAllByField(array('recruiter'=>'1'), array('order'=>'asc'));

        if ($this->_request->isPost()) {
            $mapper->resetRecruiter();
            $this->view->getHelper('DisplayMessages')->addMessage('recruiterCompaniesReset', 'info');
        }
    }
}
