<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mailqueue
 * @package Mailqueue_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of Published
 *
 * @category Mailqueue
 * @package Mailqueue_Acl
 * @subpackage CheckConfig
 */
class Mailqueue_Acl_CheckConfig implements Zend_Acl_Assert_Interface
{

	private $_errors = array();

    /**
     * Returns true if adestra config is safe
     * Checks all email types and ensure there are overrides in place for each environment
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
    
    	# no sense checking this for production
    	if(APPLICATION_ENV == 'production')
    		return true;
    
    	$cfgFile = 'mailqueue.ini';
    	$envs = array(
    		'staging','testing','development',
    	);
    	$errMessage = 'needs to be configured for the given environment and cannot be the same as for production';

    	$cfg = array();

		# get configs for each environment
    	foreach(array_merge(array('production'),$envs) as $env){
        	$cfg[$env] = new Zend_Config_Ini(SITE_PATH.'/configs/modules/'.$cfgFile,$env);
		}
		
		$cfgName = 'adestra.';

		# check for adestra blocks in each env
		if(!isset($cfg['production']->adestra))
			$this->_error('Adestra config not found in '.$cfgFile);

		# check each email
		foreach($cfg['production']->adestra->toArray() as $emailName=>$email){

			$cfgEmailName = $cfgName.$emailName.'.';

			if(!isset($email['campaign']['id']))
				$this->_error('Missing campaign in '.$emailName.' email','production');

			if(!isset($email['lists']))
				$this->_error('No lists for '.$emailName.' email','production');
				
			# check against each environment
			foreach($envs as $env){

				$envEmail = $cfg[$env]->adestra->$emailName->toArray();

				# check campaign ID
				if($envEmail['campaign']['id'] == $email['campaign']['id'])
					$this->_error("{$cfgEmailName}campaign.id $errMessage",$env);
					
				# check list IDs
				foreach($email['lists'] as $listName=>$list){
					$cfgListName = $cfgEmailName.'lists.'.$listName;
					$envList = $envEmail['lists'][$listName];

					if($list['id'] == $envList['id'])
						$this->_error("{$cfgListName}.id $errMessage",$env);

					if($list['unsubId'] == $envList['unsubId'])
						$this->_error("{$cfgListName}.unsubId $errMessage",$env);

				}
			}

		}

		if($this->_errors){
		
			$acl->setReason("Configuration Error detected in $cfgFile");
			
			# return more information if available
			if($acl instanceof Mcmr_Acl){
				foreach($this->_errors as $errorEnv=>$errors){
					foreach($errors as $err){
						$acl->addMessage('<b>['.$errorEnv.']</b> '.$err);
					}
				}
			}
		
			return false;
		}

		return true;
    }
    
    private function _error($m,$env){
    	$this->_errors[$env][] = $m;    
    }
}
