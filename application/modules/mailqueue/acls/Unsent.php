<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mailqueue
 * @package Mailqueue_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of Published
 *
 * @category Mailqueue
 * @package Mailqueue_Acl
 * @subpackage Unsent
 */
class Mailqueue_Acl_Unsent implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if the email is unsent
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
        }

        if (null !== $id) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            $email = $mapper->find($id);
            
            return (null !== $email && (Mailqueue_Model_Mail::STATUS_UNSENT === $email->getStatus()
                    || Mailqueue_Model_Mail::STATUS_FAILURE === $email->getStatus()
                    ));
        } else {
            return false;
        }

    }
}
