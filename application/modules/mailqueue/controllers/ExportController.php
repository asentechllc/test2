<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_TypeController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: TypeController.php 2345 2011-04-11 15:07:15Z leigh $
 */

/**
 * Type controller for the news module. Looks after all news types.
 *
 * @category News
 * @package News_TypeController
 */
class Mailqueue_ExportController extends Mcmr_Controller_Action
{

    public function downloadAction()
    {
        $id = $this->_request->getParam('id', null);
        $seoId = $this->_request->getParam('seoId', null);
        $url = $this->_request->getParam('url', null);
        if (null !== $id || null !== $url || null !== $seoId) {
            if($id){
                $this->_model = $this->_mapper->find($id);
            } else {
                $this->_model = $this->_mapper->findOneByField(array('url'=>$url));
            }
            if (null !== $this->_model) {
                $directory = $this->_model->getDirectory();
                $file = $this->_model->getId();
                $path = $directory . DIRECTORY_SEPARATOR . $file;
                $this->_response->setHeader('Content-type', Mcmr_StdLib::getFileType($path));
                $this->_response->setHeader('Content-Disposition', 'attachment; filename="'.$file.'"');
                $this->_response->sendHeaders();
                readfile($path);
                // Exit as the file has been output
                exit;
            } else {
                throw new Mcmr_Exception_PageNotFound('Not found');
            }
        } else {
            $this->_forward('index');
        }
    }

}
