<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mailqueue
 * @package Mailqueue_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 1543 2010-10-14 11:18:30Z jay $
 */

/**
 * Index Controller for the Mailqueue module
 *
 * @category Mailqueue
 * @package Mailqueue_IndexController
 */
class Mailqueue_IndexController extends Zend_Controller_Action
{

	public function init(){
		$cfg = $this->view->config('mailqueue');
		if(isset($cfg->subscribe->sectors)){
			$this->_sectors = array();
			foreach($cfg->subscribe->sectors->toArray() as $sector){
				$this->_sectors[$sector] = $sector;
			}
		}
		parent::init();
	}

	public function subscribeAction(){
	
		$url = $this->_request->getParam('url');
		if($url){
			$this->_helper->viewRenderer('subscribe-'.$url);
		}
	
		$this->view->countries = $this->_countries;
		$this->view->sectors = $this->_sectors;
		$this->view->functions = $this->_functions;
	}

	public function preferencesAction(){

		$this->view->pageCache()->disable();
		$cfg = $this->view->config('admin-user');

		# no config? no go
		if(empty($cfg))
			throw new Exception("Could not find user.ini module config in admin");
		
		$error = false;
		
		try{
			# try to authenticate via params, or by existing session (if params not present)
			$contact = Mcmr_Service_Adestra_Contact::authenticate(
				$cfg->adestra->coreTableId,
				$this->_request->getParam('id'),
				$this->_request->getParam('guid')
			);
		}catch(Exception $e){
		
			# invalid request
			if(preg_match('/Request could not be processed|Object not found/',$e->getMessage())){
				$error = true;
			}
			
			# otherwise pass it on
			else throw new Exception($e->getMessage());
		}

		if(empty($contact)){
			$error = true;
		}
		
		if($error){
			$this->_helper->viewRenderer('preferences-error');
			return;
			//throw new Exception('Unknown contact - invalid id or guid or session expired');
		}

        $form = $this->_helper->loadForm('Preferences');
		$this->view->form = $form;
		
		$data = $contact->getValues(true);

		$this->view->data = $data['contact_data'];

		$contactLists = $contact->lists();

		$this->view->lists = array();
		$this->view->contactLists = array();
		if(isset($cfg->adestra->preferences->lists)){
			$lists = $cfg->adestra->lists->toArray();
			$this->view->lists = $cfg->adestra->preferences->lists->toArray();

			foreach($this->view->lists as $k=>$v){
			
				# dynamically add element to form based on config
				$form->addElement('checkbox','list_'.$k.'');
				
				if(isset($lists[$k]) && in_array($lists[$k]['id'],$contactLists)){
					$this->view->contactLists[$k] = 1;
				}
			}
		}

		$this->view->countries = $this->_countries;
		$this->view->sectors = $this->_sectors;
		$this->view->functions = $this->_functions;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {

                $values = $form->getValues(true);
            	
            	$subscribes = array();
            	$unsubscribes = array();

            	foreach($this->view->lists as $k=>$v){

            		if(!isset($lists[$k]))
            			continue;
            		
            		$v = $lists[$k];

            		$subscribe = isset($values['list_'.$k]) && $values['list_'.$k];

					if(isset($lists[$k])){
						$listIDs = array(
							$lists[$k]['id'],		// sub
							$lists[$k]['unsubId'],	// unsub
						);
						
						# is user already subscribed or unsubscribed?
						$subscribed = in_array($listIDs[0],$contactLists);

						if($subscribe && !$subscribed)
							$subscribes[] = $listIDs;
						
						if(!$subscribe && $subscribed)
							$unsubscribes[] = $listIDs;
						
					}
            	}

            	# has anything changed?
            	$changed = false;
            	$contactValues = $contact->getValues();
            	foreach($contactValues['contact_data'] as $k=>$v){
            		if(isset($values[$k])){
            			if($values[$k] != $v){
            				$changed = true;
            				break;
            			}
            		}
            	}

				if($changed){
	                $contact->setValues($values);
    	            $contact->save();
    	        }

                # process subscriptions
                foreach($subscribes as $listIDs)
                	$contact->subscribe($listIDs[0],$listIDs[1]);

                # process unsubscriptions
                foreach($unsubscribes as $listIDs)
                	$contact->unsubscribe($listIDs[0],$listIDs[1]);
                
                Mcmr_Service_Adestra_Contact::deauthenticate();

				$this->view->redirectNow($this->view->url(array('module'=>'default', 'controller'=>'index', 'action'=>'read','url'=>'newsletter-preferences-thanks'), 'newsletter-preferences-thanks', true));
            }
        }
	}

    public function indexAction()
    {
        $fields = array();
        $order = array('sendtime'=>'desc');
        
        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', Zend_Registry::get('mailqueue-config')->mailqueue->itemCountPerPage);
        
        $mapper = Mailqueue_Model_Mail::getMapper();
        $this->view->emails = $mapper->findAllByField($fields, $order, $page);
    }

    public function createAction()
    {
        $type = $this->_request->getParam('type', Zend_Registry::get('mailqueue-config')->defaultType);
        $form = $this->_helper->loadForm('Mail', array('type'=>$type));
        $this->view->form = $form;
        
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $email = new Mailqueue_Model_Mail();
                $email->setOptions($values);

                $values['preview'] = isset($values['preview'])?$values['preview']:false;
                $values['test'] = isset($values['test'])?$values['test']:false;

                $config = Zend_Registry::get('mailqueue-config');
                $email->setTransport($config->profiles->{$email->getName()}->transport);
                try {
                    // If the 'send' checkbox is ticked mark the email for sending
                    if (isset($values['queue']) && $values['queue']) {
                        $email->setStatus(Mailqueue_Model_Mail::STATUS_PENDING);
                        $this->view->getHelper('DisplayMessages')->addMessage('emailQueued', 'info');
                    }

                    $mapper = Mailqueue_Model_Mail::getMapper();
                    $mapper->save($email);

                    $this->view->error = false;
                    $this->view->email = $email;
                    $this->view->getHelper('DisplayMessages')->addMessage('emailCreated', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $email);
                    if (isset($values['preview']) && $values['preview']) {
                        $email->preview();
                        $this->view->getHelper('DisplayMessages')->addMessage('previewSent', 'info');
                    }
                    if (isset($values['test']) && $values['test']) {
                        $email->test();
                        $this->view->getHelper('DisplayMessages')->addMessage('testSent', 'info');
                    }
                    $this->view->previewUser = Zend_Registry::get('authenticated-user');
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidCreateValues', 'warn');
            }
        }
    }

    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            if (null !== ($mailqueue = $mapper->find($id))) {
                $this->view->email = $mailqueue;
            } else {
                throw new Mcmr_Exception_PageNotFound('Email not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            if (null !== ($email = $mapper->find($id))) {
                $form = $this->_helper->loadForm('Mail', array('type'=>$email->getName()));
                $form->populate($email->getOptions(true));

                $this->view->email = $email;
                $this->view->form = $form;

                if ($this->_request->isPost()) {
                    // Merge the existing company information to allow partial post updates
                    if ($form->isValid($this->_request->getPost())) {
                        $values = $form->getValues(true);

                        // Clear the existing html and plain content forcing it to be regenerated
                        $email->setHtml(null);
                        $email->setPlain(null);
                        $email->setOptions($values);

                        try {
                            // If the 'send' checkbox is ticked mark the email for sending
                            if (isset($values['queue']) && $values['queue']
                                    && Mailqueue_Model_Mail::STATUS_UNSENT === $email->getStatus()) {
                                $email->setStatus(Mailqueue_Model_Mail::STATUS_PENDING);
                                $this->view->getHelper('DisplayMessages')->addMessage('emailQueued', 'info');
                            }

                            $mapper->save($email);

                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('updateSuccessful', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $email);
                            if (isset($values['preview']) && $values['preview']) {
                                $email->preview();
                                $this->view->getHelper('DisplayMessages')->addMessage('previewSent', 'info');
                            }
                            if (isset($values['test']) && $values['test']) {
                                $email->test();
                                $this->view->getHelper('DisplayMessages')->addMessage('testSent', 'info');
                            }
                            $this->view->previewUser = Zend_Registry::get('authenticated-user');
                        } catch (Exception $e) {
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->getHelper('DisplayMessages')->addMessage('invalidUpdateValues', 'warn');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Email not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            if (null !== ($email = $mapper->find($id))) {
                $this->view->email = $email;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($email);
                        $this->view->email = null;

                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('emailDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $email);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Email Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function sendAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            if (null !== ($mail = $mapper->find($id))) {
                $this->view->email = $mail;
                if ($this->_request->isPost()) {
                    if (Mailqueue_Model_Mail::STATUS_UNSENT === $mail->getStatus()
                            || Mailqueue_Model_Mail::STATUS_FAILURE === $mail->getStatus()) {
                        try {
                            $mail->setStatus(Mailqueue_Model_Mail::STATUS_PENDING);
                            $mapper->save($mail);
                            
                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('emailQueued', 'info');
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }
                    } else {
                        $this->view->getHelper('DisplayMessages')
                            ->addMessage('Email already in a queued or sent state', 'warn');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Email not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function previewAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            if (null !== ($mail = $mapper->find($id))) {
                try {
                    $this->view->email = $mail;
                    $mail->test();
                    $this->view->getHelper('DisplayMessages')->addMessage('previewSent', 'info');
                } catch (Exception $e) {
                    $this->view->getHelper('DisplayMessages')
                        ->addMessage('Unable to send preview email: '.$e->getMessage(), 'error');
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Email not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function testAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            if (null !== ($mail = $mapper->find($id))) {
                try {
                    $this->view->email = $mail;
                    $mail->test();
                    $this->view->getHelper('DisplayMessages')->addMessage('testSent', 'info');
                } catch (Exception $e) {
                    $this->view->getHelper('DisplayMessages')
                        ->addMessage('Unable to send test email: '.$e->getMessage(), 'error');
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Email not found');
            }
        } else {
            $this->_forward('index');
        }
    }

    public function registerAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            if (null !== ($mail = $mapper->find($id))) {
                $this->view->email = $mail;
                if ($this->_request->isPost()) {
                    $mail->register();
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Email not found');
            }
        } else {
            $this->_forward('index');
        }
    }
    
    public function callbackAction()
    {
        $request = $this->getRequest();

        if ($this->_request->isPost()) {
            $name = $this->getRequest()->getParam('messageName');
            $email = $this->getRequest()->getParam('email');
            $event = $this->getRequest()->getParam('type');
            if (null !== $name) {
                // Extract the ID from the message name
                $parts = explode('-', $name);
                $id = array_pop($parts);

                if (is_numeric($id)) {
                    $mapper = Mailqueue_Model_Mail::getMapper();
                    $mail = $mapper->find($id);

                    if (null !== $mail) {
                        // Record the event with the mail
                        $mail->event($event, $email);
                    }
                } else {
                    throw new Mcmr_Exception_PageNotFound("Mailqueue callback id '{$id}' not found");
                }
            } else {
//                throw new Zend_Exception("Mailqueue callback name is not set");
            }
        }
    }
    
    /**
     * Test action to show data output for a mail
     **/
    public function datatestAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Mailqueue_Model_Mail::getMapper();
            if (null !== ($mail = $mapper->find($id))) {
                try {

					$site=Zend_Registry::get('app-config')->sitename;
					$subsite = 'default';
					$type = $mail->getName();
		
					// Change the timezone so that dates are displayed correctly.
					$origTimezone = Mcmr_Date::getDefaultTimezone();
					if (isset(Zend_Registry::get('mailqueue-config')->adestra->{$type}->timezone)) {
						$timezone = Mcmr_Date::setDefaultTimezone(
							Zend_Registry::get('mailqueue-config')->adestra->{$type}->timezone
						); // Change timezone for the email
					}
		
					$parts = explode('-', $type);
					if (2 === count($parts)) {
						list($subsite, $type) = $parts;
					}

					$emailData = Intent_EmailData::getInstance($mail, $type, $subsite, $site);
		
					$data = $emailData->adestraData();

					var_dump($data);
					exit;

                } catch (Exception $e) {
                    die($e->getMessage());
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Email not found');
            }
        } else {
            $this->_forward('index');
        }
    }

	/**
	 * These values were introduced for Licensing.biz
	 * Config should be used to override these defaults for other sites
	 **/
	protected $_sectors = array(
		"Apparel" => "Apparel",
		"Art & Design" => "Art & Design",
		"Distributor" => "Distributor",
		"Entertainment" => "Entertainment",
		"Financial/Legal" => "Financial/Legal",
		"Interactive" => "Interactive",
		"Licensor" => "Licensor",
		"Manufacturer" => "Manufacturer",
		"Media" => "Media",
		"Media Buying/Advertising" => "Media Buying/Advertising",
		"PR & Marketing" => "PR & Marketing",
		"Publishing" => "Publishing",
		"Recruitment" => "Recruitment",
		"Research/Analyst" => "Research/Analyst",
		"Retail" => "Retail",
		"Sports" => "Sports",
		"Toys & Gifts" => "Toys & Gifts",
		"Enthusiast" => "Enthusiast",
	);

	/**
	 * These values were introduced for Licensing.biz
	 * Config should be used to override these defaults for other sites
	 **/
	protected $_functions = array(
		"Administration/Secretarial" => "Administration/Secretarial",
		"Audio, Video Equipment Operator" => "Audio, Video Equipment Operator",
		"CEO/Managing Director/Owner" => "CEO/Managing Director/Owner",
		"Consultant/Freelancer" => "Consultant/Freelancer",
		"Engineer: Installation, Production" => "Engineer: Installation, Production",
		"Engineer: Research, Development" => "Engineer: Research, Development",
		"Finance/Accounts/Purchasing" => "Finance/Accounts/Purchasing",
		"Graphics/Designers" => "Graphics/Designers",
		"IT/Network systems" => "IT/Network systems",
		"Lecturer/Education/Training" => "Lecturer/Education/Training",
		"Legal/Business Manager" => "Legal/Business Manager",
		"Logistics/Maintenance" => "Logistics/Maintenance",
		"Manager: General" => "Manager: General",
		"Manager: Project, Production, Operations" => "Manager: Project, Production, Operations",
		"Manager: Sales, Account Exec" => "Manager: Sales, Account Exec",
		"Manager: Senior, Board" => "Manager: Senior, Board",
		"Marketing/PR" => "Marketing/PR",
		"New Media/Online/Web" => "New Media/Online/Web",
		"Performer/Presenter/Creative/Producer" => "Performer/Presenter/Creative/Producer",
		"Student/Trainee" => "Student/Trainee",
		"System Designer/Software Developer" => "System Designer/Software Developer",
		"Other" => "Other",
	);
    
	/**
	 * These values were introduced for Licensing.biz
	 * Config should be used to override these defaults for other sites
	 **/
    protected $_countries = array(
		"United Kingdom" => "United Kingdom",
		"Afghanistan" => "Afghanistan",
		"Aland Islands" => "Aland Islands",
		"Albania" => "Albania",
		"Algeria" => "Algeria",
		"American Samoa" => "American Samoa",
		"Andorra" => "Andorra",
		"Angola" => "Angola",
		"Anguilla" => "Anguilla",
		"Antarctica" => "Antarctica",
		"Antigua and Barbuda" => "Antigua and Barbuda",
		"Argentina" => "Argentina",
		"Armenia" => "Armenia",
		"Aruba" => "Aruba",
		"Australia" => "Australia",
		"Austria" => "Austria",
		"Azerbaijan" => "Azerbaijan",
		"Bahrain" => "Bahrain",
		"Bangladesh" => "Bangladesh",
		"Barbados" => "Barbados",
		"Belarus" => "Belarus",
		"Belgium" => "Belgium",
		"Belize" => "Belize",
		"Benin" => "Benin",
		"Bermuda" => "Bermuda",
		"Bhutan" => "Bhutan",
		"Bolivia" => "Bolivia",
		"Bosnia and Herzegovina" => "Bosnia and Herzegovina",
		"Botswana" => "Botswana",
		"Bouvet Island" => "Bouvet Island",
		"Brazil" => "Brazil",
		"British Indian Ocean Territory" => "British Indian Ocean Territory",
		"British Virgin Islands" => "British Virgin Islands",
		"Brunei" => "Brunei",
		"Bulgaria" => "Bulgaria",
		"Burkina Faso" => "Burkina Faso",
		"Burundi" => "Burundi",
		"Cambodia" => "Cambodia",
		"Cameroon" => "Cameroon",
		"Canada" => "Canada",
		"Cape Verde" => "Cape Verde",
		"Caribbean Netherlands" => "Caribbean Netherlands",
		"Cayman Islands" => "Cayman Islands",
		"Central African Republic" => "Central African Republic",
		"Chad" => "Chad",
		"Chile" => "Chile",
		"China" => "China",
		"Christmas Island" => "Christmas Island",
		"Cocos (Keeling) Islands" => "Cocos (Keeling) Islands",
		"Colombia" => "Colombia",
		"Comoros" => "Comoros",
		"Congo" => "Congo",
		"Cook Islands" => "Cook Islands",
		"Costa Rica" => "Costa Rica",
		"Croatia" => "Croatia",
		"Cuba" => "Cuba",
		"Curacao" => "Curacao",
		"Cyprus" => "Cyprus",
		"Czech Republic" => "Czech Republic",
		"Democratic Republic of Congo" => "Democratic Republic of Congo",
		"Denmark" => "Denmark",
		"Disputed Territory" => "Disputed Territory",
		"Djibouti" => "Djibouti",
		"Dominica" => "Dominica",
		"Dominican Republic" => "Dominican Republic",
		"East Timor" => "East Timor",
		"Ecuador" => "Ecuador",
		"Egypt" => "Egypt",
		"El Salvador" => "El Salvador",
		"Equatorial Guinea" => "Equatorial Guinea",
		"Eritrea" => "Eritrea",
		"Estonia" => "Estonia",
		"Ethiopia" => "Ethiopia",
		"Falkland Islands" => "Falkland Islands",
		"Faroe Islands" => "Faroe Islands",
		"Federated States of Micronesia" => "Federated States of Micronesia",
		"Fiji" => "Fiji",
		"Finland" => "Finland",
		"France" => "France",
		"French Guiana" => "French Guiana",
		"French Polynesia" => "French Polynesia",
		"French Southern Territories" => "French Southern Territories",
		"Gabon" => "Gabon",
		"Georgia" => "Georgia",
		"Germany" => "Germany",
		"Ghana" => "Ghana",
		"Gibraltar" => "Gibraltar",
		"Greece" => "Greece",
		"Greenland" => "Greenland",
		"Grenada" => "Grenada",
		"Guadeloupe" => "Guadeloupe",
		"Guam" => "Guam",
		"Guatemala" => "Guatemala",
		"Guinea" => "Guinea",
		"Guinea-Bissau" => "Guinea-Bissau",
		"Guyana" => "Guyana",
		"Haiti" => "Haiti",
		"Heard Island and McDonald Islands" => "Heard Island and McDonald Islands",
		"Honduras" => "Honduras",
		"Hong Kong" => "Hong Kong",
		"Hungary" => "Hungary",
		"Iceland" => "Iceland",
		"India" => "India",
		"Indonesia" => "Indonesia",
		"Iran" => "Iran",
		"Iraq" => "Iraq",
		"Iraq-Saudi Arabia Neutral Zone" => "Iraq-Saudi Arabia Neutral Zone",
		"Ireland" => "Ireland",
		"Israel" => "Israel",
		"Italy" => "Italy",
		"Ivory Coast" => "Ivory Coast",
		"Jamaica" => "Jamaica",
		"Japan" => "Japan",
		"Jordan" => "Jordan",
		"Kazakhstan" => "Kazakhstan",
		"Kenya" => "Kenya",
		"Kiribati" => "Kiribati",
		"Kuwait" => "Kuwait",
		"Kyrgyzstan" => "Kyrgyzstan",
		"Laos" => "Laos",
		"Latvia" => "Latvia",
		"Lebanon" => "Lebanon",
		"Lesotho" => "Lesotho",
		"Liberia" => "Liberia",
		"Libya" => "Libya",
		"Liechtenstein" => "Liechtenstein",
		"Lithuania" => "Lithuania",
		"Luxembourg" => "Luxembourg",
		"Macau" => "Macau",
		"Macedonia" => "Macedonia",
		"Madagascar" => "Madagascar",
		"Malawi" => "Malawi",
		"Malaysia" => "Malaysia",
		"Maldives" => "Maldives",
		"Mali" => "Mali",
		"Malta" => "Malta",
		"Marshall Islands" => "Marshall Islands",
		"Martinique" => "Martinique",
		"Mauritania" => "Mauritania",
		"Mauritius" => "Mauritius",
		"Mayotte" => "Mayotte",
		"Mexico" => "Mexico",
		"Moldova" => "Moldova",
		"Monaco" => "Monaco",
		"Mongolia" => "Mongolia",
		"Montenegro" => "Montenegro",
		"Montserrat" => "Montserrat",
		"Morocco" => "Morocco",
		"Mozambique" => "Mozambique",
		"Myanmar" => "Myanmar",
		"Namibia" => "Namibia",
		"Nauru" => "Nauru",
		"Nepal" => "Nepal",
		"Netherlands" => "Netherlands",
		"New Caledonia" => "New Caledonia",
		"New Zealand" => "New Zealand",
		"Nicaragua" => "Nicaragua",
		"Niger" => "Niger",
		"Nigeria" => "Nigeria",
		"Niue" => "Niue",
		"Norfolk Island" => "Norfolk Island",
		"North Korea" => "North Korea",
		"Northern Mariana Islands" => "Northern Mariana Islands",
		"Norway" => "Norway",
		"Oman" => "Oman",
		"Pakistan" => "Pakistan",
		"Palau" => "Palau",
		"Palestine" => "Palestine",
		"Panama" => "Panama",
		"Papua New Guinea" => "Papua New Guinea",
		"Paraguay" => "Paraguay",
		"Peru" => "Peru",
		"Philippines" => "Philippines",
		"Pitcairn Islands" => "Pitcairn Islands",
		"Poland" => "Poland",
		"Portugal" => "Portugal",
		"Puerto Rico" => "Puerto Rico",
		"Qatar" => "Qatar",
		"Reunion" => "Reunion",
		"Romania" => "Romania",
		"Russia" => "Russia",
		"Rwanda" => "Rwanda",
		"Saint Barthelemy" => "Saint Barthelemy",
		"Saint Kitts and Nevis" => "Saint Kitts and Nevis",
		"Saint Pierre and Miquelon" => "Saint Pierre and Miquelon",
		"Saint Vincent and the Grenadines" => "Saint Vincent and the Grenadines",
		"Saint-Martin" => "Saint-Martin",
		"Samoa" => "Samoa",
		"San Marino" => "San Marino",
		"Sao Tome and Principe" => "Sao Tome and Principe",
		"Saudi Arabia" => "Saudi Arabia",
		"Senegal" => "Senegal",
		"Serbia" => "Serbia",
		"Seychelles" => "Seychelles",
		"Sierra Leone" => "Sierra Leone",
		"Singapore" => "Singapore",
		"Sint Maarten" => "Sint Maarten",
		"Slovakia" => "Slovakia",
		"Slovenia" => "Slovenia",
		"Solomon Islands" => "Solomon Islands",
		"Somalia" => "Somalia",
		"South Africa" => "South Africa",
		"South Georgia and the South Sandwich Islands" => "South Georgia and the South Sandwich Islands",
		"South Korea" => "South Korea",
		"Spain" => "Spain",
		"Spratly Islands" => "Spratly Islands",
		"Sri Lanka" => "Sri Lanka",
		"St Helena Ascension and Tristan da Cunha" => "St Helena Ascension and Tristan da Cunha",
		"St. Lucia" => "St. Lucia",
		"Sudan" => "Sudan",
		"Suriname" => "Suriname",
		"Svalbard and Jan Mayen" => "Svalbard and Jan Mayen",
		"Swaziland" => "Swaziland",
		"Sweden" => "Sweden",
		"Switzerland" => "Switzerland",
		"Syria" => "Syria",
		"Taiwan" => "Taiwan",
		"Tajikistan" => "Tajikistan",
		"Tanzania" => "Tanzania",
		"Thailand" => "Thailand",
		"The Bahamas" => "The Bahamas",
		"The Gambia" => "The Gambia",
		"The Republic of South Sudan" => "The Republic of South Sudan",
		"Togo" => "Togo",
		"Tokelau" => "Tokelau",
		"Tonga" => "Tonga",
		"Trinidad and Tobago" => "Trinidad and Tobago",
		"Tunisia" => "Tunisia",
		"Turkey" => "Turkey",
		"Turkmenistan" => "Turkmenistan",
		"Turks and Caicos Islands" => "Turks and Caicos Islands",
		"Tuvalu" => "Tuvalu",
		"US Virgin Islands" => "US Virgin Islands",
		"Uganda" => "Uganda",
		"Ukraine" => "Ukraine",
		"United Arab Emirates" => "United Arab Emirates",
		"United Nations Neutral Zone" => "United Nations Neutral Zone",
		"United States" => "USA",
		"United States Minor Outlying Islands" => "United States Minor Outlying Islands",
		"Uruguay" => "Uruguay",
		"Uzbekistan" => "Uzbekistan",
		"Vanuatu" => "Vanuatu",
		"Vatican City" => "Vatican City",
		"Venezuela" => "Venezuela",
		"Vietnam" => "Vietnam",
		"Wallis and Futuna" => "Wallis and Futuna",
		"Western Sahara" => "Western Sahara",
		"Yemen" => "Yemen",
		"Zambia" => "Zambia",
		"Zimbabwe" => "Zimbabwe",
	);

}
