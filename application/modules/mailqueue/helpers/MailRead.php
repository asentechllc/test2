<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mailqueue
 * @package Mailqueue_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View Helper for retrieving a single email
 *
 * @category Mailqueue
 * @package Mailqueue_View
 * @subpackage Helper
 */
class Mailqueue_View_Helper_MailRead extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve a single email based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function mailRead($conditions = array(), $order=null, $partial = null)
    {
        $mapper = Mailqueue_Model_Mail::getMapper();
        $email = $mapper->findOneByField($conditions, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('mail'=>$email));
                }
            }

            return $this->view->partial($partial, array('mail'=>$email));
        }

        return $email;
    }
}
