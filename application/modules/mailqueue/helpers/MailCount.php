<?php

class Mailqueue_View_Helper_MailCount
{
    public function mailCount($condition = null)
    {
        $mapper = Mailqueue_Model_Mail::getMapper();

        return $mapper->count($condition);;
    }
}
