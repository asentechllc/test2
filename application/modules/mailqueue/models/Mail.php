<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mailqueue
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all Queue emails
 *
 * @category   Mailqueue
 * @package    Model
 * @subpackage Mail
 */
class Mailqueue_Model_Mail extends Mcmr_Model_ModelAbstract implements Mcmr_Mail_Queue_StoreInterface
{
    static protected $_mapperclass = 'Mailqueue_Model_MailMapper';

    protected $_id = null;
    protected $_transport = null;
    protected $_name = null;
    protected $_subject = null;
    protected $_plain = null;
    protected $_html = null;
    protected $_status = null;
    protected $_sendtime = null;
    protected $_optin = null;
    protected $_messageid = null;
    protected $_listid = null;
    protected $_deliveryid = null;
    protected $_processstart = null;

    private $_recipients = null;

    const STATUS_QUEUED = 'queued';
    const STATUS_REGISTERED = 'registered';
    const STATUS_UNSENT = 'unsent';
    const STATUS_SENT = 'sent';
    const STATUS_FAILURE = 'failure';
    const STATUS_PENDING = 'pending';
    
    /**
     * Return mapper for model
     *
     * @return Mailqueue_Model_MailMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the email ID
     *
     * @return int 
     */
    public function getId() 
    {
        return $this->_id;
    }

    /**
     * Set the email ID
     *
     * @param int $id
     * @return Mailqueue_Model_Mail 
     */
    public function setId($id)
    {
        $this->_id = $id;

        foreach ($this->getRecipients() as $recipient) {
            $recipient->setMailid($id);
        }
        
        return $this;
    }

    /**
     * Get the email transport
     *
     * @return string 
     */
    public function getTransport()
    {
        return $this->_transport;
    }

    /**
     * Set the email transport
     *
     * @param string $transport
     * @return Mailqueue_Model_Mail 
     */
    public function setTransport($transport)
    {
        $this->_transport = $transport;

        return $this;
    }

    /**
     * Get the email name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Set the email name
     *
     * @param string $name
     * @return Mailqueue_Model_Mail 
     */
    public function setName($name)
    {
        $this->_name = $name;

        return $this;
    }

    /**
     * Get the email subject
     *
     * @return string 
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    /**
     * Set the email subject
     *
     * @param string $subject
     * @return Mailqueue_Model_Mail 
     */
    public function setSubject($subject)
    {
        $this->_subject = $subject;

        return $this;
    }

    /**
     * Get the plain text version of this email
     *
     * @return string 
     */
    public function getPlain()
    {
        if (empty($this->_plain)) {
            $config = Zend_Registry::get('mailqueue-config');
            $name = $this->getName();
            if (isset($config->profiles->$name->template)) {
                $this->_plain = $this->_generateBody($config->profiles->$name->template->plain);
            }
        }

        return $this->_plain;
    }

    /**
     * Set the plain text version of the email
     *
     * @param string $plain
     * @return Mailqueue_Model_Mail 
     */
    public function setPlain($plain)
    {
        $this->_plain = $plain;

        return $this;
    }

    /**
     * Get the HTML version of the email
     *
     * @return string 
     */
    public function getHtml()
    {
        if (empty($this->_html)) {
            $config = Zend_Registry::get('mailqueue-config');
            $name = $this->getName();
            if (isset($config->profiles->$name->template)) {
                $this->_html = $this->_generateBody($config->profiles->$name->template->html);
            }
        }

        return $this->_html;
    }

    /**
     * Set the HTML version of the email
     *
     * @param string $html
     * @return Mailqueue_Model_Mail 
     */
    public function setHtml($html)
    {
        $this->_html = $html;

        return $this;
    }

    /**
     * Get the send status of this email
     *
     * @return string 
     */
    public function getStatus()
    {
        if (null === $this->_status) {
            $this->_status = self::STATUS_UNSENT;
        }
        
        return $this->_status;
    }

    /**
     * Set the send status of this email
     *
     * @param string $status
     * @return Mailqueue_Model_Mail 
     */
    public function setStatus($status)
    {
        $this->_status = $status;

        return $this;
    }

    /**
     * Get the time this email is to be sent. Time is a unix timestamp. If it is
     * not set them it will default to 'now'.
     *
     * @return int 
     */
    public function getSendtime()
    {
        if (null === $this->_sendtime) {
            $this->_sendtime = time();
        }

        return $this->_sendtime;
    }

    /**
     * Set the send time as a unix timestamp
     *
     * @param int $sendtime
     * @return Mailqueue_Model_Mail 
     */
    public function setSendtime($sendtime)
    {
        $this->_sendtime = (int)$sendtime;

        return $this;
    }

    /**
     * Get the user's attribute that is used as the optin flag
     *
     * @return string 
     */
    public function getOptin()
    {
        if (null === $this->_optin) {
            $this->_optin = 'email'.$this->getName();
        }

        return $this->_optin;
    }

    /**
     * Set the user's attribute field that will be used as the optin
     *
     * @param string $optin
     * @return Mailqueue_Model_Mail 
     */
    public function setOptin($optin)
    {
        $this->_optin = $optin;

        return $this;
    }

    /**
     * Get the email queue message ID
     *
     * @return string
     */
    public function getMessageid()
    {
        return $this->_messageid;
    }

    /**
     * Set the email queue message ID
     *
     * @param string $messageid
     * @return Mailqueue_Model_Mail 
     */
    public function setMessageid($messageid)
    {
        $this->_messageid = $messageid;

        return $this;
    }

    /**
     * Get the email recipient list queue ID
     *
     * @return string 
     */
    public function getListid()
    {
        return $this->_listid;
    }

    /**
     * Set the recipient list queue ID
     *
     * @param string $listid
     * @return Mailqueue_Model_Mail 
     */
    public function setListid($listid)
    {
        $this->_listid = $listid;

        return $this;
    }

    /**
     * Get the queue delivery ID
     *
     * @return string 
     */
    public function getDeliveryid()
    {
        return $this->_deliveryid;
    }

    /**
     * Set the queue delivery ID
     *
     * @param string $deliveryid
     * @return Mailqueue_Model_Mail 
     */
    public function setDeliveryid($deliveryid)
    {
        $this->_deliveryid = $deliveryid;

        return $this;
    }

    /**
     * Set the recipients for this email
     *
     * @param array|Zend_Paginator $recipients
     * @return Mailqueue_Model_Mail 
     */
    public function setRecipients($recipients)
    {
        $this->_recipients = $recipients;

        return $this;
    }

    /**
     * Get the recipients for this email
     *
     * @return array|Zend_Paginator 
     */
    public function getRecipients()
    {
        if (null === $this->_recipients) {
            $this->_recipients = array();
        }

        return $this->_recipients;
    }

    public function getProcessstart()
    {
        return $this->_processstart;
    }

    public function setProcessstart($processstart=null)
    {
        if (null === $processstart) {
            $this->_processstart = time();
        } else {
            $this->_processstart = (int)$processstart;
        }
        
        return $this;
    }

    /**
     * Add a recipient to the email
     *
     * @param Mailqueue_Model_Recipient $recipient 
     * @return Mailqueue_Model_Mail 
     */
    public function addRecipient(Mailqueue_Model_Recipient $recipient)
    {
        if (null === $this->_recipients) {
            $this->_recipients = array();
        }
        $this->_recipients[] = $recipient;
        
        return $this;
    }

    /**
     * Using the optin field the recipient list will be built from the users that
     * have that optin selected
     *
     * @return Mailqueue_Model_Mail 
     */
    public function addRecipientsFromUsers()
    {
        $this->clearRecipients();
        
        $validator = new Zend_Validate_EmailAddress();
        $log = new Zend_Log(new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/error.log'));
        $optin = $this->getOptin();

        $mapper = User_Model_User::getMapper();
        $users = $mapper->findAllByField(
            array('emailconfirmed'=>1, 'attr_'.$optin=>'1'), null, array('page'=>1,'count'=>10000000000)
        );
        foreach ($users as $user) {
            $recipient = new Mailqueue_Model_Recipient();
            $email = $user->getEmail();
            if ($validator->isValid($email)) {
                $recipient->setEmail($email);
                $recipient->setName($user->getFirstname().' '.$user->getSurname());
                $recipient->setMailid($this->getId());
                $this->addRecipient($recipient);
            } else {
                $log->log($email,Zend_Log::ERR);
            }
        }

        return $this;
    }

    /**
     * Clear all recipients for this email
     * 
     * @return Mailqueue_Model_Mail
     */
    public function clearRecipients()
    {
        $this->_recipients = array();

        return $this;
    }

    /**
     * Register this email with the transport queue
     *
     * @return Mailqueue_Model_Mail
     */
    public function register()
    {
        $transport = $this->_buildTransport();
        $mail = $this->_buildMail();
        $mail->register($transport, $this);
        
        $this->setStatus(self::STATUS_REGISTERED);

        return $this;
    }

    /**
     * Queue this email for sending with the transport
     *
     * @return Mailqueue_Model_Mail
     */
    public function send()
    {
        $transport = $this->_buildTransport();
        $mail = $this->_buildMail();
        $mail->send($transport, $this);

        $this->setStatus(self::STATUS_QUEUED);

        return $this;
    }

    /**
     * Send a preview email to the user. If a user is not provided the authenticated user is used
     *
     * @param User_Model_User $user
     * @return Mailqueue_Model_Mail
     */
    public function preview($user = null)
    {
        if (null === $user) {
            $user = Zend_Registry::get('authenticated-user');
        }
        
        if (null !== $user) {
            $mail = new Mcmr_Mail('utf-8');
            $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
            $mail->setBodyText($this->getPlain());
            $mail->setBodyHtml($this->getHtml());
            $mail->setSubject($this->getSubject());
            $mail->addTo($user->getEmail());
            $mail->send();
        } else {
            throw new Mcmr_Model_Exception('Unable to send preview. $user is not set');
        }

        return $this;
    }

    /**
     * Sends the email to a test address
     *
     * @return Mailqueue_Model_Mail
     */
    public function test()
    {
        $config = Zend_Registry::get('mailqueue-config');
        if (isset($config->test->to)) {
            $to = $config->test->to->toArray();
            
            $mail = new Mcmr_Mail('utf-8');
            $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
            $mail->setBodyText($this->getPlain());
            $mail->setBodyHtml($this->getHtml());
            $mail->setSubject($this->getSubject());
            $mail->addTo($to);
            $mail->send();
        }

        return $this;
    }

    /**
     * Record an event against this email. Usually triggered by a transport callback
     *
     * @param string $name
     * @param string $email
     * @return Mailqueue_Model_Mail 
     */
    public function event($name, $email)
    {
        $name = strtolower($name);
        switch ($name) {
            case 'optout':
                $this->eventOptout($email);
                break;

            case 'optin':
                $this->eventOptin($email);
                break;

            case 'blocked':
            case 'bounce':
                $this->eventBounce($email);
                break;

            case 'delivery':
                $this->eventDelivery();
                break;

            case 'click':
            case 'open':
                break;

            default:
                throw new Mcmr_Model_Exception("Unknown mailqueue event '{$name}'");
                break;
        }

        return $this;
    }

    /**
     * Record an optout event for the user
     *
     * @param string $email
     * @return Mailqueue_Model_Mail
     */
    public function eventOptout($email)
    {
        if (empty($email)) {
            throw new Mcmr_Model_Exception("Cannot optout an empty email address");
        }

        $mapper = User_Model_User::getMapper();
        $user = $mapper->findByUsername($email);

        if (null !== $user) {
            // Opt out the user for this mailing list
            $user->setAttribute($this->getOptin(), false);
            $mapper->save($user);
        } else {
            throw new Mcmr_Model_Exception("Optout address '{$email}'. User not found");
        }

        return $this;
    }

    /**
     * Record an optin event for the user
     *
     * @param string $email
     * @return Mailqueue_Model_Mail
     */
    public function eventOptin($email)
    {
        if (empty($email)) {
            throw new Mcmr_Model_Exception("Cannot optin an empty email address");
        }

        $mapper = User_Model_User::getMapper();
        $user = $mapper->findByUsername($email);

        if (null !== $user) {
            // Optin the user for this mailing list
            $user->setAttribute($this->getOptin(), true);
            $mapper->save($user);
        } else {
            throw new Mcmr_Model_Exception("Optin address '{$email}'. User not found");
        }

        return $this;
    }

    /**
     * Record a bounce event for the user
     *
     * @param string $email
     * @return Mailqueue_Model_Mail
     */
    public function eventBounce($email)
    {
        if (empty($email)) {
            throw new Mcmr_Model_Exception("Cannot record bounce for empty email address");
        }
        
        $mapper = User_Model_User::getMapper();
        $user = $mapper->findByUsername($email);

        if (null !== $user) {
            // Opt out the user for this email address
            $user->setAttribute($this->getOptin(), false);
            $mapper->save($user);
        } else {
            throw new Mcmr_Model_Exception("Bounce address '{$email}'. User not found");
        }
        
        return $this;
    }

    /**
     * Record a delivery event for this email
     *
     * @return Mailqueue_Model_Mail
     */
    public function eventDelivery()
    {
        return $this;
    }

    /**
     * Build the email queue
     *
     * @return Mcmr_Mail_Queue 
     */
    private function _buildMail()
    {
        $config = Zend_Registry::get('mailqueue-config');
        $name = $this->getName();
        if (!isset($config->profiles->$name)) {
            throw new Mcmr_Model_Exception("Cannot send mail. Config for queue '{$name}' is not set");
        }

        $prefix = '';
        if (isset($config->profiles->$name->options->namePrefix)) {
            $prefix = $config->profiles->$name->options->namePrefix.'-';
        }
        
        $mail = new Mcmr_Mail_Queue();
        $mail->setName($prefix.$this->getName().'-'.$this->getId());
        $mail->setSendTime($this->getSendtime());
        $mail->setSubject($this->getSubject());
        $mail->setBodyText($this->getPlain());
        $mail->setBodyHtml($this->getHtml());
        foreach ($this->getRecipients() as $recipient) {
            $mail->addTo($recipient->getEmail(), $recipient->getName());
        }
        
        return $mail;
    }
    
    /**
     * Build the email transport
     *
     * @return Mcmr_Mail_Queue_Transport_Pure360 
     */
    private function _buildTransport()
    {
        $config = Zend_Registry::get('mailqueue-config');
        $name = $this->getName();
        if (!isset($config->profiles->$name)) {
            throw new Mcmr_Model_Exception("Cannot send mail. Config for queue '{$name}' is not set");
        }
        
        $transport = new Mcmr_Mail_Queue_Transport_Pure360($config->profiles->$name->options);
        
        return $transport;
    }

    /**
     * Generate the email body text from the template
     *
     * @param string $template
     * @return string 
     */
    private function _generateBody($template)
    {
        $frontView = Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource('view');

        $view = clone $frontView;
        $view->mail = $this;

        return $view->render($template);
    }
}
