<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mailqueue
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all mail models
 *
 * @category   Mailqueue
 * @package    Model
 * @subpackage MailMapper
 */
class Mailqueue_Model_MailMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Mailqueue_Model_DbTable_Mail';
    protected $_dbAttrTableClass = 'Mailqueue_Model_DbTable_MailAttr';
    protected $_columnPrefix = 'mail_';
    protected $_modelClass = 'Mailqueue_Model_Mail';
    protected $_cacheIdPrefix = 'Mailqueue';

    public function init()
    {
        $this->addObserverNamespace(array('prefix' => 'Mailqueue_Model_Observer_', 'path'=>dirname(__FILE__) . '/Observer'));
    }
    
    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        $options = $model->getOptions();
        
        // Do not save the email content if the email has not been queued yet.
        $status = array('pending', 'registered', 'queued', 'sent');
        if (!in_array($model->getStatus(), $status)) {
            unset($options['html']);
            unset($options['plain']);
        }
        
        $data = array();
        foreach ($options as $field=>$value) {
            $field = $this->_sanitiseField($field);
            $value = $this->_sanitiseValue($field, $value);
            if (null !== $value) {
                $data[$field] = $value;
            }
        }
        
        if (null === ($id = $model->getId())) {
            // Perform an insert
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);

            $this->_saveAttributes($model);
            $this->_saveRecipients($model->getRecipients());
            
            // Notify Observers
            $this->notify('insert', $model);
        } else {
            // Perform an update
            $this->getDbTable()->update($data, array($this->_columnPrefix.'id=?'=>$id));

            $this->_deleteAttributes(array($this->_columnPrefix.'id=?'=>$id));
            $this->_saveAttributes($model);
            $this->_deleteRecipients($model);
            $this->_saveRecipients($model->getRecipients());

            // Notify observers
            $this->notify('update', $model);
        }
        
        return $this;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseValue()
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'mail_sendtime':
            case 'mail_processstart':
                $value = new Zend_Db_Expr("FROM_UNIXTIME('{$value}')");
                break;
        }

        return $value;
    }
    
    /**
     * Create a list of fields to be read form the databse in SELECT query.
     *
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(mail_sendtime) AS mail_sendtime";
        $fields[] = "UNIX_TIMESTAMP(mail_processstart) AS mail_processstart";
        
        return $fields;
    }

    /**
     * Converts a row returned by database query to an instance of model class.
     * @param Zend_Db_Table_Row_Abstract $row
     * @param instance of the model class
     */
    protected function _convertRowToModel( $row )
    {
        $model = parent::_convertRowToModel($row);

        return $model;
    }

    /**
     * Save the email recipients
     *
     * @param type $recipients 
     */
    protected function _saveRecipients($recipients)
    {
        $mapper = Mailqueue_Model_Recipient::getMapper();
        foreach ($recipients as $recipient) {
            $mapper->save($recipient);
        }
    }

    /**
     * Load the email recipients
     *
     * @param Mailqueue_Model_Mail $model
     * @return Mailqueue_Model_Mail 
     */
    protected function _loadRecipients($model)
    {
        $mapper = Mailqueue_Model_Recipient::getMapper();
        $recipients = $mapper->findAllByField(
            array('mailid'=>$model->getId()), null, array('page'=>1, 'count'=>1000000)
        );
        
        $model->setRecipients($recipients->getCurrentItems());
        
        return $model;
    }

    /**
     * Delete the email recipients
     *
     * @param Mailqueue_Model_Mail $model
     * @return Mailqueue_Model_MailMapper 
     */
    protected function _deleteRecipients($model)
    {
        $condition = array('mail_id=?'=>$model->getId());
        $table = new Mcmr_Db_Table('mailqueue_recipient');
        $table->delete($condition);

        return $this;
    }
}
