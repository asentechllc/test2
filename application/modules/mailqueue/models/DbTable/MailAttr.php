<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of MailAttr
 *
 * @category News
 * @package News_Model
 * @subpackage DbTable
 */
class Mailqueue_Model_DbTable_MailAttr extends Mcmr_Db_Table_Abstract implements Mcmr_Model_AttrInterface
{
    protected $_name = 'mailqueue_mail_attr';
    protected $_primary = array('mail_id', 'attr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insert($this->_rowData);
        } else {
            parent::insert($data);
        }
    }

    public function setAttrName($name)
    {
        $this->_rowData['attr_name'] = $name;

        return $this;
    }

    public function setAttrValue($value)
    {
        $this->_rowData['attr_value'] = $value;

        return $this;
    }

    public function setAttrId($id)
    {
        $this->_rowData['mail_id'] = $id;

        return $this;
    }

    public function setAttrType($type)
    {
        $this->_rowData['attr_type'] = $type;

        return $this;
    }
}
