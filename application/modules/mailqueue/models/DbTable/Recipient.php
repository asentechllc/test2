<?php

class Mailqueue_Model_DbTable_Recipient extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'mailqueue_recipient';
    protected $_primary = 'recipient_id';
}
