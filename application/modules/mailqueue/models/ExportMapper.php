<?php
class Mailqueue_Model_ExportMapper extends Mcmr_Model_GenericMapper
{
    protected $_modelClass = 'Mailqueue_Model_Export';
    protected $_config = null;
    protected $_directory = null;
    protected $_glob = null;

    public function __construct()
    {
        parent::__construct();
        $this->_config = $this->_getConfig();
        $this->_directory = $this->_config->export->directory;
        $this->_glob = $this->_config->export->glob;
    }

    public function findAllByField($conditions = null, $order=null, $page = null) 
    {
        $allFiles = $this->_listAllFiles();
        $pageNo = $page['page'];
        $count = $page['count'];
        $exports = array();
        foreach($allFiles as $name) {
            $data = array('id'=>$name, 'directory'=>$this->_directory);
            $export = new Mailqueue_Model_Export($data);
            $exports[] = $export;
        }
        $paginator = new Mcmr_Paginator(new Zend_Paginator_Adapter_Array($exports));
        $paginator->setItemCountPerPage($count);
        $paginator->setCurrentPageNumber($pageNo);
        return $paginator;
    }

    public function findOneByField($conditions = null, $order = null)
    {
        $id = isset($conditions['id'])?$conditions['id']:(isset($conditions['url'])?$conditions['url']:null);
        $allFiles = $this->_listAllFiles();
        if (in_array($id, $allFiles)) {
            $data = array('id'=>$id, 'directory'=>$this->_directory);
            $export = new Mailqueue_Model_Export($data);
            return $export;
        }
        return null;
    }

    protected function _listAllFiles() 
    {
        $files = glob($this->_directory.$this->_glob);
        $justNames = array();
        foreach($files as $file) {
            $justNames[] = str_replace($this->_directory, '', $file);
        }
        return $justNames;
    }

    protected function _getConfig()
    {
        if (null===$this->_config) {
            $this->_config = Zend_Registry::get('mailqueue-config');
        }
        return $this->_config;
    }


}
