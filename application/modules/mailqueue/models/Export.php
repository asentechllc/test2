<?php
class Mailqueue_Model_Export extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Mailqueue_Model_ExportMapper';
    
    /**
     * Primary Key
     * 
     * @var int
     */
    protected $_id = null;

    /**
     * The Url key for this type
     *
     * @var string
     */
    protected $_url = null;

    /**
     * Human readable Title
     *
     * @var string
     */
    protected $_title = null;


    protected $_directory = null;

    private $_stat = null;

    /**
     * Return mapper for model
     *
     * @return News_Model_TypeMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the type unique ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the type unique ID
     *
     * @param int $id
     * @return News_Model_Type
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the type string ID
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->getId();
    }

    /**
     * Set the type string ID
     *
     * @param string $url
     * @return News_Model_Type
     */
    public function setUrl($url)
    {
        return $this->setId($url);
    }

    /**
     * Get the type title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getId();
    }

    /**
     * Set the type title
     *
     * @param string $title
     * @return News_Model_Type
     */
    public function setTitle($title)
    {
        return $this->setId($title);
    }


    public function getDirectory()
    {
        return $this->_directory;
    }

    /**
     * Set the type unique ID
     *
     * @param int $directory
     * @return News_Model_Type
     */
    public function setDirectory($directory)
    {
        $this->_directory = $directory;

        return $this;
    }

    public function getStat() 
    {
        if (null===$this->_stat) {
            $this->_stat = stat($this->_directory.$this->_id);
        }
        return $this->_stat;
    }

    public function getCtime()
    {
        $stat= $this->getStat();    
        return $stat['ctime'];
    }

}
