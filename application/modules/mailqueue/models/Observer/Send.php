<?php

class Mailqueue_Model_Observer_Send extends Mcmr_Model_ObserverAbstract
    implements Mcmr_Model_Observer_QueueableInterface
{
    public function update($model)
    {
        if ($model instanceof Mailqueue_Model_Mail) {
            switch ($model->getStatus()) {
                case Mailqueue_Model_Mail::STATUS_PENDING:
                    $this->_processPending($model);
                    break;
            }
        }
    }
    
    protected function _processPending(Mailqueue_Model_Mail $email)
    {
        $mapper = Mailqueue_Model_Mail::getMapper();
        
        // Indicate the email is being processed
        $enable = $mapper->getObserversEnabled();
        $mapper->setObserversEnabled(false);
        $email->setProcessstart();
        $mapper->save($email);
        $mapper->setObserversEnabled($enable);
        
        try {
            echo "Processing\n";
            $email->addRecipientsFromUsers();
            $email->register();

            $mapper->save($email);
            echo "Done\n";
        } catch (Mcmr_Service_Pure360_SearchException $e) {
            $log = new Zend_Log(new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/mailqueue.log'));
            $log->log(
                "Unable to register {$email->getSubject()}. Caught error '{$e->getMessage()}'.\n",
                Zend_Log::ERR
            );
        } catch (Exception $e) {
            $log = new Zend_Log(new Zend_Log_Writer_Stream(SITE_PATH.'/data/log/mailqueue.log'));
            $log->log(
                "Unable to register {$email->getSubject()}. Caught error '{$e->getMessage()}'\n",
                Zend_Log::ERR
            );
            $log->log(print_r($e->getErrors(), true), Zend_Log::ERR);

            $email->setStatus(Mailqueue_Model_Mail::STATUS_FAILURE);
            $email->setAttribute('error', $e->getMessage());
            $mapper->save($email);
        }
    }
    
    protected function _processRegistered($email)
    {
        
    }
}
