<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mailqueue
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all Mail queue recipients
 *
 * @category   Mailqueue
 * @package    Model
 * @subpackage Recipient
 */
class Mailqueue_Model_Recipient extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Mailqueue_Model_RecipientMapper';

    protected $_id = null;
    protected $_mailid = null;
    protected $_name = null;
    protected $_email = null;

    /**
     * Return mapper for model
     *
     * @return News_Model_ArticleMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the recipient's ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the recipient's ID
     *
     * @param int $id
     * @return Mailqueue_Model_Recipient 
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the recipient email ID
     *
     * @return int 
     */
    public function getMailid()
    {
        return $this->_mailid;
    }

    /**
     * Set the recipient email ID
     *
     * @param int $mailid
     * @return Mailqueue_Model_Recipient 
     */
    public function setMailid($mailid)
    {
        $this->_mailid = $mailid;

        return $this;
    }

    /**
     * Get the recipient's name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Set the recipient's name
     *
     * @param string $name
     * @return Mailqueue_Model_Recipient 
     */
    public function setName($name)
    {
        $this->_name = $name;

        return $this;
    }

    /**
     * Get the recipient's email address
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->_email;
    }

    /**
     * Set the recipient's email address.
     * Throws an exception if the supplied address is not a valid email address
     *
     * @param string $email
     * @return Mailqueue_Model_Recipient 
     * @throws Mcmr_Model_Exception
     */
    public function setEmail($email)
    {
        $validator = new Zend_Validate_EmailAddress();
        if ($validator->isValid($email)) {
            $this->_email = $email;
        } else {
            throw new Mcmr_Model_Exception("Invalid email address");
        }

        return $this;
    }


}
