<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Mailqueue
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all email recipient models
 *
 * @category   Mailqueue
 * @package    Model
 * @subpackage RecipientMapper
 */
class Mailqueue_Model_RecipientMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Mailqueue_Model_DbTable_Recipient';
    protected $_columnPrefix = 'recipient_';
    protected $_modelClass = 'Mailqueue_Model_Recipient';
    protected $_cacheIdPrefix = 'MailqueueRecipient';

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseField()
     */
    protected function  _sanitiseField($field)
    {
        switch ($field) {
            case 'mail':
            case 'mailid':
            case 'mail_id':
                return 'mail_id';
                break;

            default:
                return parent::_sanitiseField($field);
        }
    }
}
