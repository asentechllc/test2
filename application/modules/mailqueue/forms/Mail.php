<?php

class Mailqueue_Form_Mail extends Mcmr_Form
{
    public function __construct($options = null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }
        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'mailqueue'
                . DIRECTORY_SEPARATOR . 'mailqueue_form_mail-'.strtolower($type).'.ini';
        }

        parent::__construct($options);
    }

    public function init()
    {
        $this->setName('mailqueueformmail')->setElementsBelongTo('mailqueue-form-mail');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }
}
