<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Changelog
 * @package Changelog_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 1491 2010-10-06 16:40:10Z leigh $
 */

/**
 * Bootstrap
 *
 * @category Changelog
 * @package Changelog_Bootstrap
 */
class Changelog_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');
        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Changelog_View_Helper');
    }
}
