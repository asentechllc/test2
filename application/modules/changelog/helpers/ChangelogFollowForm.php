<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Job
 * @package Job_ApplicationController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 *
 * @category Job
 * @package Job_ApplicationController
 */
class Changelog_View_Helper_ChangelogFollowForm extends Zend_View_Helper_Abstract
{
    private $_form = null;

    public function changelogFollowForm()
    {
        return $this;
    }

    public function form($model = null, $label=null)
    {
        if (null === $label) {
            $label = array('follow' => 'Follow', 'unfollow' => 'Unfollow');
        }

        $form = new Changelog_Form_Follow();
        $form->setAction('/changelog/follow/create');

        if (null !== $model) {
            $form->modeltype->setValue(get_class($model));
            $form->modelid->setValue($model->getId());

            // Check if the user is already following this.
            $conditions = array();
            $conditions['modeltype'] = get_class($model);
            $conditions['modelid'] = $model->getId();
            $user = Zend_Registry::get('authenticated-user');
            if (null === $user || null === $user->getId()) {
                return null;
            }
            $conditions['userid'] = $user->getId();

            $mapper = Changelog_Model_Follow::getMapper();
            if (null !== ($follow = $mapper->findOneByField($conditions))) {
                // Follow exists. Make it an unfollow form.
                $form->setAction('/changelog/follow/delete/id/' . $follow->getId());
                if (null !== $form->getElement('submit')) {
                    $form->getElement('submit')->setLabel($label['unfollow']);
                }
            }
        }

        return $form;
    }

    public function type($model)
    {
        if (null !== $model) {
            // Check if the user is already following this.
            $conditions = array();
            $conditions['modeltype'] = get_class($model);
            $conditions['modelid'] = $model->getId();
            $user = Zend_Registry::get('authenticated-user');
            if (null === $user || null === $user->getId()) {
                return null;
            }
            $conditions['userid'] = $user->getId();

            $mapper = Changelog_Model_Follow::getMapper();
            if (null !== ($follow = $mapper->findOneByField($conditions))) {
                return 'unfollow';
            } else {
                return 'follow';
            }
        }
    }

}
