<?php

class Changelog_View_Helper_ChangelogUpdates
{
    public function changelogUpdates($userid = null)
    {
        if (null === $userid) {
            $user = Zend_Registry::get('authenticated-user');
            if (null === $user) {
                return null;
            } else {
                $userid = $user->getId();
            }
        }

        $mapper = Changelog_Model_Follow::getMapper();
        $updates = $mapper->findAllByField(array('userid'=>$userid, 'updates'=>array('condition'=>'<>', 'value'=>'0')));

        return $updates;
    }
}
