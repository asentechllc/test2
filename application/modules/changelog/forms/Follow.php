<?php
class Changelog_Form_Follow extends Mcmr_Form
{
    public function init()
    {
        $this->setName('changelogformfollow')->setElementsBelongTo('changelog-form-follow');
        $this->setMethod('post');
    }
}
