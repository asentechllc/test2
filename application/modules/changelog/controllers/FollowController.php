<?php
class Changelog_FollowController extends Zend_Controller_Action
{
    public function indexAction()
    {
        
    }
    
    public function createAction()
    {
        $form = $this->_helper->loadForm('Follow');
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $mapper = Changelog_Model_Follow::getMapper();
                $follow = new Changelog_Model_Follow();
                $follow->setOptions($form->getValues(true));

                $conditions = array();
                $conditions['modeltype'] = $follow->getModeltype();
                $conditions['modelid'] = $follow->getModelid();
                $conditions['userid'] = $follow->getUserid();
                if (null === $mapper->findOneByField($conditions)) {
                    try {
                        $mapper->save($follow);
                        $this->view->follow = $follow;

                        $this->view->getHelper('DisplayMessages')->addMessage('Follow Saved', 'info');
                        $this->view->getHelper('Redirect')->notify('create', $follow);
                    } catch (Exception $e) {
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                } else {
                    $this->view->getHelper('DisplayMessages')->addMessage('User Already Following', 'warn');
                }
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'warn');
            }
        }
    }
    
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Changelog_Model_Follow::getMapper();
            if (null !== ($follow = $mapper->find($id))) {
                $this->view->follow = $follow;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($follow);
                        $this->view->follow = null;

                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('Follow Deleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $follow);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('Follow Not Found');
            }
        } else {
            $this->_forward('index');
        }
        
    }

    public function clearAction()
    {
        
    }
}
