<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Changelog
 * @package    Changelog_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 746 2010-06-04 16:43:42Z leigh $
 */

/**
 * Index Controller for the Changelog module
 *
 * @category Changelog
 * @package Changelog_IndexController
 */
class Changelog_IndexController extends Zend_Controller_Action
{
    /**
     * Display an index of Changelog entries
     */
    public function indexAction()
    {
        $countPerPage = Zend_Registry::get('changelog-config')->records->itemCountPerPage;
        $mapper = Changelog_Model_Record::getMapper();
        $page['page'] = $this->_getParam('page', 1);
        $page['count'] = $this->_getParam('count', $countPerPage);
        
        $this->view->records = $mapper->findAllByField(null, array('date'=>'DESC'), $page);
    }

    public function readAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Changelog_Model_Record::getMapper();
            if (null !== ($record = $mapper->find($id))) {
                $this->view->record = $record;
            } else {
                throw new Mcmr_Exception_PageNotFound('Record does not exist');
            }
        } else {
            $this->_forward('index');
        }
    }
}
