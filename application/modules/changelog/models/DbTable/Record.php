<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Changelog
 * @package    Changelog_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Record.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Description of Record
 *
 * @category   Changelog
 * @package    Changelog_Model
 * @subpackage DbTable
 */
class Changelog_Model_DbTable_Record extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'changelog_records';
    protected $_primary = 'record_id';
}
