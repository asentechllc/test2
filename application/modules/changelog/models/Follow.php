<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Changelog
 * @package Changelog_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for Changelog Follows
 *
 * @category Changelog
 * @package Changelog_Model
 */
class Changelog_Model_Follow extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Changelog_Model_FollowMapper';

    protected $_id = null;
    protected $_userid = null;
    protected $_modeltype = null;
    protected $_modelid = null;
    protected $_date = null;
    protected $_updates = null;

    private $_user = null;
    private $_model = null;

    /**
     * Return mapper for model
     *
     * @return Company_Model_CompanyMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the unique ID for this follow model
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the unique ID for this follow model
     *
     * @param int $id
     * @return Changelog_Model_Follow
     */
    public function setId($id)
    {
        $this->_id = (int)$id;

        return $this;
    }

    /**
     * Get the user ID
     *
     * @return int
     */
    public function getUserid()
    {
        if (null === $this->_userid) {
            $user = Zend_Registry::get('authenticated-user');
            if (null !== $user) {
                $this->_userid = $user->getId();
            }
        }

        return $this->_userid;
    }

    /**
     * Set the user ID
     *
     * @param int $userid
     * @return Changelog_Model_Follow
     */
    public function setUserid($userid)
    {
        $this->_user = null;
        $this->_userid = (int)$userid;

        return $this;
    }

    /**
     * Get the classname for the model that is being followed
     *
     * @return string
     */
    public function getModeltype()
    {
        return $this->_modeltype;
    }

    /**
     * Set the classname of the model that is being followed
     *
     * @param string $modeltype
     * @return Changelog_Model_Follow
     */
    public function setModeltype($modeltype)
    {
        $this->_model = null;
        $this->_modeltype = $modeltype;

        return $this;
    }

    /**
     * Get the ID of the model that is being followed
     *
     * @return int
     */
    public function getModelid()
    {
        return $this->_modelid;
    }

    /**
     * Set the ID of the model that is being followed
     *
     * @param int $modelid
     * @return Changelog_Model_Follow
     */
    public function setModelid($modelid)
    {
        $this->_model = null;
        $this->_modelid = (int)$modelid;

        return $this;
    }

    /**
     * Get the data for this model follow
     *
     * @return int
     */
    public function getDate()
    {
        if (null === $this->_date) {
            $this->_date = time();
        }
        
        return $this->_date;
    }

    /**
     * Set the date for this model follow
     *
     * @param int $date
     * @return Changelog_Model_Follow
     */
    public function setDate($date)
    {
        $this->_date = (int)$date;

        return $this;
    }

    /**
     * Get thje number of updates this model follow has
     *
     * @return int
     */
    public function getUpdates()
    {
        if (null === $this->_updates) {
            $this->_updates = 0;
        }
        
        return $this->_updates;
    }

    /**
     * Set the number of updates this model follow has
     *
     * @param int $updates
     * @return Changelog_Model_Follow
     */
    public function setUpdates($updates)
    {
        $this->_updates = (int)$updates;

        return $this;
    }

    /**
     * Get a user model for this follow
     *
     * @return User_Model_User
     */
    public function getUser()
    {
        if (null === $this->_user) {
            $mapper = $this->getMapper('User_Model_UserMapper');
            $this->_user = $mapper->find($this->getUserid());
        }

        return $this->_user;
    }

    /**
     * Get the model that is being followed
     *
     * @return Mcmr_Model_ModelAbstract
     */
    public function getModel()
    {
        if (null === $this->_model) {
            $mapper = $this->getMapper($this->getModeltype().'Mapper');
            $this->_model = $mapper->find($this->getModelid());
        }

        return $this->_model;
    }
}
