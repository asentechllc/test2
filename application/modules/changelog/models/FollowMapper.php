<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Changelog
 * @package Changelog_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for Changelog Follows
 *
 * @category Changelog
 * @package Changelog_Model
 * @subpackage FollowMapper
 */
class Changelog_Model_FollowMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Changelog_Model_DbTable_Follow';
    protected $_dbAttrTableClass = null;
    protected $_columnPrefix = 'follow_';
    protected $_modelClass = 'Changelog_Model_Follow';
    protected $_cacheIdPrefix = 'ChangelogFollow';

    /**
     * Reset the number of updates for this follow record back to 0
     *
     * @param type $userid
     * @param type $modeltype
     * @param type $modelid
     * @return Changelog_Model_FollowMapper 
     */
    public function resetUpdates($userid, $modeltype, $modelid)
    {
        $this->getDbTable()->update(
            array('follow_updates'=>0),
            array(
                'user_id = ?'=>$userid,
                'follow_modeltype = ?'=>$modeltype,
                'follow_modelid = ?'=>$modelid,
            )
        );

        return $this;
    }

    /**
     * Increment the number of updates for this record by 1
     *
     * @param type $modeltype
     * @param type $modelid
     * @return Changelog_Model_FollowMapper 
     */
    public function incrementUpdates($modeltype, $modelid)
    {
        $this->getDbTable()->update(
            array('follow_updates'=>new Zend_Db_Expr('follow_updates+1')),
            array(
                'follow_modeltype = ?'=>$modeltype,
                'follow_modelid = ?'=>$modelid,
            )
        );

        return $this;
    }

    /**
     * Convert a readable fieldname into the database fieldname
     *
     * @param string $field
     * @return string
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }

        return $field;
    }

    /**
     * Convert field value before inserting into databse query. 
     * 
     * @param string $field field name passed to be able to tell which filed you are dealing with.
     * @param mixed $value field value as stored in  the object. Process if necessary and return.
     * @return Zend_Db_Expr|string|int field value in the format suitable for a database query. 
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'follow_date':
                $value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
                break;
        }

        return $value;
    }
    
    /**
     * Create a list of fields to be read form the databse in SELECT query.
     * By default read all fields using sql wildcard.
     * Subclasses can append to this if some calculated fields are required or replace with a list of specific fields.
     * 
     * @return array list of fields to read suitable for Zend_Db_Select
     */
    protected function _selectFields()
    {
        $all = parent::_selectFields();
        $all[] = "UNIX_TIMESTAMP(follow_date) AS follow_date";
        return $all;
    }
}
