<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Changelog
 * @package    Changelog_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: RecordMapper.php 694 2010-05-24 14:47:27Z leigh $
 */

/**
 * Mapper class for all Changelog Record models
 *
 * @category   Changelog
 * @package    Changelog_Model
 * @subpackage RecordMapper
 */
class Changelog_Model_RecordMapper extends Mcmr_Model_MapperAbstract
{
    protected $_dbTableClass = 'Changelog_Model_DbTable_Record';
    
    public function __construct()
    {
        parent::__construct();
        
        $this->registerObserver('Cache');
    }

    /**
     * Take a model and save it to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return Mcmr_Model_MapperAbstract
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        // Sanitise all model data for the database
        $data = $this->_addColumnPrefix($model->getOptions(), 'record_');
        $data['user_id'] = $data['record_userid'];
        $data['item_id'] = $data['record_itemid'];
        $data['record_date'] = new Zend_Db_Expr("FROM_UNIXTIME({$data['record_date']})");
        unset($data['record_userid']);
        unset($data['record_itemid']);

        if (null === ($id = $model->getId())) {
            // Brand new record. Insert it.
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);

            // Notify observers
            $this->notify('insert', $model);
        } else {
            // Records cannot be updated
            throw new Mcmr_Model_Exception("Cannot alter Changelog records");
        }

        return $this;
    }

    /**
     * Delete the model from the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     * @return Mcmr_Model_MapperAbstract
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        // Records cannot be deleted
        throw new Mcmr_Model_Exception("Cannot delete Changelog records");
        
        return $this;
    }

    /**
     * Find a model based on the condition
     *
     * @param array $condition
     * @return Changelog_Model_Record
     */
    public function findOneByField($condition = null)
    {
        $cacheid = 'Record_findOneByField'.$this->_getCacheId($condition);
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($record = $cache->load($cacheid))) {
            $table = $this->getDbTable();
            $select = $table->select()
                    ->from(
                        $table,
                        array(Zend_Db_Table_Select::SQL_WILDCARD, 'UNIX_TIMESTAMP(record_date) as record_date')
                    );

            if (null !== $condition) {
                foreach ($condition as $field=>$value) {
                    $field = $this->_sanitiseField($field);
                    $select->where($field.' = ?', $value);
                }
            }

            // Fetch the data from the database and create the model. Save to cache
            $row = $table->fetchRow($select);
            if (is_object($row)) {
                $row = $row->toArray();

                $record = new Changelog_Model_Record();
                $record->setOptions($this->_stripColumnPrefix($row, 'record_'));
                
                $cache->save($record, $cacheid, array('changelog_model_record_id_'.$record->getId()));
            } else {
                $record = null;
                
                $cache->save($record, $cacheid);
            }

            $this->notify('cacheMiss', $record);
        }
        // Notify observers
        $this->notify('load', $record);

        return $record;
    }

    /**
     * Get a paginator object based on the condition
     *
     * @param array $condition
     * @param array|int $page
     * @return Zend_Paginator
     */
    public function findAllByField($condition = array(), $order = array(), $page = 1)
    {
        if (is_array($page)) {
            $countPerPage = $page['count'];
            $page = $page['page'];
        } else {
            $countPerPage = 10;
        }
        
        // A Bug in Zend_Paginator means the cache is not working. We have to do it ourselves.
        // http://framework.zend.com/issues/browse/ZF-6989
        $cachecondition = $condition;
        $cachecondition['page'] = $page;
        $cachecondition['count'] = $countPerPage;
        $cacheid = 'Record_findAllByField'.$this->_getCacheId($cachecondition, $order);
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        
        if (false == ($paginator = $cache->load($cacheid))) {
            $select = $this->getDbTable()->select();

            // Set the query conditions
            if (is_array($condition)) {
                foreach ($condition as $field=>$value) {
                    $field = $this->_sanitiseField($field);
                    $select->where($field.' = ?', $value);
                }
            }

            // Set the query order
            if (!empty($order)) {
                $orderclause = array();
                foreach ($order as $field=>$direction) {
                    $field = $this->_sanitiseField($field);
                    $orderclause[] = "$field $direction";
                }
                
                $select->order($orderclause);
            }

            // Load the paginator. Use $this->filter to load individual users
            $paginator = new Mcmr_Paginator(new Zend_Paginator_Adapter_DbTableSelect($select));
            $paginator->setItemCountPerPage($countPerPage);
            $paginator->setFilter(new Mcmr_Filter_Callback(array($this, 'filter')));
            $paginator->setCurrentPageNumber($page);
            $paginator->getCurrentItems(); // Force the items to be retrieved from the database now for the cache

            $cache->save($paginator, $cacheid, array('changelog_model_record_index'));
            
            $this->notify('cacheMiss', $paginator);
        }
        
        // Notify observers of a paginator load
        $this->notify('load', $paginator);
        
        return $paginator;
    }

    /**
     * Takes Zend_Db_Table rows and maps them to the model.
     *
     * @param array $rows
     * @return array
     */
    public function filter($rows)
    {
        $models = array();
        foreach ($rows as $row) {
            // Use the mapper find. This is more taxing on first load. But afterwards every user will be cached.
            // This prevents a full DB hit if a single user is updated.
            $model = $this->find($row['record_id']);

            $models[] = $model;
        }

        return $models;
    }

    /**
     * Takes the model field name and translates it into the database fieldname
     *
     * @param type $field
     * @return string 
     */
    private function _sanitiseField($field)
    {
        switch ($field) {
            case 'userid':
                $field = 'user_id';
                break;
            case 'itemid':
                $field = 'item_id';
                break;
            default:
                $field = 'record_'.$field;
                break;
        }

        return $field;
    }
}
