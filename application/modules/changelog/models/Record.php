<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Changelog
 * @package    Changelog_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Record.php 1320 2010-09-07 08:03:53Z andrew $
 */

/**
 * Description of Record
 *
 * @category   Changelog
 * @package    Changelog_Model
 * @subpackage Record
 */
class Changelog_Model_Record extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Changelog_Model_RecordMapper';

    protected $_id = null;
    protected $_date = null;
    protected $_userid = null;
    protected $_type = null;
    protected $_itemid = null;
    protected $_action = null;

    /**
     * Return model mapper for Record
     *
     * @return Changelog_Model_RecordMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the unique ID for this record model
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the unique ID for this record model
     *
     * @param int $id
     * @return Changelog_Model_Record
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the date of this record model
     *
     * @return int A Unix timestamp
     */
    public function getDate()
    {
        return $this->_date;
    }

    /**
     * Set the date for this record
     *
     * @param int $date
     * @return Changelog_Model_Record
     */
    public function setDate($date)
    {
        $this->_date = $date;

        return $this;
    }

    /**
     * Get the user associated with this record
     *
     * @return int
     */
    public function getUserid()
    {
        return $this->_userid;
    }

    /**
     * Set the user associated with this record
     *
     * @param int $userid
     * @return Changelog_Model_Record
     */
    public function setUserid($userid)
    {
        $this->_userid = $userid;

        return $this;
    }

    /**
     * Get the type of record
     *
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Set the record model type that the user is following
     *
     * @param string $type
     * @return Changelog_Model_Record
     */
    public function setType($type)
    {
        $this->_type = $type;

        return $this;
    }

    /**
     * Get the record item model ID
     *
     * @return int
     */
    public function getItemid()
    {
        return $this->_itemid;
    }

    /**
     * Set the record item ID
     *
     * @param int $itemid
     * @return Changelog_Model_Record
     */
    public function setItemid($itemid)
    {
        $this->_itemid = $itemid;

        return $this;
    }

    /**
     * Get the record action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->_action;
    }

    /**
     * Set the record action
     *
     * @param string $action
     * @return Changelog_Model_Record
     */
    public function setAction($action)
    {
        $this->_action = $action;

        return $this;
    }

    /**
     * Get the item model the user is following
     *
     * @return Mcmr_Model_ModelAbstract
     */
    public function getItem()
    {
        $function = $this->getType().'::getMapper';
        if (method_exists($this->getType(), 'getMapper')) {
            $mapper = call_user_func($function);
            
            return $mapper->find($this->getItemid());
        } else {
            throw new Mcmr_Model_Exception("Cannot load record item '{$this->getType()}'");
        }
    }

    /**
     * Get the user model for this record
     *
     * @return User_Model_User|null
     */
    public function getUser()
    {
        $id = $this->getUserid();
        if (!empty($id)) {
            return User_Model_User::getMapper()->find($id);
        } else {
            return null;
        }
    }
}
