<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2002 2011-01-04 00:03:45Z leigh $
 */

/**
 * Default_IndexController
 *
 * The Purpose of this controller is to do nothing but forward the request onto the real default page
 * as determined by the configuration
 *
 * @category Default
 * @package Default_IndexController
 */
class Image_IndexController extends Zend_Controller_Action
{

    function init(){
        parent::init();

        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
        $this->options = $bootstrap->getOptions();
    }

    public function displayAction(){

        $path = $this->_request->getParam('path', false);

        $height = $this->_request->getParam('height', false);
        $width = $this->_request->getParam('width', false);
        $fit = $this->_request->getParam('fit', false);

        $resizeData = array();
        if (is_numeric($height)) {
            $resizeData['height'] = $height;
        }

        if (is_numeric($width)) {
            $resizeData['width'] = $width;
        }

        if ($fit) {
            $resizeData['fit'] = $fit;
        }

        $imagePath = $_SERVER['DOCUMENT_ROOT'] . $this->view->image($path, null, $resizeData, SITE_PATH, false);

        $mimeType = $this->getMimeType($imagePath);

        $this->_sendResponse($imagePath, $mimeType);
    }

    public function getMimeType($path)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        return finfo_file($finfo, $path);
    }

    private function _sendResponse($path, $mimeType)
    {
        $this->getResponse()
            ->setHeader('Content-Type', $mimeType)
            ->sendResponse();

        readfile($path);
        exit();
    }
}

