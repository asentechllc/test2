<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 2109 2011-01-25 15:48:43Z leigh $
 */

/**
 * Bootstrap
 *
 * @category Default
 * @package Default_Bootstrap
 */
class Image_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');

        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Default_View_Helper');
    }
}
