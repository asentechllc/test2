<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Bootstrap
 *
 * @category User
 * @package User_Bootstrap
 */
class User_Bootstrap extends Mcmr_Application_Module_Bootstrap
{
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');
        
        $view->addHelperPath(dirname(__FILE__) . '/helpers', 'User_View_Helper');
    }
}
