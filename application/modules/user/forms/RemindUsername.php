<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: ResetPassword.php 2335 2011-04-06 10:59:38Z leigh $
 */

/**
 * Form for resetting a password
 *
 * @category User
 * @package User_Form
 * @subpackage ResetPassword
 */
class User_Form_RemindUsername extends Mcmr_Form
{

    public function init()
    {
        $this->setMethod('post');
    }
}

