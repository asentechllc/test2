<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: User.php 1792 2010-11-17 17:09:21Z leigh $
 */

/**
 * Form for user models
 *
 * @category User
 * @package User_Form
 * @subpackage User
 */
class User_Form_User extends Mcmr_Form
{
    private $_updateform = false;
    private $_emailChanged = false;
    private $_usernameChanged = false;
    
    private $_emailValidator = null;
    private $_usernameValidator = null;

    public function __construct($options=null)
    {
        $type = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['type'])) {
                $type = $options['type'];
                unset($options['type']);
            }

            if (isset($options['isupdateform'])) {
                $this->_updateform = (bool) $options['isupdateform'];
                unset($options['isupdateform']);
            }
            
            if (isset($options['emailChanged'])) {
                $this->_emailChanged = (bool) $options['emailChanged'];
                unset($options['emailChanged']);
            }
            
            if (isset($options['usernameChanged'])) {
                $this->_usernameChanged = (bool) $options['usernameChanged'];
                unset($options['usernameChanged']);
            }
        }

        if (null != $type) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'user'
            . DIRECTORY_SEPARATOR . 'user_form_user-' . strtolower($type) . '.ini';
        }

        parent::__construct($options);
    }

    public function init()
    {
        $this->setName('userformuser')->setElementsBelongTo('user-form-user');
        $this->setMethod('post');
        $this->setEnctype("multipart/form-data");
    }

    public function postInit()
    {
        $acl = Mcmr_Acl::getInstance();
        $user = Zend_Registry::get('authenticated-user');

        // Only display the following element if the user has permission to change other user's roles
        if ((null !== $user) && $acl->isAllowed($user->getRole(), 'user-index', 'update-role')) {
            $roleElement = $this->getElement('role');
            if (null !== $roleElement && method_exists($roleElement, 'setMultiOptions')) {
                $roles = $this->_getRoles($acl, $user->getRole());
                $roleElement->setMultiOptions($roles);
            }

            $stateElement = $this->getElement('state');
            if (null !== $stateElement && method_exists($stateElement, 'setMultiOptions')) {
                $states = $this->_getStates();
                $stateElement->setMultiOptions($states);
            }
        } else {
            $this->removeElement('role');
            $this->removeElement('state');
        }

        $passwordElement = $this->getElement('password');
        if (null !== $passwordElement) {
            $passwordElement->setRequired(!$this->_updateform);
        }
        $passwordElement = $this->getElement('confirmpassword');
        if (null !== $passwordElement) {
            $passwordElement->setRequired(!$this->_updateform);
            $passwordElement->setValue('');
        }
        
        $this->_emailValidators();
        $this->_usernameValidators();
    }

    public function getEmailChanged()
    {
        return $this->_emailChanged;
    }

    public function setEmailChanged($emailChanged)
    {
        $this->_emailChanged = $emailChanged;
        
        $this->_emailValidators();
        
        return $this;
    }

    public function getUsernameChanged()
    {
        return $this->_usernameChanged;
    }

    public function setUsernameChanged($usernameChanged)
    {
        $this->_usernameChanged = $usernameChanged;
        
        $this->_usernameValidators();
        
        return $this;
    }

    private function _emailValidators()
    {
        $element = $this->getElement('email');
        if (null !== $element) {
            if ($this->_updateform && !$this->_emailChanged) {
                // Remove NoRecordExists validator from email field
                if (null === $this->_emailValidator) {
                    $this->_emailValidator = $element->getValidator('norecordexists');
                }
                
                $element->removeValidator('norecordexists');
            } elseif ($this->_emailChanged && is_object($this->_emailValidator)) {
                // Email has changed. Restore the validator
                $element->addValidator($this->_emailValidator);
            }
        }
    }
    
    private function _usernameValidators()
    {
        $element = $this->getElement('username');
        if (null !== $element) {
            if ($this->_updateform && !$this->_usernameChanged) {
                if (null === $this->_usernameValidator) {
                    $this->_usernameValidator = $element->getValidator('norecordexists');
                }

                $element->removeValidator('norecordexists');
            } elseif ($this->_usernameChanged && is_object($this->_usernameValidator)) {
                // Username has changed and there is a validator. Restore it
                $element->addValidator($this->_usernameValidator);
            }
        }
    }

    private function _getStates()
    {
        $states = User_Model_User::validStates();
        $stateValues = array();
        foreach ($states as $state) {
            $stateValues[$state] = ucfirst($state);
        }

        return $stateValues;
    }

    private function _getRoles(Zend_Acl $acl, $userrole)
    {
        $roles = $acl->getRoleOptions();

        return $roles;
    }

}
