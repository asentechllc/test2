<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   User
 * @package    Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Order.php 2196 2011-02-15 13:03:05Z michal $
 */

/**
 * A form for changing the display order of users
 *
 * @category   User
 * @package    Form
 * @subpackage Order
 */
class User_Form_Order extends Mcmr_Form_Order
{

}
