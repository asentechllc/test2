<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Login.php 1690 2010-10-30 13:14:07Z leigh $
 */

/**
 * Login Form
 *
 * @category User
 * @package User_Form
 * @subpackage Login
 */
class User_Form_Login extends Mcmr_Form
{
    public function init()
    {
        $this->setName('userformlogin')->setElementsBelongTo('user-form-login');
        $this->setMethod('post');

        // Add an email element
        $this->addElement(
            'Text', 'username', array(
                'label'      => 'Username',
                'required'   => true,
                'filters'    => array('StringTrim'),
                'invalidMessage' => 'Username must be specified',
            )
        );

        // Add an email element
        $this->addElement(
            'Password', 'password', array(
                'label'      => 'Password',
                'required'   => true,
                'filters'    => array('StringTrim'),
                'invalidMessage' => 'Password must be specified',
            )
        );
        $this->addElement(
            'Submit', 'submit',
            array(
                'ignore' => true ,
                'label' => 'Login'
            )
        );

        // Add some CSRF protection
        $this->addElement(
            'hash', 'csrf',
            array(
                'salt' => 'unique'
            )
        );
    }
}
