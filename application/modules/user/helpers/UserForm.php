<?php

class User_View_Helper_UserForm extends Zend_View_Helper_Abstract
{
    public function userForm($id=null)
    {
        $form = new User_Form_User();

        if (null !== $id) {
            $mapper = User_Model_User::getMapper();
            $user = $mapper->find($id);
            $form->populate($user->getOptions(true));
        }

        return $form;
    }
}
