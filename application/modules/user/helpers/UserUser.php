<?php

class User_View_Helper_UserUser extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve a user based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function userUser($conditions = array(), $order=null, $partial = null)
    {
        $mapper = User_Model_User::getMapper();
        $user = $mapper->findOneByField($conditions, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('user'=>$user));
                }
            }

            return $this->view->partial($partial, array('user'=>$user));
        }

        return $user;
    }
    
}
