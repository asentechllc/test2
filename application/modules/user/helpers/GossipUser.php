<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: GossipUser.php 2240 2011-03-09 17:36:28Z michal $
 */

/**
 * View helper to retrieve gossip users
 *
 * @category User
 * @package User_View
 * @subpackage Helper
 */
class User_View_Helper_GossipUser extends Zend_View_Helper_Abstract
{
    public function gossipUser()
    {
        $user = Zend_Registry::get('authenticated-user'); 
        if ( $user->getId() ) {
            return $user;
        } else {
            return null;
        }
    }
}