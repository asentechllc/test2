<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   User
 * @package    User_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: UserCount.php 1308 2010-09-03 15:22:16Z leigh $
 */

/**
 * A View helper to retrieve the number of users based on the conditions
 *
 * @category   User
 * @package    User_View
 * @subpackage Helper
 */
class User_View_Helper_UserCount extends Zend_View_Helper_Abstract
{
    function userCount($condition=null)
    {
        $mapper = User_Model_User::getMapper();
        
        return $mapper->count($condition);;
    }
}
