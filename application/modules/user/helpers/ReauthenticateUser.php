<?php

class User_View_Helper_ReauthenticateUser extends Zend_View_Helper_Abstract
{
    public function reauthenticateUser( $formPartial, $failurePartial )
    {
        $request = Zend_Controller_Front::getInstance()->getRequest(); 
        $form = new User_Form_Login();
        $showForm = false;
        if ($request->isPost()) {
            if ($form->isValid($request->getPost())) {
                $values = $form->getValues(true);
                $mapper = User_Model_User::getMapper();
                if (null !== ($user = $mapper->authenticate($values))) {
                    return true;
                } else {
                    $mapper->logout();
                    echo  $this->view->partial($failurePartial);
                }
            } else {
                $showForm = true;
            }
        } else {
            $showForm = true;
        }
        if ( $showForm ) {
            $after = $request->getRequestUri();
            echo  $this->view->partial($formPartial, array('form'=>$form));
        }
        
        return false;
    }
}
