<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   User
 * @package    User_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: UserValidStates.php 1223 2010-08-13 08:46:26Z leigh $
 */

/**
 * A View helper to retrieve the valid user states
 *
 * @category   User
 * @package    User_View
 * @subpackage Helper
 */
class User_View_Helper_UserValidStates extends Zend_View_Helper_Abstract
{

    /**
     * Return an array of valid user states
     *
     * @return array
     */
    public function userValidStates()
    {
        return User_Model_User::validStates();
    }
}
