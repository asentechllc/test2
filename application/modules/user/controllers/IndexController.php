<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2316 2011-03-31 18:11:46Z michal $
 */

/**
 * Index controller for the user module. Looks after all user models.
 *
 * @category User
 * @package User_IndexController
 */
class User_IndexController extends Mcmr_Controller_Action
{

    public function init()
    {
        parent::init();
        $contextSwitch = $this->_helper->getHelper('ContextSwitch');

        if (!$contextSwitch->hasContext('html')) {
            $contextSwitch->addContext('html', array());
        }

        $contextSwitch->addContext(
            'csv',
            array(
                'headers'=>array('Content-Type'=>'text/csv'),
                'suffix'=>'csv',
            )
        );

        $contextSwitch->addActionContext('export', 'csv')
                ->initContext();

        $ajaxContext = $this->_helper->getHelper('AjaxContext');
        $ajaxContext->setAutoJsonSerialization(false)
            ->addActionContext('index', 'json')
            ->addActionContext('index', 'html')
            ->addActionContext('read', 'json')
            ->addActionContext('read', 'html')
            ->addActionContext('create', 'html')
            ->addActionContext('create', 'json')
            ->addActionContext('register', 'html')
            ->addActionContext('update', 'html')
            ->addActionContext('update', 'json')
            ->addActionContext('login', 'html')
            ->addActionContext('login', 'json')
            ->addActionContext('password', 'html')
            ->addActionContext('password', 'json')
            ->addActionContext('username', 'html')
            ->addActionContext('username', 'json')
            ->initContext();
    }

    /**
     * List all the registered users
     */
    public function indexAction()
    {
        if (null === $this->_request->getParam('id', null)) {
            // Get and validate the role filter
            $validator = new Zend_Validate_InArray(Mcmr_Acl::getInstance()->getRoles());
            $role = $this->_getParam('role', null);
            $role = $validator->isValid($role)?$role:null;

            // Get and validate the state filter
            $validator->setHaystack(User_Model_User::validStates());
            $state = $this->_getParam('state', null);
            $state = $validator->isValid($state)?$state:null;

            $filter = $this->_getParam('filter', null);

            $emailconfirmed = $this->_getParam('emailconfirmed', null);

            $site = $this->_getParam('sites', null);

            $fields = array('state'=>$state, 'role'=>$role, 'filter'=>$filter, 'emailconfirmed'=>$emailconfirmed, 'sites'=>$site);

            foreach (array( 'email', 'surname', 'firstname', 'attr_postcode', 'attr_cdsUrn', 'username') as $filter ) {
                $value = $this->_getParam($filter, null);
                if (null !== $value) {
                    $fields[ $filter ] = array('condition'=>'LIKE', 'value'=>"%{$value}%" );
                }
            }

            $config = Zend_Registry::get('user-config');
            $page = array();
            $page['page'] = $this->_getParam('page', 1);
            $page['count'] = $this->_getParam('count', $config->users->itemCountPerPage);

            $mapper = User_Model_User::getMapper();
            $this->view->users = $mapper->findAllByField($fields, null, $page);
            $this->view->role = $role;
        } else {
            $this->_forward('read');
        }
    }

    /**
     * Create a new user
     */
    public function createAction()
    {
        $type = $this->_request->getParam('role', null);
        $form = $this->_helper->loadForm('User', array('type'=>$type, 'isupdateform'=>false));
        $this->view->form = $form;
        $this->view->role = $type;

        if ($this->_request->isPost()) {
            $mapper = User_Model_User::getMapper();
            $user = new User_Model_User();

            if ($form->isValid($this->_request->getPost())) {
                $this->view->values = $form->getValues(true);
                $user->setAttributeTypes($form->getTypes());
                $user->setOptions($form->getValues(true));
                $userformuser = $this->_request->getParam('userformuser');
                $user->setRole($userformuser['role']);

                try {
                    $mapper->save($user);
                    $this->view->user = $user;
                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('userCreated', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $user);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidCreateValues', 'warn');
            }
        }
    }

    /**
     * Display an individual user
     */
    public function readAction()
    {
    	$id = $this->_request->getParam('id', null);
    	$seoId = $this->_request->getParam('seoId', null);
    	$url = $this->_request->getParam('url', null);
    	$user = null;

        if (($seoId && $url) || $id || $url) {
            $mapper = User_Model_User::getMapper();

			if($id){
				$user = $mapper->find($id);
			}
			else if($seoId && $url){

				$id = Mcmr_StdLib::realId($seoId);
				$user = $mapper->find($id);

				if(Mcmr_StdLib::urlize($user->getFullName()) !== $url){
					$user = null;
				}

			}

			# find user by URL alone if possible
			else if($url){
				$db = $mapper->getDbTable()->getAdapter();
				$id = $db->fetchOne("SELECT user_id FROM user_users WHERE REPLACE(REPLACE(TRIM(CONCAT(user_firstname,'-',user_surname)),'\'','-'),' ','-') LIKE ? LIMIT 1",array($url));
				$user = $mapper->find($id);
			}

            if ($user) {
                $this->view->user = $user;
            } else {
                throw new Mcmr_Exception_PageNotFound('User Not Found');
            }

            // Change the template to the page name
            $this->_helper->viewRenderer('read/'.$this->_request->getParam('page', ''));
        } else {
			$redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
		    $redirector->gotoUrl('/');
        }
    }

    /**
     * Edit a user
     */
    public function updateAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = User_Model_User::getMapper();
            if (null !== ($user = $mapper->find($id))) {
                $form = $this->_helper->loadForm('User', array('type'=>$user->getRole(), 'isupdateform'=>true));
                $this->view->form = $form;

                // Change the template to the page name
                $this->_helper->viewRenderer('update/'.$this->_request->getParam('page', ''));

                $this->view->user = $user;

                if ($this->_request->isPost()) {
                    // Merge the existing user data with the form values. This allows for partial updates.
                    $post = $this->_request->getPost();
                    $uservalues = $user->getOptions(true);
                    $uservalues['confirmemail'] = $uservalues['email'];
                    $uservalues['confirmpassword'] = '';
                    $post['userformuser'] = array_merge($uservalues, $post['userformuser']);

                    // Tell form if the username or email address has been changed
                    if (null !== $post['userformuser']['email']) {
                        $form->setEmailChanged($post['userformuser']['email'] !== $user->getEmail());
                    }
                    if (null !== $post['userformuser']['username']) {
                        $form->setUsernameChanged($post['userformuser']['username'] !== $user->getUsername());
                    }

                    if ($form->isValid($post)) {
                        // Attempt to save the new user
                        $oldIncomplete = $user->getAttribute('incomplete');
                        $this->view->values = $form->getValues(true);
                        $user->setAttributeTypes($form->getTypes());

						# INT000-870                        
						# if we're trying to store as admin/editor, need to move role to 
						# end of options array so that it does not get superseded by 'state'
						$options = $form->getValues(true);
						if(isset($options['role']) && in_array($options['role'],array('admin','editor'))){
							$role = $options['role'];
							unset($options['role']);
							$options['role'] = $role;
						}
                        
                        $user->setOptions($options);
                        
                        $newIncomplete = $user->getAttribute('incomplete');
                        $completed = $oldIncomplete && !$newIncomplete;
                        try {
                            $mapper->save($user);

                            // If the email address has changed, resend the confirmation. Reset this flag.
                            if ($user->emailChanged(true) || $completed) {
                                $this->_emailConfirm($user);
                            }

                            $this->view->error = false;
                            $this->view->getHelper('DisplayMessages')->addMessage('updateSuccessful', 'info');
                            $this->view->getHelper('Redirect')->notify('update', $user);
                        } catch (Exception $e) {
                            $this->view->error = true;
                            $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                        }

                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('invalidUpdateValues', 'warn');
                        $this->view->values = $form->getValues(true);
                    }
                } else { // Not a post, display the populated form
                    $values = $user->getOptions(true);
                    $values['confirmemail'] = $user->getEmail();

                    $form->populate($values);
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('User Not Found');
            }

            // Remove password from form
            if (null !== $form->password) {
                $form->password->setValue('');
            }
        } else { // No id set, forward the user to the index
            $this->_forward('index');
        }
    }

    /**
     * Delete a user
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = User_Model_User::getMapper();
            if (null !== ($user = $mapper->find($id))) {
                $this->view->user = $user;

                if ($this->_request->isPost()) {
                    try {
                        $mapper->delete($user);
                        $this->view->error = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('userDeleted', 'info');
                        $this->view->getHelper('Redirect')->notify('delete', $user);
                        $this->view->user = null;
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');;
                    }
                }
            } else {
                throw new Mcmr_Exception_PageNotFound('User Not Found');
            }
        } else {
            $this->_forward('index');
        }
    }

    /**
     * User login/authentication action
     */
    public function loginAction()
    {
        $form = new User_Form_Login();
        $form->setAction($this->view->url(array('action' => 'login')));
        $this->view->form = $form;

        if ($this->_request->isPost()) {
            if (!$form->isValid($this->_request->getPost())) {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidLoginValues', 'warn');
            } else {
                $values = $form->getValues(true);
                $mapper = User_Model_User::getMapper();
                try {
                    if (null !== ($user = $mapper->authenticate($values))) {
                        $mapper->save($user);

                        $this->view->error = false;
                        $this->view->authenticated = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('loginSuccessful', 'info', 'default');
                        $this->view->getHelper('Redirect')->notify('login', $user);

                        $this->view->user = $user;
                    } else {
                        $this->view->error = true;
                        $this->view->authenticated = false;
                        $this->view->getHelper('DisplayMessages')->addMessage('loginFailed', 'warn', 'default');
                        $this->view->getHelper('Redirect')->notify('register', $user);

                        // Remove password from form
                        $form->password->setValue('');
                    }
                } catch (User_Model_Exception_LoginFailed $e) {
                    $this->view->error = true;
                    $this->view->authenticated = false;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'warn');

                    // Remove password from form
                    $form->password->setValue('');
                }
            }
        }
    }

    /**
     * Logout the user
     */
    public function logoutAction()
    {
        $mapper = User_Model_User::getMapper();
        $mapper->logout();

        $this->view->authenticated = false;
        $this->view->getHelper('Redirect')->notify('logout');
    }

    /**
     * Register a new user
     */
    public function registerAction()
    {
        $form = new User_Form_User();
        $this->view->form = $form;
        $this->view->user = null;

        // Change the template to the page name
        $this->_helper->viewRenderer('register/'.$this->_request->getParam('subPage', ''));

        if ($this->_request->isPost()) {
            // Save the form if the values are valid.
            if (!$form->isValid($this->_request->getPost())) {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidRegistrationValues', 'warn');
                $this->view->values = $form->getValues(true);
            } else {
                // Attempt to save the new user
                $values = $form->getValues(true);
                $user = new User_Model_User($values);
                $user->setAttributeTypes($form->getTypes());
                $config = Zend_Registry::get('user-config');
                $user->setRole($config->user->defaultRegisteredRole);
                $this->view->values = $form->getValues(true);

                try {
                    $mapper = User_Model_User::getMapper();
                    $mapper->save($user);

                    if (null === Zend_Registry::get('authenticated-user')
                            || null === Zend_Registry::get('authenticated-user')->getId()) {
                        $values['username'] = $values['email'];
                        $mapper->authenticate($values);
                    }

                    // Send a confirmation email
                    $this->_emailConfirm($user);

                    $this->view->user = $user;
                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('registrationSuccessful');
                    $this->view->getHelper('Redirect')->notify('register', $user);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        }
    }

    /**
     * Confirm an email address
     */
    public function confirmAction()
    {
        $this->view->confirmed = false;
        $token = $this->_request->getParam('token', false);
        $id = $this->_request->getParam('id', null);
        $email = $this->_request->getParam('email', null);

        if (false !== $token && null !== $id) {
            // Confirm an email address
            $user = User_Model_User::getMapper()->find($id);

            if (null !== $user && $user->getToken() == $token) {
                $user->setEmailconfirmed(true);
                $user->setToken('');
                $user->setTokenexpire(0);

                User_Model_User::getMapper()->save($user);
                $this->view->confirmed = true;
                $this->view->getHelper('DisplayMessages')->addMessage('confirmEmailConfirmed', 'info');
                $this->view->getHelper('Redirect')->notify('confirm', $user);
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('confirmNoToken', 'warn');
            }
        } elseif (null !== $email) {
            // Resend a confirmation email
            $user = User_Model_User::getMapper()->findOneByField(array('email' => $email));
            $this->view->user = $user;
            if (null !== $user && !$user->getEmailconfirmed()) {
                $this->_emailConfirm($user);
                $this->view->getHelper('DisplayMessages')->addMessage('confirmEmailSent', 'info');
                $this->view->getHelper('Redirect')->notify('confirm', $user);
            } elseif (null == $user) {
                $this->view->getHelper('DisplayMessages')->addMessage('confirmEmailNotFound', 'warn');
            } else {
                $this->view->getHelper('DisplayMessages')->addMessage('confirmUserActive', 'warn');
            }
        }
    }

    /**
     * Reset a forgotten password
     */
    public function passwordAction()
    {
        $form = new User_Form_ResetPassword();
        $this->view->form = $form;
        $this->view->reset = false;

        $userMapper = User_Model_User::getMapper();
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                // Find the user and set a token
                $email = $form->getValue('email');
                $user = $userMapper->findOneByField(array($userMapper->usernameField() => $email));
                if (null !== $user) {
                    $validator = new Zend_Validate_EmailAddress();
                    if (null !== $user->getEmail() && $validator->isValid($user->getEmail())) {
                        $this->_emailConfirmPassword($user);
                        $this->view->getHelper('DisplayMessages')->addMessage('passwordEmailSent', 'info');
                    } else {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage('userWithoutEmail', 'warn');
                    }
                } else {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage('passwordEmailNotFound', 'warn');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
            }
        } else {
            $token = $this->_request->getParam('token', null);
            $id = $this->_request->getParam('id', null);

            if (null !== $id) {
                $user = $userMapper->find($id);
                $acl = Mcmr_Acl::getInstance();
                $authenticatedUser = Zend_Registry::get('authenticated-user');
                if (null !== $user) {
                    $this->view->user = $user;

                    if (
                        (($user->getToken() == $token) && !empty($token))
                        || $acl->isAllowed($authenticatedUser->getRole(), 'user-index', 'passwordReset')
                    ) {
                        // Reset the password
                        $password = Mcmr_StdLib::randomString(6, 8);
                        $user->setPassword($password);
                        $user->setToken('');
                        $user->setTokenExpire(0);

                        $userMapper->save($user);
                        $this->view->reset = true;

                        $this->_emailPassword($user);
                        $this->view->getHelper('DisplayMessages')->addMessage('passwordReset', 'info');
                        $this->view->getHelper('Redirect')->notify('password', $user);
                    } else {
                        $this->view->getHelper('DisplayMessages')->addMessage('passwordTokenNotFound', 'warn');
                    }
                } else {
                    $this->view->getHelper('DisplayMessages')->addMessage('userNotFound', 'warn');
                }
            }
        }
    }

    public function usernameAction()
    {
        $form = new User_Form_RemindUsername();
        $this->view->form = $form;
        $this->view->reset = false;
        $userMapper = User_Model_User::getMapper();
        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values=$form->getValues();
                $user = $userMapper->findOneByField($values);
                if (null !== $user) {
                    $this->_emailRemindUsername($user);
                    $this->view->getHelper('DisplayMessages')->addMessage('usernameSent', 'info');
                } else {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage('userNotFound', 'warn');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('invalidFormValues', 'warn');
            }
        }
    }

    /**
     * Export the user list to other formats. Display form on GET. Export data on POST.
     */
    public function exportAction()
    {
        if ($this->_request->isPost()) {
            $fields = $this->_request->getPost('conditions', null);

            $mapper = User_Model_User::getMapper();
            $this->view->users = $mapper->findExportUsers($fields);
            $this->view->conditions = $fields;
        }
    }

    public function imageAction()
    {

    }

    /**
     * Communication endpoint for social authentication with HybridAuth
     */
    public function endpointAction() {
        $this->view->getHelper('PageCache')->disable();
        Mcmr_Service_Hybrid_Endpoint::process();
    }

    /**
     * Social authentication redirect and to the authentication provider and processing post successful authentication
     */
    public function authenticateAction() {
        $userProfile = null;
        try {
            //try to authenticate
            $provider = $this->_request->getParam('provider');
            $hybridauth = Mcmr_Service_Hybrid_Auth::getInstance();
            $adapter = $hybridauth->authenticate( $provider );
            $userProfile = $adapter->getUserProfile();
            $userProfile->provider = $provider;
        } catch (Exception $e) {
            $this->view->error = true;
            $this->view->authenticated = false;
            $this->view->getHelper('DisplayMessages')->addMessage('socialProfileFailed', 'warn');
            return;
        }

        //failure to retrieve profile from authentication provider
        if (null===$userProfile) {
            $this->view->error = true;
            $this->view->authenticated = false;
            $this->view->getHelper('DisplayMessages')->addMessage('loginFailed', 'warn');
            return;
        }

        $authenticatedUser = Zend_Registry::get('authenticated-user');

        $providerId = $userProfile->identifier;
        $mapper = User_Model_User::getMapper();

        if (null!==$authenticatedUser && 'guest'!==$authenticatedUser->getRole()) {
            //if a gossip user is already authenticated to the site, we will do the following
            //does the gossip user have a connected profile already?
            //1. yes
            //   this is an error, if they are already logged in and have a profile with a provider attached,
            //   they should not be able authenticate with that provider through the website,
            //
            //2. no
            //   this is the part of the flow, where they log in with gossip account and then are attaching social profile to it
            //   we need to check if this social provider account is already connected to a different gossip account?
            //   a. yes
            //      we can't connect the same facebook account to two different accounts, show them an error
            //   b. no
            //      they successfully authenticated with gossip earlier and social provider now,
            //      update gossip user to be connected with this social provider
            if (null!==$authenticatedUser->getSocialProfileId($provider)) {
                throw new Zend_Exception("This user has already authenticated with $provider!");
            } else {
                $user = $mapper->findOneBySocialProfile($userProfile);
                if (null!==$user ) {
                    $this->view->error = true;
                    $adapter->logout();
                    $this->view->getHelper('DisplayMessages')->addMessage('socialProfileRegistered', 'warn');
                }else  {
                    $authenticatedUser->setSocialProfileId($provider, $providerId);
                    $mapper->save($authenticatedUser);
                    $this->_redirectAfterProfileConnected($authenticatedUser);
                }
            }
        } else {
            //if there is no user authenticated to the site,
            //is there a user that has already connected this social profile (identified by the p[rovied and the id)?
            //1. yes
            //   force authenticate this user: load it as the authenticated gossip user without asking for password
            //   redriect to whatever the user was looking at before login attempt, as normally would happen
            //2. no
            //   we will create a user with the email from the social profile, so on this occassion we check if this email is not already
            //   registered on the system. is it?
            //   a. yes
            //      show them an error message and log out of the social provider
            //   b. no
            //      create a new gossip user, intialize it with whatever data we can get from the social profile and, force authenticate and
            //      redirect to the page where they can finish they gossip profile
            $user = $mapper->findOneBySocialProfile($userProfile);
            if (null!==$user) {
                $mapper->forceAuthenticate($user);
                $this->view->error = false;
                $this->view->authenticated = true;
                $this->view->getHelper('DisplayMessages')->addMessage('loginSuccessful', 'info');
                $this->view->getHelper('Redirect')->notify('login', $user);
                $this->view->user = $user;

            } else {
                $user = $mapper->createFromSocialProfile($userProfile);
                if ($mapper->userEmailExists($user)) {
                    $this->view->error = true;
                    $this->view->authenticated = false;
                    $adapter->logout();
                    $this->view->getHelper('DisplayMessages')->addMessage('socialEmailRegistered', 'warn');
                } else {
                    $mapper->save($user);
                    $mapper->forceAuthenticate($user);
                    $this->_redirectAfterProfileConnected($user);
                }
            }
        }
    }

    private function _redirectAfterProfileConnected($user)
    {
        $redirector = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        $params = array('module'=>'user', 'controller'=>'index', 'action'=>'update',
            'id'=>$user->getId());
        if ($user->getAttribute('incomplete')) {
            $params['journey']='welcome';
        }
        $url = $this->view->getHelper('Url')->url($params, 'default', true);
        $redirector->gotoUrl($url);
    }

    /**
     * Email a confirmation email to the user
     *
     * @param User_Model_User $user
     * @return unknown_type
     */
    private function _emailConfirm(User_Model_User $user)
    {
        $config = Zend_Registry::get('user-config');
        if (isset($config->user->register->sendConfirmEmail) && !$config->user->register->sendConfirmEmail) {
            return false;
        }

        $user->setTokenexpire(strtotime("+2 days"));
        $token = $user->getToken();
        $mapper = User_Model_User::getMapper();
        try {
            // We don't need to log this update.
            $mapper->deleteObserver('Changelog');
        } catch(Exception $e) {
        }

        $mapper->save($user); // A minor

        // Create the email and send it
        $emailtemplate = clone $this->view;
        $emailtemplate->token = $token;
        $emailtemplate->user = $user;

        $template = 'email/user-confirm.phtml';
        $sites = $user->getSites();
        if (count($sites)==1 && 'default'!=$sites[0]) {
            $template = $sites[0].'/'.$template;
        }
        $body = $emailtemplate->render( $template );
        $subject = Zend_Registry::get('user-config')->email->confirm->subject;

        $email = new Mcmr_Mail();
        $email->setBodyHtml($body);
        $email->setSubject($subject);
        $email->addTo($user->getEmail());
        $email->send();
    }

    private function _emailRemindUsername(User_Model_User $user)
    {
        // Create the email and send it
        $emailtemplate = clone $this->view;
        $emailtemplate->user = $user;

        $template = 'email/user-remind-username.phtml';
        $sites = $user->getSites();
        if (count($sites)==1 && 'default'!=$sites[0]) {
            $template = $sites[0].'/'.$template;
        }
        $body = $emailtemplate->render( $template );
        $subject = Zend_Registry::get('user-config')->email->remindUsername->subject;

        $email = new Mcmr_Mail();
        $email->setBodyHtml($body);
        $email->setSubject($subject);
        $email->addTo($user->getEmail());
        $email->send();
    }

    /**
     * Email a reset password confirmation email
     *
     * @param User_Model_User $user
     * @return unknown_type
     */
    private function _emailConfirmPassword(User_Model_User $user)
    {
        $user->setTokenexpire(strtotime("+2 days"));
        $token = $user->getToken();
        User_Model_User::getMapper()->save($user);

        // Create the email and send it
        $emailtemplate = new Zend_View();
        $emailtemplate->setScriptPath(SITE_PATH . '/views/user/scripts/');
        $emailtemplate->token = $token;
        $emailtemplate->user = $user;

        $body = $emailtemplate->render('email/confirmPassword.phtml');
        $subject = Zend_Registry::get('user-config')->email->confirmPassword->subject;

        $email = new Mcmr_Mail();
        $email->setBodyHtml($body);
        $email->setSubject($subject);
        $email->addTo($user->getEmail());
        $email->send();
    }

    /**
     * Email the new password to the user
     *
     * @param User_Model_User $user
     * @return unknown_type
     */
    private function _emailPassword(User_Model_User $user)
    {
        // Create the email and send it
        $emailtemplate = new Zend_View();
        $emailtemplate->setScriptPath(SITE_PATH . '/views/user/scripts/');
        $emailtemplate->user = $user;

        $body = $emailtemplate->render('email/password.phtml');
        $subject = Zend_Registry::get('user-config')->email->password->subject;

        $email = new Mcmr_Mail();
        $email->setBodyHtml($body);
        $email->setSubject($subject);
        $email->addTo($user->getEmail());
        $email->send();
    }
}

