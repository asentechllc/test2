<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category 
 * @package 
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Description of ExportIterator
 *
 * @category 
 * @package 
 * @subpackage 
 */
class User_Model_ExportIterator extends User_Model_UserMapper implements Iterator
{
    private $_rows = array();
    private $_index = 0;

    public function setRows($rows)
    {
        $this->_rows = $rows;

        return $this;
    }

    public function current()
    {
        if ($this->valid()) {
            $row = $this->_rows[$this->_index];

            $row = $row->toArray();
            unset($row['user_password']);

            $user = new User_Model_User();
            $user->setOptions($this->_stripColumnPrefix($row, 'user_'));
            $this->_loadAttributes($user);

            return $user;
        } else {
            return null;
        }
    }

    public function key()
    {
        return $this->_index;
    }

    public function next()
    {
        $this->_index++;
    }

    public function rewind()
    {
        $this->_index = 0;
    }

    public function valid()
    {
        return isset($this->_rows[$this->_index]);
    }

}
