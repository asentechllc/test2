<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: User.php 2310 2011-03-31 14:37:32Z leigh $
 */

/**
 * Model class for users
 *
 * @category User
 * @package User_Model
 * @subpackage User
 */
class User_Model_User extends Mcmr_Model_Ordered
{
    static protected $_mapperclass = 'User_Model_UserMapper';
    protected $_id = null;
    protected $_password = null;
    protected $_username = null;
    protected $_email = null;
    protected $_emailconfirmed = false;
    protected $_emailchanged = false;
    protected $_lastlogin = null;
    protected $_salt = null;
    protected $_createdate = null;
    protected $_state = null;
    protected $_role = null;
    protected $_token = null;
    protected $_tokenexpire = null;
    protected $_firstname = null;
    protected $_surname = null;
    protected $_timezone = null;
    protected $_sites = null;

    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Function for getting the valid user states
     *
     * @return array
     */
    static public function validStates()
    {
        return array_keys(Zend_Registry::get('user-config')->user->staterole->toArray());
    }

    /**
     * @param $role the $role to set
     */
    public function setRole($role)
    {
        $this->_role = $role;

        return $this;
    }

    /**
     * @return the role
     */
    public function getRole()
    {
        if (null == $this->_role) {
            $this->_role = Zend_Registry::get('user-config')->user->defaultrole;
        }

        return $this->_role;
    }

    public function setId($_id)
    {
        $this->_id = $_id;

        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param $_status the $_status to set
     */
    public function setState($state)
    {
        // If we are changing the state, change the role according to config.
        if (null !== $this->_state) {
            $config = Zend_Registry::get('user-config')->user;
            if (!empty($config)) {
                $config = $config->staterole->toArray();

                if (isset($config[$state]) && !empty($config[$state])) {
                    if (is_array($config[$state])) {
                        if (isset($config[$state][$this->getRole()])) {
                            $this->setRole($config[$state][$this->getRole()]);
                        }
                    } else {
                        $this->setRole($config[$state]);
                    }
                }
            }
        }

        $this->_state = $state;

        return $this;
    }

    /**
     * @return the $_status
     */
    public function getState()
    {
        if (null === $this->_state) {
            $this->_state = Zend_Registry::get('user-config')->user->defaultstate;
        }
        return $this->_state;
    }

    public function setPassword($_password)
    {
        $this->_password = $_password;

        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->_password;
    }

    /**
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->_username;
    }

    /**
     *
     * @param string $username
     * @return User_Model_User 
     */
    public function setUsername($username)
    {
        $this->_username = $username;
        
        return $this;
    }

    public function getUsernameFromField() {
        $mapper = self::getMapper();
        $field = $mapper->usernameField();
        $fieldGetter = 'get'.ucfirst($field);
        return $this->$fieldGetter();
    }

    /**
     *
     * @param string $email
     * @return User_Model_User 
     * @throws User_Model_Exception_EmailAddress
     */
    public function setEmail($email)
    {
        $filter = new Zend_Filter_StringTrim();
        $email = $filter->filter($email);
        
        $validator = new Zend_Validate_EmailAddress();
        if (!$validator->isValid($email) && !empty($email)) {
            //throw new User_Model_Exception_EmailAddress("{$email} is not a valid email address");
        }
        
        if (null !== $this->_email && $this->_email !== $email) {
            $this->setEmailconfirmed(false);
            $this->_emailchanged = true;
        }
        $this->_email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmailconfirmed($confirmed)
    {
        // When the email is confirmed change the user's state and role from the defaults.
        if (!$this->_emailconfirmed && $confirmed && ($config = Zend_Registry::get('user-config'))) {
            $state = $config->onEmailconfirm->state;
            $role = $config->onEmailconfirm->role;

            if ('' !== $state && $this->_state == $config->user->defaultstate) {
                $this->setState($state);
            }

            if ('' !== $role && $this->_role == $config->user->defaultrole) {
                $this->setRole($role);
            }
        }

        $this->_emailconfirmed = (bool) $confirmed;

        return $this;
    }

    public function getEmailconfirmed()
    {
        if (null === $this->_emailconfirmed) {
            $this->_emailconfirmed = false;
        }

        return $this->_emailconfirmed;
    }

    /**
     * Returns true if the email address has been changed
     *
     * @param bool $reset Reset the flag. Only use on save
     * @return bool
     */
    public function emailChanged($reset = false)
    {
        $changed = $this->_emailchanged;
        if ($reset) {
            $this->_emailchanged = false;
        }

        return $changed;
    }

    public function setLastlogin($lastlogin)
    {
        $this->_lastlogin = intval($lastlogin);

        return $this;
    }

    public function getLastlogin()
    {
        if (null === $this->_lastlogin) {
            $this->_lastlogin = 0;
        }
        return $this->_lastlogin;
    }

    public function setCreatedate($createdate)
    {
        $this->_createdate = intval($createdate);

        return $this;
    }

    public function getCreatedate()
    {
        if (null === $this->_createdate) {
            $this->_createdate = time();
        }
        return $this->_createdate;
    }

    /**
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->_salt = $salt;

        return $this;
    }

    public function getSalt()
    {
        if (null == $this->_salt) {
            $this->_salt = Mcmr_StdLib::randomString(4, 8);
        }

        return $this->_salt;
    }

    public function getToken()
    {
        // If the token expire time is greater than now, but we have no token then we want to create a new one
        if ((null == $this->_token) && ($this->getTokenexpire() > time())) {
            $this->_token = Mcmr_StdLib::randomString(12, 16);
        } elseif ($this->getTokenexpire() < time()) {
            // A token is set, but expired. Unset it.
            $this->_token = null;
        }

        return $this->_token;
    }

    public function setToken($token)
    {
        $this->_token = $token;

        return $this;
    }

    public function getTokenexpire()
    {
        if (null == $this->_tokenexpire) {
            $this->_tokenexpire = 0;
        }

        return $this->_tokenexpire;
    }

    public function setTokenexpire($tokenexpire)
    {
        $this->_tokenexpire = intval($tokenexpire);

        return $this;
    }

    public function getFirstname()
    {
        return $this->_firstname;
    }

    public function setFirstname($firstname)
    {
        $this->_firstname = $firstname;

        return $this;
    }

    public function getSurname()
    {
        return $this->_surname;
    }

    public function setSurname($surname)
    {
        $this->_surname = $surname;

        return $this;
    }
	
	public function getFullname()
	{
		return $this->getFirstname().' '.$this->getSurname();
	}

	public function getUrl()
	{
		return Mcmr_StdLib::urlize($this->getFullname());
	}
	
	public function getSeoId(){
		return Mcmr_StdLib::seoId($this->getId());
	}
	
	public static function getRealId($seoId){
		return Mcmr_StdLib::realId($seoId);
	}
    

    /**
     * Get the user's timezone
     *
     * @return string
     */
    public function getTimezone() 
    {
        if (null === $this->_timezone) {
            $this->_timezone = Mcmr_Date::getDefaultTimezone();
        }
        
        return $this->_timezone;
    }

    /**
     * Set the user's timezone
     *
     * @param string $timezone
     * @return User_Model_User 
     */
    public function setTimezone($timezone) 
    {
        $this->_timezone = $timezone;
        
        return $this;
    }

    /**
     * Get the sites that the user is regeistered on.
     * The sites are returned as a comma separated string of site identifiers.
     *
     * @return string 
     */
    public function getSites() 
    {
        //if nothing is set then assume the article is published on the current site
        if (null === $this->_sites) {
            $site = Zend_Controller_Front::getInstance()->getRequest()->getSiteName();
            $this->_sites = array($site);
        }
        return $this->_sites;
    }
    
    
    /**
     * Set the sites that the user is registered on. 
     *
     */
    public function setSites($sites)
    {
        if (is_string($sites)) {
            $sites = explode(',', $sites);
        }
        $this->_sites = $sites;
    }

    public function getSocialProfileId($provider) 
    {
        return $this->getAttribute('auth_'.$provider);
    }

    public function setSocialProfileId($provider, $identifier)
    {
        return $this->setAttribute('auth_'.$provider, $identifier);
    }

    
    /**
     * Convert this object to an array. Ensure password is not returned
     *
     * @return array
     */
    public function toArray()
    {
        $values = parent::toArray();
        unset($values['password']);

        return $values;
    }

}
