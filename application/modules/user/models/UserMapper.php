<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: UserMapper.php 2094 2011-01-20 15:43:58Z leigh $
 */

/**
 * Mapper for User Model
 *
 * @category User
 * @package User_Model
 * @subpackage UserMapper
 */
class User_Model_UserMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'User_Model_DbTable_User';
    protected $_columnPrefix = 'user_';
    protected $_modelClass = 'User_Model_User';
    protected $_cacheIdPrefix = 'User';
    protected $_dbAttrTableClass = 'User_Model_DbTable_UserAttr';
    protected $_dbOrdrTableClass = 'User_Model_DbTable_UserOrdr';

    public function init()
    {
        $this->addObserverNamespace(array('prefix' => 'Mcmr_Model_Observer_', 'path'=>'Mcmr/Model/Observer'));
        $this->addObserverNamespace(array('prefix' => 'User_Model_Observer_', 'path'=>dirname(__FILE__) . '/Observer'));
        
        // Register the observers
        $this->registerObserver('Cache');
        $this->registerObserver('Changelog');
        $this->registerObserver('UsernameUpdate');
    }

    /**
     * Takes Zend_Db_Table rows and maps them to the user model.
     *
     * @param array $rows
     * @return array
     */
    public function filter($rows)
    {
        $users = array();
        foreach ($rows as $row) {
            // Use the mapper find. This is more taxing on first load. But afterwards every user will be cached.
            // This prevents a full DB hit if a single user is updated.
            $user = $this->find($row['user_id']);

            $users[] = $user;
        }

        return $users;
    }

    /**
     * Save a User_Model_User to the database
     *
     * @param Mcmr_Model_ModelAbstract $model
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        $options = $model->getOptions();
        unset($options['order']);
        $data = $this->_addColumnPrefix($options, 'user_');
        $data['user_password'] = new Zend_Db_Expr(
            $this->getDbtable()
            ->getAdapter()
            ->quoteInto(
                'MD5(CONCAT(\''
                . Zend_Registry::get('user-config')->user->basesalt
                . '\', ?, \''
                . $model->getSalt() . '\'))',
                $model->getPassword()
            )
        );
        $data['user_lastlogin'] = new Zend_Db_Expr("FROM_UNIXTIME({$data['user_lastlogin']})");
        $data['user_tokenexpire'] = new Zend_Db_Expr("FROM_UNIXTIME({$data['user_tokenexpire']})");
        $data['user_createdate'] = new Zend_Db_Expr("FROM_UNIXTIME({$data['user_createdate']})");
        $data['user_sites'] = implode(',', $data['user_sites']);

        // Insert or update the data in the database
        if (null === ($id = $model->getId())) {
            unset($data['id']);
            $id = $this->getDbTable()->insert($data);
            $model->setId($id);

            $this->_saveAttributes($model);
            $this->_saveOrder($model);

            $model->state(Mcmr_Model_ModelAbstract::STATE_CLEAN);

            // Notify observers
            $this->notify('insert', $model);
        } else {
            $this->notify('preUpdate', $this->find($model->getId()));
            // If the password is blank then we do not want to overwrite the database entry.
            $password = $model->getPassword();
            if (empty($password)) {
                unset($data['user_password']);
            }

            $this->getDbTable()->update($data, array('user_id=?' => $id));

            $this->_deleteAttributes(array('user_id=?' => $id));
            $this->_saveAttributes($model);
            $this->_saveOrder($model);

            $model->state(Mcmr_Model_ModelAbstract::STATE_CLEAN);

            // Notify observers
            $this->notify('update', $model);
        }

        return true;
    }

    protected function _sanitiseValues($data) 
    {
        unset($data['user_order']);
        return $data;
    }

    /**
     * Authenticate a user against the database - check credentials.
     *
     * @param array $credentials
     * @return boolean
     */
    public function authenticate($credentials = array())
    {
        $user = $this->findByUsername($credentials['username']);

        if (null !== $user) {

			# extra layer of security to avoid non admin/editor logins
			if(Mcmr_StdLib::isAdmin() && !in_array($user->getRole(),array('admin','editor'))){
				return null;
			}

            $auth = Zend_Auth::getInstance();
            
            $table = $this->getDbtable();
            if ($table instanceof Mcmr_Db_Table_Abstract) {
                $adapter = $table->getReadAdapter();
            } else {
                $adapter = $table->getAdapter();
            }
            
            $usernameField = $this->_sanitiseField($this->usernameField());
            $adapter = new Zend_Auth_Adapter_DbTable(
                $adapter,
                'user_users', $usernameField, 'user_password',
                "MD5(CONCAT('" . Zend_Registry::get('user-config')->user->basesalt . "', ?, user_salt))"
            );
            $adapter->setIdentity($credentials['username']);
            $adapter->setCredential($credentials['password']);

            $result = $auth->authenticate($adapter);

            if ($result->isValid()) {

                $remember = isset($credentials['remember']) && $credentials['remember'];
                $this->_authenticateUser($user, $remember);
                return $user;
            } else {
                $user = new User_Model_User();
                $user->setRole('guest');
                
                Zend_Registry::set('authenticated-user', $user);
                $user = null;
            }
        }

        return $user;
    }

    public function forceAuthenticate($user) {
        $this->_authenticateUser($user, false);
        Zend_Auth::getInstance()->getStorage()->write($user->getUsernameFromField());
    }

    /**
     * Authenticate given user without checking credentials
     */
    public function _authenticateUser($user, $remember=true) {
        if ($remember) {
            $config = Zend_Registry::get('user-config');
            // Default to 1 day
            $seconds = isset($config->user->rememberMe->time) ? $config->user->rememberMe->time
                        : 86400;
            Zend_Session::rememberMe($seconds);
        } else {
            Zend_Session::forgetMe();
        }
        Zend_Registry::set('authenticated-user', $user);
        $user->setLastlogin(time());
        $this->notify('login', $user);
        $this->save($user);
        return $user;        
    }

    public function logout()
    {
        $user = Zend_Registry::get('authenticated-user');

        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::forgetMe(); // Forget the session
        Zend_Registry::set('authenticated-user', new User_Model_User());

        $this->notify('logout', $user);

        return $this;
    }


    /**
     * Find the User_Model_User by username
     *
     * @param string $username
     * @return User_Model_User
     */
    public function findByUsername($username)
    {
        $field = $this->usernameField();
        return $this->findOneByField(array($field => $username));
    }

    public function findExportUsers($conditions)
    {
        $table = $this->getDbTable();
        $select = $table->select();
        $select->from($table, $this->_selectFields($select));
        if (is_array($conditions)) {
            foreach ($conditions as $field => $value) {
                if (0 === strncmp('attr_', $field, 5)) {
                    $fieldName = str_replace('attr_', '', $field);
                    if (is_array($value)) {
                        $condition = $value['condition'];
                        $value = $value['value'];
                    } else {
                        $condition = '=';
                    }

                    $this->_addAttributeCondition( $select, null, $fieldName, null, $value, $condition );
                } else {
                    $fieldName = $this->_sanitiseField($field);
                    $select = $this->_addCondition($select, $fieldName, $value);
                }
            }
        }

        $users = new User_Model_ExportIterator();
        $users->setRows($this->getDbTable()->fetchAll($select));

        return $users;
    }

    protected function _selectFields()
    {
        $fields = parent::_selectFields();
        $fields[] = "UNIX_TIMESTAMP(user_createdate) AS user_createdate";
        $fields[] = "UNIX_TIMESTAMP(user_lastlogin) AS user_lastlogin";
        $fields[] = "UNIX_TIMESTAMP(user_tokenexpire ) AS user_tokenexpire";
        return $fields;
    }
    
    protected function _convertRowToModel($row)
    {
        if (is_object($row) && $row instanceof Zend_Db_Table_Row) {
            $row = $row->toArray();
        }
        unset($row['user_password']);
        return parent::_convertRowToModel($row);
    }
    
    /**
     *
     * @param string $email
     * @return boolean
     */
    public function userEmailExists($model)
    {
        $checkUnique = true;
        if (isset(Zend_Registry::get('user-config')->email->unique->required->{$model->getRole()})) {
            $checkUnique = Zend_Registry::get('user-config')->email->unique->required->{$model->getRole()};
        }

        return $checkUnique && $this->emailExists($model->getEmail());
    }

    public function emailExists($email)
    {
        $userWithEmail=$this->findOneByField(array('email'=>$email));
        return null !== $userWithEmail;   
    }


    public function usernameField()
    {
        $field = 'email';
        
        $config = Zend_Registry::get('user-config');
        if (isset($config->authentication) && isset($config->authentication->usernameField)) {
            $field = $config->authentication->usernameField;
        }
        
        return $field;
    }

/*
    protected function _postOneOrder($select) {
        print_r((string)$select);
        return $select;
    }
*/
    

    public function findOneBySocialProfile($profile) 
    {
        $attrName = 'attr_auth_'.$profile->provider;
        $attrValue = $profile->identifier;
        $user = $this->findOneByField(array($attrName=>$attrValue));
        return $user;
    }

    public function createFromSocialProfile($profile) 
    {
        $user = new User_Model_User();
        $config = Zend_Registry::get('user-config');
        $user->setRole($config->user->defaultRegisteredRole);
        if (isset($profile->email)) {
            $user->setEmail($profile->email);
        }

        if (isset($profile->firstName)) {
            $user->setFirstname($profile->firstName);
        }

        if (isset($profile->lastName)) {
            $user->setSurname($profile->lastName);
        }

        if (isset($profile->photoURL)) {
            $user->setAttribute('image', $profile->photoURL);
        }

        $user->setAttribute('incomplete', 1);
        $user->setSocialProfileId($profile->provider, $profile->identifier);
        return $user;
    }

}
