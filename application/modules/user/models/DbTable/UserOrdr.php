<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: UserAttr.php 718 2010-06-01 13:33:37Z leigh $
 */

/**
 * Description of UserAttr
 *
 * @category User
 * @package User_Model
 * @subpackage DbTable
 */
class User_Model_DbTable_UserOrdr extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'user_users_ordr';
    protected $_primary = array('user_id', 'ordr_name');
    protected $_rowData = array();

    public function insert(array $data=null)
    {
        if (null === $data) {
            parent::insert($this->_rowData);
        } else {
            parent::insert($data);
        }
    }

    public function setOrdrName($name)
    {
        $this->_rowData['ordr_name'] = $name;

        return $this;
    }

    public function setOrdrValue($value)
    {
        $this->_rowData['ordr_value'] = (int)$value;

        return $this;
    }

    public function setOrdrId($id)
    {
        $this->_rowData['user_id'] = $id;

        return $this;
    }
}
