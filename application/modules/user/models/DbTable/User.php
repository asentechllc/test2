<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category User
 * @package User_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: User.php 645 2010-05-12 14:54:06Z leigh $
 */

/**
 * Description of User
 *
 * @category User
 * @package User_Model
 * @subpackage DbTable
 */
class User_Model_DbTable_User extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'user_users';
    protected $_primary = 'user_id';
}
