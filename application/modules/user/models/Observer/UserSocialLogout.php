<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of AdestraUserSubscribe
 *
 * @category Mcmr
 */
class User_Model_Observer_UserSocialLogout extends Mcmr_Model_ObserverAbstract
{
    public function logout($user) {
        $config = Zend_Registry::get('user-config')->hybridauth;
        if ($config) {
            $siteName = Zend_Controller_Front::getInstance()->getRequest()->getSitename();  
        	$hybridauth = Mcmr_Service_Hybrid_Auth::getInstance();
        	foreach ($config->{$siteName}->providers as $name => $provider ) {
        		try {
                    Mcmr_Debug::dump("Logging out provider: $name");
		    		$adapter = $hybridauth->getAdapter($name); 
        			$adapter->logout();
        		} catch (Exception $e) {
                    Mcmr_Debug::dump($e->getMessage());
        		}
        	}
        }
    }
}
