<?php

/*
 * McCormack & Morrison Gossip 2 CMS System
 */

/**
 * Description of AdestraUserSubscribe
 *
 * @category Mcmr
 */
class User_Model_Observer_Auth extends Mcmr_Model_ObserverAbstract
{
    public function logout($user) {
        $config = Zend_Registry::get('user-config')->hybridauth;
        if ($config) {
        	$hybridauth = Mcmr_Service_Hybrid_Auth::getInstance();
        	foreach ($config->providers as $name => $provider ) {
        		try {
		    		$adapter = $hybridauth->getAdapter($name); 
        			$adapter->logout();
        		} catch (Excpetion $e) {
        		}
        	}
        }
    }
}
