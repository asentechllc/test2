<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category News
 * @package News_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Feature.php 1752 2010-11-10 16:12:40Z leigh $
 */

/**
 * Form class for Ads
 */
class Misc_Form_Ads extends Mcmr_Form
{
    public function __construct($options = null)
    {
        parent::__construct($options);
    }

    public function init()
    {
        $this->setName('adsform')->setElementsBelongTo('ads-form');
        $this->setMethod('post');
        $this->setAttrib('enctype', 'multipart/form-data');
    }

}
