<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Default
 * @package Default_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2002 2011-01-04 00:03:45Z leigh $
 */

/**
 * Default_IndexController
 *
 * The Purpose of this controller is to do nothing but forward the request onto the real default page
 * as determined by the configuration
 *
 * @category Default
 * @package Default_IndexController
 */
class Misc_IndexController extends Zend_Controller_Action
{

	public function indexAction(){
		$this->forward('ads');
	}

    /**
     * Advert settings
     **/
    public function adsAction(){
    	$this->_settings('Ads','ads');
	}
	
	function titlesAction(){
    	$this->_settings('PageTitles','meta-data');
	}
	
	private function _settings($formName,$settingGroup){

		$form = $this->_helper->loadForm($formName);
		$this->view->form = $form;

		// Populate the form from request variables
		$form->populate($this->_request->getParams());

		if ($this->_request->isPost()) {
		
			$post = $this->_request->getPost();
		
			if ($form->isValid($post)) {
				$values = $form->getValues(true);
				
				try {
					Mcmr_Settings::set($settingGroup,$values,true);
					$this->view->error = false;
					$this->view->getHelper('DisplayMessages')->addMessage('Settings Saved', 'info');
					$this->view->getHelper('Redirect')->notify('update');
				} catch (Exception $e) {
					$this->view->error = true;
					$this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
				}
			} else {
				$this->view->error = true;
				$this->view->getHelper('DisplayMessages')->addMessage('Incorrect Form Values', 'warn');
			}
		} else {
			$values = Mcmr_Settings::get($settingGroup);
			$form->populate($values);
		}
	}
	
	
}