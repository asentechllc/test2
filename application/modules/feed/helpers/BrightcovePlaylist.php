<?php

/**
 * Load a brightcove playlist identified by conditision.
 * playlist_id needs to be passed in conditions
 *
 * @author Michal
 */
class Feed_View_Helper_BrightcovePlaylist extends Zend_View_Helper_Abstract
{
    public function brightcovePlaylist($conditions = null )
    {
        $mapper = Feed_Model_Brightcove_Playlist::getMapper();
        return $mapper->findOneByField($conditions);
    }
}
