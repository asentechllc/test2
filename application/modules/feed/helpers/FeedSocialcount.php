<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of feedRead
 *
 * @author leigh
 */
class Feed_View_Helper_FeedSocialcount extends Zend_View_Helper_Abstract
{
    public function feedSocialcount($conditions)
    {
    	if (!is_array($conditions)) {
    		$conditions = array('url'=>$conditions);
    	}
        
        $mapper = Feed_Model_Socialcount::getMapper();
        return $mapper->findOneByField($conditions);
    }
}
