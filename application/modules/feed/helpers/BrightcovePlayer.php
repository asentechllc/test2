<?php


/**
 *
 * Show a brightcove player for a video from a playlist
 * @author Michal
 */
class Feed_View_Helper_BrightcovePlayer extends Zend_View_Helper_Abstract
{
    public function brightcovePlayer( $conditions, $partial )
    {
        $videoId = $conditions[ 'video_id' ];
        $config = Zend_Registry::get('feed-config');
        $playerId = (isset($conditions['player_id']))
            ? $conditions[ 'player_id' ] : $config->brightcove->playerId;
        $playerKey = (isset($conditions['player_key']))
            ? $conditions[ 'player_key' ] : $config->brightcove->playerKey;
        
        return $this->view->partial(
            $partial,
            array('playerId'=>$playerId, 'playerKey'=>$playerKey, 'videoId'=>$videoId)
        );
    }
}
