<?php

/**
 * Load a brightcove playlist identified by conditision.
 * playlist_id needs to be passed in conditions
 *
 * @author Michal
 */
class Feed_View_Helper_TwitterRead extends Zend_View_Helper_Abstract
{
    public function twitterRead($conditions = null )
    {
        $mapper = Feed_Model_Twitter_Tweets::getMapper();
        $feed = $mapper->findOneByField($conditions);
        return $feed;
    }
}
