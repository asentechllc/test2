<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of feedRead
 *
 * @author leigh
 */
class Feed_View_Helper_FeedRead extends Zend_View_Helper_Abstract
{
    public function feedRead($conditions = null)
    {
        $mapper = Feed_Model_Feed::getMapper();
        
        return $mapper->findOneByField($conditions);
    }
}
