<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of feedRead
 *
 * @author leigh
 */
class Feed_View_Helper_FeedSocialshare extends Zend_View_Helper_Abstract
{
    public function feedSocialshare($network, $permalink, $title = '', $excerpt = '') 
    {        
	    switch($network) {			
			case 'ycombinator':
				$url = 'http://news.ycombinator.com/submitlink?'.http_build_query(array(
	            	'op' => 'basic',
	                'u' => $permalink,
	                't' => $title));
			break;
			
	        case 'instapaper':
	            $url = 'http://www.instapaper.com/hello2?'.http_build_query(array(
	                'url' => $permalink,
	                'title' => $title,
	                'description' => $excerpt));
	        break;

	        case 'reddit':
	            $url = 'http://www.reddit.com/submit?'.http_build_query(array(
	                'url' => $permalink,
	                'title' => $title));
	        break;
		
	        case 'readitlater':
	            $url = 'https://readitlaterlist.com/save?'.http_build_query(array(
	                'url' => $permalink,
	                'title' => $title));
	        break;
	        
	        case 'facebook':
	        	$url = 'https://www.facebook.com/sharer/sharer.php?'.http_build_query(array(
	                'u' => $permalink));            
	        break;
	        
	        case 'google':
	        	$url = 'https://plus.google.com/share?url='.$permalink;
	        break;
	        
	        case 'twitter':
	        	$url = 'https://twitter.com/intent/tweet?'.http_build_query(array(
	                'text' => $title.' '.$permalink));
	        break;
	        
	        case 'linkedin':
	        	$url = 'http://www.linkedin.com/shareArticle?'.http_build_query(array(
	        		'mini' => 'true',
	                'url' => $permalink,
	                'title' => $title,
	                'summary' => $excerpt));
	        break;
	                
	        case 'digg':
				$url = 'http://digg.com/submit?'.http_build_query(array(
	                'url' => $permalink,
	                'title' => $title));
	        break;
	        case 'stumbleupon':
	        	$url = 'http://www.stumbleupon.com/submit?'.http_build_query(array(
	                'url' => $permalink,
	                'title' => $title));
	        break;
	       
	    }
	    return $url;
	}
}
