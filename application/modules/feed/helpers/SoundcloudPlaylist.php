<?php

/**
 * Load a soundcloud playlist identified by conditision.
 * playlist_id needs to be passed in conditions
 *
 * @author Michal
 */
class Feed_View_Helper_SoundcloudPlaylist extends Zend_View_Helper_Abstract
{
    public function soundcloudPlaylist($conditions = null )
    {
        $mapper = Feed_Model_Soundcloud_Playlist::getMapper();
        return $mapper->findOneByField($conditions);
    }
}
