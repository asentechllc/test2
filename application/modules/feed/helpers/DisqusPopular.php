<?php

/**
 * Load a soundcloud playlist identified by conditision.
 * playlist_id needs to be passed in conditions
 *
 * @author Michal
 */
class Feed_View_Helper_DisqusPopular extends Zend_View_Helper_Abstract
{
    public function disqusPopular($conditions = null )
    {
        $mapper = Feed_Model_Disqus_Forum::getMapper();
        return $mapper->findPopular($conditions);
    }
}
