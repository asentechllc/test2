<?php

/**
 * Load a soundcloud playlist identified by conditision.
 * playlist_id needs to be passed in conditions
 *
 * @author Michal
 */
class Feed_View_Helper_DisqusThread extends Zend_View_Helper_Abstract
{
    public function disqusThread($conditions = null)
    {
        $mapper = Feed_Model_Disqus_Thread::getMapper();
        return $mapper->findOneByField($conditions);
    }
    
}
