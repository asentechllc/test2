<?php
/**
 * Description of feedRead
 *
 * @author leigh
 */
class Feed_View_Helper_FeedEntryRead extends Zend_View_Helper_Abstract
{
    public function feedEntryRead($conditions = null, $order=null, $page=null)
    {
        $mapper = Feed_Model_Entry::getMapper();
        
        return $mapper->findAllByField($conditions, $order, $page);
    }
}
