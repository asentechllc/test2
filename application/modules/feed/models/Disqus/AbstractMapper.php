<?php

class Feed_Model_Disqus_AbstractMapper extends Feed_Model_FeedMapper
{

    public static function getApiSecret() {
        $config = Zend_Registry::get('feed-config');
        return $config->disqus->api_secret; 
    }

}
