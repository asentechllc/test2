<?php

class Feed_Model_Disqus_ForumMapper extends Feed_Model_Disqus_AbstractMapper
{
    protected $_feedClass = 'Feed_Model_Disqus_Forum';
    protected $_entryClass = 'Feed_Model_Disqus_Thread';

    public function findOneByField($condition = null)
    {
        throw new Mcmr_Model_Exception('No find one for threads');
    }

    public function findAllByField($condition = null, $order = null, $page = 1)
    {
        throw new Mcmr_Model_Exception('No find all for threads');
    }

    public function findPopular($condition = null)
    {
        if (null === $condition['forum']) {
            throw new Mcmr_Model_Exception("Forum id required");
        }
        $url = $this->getListPopularUrl($condition);
        $res = $this->_fetchAndCache(null, $url);
        return $res;
    }

    public static function getListPopularUrl($condition)
    {
        $forum = $condition['forum'];
        $apiSecret = parent::getApiSecret();
        $url = "http://disqus.com/api/3.0/threads/listPopular.rss?forum={$forum}&api_secret={$apiSecret}";
        return $url;
    }

    protected function _populateFeed($feed, $channel) {
        $feed->setTitle((string)$channel->title);
        $feed->setLink((string)$channel->link);
        $feed->setDescription((string)$channel->description);
    }
    
}
