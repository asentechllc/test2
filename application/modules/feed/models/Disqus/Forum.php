<?php
/**
 * 
 */
class Feed_Model_Disqus_Forum extends Feed_Model_Feed
{
    static protected $_mapperclass = 'Feed_Model_Disqus_ForumMapper';

    public function __construct() {
        parent::__construct();
        $this->_persist = false;
    }

    /**
     * Return mapper for model
     *
     * @return News_Model_FeedMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

}
