<?php

/**
 * 
 */
class Feed_Model_Disqus_Thread extends Feed_Model_Entry
{
    static protected $_mapperclass = 'Feed_Model_Disqus_ThreadMapper';
    private $_commentscount;
    private $_reactionscount;

    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }
    
    public function getCommentsCount() {
        return $this->_commentscount;
    }
    
    public function setCommentsCount($count) {
        $this->_commentscount = $count;
        return $this;
    }

    public function getReactionsCount() {
        return $this->_reactionscount;
    }
    
    public function setReactionsCount($count) {
        $this->_reactionscount = $count;
        return $this;
    }
    

}
