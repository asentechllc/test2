<?php

class Feed_Model_Disqus_ThreadMapper extends Feed_Model_EntryMapper
{

    public function xmlToModel(SimpleXMLElement $xml)
    {
        $entry = new Feed_Model_Disqus_Thread();
        $entry->setGuid(trim((string) $xml->guid));
        $entry->setTitle(trim((string) $xml->title));
        $entry->setLink(trim((string) $xml->link));
        $entry->setPubDate(strtotime(trim((string)$xml->pubDate)));
        $slash = $xml->children("http://purl.org/rss/1.0/modules/slash/");
        $entry->setCommentsCount(trim((string)$slash->comments));
        return $entry;
    }

    public function findOneByField($condition = null)
    {
        $conditionString = serialize($condition);
        $cacheid = 'Feed_findOne_'.md5($conditionString);
        $tags =  array('feed_model_feed_'.md5($conditionString));
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        if (false == ($thread = $cache->load($cacheid))) {
            $thread = $this->_loadOneByField($condition);
            $cache->save($thread, $cacheid, $tags, $this->_specificLifetime);
            $this->notify('cacheMiss', $thread);
        }
        // Notify observers of a load
        $this->notify('load', $thread);
        return $thread;
    }

    protected function _loadOneByField($condition)
    {
        $thredDetailsUrl = $this->getThreadDetailsUrl($condition);
        $client = new Zend_Http_Client($thredDetailsUrl);
        $response = null;
        try {
            $response = $client->request();
            $decodedResponse = json_decode($response->getBody());
        } catch ( Exception $e ) {
            Mcmr_Debug::dump("Exception caught in Disqus request. Probably quota issue.");
            Mcmr_Debug::dump($e->getMessage());
        }
        return $this->_processResponse($decodedResponse);
    }

    protected function _processResponse($response)
    {
        $thread = new Feed_Model_Disqus_Thread();
        if ($response && !$response->code) {
            $thread->setCommentsCount($response->response->posts);
            $thread->setReactionsCount($response->response->reactions);
        }
        return $thread;
    }

    public static function getThreadDetailsUrl($condition)
    {
        $forum = $condition['forum'];
        $link = $condition['link'];
        $apiSecret = static::getApiSecret();
        $url = "http://disqus.com/api/3.0/threads/details.json?forum={$forum}&api_secret={$apiSecret}&thread=link:{$link}";
        return $url;
    }

    public static function getApiSecret() {
        $config = Zend_Registry::get('feed-config');
        return $config->disqus->api_secret; 
    }


}
