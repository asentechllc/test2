<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Feed
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all feed entries
 *
 * @category   Feed
 * @package    Model
 * @subpackage Entry
 */
class Feed_Model_Entry extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Feed_Model_EntryMapper';

    protected $_id = null;
    protected $_feedname = null;
    protected $_guid = null;
    protected $_title = null;
    protected $_link = null;
    protected $_pubDate = null;
    protected $_description = null;
    
    /**
     * Return mapper for model
     *
     * @return News_Model_EntryMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }
    
    /**
     * Get the entry's unique ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the entry's unique ID
     *
     * @param int $id
     * @return Feed_Model_Feed
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the name ID of this feed
     *
     * @return string
     */
    public function getFeedname()
    {
        return $this->_feedname;
    }

    /**
     * Set the name ID of this feed
     *
     * @param string $feedname
     * @return Feed_Model_Entry
     */
    public function setFeedname($feedname)
    {
        $this->_feedname = $feedname;

        return $this;
    }


    /**
     * Fetch the item GUID
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->_guid;
    }

    /**
     * The string GUID for this rss feed item
     *
     * @param string $guid
     * @return Feed_Model_Entry
     */
    public function setGuid($guid)
    {
        $this->_guid = $guid;

        return $this;
    }


    /**
     * Get the feed entry title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the feed entry title
     *
     * @param string $title
     * @return Feed_Model_Feed
     */
    public function setTitle($title)
    {
        $this->_title = (string)$title;

        return $this;
    }

    /**
     * Get the Feed entry link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->_link;
    }

    /**
     * Set the feed entry link
     *
     * @param string $link
     * @return Feed_Model_Feed
     */
    public function setLink($link)
    {
        $this->_link = (string)$link;

        return $this;
    }

    /**
     * Get the feed entry publish date
     *
     * @return string
     */
    public function getPubDate()
    {
        return $this->_pubDate;
    }

    /**
     * Set the feed entry publish date
     *
     * @param int $publishdate
     * @return Feed_Model_Feed
     */
    public function setPubDate($publishdate)
    {
        $this->_pubDate = (int)$publishdate;

        return $this;
    }

    /**
     * Get the feed entry description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the feed entry description
     *
     * @param string $description
     * @return Feed_Model_Feed
     */
    public function setDescription($description)
    {
        $this->_description = (string)$description;

        return $this;
    }
}
