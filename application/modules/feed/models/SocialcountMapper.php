<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Feed
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for Social counts / sharing links
 * Port of this:
 * https://github.com/thenextweb/TNW-Social-Count
 *
 * @category   Feed
 * @package    Model
 * @subpackage SocialcountMapper
 */
class Feed_Model_SocialcountMapper extends Mcmr_Model_MapperAbstract
{

    protected $_feedClass = 'Feed_Model_Socialcount';
    protected $_specificLifetime = false;
    protected $_services = null;

    public function  __construct()
    {
        parent::__construct();
        $config = Zend_Registry::get('feed-config');
        $this->_specificLifetime = (isset($config->socialcount) && isset($config->socialcount->cacheTime))
                    ? $config->socialcount->cacheTime : false; 
        $this->_services = $config->socialcount->services->toArray();
    }

    /**
     * Feeds cannot be saved. This function always throws an exception
     * 
     * @throws Mcmr_Model_Exception
     * @param Mcmr_Model_ModelAbstract $model 
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        throw new Mcmr_Model_Exception('Social counts cannot be saved, only loaded');
    }

    /**
     * Feeds cannot be deleted. This function always throws an exception
     *
     * @throws Mcmr_Model_Exception
     * @param Mcmr_Model_ModelAbstract $model 
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        throw new Mcmr_Model_Exception('Social counts cannot be deleted, only loaded');
    }

    /**
     * @see Mcmr_Model_MapperAbstract::findOneByField()
     */
    public function findOneByField($condition=null, $order=null, $page=null)
    {
        if (isset($condition['url'])) {
            $url = $condition['url'];
            return $this->_fetchAndCache($url);
        } 
        throw new Mcmr_Model_Exception('URL must be provided');
    }

    /**
     * @see Mcmr_Model_MapperAbstract::findAllByField()
     */
    public function findAllByField($condition = null, $order = null, $page = 1)
    {
        throw new Mcmr_Model_Exception('Social counts can only be loaded by one');
    }
    
    /**
     * Fetch a feed and cache the results.
     *
     * @param string $id
     * @param string $url
     * @return Feed_Model_Feed 
     */
    protected function _fetchAndCache($url)
    {
        $cacheid = 'Feed_Socialcount_findOne_'.md5($url);
        $tags =  array('feed_model_socialcount', 'feed_model_socialcount_'.md5($url));
        $cache = Zend_Registry::get('cachemanager')->getCache('database');
        if (false == ($model = $cache->load($cacheid))) {
            $model = $this->_createModel($url);
            $cache->save($model, $cacheid, $tags, $this->_specificLifetime);
            $this->notify('cacheMiss', $model);
        } else {
        }
        // Notify observers of a load
        $this->notify('load', $model);
        return $model;
    }


    /**
     * Build a model by fetching counts and creating share links for the url 
     * @param string $id
     * @param string $xml
     * @return Feed_Model_Feed 
     */
    protected function _createModel($permalink)
    {
        $counts = array();
        $shareUrls = array();
        foreach($this->_services as $service_name => $service_config) {
            $enabled = $service_config['enabled'];
            if( $enabled ) {
                $count = $this->_tnwscGetCount($permalink, $service_name);
                $counts[$service_name]=$count;
            }
        }
        $model = new Feed_Model_Socialcount();
        $model->setCounts($counts);
        return $model;
    }

    
    protected function _tnwscGetCount( $permalink, $service ) 
    {
        global $tnwsc_config;
        
        if( $service === 'google' ) {
            return $this->_tnwscDoCurl( $permalink, $service );
        } else {
            // If there are extra params in the url, append them to the permalink first
            if( isset( $this->_services[$service]["params"] ) ) {
                $permalink = sprintf( $this->_services[$service]["params"], $permalink );
            }
            $url = sprintf( $this->_services[$service]["url"], urlencode ( $permalink ) );
            return $this->_tnwscDoCurl( $url, $service );
        }
    }

    protected function _tnwscDoCurl($url, $service) 
    {
        $social_count = 0;
        
        // Google+ is an special, hack-ish case
        if( $service == 'google' ) 
        {
            // GET +1s. Credits to Tom Anthony: http://www.tomanthony.co.uk/blog/google_plus_one_button_seo_count_api/
            $curl = curl_init();
            curl_setopt( $curl, CURLOPT_URL, "https://clients6.google.com/rpc" );
            curl_setopt( $curl, CURLOPT_POST, 1 );
            curl_setopt( $curl, CURLOPT_POSTFIELDS, '[{"method":"pos.plusones.get","id":"p","params":{"nolog":true,"id":"' . $url . '","source":"widget","userId":"@viewer","groupId":"@self"},"jsonrpc":"2.0","key":"p","apiVersion":"v1"}]' );
            curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $curl, CURLOPT_HTTPHEADER, array( 'Content-type: application/json' ) );
            $curl_results = curl_exec ( $curl );
            curl_close ( $curl );
         
            $json = json_decode($curl_results, true);
            if ( isset($json[0]) && isset($json[0]['result']) && isset($json[0]['result']['metadata']) 
                && isset($json[0]['result']['metadata']['globalCounts']) && isset($json[0]['result']['metadata']['globalCounts']['count'])) {
                $social_count = intval( $json[0]['result']['metadata']['globalCounts']['count'] );
            } else {
                $social_count = 0;
            }
            
        } else {
            $ch = curl_init();
            curl_setopt ($ch, CURLOPT_URL, $url);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            $return = curl_exec( $ch );
        
            if( curl_errno( $ch ) ) { 
                $error = print_r( curl_error( $ch ), true );
                Mcmr_Debug::dump("Social Count Error");
                Mcmr_Debug::dump($error);
            } else {
                switch( $service ) {
                    case 'facebook':
                        $return = json_decode( $return, true );
                        $social_count = ( isset( $return['data'][0]['total_count'] ) ) ? $return['data'][0]['total_count'] : 0;
                        // TODO: Better handling of errors  
                        if( isset( $return['error'] ) ) { 
                            Mcmr_Debug::dump("Social Count Error");
                            Mcmr_Debug::dump($return['error']);
                        }
                    break;
                    
                    case 'twitter':
                        $return = json_decode( $return, true );
                        $social_count = ( isset($return['count'] ) ) ? $return['count'] : 0;
                    break;
                    
                    case 'linkedin':
                        $return = json_decode( str_replace( 'IN.Tags.Share.handleCount(', '', str_replace( ');', '', $return ) ), true );
                        $social_count = ( isset( $return['count'] ) ) ? $return['count'] : 0;
                    break;
                }
            }
        }
        return intval( $social_count );
    }

}
