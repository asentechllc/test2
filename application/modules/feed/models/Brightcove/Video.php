<?php

/**
 * 
 */
class Feed_Model_Brightcove_Video extends Feed_Model_Entry
{
    static protected $_mapperclass = 'Feed_Model_Brightcove_VideoMapper';
    protected $_thumbnail;
    protected $_thumbnails;

    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    public function setThumbnails($thumbs)
    {
        $this->_thumbnails = $thumbs;
        return $this;
    }

    public function getThumbnails()
    {
        return $this->_thumbnails;
    }

    public function getThumbnail($condition=null)
    {
        if (null === $condition) {
            $smallest = null;
            foreach ($this->_thumbnails as $thumb) {
                if (null === $smallest || $thumb['width'] < $smallest['width']) {
                    $smallest = $thumb;
                }
            }
            return $smallest;
        } else {
            foreach ($this->_thumbnails as $thumb) {
                $match = true;
                foreach ($condition as $key => $value) {
                    if ($thumb[$key] != $value) {
                        $match = false;
                        break;
                    }
                }
                if ($match) {
                    return $thumb;
                }
            }
        }
    }

}
