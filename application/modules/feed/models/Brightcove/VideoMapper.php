<?php

class Feed_Model_Brightcove_VideoMapper extends Feed_Model_EntryMapper
{

    public function xmlToModel(SimpleXMLElement $xml)
    {
        $entry = new Feed_Model_Brightcove_Video();
        $entry->setGuid((string) $xml->guid);
        $entry->setTitle((string) $xml->title);
        $entry->setLink((string) $xml->link);
        $entry->setPubDate(strtotime($xml->pubDate));
        $entry->setDescription((string) $xml->description);
        $bcChildren = $xml->children('http://www.brightcove.tv/link');
        $id = (string) $bcChildren->titleid;
        $entry->setId($id);
        $mediaChildren = $xml->children('http://search.yahoo.com/mrss/');
        $thumbnails = array();
        foreach ($mediaChildren as $mc) {
            if ('thumbnail' == (string) $mc->getName()) {
                $attrs = $mc->attributes();
                $thumb = array(
                        'height' => (string) $attrs['height'],
                        'width' => (string) $attrs['width'],
                        'url' => (string) $attrs['url'],
                );
                $thumbnails[] = $thumb;
            }
        }
        $entry->setThumbnails($thumbnails);
        return $entry;
    }

}
