<?php

class Feed_Model_Brightcove_PlaylistMapper extends Feed_Model_FeedMapper
{
    protected $_feedClass = 'Feed_Model_Brightcove_Playlist';
    protected $_entryClass = 'Feed_Model_Brightcove_Video';

    public function findOneByField($condition = null)
    {
        if (null === $condition['playlist_id']) {
            throw new Mcmr_Model_Exception("Playlist id required");
        }
        $url = $this->getPlaylistUrl($condition);
        $res = $this->_fetchAndCache($condition['playlist_id'], $url);
        
        return $res;
    }

    public function findAllByField($condition = null, $order = null, $page = 1)
    {
        throw new Mcmr_Model_Exception('No find all for brightcove');
    }

    public static function getPlaylistUrl($condition)
    {
        $playlistId = $condition['playlist_id'];
        $url = "http://link.brightcove.com/services/mrss/playlist{$playlistId}";
        
        return $url;
    }

}
