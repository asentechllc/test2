<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Feed
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for all feed entry models
 *
 * @category   Feed
 * @package    Model
 * @subpackage EntryMapper
 */
class Feed_Model_EntryMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Feed_Model_DbTable_Entry';
    protected $_columnPrefix = 'entry_';
    protected $_modelClass = 'Feed_Model_Entry';
    protected $_cacheIdPrefix = 'FeedEntry';
    protected $_specificLifetime = false;

    public function  __construct()
    {
        parent::__construct();
        $config = Zend_Registry::get('feed-config');
        $this->_specificLifetime = (isset($config->cacheTime))?$config->cacheTime:false;
    }


    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseField()
     */
    protected function _sanitiseField($field)
    {
        switch ($field) {
            case 'entry_id':
            case 'entryid':
            case 'entry':
                $field = 'entry_id';
                break;

            case 'feed':
            case 'feedname':
            case 'feed_name':
                $field = 'feed_name';
                break;

            default:
                $field = $this->_columnPrefix.$field;
                break;
        }
        
        return $field;
    }

    /**
     * Convert an XML entry to a model object
     *
     * @param SimpleXMLElement $item
     * @return Feed_Model_Entry 
     */
    public function xmlToModel(SimpleXMLElement $item)
    {
        $entry = $this->findOneByField(array('guid'=>$item->guid));

        if (null === $entry) {
            $entry = new Feed_Model_Entry();

            $entry->setGuid((string)$item->guid);
            $entry->setTitle((string)$item->title);
            $entry->setLink((string)$item->link);
            $entry->setPubDate(strtotime($item->pubDate));
            $entry->setDescription((string)$item->description);
        }
        
        return $entry;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseValue()
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'entry_pubDate':
                if (!empty($value)) {
                    $value = new Zend_Db_Expr("FROM_UNIXTIME({$value})");
                }
                break;
        }

        return $value;
    }

    /**
     * @see Mcmr_Model_GenericMapper::_selectFields()
     */
    protected function _selectFields()
    {
        $all = parent::_selectFields();
        $all[] = "UNIX_TIMESTAMP(entry_pubDate) AS entry_pubDate";

        return $all;
    }
}
