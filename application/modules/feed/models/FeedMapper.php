<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Feed
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for feed models
 *
 * @category   Feed
 * @package    Model
 * @subpackage FeedMapper
 */
class Feed_Model_FeedMapper extends Mcmr_Model_MapperAbstract
{

    protected $_feedClass = 'Feed_Model_Feed';
    protected $_entryClass = 'Feed_Model_Entry';
    protected $_specificLifetime = false;

    public function  __construct()
    {
        parent::__construct();
        $config = Zend_Registry::get('feed-config');
        $this->_specificLifetime = (isset($config->cacheTime))?$config->cacheTime:false;
    }

    /**
     * Feeds cannot be saved. This function always throws an exception
     * 
     * @throws Mcmr_Model_Exception
     * @param Mcmr_Model_ModelAbstract $model 
     */
    public function save(Mcmr_Model_ModelAbstract $model)
    {
        throw new Mcmr_Model_Exception('Feed models cannot be saved, only loaded');
    }

    /**
     * Feeds cannot be deleted. This function always throws an exception
     *
     * @throws Mcmr_Model_Exception
     * @param Mcmr_Model_ModelAbstract $model 
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        throw new Mcmr_Model_Exception('Feed models cannot be deleted, only loaded');
    }

    /**
     * @see Mcmr_Model_MapperAbstract::findOneByField()
     */
    public function findOneByField($condition = null, $order=null, $page=1)
    {
        if (isset($condition['id'])) {
            $config = Zend_Registry::get('feed-config');
            $id = $condition['id'];
            if (!isset($config->feeds->$id)) {
                throw new Mcmr_Model_Exception("Feed '{$id}' not found in configuration");
            }
            $url = $config->feeds->$id->url;
            return $this->_fetchAndCache($id, $url, $page);
        } 

        if (isset($condition['url'])) {
            $url = $condition['url'];
            return $this->_fetchAndCache($url, $url, $page);
        } 

        throw new Mcmr_Model_Exception('Feed ID or URL must be provided');
    }

    /**
     * @see Mcmr_Model_MapperAbstract::findAllByField()
     */
    public function findAllByField($condition = null, $order = null, $page = 1)
    {
        $cacheid = 'Feed_findAll';
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($paginator = $cache->load($cacheid))) {
            $config = Zend_Registry::get('feed-config');
            $feeds = array();
            foreach ($config->feeds as $id=>$feed) {
                $feeds[] = $this->findOneByField(array('id'=>$id));
            }

            $paginator = new Mcmr_Paginator(new Zend_Paginator_Adapter_Array($feeds));
            $cache->save($paginator, $cacheid);
            $this->notify('cacheMiss', $paginator);
        }

        $paginator->setCurrentPageNumber($page);
        // Notify observers of a paginator load
        $this->notify('load', $paginator);
        
        return $paginator;
    }
    
    /**
     * Fetch a feed and cache the results.
     *
     * @param string $id
     * @param string $url
     * @return Feed_Model_Feed 
     */
    protected function _fetchAndCache($id, $url)
    {
        $cacheid = 'Feed_findOne_'.md5($url);
        $tags =  array('feed_model_feed_'.md5($url));
        $cache = Zend_Registry::get('cachemanager')->getCache('database');

        if (false == ($feed = $cache->load($cacheid))) {
            // @TODO Wrap in circuit breaker
            try {
                $xmlFeed = $this->_fetchFeed($url);
            } catch (Exception $e) {
                $xmlFeed = new SimpleXMLElement("<?xml version='1.0' standalone='yes'?><empty></empty>");
            }
            $feed = $this->_xmlToModel($id, $xmlFeed);
            $cache->save($feed, $cacheid, $tags, $this->_specificLifetime);
            $this->notify('cacheMiss', $feed);
        }
        // Notify observers of a load
        $this->notify('load', $feed);

        return $feed;
    }

    protected function _fetchFeed($url)
    {
        $xml = @new SimpleXMLElement($url, null, true);
        return $xml;
    }

    /**
     * Convert an XML object to a feed model
     *
     * @param string $id
     * @param string $xml
     * @return Feed_Model_Feed 
     */
    protected function _xmlToModel($id, $xml)
    {
        $config = Zend_Registry::get('feed-config');
        $defaultPersist = (isset($config->feed) && $config->feed->persistData);

        $feed = new $this->_feedClass();
        $entryMapper = call_user_func(array($this->_entryClass, 'getMapper'));

        $channel = $this->_getChannel($xml);
        if (null !== $channel) {
            $feed->setId($id);
            $this->_populateFeed($feed, $channel);
            $entries = array();
            $items = $this->_getItems($channel);
            if (null !== $items) {
                foreach ($items as $item) {
                    $entry = $entryMapper->xmlToModel($item);
                    $entry->setFeedname($id);
                    
                    // Save the entry to the database if configured to do so
                    if (isset($config->feeds->$id->persistData)) {
                        $persist = $config->feeds->$id->persistData;
                    } else {
                        $persist = $defaultPersist;
                    }

                    if (null === $entry->getId() && $persist) {
                        $entryMapper->save($entry);
                    }

                    $entries[] = $entry;
                }
            }

            $feed->setEntries($entries);
        }
        
        return $feed;
    }
    
    protected function _getChannel($xml) {
        return $xml->channel;
    }
    
    protected function _getItems($channel) {
        return $channel->item;
    }

    protected function _populateFeed($feed, $channel) {
        $feed->setTitle((string)$channel->title);
        $feed->setLink((string)$channel->link);
        $feed->setDescription((string)$channel->description);
    }
    
}
