<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Feed
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all feeds
 *
 * @category   Feed
 * @package    Model
 * @subpackage Feed
 */
class Feed_Model_Feed extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Feed_Model_FeedMapper';

    protected $_id = null;
    protected $_title = null;
    protected $_link = null;
    protected $_description = null;
    protected $_entries = null;
    protected $_persist = true;

    
    /**
     * Return mapper for model
     *
     * @return News_Model_FeedMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    /**
     * Get the feed's unique ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the feed's unique ID
     *
     * @param int $id
     * @return Feed_Model_Feed
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Get the feed title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * Set the feed title
     *
     * @param string $title
     * @return Feed_Model_Feed
     */
    public function setTitle($title)
    {
        $this->_title = (string)$title;

        return $this;
    }

    /**
     * Get the feed link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->_link;
    }

    /**
     * Set the feed link
     *
     * @param string $link
     * @return Feed_Model_Feed
     */
    public function setLink($link)
    {
        $this->_link = (string)$link;

        return $this;
    }

    /**
     * Get the feed description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * Set the feed description
     *
     * @param string $description
     * @return Feed_Model_Feed
     */
    public function setDescription($description)
    {
        $this->_description = (string)$description;

        return $this;
    }

    /**
     * Get the item entries for this feed. 
     * 
     * @return Zend_Paginator
     */
    public function getEntries($page=1)
    {
        $config = Zend_Registry::get('feed-config');
        $defaultPersist = $this->_persist && (isset($config->feed) && $config->feed->persistData);
        if (isset($config->feeds->{$this->getId()}->persistData)) {
            $persist = $config->feeds->{$this->getId()}->persistData;
        } else {
            $persist = $defaultPersist;
        }
        if ($persist) {
            // Fetch the feed from the database.
            $mapper = Feed_Model_Entry::getMapper();
            return $mapper->findAllByField(array('feed_name' => $this->getId()), array('pubDate'=>'desc'), $page);
        } else {
            if (is_array($page)) {
                $count = $page['count'];
                $page = $page['page'];
            } else {
                $count = 10;
            }
            
            // We have fetched teh feed live, use the set entries.
            $paginator = new Mcmr_Paginator(new Zend_Paginator_Adapter_Array($this->_entries));
            $paginator->setItemCountPerPage($count);
            $paginator->setCurrentPageNumber($page);

            return $paginator;
        }
    }

    /**
     * Set the feed Entries. This is only used if the feed information is not peristed to the database
     * If we are using the database all feed entries are read from storage.
     *
     * @param array $entries
     * @return Feed_Model_Feed 
     */
    public function setEntries($entries)
    {
        $this->_entries = $entries;

        return $this;
    }
}
