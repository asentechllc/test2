<?php
/**
 * McCormack & Morrison Gossip 3 CMS System
 *
 * @category   Feed
 * @package    Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all feeds
 *
 * @category   Feed
 * @package    Model
 * @subpackage Socialcount
 */
class Feed_Model_Socialcount extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Feed_Model_SocialcountMapper';

    protected $_id = null;
    protected $_url = null;
    protected $_counts = null;
    
    /**
     * Get the feed's unique ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the feed's unique ID
     *
     * @param int $id
     * @return Feed_Model_Socialcount
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    }

    /**
     * Set the feed's url
     *
     * @param int $url
     * @return Feed_Model_Socialcount
     */
    public function setUrl($url)
    {
        $this->_url = $url;

        return $this;
    }

    public function getUrl()
    {
        return $this->_url;
    }

    public function getTwitterCount()
    {
        return $this->getCount('twitter');
    }

    public function getFacebookCount()
    {
        return $this->getCount('facebook');
    }

    public function getLinkedinCount()
    {
        return $this->getCount('linkedin');
    }

    public function getGoogleCount()
    {
        return $this->getCount('google');
    }

    public function getTotalCount()
    {
        $total = 0;
        foreach ($this->_counts as $service=>$count) {
            $total+=$count;
        }
        return $total;
    }

    public function getCount($service)
    {
        if (isset($this->_counts[$service])) {
            return $this->_counts[$service];
        }
        return null;
    }

    public function getCounts()
    {
        return $this->_counts;
    }

    public function setCounts($counts) 
    {
        $this->_counts = $counts;
    }

}
