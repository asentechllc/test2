<?php

class Feed_Model_Soundcloud_PlaylistMapper extends Feed_Model_Soundcloud_AbstractMapper
{
    protected $_feedClass = 'Feed_Model_Soundcloud_Playlist';
    protected $_entryClass = 'Feed_Model_Soundcloud_Track';

    public function findOneByField($condition = null)
    {
        if (null === $condition['playlist_id']) {
            throw new Mcmr_Model_Exception("Playlist id required");
        }
        $url = $this->getPlaylistUrl($condition);
        $res = $this->_fetchAndCache($condition['playlist_id'], $url);
        
        return $res;
    }

    public function findAllByField($condition = null, $order = null, $page = 1)
    {
        throw new Mcmr_Model_Exception('No find all for soundcloud');
    }

    public static function getPlaylistUrl($condition)
    {
        $playlistId = $condition['playlist_id'];
        $clientId = parent::getClientId();
        $url = "http://api.soundcloud.com/playlists/{$playlistId}.xml?client_id={$clientId}";
        return $url;
    }

    protected function _getChannel($xml) {
        return $xml;
    }
    
    protected function _getItems($playlist) {
        return $playlist->tracks->track;
    }

    protected function _populateFeed($feed, $channel) {
        $feed->setTitle((string)$channel->title);
        /*
        $feed->setLink((string)$channel->link);
        $feed->setDescription((string)$channel->description);
        */
    }
    
}
