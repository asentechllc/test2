<?php
/**
 * 
 */
class Feed_Model_Soundcloud_Playlist extends Feed_Model_Feed
{
    static protected $_mapperclass = 'Feed_Model_Soundcloud_PlaylistMapper';

    /**
     * Return mapper for model
     *
     * @return News_Model_FeedMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

}
