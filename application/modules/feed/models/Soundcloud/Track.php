<?php

/**
 * 
 */
class Feed_Model_Soundcloud_Track extends Feed_Model_Entry
{
    static protected $_mapperclass = 'Feed_Model_Soundcloud_TrackMapper';

    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

}
