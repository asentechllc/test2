<?php

class Feed_Model_Soundcloud_TrackMapper extends Feed_Model_EntryMapper
{

    public function xmlToModel(SimpleXMLElement $xml)
    {
        $entry = new Feed_Model_Soundcloud_Track();
        $entry->setGuid((string) $xml->uri);
        $entry->setTitle((string) $xml->title);
        $entry->setDescription((string) $xml->description);
        $entry->setLink((string) $xml->{'permalink-url'});
        $entry->setPubDate(strtotime($xml->{'created-at'}));
        return $entry;
    }

}
