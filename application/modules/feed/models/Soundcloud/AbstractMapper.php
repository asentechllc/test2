<?php

class Feed_Model_Soundcloud_AbstractMapper extends Feed_Model_FeedMapper
{

    public static function getClientId() {
        $config = Zend_Registry::get('feed-config');
        return $config->soundcloud->clientId; 
    }

}
