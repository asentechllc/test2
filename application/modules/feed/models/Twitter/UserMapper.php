<?php

/**
 *  Simple mapper for anonymous twitter json API
 */

class Feed_Model_Twitter_UserMapper extends Feed_Model_FeedMapper
{


    public function xmlToModel(SimpleXMLElement $xml)
    {
        $entry = new Feed_Model_Twitter_User();
        $entry->setName(strtotime($xml->name));
        $entry->setScreenname((string)$xml->screen_name);
        $entry->setImage((string)$xml->profile_image_url);
        return $entry;
    }


}
