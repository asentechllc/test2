<?php

/**
 *  Simple mapper for anonymous twitter json API
 */
class Feed_Model_Twitter_TweetMapper extends Feed_Model_FeedMapper
{

    public function xmlToModel(SimpleXMLElement $xml)
    {
        $entry = new Feed_Model_Twitter_Tweet();
        $entry->setCreatedate(strtotime($xml->created_at));
        $entry->setText((string) $xml->text);
        $userMapper = Feed_Model_Twitter_User::getMapper();
        $entry->setUser($userMapper->xmlToModel($xml->user));
        
        return $entry;
    }

}
