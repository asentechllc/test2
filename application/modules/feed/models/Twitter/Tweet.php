<?php
/**
 * 
 */
class Feed_Model_Twitter_Tweet extends Feed_Model_Entry
{
    static protected $_mapperclass = 'Feed_Model_Twitter_TweetMapper';

    protected $_text;
    protected $_createdate;
    protected $_user;

    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }
    
    /**
     * Get the article creation date as a timestamp
     *
     * @return int
     */
    public function getCreatedate()
    {
        if (null === $this->_createdate) {
            $this->_createdate = time();
        }

        return $this->_createdate;
    }

    /**
     * Set the creation date as a timestamp
     *
     * @param int $createdate
     * @return News_Model_Article
     */
    public function setCreatedate($createdate)
    {
        $this->_createdate = intval($createdate);

        return $this;
    }

    /**
     * Get tweet content
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * Set the article content
     *
     * @param string $content
     * @return News_Model_Article
     */
    public function setText($text)
    {
        $this->_text = $text;

        return $this;
    }

    /**
     */
    public function getUser()
    {
        return $this->_user;
    }

    /**
     */
    public function setUser($user)
    {
        $this->_user = $user;

        return $user;
    }

}
