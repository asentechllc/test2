<?php

/**
 *  Simple mapper for anonymous twitter json API
 */
class Feed_Model_Twitter_TweetsMapper extends Feed_Model_FeedMapper
{
    protected $_feedClass = 'Feed_Model_Twitter_Tweets';
    protected $_entryClass = 'Feed_Model_Twitter_Tweet';

    const API_URL = 'http://api.twitter.com/1/';
    const API_FORMAT = 'xml';

    public function  __construct()
    {
        parent::__construct();
        $config = Zend_Registry::get('feed-config');
        $this->_specificLifetime = (isset($config->twitter) && isset($config->twitter->cacheTime))
                    ? $config->twitter->cacheTime : false;
    }

    /**
     * Feed url is build from conditions.
     * Order is and will be ingored
     * Form n
     */
    public function findOneByField($condition = null, $order=null, $page=1)
    {
        $url = $this->_buildTwitterUrl($condition);
        return $this->_fetchAndCache(null, $url, $page);
    }

    protected function _buildTwitterUrl($condition)
    {
        $url = self::API_URL;
        switch ($condition['method']) {
            case 'list_statuses':
                $url.=$condition["user_id"] . "/lists/" . $condition["list_id"] . "/statuses";
                break;
            case 'statuses':
                $url.="statuses/user_timeline/".$condition["user_id"];
                break;
        }
        $url .= '.' . self::API_FORMAT;
        return $url;
    }

    protected function _xmlToModel($id, $xml)
    {
        $feed = new $this->_feedClass();
        $entryMapper = call_user_func(array($this->_entryClass, 'getMapper'));
        $entries = array();
        $statuses = $xml;
        if (null !== $statuses->status) {
            foreach ($statuses->status as $item) {
                $entry = $entryMapper->xmlToModel($item);
                $entries[] = $entry;
            }
        }

        $feed->setEntries($entries);
        return $feed;
    }

}
