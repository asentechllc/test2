<?php

/**
 * 
 */
class Feed_Model_Twitter_User extends Feed_Model_Entry
{
    static protected $_mapperclass = 'Feed_Model_Twitter_UserMapper';
    
    protected $_image;
    protected $_screenname;
    protected $_name;
    private $_url;

    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }

    public function setImage($image)
    {
        $this->_image = $image;
        return $this;
    }

    public function getImage()
    {
        return $this->_image;
    }

    public function setScreenname($screenname)
    {
        $this->_screenname = $screenname;
        return $this;
    }

    public function getScreenname()
    {
        return $this->_screenname;
    }

    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function getUrl()
    {
        if (null === $this->_url) {
            if (null !== $this->_screenname) {
                $this->_url = 'http://twitter.com/' . $this->_screenname;
            }
        }
        return $this->_url;
    }

}
