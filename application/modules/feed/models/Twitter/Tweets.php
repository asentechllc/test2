<?php
/**
 * 
 */
class Feed_Model_Twitter_Tweets extends Feed_Model_Feed
{
    static protected $_mapperclass = 'Feed_Model_Twitter_TweetsMapper';

    /**
     * Return mapper for model
     *
     * @return News_Model_FeedMapper
     */
    static public function getMapper($mapperclass=null)
    {
        if (null === $mapperclass) {
            return parent::getMapper(self::$_mapperclass);
        } else {
            return parent::getMapper($mapperclass);
        }
    }
    
    public function getEntries($page=null)
    {
        return $this->_entries;
    }

}
