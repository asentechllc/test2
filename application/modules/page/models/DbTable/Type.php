<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Database table class for the article types table
 *
 * @category Page
 * @package Page_Model
 * @subpackage DbTable
 */
class Page_Model_DbTable_Type extends Mcmr_Db_Table_Abstract
{
    protected $_name = 'page_types';
    protected $_primary = 'type_id';
}
