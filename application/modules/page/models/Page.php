<?php   
/** 
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Model class for all Pages
 *
 * @category Page
 * @package Page_Model
 * @subpackage Page
 */
class Page_Model_Page extends Mcmr_Model_ModelAbstract
{
    static protected $_mapperclass = 'Page_Model_PageMapper';

    protected $_id = null;
    protected $_title = null;
    protected $_content = null;
    protected $_url = null;
    protected $_userid = null;
    protected $_parentid = null;
    protected $_createdate = null;
    protected $_updatedate = null;
    protected $_locked = null;
    protected $_published = null;
    protected $_order = null; 
    protected $_childtemplate = null;
    protected $_childrenallowed = null;

    private $_userObject = null;
    private $_parentObject = null;
    private $_children = null;

    /**
     * Return mapper for model
     *
     * @return Page_Model_PageMapper
     */
    static function getMapper($mapperclass=NULL)
    {
        return parent::getMapper(self::$_mapperclass);
    } //getMapper

    /**
     * Get the model ID
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * Set the ID
     *
     * @param int $id
     * @return Page_Model_Page
     */
    public function setId($id)
    {
        $this->_id = $id;

        return $this;
    } //setId

    /**
     * Get the url or create one based on title if not set before.
     * @return string
     */
    public function getUrl()
    {
        if (null === $this->_url && null !== $this->_title) {
            $this->_url = Mcmr_StdLib::urlize($this->_title);
            $this->_updatedate = time();
        }
        return $this->_url;
    } //getUrl

    /**
     * Set the url
     * @param string $arg the new url
     * @return Page_Model_Page
     */
    public function setUrl($arg)
    {
        $urlized =  Mcmr_StdLib::urlize($arg);
        $this->_toggleUpdatedate($this->_url, $urlized);
        $this->_url = $urlized;
        return $this;
    } //setUrl

    
    /**
     * Get the title.
     * @return string
     */
    public function getTitle()
    {
        return $this->_title;
    } //getTitle

    /**
     * Set the title
     * @param string $title
     * @return Page_Model_Page
     */
    public function setTitle($arg)
    {
        $this->_toggleUpdatedate($this->_title, $arg);
        $this->_title = $arg;
        return $this;
    } //setTitle

    
    /**
     * Get the ordering number.
     * @return int
     */
    public function getOrder()
    {
        if ($this->_order === null) {
            $this->_order =  self::getMapper()->findMaxOrder() + 10;
        } 
        return $this->_order;
    } //getOrder

    /**
     * Set the ordering number.
     * @param int $arg new order
     * @return Page_Model_Page
     */
    public function setOrder($arg)
    {
        $this->_toggleUpdatedate($this->_order, $arg);
        $this->_order = $arg;
        return $this;
    } //setOrder

    
    /**
     * Get the content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->_content;
    } //getContent

    /**
     * Set the content
     *
     * @param string $arg
     * @return Page_Model_Page
     */
    public function setContent($arg)
    {
        $this->_toggleUpdatedate($this->_content, $arg);
        $this->_content = $arg;
        return $this;
    } //setContent

    

    /**
     * Get the author user id
     * @return int
     */
    public function getUserid()
    {
        if (null === $this->_userid) {
            $this->_userid = Zend_Registry::get('authenticated-user')->getId();
        }

        return $this->_userid;
    } //getUserid

   /**
    * Set the author user
    *
    * @param int $userid
    * @return Page_Model_Page
    */
    public function setUserid($userid)
    {
        $this->_userid = $userid;
        return $this;
    } //setUserid
    


   /**
    * Set the parent page id.
    *
    * @param int $arg
    * @return Page_Model_Page
    */
    public function setParentid( $arg )
    {
        $this->_parentid = $arg;
        return $this;
    } //setParentId
    

    /**
     * Get the parent page id.
     * @return int
     */
    public function getParentid()
    {
        if (null === $this->_parentid) {
            $this->_parentid = 0;
        }
        return $this->_parentid;
    } //getParentId

    
    /**
     * Return create timestamp.
     * Automatically set to current time for a fresh object.
     * @return int
     */
    public function getCreatedate()
    {
        if (null === $this->_createdate) {
            $this->_createdate = time();
        }

        return $this->_createdate;
    } //getCreatedate

    /**
     * Set create timesatmp
     * @param int $updatedate new create timestamp. 
     * @return Page_Model_Page
     */
    public function setCreatedate($createdate)
    {
        $this->_createdate = intval($createdate);

        return $this;
    } //setCreatedate


    /**
     * Return last update timestamp
     * @return int
     */
    public function getUpdatedate()
    {
        if (null === $this->_updatedate) {
            $this->_updatedate = time();
        }

        return $this->_updatedate;
    } //getUpdatedate

    /**
     * Set the update timestamp. Most of times this is not necessary as other setters will
     * refresh the timestamp when their variables are changed.
     *
     * @param int $updatedate new update timestamp.
     * @return Page_Model_Page
     */
    public function setUpdatedate($updatedate)
    {
        $this->_updatedate = intval($updatedate);
        return $this;
    } //setUpdatedate
    
    /**
     * Return the parent page as object.
     * @return Page_Model_Page
     */
    public function getParent() 
    {
        if ($this->_parentid && $this->_parentObject === null) {
            $mapper = self::getMapper();
            $this->_parentObject= $mapper->find($this->_parentid);
        }
        return $this->_parentObject;
    } //getParent
    
    
    /**
     * Get children of this page.
     * @param int $published value for the published flag or null if any
     * @return Zend_Paginator
     */
    public function getChildren( $published=null ) 
    {
        if ($this->_children === null) {
            $mapper = self::getMapper();
            $this->_children = $mapper->findChildren($this, $published);
        }
        return $this->_children;
    } //getChildren
    

    /**
     * Return the user who created this page.
     * @return User_Model_User
     */
    public function getUser() 
    {
        if ($this->_userObject === null) {
            $mapper = User_Model_User::getMapper();
            $this->_userObject = $mapper->find($this->_userid);
        }
        return $this->_userObject;
    } //getUser
    
    /**
     * Are children allowed for this page?
     * @return boolean
     */
    public function getChildrenallowed() 
    {
        if ($this->_childrenallowed === null) {
            $this->_childrenallowed=false;
        }
        return $this->_childrenallowed;
    } //getChildrenAllowed
    
    /**
     * Set if the children ale allowed for this page
     * @param bool $arg new value for the flag
     * @return Page_Model_Page
     */
    public function setChildrenallowed($arg) 
    {
        $this->_toggleUpdatedate($this->_childrenallowed, $arg);
        $this->_childrenallowed = $arg;
        return $this;
    } //setChildrenAllowed

    /**
     * Is this page locked?
     * @return boolean
     */
    public function getLocked() 
    {
        if ($this->_locked === null) {
            $this->_locked=false;
        }
        return $this->_locked;
    } //getLocked
    
    /**
     * Set if the page is locked
     * @param bool $arg new value for the locked flag
     * @return Page_Model_Page
     */
    public function setLocked($arg) 
    {
        $this->_toggleUpdatedate($this->_locked, $arg);
        $this->_locked = $arg;
        return $this;
    } //setLocked
    
    /**
     * Is this page published?
     * @return boolean
     */
    public function getPublished() 
    {
        if ( $this->_published === null ) {
            $this->_published=true;
        }
        return $this->_published;
    } //getPublished
    
    /**
     * Set if the page is published
     * @param bool $arg new value for the published flag
     * @return Page_Model_Page
     */
    public function setPublished($arg) 
    {
        $this->_toggleUpdatedate($this->_published, $arg);
        $this->_published = $arg;
        return $this;
    } //setPublished
    
    
    /**
     * Return child template name.
     * @return string
     */
    public function getChildtemplate() 
    {
        //this is a nullable field and it defaults to null
        return $this->_childtemplate;
    } //getChildTemplate
    
    /**
     * Set child template name.
     * @param bool $arg new value for chile template name
     * @return Page_Model_Page
     */
    public function setChildtemplate($arg) 
    {
        $this->_toggleUpdatedate($this->_childtemplate, $arg);
        $this->_childtemplate = $arg;
        return $this;
    } //setChildTemplate
    
    
    public function setOptions(array $options) 
    {
        parent::setOptions($options);
    }
    
    public function isSubsite($siteid=null){
        if (null!==$siteid) {
            return substr($this->getUrl(),0,strlen($siteid))==$siteid;
        } else {
            $sites = Default_Model_Site::getMapper()->findAllByField(array('withAll' => false));
            foreach($sites as $site ) {
                $name = $site->getName();
                if (substr($this->getUrl(),0,strlen($name))==$name) {
                    return true;
                }
            }
            return false;
        }
    }

    /**
     * Change update timestamp if the new value is different from old.
     * Utility function to be used in setter. Doesn't deal with updating the value,
     * that is left to the stter itself. All it does is refresh the timestamp if required.
     *
     * @param mixed $old_value
     * @param mixed $new_value
     * @return Page_Model_Page
     */
    protected function _toggleUpdatedate($oldValue, $newValue) 
    {
        if ( $oldValue != $newValue ) {
            $this->_updatedate = time();
        }
        return $this;
    } //toggleUpdatedate

    
} //Page_Model_Page 