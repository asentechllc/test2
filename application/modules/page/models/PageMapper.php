<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Model
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Mapper class for Pages
 *
 * @category Page
 * @package Page_Model
 * @subpackage PageMapper
 */
class Page_Model_PageMapper extends Mcmr_Model_GenericMapper
{
    protected $_dbTableClass = 'Page_Model_DbTable_Page';
    protected $_dbAttrTableClass = 'Page_Model_DbTable_PageAttr';
    protected $_columnPrefix = 'page_';
    protected $_modelClass = 'Page_Model_Page';
    protected $_cacheIdPrefix = 'Page_Page';
    
    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseField()
     */
    protected function _sanitiseField($field)
    {
        switch (strtolower($field)) {
            case 'user':
            case 'userid':
            case 'user_id':
                $field = 'user_id';
                break;
            case 'parent':
            case 'parentid':
            case 'parent_id':
                $field = 'page_parentid';
                break;
            default:
                $field = $this->_columnPrefix . $field;
                break;
        }
        return $field;
    } //_sanitiseField
    

    /**
     * @see Mcmr_Model_GenericMapper::_sanitiseValue()
     */
    protected function _sanitiseValue($field, $value)
    {
        switch ($field) {
            case 'page_createdate':
            case 'page_updatedate':
                return new Zend_Db_Expr("FROM_UNIXTIME({$value})");
            default:
                return $value;
        }
    } //_sanitiseValue
    

    /**
     * @see Mcmr_Model_GenericMapper::_selectFields()
     */
    protected function _selectFields()
    {
        $all = parent::_selectFields();
        $all[] = "UNIX_TIMESTAMP(page_createdate) AS page_createdate";
        $all[] = "UNIX_TIMESTAMP(page_updatedate) AS page_updatedate";
        return $all;
    } //_selectFields
    

    /**
     * Check if the page has subpages before deleting.
     * @see Mcmr_Model_GenericMapper::delete()
     */
    public function delete(Mcmr_Model_ModelAbstract $model)
    {
        if (count($this->findChildren($model))) {
            throw new Mcmr_Model_Exception('Cannot delete page with subpages');
        }
        parent::delete($model);
    } //delete
    

    /**
     * Find the nearest neighbouring subpage of the page in the direction given.
     * @param Page_Model_Page $page 
     * @param string $direction which direction to look in.
     * Ahs to be given as either &lt; meaning 'before' or &gt; meaning 'after'
     * @param Page_Model_Page the nearest subpage in the direction given or null if nothing is found.
     */
    public function findSibling($page, $direction)
    {
        if ($direction != '<' && $direction != '>') {
            throw new Mcmr_Model_Exception('Invalid direction');
        }
        $table = $this->getDbTable();
        $select = $table->select();
        $select->where("page_parentid = ?", $page->getParentid());
        $select->where("page_order $direction ?", $page->getOrder());
        $ordering = ($direction == '<') ? 'DESC' : 'ASC';
        $select->order("page_order $ordering");
        return $this->_convertRowToModel($table->fetchRow($select));
    } //findSibling
    

    /**
     * Swap the places occupied by two pages in the ordering sequence.
     * @param Page_Model_Page $firstPage first sibling
     * @param Page_Model_Page $firstPage second sibling
     */
    public function swapSiblings($firstPage, $secondPage)
    {
        if ($firstPage->getParentid() != $secondPage->getParentid()) {
            throw new Mcmr_Model_Exception('Trying to swap pages that are not siblings!');
        }
        
        $firstOrder = $firstPage->getOrder();
        $secondOrder = $secondPage->getOrder();
        $firstPage->setOrder($secondOrder);
        $secondPage->setOrder($firstOrder);
        $this->save($firstPage);
        $this->save($secondPage);
    } //swapSiblings
    

    /**
     * Find all top level pages.
     * Pages are filtered by published status if requested.
     * @param int $published value for the published flag or null if any
     */
    public function findTopPages($published = null)
    {
        return $this->_findChildren(0, $published);
    } //findTopPages
    

    /**
     * Find all subpages of given page.
     * Pages are filtered by published status if requested.
     * @param Page_Model_Page $parent parent page id or zero if looking for top level pages
     * @param int $published value for the published flag or null if any
     * @return Zend_Paginator
     */
    public function findChildren($parent, $published = null)
    {
        return $this->_findChildren($parent->getId(), $published);
    } //findChildren
    

    /**
     * Helper function to find pages by given parent_id and published field.
     * @param int $parentId parent page id or zero if looking for top level pages
     * @param int $published value for the published flag or null if any
     * @return Zend_Paginator
     */
    protected function _findChildren($parentId, $published = null)
    {
        $params = array('parent' => $parentId);
        if ($published) {
            $params['published'] = $published;
        }
        return $this->findAllByField(
            $params, 
            array('order' => 'ASC'),
            array('page' => '1', 'count' => '100')
        );
    } //_findChildren
    

    /**
     * Find the maxium 'order'.
     * @return int the maximum value of the order field among all pages. 
     */
    public function findMaxOrder()
    {
        //this should not be cached - we only request that when new page is created and then it should be exact
        //would be nicer to extract the table name from the table class, but it's protected there
        $table = $this->getDbTable();
        if ($table instanceof Mcmr_Db_Table_Abstract) {
            $adapter = $table->getReadAdapter();
        } else {
            $adapter = $table->getAdapter();
        }
        //$row = $adapter->fetchRow( "SELECT coalesce((SELECT max(`page_order`)
        //FROM `page_pages` WHERE `page_parentid`=?), 0) AS `max`", array( 0 ) );
        $row = $adapter->fetchRow(
            "SELECT coalesce((SELECT max(`page_order`) FROM `page_pages` ), 0) AS `max`"
        );
        
        return $row['max'];
    } //findMaxOrder


} //Page_Model_PageMapper
