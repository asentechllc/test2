<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Form
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Form for pages
 *
 * @category Page
 * @package Page_Form
 * @subpackage Page
 */
class Page_Form_Page extends Mcmr_Form
{
    public function __construct($options = null)
    {
        $page = null;
        if (null !== $options) {
            if (is_object($options)) {
                $options = $options->toArray();
            }

            if (isset($options['page'])) {
                $page = $options['page'];
                unset($options['page']);
            }
        }

        if (null != $page) {
            // We have an article type. Try and load the config file for this specific type.
            $options['configFile'] = 'forms' . DIRECTORY_SEPARATOR . 'page' 
            . DIRECTORY_SEPARATOR . 'page_form_page-'.strtolower($page).'.ini';

        }
        
        parent::__construct($options);
    }

    public function init()
    {
        $this->setName('pageformpage')->setElementsBelongTo('page-form-page');
        $this->setMethod('post');
        
        $this->addElement(
            'Submit', 'submit',
            array(
                'ignore' => true ,
                'label' => 'Save Page'
            )
        );

    } //init

    public function postInit()
    {
    } //postInit

} //Page_Form_Page 
