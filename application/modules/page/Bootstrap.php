<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 792 2010-06-16 11:17:26Z michal $
 */

/**
 * Bootstrap
 *
 * @category Page
 * @package Page_Bootstrap
 */
class Page_Bootstrap extends Mcmr_Application_Module_Bootstrap
{

    /**
     * @see Mcmr_Application_Module_Bootstrap
     */
    public function _initHelpers()
    {
        $bootstrap = $this->getApplication();
        $bootstrap->bootstrap('view');
        $view = $bootstrap->getResource('view');
            $view->addHelperPath(dirname(__FILE__) . '/helpers', 'Page_View_Helper');
    }
}
