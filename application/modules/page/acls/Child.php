<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Delete child page
 *
 * @category News
 * @package News_Acl
 * @subpackage ViewPublished
 */
class Page_Acl_Child implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if the page is a child
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the page object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
        }
        if ( null !== $id ) {
            $mapper = Page_Model_Page::getMapper();
            $page = $mapper->find($id);
            return (null !== $page && $page->getParentId() );
        } else {
            return false;
        }

    }
}
