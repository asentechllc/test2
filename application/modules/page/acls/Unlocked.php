<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Only allow operation if the page is unlocked.
 *
 * @category Page
 * @package Page_Acl
 * @subpackage ViewPublished
 */
class Page_Acl_Unlocked implements Zend_Acl_Assert_Interface
{
    /**
     * Returns true if and only if the page is unlocked
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the user object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();

        if (null !== $params && isset($params['id'])) {
            $id = $params['id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $id = $controller->getRequest()->getParam('id', null);
            $url = $controller->getRequest()->getParam('url', null);
        }

        if (null !== $id || null !== $url) {
            $mapper = Page_Model_Page::getMapper();
            $page = (null !== $id)?$mapper->find($id):$mapper->findOneByField(array('url'=>$url));
            return (null !== $page && !$page->getLocked());
        } else {
            return true;
        }

    }

}
