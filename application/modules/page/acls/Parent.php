<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_Acl
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Create subpage for a parent
 *
 * @category News
 * @package News_Acl
 * @subpackage ViewPublished
 */
class Page_Acl_Parent implements Zend_Acl_Assert_Interface
{

    /**
     * Returns true if the page identified by parent_id allows subpages creation
     *
     * @param  Zend_Acl                    $acl
     * @param  Zend_Acl_Role_Interface     $role
     * @param  Zend_Acl_Resource_Interface $resource
     * @param  string                      $privilege
     * @return boolean
     */
    public function assert(Zend_Acl $acl, Zend_Acl_Role_Interface $role = null,
            Zend_Acl_Resource_Interface $resource = null, $privilege = null)
    {
        // Get the ID of the page object. Ether from the assertion params in ACL, or the request object
        $params = Mcmr_Acl::getAssertParams();
        if (null !== $params && isset($params['parent_id'])) {
            $parentId = $params['parent_id'];
        } else {
            $controller = Zend_Controller_Front::getInstance();
            $parentId = $controller->getRequest()->getParam('parent_id', null);
        }
        if ($parentId) {
            $mapper = Page_Model_Page::getMapper();
            $parent = $mapper->find($parentId);
            return (null !== $parent && $parent->getChildrenallowed() );
        } else {
            return false;
        }
    }

}
