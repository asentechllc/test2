<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * Show all subpages of a page. 
 * Takes into account published flag if that's in the request.
 * This will work in conjunction with the acl will set that flag for guest users
 *
 * @category Page
 * @package Page_View
 * @subpackage 
 */
class Page_View_Helper_PageChildren extends Zend_View_Helper_Abstract
{
    /**
     * Get all subpages of given page filtering by published is requested.
     * @param Page_Model_Page $page parent page
     * @return Zend_Paginator collection of all subpages.
     */
    public function pageChildren( $page )
    {
        $controller = Zend_Controller_Front::getInstance();
        $published = $controller->getRequest()->getParam('published', null);
        $mapper = Page_Model_Page::getMapper();
        
        return $mapper->findChildren($page, $published);
    } //pageChildren
    
} //Page_View_Helper_PageChildren 
