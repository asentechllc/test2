<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category   Page
 * @package    Page_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: PageIndex.php 711 2010-05-28 09:35:13Z leigh $
 */

/**
 * A view helper to retrieve a list of pages
 *
 * @category   Page
 * @package    Page_View
 * @subpackage Helper
 */
class Page_View_Helper_PageIndex extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve an index of news articles based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param array|int $page
     * @param string $partial
     * @return array|string
     */
    public function pageIndex($conditions = array(), $order=null, $page=null, $partial = null)
    {
        $mapper = Page_Model_Page::getMapper();
        $pages = $mapper->findAllByField($conditions, $order, $page);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('pages'=>$pages));
                }
            }

            return $this->view->partial($partial, array('pages'=>$pages));
        }

        return $pages;
    }
}
