<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_View
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id$
 */

/**
 * A View Helper for retrieving a single page
 *
 * @category Page
 * @package Page_View
 * @subpackage Helper
 */
class Page_View_Helper_PagePage extends Zend_View_Helper_Abstract
{
    /**
     * Retrieve a page based on the conditions
     *
     * @param array $conditions
     * @param array $order
     * @param string $partial
     * @return array|string
     */
    public function pagePage($conditions = array(), $order=null, $partial = null)
    {
        $mapper = Page_Model_Page::getMapper();
        $page = $mapper->findOneByField($conditions, $order);

        if (null !== $partial) {
            if (is_array($partial)) {
                if (count($partial) != 2) {
                    $e = new Zend_View_Exception(
                        'A view partial supplied as an array must contain two values: the filename and its module'
                    );
                    $e->setView($this->view);
                    throw $e;
                }

                if ($partial[1] !== null) {
                    return $this->view->partial($partial[0], $partial[1], array('page'=>$page));
                }
            }

            return $this->view->partial($partial, array('page'=>$page));
        }

        return $page;
    }
}
