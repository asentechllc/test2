<?php

/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Page
 * @package Page_IndexController
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: IndexController.php 2234 2011-03-03 18:03:23Z michal $
 */

/**
 * Index Controller for the Page module. CRUD operations on pages.
 *
 * @category Page
 * @package Page_IndexController
 */
class Page_IndexController extends Mcmr_Controller_Action
{


    /**
     * Show all pages (for admin purposes only) or a single page.
     */
    public function indexAction()
    {
        if (null === $this->_request->getParam('url', null)) {

            $mapper = Page_Model_Page::getMapper();
            $this->view->pages = $mapper->findTopPages($this->_request->getParam('published', null));
        } else {
            $this->_forward('read');
        }
    }

    /**
     * Show create page form on GET or process create request on POST.
     */
    public function createAction()
    {
        $page = $this->_request->getParam('page');
        $form = $this->_helper->loadForm('Page', array('page'=>$page));
        $this->view->form = $form;
        $mapper = Page_Model_Page::getMapper();

        $parent = null;
        $parentId = $this->_request->getParam('parent_id', null);

        //check if parent page exists if parent_id present in the request
        if (null !== $parentId) {
            $parent = $mapper->find($parentId);
            if (null === $parent) {
                throw new Mcmr_Exception_PageNotFound('Parent page not found');
            }
        }

        if ($this->_request->isPost()) {
            if ($form->isValid($this->_request->getPost())) {
                $values = $form->getValues(true);
                $page = new Page_Model_Page();
                $page->setOptions($values);
                if ($parent) {
                    $page->setParentId($parent->getId());
                }
                try {
                    $this->view->error = false;
                    $mapper->save($page);

                    $this->view->getHelper('DisplayMessages')->addMessage('Page Saved', 'info');
                    $this->view->getHelper('Redirect')->notify('create', $page);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            } else {
                $this->view->error = true;
                $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'warn');
            }
        }
    }

    /**
     * Show a page identified by id or a url key.
     */
    public function readAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        $subpage = $this->_request->getParam('subpage', null);
        $parent = null;
        
        if (null !== $id || null !== $url) {
            $mapper = Page_Model_Page::getMapper();
            $page = null;
            if ( null !== $id ) {
                $page = $mapper->find($id);
            } else {
                if ('default' !== $this->_request->getSitename()) {
                    $siteUrl = $this->_request->getSitename() . '-' . $url;
                    $page = $mapper->findOneByField(array('url' => $siteUrl));
                } 
                if ( null === $page ) {
                    if($subpage){
                    	$parent = $mapper->findOneByField(array('url' => $url));
	                    $page = $mapper->findOneByField(array('url' => $subpage));
	                    if(!$page || !$parent || $page->getParentId() != $parent->getId()){
	                    	$page = null;
	                    	$parent = null;
	                    }
                    }
                    else{
	                    $page = $mapper->findOneByField(array('url' => $url));
                    }
                }
            }
            if (null === $page || ($page->getParentid() && !$parent) || !$page->getPublished()) {
                throw new Mcmr_Exception_PageNotFound('Page not found');
            }


			# SEO title & description override
			if($seoTitle = $page->getAttribute('seoTitle'))
				$this->view->headTitle($seoTitle);

			if($seoDesc = $page->getAttribute('seoDesc'))
				$this->view->headMeta()->appendName('description', $seoDesc);

            $this->view->page = $page;
            $this->_helper->viewRenderer('read/' . $url);

			if ($parent && ($template = $parent->getChildtemplate())) {
				$this->_helper->viewRenderer('read/' . $template);
			}
        } else {
            $this->_forward('index');
        }
    }

    /**
     * Show update page form on GET or process update requeston POST.
     */
    public function updateAction()
    {
        $id = $this->_request->getParam('id', null);
        $url = $this->_request->getParam('url', null);
        if (null !== $id || null !== $url) {
            $mapper = Page_Model_Page::getMapper();
            $page = ( null !== $id ) ? $mapper->find($id) : $mapper->findOneByField(array('url' => $url));

            if (null === $page) {
                throw new Mcmr_Exception_PageNotFound('Page not found');
            }

            $form = $this->_helper->loadForm('Page', array('page'=>$page->getUrl()));
            $this->view->form = $form;

            $this->view->page = $page;

            if ($this->_request->isPost()) {
                if ($form->isValid($this->_request->getPost())) {
                    $values = $form->getValues(true);
                    $page->setOptions($values);

                    try {
                        $this->view->error = false;
                        $mapper->save($page);
                        $this->view->getHelper('DisplayMessages')->addMessage('Page Saved', 'info');
                        $this->view->getHelper('Redirect')->notify('update', $page);
                    } catch (Exception $e) {
                        $this->view->error = true;
                        $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                    }
                } else {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage('Invalid Form Values', 'warn');
                }
            } else {
                $form->populate($page->getOptions(true));
            }
        } else {
            $this->_forward('index');
        }
    }

//updateAction

    /**
     * Show delete page confirmation on GET or delte page on POST.
     */
    public function deleteAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Page_Model_Page::getMapper();
            $page = $mapper->find($id);

            if (null === $page) {
                throw new Mcmr_Exception_PageNotFound('Page not found');
            }

            $this->view->page = $page;

            if ($this->_request->isPost()) {
                try {
                    $mapper->delete($page);
                    $this->view->error = false;
                    $this->view->getHelper('DisplayMessages')->addMessage('Page Deleted', 'info');
                    $this->view->getHelper('Redirect')->notify('delete', $page);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        } else {
            $this->_forward('index');
        }
    }

//deleteAction

    /**
     * Move the page
     */
    public function moveAction()
    {
        if (null !== ($id = $this->_request->getParam('id', null))) {
            $mapper = Page_Model_Page::getMapper();
            $page = $mapper->find($id);

            if (null === $page) {
                throw new Mcmr_Exception_PageNotFound('Page not found');
            }

            $this->view->page = $page;
            if ($this->_request->isPost()) {
                try {
                    $dir = ($this->_request->getParam('dir', 'up') == 'up') ? '<'
                                : '>';
                    $sibling = $mapper->findSibling($page, $dir);
                    if ($sibling) {
                        $mapper->swapSiblings($page, $sibling);
                    }
                    $this->view->getHelper('DisplayMessages')->addMessage('Page Moved', 'info');
                    $this->view->getHelper('Redirect')->notify('move', $page);
                } catch (Exception $e) {
                    $this->view->error = true;
                    $this->view->getHelper('DisplayMessages')->addMessage($e->getMessage(), 'error');
                }
            }
        } else {
            $this->_forward('index');
        }
    }

//moveAction
}

//Page_IndexController
