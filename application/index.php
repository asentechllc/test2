<?php
class SimpleStats
{
    public static function getTime()
    {
        static $start = null;

        $time = microtime();
        $time = explode(" ", $time);
        $time = $time[1] + $time[0];

        if (null === $start) {
            $start = $time;

            return 0;
        } else {
            return $time - $start;
        }
    }

    public static function output()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        $format = $request->getParam('format');
        
        if (APPLICATION_STATS && null === $format) {
            $view = new Zend_View();
            $view->addScriptPath(SITE_PATH.'/views/_common');
            $view->generateTime = self::getTime();
            
            echo $view->render('statsfooter.phtml');
        }
    }
}

SimpleStats::getTime();

// Flag to display application statistics
defined('APPLICATION_STATS')
    || define(
        'APPLICATION_STATS',
        (isset($_GET['APPLICATION_STATS'])
            ? $_GET['APPLICATION_STATS'] : (getenv('APPLICATION_STATS') ? getenv('APPLICATION_STATS') : '0'))
    );

defined('SITE_PATH')
    || define('SITE_PATH', (getenv('SITE_PATH') ? getenv('SITE_PATH') : APPLICATION_PATH));

defined('SITE_ID')
    || define('SITE_ID', (getenv('SITE_ID') ? getenv('SITE_ID') : 'default'));

defined('PUBLIC_SITE_PATH')
    || define('PUBLIC_SITE_PATH', (getenv('PUBLIC_SITE_PATH') ? getenv('PUBLIC_SITE_PATH') : SITE_PATH));

defined('ADMIN_SITE_PATH')
    || define('ADMIN_SITE_PATH', (getenv('ADMIN_SITE_PATH') ? getenv('ADMIN_SITE_PATH') : SITE_PATH.(strpos(SITE_PATH,'-admin') ? '' : '-admin')));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(
    implode(
        PATH_SEPARATOR,
        array(
            realpath(APPLICATION_PATH . '/../library'),
            realpath(SITE_PATH . '/library'),
            get_include_path(),
        )
    )
);

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    SITE_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();

SimpleStats::output();
