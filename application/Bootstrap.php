<?php
/**
 * McCormack & Morrison Gossip 2 CMS System
 *
 * @category Mcmr
 * @package Bootstrap
 * @copyright  Copyright (c) 2010 McCormack & Morrison. (http://www.mccormackmorrison.com/)
 * @version    $Id: Bootstrap.php 2379 2011-04-26 14:41:08Z leigh $
 */

/**
 * Main Application Bootstrap file
 *
 * @category Mcmr
 * @package Bootstrap
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    /**
     * Bootstrap system namespaces
     */
    protected function _initNamespaces()
    {
        $loader = Zend_Loader_Autoloader::getInstance();
        $loader->registerNamespace('Mcmr');
        $loader->registerNamespace('Hybrid');
    }

    /**
     * Bootstrap the session handler. Session handler is the 'session' cache in the cachemanager resource
     */
    protected function _initSessionHandler()
    {
        $this->bootstrap('cachemanager');
        $manager = $this->getResource('cachemanager');
        
        if ($manager->hasCache('session')) {
            $cache = $manager->getCache('session');
            Zend_Session::setSaveHandler(new Mcmr_Session_SaveHandler_Cache($cache));
        }
    }

    /**
     * Change the default dispatcher. Mcmr dispatcher adds site components to the dispatcher same as modules
     * controllers and actions.
     */
    protected function _initDispatcher()
    {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
 
        // Initialize the request object
        $dispatcher = new Mcmr_Controller_Dispatcher_Standard();
        $front->setDispatcher($dispatcher);
    }
    
    /**
     * Change the default request object. Adds site functions same as modules, controllers and actions
     */
    protected function _initRequest()
    {
        $this->bootstrap('FrontController');
        $front = $this->getResource('FrontController');
 
        // Initialize the request object
        $request = new Mcmr_Controller_Request_Http();
        $front->setRequest($request);
 
        if (defined("SITE_ID")) {
            $request->setParam('site', SITE_ID);
        }
        
        return $request;
    }
    
    /**
     * Put the application configuration into the registry
     */
    protected function _initApplication()
    {
        $this->bootstrap('frontcontroller');
        $front = $this->getResource('frontcontroller');
        
        // Set the path for all application modules.
        $front->addModuleDirectory(realpath(dirname(__FILE__) . '/modules'));

        // Set registry for authenticated user and application config.
        Zend_Registry::set('authenticated-user', null);
        Zend_Registry::set('app-config', new Zend_Config($this->getOptions()));
        
        $locale = new Zend_Locale('en_GB');
        Zend_Registry::set('Zend_Locale', $locale);        
    }

    /**
     * Initialise the system caches. Specifically the DB metadata cache
     */
    protected function _initCache()
    {
        $this->bootstrap('cachemanager');
        $manager = $this->getResource('cachemanager');
        Zend_Registry::set('cachemanager', $manager);

        // Set the cache for all DB Table metadata
        $cache = $manager->getCache('metadata');
        Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
    }

    /**
     * Set the view render as the Mcmr view renderer.
     * Add a site path in addition to the base path. Allows subsite templates to fall back to
     * the default templates if the template doesn't exist.
     */
    protected function _initViewRenderer()
    {
        $this->bootstrap('view');
        
        // Override the default view renderer with our own.
        Zend_Controller_Action_HelperBroker::addPath('Mcmr/Controller/Action/Helper', 'Mcmr_Controller_Action_Helper');
        Zend_Controller_Action_HelperBroker::addHelper(new Mcmr_Controller_Action_Helper_ViewRenderer());

        // Moves view scripts to each site's path. Adds subsite script path
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
        $viewRenderer->setViewBasePathSpec(SITE_PATH . '/views/:module');
        $viewRenderer->setViewSitePathSpec(SITE_PATH . '/subsites/:site/:module');
        $view = $this->getResource('view');

        // Add Mcmr View helpers
        $view->addHelperPath('Mcmr/View/Helper', 'Mcmr_View_Helper');
        $view->addHelperPath('Mcmr/View/Helper/Navigation', 'Mcmr_View_Helper_Navigation');

        // Add a shared folder for all modules. Useful for partials
        $view->addScriptPath(SITE_PATH.'/views/_common');
        $view->addScriptPath(SITE_PATH.'/_common');

        $viewRenderer->setView($view);
        
        // Change the layout controller plugin so that we can change layout based on site ID
        Mcmr_Layout::startMvc(array('pluginClass'=>'Mcmr_Layout_Controller_Plugin_Layout'));
    }

    /**
     * Initilise the system plugins. Add ACLs, image manager access, and the full page cache.
     */
    protected function _initPlugins()
    {
        // Set a plugin loader cache
        if (Zend_Registry::get('app-config')->pluginLoaderCache) {
            $classFileIncCache = SITE_PATH . '/data/pluginLoaderCache.php';
            if (file_exists($classFileIncCache)) {
                include_once $classFileIncCache;
            }
            Zend_Loader_PluginLoader::setIncludeFileCache($classFileIncCache);
        }

        
        $front = $this->getResource('frontcontroller');
        
		# ACL plugin. Authenticates each user before dispatch.
		$front->registerPlugin(new Mcmr_Controller_Plugin_Acl());
		
		# URL Fixer plugin. Fixes invalid URLs and performs 301 redirects
		$front->registerPlugin(new Mcmr_Controller_Plugin_UrlFixer());
		
		# Timezome Plugin. Custom per-user timezone support
		$front->registerPlugin(new Mcmr_Controller_Plugin_Timezone());

		# Page Caching Plugin
        $front->registerPlugin(new Mcmr_Controller_Plugin_PageCache());

    }

    /**
     * Load the sitemap file and create the Zend_Navigation object.
     */
    protected function _initNavigation()
    {
        $this->bootstrap('FrontController');
        $this->bootstrap('request');
        
        $front = $this->getResource('FrontController');
        $request = $front->getRequest();

        try {
            $pages = Mcmr_Config_Xml::getInstance('sitemap/'.$request->getSiteName().'.xml', APPLICATION_ENV);
        } catch (Zend_Config_Exception $e) {
            $pages = Mcmr_Config_Xml::getInstance('sitemap/pages.xml', APPLICATION_ENV);
        }
        Zend_Registry::set('Zend_Navigation', new Zend_Navigation($pages));
    }

    /**
     * Initilise the database profiler. Mcmr profiler records last SQL query. Reported in error logs on failure
     */
    protected function _initProfiler()
    {
        try {
            $this->bootstrap('db');
        } catch (Zend_Application_Bootstrap_Exception $e) {
            // The DB adapter may not be configured. But that isn't a problem
        }
        
        try {
            $this->bootstrap('splitdb');
        } catch (Zend_Application_Bootstrap_Exception $e) {
            // The split DB adapter may not be configured. But that isn't a problem
        }
        
        $db = $this->getResource('db');
        if (null !== $db) {
            if (APPLICATION_STATS) {
                $profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
            } else {
                $profiler = new Mcmr_Db_Profiler();
            }
            $profiler->setEnabled(true);
        
            $db->setProfiler($profiler);
        }
        
        $db = Mcmr_Db_Table_Abstract::getDefaultReadAdapter();
        if (null !== $db) {
            if (APPLICATION_STATS) {
                $profiler = new Zend_Db_Profiler_Firebug('Read DB Queries');
            } else {
                $profiler = new Mcmr_Db_Profiler();
            }
            $profiler->setEnabled(true);
        
            $db->setProfiler($profiler);
        }
        
        $db = Mcmr_Db_Table_Abstract::getDefaultWriteAdapter();
        if (null !== $db) {
            if (APPLICATION_STATS) {
                $profiler = new Zend_Db_Profiler_Firebug('Write DB Queries');
            } else {
                $profiler = new Mcmr_Db_Profiler();
            }
            $profiler->setEnabled(true);
        
            $db->setProfiler($profiler);
        }
    }

    /**
     * Allow the custom routes to be disabled by confuguration during development. Makes it easier to
     * see what templates should be used. Routes aren't obfuscated by custom routes.
     */
    protected function _initRouter()
    {
        if ('production' !== APPLICATION_ENV && Zend_Registry::get('app-config')->disableRoutes) {
            $this->bootstrap('frontcontroller');
            $front = $this->getResource('frontcontroller');
            $front = Zend_Controller_Front::getInstance();

            $front->setRouter(new Mcmr_Controller_Router_Development());
        }
    }

    /**
     * Execute the site specific bootstrap
     */
    protected function _initSite()
    {
        $this->_siteInit();
    }

    protected function _siteInit()
    {
    }
}
